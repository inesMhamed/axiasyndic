import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardInterneComponent } from './dashboard-interne.component';

describe('DashboardInterneComponent', () => {
  let component: DashboardInterneComponent;
  let fixture: ComponentFixture<DashboardInterneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardInterneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
