import { AfterContentChecked, AfterViewChecked,AfterViewInit,ChangeDetectorRef , Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { echartStyles } from 'src/app/shared/echart-styles';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import { delay, first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
//import { EChartOption } from 'echarts';
Quill.register('modules/imageResize', ImageResize);

import Quill from 'quill'
import QuillEmoji from 'quill-emoji'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
Quill.register('modules/emoji-shortname', QuillEmoji.ShortNameEmoji)

import { DataLayerService } from 'src/app/shared/services/data-layer.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { CommuneService } from 'src/app/shared/services/commune.service';
import { GroupementService } from 'src/app/shared/services/groupement.service';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { Actualite } from 'src/app/shared/models/actualites.model';
import * as moment from 'moment';
import { ItemsList } from '@ng-select/ng-select/lib/items-list';
import { Commentaire } from 'src/app/shared/models/commentaire.model';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { Reaction } from 'src/app/shared/models/reaction.model';
import { Proprietaire } from 'src/app/shared/models/proprietaire.model';
import { PrivacyPublication } from 'src/app/shared/models/privacyPublication.model';
import { Publication } from 'src/app/shared/models/publication.model';
import { Bloc } from 'src/app/shared/models/bloc.model';
import { Appartement } from 'src/app/shared/models/appartement.model';
import { Residence } from 'src/app/shared/models/residence.model';
import Swal from 'sweetalert2';

import {NgxGalleryOptions} from '@kolkov/ngx-gallery';
import {NgxGalleryImage} from '@kolkov/ngx-gallery';
import {NgxGalleryAnimation} from '@kolkov/ngx-gallery';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { StatAdmin } from 'src/app/shared/models/dashboard/statAdmin';
import { trigger, state, style, transition, animate } from '@angular/animations';
import * as echarts from 'echarts';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-dashboard-interne',
  templateUrl: './dashboard-interne.component.html',
  styleUrls: ['./../dashboard-v3.component.scss'],
  providers: [DatePipe],
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(1000, style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger("scrollAnimation", [
      state(
        "show",
        style({
          opacity: 1,
          transform: "translateY(0)"
        })
      ),
      state(
        "hide",
        style({
          height: 80,
          opacity: 0,
          transform: "translateY(150%)"
        })
      ),
      transition("show => hide", animate("1000ms ease-out")),
      transition("hide => show", animate("1000ms ease-in"))
    ]),
  ]
})
export class DashboardInterneComponent implements OnInit {
  showContact: any[] = []
  showlistjaime: any[] = []
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    headerToolbar: {
      left: 'title',
      center: '',
      right: 'prev,next'
    },
    dateClick: this.handleDateClick.bind(this), // bind is important!
    events: [],
    //eventLimit: true, // for all non-TimeGrid views
    views: {
      timeGrid: {
        eventLimit: 3 // adjust to 6 only for timeGridWeek/timeGridDay
      }
    }


  };
  submitted: boolean = false;
  handleDateClick(arg) {
  }
  chartBar1: any;
  formBasic: FormGroup;
  formBasicPrivacy: FormGroup;
  @ViewChild('editor') editor;
  htmlStringModel: any;
  htmlStringFormatted: any;
  quillEditorRef: any;
  modules = {
    'emoji-shortname': true,
    'emoji-textarea': true,
    'emoji-toolbar': true,
    toolbar: [

      ['bold', 'italic', 'underline'], // toggled buttons
      // [{ 'header': 1 }, { 'header': 2 }], // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      // [{ 'script': 'sub' }, { 'script': 'super' }], // superscript/subscript
      //[{ 'indent': '-1' }, { 'indent': '+1' }], // outdent/indent
      [{ 'direction': 'rtl' }], // text direction
      [{ 'size': ['small', false, 'large', 'huge'] }], // custom dropdown
      //[{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ 'color': [] }, { 'background': [] }], // dropdown with defaults from theme
      //[{ 'font': [] }],
      // [{ 'align': [] }],
      // ['clean'], // remove formatting button
      ['image'], // link and image, video
      ['emoji']
    ],
    imageResize: true // for image resize
  };

  modules1 = {
    'emoji-shortname': true,
    'emoji-textarea': true,
    'emoji-toolbar': true,
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      ['link', 'image', 'video'],
      ['emoji']                      // link and image, video
    ],
    imageResize: true, // for image resize
    imageCompress: {
      quality: 0.7, // default 0.7
      maxWidth: 640, // default 1024
      maxHeight: 479, // default 400
      //imageType: 'image/jpeg', // default
      debug: false, // default
      suppressErrorLogging: false, // default
    }
  };

  comment: boolean = true


  isCommentOpened: boolean = false;
  actualiteIdCourant: string;
  isLikedPost: boolean = false;
  valueModelPrivacy: number = 3;
  isDialogPrivacyOpened: boolean = false;
  actualitesList: any[] = [];
  commentairesList: Commentaire[] = [];
  blocs: any[];
  residences: any[];
  appartements: any[];
  readonly: boolean = false;
  id_current_user: any;
  selected: any[] = null;
  fileName = '';
  currentAttachmentPost: any;
  id: string;
  role: any;
  Admin: any;
  root: any;
  imgSrc: string = "../../../assets/images/faces/defaultAvatar.png"
  proprietaire: Proprietaire = new Proprietaire();
  currentCommentPost: any[] = [];
  privacyPublicationModel: PrivacyPublication = new PrivacyPublication();
  publicationModel: Publication = new Publication();
  //salesChartPie: EChartOption;

  /*----------------------*/
  images: any[];
  responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 1
    },
    {
      breakpoint: '768px',
      numVisible: 1
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];

  responsiveOptions2: any[] = [
    {
      breakpoint: '1500px',
      numVisible: 5
    },
    {
      breakpoint: '1024px',
      numVisible: 3
    },
    {
      breakpoint: '768px',
      numVisible: 2
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];
  disabledButton: boolean = false;
  fileNameVideo = ''
  displayBasic: boolean;
  displayBasic2: boolean;
  displayCustom: boolean;
  activeIndex: number = 0;
  /*---------------------*/
  showScroll: boolean;
  showScrollHeight = 300;
  hideScrollHeight = 20;
  lineChart1;


    editorStyle = {

    minHeight: '55px',
    position: 'relative',
    marginLeft: '20px',
    marginRight: '20px'
  }
  urlVideo: File;
  imgpublication: any;
  publiciteList: any[] = [];
  durepubModel: any;
  datedebutpubModel: any = '';
  showbtnpub: boolean = false;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  statistiqueAdminModel:StatAdmin=new StatAdmin();
  cardReclOpview:boolean=false;
  flip: string = 'inactive';
  optionPieRecette: any;
  optionBarTransfert:any;
  optionReclamationFirstRec:any;
  optionReclamationSecondRec:any;

  currentDate:Date=new Date();
  valueFirstRec:number=0;
  valueSecondRec:number=0;

  viewDashboardStat:boolean=false;
  viewMobile:boolean=false;
  switchRec:boolean=false;
  listBienProp:any[]=[];
  selectedBienProp:any[]=[];
  selectedCoprop:any;


  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;
  pauseOnFocus = true;
  imagesCaroussel = [62, 83, 466, 965, 982, 1043, 738].map((n) => `https://picsum.photos/id/${n}/900/500`);
  isliked: any = [];
  restlistContact: any = [];
  contactPublication: any = [];
  likePublication: any;
  appartementsModel: any[] = [];
  blocsModel: any[] = [];
  @ViewChild('carousel', { static: true }) carousel: NgbCarousel;

  constructor(private cdRef: ChangeDetectorRef, private fb: FormBuilder, private fbPrivacy: FormBuilder,
    public sanitizer: DomSanitizer, private datePipe: DatePipe,
    private actualiteservice: ActualiteService, private toastr: ToastrService, private modalService: NgbModal, private dl: DataLayerService,
    private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService,
    private proprietaireService: ProprietaireService, private spinnerService: NgxSpinnerService, private dashboardService: DashboardService, public router: Router) {

    this.id_current_user = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.Admin = this.role.includes('ROLE_ADMIN');
    this.root = this.role.includes('ROLE_ROOT');
    this.getDetailUtilisateur(this.id_current_user);
    if (window.innerWidth <= 1200) {
      this.viewMobile = true;
    } else {
      this.viewMobile = false;
    }
  }

  @HostListener('window:scroll', [])
  onWindowScroll()
  {
    if (( window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > this.showScrollHeight)
    {
        this.showScroll = true;
    }
    else if ( this.showScroll && (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < this.hideScrollHeight)
    {
      this.showScroll = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    //event.target.innerWidth;

    if( event.target.innerWidth<=1200){
        this.viewMobile=true;
    }else{
      this.viewMobile=false;
    }
    

    //console.log("viewMobile: ",this.viewMobile, "   -- width",event.target.innerWidth)

  }

  


  ngOnInit() {
    this.spinnerService.show()
    this.buildFormPrivacy();
    this.getActualite();
    this.listeresidence();
    this.getStatisticAdmin();



    setTimeout(() => {
      this.initPIEChartRecette();

      }, 1000);


      

      this.formBasic = this.fb.group({
          content: new FormControl( null, ),
          nameValueModelPrivacy: new FormControl( null, )
      });




      this.chartBar1 = {
          ...echartStyles.defaultOptions, ...{
              series: [{
                  type: 'bar',
                  barWidth: 6,

                  itemStyle: {
                      color: '#b75bff',
                      ...echartStyles.lineShadow
                  },
                  data: [{
                      name: 'Bar 1',
                      value: 40
                  }, {
                      name: 'Bar 2',
                      value: 60,
                      itemStyle: {
                          color: '#4CAF50'
                      }
                  }, {
                      name: 'Bar 3',
                      value: 80,
                  }, {
                      name: 'Bar 4',
                      value: 70,
                  }, {
                      name: 'Bar 5',
                      value: 60,
                  }, {
                      name: 'Bar 6',
                      value: 70,
                  }, {
                      name: 'Bar 7',
                      value: 80,
                  }, {
                      name: 'Bar 8',
                      value: 40,
                  }, {
                      name: 'Bar 9',
                      value: 70,
                      itemStyle: {
                          color: '#4CAF50'
                      }
                  }]
              }]
          }
      };
      //wz
      this.galleryOptions = [
          {
              width: '100%',
              height: '400px',
              thumbnailsColumns: 4,
              arrowPrevIcon: 'i-Arrow-Back-3',
              arrowNextIcon: 'i-Arrow-Forward-2',
              closeIcon: 'i-Close-Window',
              imageAnimation: NgxGalleryAnimation.Slide
          },
          // max-width 800
          {
              breakpoint: 800,
              width: '100%',
              height: '600px',
              thumbnailsColumns: 3,
              imagePercent: 80,
              thumbnailsPercent: 20,
              thumbnailsMargin: 20,
              thumbnailMargin: 20
          },
          // max-width 400
          {
              breakpoint: 400,
              thumbnailsColumns: 2,
              preview: false
          }
      ];

      this.galleryImages = [
          /*{
              small: 'https://preview.ibb.co/jrsA6R/img12.jpg',
              medium: 'https://preview.ibb.co/jrsA6R/img12.jpg',
              big: 'https://preview.ibb.co/jrsA6R/img12.jpg'
            },
            {
              small: 'https://preview.ibb.co/kPE1D6/clouds.jpg',
              medium: 'https://preview.ibb.co/kPE1D6/clouds.jpg',
              big: 'https://preview.ibb.co/kPE1D6/clouds.jpg'
            },
            {
              small: 'https://preview.ibb.co/mwsA6R/img7.jpg',
              medium: 'https://preview.ibb.co/mwsA6R/img7.jpg',
              big: 'https://preview.ibb.co/mwsA6R/img7.jpg'
            },{
              small: 'https://preview.ibb.co/kZGsLm/img8.jpg',
              medium: 'https://preview.ibb.co/kZGsLm/img8.jpg',
              big: 'https://preview.ibb.co/kZGsLm/img8.jpg'
            }  */
      ];

     
     
      this.lineChart1 = {
          ...echartStyles.lineFullWidth, ...{
              series: [{
                  data: [80, 40, 90, 20, 80, 30, 90, 30, 80, 10, 70, 30, 90],
                  ...echartStyles.smoothLine,
                  markArea: {
                      label: {
                          show: true
                      }
                  },
                  areaStyle: {
                      color: 'rgba(102, 51, 153, .15)',
                      origin: 'start'
                  },
                  lineStyle: {
                      // width: 1,
                      color: 'rgba(102, 51, 153, 0.68)',
                  },
                  itemStyle: {
                      color: '#663399'
                  }
              }, {
                  data: [20, 80, 40, 90, 20, 80, 30, 90, 30, 80, 10, 70, 30],
                  ...echartStyles.smoothLine,
                  markArea: {
                      label: {
                          show: true
                      }
                  },
                  areaStyle: {
                      color: 'rgba(255, 152, 0, 0.15)',
                      origin: 'start'
                  },
                  lineStyle: {
                      // width: 1,
                      color: 'rgba(255, 152, 0, .6)',
                  },
                  itemStyle: {
                      color: 'rgba(255, 152, 0, 1)'
                  }
              }]
          }
      };
  }

  
  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }

  


  scrollToTop()
  {

    (function smoothscroll()
    { var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0)
      {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 5));
      }
    })();
  }

  imageClick(index: number,srcList) {
      this.activeIndex = index;
      this.displayCustom = true;
      this.images=srcList;
      ////console.log("this.images: ",this.images," activeIndex: ",this.activeIndex)
  }

  buildFormPrivacy() {
    this.formBasicPrivacy = this.fbPrivacy.group({
      residence: new FormControl(null, Validators.required),
      bloc: new FormControl(null,),
      appartement: new FormControl(null,)
    });
  }

  ngAfterViewChecked():void {
      this.cdRef.detectChanges();
  }

  ngAfterContentChecked(): void {
     this.cdRef.detectChanges();
  }

  /* ========= get service ========== */

  getDetailUtilisateur(idUtilisateur:string){
      if (idUtilisateur != null) {

          this.proprietaireService.getProprietairebyId(idUtilisateur).subscribe((pro: any) => {
              if (pro.statut === true) {
                  //console.log('pro!', pro.data)
                  this.proprietaire = pro.data
                  this.imgSrc = pro.data.photo
                  this.proprietaire.logoText=this.proprietaire.nomComplet.charAt(0);

          this.spinnerService.hide()
        }
        else {
          this.spinnerService.hide()
        }
      })
    }
    else {
      this.spinnerService.hide()
    }
  }

  getActualite() {
    this.spinnerService.show()
    this.actualiteservice.getActualites().pipe(first()).subscribe((res: any = []) => {
      this.actualitesList = res.listPublication
      this.publiciteList = res.listPublicite
      this.actualitesList.forEach((element, i) => {
        if (element.publicite == true) {
          this.imgpublication = element.srcList[0].previewImageSrc
          this.spinnerService.hide()
        }
        Object.keys(element.reactions).forEach((n) => {
          var el = element.reactions[n]
          if (parseInt(el?.cbmarq) === parseInt(this.id_current_user)) {
            el.NomComplet = 'Vous'
            this.isliked = el
            element.reactions.splice(parseInt(n), 1)
            element.reactions.unshift(this.isliked)
          }
          this.isliked = []
        })
      });
      this.spinnerService.hide()
    },
      (error) => {
        this.spinnerService.hide()
      }, () => {
        this.spinnerService.show()
        this.actualitesList.forEach((post: Actualite) => {
          post.logoText = post.cbcreateur.nomComplet.charAt(0);
          post.srcList = post.srcList.map(item => ({ small: item.previewImageSrc, medium: item.previewImageSrc, big: item.previewImageSrc }))
        })
        this.publiciteList.forEach((post: Actualite) => {
          post.logoText = post.cbcreateur.nomComplet.charAt(0);
          post.srcList = post.srcList.map(item => ({ small: item.previewImageSrc, medium: item.previewImageSrc, big: item.previewImageSrc }))
        })
        this.spinnerService.hide()
      });
    this.spinnerService.hide()
  }
  //open modal list contact publication
  openModalLike(like, content) {
    this.likePublication = like
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title-like', windowClass: 'modalContactpub' })
      .result.then((result) => {

      }, (error) => {

      })
  }

  //open modal list contact publication
  openModalpublication(contact, content) {
    this.contactPublication = contact
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title-contact', windowClass: 'modalContactpub' })
      .result.then((result) => {
        console.log("result : ", result);
      }, (error) => {
        console.log('error dismiss modal!', error);
      })
  }
  //open modal periode publication
  openModalperiode(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title-pub', windowClass: 'modalperiodecss' })
      .result.then((result) => {
      }, (error) => {
      })
  }
  hiddenPub() {
    document.getElementById('carouselpub').style.display = 'none'
  }
  listeresidence() {
    this.spinnerService.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule }));
        this.spinnerService.hide()
      }
      else {
        this.spinnerService.hide()
      }
    },
      error => {
        this.spinnerService.hide()
      }
    );
  }


  OnChangeResidence(event: any) {
    this.spinnerService.show()
    this.blocs = [];

    if (event) {
      event.forEach(i => {
        this.blocService.getBlocs(i.cbmarq).subscribe((res: any) => {
          // this.blocs.push(res.data);
          if (res.data) {
            this.blocs = this.blocs.concat(res.data.map(clt =>
              ({ cbmarq: clt.cbmarq, label: clt.intitule + '-R' + clt.residence?.cbmarq, cbres: clt.residence?.cbmarq })));
            this.spinnerService.hide()
          }
          else {
            this.spinnerService.hide()
          }
        },
          error => {
            this.spinnerService.hide()
            //console.log( 'erreur: ', error);
          }
        );
      });
    }

  }

  OnChangeBloc(event: any) {
    this.spinnerService.hide()
    this.appartements = [];
    if (event) {
      event.forEach(i => {
        this.appartementService.getAppartementByBloc(i.cbmarq).subscribe((res: any) => {
          if (res.data) {
            this.appartements = this.appartements.concat(res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule + '-B' + clt.bloc.cbmarq, cbbl: clt.bloc.cbmarq })));
            this.spinnerService.hide()
          } else {
            this.spinnerService.hide()
          }
        },
          error => {
            this.spinnerService.hide()
            //console.log( 'erreur: ', error);
          }
        );
      });
    }
  }

  cleargroupement(event: any) {
      this.formBasicPrivacy.patchValue({residences: null});
      //this.readonly = false;
      this.listeresidence();
  }

  clearresidence(event: any) {
      this.formBasicPrivacy.patchValue({bloc: null});
      this.formBasicPrivacy.patchValue({appartement: null});
  }

  clearbloc(event: any) {
      this.formBasicPrivacy.patchValue({appartement: null});
  }

  removeresidence(event: any) {
      const thisBloc: any[] = [];
      this.formBasicPrivacy.value.bloc.forEach(i => {
          const removeIndex =  this.formBasicPrivacy.value.bloc.findIndex(itm => itm?.cbres === event.value.cbmarq);
          if (removeIndex !== -1) {
              thisBloc.push(this.formBasicPrivacy.value.bloc[removeIndex]);
              delete this.formBasicPrivacy.value.bloc[removeIndex];
          }
      });
      thisBloc.forEach(j => {
          this.formBasicPrivacy.value.appartement.forEach(k => {
              const removeIndex2 = this.formBasicPrivacy.value.appartement.findIndex(itm => itm?.cbbl === j?.cbmarq);
              if (removeIndex2 !== -1) {
                  delete this.formBasicPrivacy.value.appartement[removeIndex2];
              }
          });
      });
      this.formBasicPrivacy.patchValue({bloc: this.formBasicPrivacy.value.bloc});
      this.formBasicPrivacy.patchValue({appartement: this.formBasicPrivacy.value.appartement});
  }

  removebloc(event: any) {

      this.formBasicPrivacy.value.appartement.forEach(k => {
          const removeIndex2 = this.formBasicPrivacy.value.appartement.findIndex(itm => itm?.cbbl === event.value.cbmarq);
          if (removeIndex2 !== -1) {
              delete this.formBasicPrivacy.value.appartement[removeIndex2];
          }
      });
      this.formBasicPrivacy.patchValue({appartement: this.formBasicPrivacy.value.appartement});
  }

  /* =========================================== */

  onFileSelected(event) {
    const file: File = event.target.files[0];
    if (file) {
      this.fileName = file.name;
      const formData = new FormData();
      this.currentAttachmentPost = file;
      formData.append("thumbnail", file);
    }
    if (this.currentAttachmentPost) {
      this.urlVideo = null
      this.images = null

    }

  }

  deleteVideoUpload() {
    this.fileNameVideo = "";
    this.url = null
    this.urlVideo = null
    this.disabledButton = false
  }
  deleteFileUpload() {
    this.fileName = "";
    this.urlVideo = null
    this.currentAttachmentPost = null;

  }
  url;
  format;
  onSelectFile(event) {


    const file = event.target.files && event.target.files[0];
    if (file) {
      this.urlVideo = event.target.files[0]
      this.fileNameVideo = file.name;
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if (file.type.indexOf('image') > -1) {
        this.format = 'image';
      } else if (file.type.indexOf('video') > -1) {
        this.format = 'video';
        var lengthFile = event.target.files[0].size / 1024 / 1024
        if (lengthFile > 50) {
          //  this.openmodalVideo(this.modalBasicVideo)
          Swal.fire({
            title: 'Atteindre taille max du fichier',
            text: 'Impossible de joindre ce fichier. Fichier trop volumineux !',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonText: 'Ok',
          }).then((result) => {
            if (result.value) {
            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
          });
          return
        }

      }
      reader.onload = (event) => {
        this.url = (<FileReader>event.target).result;
        //  this.publicationModel.video=this.urlVideo
      }
      if (this.urlVideo != null || this.urlVideo != undefined) {
        this.disabledButton = true
        this.currentAttachmentPost = null
        this.fileName = "";
        this.images = null
      }
      else {
        this.disabledButton = false

      }
    }

  }
  showSpinner(): void {
    this.spinnerService.show();
  }
  hideSpinner(): void {
    this.spinnerService.hide();
  }
  submitPost() {
    this.showSpinner();
    this.publicationModel.durepub = this.durepubModel;
    this.publicationModel.produit = this.privacyPublicationModel.residence;
    this.publicationModel.bloc = this.privacyPublicationModel.bloc;
    this.publicationModel.bien = this.privacyPublicationModel.appartement;
    this.publicationModel.text = this.htmlStringFormatted;
    this.publicationModel.video = this.urlVideo;
    this.publicationModel.attachement = this.currentAttachmentPost;
    this.publicationModel.all = +this.privacyPublicationModel.etat;
    if (this.root == false) {
      this.publicationModel.publicite = 0
    } else {
      this.publicationModel.publicite = 1
      this.publicationModel.all = 1
      this.publicationModel.attachement = null
      this.publicationModel.datedebutpub = this.datePipe.transform(this.datedebutpubModel, 'dd-MM-yyyy')
    }
    if (this.comment == false) {
      this.publicationModel.commenter = 0
    } else {
      this.publicationModel.commenter = 1

    }
    if ((this.publicationModel.text != null || this.publicationModel.text != null) || (this.publicationModel.attachement != null || this.publicationModel.attachement != undefined) || (this.publicationModel.video != undefined || this.publicationModel.video != null)) {
      this.showbtnpub = true
      const myFormValue = this.publicationModel;
      const myFormData = new FormData();
      Object.keys(myFormValue).forEach(name => {
        myFormData.append(name, myFormValue[name]);
      });
      this.modalService.dismissAll();
      this.actualiteservice.postActualites(myFormData).pipe(first()).subscribe((res: any) => {
        if (res.statut === true) {
          this.toastr.success('Publication ajouté avec succès .', 'Success!', { progressBar: true });
          this.getActualite();
          this.oncloseDialogPrivacy();
          this.modalService.dismissAll();
          this.htmlStringFormatted = "";
          this.url = "";
          this.fileNameVideo = ""
          this.fileName = ""
          this.htmlStringModel = "";
          this.publicationModel = new Publication();
          this.valueModelPrivacy = 3;
          this.hideSpinner();

        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          this.hideSpinner();
        }
      },
        (error) => {

          this.toastr.error('Erreur lors d\'ajout d\'une publication . Veuillez réessayer !', 'Erreur!', { progressBar: true });
          this.hideSpinner();
        });
    }
    else {
      this.showbtnpub = false
      this.spinnerService.hide()
    }

    this.hideSpinner();
  }
  changeTitle() {
    this.comment = !this.comment
    if (!this.comment) {
      document.getElementById("iconcomment").title = "Activer commentaire"
    }
    if (this.comment) {
      document.getElementById("iconcomment").title = "Désactiver commentaire"
    }
  }
  onSelectionChanged = (event) => { }
  onContentChanged = (event) => {
      ////console.log("content changed: ",event.html)
      this.htmlStringFormatted = event.html;
  }

  public onSelect(item) {
      //console.log('tag selected: value is ' + item);
  }



  /* ============= POST FCT=============*/

  /* ============= POST FCT=============*/
  openModalPrivacy(selectValue, content) {
    this.privacyPublicationModel.etat = selectValue;
    if (selectValue == 2) {
      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
        .result.then((result) => {
          console.log("result : ", result);
        }, (error) => {
          console.log('error dismiss modal!', error);
          if (error == 0) {
            this.selected = [];
            this.blocs = [];
            this.appartements = [];
            this.blocsModel = [];
            this.durepubModel = null;
            this.datedebutpubModel = null;
            this.blocsModel = [];
            this.appartementsModel = [];
            this.privacyPublicationModel.bloc = [];
            this.privacyPublicationModel.appartement = [];
            this.privacyPublicationModel.residence = [];
          }
          // this.oncloseDialogPrivacy();
        });
    }
    else {
      this.privacyPublicationModel.bloc = [];
      this.privacyPublicationModel.appartement = [];
      this.privacyPublicationModel.residence = [];
    }
  }



  oncloseDialogPrivacy() {
      this.formBasicPrivacy.reset();
      this.modalService.dismissAll();
  }


  submitPrivacy() {
    this.spinnerService.hide()
    if (this.formBasicPrivacy.invalid) {
      this.spinnerService.hide()
      this.submitted = true;
      return;
    }
    if (this.formBasicPrivacy.value.bloc != null && this.formBasicPrivacy.value.bloc != undefined) {
      this.formBasicPrivacy.value.bloc.forEach((bloc: Bloc) => {
        ////console.log('bloc:',this.formBasicPrivacy.value.bloc)
        this.privacyPublicationModel.bloc.push(bloc.cbmarq)
      });
      this.spinnerService.hide()
    }
    if (this.formBasicPrivacy.value.appartement != null && this.formBasicPrivacy.value.appartement != undefined) {
      this.formBasicPrivacy.value.appartement.forEach((appartement: Appartement) => {
        this.privacyPublicationModel.appartement.push(appartement.cbmarq)
      });
      this.spinnerService.hide()
    }
    if (this.formBasicPrivacy.value.residence != null && this.formBasicPrivacy.value.residence != undefined) {
      this.formBasicPrivacy.value.residence.forEach((residence: Residence) => {
        this.privacyPublicationModel.residence.push(residence.cbmarq)
      });
      this.spinnerService.hide()
    }
    this.modalService.dismissAll()
    // this.oncloseDialogPrivacy();
  }



  confirmUpdateStatutActualite(actualite: Actualite, statut: boolean) {

    let title: string = "";
    let texte: string = "";
    if (statut) {
      title = "Supprimer une publication";
      texte = "Voulez-vous vraiment supprimer cette publication ?"
    } else {
      title = "Restaurer une publication";
      texte = "Voulez-vous vraiment restaurer cette publication ?"
    }

    Swal.fire({
      title: title,
      text: texte,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {
        this.updateStatutActualite(actualite, statut);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }



  updateStatutActualite(actualite: Actualite, statut: boolean) {

    this.spinnerService.show()
    this.actualiteservice.updateStatutActualite(actualite).subscribe((res: any) => {

          if (res.statut === true) {
              if(statut){
                  this.toastr.success('Publication supprimé avec succès .', 'Success!', {progressBar: true});
                  //this.actualitesList=this.actualitesList.filter(item=> item.cbmarq != actualite.cbmarq);
                  this.getActualite();


              }else{
                  this.toastr.success('Publication restauré avec succès .', 'Success!', {progressBar: true});
                  actualite.publier=true;
                  this.getActualite();
              }

        this.spinnerService.hide()

      } else {
        this.spinnerService.hide()
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }

    }, error => {
      this.spinnerService.hide()
      this.toastr.error('Erreur lors de modification de statut d\'une publication . Veuillez réessayer !', 'Erreur!', { progressBar: true });

      })
  }

  // ============ supprimer definitivement la publication =============//


 confirmDeleteDefinetlyPost(actualite:Actualite) {


  Swal.fire({
    title: 'Supprimer une publication',
    text: 'Voulez-vous vraiment supprimer définitivement cette publication ?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Confirmer',
    cancelButtonText: 'Cancel'
  }).then((result) => {
    if (result.value) {
      this.deleteDefinetlyPost(actualite);
    } else if (result.dismiss === Swal.DismissReason.cancel) {
    }
  });
}

  deleteDefinetlyPost(actualite: Actualite) {
    this.spinnerService.show()
    this.actualiteservice.deleteDefinetlyPost(actualite).subscribe((res: any) => {
      if (res.statut === true) {
        this.toastr.success('Publication supprimer définitivement avec succès !');

        this.getActualite();
      } else {
        this.spinnerService.hide()
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    },
      error => {
        this.spinnerService.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }

    );
  }

  /* ============= COMMENT FCT ============*/

  openComment(actualiteId: any) {
    this.spinnerService.show()
    if (actualiteId != this.actualiteIdCourant) {
      this.isCommentOpened = true;
    } else {
      this.isCommentOpened = !this.isCommentOpened;
    }
    this.actualiteIdCourant = actualiteId;
    if (this.isCommentOpened) {

          this.actualiteservice.getCommentsOfActualite(actualiteId).subscribe((res: any) => {
              this.commentairesList = res.listCommentaire;
              if (this.commentairesList != null && this.commentairesList.length > 0) {
                  this.commentairesList.forEach((comm: any) => {
                      comm.logoText = comm?.cbcreateur?.name?.charAt(0);

          });
        }
        this.spinnerService.hide()
        // //console.log("open list comment: ",this.commentairesList)
      }, error => {
        this.spinnerService.hide()
      }, () => {
        this.spinnerService.hide()
      });
    }
  }

  refreshCommentList() {
    this.spinnerService.show()
    this.actualiteservice.getCommentsOfActualite(this.actualiteIdCourant).subscribe((res: any) => {
      this.commentairesList = res.listCommentaire;
      if (this.commentairesList != null && this.commentairesList.length > 0) {
        this.commentairesList.forEach((comm: any) => {
          comm.logoText = comm?.cbcreateur?.name?.charAt(0);

        })
        this.spinnerService.hide()
      }
      else {
        this.spinnerService.hide()
      }
    }, error => {
      this.spinnerService.hide()
    })
  }

  postComment(actualite: Actualite, indic, currentCommentPost: any) {
    this.spinnerService.show()
    let reaction: Reaction = new Reaction();
    reaction.type = 1;
    reaction.commentaire = currentCommentPost;
    this.actualiteservice.postReactionOfActualite(reaction, actualite.cbmarq).subscribe(
      res => {
        this.spinnerService.hide()
      },
      error => {
        this.spinnerService.hide()
      },
      () => {
        // this.currentCommentPost = '';
        this.currentCommentPost[indic] = "";
        actualite.NBcomment = actualite.NBcomment + 1;
        this.refreshCommentList();
        this.openComment(actualite.cbmarq);
        this.spinnerService.hide()
      }
    );

    }



  confirmDeleteComment(actualite:Actualite,comm:Commentaire) {

    //console.log("cbmarq: ",comm.cbmarq)
    Swal.fire({
      title: 'Supprimer un Commentaire',
      text: 'Voulez-vous vraiment supprimer ce commentaire ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {
        this.deleteComment(actualite,comm);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  deleteComment(actualite: Actualite, comm: Commentaire) {
    this.spinnerService.show()
    this.actualiteservice.deleteComment(comm, comm.cbmarq).subscribe((res: any) => {
      if (res.statut === true) {
        this.toastr.success('Commentaire supprimer avec succès !');
        actualite.NBcomment = actualite.NBcomment - 1;
        this.refreshCommentList();
        this.spinnerService.hide()
      } else {
        this.spinnerService.hide()
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    },
      error => {
        this.spinnerService.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }

      );
  }

  /* ======== LIKE FCT ==================*/

  likePost(actualite: Actualite) {
    this.spinnerService.show()


      this.isLikedPost=!this.isLikedPost;


      //inverser dislike --> like //
      if(actualite.aime==3 ){
          let reaction:Reaction=new Reaction();
          reaction.type=2;
          reaction.commentaire="";

      this.actualiteservice.postReactionOfActualite(reaction, actualite.cbmarq).subscribe(res => {
        this.spinnerService.hide()
      }, error => {
        this.spinnerService.hide()
      }, () => {
        this.spinnerService.hide()
        actualite.aime = 2;
        actualite.NBaime = actualite.NBaime + 1;
      })

      }

      //inverser like --> dislike  //
      if(actualite.aime==2){
          let reaction:Reaction=new Reaction();
          reaction.type=3;
          reaction.commentaire="";

      this.actualiteservice.postReactionOfActualite(reaction, actualite.cbmarq).subscribe(res => {
        this.spinnerService.hide()
      }, error => { this.spinnerService.hide() }, () => {
        this.spinnerService.hide()
        actualite.aime = 3;
        actualite.NBaime = actualite.NBaime - 1;
      })
    }
  }





  //================
  openLinkHref(link:string){
      window.open(link, "_blank");
  }

  onScrollTop(){
      window.scroll(0,0);

  }

  //=========== STATISTICS ==========//


  initPIEChartRecette() {
    this.spinnerService.show()
    var myChart;

    //console.log("elemnt Recette: ",document.getElementById('mainRecetteStat'))
    if(document.getElementById('mainRecetteStat')!=null){
       myChart = echarts.init(document.getElementById('mainRecetteStat'));
    }
     

    this.optionPieRecette = {
      tooltip: {
        trigger: 'item',
        formatter: function (params) {
          return `
                  ${params.name}: (${params.percent}%)<br />
                  ${params.data.name1}: ${params.data.value1}<br />
                  ${params.data.name2}: ${params.data.value2}
                  `;
        },
      },
      legend: {
        orient: 'vertical',
        left: 'left',
        data: [],
      },
      series: [],
     
    };

    // use configuration item and data specified to show chart
    //if(document.getElementById('mainRecetteStat')!=null){
    myChart.setOption(this.optionPieRecette);
    this.spinnerService.hide()
    //}
   

  

  } 

  initBarChartTransfert() {
    this.spinnerService.show()
    var myChart;
    if(document.getElementById('mainTransfertBar')!=null){
      myChart = echarts.init(document.getElementById('mainTransfertBar'));
    }
   
   

    this.optionBarTransfert  = {
        title: {
          text: ''
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow'
          }
        },
        legend: {},
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        xAxis: {
          type: 'category',
          boundaryGap: []
        },
        yAxis: {
          type: 'value',
          data: [0, 0.01]
        },
        series: [
          {
            name: 'Prévu',
            type: 'bar',
            data: [],
            itemStyle: {
              color: "#b39ddb"
            },
          },
          {
            name: 'Réel',
            type: 'bar',
            data: [],
            itemStyle: {
              color: "#90caf9"
            },
          }
        ]
      };
    

    // use configuration item and data specified to show chart
    if(document.getElementById('mainTransfertBar')!=null){
      myChart.setOption(this.optionBarTransfert);
      this.spinnerService.hide()
    }
    this.spinnerService.hide()

  }

  initChartReclamation() {
    this.spinnerService.show()
    var myChart;
    if(document.getElementById('mainReclamation')!=null){
      myChart = echarts.init(document.getElementById('mainReclamation'));
      this.spinnerService.hide()
    }


    this.optionReclamationFirstRec = {
      series: [
        {
          type: 'gauge',
          axisLine: {
            lineStyle: {
              width: 30,
              color: [
                [0.3, '#67e0e3'],
                [0.7, '#37a2da'],
                [1, '#fd666d']
              ]
            }
          },
          pointer: {
            itemStyle: {
              color: 'auto'
            }
          },
          axisTick: {
            distance: -30,
            length: 8,
            lineStyle: {
              color: '#fff',
              width: 2
            }
          },
          splitLine: {
            distance: -30,
            length: 30,
            lineStyle: {
              color: '#fff',
              width: 4
            }
          },
          axisLabel: {
            color: 'auto',
            distance: 10,
            fontSize: 13
          },
          detail: {
            valueAnimation: true,
            formatter: '{value} ',
            color: 'auto'
          },
          data: [
            {
              value: 0
            }
          ]
        }
      ]
    };
     

    myChart.setOption(this.optionReclamationFirstRec);
    this.spinnerService.hide()
  }
  getStatisticAdmin() {
    this.spinnerService.show()
    this.dashboardService.getstatisticInterne().subscribe((res: any) => {
      if (res.statut) {
        this.statistiqueAdminModel = res.data;
        this.spinnerService.hide()
      }
      else {
        this.spinnerService.hide()
      }
    }, error => {
      this.spinnerService.hide()
    }, () => {
      this.spinnerService.hide()
      this.fillPieChartRecette();
      this.getEventOfCalendar();
      //set color to transfert reçus
      this.statistiqueAdminModel.TransfertRecu.forEach(item => {
        item.backgroundColor = this.materialColor2();
      })
    })
  }

  getEventOfCalendar(){
    //filter reunion
    var datePipe = new DatePipe('en-FR');
    let listReunion= this.statistiqueAdminModel.listeReunion.map(item => 
      ({cbmarq: item.cbmarq,title:item.objet, start: datePipe.transform(item.dateDebut2.date, 'yyyy-MM-dd HH:mm') , end: datePipe.transform(item.dateFin2.date, 'yyyy-MM-dd HH:mm'),backgroundColor: '#4caf50',borderColor: '#4caf50' })
      )

      /*listReunion=listReunion.concat({cbmarq: 20,title:'event1', start: datePipe.transform('2022-01-01 09:30:00.000000', 'yyyy-MM-dd HH:mm') , end: datePipe.transform('2022-01-01 09:30:00.000000', 'yyyy-MM-dd HH:mm'),backgroundColor: 'green',borderColor: 'green' });
      listReunion=listReunion.concat({cbmarq: 21,title:'event2', start: datePipe.transform('2022-01-05 09:30:00.000000', 'yyyy-MM-dd HH:mm')  , end: datePipe.transform('2022-01-05 09:30:00.000000', 'yyyy-MM-dd HH:mm') ,backgroundColor: 'green',borderColor: 'green' });
      listReunion=listReunion.concat({cbmarq: 22,title:'event3', start: datePipe.transform('2022-01-05 09:30:00.000000', 'yyyy-MM-dd HH:mm'), end: datePipe.transform('2022-01-05 09:30:00.000000', 'yyyy-MM-dd HH:mm'),backgroundColor: 'green',borderColor: 'green' });
      listReunion=listReunion.concat({cbmarq: 23,title:'event4', start: datePipe.transform('2022-01-23 09:30:00.000000', 'yyyy-MM-dd HH:mm') , end: datePipe.transform('2022-01-23 09:30:00.000000', 'yyyy-MM-dd HH:mm'),backgroundColor: 'green',borderColor: 'green' });
      listReunion=listReunion.concat({cbmarq: 24,title:'event5', start: datePipe.transform('2022-01-25 09:30:00.000000', 'yyyy-MM-dd HH:mm') , end: datePipe.transform('2022-01-25 09:30:00.000000', 'yyyy-MM-dd HH:mm'),backgroundColor: 'green',borderColor: 'green' });
      listReunion=listReunion.concat({cbmarq: 25,title:'event6', start: datePipe.transform('2022-01-31 09:30:00.000000', 'yyyy-MM-dd HH:mm') , end: datePipe.transform('2022-01-31 09:30:00.000000', 'yyyy-MM-dd HH:mm'),backgroundColor: 'green',borderColor: 'green' });
      */


      //listReunion=listReunion.concat(listReunion);   
      //console.log("listReunion: ",this.statistiqueAdminModel.listeReunion)

    this.calendarOptions = {
      initialView: 'dayGridMonth',      
      headerToolbar: {
        left: 'title',
        center: '',
        right: 'prev,next'
      },
      dateClick: this.handleDateClick.bind(this), // bind is important!,
      dayMaxEvents: true,
      dayMaxEventRows: true,
      views: {
        dayGridMonth: {
          //dayMaxEventRows: 2,
          dayMaxEvents:1,
          moreLinkContent:function(args){            
            let count_element = document.createElement('span');           
            count_element.setAttribute('style', 'font-size: 9px;font-weight:600;color:#4caf50');
            count_element.innerHTML = "+"+args.num+'Plus';
           return {
               domNodes: [count_element],
           }
        },  
        /*eventContent :function(event, element){
          //console.log("info: ",event)
          let icon_element = document.createElement('span.fc-circle'); 
          icon_element.setAttribute('style', 'font-size: 16px;font-weight:600;color:green');
          icon_element.innerHTML = "◴"; //● 

          let title_element = document.createElement('span.fc-title'); 
          title_element.setAttribute('style', 'font-size: 10px;font-weight:600;color:green');
          title_element.innerHTML = event.timeText+" "+event.title; //event.timeText;
          
          return {
            domNodes: [icon_element,title_element],
          }
        
        }*/    
        },        
      },

      events: listReunion ,
      firstDay: 1,
      locale:'fr',
      timeZone: 'Africa/Tunis',
      eventTimeFormat: { hour: '2-digit', minute: '2-digit' },
      themeSystem: 'Darky',
      eventClick: this.navigateToDetailReunion.bind(this),  
     
     
      
     
    };

    

  }

 

  navigateToDetailReunion(info){

    this.router.navigate(['/communite/detail-reunion/'+info.event._def.extendedProps.cbmarq]);    

  }

  navigateToDetailTransfert(cbmarq){
    this.router.navigate(['/syndic/details-transfert/'+cbmarq]);      


  }


  fillPieChartRecette() {
    this.spinnerService.show()
    //var myChart;

    if(document.getElementById('mainRecetteStat')!=null){

      var  myChart = echarts.init(document.getElementById('mainRecetteStat'));

    }
    let headerData=this.statistiqueAdminModel.recettes.map(item=> item.Produit);
    let bodyData=this.statistiqueAdminModel.recettes.map(el => ({ percent: el?.pourcentage,value: (el?.Prevu+el?.Reel), name: el?.Produit, value1: el?.Reel, name1: "Réel", value2: el?.Prevu, name2: "Prevu" }));


    //console.log("header pie chart: ",headerData)
    //console.log("bodyData pie chart: ",bodyData)



    this.optionPieRecette = {
      tooltip: {
        trigger: 'item',
        formatter: function (params) {
          return `
          ${params.name}: (${params.percent}%)<br />
          ${params.data.name1}: ${params.data.value1}<br />
          ${params.data.name2}: ${params.data.value2}
                 
          `;
        },
      },
      legend: {
        orient: 'vertical',
        left: 'left',
        // data: headerData,
      },
      series: [
        {
          name: 'Recette par produit',
          type: 'pie',
          radius: '55%',
          center: ['50%', '50%'],
          data: bodyData         
          ,
        },
      ],
      color :['#2d72c0', '#f3a254', '#6abebf', '#52a1e5', '#916df6', '#ef6e85', '#ff7043','#dce775','#a1887f','#90a4ae','#f44336','#9c27b0','#43a047','#827717'],

      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)'
        }
      }
    };

    // use configuration item and data specified to show chart
    if(document.getElementById('mainRecetteStat')!=null){

      myChart.setOption(this.optionPieRecette);
      window.addEventListener('resize',function(){
        myChart.resize();
      })
      this.spinnerService.hide()
    }
   

  }

  fillBarChartTransfert() {
    this.spinnerService.show()
    var myChart;
    if (document.getElementById('mainTransfertBar') != null) {

      myChart = echarts.init(document.getElementById('mainTransfertBar'));
      this.spinnerService.hide()
    }

    let headerData:any[]=[];
    let bodyDataPrevu:any[]=[]
    let bodyDataReel:any[]=[]
 
    if(this.statistiqueAdminModel?.listetransfert){
      headerData=this.statistiqueAdminModel?.listetransfert?.map(item=> item.Produit);
      bodyDataPrevu =this.statistiqueAdminModel?.listetransfert?.map(el =>  el.prevu );
      bodyDataReel =this.statistiqueAdminModel?.listetransfert?.map(el => el.reel );
    }

    //console.log("header pie chart: ",headerData)
    //console.log("Prevu pie chart: ",bodyDataPrevu)
    //console.log("Reel pie chart: ",bodyDataReel)



    this.optionBarTransfert  = {
     
        title: {
          text: ''
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow'
          },
          formatter: function (params) {
      
            return `${params[0]?.axisValueLabel}<br />
                    ${params[0]?.seriesName}: ${params[0]?.data}<br />
                    ${params[1]?.seriesName}: ${params[1]?.data}`;
          }
        },
        legend: {
          data: [
            {
              name: 'Prévu',
              icon: 'circle'
            },
            {
              name: 'Réel',
              icon: 'circle'
            },
          ]
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        xAxis: {
          type: 'category',  
          data: headerData       
        },
        yAxis: {
          type: 'value',
          boundaryGap: [0, 0.01],
          axisLabel: {
            formatter: (function(value){
              let label=(""+value).replace(',', '');       
              
              return label;
            })
           }
        },           
        series: [
          {
            name: 'Prévu',
            type: 'bar',
            data: bodyDataPrevu,
            itemStyle: {
              color: "#b39ddb"
            },
            barMinWidth :10,
            barMaxWidth : 10
           
          },
          {
            name: 'Réel',
            type: 'bar',
            data: bodyDataReel,
            itemStyle: {
              color: "#90caf9"
            },
            barMinWidth :10,
            barMaxWidth : 10
           
          }
        ]
      };
    

    // use configuration item and data specified to show chart
    if(document.getElementById('mainTransfertBar')!=null){

      myChart.setOption(this.optionBarTransfert);
          
      window.addEventListener('resize',function(){
        myChart.resize();
      })
      this.spinnerService.hide()
    }
   

  }


  fillReclamation(){

    this.spinnerService.hide()
    if (this.statistiqueAdminModel?.firstRec != undefined) {
      this.valueFirstRec = (this.statistiqueAdminModel?.firstRec?.rec / this.statistiqueAdminModel?.allrec) * 100;

    }
    if(this.statistiqueAdminModel?.secondRec!=undefined){
      this.valueSecondRec=((this.statistiqueAdminModel?.secondRec?.rec / this.statistiqueAdminModel?.allrec) * 100);

    }
    
    this.fillChartReclamationFirstRec();
    
  }

  fillChartReclamationFirstRec() {
    this.spinnerService.show()
    var myChart;
    if (document.getElementById('mainReclamation') != null) {
      myChart = echarts.init(document.getElementById('mainReclamation'));
      this.spinnerService.hide()
    }

    this.optionReclamationFirstRec = {
      series: [
        {
          type: 'gauge',
          axisLine: {
            lineStyle: {
              width: 30,
              color: [
                [0.3, '#67e0e3'],
                [0.7, '#37a2da'],
                [1, '#fd666d']
              ]
            }
          },
          pointer: {
            itemStyle: {
              color: 'auto'
            }
          },
          axisTick: {
            distance: -30,
            length: 8,
            lineStyle: {
              color: '#fff',
              width: 2
            }
          },
          splitLine: {
            distance: -30,
            length: 30,
            lineStyle: {
              color: '#fff',
              width: 4
            }
          },
          axisLabel: {
            color: 'auto',
            distance: 10,
            fontSize: 13
          },
          detail: {
            valueAnimation: true,
            formatter: '\n\n\n\n {value}% \n'+ (this.statistiqueAdminModel?.firstRec?.Label!=undefined?this.statistiqueAdminModel?.firstRec?.Label:''),
            color: 'auto'
          },
          data: [
            {
              value: +this.valueFirstRec.toFixed(1)           
            }
          ]
        }
      ]
    };

    
    if(document.getElementById('mainReclamation')!=null){
      myChart.setOption(this.optionReclamationFirstRec);
      this.spinnerService.hide()
    }
    


  }

  fillChartReclamationSecondRec(){

    var myChart;
    if(document.getElementById('mainReclamation')!=null){
      myChart = echarts.init(document.getElementById('mainReclamation'));
    }
    
    this.optionReclamationSecondRec= {
      series: [
        {
          type: 'gauge',
          axisLine: {
            lineStyle: {
              width: 30,
              color: [
                [0.3, '#67e0e3'],
                [0.7, '#37a2da'],
                [1, '#fd666d']
              ]
            }
          },
          pointer: {
            itemStyle: {
              color: 'auto'
            }
          },
          axisTick: {
            distance: -30,
            length: 8,
            lineStyle: {
              color: '#fff',
              width: 2
            }
          },
          splitLine: {
            distance: -30,
            length: 30,
            lineStyle: {
              color: '#fff',
              width: 4
            }
          },
          axisLabel: {
            color: 'auto',
            distance: 10,
            fontSize: 13
          },
          detail: {
            valueAnimation: true,
            formatter: '\n\n\n\n{value}% \n'+(this.statistiqueAdminModel?.secondRec?.Label!=undefined?this.statistiqueAdminModel?.secondRec?.Label:''),//
            color: 'auto'
          },
          data: [
            {
              value: +this.valueSecondRec.toFixed(1),//           
            }
          ]
        }
      ]
    };

    if(document.getElementById('mainReclamation')!=null){
      myChart.setOption(this.optionReclamationSecondRec);
    }

    

  


  }

  /*** type 1 : reclamation, type 2 : intervention , type 3 : reunions , type 4 : documenets , type  5  : transferts**/
  actualiteNavigateLink(timeline){

    //console.log("navigation : ",timeline.cbmarq," type: ",timeline.type);
    this.router.navigate(['/communite/info-preventif/'+timeline.cbmarq]); 
    

  }



  onClickCrumb(event){ 


    if(event.target.innerHTML=="Dashboard"){
      this.viewDashboardStat= false;
      setTimeout(() => {
        let el = document.getElementById('middleSide');
        el.scrollIntoView();
      }, 100);
     
    }

    if(event.target.innerHTML=="Statistique"){
      this.viewDashboardStat= true;
      setTimeout(() => {
        let el = document.getElementById('leftSide');
        el.scrollIntoView();
      }, 100);
        
   
    }

    this.getStatisticAdmin();

  }


  confirmerActiveCoProp(coprop: any, content) {
    this.spinnerService.show()
    //console.log("copprop: ",coprop);
    this.selectedCoprop=coprop;

    this.appartementService.getAppartementsProp(coprop.parent.id).subscribe((res:any)=>{
      if(res.statut){
        //console.log("liste des bien by prop: ",res.data)
        this.listBienProp = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerService.hide()
      }
    }, error => {
      this.spinnerService.hide()
     }, () => {
      this.spinnerService.hide()
      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
        .result.then((result) => {
          //console.log("result",result);
        }, (error) => {
          //console.log("on error modal: ",error);
          this.oncloseDialogCoprop();
        });

    })

    
  }

  activerCoprop() {
    this.spinnerService.show()
    let formActiver = {
      id: this.selectedCoprop.id,
      appartements: this.selectedBienProp.map(clt => ({ appartement: clt.value })),
    }
    this.proprietaireService.activateCopropMobile(formActiver).subscribe((res: any) => {
      if (res.statut) {
        this.toastr.success('Co-proprietaire activé avec succès !');
        this.oncloseDialogCoprop();
        this.getStatisticAdmin();
        this.spinnerService.hide()
      } else {
        this.spinnerService.hide()
        this.toastr.error('Erreur lors d\'activation, veuillez réessayer plus tard !');

      }
    }, error => {
      this.spinnerService.hide()
    })

  }

  oncloseDialogCoprop() {
    this.listBienProp=[];
    this.modalService.dismissAll();
    this.selectedCoprop=null;
    this.selectedBienProp=[];
}




  /********** RANDOM MATERIAL COLOR **********/

  
  pickRandomProperty(obj) {
    var result;
    var count = 0;
    for (var prop in obj)
      if (Math.random() < 1 / ++count)
        result = prop;
    return result;
  }
  

  materialColor2(){

    var colors = {
      "red": {        
       
        "300": "#e57373",
       
      },
      "pink": {        
      
        "300": "#f06292",
       
      },
      "purple": {        
        
        "200": "#ce93d8",
       
      },
      "deepPurple": {         
      
        "200": "#b39ddb",
       
      },
      "indigo": {        
        
        "300": "#9fa8da",
       
      },
      "blue": {         
        
        "300": "#64b5f6",
      
      },
      "lightBlue": {         
      
        "300": "#4fc3f7",
      
      },
      "cyan": {        
       
        "300": "#4dd0e1",
       
      },
      "teal": {         
      
        "300": "#4db6ac",
       
      },
      "green": {         
       
        "300": "#81c784",
       
      },
      "lightGreen": {
                
        "300": "#aed581",
       
      },
      "lime": {
              
        "200": "#e6ee9c",
       
      },
      "yellow": {         
      
        "200": "#fff59d",
       
      },
      "amber": {         
       
        "200": "#ffe082",
       
      },
      "orange": {         
       
        "200": "#ffcc80",
       
      },
      "deepOrange": {         
       
        "200": "#ffab91",
       
      }
     
     
    }
    // pick random property
  var colorList = colors[this.pickRandomProperty(colors)];
  var newColorKey = this.pickRandomProperty(colorList);
  var newColor = colorList[newColorKey];
  return newColor;



  }
  materialColor() {
    // colors from https://github.com/egoist/color-lib/blob/master/color.json
    var colors = {
        "red": {        
         
          "300": "#e57373",
         
        },
        "pink": {        
        
          "300": "#f06292",
         
        },
        "purple": {        
          
          "300": "#ba68c8",
         
        },
        "deepPurple": {         
        
          "300": "#9575cd",
         
        },
        "indigo": {        
          
          "300": "#7986cb",
         
        },
        "blue": {         
          
          "300": "#64b5f6",
        
        },
        "lightBlue": {         
        
          "300": "#4fc3f7",
        
        },
        "cyan": {        
         
          "300": "#4dd0e1",
         
        },
        "teal": {         
        
          "300": "#4db6ac",
         
        },
        "green": {         
         
          "300": "#81c784",
         
        },
        "lightGreen": {
                  
          "300": "#aed581",
         
        },
        "lime": {
                
          "300": "#dce775",
         
        },
        "yellow": {         
        
          "300": "#fff176",
         
        },
        "amber": {         
         
          "300": "#ffd54f",
         
        },
        "orange": {         
         
          "300": "#ffb74d",
         
        },
        "deepOrange": {         
         
          "300": "#ff8a65",
         
        }
       
       
      }
      // pick random property
      //var property = pickRandomProperty(colors);
    var colorList = colors[this.pickRandomProperty(colors)];
    var newColorKey = this.pickRandomProperty(colorList);
    var newColor = colorList[newColorKey];
    return newColor;
  }
  
  

}
