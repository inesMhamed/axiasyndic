import { AfterContentChecked, AfterViewChecked, ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { echartStyles } from 'src/app/shared/echart-styles';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
Quill.register('modules/imageResize', ImageResize);

import Quill from 'quill'
import QuillEmoji from 'quill-emoji'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
Quill.register('modules/emoji-shortname', QuillEmoji.ShortNameEmoji)

import { DataLayerService } from 'src/app/shared/services/data-layer.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { CommuneService } from 'src/app/shared/services/commune.service';
import { GroupementService } from 'src/app/shared/services/groupement.service';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { Actualite } from 'src/app/shared/models/actualites.model';
import * as moment from 'moment';
import { ItemsList } from '@ng-select/ng-select/lib/items-list';
import { Commentaire } from 'src/app/shared/models/commentaire.model';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { Reaction } from 'src/app/shared/models/reaction.model';
import { Proprietaire } from 'src/app/shared/models/proprietaire.model';
import { PrivacyPublication } from 'src/app/shared/models/privacyPublication.model';
import { Publication } from 'src/app/shared/models/publication.model';
import { Bloc } from 'src/app/shared/models/bloc.model';
import { Appartement } from 'src/app/shared/models/appartement.model';
import { Residence } from 'src/app/shared/models/residence.model';
import Swal from 'sweetalert2';

import { NgxGalleryOptions } from '@kolkov/ngx-gallery';
import { NgxGalleryImage } from '@kolkov/ngx-gallery';
import { NgxGalleryAnimation } from '@kolkov/ngx-gallery';


@Component({
    selector: 'app-dashboard-v3',
    templateUrl: './dashboard-v3.component.html',
    styleUrls: ['./dashboard-v3.component.scss']
})



export class DashboardV3Component implements OnInit {


    chartPie1: any;
    chartPie2: any;
    chartBar1: any;
    formBasic: FormGroup;
    formBasicPrivacy: FormGroup;
    @ViewChild('editor') editor;
    htmlStringModel: any;
    htmlStringFormatted: any;
    quillEditorRef: any;


    isCommentOpened: boolean = false;
    actualiteIdCourant: string;
    isLikedPost: boolean = false;
    valueModelPrivacy: number = 1;
    isDialogPrivacyOpened: boolean = false;
    actualitesList: Actualite[] = [];
    commentairesList: Commentaire[] = [];
    blocs: any[];
    residences: any[];
    appartements: any[];
    readonly: boolean = false;
    id_current_user: any;
    selected: any[] = null;
    fileName = '';
    currentAttachmentPost: any;
    id: string;
    role: any;
    isAdmin: boolean = false;
    isinterne: boolean = false;
    isprop: boolean = false;
    isroot: boolean = false;
    imgSrc: string = "../../../assets/images/faces/defaultAvatar.png"
    proprietaire: Proprietaire = new Proprietaire();
    currentCommentPost: string;
    privacyPublicationModel: PrivacyPublication = new PrivacyPublication();
    publicationModel: Publication = new Publication();

    /*----------------------*/
    images: any[];

    displayBasic: boolean;
    displayBasic2: boolean;
    displayCustom: boolean;
    activeIndex: number = 0;
    /*---------------------*/
    showScroll: boolean;
    showScrollHeight = 300;
    hideScrollHeight = 20;
    lineChart1;


    editorStyle = {

        minHeight: '55px',
        position: 'relative',
        marginLeft: '20px',
        marginRight: '20px'
    }

    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];



    constructor(private cdRef: ChangeDetectorRef, private fb: FormBuilder, private fbPrivacy: FormBuilder,
        private actualiteservice: ActualiteService, private toastr: ToastrService, private modalService: NgbModal, private dl: DataLayerService,
        private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService,
        private proprietaireService: ProprietaireService) {

        this.id_current_user = localStorage.getItem('id');
        this.role = localStorage.getItem('role');


        /*console.log("role: ",this.role)
        console.log("isAdmin: ",this.isAdmin)
        console.log("isinterne: ",this.isinterne)
        console.log("isprop: ",this.isprop)*/

    }

    @HostListener('window:scroll', [])
    onWindowScroll() {
        if ((window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) > this.showScrollHeight) {
            this.showScroll = true;
        }
        else if (this.showScroll && (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) < this.hideScrollHeight) {
            this.showScroll = false;
        }
    }



    ngOnInit() {

        this.isAdmin = this.role.includes('ROLE_ADMIN');
        this.isinterne = this.role.includes('ROLE_INTERN');
        this.isprop = this.role.includes('ROLE_RESIDENT');
        this.isroot = this.role.includes('ROLE_ROOT');
        this.getDetailUtilisateur(this.id_current_user);
    }



    ngAfterViewChecked(): void {
        this.cdRef.detectChanges();
    }

    ngAfterContentChecked(): void {
        this.cdRef.detectChanges();
    }

    /* ========= get service ========== */

    getDetailUtilisateur(idUtilisateur: string) {
        if (idUtilisateur != null) {

            this.proprietaireService.getProprietairebyId(idUtilisateur).subscribe((pro: any) => {
                if (pro.statut === true) {
                    //console.log('pro!', pro.data)
                    this.proprietaire = pro.data
                    this.imgSrc = pro.data.photo
                    this.proprietaire.logoText = this.proprietaire.nomComplet.charAt(0);


                }
            })
        }
    }






}
