import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListReunionComponent } from './reunion/list-reunion/list-reunion.component';
import { AddReunionComponent } from './reunion/add-reunion/add-reunion.component';
import { DetailReunionComponent } from './reunion/detail-reunion/detail-reunion.component';
import { ListReclamationComponent } from './reclamation/list-reclamation/list-reclamation.component';
import { DetailReclamationComponent } from './reclamation/detail-reclamation/detail-reclamation.component';
import { RoomReunionComponent } from './reunion/room-reunion/room-reunion.component';
import { AuthGuard } from 'src/app/_directives/_helpers/auth.guard';
import { ListDocumentComponent } from './document/list-document/list-document.component';
import { InfoDocumentComponent } from './document/info-document/info-document.component';
import { ChatComponent } from '../chat/chat/chat.component';
import { ListPreventifComponent } from './preventif/list-preventif/list-preventif.component';
import { MessagesComponent } from '../inbox/messages/messages.component';
import { InfoPreventifComponent } from './preventif/info-preventif/info-preventif.component';
import { GenOperationComponent } from '../syndic/config-maintenance/operation/gen-operation/gen-operation.component';
import { MobileroomReunionComponent } from './reunion/room-reunion/mobileroom-reunion/mobileroom-reunion.component';

const routes: Routes = [
  {
    path: 'reunions',
    component: ListReunionComponent, canActivate: [AuthGuard]
  }, {
    path: 'info-reunion/:cbmarq',
    component: AddReunionComponent, canActivate: [AuthGuard]
  }, {
    path: 'detail-reunion/:cbmarq',
    component: DetailReunionComponent, canActivate: [AuthGuard]
  },
  {
    path: 'mobileroom-reunion/:cbmarq',
    component: MobileroomReunionComponent, canActivate: [AuthGuard]
  },
  {
    path: 'room-reunion/:cbmarq',
    component: RoomReunionComponent, canActivate: [AuthGuard]
  }, {
    path: 'reclamations',
    component: ListReclamationComponent, canActivate: [AuthGuard]
  }, {
    path: 'detail-reclamation/:cbmarq',
    component: DetailReclamationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'preventif',
    component: ListPreventifComponent, canActivate: [AuthGuard]
  },
  {
    path: 'info-preventif/:cbmarq',
    component: InfoPreventifComponent, canActivate: [AuthGuard]
  },
  {
    path: 'gen-maintenance',
    component: GenOperationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'documents',
    component: ListDocumentComponent, canActivate: [AuthGuard]
  }, {
    path: 'info-document/:cbmarq',
    component: InfoDocumentComponent, canActivate: [AuthGuard]
  }
  , {
    path: 'inbox',
    component: MessagesComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommuniteRoutingModule { }
