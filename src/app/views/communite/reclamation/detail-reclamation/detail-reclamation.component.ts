import {Component, ElementRef, Input, OnInit, TemplateRef, ViewChild, ViewChildren} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ReclamationService} from '../../../../shared/services/reclamation.service';
import {ToastrService} from 'ngx-toastr';
import {first, scan} from 'rxjs/operators';
import {Reclamation} from '../../../../shared/models/reclamation.model';
import {from, merge, Observable, Subject, Subscription} from 'rxjs';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {NgForm} from '@angular/forms';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import marked from 'marked';
import {
  Message,
  SendMessageEvent
} from '@progress/kendo-angular-conversational-ui';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { SafeUrl } from '@angular/platform-browser';


import { NgxGalleryOptions } from '@kolkov/ngx-gallery';
import { NgxGalleryImage } from '@kolkov/ngx-gallery';
import { NgxGalleryAnimation } from '@kolkov/ngx-gallery';
import { NgxSpinnerService } from 'ngx-spinner';
import { th } from 'date-fns/locale';


class User{

    id: any;
    name?: string;
    avatarUrl?: string | SafeUrl;

    constructor(){}

}

@Component({
  selector: 'app-detail-reclamation',
  templateUrl: './detail-reclamation.component.html',
  styleUrls: ['./detail-reclamation.component.scss'],
  animations: [SharedAnimations]
})
export class DetailReclamationComponent implements OnInit {

  @ViewChild('labelImport', { static: true }) labelImport: ElementRef;
  @ViewChild('modalLong') modalLong: TemplateRef<any>;
  fileToUpload: File = null;
  id: any;
  reclamation = new Reclamation();
  imageObject: Array<object>=[];
  images: any[] = [];
  commentaires: Message;
  infinite: boolean = false;
  widht: any = '50%';
  imgURL: any;
  role: any;
  isAdmin: boolean = false;

  public feed: Observable<Message[]>;

  public user: User=new User();

  public bot: User =new User();

  private local: Subject<Message> = new Subject<Message>();
  can_statut: Permission;
  id_current_user: any;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  have_access:boolean =false;
  can_detail: Permission;


  constructor(
    private reclamationService: ReclamationService,
    private actRoute: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private spinnerservice: NgxSpinnerService,
    private permissionservice: DroitAccesService
  ) {
    this.feed = merge(from([]), this.local).pipe(
        scan((acc: Message[], x: Message) => [...acc, x], [])
    );
  }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.role = localStorage.getItem('role');
    if (this.role.indexOf('ROLE_ADMIN') !== -1) {
      this.isAdmin = true;
    }
    this.id_current_user = localStorage.getItem('id');
    this.can_statut = this.permissionservice.search( this.id_current_user, 'FN21000103');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000084');


    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
    });

    ////console.log("this.id: ",this.id);
    ////console.log("id_current_user: ",this.id_current_user);

    if (this.id) {
      this.checkUpdateAccess();
      
    }else{
      this.goOut();
    }

    this.galleryOptions = [
      {
          width: '100%',
          height: '400px',
          //thumbnailsColumns: 4,
          arrowPrevIcon: 'i-Arrow-Back-3',
          arrowNextIcon: 'i-Arrow-Forward-2',
          closeIcon: 'i-Close-Window',
          imageAnimation: NgxGalleryAnimation.Slide,
          thumbnails:false
        
      },
      // max-width 800
      {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          thumbnailsColumns: 3,
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20,
          thumbnails:false
         
      },
      // max-width 400
      {
          breakpoint: 400,
          thumbnailsColumns: 2,
          thumbnails:false,
          preview: false
      }
    ];

    this.galleryImages = [];
  }

  goOut(){

    //if access false ==> go out to 403
    this.router.navigate(['403']);   

  }
  checkUpdateAccess(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000084').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getReclamation();
      }
    });
  }


  getReclamation(){

    this.reclamationService.getReclamation(this.id).pipe(first())
    .subscribe((rest: any) => {
      if (rest.data) {
        this.reclamation = rest.data;
        //console.log("get reclamation: ",this.reclamation)
        this.listeCommentaire(rest.data.commentaire);
        ////console.log('rest.data.commentaire', rest.data);
        if (this.reclamation?.attachements.length > 2) {
          this.infinite = true;
        }
        if (this.reclamation?.attachements.length === 1) {
          this.widht = '50%';
        }
        /*this.reclamation?.attachements?.forEach(res => {
          this.images.push({
            image: res.attachement,
            thumbImage: res.attachement,
            alt: 'image réclamation',
            // title: 'image réclamation'
          });
        })
        this.imageObject = this.images ;*/

          this.galleryImages = this.reclamation?.attachements.map(item => ({ small: item.attachement, medium: item.attachement, big: item.attachement }))
          this.spinnerservice.hide()
        }
      });

  }

  ngAfterViewInit() {

  }

  public sendMessage(e: SendMessageEvent): void {
    // this.local.next(e.message);
    if (this.reclamation?.statut?.code !== 'E0049') {
      const formCommentaire = { user: this.id_current_user, commentaire: e.message.text }
      this.reclamationService.postcommentaire(this.id, formCommentaire).pipe(first()).subscribe((res: any) => {
        if (res.statut === true) {
          const resultat: any = [];
          resultat.push(res.data.commentaire[res.data.commentaire.length - 1])
          this.listeCommentaire(resultat);
          ////console.log( res, res.data);
          this.spinnerservice.hide()
        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
        (error) => {
          this.toastr.error('Erreur lors de l\'ajout d\'une réclamation . Veuillez réessayer !', 'Erreur!', { progressBar: true });
        });
    } else {
      this.spinnerservice.hide()
      this.toastr.error('Cette discussion est fermé. Réclamation cloturé! ', 'Erreur!', { progressBar: true });
    }
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' , size: 'xl'})
        .result.then((result) => {
      ////console.log(result);
    }, (reason) => {
      //console.log('Err!', reason);
    });
  }
  onclose() {
    this.modalService.dismissAll();
  }
  public listeCommentaire(commentaires) {
    // this.local = new Subject<Message>();
   // this.clearMessages();

    if (commentaires) {
      commentaires.forEach(res => {
        if (res.type === 1 ) {
          ////console.log('type 1', res.proprietaire);
          this.bot = res.proprietaire;
          this.local.next({
            author: this.bot,
            text: res.commentaire,
            timestamp: new Date(res.cbcreation)
          });

        } else {
          ////console.log('type 2', res.proprietaire);
          this.user = res.proprietaire;
          this.local.next({
            author: this.user,
            text: res.commentaire,
            timestamp: new Date(res.cbcreation)
          });
        }
      });
    }

  }
  clearMessages() {
    this.local.next();
  }
  public renderMarkdown(md: string): string {
    return marked(md);
  }

  onFileChange(files: FileList) {
    let reader = new FileReader();
    this.fileToUpload = files.item(0);
    reader.readAsDataURL(files.item(0));
    reader.onload = (_event) => {
      this.imgURL = (reader.result);
    };
  }
  public changeStatut(statut: any) {
    this.spinnerservice.show()
    const myFormData = new FormData();
    myFormData.append('user',this.id_current_user );
    myFormData.append('statut', statut );
    myFormData.append('attachement', this.fileToUpload);
    ////console.log(myFormData);
    this.reclamationService.changeStatut(this.id, myFormData).pipe(first()).subscribe((res: any) => {
      if (res.statut === true) {
        this.toastr.success('Le statut de la réclamation a été modifié avec succès !');
        //this.router.navigate(['/communite/reclamations']);
        // this.ngOnInit();
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/communite/detail-reclamation/', this.id]);
        });
        // this.getReclamation();
        this.modalService.dismissAll();
        this.spinnerservice.hide()
      } else {
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    },
      (error) => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
      });
  }

}
