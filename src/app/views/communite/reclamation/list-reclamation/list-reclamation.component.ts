import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Reclamation} from '../../../../shared/models/reclamation.model';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatInput} from '@angular/material/input';
import {ReclamationService} from '../../../../shared/services/reclamation.service';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {ToastrService} from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import {AppartementService} from '../../../../shared/services/appartement.service';
import {Router} from '@angular/router';
import {BlocService} from '../../../../shared/services/bloc.service';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-reclamation',
  templateUrl: './list-reclamation.component.html',
  styleUrls: ['./list-reclamation.component.scss']
})
export class ListReclamationComponent implements OnInit {

  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File[] = [];
  displayedColumns: string[] = ['type' , 'emplacement' , 'objet' , 'description' , 'cbcreateur', 'cbcreation' , 'statut' , 'action'];
  dataSource = new MatTableDataSource<Reclamation>();
  pageEvent: PageEvent;
  reunions;
  loading: boolean;
  submitted: boolean=false;
  blocs: any[] = [];
  types: any[];
  appartements: any[] = [];
  appartementsProp: any[] = [];
  imgURL: any[] = [];
  formBasic: FormGroup;
  transforme: any = false;
  rec: any = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  @ViewChild('closeModal') private closeModal: ElementRef;
  @ViewChild('closedModal') private closedModal: ElementRef;
  data: any;
  role: any;
  isInterne: any = false;

  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];
  categories:any[]=[];
  isTerrain:boolean=false; //check categorie terrain
  isAppartement:boolean=false;//check categorie appartement
  terrains:any[]=[];
  have_access:boolean =false;

  constructor(
    private reclamationService: ReclamationService,
    private dl: ResidenceService,
    private toastr: ToastrService,
    private preferenceService: PreferencesService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private appartementService: AppartementService,
    private blocService: BlocService,
    private router: Router,
    private spinnerservice: NgxSpinnerService,
    private permissionservice: DroitAccesService,
    private residenceService: ResidenceService
  ) { }

  ngOnInit() {
    this.spinnerservice.show()
    this.role = localStorage.getItem('role');
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000081');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000083');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000082');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000084');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000085');

    //console.log("can delete: ",this.can_delete)

      //if access false ==> go out to 403
      this.permissionservice.getAccessUser( this.id_current_user, 'FN21000085').subscribe((res:any)=>{

        this.have_access =res.data;
       
      },error=>{},()=>{
  
        if(!this.have_access){
          this.router.navigate(['403']);
        }else{
          this.buildFormBasic();
          this.listeReclamations();
          this.listCategorie();
        }
      });
  }
  buildFormBasic() {
    this.formBasic = this.fb.group({
      bien: new FormControl( null, ),
      emplacement: new FormControl( null, Validators.required),
      terrain: new FormControl( null, ),
      objet: new FormControl( null,  Validators.required),
      description: new FormControl( null,  Validators.required),
      priorite: new FormControl( 'Moyenne',  Validators.required),
      appartements: new FormControl(null, ),
      attachements: new FormControl( [],  ),
      categorie:new FormControl( null,  Validators.required),
    });


    if (this.role.indexOf('ROLE_INTERN') !== -1 || this.role.indexOf('ROLE_ADMIN') !== -1) {

      ////console.log("user interne / Admin ...")
      this.isInterne = true;
      this.listeappartements();//pour input "destiné à"
      //this.listebloc();
      this.formBasic.controls['appartements'].setValidators([Validators.required]);//destiné à  obligatoire
      this.formBasic.controls['appartements'].updateValueAndValidity();//destiné à obligatoire
      this.formBasic.controls['bien'].clearValidators;//bien non obligatoire
      this.formBasic.controls['bien'].updateValueAndValidity();//bien non obligatoire
    } else {

      ////console.log("user prop /co-prop...")

      this.isInterne = false;
      this.listeappartementsbyuser();

      this.blocs = [];
      this.formBasic.controls['bien'].setValidators([Validators.required]);//bien obligatoire
      this.formBasic.controls['bien'].updateValueAndValidity();//bien obligatoire
      this.formBasic.controls['appartements'].clearValidators;//destiné à non obligatoire
      this.formBasic.controls['appartements'].updateValueAndValidity();//destiné à non obligatoire

    }

    ////console.log("buildFormBasic :",this.formBasic)
  }

  listCategorie(){


    this.appartementService.gettypesAppartement('CRC').subscribe((res:any)=>{
      if(res.statut){
        this.categories= res.data.map(clt => ({
          cbmarq: clt.cbmarq,
          label: clt?.label ,
        })) ;
      }
    })

  }

  listeappartements() {
    this.appartements=[];
    this.appartementService.getAppartements().subscribe((res: any) => {
          if (res.data) {
            ////console.log("residencec: ",res.data)
            this.appartements = res.data.map(clt => ({
              cbmarq: clt.cbmarq,
              label: clt?.residence?.intitule + (clt?.bloc!=undefined? ('-'+clt?.bloc?.intitule):'' ) + '-' + clt.intitule ,
              categorie: clt.categorie,
              residence: clt?.residence
            })) ;
          }
        },
        error => {
          //console.log('erreur: ', error);
        }
    );
  }

  listeappartementsbyuser() {

    this.appartementsProp=[];

    this.appartementService.getAppartementsProp(this.id_current_user).subscribe((res: any) => {
          if (res.data) {
            this.appartementsProp = res.data.map(clt => ({
              cbmarq: clt.cbmarq,
              label: clt?.residence?.intitule + (clt?.bloc!=undefined? ('-'+clt?.bloc?.intitule):'' )+ '-' + clt.intitule ,
              categorie: clt.categorie,
              residence: clt?.residence
            })) ;
            ////console.log(' this.appartementsProp: ',  res.data);
          }
        },
        error => {
          //console.log('erreur: ', error);
        }
    );
  }

  //onChange bien
  changeemp(event) {
    if (event && event.cbmarq) {
      this.blocs = [];
      this.isTerrain=false;
      this.isAppartement=false;
      ////console.log("changeemp Bien: ",event);

      //check categorie terrain (il faut choisir une categorie unique)
      if(event.categorie.code=="E0065"){
        this.isTerrain=true;

      }
      if(event.categorie.code=="E0064"){
        this.isAppartement=true;

      }
      ////console.log("isTerrain: ",this.isTerrain," isApp: ",this.isAppartement)


      //
      if(this.isTerrain && !this.isAppartement){
        this.formBasic.get('emplacement').clearValidators();
        this.formBasic.controls['emplacement'].updateValueAndValidity();
        ////console.log("is terrain : ",this.formBasic)

      }
      // is appart

      if(!this.isTerrain && this.isAppartement){

          this.blocService.getBlocs(event?.residence?.cbmarq).subscribe((res: any) => {
                if (res.statut === true) {
                  this.blocs = res.data?.map(clt =>
                      ({value: clt.cbmarq, label: clt.residence.intitule + ' - ' + clt.intitule})
                  );

                  this.blocs = this.blocs.filter((thing, index) => {
                    const _thing = JSON.stringify(thing);
                    return index === this.blocs.findIndex(obj => {
                      return JSON.stringify(obj) === _thing;
                    });
                  });
                }
              },
              error => {
                //console.log( 'erreur: ', error);
              }
          );
      }
      //is terrain
      if(this.isTerrain && !this.isAppartement){
        
        this.residenceService.getResidence(event?.residence?.cbmarq).subscribe( (res: any) => {

          if(res.statut){

              //don't push data if exist
                if (! this.terrains.some((item) => item?.residence?.cbmarq == res.data?.residence?.cbmarq)) {
                  this.terrains.push(res.data);
                }

          }

        },
        error => {
          //console.log( 'erreur: ', error);
        },()=>{
         // //console.log("totalTerrains: ",this.terrains);

          this.blocs=this.terrains?.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));

            }
        );

    }


    }

  }

  //onChange destiné à
  changeempDest(event){

    ////console.log("event: ",event)
    if (event) {
      this.blocs = [];
      this.terrains=[];
      this.isTerrain=false;
      this.isAppartement=false;

     //check categorie terrain (il faut choisir une categorie unique)
      event.forEach(element => {
       // //console.log("element: ",element)
        if(element.categorie.code=="E0065"){
          this.isTerrain=true;


        }
        if(element.categorie.code=="E0064"){
          this.isAppartement=true;

        }

      });

      ////console.log("isTerrain: ",this.isTerrain," isApp: ",this.isAppartement)
      /**** Cas Terrain *****/
      if(this.isTerrain && !this.isAppartement){
        ////console.log("innn is isTerrain")

        this.formBasic.get('emplacement').clearValidators();
        this.formBasic.controls['emplacement'].updateValueAndValidity();
        ////console.log("is terrain : ",this.formBasic)





        //set blocs in some data of destiné à
        for(let dest of event){

          ////console.log("dest: ",dest)
          ////console.log("this.terrains: ",this.blocs)
          //check if dest id exist in list of terrain

                    this.residenceService.getResidence(dest?.residence?.cbmarq).subscribe( (res: any) => {

                      if(res.statut){
                            ////console.log("get  terrain: ",res.data)
                            //don't push data if exist
                            if (! this.terrains.some((item) => item?.residence?.cbmarq == res.data?.residence?.cbmarq)) {
                              this.terrains.push(res.data);
                            }

                      }

                    },
                    error => {
                      //console.log( 'erreur: ', error);
                    },()=>{
                     // //console.log("totalTerrains: ",this.terrains);

                      this.blocs=this.terrains?.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
                      ////console.log("totalBlocs: ",this.blocs);



                        }
                    );




          //


        }


      }
      if(!this.isTerrain && this.isAppartement){

        ////console.log("innn is appart")

        this.formBasic.controls["emplacement"].setValidators(Validators.required);
        this.formBasic.controls['emplacement'].updateValueAndValidity();

        for(let dest of event){
          this.blocService.getBlocs(dest?.residence?.cbmarq).subscribe((res: any) => {

                if (res.statut) {
                  this.blocs=this.blocs.concat( res.data?.map(clt => ({ value: clt.cbmarq, label: clt.residence.intitule + ' - ' + clt.intitule }))  );
                  this.blocs = this.blocs.filter((thing, index) => {
                    const _thing = JSON.stringify(thing);
                    return index === this.blocs.findIndex(obj => {
                      return JSON.stringify(obj) === _thing;
                    });
                  });
                }

              },
              error => {
                //console.log( 'erreur: ', error);
              }
          );

        }



      }
      if(this.isTerrain && this.isAppartement){
        //this.toastr.error('Veuillez choisir une catégorie de bien unique!');

      }


    }
  }


  listeReclamations() {
    this.reclamationService.getReclamations().subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
        ////console.log("list reclamations: ",this.dataSource.data)


      }
      else {
        this.spinnerservice.hide()
      }
        },
        error => {
          //console.log( 'erreur:' , error);
        },() => {
          this.spinnerservice.hide();
        }
    );
  }
  listebloc() {
    this.blocs = [];

    this.blocService.getBlocs(1).subscribe((res: any) => {
          this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.residence.intitule + ' - ' + clt.intitule }));
        },
        error => {
          console.log( 'erreur: ', error);
        }
    );
  }
  cancelCmp(cbmarq: any) {

    this.reclamationService.deleteReclamation(cbmarq).subscribe(
        (res: any) => {
          if (res.statut === true) {
            this.toastr.success('Réclamation supprimer avec succès !');
            this.listeReclamations();
          } else {
            this.toastr.error(res.message, 'Erreur!', {progressBar: true});
          }
        },
        error => {
          this.toastr.error('Veuillez réessayer plus tard!');
        }
    );
  }
  confirmBox(row: any) {


    if(row?.type == 2){
      Swal.fire({
        title: 'Supprimer une réclamation',
        text: 'Voulez-vous vraiment supprimer cette réclamation ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmer',
        cancelButtonText: 'Annuler'
      }).then((result) => {
        if (result.value) {
          this.cancelCmp(row?.cbmarq);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });
    }
   
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onFileChange(files: FileList) {
    Array.from(files).forEach(file => {
      let reader = new FileReader();
      this.fileToUpload.push(file);
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imgURL.push(reader.result);
      };
    });

  }
  removeImage(img) {
    const index: number = this.imgURL.indexOf(img);
    if (index !== -1) {
      this.imgURL.splice(index, 1);
    }
    if (index !== -1) {
      this.fileToUpload.splice(index, 1);
    }
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' ,   windowClass: 'my-classModal' })
        .result.then((result) => {
      ////console.log(result);
    }, (reason) => {
      //console.log('Err!', reason);
    });
  }
  onclose() {
    this.transforme = false;
    this.submitted=false;
    this.rec = 0;
    this.imgURL = [];
    this.formBasic.reset();
    this.formBasic.patchValue({
      priorite: 'Moyenne'

    })
    this.modalService.dismissAll();
  }

  submit() {

    ////console.log("1 inn submit...",this.formBasic)
    this.formBasic.value.attachements = [];

    if ( this.can_add?.permission) {
      this.formBasic.value.attachements = this.fileToUpload;
      if (this.formBasic.invalid || (this.isTerrain && this.isAppartement)) {
        this.submitted = true;
        return;
      }

      let terrain:number =this.isTerrain?1:2;

     // //console.log("verif terrain /app : ",this.isTerrain, " - ",this.isAppartement)
      //if(this.isTerrain && !this.isAppartement){
        this.formBasic.patchValue({
          terrain:terrain
        })
      //}

      //console.log(" form: ",this.formBasic.value)



      const myFormData = new FormData();
      myFormData.append('user', this.formBasic.value.user);
      myFormData.append('bien', this.formBasic.value.bien);
      myFormData.append('emplacement', this.formBasic.value.emplacement);
      myFormData.append('objet', this.formBasic.value.objet);
      myFormData.append('description', this.formBasic.value.description);
      myFormData.append('priorite', this.formBasic.value.priorite);
      myFormData.append('appartements', this.formBasic.value.appartements);
      myFormData.append('categorie', this.formBasic.value.categorie);
      myFormData.append('terrain',this.formBasic.value.terrain)

      this.fileToUpload.forEach((file) => { myFormData.append('attachements[]', file); });

      ////console.log("myuFormData: ",myFormData)


      this.reclamationService.postReclamation(myFormData, this.rec ).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
              this.toastr.success('Réclamation ajouté avec succès .', 'Success!', {progressBar: true});
              this.submitted = false;
              ////console.log( res, res.data);
              this.formBasic.reset();
              this.imgURL = [];
              this.listeReclamations();
              this.onclose();
              // this.router.navigate([ '/communite/reclamations']);
            } else {
              this.submitted = true;
              this.toastr.error(res.message, 'Erreur!', {progressBar: true});
            }
          },
          (error) => {
            this.toastr.error('Erreur lors d\'ajout d\'une réclamation . Veuillez réessayer !', 'Erreur!', {progressBar: true});
          });
    } else {
      this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
    }

  }

  transformer(row, modalLong) {
    if (row?.type === 1) {
      this.spinnerservice.show()
      this.transforme = true;
      this.rec = row.cbmarq;
      this.formBasic.patchValue(row);
      this.formBasic.patchValue({emplacement: row?.emplacement?.cbmarq});
      this.formBasic.patchValue({categorie: row?.categorie?.cbmarq});

      this.open(modalLong);
    }
    this.spinnerservice.hide();
  }

  }
