import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CommuniteRoutingModule} from './communite-routing.module';
import { ListReunionComponent } from './reunion/list-reunion/list-reunion.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';

import {MatInputModule} from '@angular/material/input';
import { AddReunionComponent } from './reunion/add-reunion/add-reunion.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgxMatDatetimePickerModule , NgxMatTimepickerModule , NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import {MatCardModule} from '@angular/material/card';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FieldsetComponent} from '../fieldset/fieldset.component';
import {AppModule} from '../../app.module';
import { DetailReunionComponent } from './reunion/detail-reunion/detail-reunion.component';
import { ListReclamationComponent } from './reclamation/list-reclamation/list-reclamation.component';
import { DetailReclamationComponent } from './reclamation/detail-reclamation/detail-reclamation.component';
import { NgImageSliderModule } from 'ng-image-slider';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { ChatModule } from '@progress/kendo-angular-conversational-ui';
import { RoomReunionComponent } from './reunion/room-reunion/room-reunion.component';
import { ListDocumentComponent } from './document/list-document/list-document.component';
import { MobileroomReunionComponent } from './reunion/room-reunion/mobileroom-reunion/mobileroom-reunion.component';
// import {MatButtonToggleModule} from '@angular/material/button-toggle';
// import {MatGridListModule} from '@angular/material/grid-list';
// import {MatListModule} from '@angular/material/list';
// import {MatMenuModule} from '@angular/material/menu';
// import {MatIconModule} from '@angular/material/icon';
// import {MatTreeModule} from '@angular/material/tree';
// import {CdkTableModule} from '@angular/cdk/table';
// import {CdkTreeModule} from '@angular/cdk/tree';
import {TreeModule} from 'primeng/tree';
import { InfoDocumentComponent } from './document/info-document/info-document.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { InboxModule } from '../inbox/inbox.module';
import { ListPreventifComponent } from './preventif/list-preventif/list-preventif.component';
import { InfoPreventifComponent } from './preventif/info-preventif/info-preventif.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { CustomPaginator } from '../customPaginator';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatBottomSheetModule} from '@angular/material/bottom-sheet';


@NgModule({
  declarations: [ListReunionComponent, AddReunionComponent, DetailReunionComponent, ListReclamationComponent, DetailReclamationComponent, RoomReunionComponent,
     ListDocumentComponent, InfoDocumentComponent, MobileroomReunionComponent ,
      ListPreventifComponent, InfoPreventifComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatTableModule,
        MatPaginatorModule,
        CommuniteRoutingModule,
        MatInputModule,
        ReactiveFormsModule,
        NgSelectModule,
        FormsModule,
        NgxMatDatetimePickerModule,
        MatDatepickerModule,
        NgxMatTimepickerModule,
        NgxMatMomentModule,
        MatCardModule,
        NgbModule,
        NgImageSliderModule,
        PerfectScrollbarModule,
        ChatModule,
        TreeModule,
        MatGridListModule,
        InboxModule,
        NgxGalleryModule,
        NgxSpinnerModule,
        MatBottomSheetModule,
        // AppModule,

        // MatButtonToggleModule,
        // MatGridListModule,
        // MatListModule,
        // MatMenuModule,
        // MatIconModule,
        // MatTreeModule,

        // CdkTableModule,
        // CdkTreeModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        { provide: MatPaginatorIntl, useValue: CustomPaginator() },  // Here
        {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    ],
})
export class CommuniteModule { }
