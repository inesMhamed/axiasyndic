import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailReunionComponent } from './detail-reunion.component';

describe('DetailReunionComponent', () => {
  let component: DetailReunionComponent;
  let fixture: ComponentFixture<DetailReunionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailReunionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailReunionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
