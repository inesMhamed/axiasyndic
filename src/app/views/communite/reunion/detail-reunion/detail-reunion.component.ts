import { Component, ElementRef, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ReunionService } from '../../../../shared/services/reunion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Reunion } from '../../../../shared/models/reunion.model';
import { SharedAnimations } from '../../../../shared/animations/shared-animations';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { BlocService } from '../../../../shared/services/bloc.service';
import { AppartementService } from '../../../../shared/services/appartement.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { interval, Subscription } from 'rxjs';
import { DatePipe, formatDate } from '@angular/common';
import * as pdfmake from 'pdfmake/build/pdfmake';
import pdfMake from 'pdfmake/build/pdfmake';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import pdfFonts from 'pdfmake/build/vfs_fonts';

import { ParametreService } from 'src/app/shared/services/parametre.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { el } from 'date-fns/locale';
import { NgxSpinnerService } from 'ngx-spinner';
import { date } from 'ngx-custom-validators/src/app/date/validator';

@Component({
    selector: 'app-detail-reunion',
    templateUrl: './detail-reunion.component.html',
    styleUrls: ['./detail-reunion.component.scss'],
    providers: [DatePipe],
    animations: [SharedAnimations]
})
export class DetailReunionComponent implements OnInit {
    attachements: any[] = [];
    @ViewChild('labelImport')
    labelImport: ElementRef;
    fileToUpload: File = null;
    intitule_doc: any;
    @ViewChild("namedoc") namedoc: ElementRef;
    notevote: any
    id: any;
    role: any;
    create_room: any = false;
    reunion = new Reunion();
    formBasic: FormGroup;
    formterminer: FormGroup
    formrapportReunion: FormGroup;
    residences: any[];
    blocs: any[];
    appartement: any[];

    listParticipants: any = []
    submitted: boolean;
    date_now = new Date();
    acceptedAllInvitation: boolean = false;
    // @ViewChild('modalLongReunion') modalLongReunion: TemplateRef<any>;
    private subscription: Subscription;
    moduleLoading: boolean;

    public dateNow = new Date();
    public dDay = new Date();
    milliSecondsInASecond = 1000;
    hoursInADay = 24;
    minutesInAnHour = 60;
    SecondsInAMinute = 60;

    public timeDifference;
    public secondsToDday = 0;
    public minutesToDday = 0;
    public hoursToDday = 0;
    public daysToDday = 0;
    id_current_user: any;
    listInvitation: any[] = [];
    isReunionExpire: boolean = false;
    datedujour: any
    dateDebut: any;
    dateFin: any;
    details: any;
    societe: any = [];
    societeObj: any = [];
    typereunion: string;
    participantspresents: any = [];
    participantsnonpresents: any = [];
    objannuler: any
    listnotegenerale: any = []
    today: any;
    constructor(
        private reunionService: ReunionService,
        private actRoute: ActivatedRoute,
        private paramSociete: ParametreService,
        private datepipe: DatePipe,
        private toastr: ToastrService,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private route: Router,
        private spinnerservice: NgxSpinnerService,
        private userService: UtilisateurService,
        private dl: ResidenceService,
        private blocService: BlocService,
        private appartementService: AppartementService,
    ) { }

    ngOnInit(): void {
        this.spinnerservice.show()
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + 1);
        this.today = currentDate;
        // this.date_now = this.datepipe.transform(new Date(), 'yyyy-MM-dd')
        this.datedujour = this.datepipe.transform(new Date(), 'yyyy-MM-dd')
        this.id_current_user = localStorage.getItem('id');
        this.getDetailsUser(this.id_current_user)
        this.role = localStorage.getItem('role');
        if (this.role.indexOf('ROLE_INTERN') !== -1) {
            this.create_room = false;

        }
        this.formterminer = this.fb.group({
            conclusion: new FormControl("", Validators.required),
        })
        this.submitted = false;
        this.buildFormBasic();
        this.listeresidence();
        this.formrapportReunion = this.fb.group({
            dateDebut: new FormControl(new Date(), Validators.required),
            raisonreport: new FormControl("", Validators.required),
        });
        this.actRoute.paramMap.subscribe(params => {
            this.id = params.get('cbmarq');
        });
        if (this.id) {
            this.getReunion();
            this.getInvitationByReunionIdAndUserId();
            this.participantspointes(this.id)

        }
    }
    console(ev){
        console.log('aaaa====>',ev)
    }
    getDetailsUser(id) {
        this.userService.getuserbyId(id).subscribe((admin: any) => {
            this.details = admin.data
            this.paramSociete.getInfoSociete(this.details.societe).subscribe((res: any) => {
                if (res.statut) {
                    this.societe = res.data
                    this.societeObj = {
                        adresse: res.data.adresse,
                        banque: res.data?.banque,
                        cbmarq: res.data?.cbmarq,
                        code: res.data?.code,
                        description: res.data?.description,
                        devise: res.data?.devise,
                        duplicate: res.data?.duplicate,
                        email: res.data?.email,
                        intitule: res.data?.intitule,
                        logo: res.data?.logo,
                        logobase64: res.data?.logobase64,
                        matriculeFiscale: res.data?.matriculeFiscale,
                        round: res.data?.round,
                        telephone: res.data?.telephone,
                    }
                }
            })
        })
    }
    submitrapportrer() {
        this.spinnerservice.show()
        if (this.formrapportReunion.invalid) {
            this.submitted = true;
            this.spinnerservice.hide()
            return;
        }
        this.formrapportReunion.value.dateDebut = this.datepipe.transform(new Date(this.formrapportReunion.value.dateDebut), 'yyyy-MM-dd H:mm');
        this.reunionService.reportReunion(this.id, this.formrapportReunion.value).subscribe((res: any) => {
            if (res.statut == false) {
                this.toastr.error(res.message)
                this.spinnerservice.hide()
            }
            else {
                this.toastr.success(res.message)
                this.formrapportReunion.reset()
                this.spinnerservice.hide()
            }
        })
    }
    getReunion() {
        this.reunionService.getReunion(this.id)
            .subscribe((x: any) => {
                if (x.data) {
                    this.reunion = x.data;
                    console.log('reunion', this.reunion);
                    if (this.reunion?.type?.code == "E0025")
                        this.typereunion = 'Extraordinaire'
                    if (this.reunion?.type?.code == "E0024")
                        this.typereunion = 'Ordinaire'
                    if (this.reunion?.type?.code == "E0091")
                        this.typereunion = 'Délégué'
                    if (x?.data?.biens) {
                        Object(x?.data?.biens).forEach(name => {
                            if (name?.appartement !== null && name.bloc !== null && name.residence !== null) {
                                this.listParticipants.push(name.residence?.intitule + '|' + name.bloc?.intitule + '|' + name.appartement?.intitule)
                            }
                            if (name.bloc !== null && name.appartement === null && name.residence !== null)
                                this.listParticipants.push(name.residence?.intitule + '|' + name.bloc?.intitule)
                            if (name.appartement === null && name.residence !== null && name.bloc === null)
                                this.listParticipants.push(name.residence?.intitule)
                            if (name.groupement !== null && name.residence === null)
                                this.listParticipants.push(name.groupement?.intitule)
                            this.spinnerservice.hide()

                        })
                    }
                    this.datedujour = formatDate(new Date(), 'yyyy-MM-dd HH:mm', 'fr-FR');
                    this.dateDebut = formatDate(this.reunion.dateDebut2.date, 'yyyy-MM-dd HH:mm', 'fr-FR');
                    this.dateFin = formatDate(this.reunion.dateFin2.date, 'yyyy-MM-dd HH:mm', 'fr-FR');
                    if (this.returnDifferenceTime(this.reunion?.dateDebut, this.reunion?.dateFin) <= 0) {
                        this.toastr.error("Réunion Expiré !", "Information")
                        this.isReunionExpire = true;
                        this.spinnerservice.hide()
                    } else {

                        this.spinnerservice.hide()
                        this.subscription = interval(1000)
                            .subscribe(z => {
                                this.getTimeDifference(this.reunion?.dateDebut, this.reunion?.dateFin);
                            });
                    }

                }
            });
    }
    convert(url) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.responseType = "blob";
        xhr.onload = function (e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                var res = event.target.result;
            }
            var file = this.response;
            reader.readAsDataURL(file)
        };
        xhr.send()
    }
    toDataURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }
    GenererPV() {
        let listparticip = this.listParticipants.map((ell: any, i: any) => {
            if (i !== this.listParticipants.length - 1)
                return ell + ', '
            else
                return ell
        })

        let ordrejour = this.reunion?.notes.filter(ss => ss.type.code == 'E0027').map(el => {
            return [
                { text: el?.description, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'left' },
                { text: el?.v_oui, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'center' },
                { text: el?.v_non, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'center' },
                { text: el?.v_neutre, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'center' },
            ]
        })
        let notegenerale = this.reunion?.notes.filter(ss => ss.type.code === 'E0026')
        if (notegenerale.length !== 0) {
            this.listnotegenerale = notegenerale.map(el => {
                return { text: '- ' + el?.description + '\n', margin: [0, 2, 0, 2] }
            })
        } else {
            this.listnotegenerale.push({ text: '- Aucune note générale saisie.\n', margin: [0, 2, 0, 2] })
        }

        let presents = this.participantspresents.map((ell: any) => {
            if (ell?.user) {
                return [
                    { text: ell?.user?.nomComplet, fontSize: 10, margin: [5, 0, 0, 0] },
                ]
            }
            if (ell?.email) {
                return [
                    { text: ell?.email, fontSize: 10, margin: [5, 0, 0, 0] },
                ]
            }
        })
        // console.log(presents)
        var perChunk = 4 // items per chunk    
        //diviser prensent en 4
        if (presents.length > 0) {
            var resultpresent = presents.reduce((resultArray, item, index) => {
                const chunkIndex = Math.floor(index / perChunk)
                if (!resultArray[chunkIndex]) {
                    resultArray[chunkIndex] = [] // start a new chunk
                }
                resultArray[chunkIndex].push(item[0])
                return resultArray
            }, [])
            for (let k of resultpresent) {
                while (k.length < 4) {
                    k.push({ text: '', fontSize: 10, margin: [5, 0, 0, 0] })
                }
            }
        } else {
            var resultpresent: any = [[{ text: 'Aucun présent.', fontSize: 10, margin: [5, 0, 0, 0] }]]
        }
        let absents = this.participantsnonpresents.map((ell: any, i: any) => {
            if (ell?.user) {


                return [
                    { text: ell?.user?.nomComplet, fontSize: 10, lineHeight: '1.2', margin: [5, 0, 0, 0] },
                ]
            }
            if (ell?.email) {
                return [
                    { text: ell?.email, fontSize: 10, lineHeight: '1.2', margin: [5, 0, 0, 0] },
                ]
            }
        })
        //diviser absent en 4
        if (absents.length > 0) {
            var resultabsent = absents.reduce((resultArray, item, index) => {

                const chunkIndex = Math.floor(index / perChunk)
                if (!resultArray[chunkIndex]) {
                    resultArray[chunkIndex] = [] // start a new chunk
                }
                resultArray[chunkIndex].push(item[0])
                return resultArray
            }, [])
            for (let k of resultabsent) {
                while (k.length < 4) {
                    k.push({ text: '', fontSize: 10, margin: [5, 0, 0, 0] })
                }
            }

        } else {
            var resultabsent: any = [[{ text: 'Aucun absent !', fontSize: 10, margin: [5, 0, 0, 0] }]]
        }
        let conclusion: any
        if (this.reunion?.conclusion == null) {
            conclusion = 'Aucune conclusion saisie.'
        } else {
            conclusion = this.reunion?.conclusion
        }
        var docDefinition = {
            footer: {
                margin: [40, 12, 40, 0],
                height: 20,
                table: {
                    alignment: 'justify',
                    italics: true,
                    widths: ['20%', '80%'],
                    heights: [10.25],
                    body: [[
                        { text: 'Tel: ' + this.societeObj?.telephone, margin: [5, 0, 0, 0], fontSize: 9, style: 'filledHeader' },
                        { text: 'Adresse: ' + this.societeObj?.adresse + '', alignment: 'right', fontSize: 9, margin: [0, 0, 5, 0], style: 'filledHeader' },

                    ]]

                },
                layout: 'noBorders'
            },
            info: {
                title: 'Reunion ' + this.typereunion,
                author: 'axiasyndic',
                subject: 'axiasyndic',
                keywords: 'axiasyndic',
            },
            content: [
                {
                    table: {

                        widths: ['auto', '*'],
                        body: [
                            [{
                                image: this.societeObj?.logobase64, margin: [0, 3, 0, 3],
                                fit: [50, 50]
                            },
                            {
                                margin: [0, 10, 0, 2],
                                text: [
                                    { text: 'Réunion  ' + this.typereunion, bold: true, fontSize: '18', color: '#102758' },
                                    { text: '\nLe  ' + this.datepipe.transform(new Date(), 'd MMMM  y') },
                                ]
                            },
                            ],
                        ]
                    },
                    layout: 'noBorders'
                },
                {
                    table: {
                        widths: ['30%', '70%'],
                        heights: [12],
                        body: [[
                            { text: 'Début: ' + this.datepipe.transform(this.reunion?.dateDebut2?.date, 'HH:mm'), fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
                            { text: 'Fin: ' + this.datepipe.transform(this.reunion?.dateFin2?.date, 'HH:mm'), fontSize: 10, bold: true, margin: [0, 0, 5, 0], style: 'filledHeader', alignment: 'right' },

                        ]]
                    },
                    layout: 'noBorders'
                },
                { text: '', margin: [0, 5, 0, 5] },
                //Infos Générales
                {
                    table: {

                        widths: ['17%', '83%'],
                        body: [
                            [
                                { text: 'Objet: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                                { text: this.reunion.objet, margin: [3, 5, 0, 5], style: 'headertable' },
                            ],
                            [
                                { text: 'Présenteur: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                                { text: this.reunion.cbcreateur?.nomComplet, margin: [3, 5, 0, 5], style: 'headertable' },
                            ],
                            [
                                { text: 'Société: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                                { text: this.societeObj?.intitule, margin: [3, 5, 0, 5], style: 'headertable' },
                            ],
                            [
                                { text: 'Lieu: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                                { text: this.reunion.emplacement, margin: [3, 5, 0, 5], style: 'headertable' },
                            ],
                            [
                                { text: 'Description: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                                { text: this.reunion.description, lineHeight: '1.2', margin: [3, 5, 0, 5], style: 'headertable' },
                            ],
                            [
                                { text: 'Particpants: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                                { text: listparticip, margin: [3, 5, 0, 5], style: 'headertable' },
                            ],

                        ],
                    },
                    layout: {
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? '#edeff8' : '#edeff8';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? '#edeff8' : '#edeff8';
                        },
                    }
                },
                {
                    text: '', margin: [0, 5, 0, 5]
                },
                //participants
                {
                    table: {
                        widths: ['100%'],
                        heights: [12],
                        body: [[
                            { text: 'Participants ', fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
                        ]]
                    },
                    layout: 'noBorders'
                },//Présents
                { text: 'Présents: \n', bold: true, style: 'header', margin: [3, 5, 0, 3] },
                {
                    table: {
                        widths: ['25%', '25%', '25%', '25%'],
                        body: [].concat(resultpresent)
                    },
                    layout: 'noBorders'
                },
                //Absents
                { text: 'Absents et excusés: \n', bold: true, style: 'header', margin: [3, 3, 0, 0] },
                {
                    table: {
                        widths: ['25%', '25%', '25%', '25%'],
                        fontSize: 10,
                        body: [].concat(resultabsent)
                    },
                    layout: 'noBorders'
                },
                { text: '', margin: [0, 3, 0, 3] },
                //Point à discuter
                {
                    table: {

                        widths: ['100%'],
                        heights: [12],
                        body: [[
                            { text: 'Point à discuter et Vote', fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
                        ]]
                    },
                    layout: 'noBorders'
                },
                { text: 'Note Générale:\n', style: 'header', bold: true, margin: [5, 5, 0, 3], alignment: 'left' },
                { text: this.listnotegenerale, style: 'headertable', margin: [5, 0, 0, 5], alignment: 'left', fontSize: 10 },
                { text: 'Ordre du jour:\n', style: 'header', bold: true, margin: [5, 0, 0, 5], alignment: 'left' },
                {
                    table: {
                        widths: ['70%', '10%', '10%', '10%'],
                        body: [
                            [
                                {
                                    text: 'Vote ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header', alignment: 'left'
                                },
                                { text: 'Oui', bold: true, margin: [3, 5, 0, 5], fillColor: '#f5f6fa', style: 'header', alignment: 'center' },
                                { text: 'Non', bold: true, margin: [3, 5, 0, 5], fillColor: '#f5f6fa', style: 'header', alignment: 'center' },
                                { text: 'Neutre', bold: true, margin: [3, 5, 0, 5], fillColor: '#f5f6fa', style: 'header', alignment: 'center' },
                            ],
                        ].concat(ordrejour),
                    },
                    layout: {
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? '#edeff8' : '#edeff8';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? '#edeff8' : '#edeff8';
                        },
                    }
                },
                { text: '', margin: [0, 5, 0, 5] },
                //Conclusion
                {
                    table: {
                        widths: ['100%'],

                        heights: [12],
                        body: [[
                            { text: 'Conclusion', fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
                        ]]
                    },
                    layout: 'noBorders'
                },
                { text: conclusion, lineHeight: '1.2', style: 'headertable', fontSize: 10, margin: [5, 5, 5, 5], alignment: 'left' },

            ],
            styles: {
                date: {
                    alignment: 'right',
                    fontSize: '10',
                    color: '#102758',
                    fillColor: '#f5f6fa',
                    margin: [0, 20, 5, 0]
                },
                table: {
                    fontSize: '10',
                    color: 'black',
                    alignment: 'left',
                },
                filll: {
                    fontSize: '10',
                    color: '#102758',
                    fillColor: '#f5f6fa',
                    margin: [0, 0, 5, 0]
                },
                filledHeader: {
                    borderColor: '#213C7F ',
                    fontSize: '10',
                    color: 'white',
                    fillColor: '#213C7F ',
                },

                headertable: {
                    color: 'black',
                    fontSize: '10',

                },
                header: {
                    color: '#102758',
                    fontSize: '10',

                },
                total: {
                    color: 'black',
                    fontSize: '10',
                    alignment: 'right',
                    italics: true,
                    margin: [5, 0, 5, 0],

                },
                totals: {
                    color: '#102758',
                    bold: true,
                    fontSize: '10',
                    margin: [0, 0, 5, 0],
                    alignment: 'right',
                    italics: true,

                },

                pageSize: 'A4',
                pageOrientation: 'portrait'

            },
        };
        pdfmake.createPdf(docDefinition).open();
    }
    participantspointes(cbmarq) {
        this.reunionService.getpresnecespointage(cbmarq).subscribe((res: any) => {
            if (res.data.pointage) { this.participantspresents = res.data.pointage }
            if (res.data.dépointage) { this.participantsnonpresents = res.data.dépointage }
        })
    }

    getInvitationByReunionIdAndUserId() {
        this.acceptedAllInvitation = false;
        this.reunionService.getInvitationByReunionAndUserId(this.id).subscribe((res: any) => {
            if (res.statut) {
                this.listInvitation = res.data;
                //tant que all invitation on le meme statut donc on peut prendre l'une des invitation
                if (this.listInvitation[0].presence == 1) {
                    this.acceptedAllInvitation = true;
                }
            }
        })
    }
    private getTimeDifference(datedebut, datefin) {
        this.dDay = new Date(datedebut);
        this.timeDifference = this.dDay.getTime() - new Date().getTime();
        if (this.timeDifference > 0) {
            this.allocateTimeUnits(this.timeDifference);
        } else {
            this.dDay = new Date(datefin);
            this.timeDifference = this.dDay.getTime() - new Date().getTime();
            if (this.timeDifference > 0) {
                this.allocateTimeUnits(this.timeDifference);
            }
        }
    }
    joindreReunion(cbmarq, mode) {
        console.log('id=', cbmarq)
        if (mode == 2) {
            this.reunionService.joindreReunion(cbmarq).subscribe((res: any) => {
                if (res.statut === true) {
                    this.toastr.success(res.message, 'Success!', { progressBar: true });
                    this.getReunion()
                    this.route.navigate(['/communite/room-reunion/', this.reunion?.cbmarq])

                } else {
                    this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                }
            })
        }
        else {
            this.route.navigate(['/communite/room-reunion/', this.reunion?.cbmarq])
        }
    }
    returnDifferenceTime(datedebut, datefin) {
        let dDay = new Date(datedebut);
        let timeDifference = dDay.getTime() - new Date().getTime();
        if (timeDifference > 0) {
            this.allocateTimeUnits(timeDifference);
        } else {
            dDay = new Date(datefin);
            timeDifference = dDay.getTime() - new Date().getTime();
            if (timeDifference > 0) {
                this.allocateTimeUnits(timeDifference);
            }
        }
        return timeDifference;
    }

    clotureReunion(id) {
        this.reunionService.cloturerReunion(id, this.formterminer.value).subscribe((res: any) => {
            if (res.statut === true) {
                this.toastr.success(res.message, 'Success!', { progressBar: true });
                this.getReunion()
            } else {
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
        })
    }
    openmodalterminer(cbmarq, content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
            .result.then((result) => {
            }, (reason) => {
            });
    }
    submitterminer() {
        if (this.formterminer.invalid) {
            this.submitted = true;
            return;
        }
        this.reunionService.cloturerReunion(this.id, this.formterminer.value).subscribe((res: any) => {
            if (res.statut === true) {
                this.toastr.success(res.message, 'Success!', { progressBar: true });
                this.getReunion()
                this.formterminer.reset()
                this.modalService.dismissAll();
            } else {
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
        })
    }
    private allocateTimeUnits(timeDifference) {
        this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
        this.minutesToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
        this.hoursToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute) % this.hoursInADay);
        this.daysToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute * this.hoursInADay));
    }

    OnDestroy() {
        this.subscription.unsubscribe();
    }
    sendInvitation(cbmarq) {
        this.spinnerservice.show()
        this.reunionService.sendInvitation(cbmarq).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
                this.toastr.success('Invitations ont été envoyées avec succès !!', 'Success!', { progressBar: true });
                this.getReunion()
            } else {
                this.spinnerservice.hide()
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
        },
            (error) => {
                this.spinnerservice.hide()
                this.toastr.error(' Veuillez réessayer !', 'Erreur!', { progressBar: true });
            });
    }

    confirmerDeleteAttachement(cbmarq: any) {
        Swal.fire({
            title: 'Supprimer un document',
            text: 'Voulez-vous vraiment supprimer ce document ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.value) {
                this.DeleteAttachement(cbmarq);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        });

    }

    DeleteAttachement(cbmarq: any) {
        this.reunionService.deleteAttachement(cbmarq).subscribe(
            (res: any) => {
                if (res.statut === true) {
                    this.getReunion()
                    this.toastr.success('attachement a été supprimé avec succès !', 'Success!', { progressBar: true });
                } else {
                    this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                }
            },
            error => {
                this.toastr.error('Veuillez réessayer plus tard!', 'Erreur!', { progressBar: true });
            }
        );
    }

    openAttachement(url: any) {
        window.open(url, '_blank');
    }

    getExstendsion(image) {
        if (image.endsWith('jpg') || image.endsWith('jpeg')) {
            return 'jpg';
        }
        if (image.endsWith('png')) {
            return 'png';
        }
    }

    /***************Invité autre appartement*********************/
    listeresidence() {
        this.dl.getResidences().subscribe((res: any) => {
            if (res.statut) {
                this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
            }

        },
            error => {
                //console.log( 'erreur: ', error);
            }
        );
    }
    OnchangeResidence(event) {
        this.blocs = [];
        this.formBasic.patchValue({ bloc: null });
        this.formBasic.patchValue({ appartement: null });
        if (event && event.value) {
            this.blocService.getBlocs(event.value).subscribe((res: any) => {
                if (res.data) {
                    this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
                }
            },
                error => {
                    //console.log( 'erreur: ', error);
                }
            );
        }
    }
    OnchangeBloc(event) {
        this.appartement = [];
        this.formBasic.patchValue({ appartement: null });
        if (event && event.value) {
            this.appartementService.getAppartementByBloc(event.value).subscribe((res: any) => {
                if (res.data) {
                    this.appartement = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
                }
            },
                error => {
                    //console.log( 'erreur: ', error);
                }
            );
        }
    }
    buildFormBasic() {
        this.formBasic = this.fb.group({
            residence: new FormControl(null, Validators.required),
            bloc: new FormControl(null, Validators.required),
            appartement: new FormControl(null, Validators.required),
        });
    }
    open(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
            .result.then((result) => {
                //console.log(result);
            }, (reason) => {
                //console.log('Err!', reason);
            });
    }
    openmodalreunion(cbmarq, content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
            .result.then((result) => {
                //console.log(result);
            }, (reason) => {
                //console.log('Err!', reason);
            });
    }
    onclose() {
        this.modalService.dismissAll();
        this.formrapportReunion.reset()
    }

    submit() {
        this.spinnerservice.show()
        if (this.formBasic.invalid) {
            this.spinnerservice.hide()
            this.submitted = true;
            return;
        }

        //console.log('end', this.formBasic.value);
        this.reunionService.addAutreInvitation(this.id, this.formBasic.value).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
                this.toastr.success('Invitation envoyer avec succè !', 'Success!', { progressBar: true });
                this.getReunion()
                this.submitted = false;
                this.modalService.dismissAll();
                this.formBasic.reset();
            } else {
                this.submitted = true;
                this.spinnerservice.hide()
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
        })

    }

    commencerreunion() {
        this.reunionService.startReunion(this.id).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
                this.getReunion()
                this.toastr.success(res.message, 'Success!', { progressBar: true });
                this.submitted = false;
            } else {
                this.submitted = true;
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
        })

    }
    AnnulerReunion() {

        Swal.fire({
            title: 'Veuillez saisir la raison pour annuler cette réunion !!',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                this.objannuler = { commentaire: login };
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinnerservice.show()
                this.reunionService.annulerReunion(this.id, this.objannuler).subscribe(
                    (res: any) => {
                        if (res.statut == false) {
                            this.spinnerservice.hide()
                            this.toastr.error(res.message, 'Erreur!', { progressBar: true })
                        }
                        else {
                            this.getReunion()
                            this.toastr.success(res.message, 'Succès!', { progressBar: true });
                        }
                    },
                    error => {
                        this.spinnerservice.hide()
                        this.toastr.error('Veuillez réessayer plus tard !', 'Erreur!', { progressBar: true })
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }
        })
    }

    confirmInvitationReunion(cbmarq, action) {
        let title = "";
        if (action == 1) {
            title = "Valider votre présence";
            this.presenceReunion(cbmarq, action, '')
        }
        if (action == 2) {
            title = "Annuler votre présence";


            Swal.fire({
                title: title,
                text: "Ecriver votre commentaire !",
                input: 'text',
                customClass: {
                    validationMessage: 'my-validation-message'
                },
                preConfirm: (value) => {

                    if (!value) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Votre commentaire est requis'
                        )
                    } else {
                        ////console.log("submit comment: " + value);


                        this.presenceReunion(cbmarq, action, value)
                    }
                }
            })
        }




    }

    presenceReunion(cbmarq, action, commentaire) {
        let msg: string = "";
        if (action == 1) {
            msg = "Vous avez accepté d\'assister la réunion !";
        } else {
            msg = "Vous avez refuser d\'assister la réunion !";
        }
        let modelPresence = {
            presence: action,
            commentaire: commentaire
        }
        this.reunionService.presenceReunion(cbmarq, modelPresence).subscribe((res: any) => {
            if (res.statut) {
                this.toastr.success(msg, 'Success!', { progressBar: true });
                this.getInvitationByReunionIdAndUserId();
                this.getReunion();

            }
        })
    }
    /*************************************WEBRTC MULTI-CONNECTION************************************/
    /*************************************WEBRTC MULTI-CONNECTION***********************************/
    onFileChange(files: FileList) {
        this.labelImport.nativeElement.innerText = Array.from(files)
            .map(f => f.name)
            .join(', ');
        this.fileToUpload = files.item(0);
    }
    blurEvent(event: any) {
        this.intitule_doc = event.target.value;
    }
    AddAttachement() {
        const myFormData = new FormData();
        myFormData.append('intitule', this.intitule_doc);
        myFormData.append('attachement', this.fileToUpload);

        if (parseInt(this.id) === 0) {
            this.toastr.error('Veuillez enregistrer les informations de la réunion au premier puis réessayer !', 'Erreur!', { progressBar: true });
            return;
        } else {
            if (this.intitule_doc != null && this.fileToUpload != null) {
                this.reunionService.postAttachementReunion(this.id, myFormData).pipe(first()).subscribe((res: any) => {
                    if (res.statut === true) {
                        this.attachements = res.data;
                        this.namedoc.nativeElement.value = "";
                        this.labelImport.nativeElement.innerText = "";
                        this.intitule_doc = null;
                        this.fileToUpload = null;
                        this.getReunion()
                        this.toastr.success('Attachement a été ajouté avec succès', 'Succès!', { progressBar: true });

                    } else {
                        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                    }
                },
                    (error) => {
                        this.toastr.error('Erreur lors de l\'ajout d\'un attachement . Veuillez réessayer !', 'Erreur!', { progressBar: true });
                    });
            } else {
                this.toastr.error('Veuillez remplir tous les champs', 'Erreur!', { progressBar: true });
            }
        }
    }
}
