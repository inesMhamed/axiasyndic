import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Reunion } from '../../../../shared/models/reunion.model';
import { FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatInput } from '@angular/material/input';
import { ReunionService } from '../../../../shared/services/reunion.service';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-reunion',
  templateUrl: './list-reunion.component.html',
  styleUrls: ['./list-reunion.component.scss']
})
export class ListReunionComponent implements OnInit {

  displayedColumns: string[] = ['type', 'objet', 'emplacement', 'dateDebut', 'dateFin', 'statut', 'action'];
  dataSource = new MatTableDataSource<Reunion>();
  pageEvent: PageEvent;
  reunions;
  loading: boolean;
  submitted: boolean;
  residences: any[];
  types: any[];
  appartements: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  @ViewChild('closeModal') private closeModal: ElementRef;
  @ViewChild('closedModal') private closedModal: ElementRef;
  data: any;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: any;

  have_access: boolean = false;

  constructor(
    private reunionService: ReunionService, private preferenceService: PreferencesService,
    private dl: ResidenceService,
    private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService,
    private permissionservice: DroitAccesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000091');
    this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000092');
    this.can_delete = this.permissionservice.search(this.id_current_user, 'FN21000093');
    this.can_detail = this.permissionservice.search(this.id_current_user, 'FN21000094');
    this.can_liste = this.permissionservice.search(this.id_current_user, 'FN21000095');
    this.checkAccess();
  }

  checkAccess() {
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000095').subscribe((res: any) => {
      this.have_access = res.data;

    }, error => { }, () => {

      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {
        this.listeresidence();
        this.listeReunions();
      }

    });
  }

  listeReunions() {
    this.spinnerservice.show()
    this.reunionService.getReunions().subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
      }
      else {
        this.spinnerservice.hide()
      }
    },
      error => {
        //console.log( 'erreur:' , error);
      }
    );
  }
  listeresidence() {
    this.dl.getResidences().subscribe((res: any) => {
      if (res.statut) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
      }

    },
      error => {
        //console.log( 'erreur: ', error);
      }
    );
  }
  cancelCmp(cbmarq: any) {
    this.spinnerservice.show()
    this.reunionService.deleteReunion(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Réunion a été supprimée avec succès !');
          this.listeReunions();
        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          this.spinnerservice.hide()

        }
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une réunion',
      text: 'Voulez-vous vraiment supprimer Cette réunion ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.cancelCmp(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
