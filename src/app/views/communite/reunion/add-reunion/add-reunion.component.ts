import { Component, ElementRef, Inject, LOCALE_ID, NgModule, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { CommuneService } from '../../../../shared/services/commune.service';
import { GroupementService } from '../../../../shared/services/groupement.service';
import { AppartementService } from '../../../../shared/services/appartement.service';
import { BlocService } from '../../../../shared/services/bloc.service';
import * as moment from 'moment';
// import Map from 'ol/Map';
import { MatNativeDateModule, MatRippleModule, ThemePalette } from '@angular/material/core';
import { first, isEmpty } from 'rxjs/operators';
import { ReunionService } from '../../../../shared/services/reunion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Reunion } from '../../../../shared/models/reunion.model';
import { Note } from '../../../../shared/models/note.model';
import { DatePipe } from '@angular/common';
import { DOCUMENT } from '@angular/common';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-add-reunion',
  templateUrl: './add-reunion.component.html',
  styleUrls: ['./add-reunion.component.scss'],
  providers: [DatePipe]
})

export class AddReunionComponent implements OnInit {

  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File = null;
  id: any = 0;
  formFinal: any;
  formBasic: FormGroup;
  formBiens: FormGroup;
  submitted: boolean;
  submitted2: boolean;
  readonly: boolean;
  types: any[];
  types_note: any[];
  groupements: any[];
  residences: any[];
  appartements: any[];
  blocs: any[];
  selected: any[] = null;
  internes: any[];
  grp: any[];
  resi: any[];
  blc: any[];
  apprt: any[];
  interne: any[];
  test: any;
  invitation: any;
  date_now = new Date();
  attachements: any[] = [];
  intitule_doc: any;
  closed: boolean;
  public dateControl = new FormControl(new Date(2021, 9, 4, 5, 6));
  public color: ThemePalette = 'primary';
  @ViewChild("namedoc") namedoc: ElementRef;
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;

  have_access: boolean = false;
  can_delete: Permission;
  datedujour: any;
  isDisabled: boolean = false;


  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private residenceService: ResidenceService,
    private communeService: CommuneService,
    private groupementService: GroupementService,
    private appartementService: AppartementService,
    private blocService: BlocService,
    private reunionService: ReunionService,
    private actRoute: ActivatedRoute,
    private spinnerservice: NgxSpinnerService,
    private router: Router,
    private datepipe: DatePipe,
    @Inject(DOCUMENT) documen,
    private permissionservice: DroitAccesService
  ) { }

  ngOnInit() {

    this.datedujour = this.datepipe.transform(new Date(), 'dd/MM/yyy , HH:mm')
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000091');
    this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000092');
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
    });

    this.can_delete = this.permissionservice.search(this.id_current_user, 'FN21000093');

    this.submitted = false;
    this.submitted2 = false;
    this.readonly = false;
    this.closed = false;
    this.buildFormBasic();
    this.buildFormBien();

    this.checkAddEdit();


    this.listetypes();
    this.gettypesNotes();
    this.listegroupement();
    this.listeresidence();
    this.listeinternes();

    if (parseInt(this.id) !== 0) {
      this.getRéunionOnInit();
    } else {
      ////console.log(this.id);
      this.addLigne();
      this.addEmail();
    }
  }


  getRéunionOnInit() {
    this.spinnerservice.show()

    this.grp = [];
    this.resi = [];
    this.blc = [];
    this.apprt = [];
    this.interne = [];
    this.reunionService.getReunion(this.id).pipe(first())
      .subscribe((x: any) => {

        /*------------------------------------Patch reunion  info--------------------------------------*/
        var reu: Reunion = x.data;
        console.log("reunion: ", x.data, "\n", reu);
        this.attachements = x.data.attachements;
        this.invitation = reu.invitation;
        this.formBasic.patchValue(reu);
        this.formBasic.patchValue({ type: reu['type'].cbmarq });
        this.formBasic.patchValue({ dateDebut: new Date(reu['dateDebut']).toISOString() });

        /*------------------------------------Patch notes  info--------------------------------------*/
        Object.keys(x.data).forEach(name => {
          if (name === 'notes') {
            this.test = x.data[name];
            Object.keys(this.test).forEach(n => {
              this.addLigne();
              var lig: Note = this.test[n];
              ((this.formBasic.get('notes') as FormArray).at(parseInt(n)) as FormGroup).get('cbmarq').patchValue(<number>lig.cbmarq);
              ((this.formBasic.get('notes') as FormArray).at(parseInt(n)) as FormGroup).get('type').patchValue(<number>lig.type.cbmarq);
              ((this.formBasic.get('notes') as FormArray).at(parseInt(n)) as FormGroup).get('contenu').patchValue(lig.description);
            });
          }
        });

        /*------------------------------------Patch biens  info--------------------------------------*/
        x.data.biens?.forEach(res => {
          if (res.groupement) { this.grp.push(res.groupement); }
          if (res.residence) { this.resi.push(res.residence); }
          if (res.bloc) { this.blc.push(res.bloc); }
          if (res.appartement) { this.apprt.push(res.appartement); }
        });

        this.grp = this.grp.map(clt => ({ cbmarq: clt?.cbmarq, label: clt?.intitule }));
        this.grp = this.grp.filter((v, i) => this.grp.findIndex(item => item.value === v.value) === i);
        if (this.grp.length > 0) {
          this.formBiens.patchValue({ groupement: this.grp });
          this.OnChangeGroupement(this.grp);
        }

        this.resi = this.resi.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule }));
        this.resi = this.resi.filter((v, i) => this.resi.findIndex(item => item.cbmarq === v.cbmarq) === i);
        this.OnChangeResidence(this.resi);
        this.formBiens.patchValue({ residence: this.resi });
        //console.log('ress id',this.resi,this.formBiens.value.residence);

        this.blc = this.blc.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule + '-R' + clt.residence?.cbmarq, cbres: clt.residence?.cbmarq }));
        this.blc = this.blc.filter((v, i) => this.blc.findIndex(item => item.cbmarq === v.cbmarq) === i);
        this.formBiens.patchValue({ bloc: this.blc });
        this.OnChangeBloc(this.blc);

        this.apprt = this.apprt.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule + '-B' + clt.bloc.cbmarq, cbbl: clt.bloc.cbmarq }));
        this.apprt = this.apprt.filter((v, i) => this.apprt.findIndex(item => item.cbmarq === v.cbmarq) === i);
        this.formBiens.patchValue({ appartement: this.apprt });

        /*------------------------------------Patch interne  info--------------------------------------*/
        x.data.interne.forEach(res => {
          this.interne.push(res);
        });
        this.interne = this.interne.map(clt => ({ id: clt.user.id, nomComplet: clt.user.nomComplet }));
        this.formBiens.patchValue({ interne: this.interne });

        /*------------------------------------Patch externe  info--------------------------------------*/
        x.data.externe.forEach((value: any, key: number) => {
          this.addEmail();
          ((this.formBiens.get('externe') as FormArray).at(key) as FormGroup).get('adresse').patchValue(value.email);
        });
        this.spinnerservice.hide()
      });

  }


  refraichirReunion() {
    this.spinnerservice.show()

    this.grp = [];
    this.resi = [];
    this.blc = [];
    this.apprt = [];
    this.interne = [];
    this.reunionService.getReunion(this.id).pipe(first())
      .subscribe((x: any) => {
        /*------------------------------------Patch reunion  info--------------------------------------*/
        var reu: Reunion = x.data;
        console.log("refraichirReunion: ", x.data);
        this.attachements = x.data.attachements;
        this.invitation = reu.invitation;
        this.formBasic.patchValue(reu);
        this.formBasic.patchValue({ type: reu['type'].cbmarq });
        this.formBasic.patchValue({ dateDebut: new Date(reu['dateDebut']).toISOString() });

        /*------------------------------------Patch notes  info--------------------------------------*/
        Object.keys(x.data).forEach(name => {
          if (name === 'notes') {
            this.test = x.data[name];
            Object.keys(this.test).forEach(n => {
              //this.addLigne();
              var lig: Note = this.test[n];
              ((this.formBasic.get('notes') as FormArray).at(parseInt(n)) as FormGroup).get('cbmarq').patchValue(<number>lig.cbmarq);
              ((this.formBasic.get('notes') as FormArray).at(parseInt(n)) as FormGroup).get('type').patchValue(<number>lig.type.cbmarq);
              ((this.formBasic.get('notes') as FormArray).at(parseInt(n)) as FormGroup).get('contenu').patchValue(lig.description);
            });
          }
        });

        /*------------------------------------Patch biens  info--------------------------------------*/
        x.data.biens?.forEach(res => {
          if (res.groupement) { this.grp.push(res.groupement); }
          if (res.residence) { this.resi.push(res.residence); }
          if (res.bloc) { this.blc.push(res.bloc); }
          if (res.appartement) { this.apprt.push(res.appartement); }
        });

        this.grp = this.grp.map(clt => ({ cbmarq: clt?.cbmarq, label: clt?.intitule }));
        this.grp = this.grp.filter((v, i) => this.grp.findIndex(item => item.value === v.value) === i);
        if (this.grp.length > 0) {
          this.formBiens.patchValue({ groupement: this.grp });
          this.OnChangeGroupement(this.grp);
        }

        this.resi = this.resi.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule }));
        this.resi = this.resi.filter((v, i) => this.resi.findIndex(item => item.cbmarq === v.cbmarq) === i);
        this.OnChangeResidence(this.resi);
        this.formBiens.patchValue({ residence: this.resi });
        //console.log('ress id',this.resi,this.formBiens.value.residence);

        this.blc = this.blc.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule + '-R' + clt.residence?.cbmarq, cbres: clt.residence?.cbmarq }));
        this.blc = this.blc.filter((v, i) => this.blc.findIndex(item => item.cbmarq === v.cbmarq) === i);
        this.formBiens.patchValue({ bloc: this.blc });
        this.OnChangeBloc(this.blc);

        this.apprt = this.apprt.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule + '-B' + clt.bloc.cbmarq, cbbl: clt.bloc.cbmarq }));
        this.apprt = this.apprt.filter((v, i) => this.apprt.findIndex(item => item.cbmarq === v.cbmarq) === i);
        this.formBiens.patchValue({ appartement: this.apprt });

        /*------------------------------------Patch interne  info--------------------------------------*/
        x.data.interne.forEach(res => {
          this.interne.push(res);
        });
        this.interne = this.interne.map(clt => ({ id: clt.user.id, nomComplet: clt.user.nomComplet }));
        this.formBiens.patchValue({ interne: this.interne });

        /*------------------------------------Patch externe  info--------------------------------------*/
        x.data.externe.forEach((value: any, key: number) => {
          this.addEmail();
          ((this.formBiens.get('externe') as FormArray).at(key) as FormGroup).get('adresse').patchValue(value.email);
        });
        this.spinnerservice.hide()
      });
  }

  checkAddEdit() {

    if (this.id == 0) {
      this.checkAccess("FN21000091");

    } else {
      this.checkAccess("FN21000092")
    }

  }

  checkAccess(code) {
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, code).subscribe((res: any) => {

      this.have_access = res.data;

    }, error => { }, () => {

      if (!this.have_access) {
        this.router.navigate(['403']);
      }

    });
  }



  listetypes() {
    this.communeService.gettype('REU').subscribe((res: any) => {
      if (res.statut) {
        this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
      }
    },
      error => {
        //console.log( 'erreur: ', error);
      }
    );
  }
  gettypesNotes() {
    this.communeService.gettype('RNT').subscribe((res: any) => {
      if (res.statut) {
        this.types_note = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
      }
    },
      error => {
        //console.log( 'erreur: ', error);
      }
    );
  }
  listegroupement() {
    this.groupementService.getGroupements().subscribe((res: any) => {
      if (res.statut) {
        this.groupements = res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule }));

      }
    },
      error => {
        //console.log( 'erreur: ' , error);
      }
    );
  }
  listeresidence() {
    this.residenceService.getResidences().subscribe((res: any) => {
      if (res.statut) {
        this.residences = res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule }));

      }
    },
      error => {
        //console.log( 'erreur: ', error);
      }
    );
  }
  listeinternes() {
    this.reunionService.getInternes().subscribe((res: any) => {
      if (res.statut) {
        this.internes = res.data.map(clt => ({ id: clt.id, nomComplet: clt.nomComplet }));
      }

    },
      error => {
        //console.log( 'erreur: ', error);
      }
    );
  }

  OnChangeGroupement(event: any) {
    this.residences = [];
    this.selected = [];
    if (event) {
      event.forEach(i => {
        this.residenceService.getResidencesByCroupe(i.cbmarq).subscribe((res: any) => {
          if (res.data) {
            // this.residences = res.data;
            this.residences = this.residences.concat(res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule })));
            // this.residences = res.data.map(c => ({ value: c.cbmarq, label: c.intitule  }));
            this.selected = this.selected.concat(res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule })));
            this.readonly = true;
          }
        },
          error => {
            //console.log('erreur: ', error);
          }
        );
      });

    }

    if (!this.selected) {
      this.readonly = false;
    }
  }
  OnChangeResidence(event: any) {
    this.blocs = [];

    if (event) {
      event.forEach(i => {
        this.blocService.getBlocs(i.cbmarq).subscribe((res: any) => {
          // this.blocs.push(res.data);
          if (res.data) {
            this.blocs = this.blocs.concat(res.data.map(clt =>
              ({ cbmarq: clt.cbmarq, label: clt.intitule + '-' + clt.residence?.intitule, cbres: clt.residence?.cbmarq })));
            // this.selectedB = this.selectedB.concat(res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule })));
          }
        },
          error => {
            //console.log( 'erreur: ', error);
          }
        );
      });
    }

  }
  OnChangeBloc(event: any) {
    this.appartements = [];
    if (event) {
      event.forEach(i => {
        this.appartementService.getAppartementByBloc(i.cbmarq).subscribe((res: any) => {
          if (res.data) {
            this.appartements = this.appartements.concat(res.data.map(clt => ({ cbmarq: clt.cbmarq, label: clt.intitule + '-' + clt.bloc.intitule + "-" + clt.residence.intitule, cbbl: clt.bloc.cbmarq })));
          }
        },
          error => {
            //console.log( 'erreur: ', error);
          }
        );
      });
    }
  }

  cleargroupement(event: any) {
    this.formBiens.patchValue({ residences: null });
    this.readonly = false;
    this.listeresidence();
  }
  clearresidence(event: any) {
    this.formBiens.patchValue({ bloc: null });
    this.formBiens.patchValue({ appartement: null });
  }
  clearbloc(event: any) {
    this.formBiens.patchValue({ appartement: null });
  }

  removegroupe(event: any) {
    this.readonly = false;
  }
  removeresidence(event: any) {
    const thisBloc: any[] = [];
    this.formBiens.value.bloc.forEach(i => {
      const removeIndex = this.formBiens.value.bloc.findIndex(itm => itm?.cbres === event.value.cbmarq);
      if (removeIndex !== -1) {
        thisBloc.push(this.formBiens.value.bloc[removeIndex]);
        delete this.formBiens.value.bloc[removeIndex];
      }
    });
    thisBloc.forEach(j => {
      this.formBiens.value.appartement.forEach(k => {
        const removeIndex2 = this.formBiens.value.appartement.findIndex(itm => itm?.cbbl === j?.cbmarq);
        if (removeIndex2 !== -1) {
          delete this.formBiens.value.appartement[removeIndex2];
        }
      });
    });
    this.formBiens.patchValue({ bloc: this.formBiens.value.bloc });
    this.formBiens.patchValue({ appartement: this.formBiens.value.appartement });
  }
  removebloc(event: any) {

    this.formBiens.value.appartement.forEach(k => {
      const removeIndex2 = this.formBiens.value.appartement.findIndex(itm => itm?.cbbl === event.value.cbmarq);
      if (removeIndex2 !== -1) {
        delete this.formBiens.value.appartement[removeIndex2];
      }
    });
    this.formBiens.patchValue({ appartement: this.formBiens.value.appartement });
  }

  supprimerLigneDB(cbmarq: any, ligIndex: number) {
    this.reunionService.deleteNote(cbmarq).subscribe(
      res => {
        this.removeLigne(ligIndex);
        this.toastr.success('Ligne supprimé avec succès !', 'Success!', { progressBar: true });
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!', 'Erreur!', { progressBar: true });
      }
    );
  }

  /************************************-------Bloc Formulaire-------***************************************/
  buildFormBasic() {
    this.formBasic = this.fb.group({
      objet: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      emplacement: new FormControl(null, Validators.required),
      dateDebut: new FormControl(null, Validators.required),
      duree: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required),
      mode: new FormControl(null, Validators.required),
      enligne: new FormControl(1, Validators.required),
      notes: this.fb.array([]),
    });
  }
  buildFormBien() {
    this.formBiens = this.fb.group({
      groupement: new FormControl(null,),
      residence: new FormControl(null,),
      bloc: new FormControl(null,),
      appartement: new FormControl(null,),
      interne: new FormControl(null,),
      externe: this.fb.array([]),
    });
  }

  lignes(): FormArray {
    return this.formBasic.get('notes') as FormArray;
  }
  newligne(): FormGroup {
    return this.fb.group({
      cbmarq: new FormControl(null,),
      type: new FormControl(null, Validators.required),
      contenu: new FormControl(null, Validators.required),
    });
  }
  onchangeMode(event) {
    if (event == 1) {
      this.isDisabled = true;
      this.formBasic.patchValue({ enligne: false })
    }
    else {
      this.isDisabled = false;
      this.formBasic.patchValue({ enligne: true })
    }
  }
  addLigne() {
    this.lignes().push(this.newligne());
  }
  removeLigne(ligIndex: number) {
    this.lignes().removeAt(ligIndex);
  }

  emails(): FormArray {
    return this.formBiens.get('externe') as FormArray;
  }
  newEmail(): FormGroup {
    return this.fb.group({
      adresse: new FormControl(null,),
    });
  }
  addEmail() {
    this.emails().push(this.newEmail());
  }
  removeEmail(ligIndex: number) {
    this.emails().removeAt(ligIndex);
  }
  getExstendsion(image) {
    if (image.endsWith('jpg') || image.endsWith('jpeg')) {
      return 'jpg';
    }
    if (image.endsWith('png')) {
      return 'png';
    }
  }
  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUpload = files.item(0);
  }
  blurEvent(event: any) {
    this.intitule_doc = event.target.value;
  }
  AddAttachement() {
    const myFormData = new FormData();
    myFormData.append('intitule', this.intitule_doc);
    myFormData.append('attachement', this.fileToUpload);

    if (parseInt(this.id) === 0) {
      this.toastr.error('Veuillez enregistrer les informations de la réunion au premier puis réessayer !', 'Erreur!', { progressBar: true });
      return;
    } else {
      if (this.intitule_doc != null && this.fileToUpload != null) {
        this.reunionService.postAttachementReunion(this.id, myFormData).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.attachements = res.data;
            this.namedoc.nativeElement.value = "";
            this.labelImport.nativeElement.innerText = "";
            this.intitule_doc = null;
            this.fileToUpload = null;
            this.toastr.success('Attachement a été ajouté avec succès', 'Succès!', { progressBar: true });

          } else {
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }
        },
          (error) => {
            this.toastr.error('Erreur lors de l\'ajout d\'un attachement . Veuillez réessayer !', 'Erreur!', { progressBar: true });
          });
      } else {
        this.toastr.error('Veuillez remplir tous les champs', 'Erreur!', { progressBar: true });
      }
    }
  }


  confirmerDeleteAttachement(cbmarq: any) {

    Swal.fire({
      title: 'Supprimer un document',
      text: 'Voulez-vous vraiment supprimer ce document ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.DeleteAttachement(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });

  }

  DeleteAttachement(cbmarq: any) {
    this.reunionService.deleteAttachement(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.attachements = res.data;
          this.toastr.success('attachement supprimé avec succès !', 'Success!', { progressBar: true });
        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!', 'Erreur!', { progressBar: true });
      }
    );
  }
  /************************************-------Bloc Formulaire-------***************************************/

  async submit() {
    this.spinnerservice.show()

    if (this.formBasic.invalid) {
      console.log("controls:", this.formBasic.controls)
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    if (this.formBasic.value.enligne == false) {
      this.formBasic.value.enligne = 0
    }
    this.formFinal = this.formBasic.value;
    this.formFinal.dateDebut = this.datepipe.transform(new Date(this.formBasic.value.dateDebut), 'yyyy-MM-dd H:mm');

    if (parseInt(this.id) === 0) {
      console.log("formu:", this.formBasic.value)
      if (this.can_add?.permission) {
        await this.reunionService.postReunion(this.formFinal).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.toastr.success('Réunion a été ajoutée avec succès ! Veuillez affecter des biens', 'Success!', { progressBar: true });
            this.submitted = false;
            this.id = res.data.cbmarq;

            this.refraichirReunion();
            this.spinnerservice.hide()
          } else {
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            this.spinnerservice.hide()
          }
        },
          (error) => {
            this.toastr.error('Erreur lors de l\'ajout d\'une réunion . Veuillez réessayer !', 'Erreur!', { progressBar: true });
            this.spinnerservice.hide()
          });
      } else {
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
        this.spinnerservice.hide()
      }

    } else {
      console.log("www:", this.formBasic.value)
      if (this.can_edit?.permission) {
        this.spinnerservice.hide()
        this.reunionService.putReunion(this.formBasic.value, this.id).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.toastr.success('Réunion a été modifiée avec succès !', 'Success!', { progressBar: true });
            this.submitted = false;
            this.refraichirReunion();

          } else {
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            this.spinnerservice.hide()
          }
        },
          (error) => {
            this.toastr.error('Erreur lors de la modification d\'une réunion . Veuillez réessayer !', 'Erreur!', { progressBar: true });
            this.spinnerservice.hide()
          });
      } else {
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
        this.spinnerservice.hide()
      }

    }

  }


  submitBiens() {
    if (this.formBiens.invalid) {
      this.submitted2 = true;
      return;
    }
    if (parseInt(this.id) === 0) {
      this.toastr.error('Veuillez enregistrer les informations de la réunion au premier puis réessayer !', 'Erreur!', { progressBar: true });
      return;
    } else {
      if (this.can_add?.permission) {
        if (!this.formBiens.value.groupement && !(this.formBiens.value.residence) && !(this.formBiens.value.bloc) && !(this.formBiens.value.appartement) && !(this.formBiens.value.interne) && !(this.formBiens.value.externe[0].adresse)) {
          this.toastr.error('Veuillez affecter des participants !', 'Erreur!', { progressBar: true });
          return;
        }
        this.reunionService.postReunionParticipants(this.id, this.formBiens.value).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.toastr.success('Participants ont été enregistrés avec succès !!', 'Success!', { progressBar: true });
            this.submitted2 = false;
            // this.formBasic.reset();
          } else {
            this.submitted2 = true;
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }
        },
          (error) => {
            this.toastr.error(' Veuillez réessayer !', 'Erreur!', { progressBar: true });
          });
        //console.log(this.formBiens.value);
      } else {
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }

    }

  }

}
