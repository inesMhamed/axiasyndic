import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { DatePipe, DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ReunionService } from 'src/app/shared/services/reunion.service';
import { Reunion } from 'src/app/shared/models/reunion.model';
import { first, isEmpty } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ParametreService } from 'src/app/shared/services/parametre.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import colorGradient from "javascript-color-gradient";
import * as pdfmake from 'pdfmake/build/pdfmake';
import pdfMake from 'pdfmake/build/pdfmake';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import pdfFonts from 'pdfmake/build/vfs_fonts';
import { NgxSpinnerService } from 'ngx-spinner';

// import DetectRTC from 'detectrtc';
declare var RTCMultiConnection;
import * as $ from 'jquery';
import { animate, style, transition, trigger } from '@angular/animations';
class participant {
  name: string;
  photo: string;
  isMicActivated: boolean;
  isActivatedCam: boolean;
  isHandUp: boolean;

  constructor() {

  }
}
@Component({
  selector: 'app-room-reunion',
  templateUrl: './room-reunion.component.html',
  styleUrls: ['./room-reunion.component.scss'],
  providers: [DatePipe],
  animations: [
    trigger(
      'enterAnimation', [
      transition(':enter', [
        // style({ transform: 'opacity 0.3s linear 2s;', opacity: 0 }),
        animate('300ms', style({ transform: 'opacity 0.3s linear 2s;', opacity: 1 }))
      ]),
      transition(':leave', [
        // style({ transform: 'translateX(0)', opacity: 1 }),
        animate('300ms', style({ transform: 'opacity 0.3s linear 2s;', opacity: 0 }))
      ])
    ]
    )
  ]
  // visibility 0s, opacity 0.5s linear
})
export class RoomReunionComponent implements OnInit {
  urlimg = 'http://syndic.gloulougroupe.com/uploads/documents/'
  btnmsg: boolean = false
  minimizewindow: boolean = true
  maximizewindow: boolean = true
  modereunion: boolean = false
  connection: any;
  fileToUpload: File = null;
  fileToUploadChat: File = null;
  isReunionExpire: boolean = false;
  @ViewChild('videocontainer') videocontainer: ElementRef;
  attachements: any[] = [];
  @ViewChild('labelImportchat')
  labelImportchat: ElementRef;
  intitule_doc: any;
  conversationPanel: any
  isActiveMicAdmin: boolean = false;
  isActiveMicRemote: boolean = true;
  isActiveCamAdmin: boolean = true;
  isActiveCamRemote: boolean = true;
  isdarkMode: boolean = true;
  id_current_user: any;
  name_current_user: any;
  id: any;
  reunion: Reunion = new Reunion();
  listParticipant: any[] = [];
  participantspresents: any[] = [];
  participantsnonpresents: any[] = [];
  defaultLogo = "/assets/images/faces/2.jpg";
  maximaseWindowsRoom: boolean = false;
  disabled: boolean = false;
  role: string;
  formvote: FormGroup
  formchat: FormGroup
  formterminer: FormGroup
  submitted: boolean;
  @ViewChild("namedoc") namedoc: ElementRef;
  notevote: any
  clientObj: any
  societe: any;
  timerduration: any;
  dataDuration: any;
  listParticipants: any = []
  listnotegenerale: any = []
  societeObj: any = [];
  typereunion: string;
  details: any;
  allCam: boolean = false
  allAudio: boolean = false
  videoconference: boolean = false
  callActive: boolean = false;
  pc: any;
  localStream: any;
  connectionmeeting: any;
  allparticipants: any;
  arrayparticip: any[];
  @ViewChild('labelImport')
  labelImport: ElementRef;
  @ViewChild('targetscroll') myScrollContainer: ElementRef;
  msgtext: any = ''
  datachat: any;
  today: any;
  contentModal: any;
  cbmarqnote: any;
  valuenote: any = 0;
  notedetails: any = null;
  showvotemodal: boolean = false;
  isVoting: boolean = false;
  modemobile: boolean = false;
  constructor(@Inject(DOCUMENT) private document: Document, private modalService: NgbModal, private fb: FormBuilder,
    private actRoute: ActivatedRoute, private reunionService: ReunionService,
    private router: Router,
    private datepipe: DatePipe, private spinner: NgxSpinnerService, private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService, private paramSociete: ParametreService, private userService: UtilisateurService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.modemobile = false

    this.today = this.datepipe.transform(new Date(), 'dd/MM/yyyy')
    this.document.getElementById('modal-alert').style.display = "none"
    this.id_current_user = localStorage.getItem('id');
    this.name_current_user = localStorage.getItem('username');
    this.getDetailsUser(this.id_current_user)
    this.role = localStorage.getItem('role')
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
    });
    if (this.id) {
      if (this.modemobile) {
        this.router.navigate(['/communite/mobileroom-reunion/', this.id])
      }
      this.getReunion();
      this.reunionService.getchatReunion(this.id).pipe(first()).subscribe((res: any) => {
        if (res.statut === true) {
          this.datachat = res.data
        }
      })
    }
    this.formterminer = this.fb.group({
      conclusion: new FormControl("", Validators.required),
    })
    this.formchat = this.fb.group({
      message: new FormControl(''),
      attachement: new FormControl(''),
    })
    this.formvote = this.fb.group({
      oui: new FormControl(0, Validators.required),
      non: new FormControl(0, Validators.required),
      neutre: new FormControl(0, Validators.required),
    });
    // this.spinner.hide();
    // if (this.videoconference) { this.DemmarrerReunionJNT(); }
  }
  getDetailsUser(id) {
    this.spinner.show();
    this.userService.getuserbyId(id).subscribe((admin: any) => {
      this.details = admin.data
      this.paramSociete.getInfoSociete(this.details.societe).subscribe((res: any) => {
        if (res.statut) {
          this.societe = res.data
          this.timerduration = res.data.DureeVote
          this.societeObj = {
            adresse: res.data.adresse,
            banque: res.data?.banque,
            cbmarq: res.data?.cbmarq,
            code: res.data?.code,
            description: res.data?.description,
            devise: res.data?.devise,
            duplicate: res.data?.duplicate,
            email: res.data?.email,
            intitule: res.data?.intitule,
            logo: res.data?.logo,
            logobase64: res.data?.logobase64,
            matriculeFiscale: res.data?.matriculeFiscale,
            round: res.data?.round,
            telephone: res.data?.telephone,
          }
          this.spinner.hide();
        }
        this.spinner.hide();
      })
    })
  }
  DemmarrerReunionJNT() {
    const unsername = localStorage.getItem('unsername');
    const connection = new RTCMultiConnection();
    let content = document.querySelector('#videocontainer') as HTMLElement;
    let content2 = document.querySelector('#propvideo') as HTMLElement;
    // this line is VERY_important
    connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    // if you want audio+video conferencing
    connection.session = {
      audio: true,
      video: true
    };
    connection.extra = {
      username: unsername,
      fullname: unsername,
      email: unsername + 'muazkh@gmail.com',
      boolean: true,
      integer: 123,
      objects: {},
      whatever: 'whatever'
    };
    connection.openOrJoin(3);

    connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };

    connection.onstream = function (event) {
      const video = event.mediaElement;


      if (event.type === 'local') {
        // content.appendChild(h2);
        content.appendChild(event.mediaElement);
      } if (event.type === 'remote') {
        // content2.appendChild(h2);
        content2.appendChild(event.mediaElement);
      }
      this.renderer.appendChild(this.videocontainer.nativeElement, video);
    };
    this.connectionmeeting = connection
  }
  closemeeting() {
    // close socket.io connection
    this.connectionmeeting.closeSocket();
  }
  stopcameras() {
    // stop all local cameras
    this.connectionmeeting.attachStreams.forEach(function (localStream) {
      localStream.stop();
    });
  }
  disconnectusers() {
    // disconnect with all users
    this.connectionmeeting.getAllParticipants().forEach(function (pid) {
      this.connectionmeeting.disconnectWith(pid);
    });
  }

  onChangeMode() {
    this.isdarkMode = !this.isdarkMode;
  }
  openAttachement(url: any) {
    window.open(url, '_blank');
  }
  getExstendsion(image) {
    if (image.endsWith('jpg') || image.endsWith('jpeg')) {
      return 'jpg';
    }
    if (image.endsWith('png')) {
      return 'png';
    }
  }
  getReunion() {
    this.spinner.show();
    this.reunionService.getReunion(this.id).subscribe((x: any) => {
      if (x.data) {

        console.log('reunion', x.data)
        this.timerduration = x.data.DureeVote
        this.participantspointes(this.id)
        this.reunion = x.data;
        if (x?.data?.enligne && x.data.statut != 'E0042') {
          this.videoconference = true
          this.OpenRoomMaster();
        }
        else {
          this.videoconference = false
        }
        this.allparticipants = x?.data?.proprietaire?.map((el) => { el.photo = this.defaultLogo; return el })
        if (this.reunion?.type?.code == "E0025")
          this.typereunion = 'Extraordinaire'
        if (this.reunion?.type?.code == "E0024")
          this.typereunion = 'Ordinaire'
        if (this.reunion?.type?.code == "E0091")
          this.typereunion = 'Délégué'
        Object(x?.data?.biens).forEach(name => {
          if (name?.appartement !== null && name.bloc !== null && name.residence !== null) {
            this.listParticipants.push(name.residence?.intitule + '|' + name.bloc?.intitule + '|' + name.appartement?.intitule)
          }
          if (name.bloc !== null && name.appartement === null && name.residence !== null)
            this.listParticipants.push(name.residence?.intitule + '|' + name.bloc?.intitule)
          if (name.appartement === null && name.residence !== null && name.bloc === null)
            this.listParticipants.push(name.residence?.intitule)
          if (name.groupement !== null && name.residence === null)
            this.listParticipants.push(name.groupement?.intitule)
        })
        this.spinner.hide();
      }
      this.spinner.hide();
    }
      , error => {
        this.spinner.hide();
      }
    );
  }

  muteActiveMicParticip(participant: participant) {
    participant.isMicActivated = !participant.isMicActivated;
  }

  muteActiveCamParticip(participant: participant) {
    participant.isActivatedCam = !participant.isActivatedCam;
  }

  muteActiveMicparticipant(i) {
    this.isActiveMicRemote = !this.isActiveMicRemote;
  }
  muteActiveMicAdmin() {
    if (this.isActiveMicAdmin) {
      this.connectionmeeting.attachStreams[0].mute('audio');
    } else {
      this.connectionmeeting.attachStreams[0].unmute('audio')
    }
    this.isActiveMicAdmin = !this.isActiveMicAdmin;
  }

  muteActiveCamParticipant(i) {

    this.isActiveCamRemote = !this.isActiveCamRemote;
  }

  getLocalStream(stream, enable) {
    if (stream) {
      for (var i = 0; i < stream.getTracks().length; i++) {
        var track = stream.getAudioTracks()[0];
        if (track)
          track.enabled = enable;
        //track.stop();
      }
    }

  }
  getFileHTML(file) {
    var url = file.url || URL.createObjectURL(file);
    var attachment = '<a href="' + url + '" target="_blank" download="' + file.name + '">Download: <b>' + file.name + '</b></a>';
    if (file.name.match(/\.jpg|\.png|\.jpeg|\.gif/gi)) {
      attachment += '<br><img crossOrigin="anonymous" src="' + url + '">';
    } else if (file.name.match(/\.wav|\.mp3/gi)) {
      attachment += '<br><audio src="' + url + '" controls></audio>';
    } else if (file.name.match(/\.pdf|\.js|\.txt|\.sh/gi)) {
      attachment += '<iframe class="inline-iframe" src="' + url + '"></iframe></a>';
    }
    return attachment;
  }


  muteActiveCamAdmin() {
    if (this.isActiveCamAdmin) {
      this.connectionmeeting.attachStreams[0].mute('video');
    } else {
      this.connectionmeeting.attachStreams[0].unmute('video')
    }
    this.isActiveCamAdmin = !this.isActiveCamAdmin;
  }

  minimiseMaximiseWindows() {
    this.maximaseWindowsRoom = !this.maximaseWindowsRoom;
  }

  pointageparticipant(id) {
    if (this.reunion?.mode == 1) {
      this.reunionService.pointagerReunion(id).subscribe((res: any) => {
        this.participantspointes(this.id)
      })
    }
    else {

    }
  }
  participantspointes(cbmarq) {
    this.spinner.show();
    this.reunionService.getpresnecespointage(cbmarq).subscribe((res: any) => {
      this.participantspresents = res.data.pointage
      this.participantsnonpresents = res.data.dépointage
      this.allparticipants = res.data.pointage.concat(res.data.dépointage)
      this.spinner.hide();
      this.allparticipants = this.allparticipants.reduce((accumalator, current) => {
        if (!accumalator.some((item) => item.user?.id === current.user?.id)) {
          accumalator.push(current);
        }
        return accumalator;
      }, []);
    })

  }

  openmodalvote(cbmarq, item, content) {
    this.notevote = item
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result.then((result) => {
      }, (reason) => {
      });
  }
  openmodalterminer(cbmarq, content) {

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result.then((result) => {
      }, (reason) => {
      });
  }

  onclose() {
    this.modalService.dismissAll();
  }
  resetForm() {
    this.formvote.patchValue({ oui: 0, non: 0, neutre: 0 })
  }
  voteauto(id, note, vote, i) {
    if (vote == 0) {
      var elems = document.querySelectorAll(".bgneutreactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgneutreactive");
      });
      var elems = document.querySelectorAll(".bgouiactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgouiactive");
      });
      var elems = document.querySelectorAll(".bgnonactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgnonactive");
      });
      document.getElementById('neutre' + i).classList.add('bgneutreactive');
    }
    if (vote == 1) {
      var elems = document.querySelectorAll(".bgouiactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgouiactive");
      });
      var elems = document.querySelectorAll(".bgnonactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgnonactive");
      });
      var elems = document.querySelectorAll(".bgneutreactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgneutreactive");
      });
      document.getElementById('oui' + i).classList.add('bgouiactive');
    }
    if (vote == 2) {
      var elems = document.querySelectorAll(".bgnonactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgnonactive");
      });
      var elems = document.querySelectorAll(".bgneutreactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgneutreactive");
      });
      var elems = document.querySelectorAll(".bgouiactive");
      [].forEach.call(elems, function (el) {
        el.classList.remove("bgouiactive");
      });
      document.getElementById('non' + i).classList.add('bgnonactive');
    }
    //action vote
    if (this.reunion?.mode == 2) {
      this.reunionService.voterautoReunion(id, note, { vote: vote }).subscribe((res: any) => {
        if (res.statut == false) {
          this.toastr.error(res.message)
        }
        else {
          this.getReunion()
          this.toastr.success(res.message)
        }
      })
    }
  }
  //timer of duration
  starttimer() {
    this.document.getElementById('modal-alert').style.display = "block"
    const nbcolor = this.timerduration
    const gradientArray1 = colorGradient

      .setGradient("#ffc000", "#396aa1")
      .setMidpoint(nbcolor)
      .getArray();
    setInterval(() => {
      if (this.timerduration > 0) {
        setTimeout(() => {
          for (let i = gradientArray1.length; i > 0; i--) {
            if (i == this.timerduration)
              this.document.getElementById('circle-timer').style.backgroundColor = colorGradient.getColor(i);
          }
        }, this.timerduration);
        this.timerduration--;
      }
      if (this.timerduration == 0) {
        document.getElementById('modal-alert').style.display = "none"
      }
    }, 1000)
  }
  submitterminer() {
    this.spinnerservice.show()
    if (this.formterminer.invalid) {
      this.submitted = true;
      this.spinnerservice.hide()
      return;
    }
    this.reunionService.cloturerReunion(this.id, this.formterminer.value).subscribe((res: any) => {
      if (res.statut === true) {
        this.toastr.success(res.message, 'Success!', { progressBar: true });

        this.formterminer.reset()
        this.modalService.dismissAll();
        this.closeallroom()
      } else {
        this.spinnerservice.hide()
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    })
  }
  submitvote() {
    this.spinnerservice.show()
    if (this.formvote.invalid) {
      this.submitted = true;
      this.spinnerservice.hide()
      return;
    }
    if (this.reunion.mode == 1) {
      this.reunionService.votermanuelReunion(this.id, this.notevote.cbmarq, this.formvote.value).subscribe((res: any) => {
        if (res.statut == false) {
          this.toastr.error(res.message)
          this.spinnerservice.hide()
        }
        else {
          this.getReunion()
          this.toastr.success(res.message)
          this.resetForm()
          this.modalService.dismissAll();
        }
      })
    }
  }
  //lancer vote
  commencerVote(id, note) {
    this.starttimer()
    this.reunionService.lancerVote(id, note).subscribe((res: any) => {
      this.dataDuration = res
      if (res.statut == false) {
        this.toastr.error(res.message)
      }
      else {
        this.getReunion()
        this.toastr.success(res.message)
      }
    })
  }
  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUpload = files.item(0);
  }
  blurEvent(event: any) {
    this.intitule_doc = event.target.value;
  }
  AddAttachement() {
    const myFormData = new FormData();
    myFormData.append('intitule', this.intitule_doc);
    myFormData.append('attachement', this.fileToUpload);

    if (parseInt(this.id) === 0) {
      this.toastr.error('Veuillez enregistrer les informations de la réunion au premier puis réessayer !', 'Erreur!', { progressBar: true });
      return;
    } else {
      if (this.intitule_doc != null && this.fileToUpload != null) {
        this.reunionService.postAttachementReunion(this.id, myFormData).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.attachements = res.data;

            this.namedoc.nativeElement.value = "";
            this.labelImport.nativeElement.innerText = "";
            this.intitule_doc = null;
            this.fileToUpload = null;
            this.getReunion()
            this.toastr.success('Attachement ajouté avec succès', 'Succès!', { progressBar: true });

          } else {
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }
        },
          (error) => {
            this.toastr.error('Erreur lors d\'ajout d\'une attachement . Veuillez réessayer !', 'Erreur!', { progressBar: true });
          });
      } else {
        this.toastr.error('Veuillez remplir tous les champs', 'Erreur!', { progressBar: true });
      }
    }
  }
  GenererPV() {
    this.spinnerservice.show()
    let listparticip = this.listParticipants.map((ell: any, i: any) => {
      if (i !== this.listParticipants.length - 1) { return ell + ', ' }
      else { return ell }
    })

    let ordrejour = this.reunion?.notes.filter(ss => ss.type.code == 'E0027').map(el => {
      return [
        { text: el?.description, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'left' },
        { text: el?.v_oui, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'center' },
        { text: el?.v_non, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'center' },
        { text: el?.v_neutre, fontSize: 10, bold: false, margin: [3, 0, 0, 0], fillColor: 'white', style: 'headertable', alignment: 'center' },
      ]
    })
    let notegenerale = this.reunion?.notes.filter(ss => ss.type.code === 'E0026')
    if (notegenerale.length !== 0) {
      this.listnotegenerale = notegenerale.map(el => {
        return { text: '- ' + el?.description + '\n', margin: [0, 2, 0, 2] }
      })
    } else {
      this.listnotegenerale.push({ text: '- Aucune note générale saisie.\n', margin: [0, 2, 0, 2] })
    }
    let presents: any = this.participantspresents.map((ell: any) => {
      if (ell?.user) {
        return [
          { text: ell?.user?.nomComplet, fontSize: 10, margin: [5, 0, 0, 0] },
        ]
      }
      if (ell?.email) {
        return [
          { text: ell?.email, fontSize: 10, margin: [5, 0, 0, 0] },
        ]
      }
    })
    var perChunk = 4 // items per chunk
    //diviser prensent en 4
    if (presents.length > 0) {
      var resultpresent = presents.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / perChunk)
        if (!resultArray[chunkIndex]) {
          resultArray[chunkIndex] = [] // start a new chunk
        }
        resultArray[chunkIndex].push(item[0])
        return resultArray
      }, [])
      for (let k of resultpresent) {
        while (k.length < 4) {
          k.push({ text: '', fontSize: 10, margin: [5, 0, 0, 0] })
        }
      }
    } else {
      var resultabsent: any = [[{ text: 'Aucun présent.', fontSize: 10, margin: [5, 0, 0, 0] }]]
    }
    let absents: any = this.participantsnonpresents.map((ell: any, i: any) => {
      if (ell?.user) {
        return [
          { text: ell?.user?.nomComplet, fontSize: 10, lineHeight: '1.2', margin: [5, 0, 0, 0] },
        ]
      }
      if (ell?.email) {
        return [
          { text: ell?.email, fontSize: 10, lineHeight: '1.2', margin: [5, 0, 0, 0] },
        ]
      }
    })
    //diviser absent en 4
    if (absents.length > 0) {
      var resultabsent = absents.reduce((resultArray, item, index) => {

        const chunkIndex = Math.floor(index / perChunk)
        if (!resultArray[chunkIndex]) {
          resultArray[chunkIndex] = [] // start a new chunk
        }

        resultArray[chunkIndex].push(item[0])
        return resultArray

      }, [])
      for (let k of resultabsent) {
        while (k.length < 4) {
          k.push({ text: '', fontSize: 10, margin: [5, 0, 0, 0] })
        }
      }
    } else {
      var resultabsent: any = [[{ text: 'Aucun absent !', fontSize: 10, margin: [5, 0, 0, 0] }]]
    }

    let conclusion: any
    if (this.reunion?.conclusion == null) {
      conclusion = 'Aucune conclusion saisie.'
    } else {
      conclusion = this.reunion?.conclusion
    }
    var docDefinition = {
      footer: {
        margin: [40, 12, 40, 0],
        height: 20,
        table: {
          alignment: 'justify',
          italics: true,
          widths: ['20%', '80%'],
          heights: [10.25],
          body: [[
            { text: 'Tel: ' + this.societeObj?.telephone, margin: [5, 0, 0, 0], fontSize: 9, style: 'filledHeader' },
            { text: 'Adresse: ' + this.societeObj?.adresse + '', alignment: 'right', fontSize: 9, margin: [0, 0, 5, 0], style: 'filledHeader' },

          ]]

        },
        layout: 'noBorders'
      },
      info: {
        title: 'Reunion ' + this.typereunion,
        author: 'axiasyndic',
        subject: 'axiasyndic',
        keywords: 'axiasyndic',
      },
      content: [
        {
          table: {

            widths: ['auto', '*'],
            body: [
              [{
                image: this.societeObj?.logobase64, margin: [0, 3, 0, 3],
                fit: [50, 50]
              },
              {
                margin: [0, 10, 0, 2],
                text: [
                  { text: 'Réunion  ' + this.typereunion, bold: true, fontSize: '18', color: '#102758' },
                  { text: '\nLe  ' + this.datepipe.transform(new Date(), 'd MMMM  y') },
                ]
              },
              ],
            ]
          },
          layout: 'noBorders'
        },
        {
          table: {
            widths: ['30%', '70%'],
            heights: [12],
            body: [[
              { text: 'Début: ' + this.datepipe.transform(this.reunion?.dateDebut2?.date, 'HH:mm'), fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
              { text: 'Fin: ' + this.datepipe.transform(this.reunion?.dateFin2?.date, 'HH:mm'), fontSize: 10, bold: true, margin: [0, 0, 5, 0], style: 'filledHeader', alignment: 'right' },

            ]]
          },
          layout: 'noBorders'
        },
        { text: '', margin: [0, 5, 0, 5] },
        //Infos Générales
        {
          table: {

            widths: ['17%', '83%'],
            body: [
              [
                { text: 'Objet: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                { text: this.reunion.objet, margin: [3, 5, 0, 5], style: 'headertable' },
              ],
              [
                { text: 'Présenteur: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                { text: this.reunion.cbcreateur?.nomComplet, margin: [3, 5, 0, 5], style: 'headertable' },
              ],
              [
                { text: 'Société: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                { text: this.societeObj?.intitule, margin: [3, 5, 0, 5], style: 'headertable' },
              ],
              [
                { text: 'Lieu: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                { text: this.reunion.emplacement, margin: [3, 5, 0, 5], style: 'headertable' },
              ],
              [
                { text: 'Description: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                { text: this.reunion.description, lineHeight: '1.2', margin: [3, 5, 0, 5], style: 'headertable' },
              ],
              [
                { text: 'Particpants: ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header' },
                { text: listparticip, margin: [3, 5, 0, 5], style: 'headertable' },
              ],

            ],
          },
          layout: {
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? '#edeff8' : '#edeff8';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? '#edeff8' : '#edeff8';
            },
          }
        },
        {
          text: '', margin: [0, 5, 0, 5]
        },
        //participants
        {
          table: {
            widths: ['100%'],
            heights: [12],
            body: [[
              { text: 'Participants ', fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
            ]]
          },
          layout: 'noBorders'
        },//Présents
        { text: 'Présents: \n', bold: true, style: 'header', margin: [3, 5, 0, 3] },
        {
          table: {
            widths: ['25%', '25%', '25%', '25%'],
            body: [].concat(resultpresent)
          },
          layout: 'noBorders'
        },
        //Absents
        { text: 'Absents et excusés: \n', bold: true, style: 'header', margin: [3, 3, 0, 0] },
        {
          table: {
            widths: ['25%', '25%', '25%', '25%'],
            fontSize: 10,
            body: [].concat(resultabsent)
          },
          layout: 'noBorders'
        },
        { text: '', margin: [0, 3, 0, 3] },
        //Point à discuter
        {
          table: {

            widths: ['100%'],
            heights: [12],
            body: [[
              { text: 'Point à discuter et Vote', fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
            ]]
          },
          layout: 'noBorders'
        },
        { text: 'Note Générale:\n', style: 'header', bold: true, margin: [5, 5, 0, 3], alignment: 'left' },
        { text: this.listnotegenerale, style: 'headertable', margin: [5, 0, 0, 5], alignment: 'left', fontSize: 10 },
        { text: 'Ordre du jour:\n', style: 'header', bold: true, margin: [5, 0, 0, 5], alignment: 'left' },
        {
          table: {
            widths: ['70%', '10%', '10%', '10%'],
            body: [
              [
                {
                  text: 'Vote ', bold: true, fillColor: '#f5f6fa', margin: [3, 5, 0, 5], style: 'header', alignment: 'left'
                },
                { text: 'Oui', bold: true, margin: [3, 5, 0, 5], fillColor: '#f5f6fa', style: 'header', alignment: 'center' },
                { text: 'Non', bold: true, margin: [3, 5, 0, 5], fillColor: '#f5f6fa', style: 'header', alignment: 'center' },
                { text: 'Neutre', bold: true, margin: [3, 5, 0, 5], fillColor: '#f5f6fa', style: 'header', alignment: 'center' },
              ],
            ].concat(ordrejour),
          },
          layout: {
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? '#edeff8' : '#edeff8';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? '#edeff8' : '#edeff8';
            },
          }
        },
        { text: '', margin: [0, 5, 0, 5] },
        //Conclusion
        {
          table: {
            widths: ['100%'],

            heights: [12],
            body: [[
              { text: 'Conclusion', fontSize: 10, bold: true, margin: [5, 0, 0, 0], style: 'filledHeader', alignment: 'left' },
            ]]
          },
          layout: 'noBorders'
        },
        { text: conclusion, lineHeight: '1.2', style: 'headertable', fontSize: 10, margin: [5, 5, 5, 5], alignment: 'left' },

      ],
      styles: {
        date: {
          alignment: 'right',
          fontSize: '10',
          color: '#102758',
          fillColor: '#f5f6fa',
          margin: [0, 20, 5, 0]
        },
        table: {
          fontSize: '10',
          color: 'black',
          alignment: 'left',
        },
        filll: {
          fontSize: '10',
          color: '#102758',
          fillColor: '#f5f6fa',
          margin: [0, 0, 5, 0]
        },
        filledHeader: {
          borderColor: '#213C7F ',
          fontSize: '10',
          color: 'white',
          fillColor: '#213C7F ',
        },

        headertable: {
          color: 'black',
          fontSize: '10',

        },
        header: {
          color: '#102758',
          fontSize: '10',

        },
        total: {
          color: 'black',
          fontSize: '10',
          alignment: 'right',
          italics: true,
          margin: [5, 0, 5, 0],

        },
        totals: {
          color: '#102758',
          bold: true,
          fontSize: '10',
          margin: [0, 0, 5, 0],
          alignment: 'right',
          italics: true,

        },

        pageSize: 'A4',
        pageOrientation: 'portrait'

      },
    };
    pdfmake.createPdf(docDefinition).open();
    this.spinnerservice.hide()
  }

  OpenRoomMaster() {

    var roomid = 'REU' + this.id;
    var idReunion = this.id;
    var fullName = localStorage.getItem('username')
    var roomPassword = 'REU' + this.id;
    var roleUser = localStorage.getItem('role')
    var createurId = parseInt(this.reunion?.cbcreateur?.id);
    var CoonectUserId = parseInt(this.id_current_user);
    var testonerleave = false;
    var urlfile = this.urlimg;
    var notedetails = this.notedetails
    var conteent: any[] = this.contentModal
    var timeduree = this.timerduration * 1000
    var params = {
      'userFullName': fullName,
      'userlogo': this.defaultLogo,
      'publicRoomIdentifier': roomid,
      'open': true,
      'password': roomPassword,
      'sessionid': roomid,
    }
    const connection = new RTCMultiConnection();
    // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
    connection.socketURL = 'https://muazkhan.com:9001/';
    connection.socketMessageEvent = 'AXIA-message';
    this.connection = connection;
    console.log('connn========', connection)

    if (parseInt(this.id_current_user) === parseInt(this.reunion?.cbcreateur?.id)) {
      connection.session = {
        data: true, // at least data-connection must open if he do not have camera+mic
        video: true,
        audio: true
      };
    } else {
      this.connection.session = {
        audio: true,
        data: true  // at least data-connection must open if he do not have camera+mic
      };
    }

    if (!!params.password) {
      this.connection.password = params.password;
    }

    this.connection.extra.userFullName = params.userFullName;
    this.connection.publicRoomIdentifier = params.publicRoomIdentifier;
    this.connection.socketMessageEvent = 'canvas-dashboard-demo';
    // keep room opened even if owner leaves
    this.connection.autoCloseEntireSession = true;
    this.connection.maxParticipantsAllowed = 1000;
    this.connection.chunkSize = 15 * 1000; // using 15k
    this.connection.enableFileSharing = true;

    connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };

    this.connection.DetectRTC.load(function () {
      if (connection.DetectRTC.hasMicrophone === true) {
        // enable microphone
        connection.mediaConstraints = {
          audio: {
            echoCancellation: { exact: true },
            googEchoCancellation: { exact: true },
            googAutoGainControl: { exact: true },
            googNoiseSuppression: { exact: true },
          }
        };
        // connection.mediaConstraints.audio = true;
        // connection.session.audio = true;
      } else {
        connection.mediaConstraints.audio = false;
        connection.session.audio = false;
      }
      if (connection.DetectRTC.hasWebcam === true) {
        if (createurId === CoonectUserId) {
          // enable camera
          connection.mediaConstraints.video = true;
          connection.session.video = true;
        } else {
          connection.mediaConstraints.video = false;
          connection.session.video = false;
        }
      } else {
        connection.mediaConstraints.video = false;
        connection.session.video = false;
      }
    });
    //panel discussion
    this.conversationPanel = document.getElementById('idscrl');
    var panel = this.conversationPanel
    this.myScrollContainer.nativeElement.scroll({
      top: this.myScrollContainer.nativeElement.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
    // panel.scrollTop = panel.clientHeight;
    // panel.scrollTop = panel.scrollHeight - panel.scrollTop;


    this.connection.onclose = this.connection.onerror = this.connection.onleave = function (event) {
      // this.connection.onUserStatusChanged(event);
      connection.onUserStatusChanged(event);
      if (event.extra.roomOwner === true) {
        window.location.href = '/communite/detail-reunion/' + idReunion;
      }
    };
    var modalservice = this.modalService

    this.connection.onmessage = function (event) {

      var conversationPanel = document.getElementById('globalcontainer');
      const urlattachement = urlfile

      if (event.data.chatMessage.includes("modalvote")) {
        var div = document.createElement('div');
        var titre = document.getElementById('exampleModalLabel');
        var divcontent = document.getElementById('modalmodalvote');
        divcontent.style.visibility = 'visible';
        var btnoui = document.getElementById('oui')
        var btnnon = document.getElementById('non')
        var btnneutre = document.getElementById('neutre')
        var cbmarqnote = event.data.chatMessage.slice(event.data.chatMessage.indexOf('modalvote') + 'modalvote'.length);
        titre.innerHTML = 'Vote sur la note :<br><span style="font-size: 12px; font-weight: normal;">' + event.data.chatdata + ' </span>'

        btnoui.onclick = function (e) {
          voteauto(idReunion, cbmarqnote, 1, event)
        }
        btnnon.onclick = function (b) {
          voteauto(idReunion, cbmarqnote, 2, event)
        }
        btnneutre.onclick = function (c) {
          voteauto(idReunion, cbmarqnote, 0, event)
        }

        divcontent.appendChild(div)

        setTimeout(() => {
          if (createurId != CoonectUserId) {
            window.location.reload();
          }
        }, timeduree);

      }


      else {
        if (event.data.chatMessage) {
          if ($('.chatbox-open').hasClass('chat_open') == false) {
            $('.chatbox-open').addClass('bg-danger');
          } else {
            $('.chatbox-open').removeClass('bg-danger');
            $('.chatbox-popup__header').addClass('bg-danger');
          }

          var div = document.createElement('div');
          div.className = 'chat-msg';
          div.id = 'cm-msg-1';
          var divchild = document.createElement('div');
          divchild.className = 'cm-msg-text';
          var span = document.createElement('span');
          span.className = 'cm-msg-text';
          if (event.data) {
            var Z = event.data.chatMessage.slice(event.data.chatMessage.indexOf('<br>') + '<br>'.length);
            // (click)="' + this.openAttachement(urlattachement) + '"
            div.innerHTML = '<div style="clear: both;position: relative;float: left;text-align:justify;background: #c2c2c254!important; color: #212020;padding: 10px 15px 10px 15px;max-width: 75%;text-align:left; margin-left: -11px;margin-bottom: 20px;border-radius: 30px;word-break: break-all;"><b><span style="color: #055aa5;font-weight: bold;float: left;">' + event.extra.userFullName + '</span>:<span style="font-size: x-small;float: right;color: #9c9a9a;margin-left: 5px;">' + new Date().toLocaleTimeString([], { hour12: false, hour: "numeric", minute: "numeric" }) + '</span> </br><a style="cursor:pointer" href="' + urlfile.concat(Z) + '" target="_blank" class="font-weight-normal text-dark">' + event.data.chatMessage + '</a></div>';
            if (event.data.checkmark_id) {
              connection.send({
                checkmark: 'received',
                checkmark_id: event.data.checkmark_id
              });
            }
          } else {
            divchild.innerHTML = '<b>You:</b>-------- <img class="checkmark" id="' + event.data.checkmark_id + '" title="Received" src="https://www.webrtc-experiment.com/images/checkmark.png"><br>' + event;
            divchild.style.background = '#cbffcb';
          }
          setTimeout(() => {
            var conversationPanelx2 = document.getElementById('idscrl');
            // div.appendChild(divchild)
            conversationPanelx2.appendChild(div);
            conversationPanelx2.scrollTop = conversationPanelx2.clientHeight;
            conversationPanelx2.scrollTop = conversationPanelx2.scrollHeight - conversationPanelx2.scrollTop;
          }, 500);
        }
      }

      if (event.data.checkmark === 'received') {
        var checkmarkElement = document.getElementById(event.data.checkmark_id);
        if (checkmarkElement) {
          checkmarkElement.style.display = 'inline';
        }
        return;
      }

    }

    connection.onUserStatusChanged = function (event) {
      var names = [];
      $(".fa-user-circle-o").removeClass('text-success').addClass('text-danger');
      $(".participt").removeClass('text-success').addClass('text-danger');

      connection.getAllParticipants().forEach(function (pid) {
        var user = connection.peers[pid];
        names.push([user?.extra?.usercbmarq, getFullName(pid), user?.extra?.logo]);

        $(".part" + user[0]).removeClass('text-danger').addClass('text-success');
        $(".particip" + user[0]).removeClass('text-danger').addClass('text-success');
      });

      names.forEach(function (user) {
        $(".part" + user[0]).removeClass('text-danger').addClass('text-success');
        // $(".parti" + user[0]).removeClass('bg-danger').addClass('bg-success');
        $(".particip" + user[0]).removeClass('text-danger').addClass('text-success');
      });
      if (createurId !== CoonectUserId) {
        $(".part" + connection.extra.usercbmarq).removeClass('text-danger').addClass('text-success');
        $(".particip" + connection.extra.usercbmarq).removeClass('text-danger').addClass('text-success');
      }
    }
    connection.onopen = function (event) {
      connection.onUserStatusChanged(event);
    };

    this.connection.onstream = function (event) {
      event.mediaElement.controls = true;
      if (event.stream.isScreen && !event.stream.canvasStream) {

        var div = document.getElementById('video-container-remote') as HTMLVideoElement;
        div.srcObject = event.stream;
        // $('#video-container-remote').get(0).srcObject = event.stream;
        $('#video-container-remote').hide();
      } else if (event.extra.roomOwner === true) {
        var video = document.getElementById('main-video') as HTMLVideoElement;
        video.setAttribute('data-streamid', event.streamid);
        if (event.type === 'local') {
          video.muted = false;
          video.volume = 0.5;
        }
        video.srcObject = event.stream;
        $('#main-video').show();

      } else {
      }
      connection.onUserStatusChanged(event);

    };


    window.onkeyup = function (e) {
      var filechatname = this.fileToUploadChat;
      var filechat = this.labelImportchat;
      var code = e.keyCode || e.which;
      if (code == 13) {
        filechatname = null
        filechat = null
        $('#btn-chat-message').click();
      }
    };
    if (params.open === true) {
      this.connection.extra.usercbmarq = localStorage.getItem('id');
      this.connection.extra.userOwner = false;
      this.connection.extra.logo = this.defaultLogo;
      this.connection.extra.nom = params.userFullName;

      if (parseInt(this.id_current_user) === parseInt(this.reunion?.cbcreateur?.id)) {
        this.connection.extra.roomOwner = true;
        this.connection.extra.userOwner = true;
      }
      this.connection.openOrJoin(params.sessionid, function (isRoomOpened, roomid, error) {
        if (error) {
          if (error === connection.errors.ROOM_NOT_AVAILABLE) {
            return;
          }
          console.log(error);
        }
      });
    } else {
      this.connection.join(params.sessionid, function (isRoomJoined, roomid, error) {
        if (error) {
          if (error === this.connection.errors.ROOM_NOT_AVAILABLE) {
            alert('This room does not exist. Please either create it or wait for moderator to enter in the room.');
            return;
          }
          if (error === this.connection.errors.ROOM_FULL) {
            alert('Room is full.');
            return;
          }
          if (error === this.connection.errors.INVALID_PASSWORD) {
            this.connection.password = prompt('Please enter room password.') || '';
            if (!this.connection.password.length) {
              alert('Invalid password.');
              return;
            }
            this.connection.join(params.sessionid, function (isRoomJoined, roomid, error) {
              if (error) {

              }
            });
            return;
          }
        }
      });
    }
    var detailreunion = this.reunion
    var servicereunion = this.reunionService


    var toastr = this.toastr
    function voteauto(id, note, vote, event) {
      if (vote == 0) {
        $('.non').removeClass('bgnonactive').addClass('nonactive');
        $('.oui').removeClass('bgouiactive').addClass('ouiactive');
        $('.neutre').removeClass('neutreactive').addClass('bgneutreactive');
      }
      if (vote == 1) {
        $('.non').removeClass('bgnonactive').addClass('nonactive');
        $('.oui').addClass('bgouiactive').removeClass('ouiactive');
        $('.neutre').removeClass('bgneutreactive').addClass('neutreactive');
      }
      if (vote == 2) {

        $('.non').addClass('bgnonactive').removeClass('nonactive');
        $('.oui').removeClass('bgouiactive').addClass('ouiactive');
        $('.neutre').removeClass('bgneutreactive').addClass('neutreactive');
      }
      //action vote
      if (detailreunion.mode == 2) {
        servicereunion.voterautoReunion(id, note, { vote: vote }).subscribe((res: any) => {
          if (res.statut == false) {
            toastr.error(res.message)
          }
          else {
            toastr.success(res.message)
          }
        })
      }
    }
    function getFullName(userid) {
      var _userFullName = userid;
      if (connection.peers[userid] && connection.peers[userid].extra.userFullName) {
        _userFullName = connection.peers[userid].extra.userFullName;
      }
      return _userFullName;
    }
    this.connection.socketCustomEvent = params.sessionid;
    connection.connectSocket(function (socket) {
      // listen for custom messaging event
      socket.on(connection.socketCustomEvent, function (event) {
        // check if someone sent you a custom message
        if (event.message === 'remotemuteev') {
          connection.socket.on('remotemute', function () {
            connection.attachStreams[0].mute('audio');
            // if (data.video) connection.attachStreams[0].mute('video');
          });
        }
        if (event.message === 'remoteunmuteev') {
          connection.socket.on('remoteunmute', function () {
            connection.attachStreams[0].unmute('audio');
            // if (data.video) connection.attachStreams[0].mute('video');
          });
        }
        if (event.message === 'remoteunmutepart') {
          connection.socket.on('remoteunmute', function () {
            var streamByUserId = connection.streamEvents.selectFirst({ userid: event.userid }).stream;
            streamByUserId.unmute();
            // connection.attachStreams[0].unmute('audio');

          });
        }
        if (event.message === 'remotemutepart') {
          connection.socket.on('remotemute', function () {
            var streamByUserId = connection.streamEvents.selectFirst({ userid: event.userid }).stream;
            streamByUserId.mute();
            // connection.attachStreams[0].unmute('audio');

          });
        }
        if (event.message === 'voteremote') {
          connection.socket.on('voteremote', function () {
            // var streamByUserId = connection.streamEvents.selectFirst({ userid: event.userid }).stream;

            // console.log('streamByUserId', streamByUserId)
          });
        }

      });
    });
  }

  lancerVote(note) {
    this.commencerVote(this.reunion.cbmarq, note.cbmarq)
    this.notedetails = note
    this.isVoting = true
    var checkmark_id = this.connection.userid + this.connection.token();
    this.connection.send({
      chatMessage: 'modalvote' + note.cbmarq,
      chatdata: note.description,
      checkmark_id: checkmark_id
    });

  }
  closeVote() {
    this.document.getElementById('modalmodalvote').style.visibility = 'hidden'
  }
  getnotes() {
    this.reunionService.getReunion(this.id).subscribe((res: any) => {
      if (res.statut)
        this.reunion = res.data
    })
  }
  openpanel() {
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.clientHeight;
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.myScrollContainer.nativeElement.scrollTop;

    this.btnmsg = !this.btnmsg;
    if (this.btnmsg) {
      document.getElementById('mytextarea').focus()
      $('.chatbox-open').removeClass('bg-danger');
    }
    this.myScrollContainer.nativeElement.click()
  }
  onpanelopened() {
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.clientHeight;
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.myScrollContainer.nativeElement.scrollTop;
  }
  textfocus() {
    if ($("#mytextarea").is(":focus")) {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.clientHeight;
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.myScrollContainer.nativeElement.scrollTop;
      $('.chatbox-popup__header').removeClass('bg-danger');
    }
  }
  ecriremessage() {
    if (this.msgtext == '') { return; }
    this.conversationPanel = document.getElementById('idscrl');
    var filenamechat = '';

    var urlfile = '';
    var checkmark_id = this.connection.userid + this.connection.token();
    this.appendChatMessage(this.msgtext, checkmark_id);
    if (this.fileToUploadChat == null) { filenamechat = '' }
    else { filenamechat = this.fileToUploadChat.name; urlfile = this.urlimg.concat(filenamechat); }

    this.connection.send({
      chatMessage: this.msgtext + '<br>' + filenamechat,
      checkmark_id: checkmark_id
    });
    $('.textchat').val('');
    this.msgtext = ''

  }

  appendChatMessage(event, checkmark_id) {
    var conversationPanel = document.getElementById('idscrl');

    var filenamechat = ''; var urlfile = '';
    if (this.fileToUploadChat == null) { filenamechat = '' }
    else {
      filenamechat = this.fileToUploadChat.name
      urlfile = this.urlimg.concat(filenamechat)
    }
    this.formchat.patchValue({ message: this.msgtext })
    const myFormValue = this.formchat.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    this.reunionService.postchatReunion(this.id, myFormData).pipe(first()).subscribe((res: any) => {
      if (res.statut === true) {
        this.labelImportchat.nativeElement.innerText = "";
        this.intitule_doc = null;
        this.fileToUploadChat = null;
        this.msgtext = ''
        // this.datachat = res.data

      } else {
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    })


    if (!$('.chatbox-open').hasClass('chat_open')) {
      $('.chatbox-open').addClass('btn-danger');
    }

    var div = document.createElement('div');
    div.className = 'chat-msg';
    div.id = 'cm-msg-1';
    var divchild = document.createElement('div');
    divchild.className = 'cm-msg-text';

    divchild.innerHTML = '<div style="clear: both;background:#055aa5!important; color: white;padding: 10px 15px 10px 15px;max-width: 75%;float: right;text-align:left; margin-left: -11px;position: relative;margin-bottom: 20px;border-radius: 30px;word-break: break-all;"><div><span style="color: white;font-weight: bold;float: left;">' + this.name_current_user + '</span>:<span style="font-size: x-small;float: right;color: #9c9a9a;margin-left: 5px;">' + new Date().toLocaleTimeString([], {
      hour12: false,
      hour: 'numeric',
      minute: 'numeric'
    }) + '</span><br><span class="font-weight-normal text-white">' + event + '<br></span>' +
      '<a style="color: #9c9a9a;font-weight:normal;cursor:pointer" href="' + urlfile + '" target="_blank">' + filenamechat
      + '</a></div>';

    div.appendChild(divchild)
    conversationPanel.appendChild(div);
    conversationPanel.scrollTop = conversationPanel.clientHeight;
    conversationPanel.scrollTop = conversationPanel.scrollHeight - conversationPanel.scrollTop;

  }
  onFileChangeChat(files: FileList) {
    this.labelImportchat.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUploadChat = files.item(0);
    if (files.length > 0) {
      const file = files[0];
      this.formchat.patchValue({
        attachement: file
      });
    }
    // this.msgtext = this.fileToUploadChat
  }

  muteall() {
    this.allAudio = true;
    /*********************REMOTE MUTEALL***********************/
    var socket = this.connection.socket;
    socket.emit(this.connection.socketCustomEvent, {
      messageFor: this.reunion?.cbcreateur?.id,
      message: 'remotemuteev',
      fullName: '',
      userid: ''
    });
    /*********************PROF MUTEALL***********************/
    const connection = this.connection;
    this.connection.getAllParticipants().forEach(function (pid) {
      
      var streamByUserId = connection.streamEvents.selectFirst({ userid: pid }).stream;
      console.log('wwwwwwww', streamByUserId)
      streamByUserId.mute();
    });
  }
  unmuteall() {
    this.allAudio = false;
    // $("#unmutebtn").addClass('disabled');
    // $("#mutebtn").removeClass('disabled');
    /*********************REMOTE MUTEALL***********************/
    var socket = this.connection.socket;
    socket.emit(this.connection.socketCustomEvent, {
      messageFor: this.reunion?.cbcreateur?.id,
      message: 'remoteunmuteev',
      fullName: '',
      userid: ''
    });
    /*********************PROF MUTEALL***********************/
    const connection = this.connection;
    this.connection.getAllParticipants().forEach(function (pid) {
      console.log('partoiii====>', pid)
      var streamByUserId = connection.streamEvents.selectFirst({ userid: pid }).stream;
      streamByUserId.unmute();
    });

  }

  quitter() {
    var testonerleave = true;

    // disconnect with all users
    this.connection.getAllParticipants().forEach(function (pid) {
      this.connection.disconnectWith(pid);
    });

    // stop all local cameras
    this.connection.attachStreams.forEach(function (localStream) {
      localStream.stop();
    });

    // close socket.io connection
    this.connection.closeSocket();

    // window.location.replace('{{ path('cour_onligne;') }}';
  }

  closewindow() {
    // this.btnmsg = !this.btnmsg
    $('header').on('click', function () {
      // $('.chatbox-popup').toggleClass('visuallyhidden');
      $('section').toggleClass('visuallyhidden');
    });

  }
  closeallroom() {
    this.connection.closeSocket()
    window.location.assign('/communite/detail-reunion/' + this.id)
    // this.router.navigate(['/communite/detail-reunion/',this.id])
  }

}


