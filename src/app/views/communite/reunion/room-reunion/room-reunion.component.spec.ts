import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomReunionComponent } from './room-reunion.component';

describe('RoomReunionComponent', () => {
  let component: RoomReunionComponent;
  let fixture: ComponentFixture<RoomReunionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomReunionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomReunionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
