import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Reunion } from 'src/app/shared/models/reunion.model';
import { ParametreService } from 'src/app/shared/services/parametre.service';
import { ReunionService } from 'src/app/shared/services/reunion.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
declare var RTCMultiConnection;
import * as $ from 'jquery';
import { first } from 'rxjs/operators';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-mobileroom-reunion',
  templateUrl: './mobileroom-reunion.component.html',
  styleUrls: ['./../room-reunion.component.scss'],
  providers: [DatePipe],
  animations: [
    trigger(
      'enterAnimation', [
      transition(':enter', [
        // style({ transform: 'opacity 0.3s linear 2s;', opacity: 0 }),
        animate('300ms', style({ transform: 'opacity 0.3s linear 2s;', opacity: 1 }))
      ]),
      transition(':leave', [
        // style({ transform: 'translateX(0)', opacity: 1 }),
        animate('300ms', style({ transform: 'opacity 0.3s linear 2s;', opacity: 0 }))
      ])
    ]
    ),
    trigger(
      'rightbar', [
      transition(':enter', [
        style({ transform: 'translateX(250px)', opacity: 0 }),
        animate('300ms', style({ transform: 'opacity 0.3s linear 2s;', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'opacity 0.3s linear 2s;', opacity: 1 }),
        animate('300ms', style({ transform: 'opacity 0.3s linear 2s;', right: '0px', opacity: 0 }))
      ])
    ]
    ),
    trigger('openClose', [
      state('true', style({ height: '*', transform: 'opacity 0.3s linear 2s;', opacity: 1 })),
      state('false', style({ height: '0px', transform: 'all 0.3s linear 2s;', opacity: 0 })),
      transition('false <=> true', [animate(500)])
    ]),
    trigger('showparticipant', [
      state('true', style({ width: '*', transform: 'opacity 0.3s linear 2s;', opacity: 1 })),
      state('false', style({ width: '0px', transform: 'all 0.3s linear 2s;', opacity: 0 })),
      transition('false <=> true', [animate(500)])
    ]),
  ]
})
export class MobileroomReunionComponent implements OnInit {
  id: any;
  timerduration: any;
  videoconference: boolean;
  allparticipants: any;
  typereunion: string;
  listParticipants: any = []
  listnotegenerale: any = []
  id_current_user: any;
  reunion: Reunion = new Reunion();
  listParticipant: any[] = [];
  participantspresents: any[] = [];
  participantsnonpresents: any[] = [];
  connection: any;

  urlimg = 'http://syndic.gloulougroupe.com/uploads/documents/'
  notedetails: any;
  contentModal: any[];
  conversationPanel: any
  @ViewChild('targetscroll') myScrollContainer: ElementRef;
  msgtext: string;
  formchat: FormGroup
  name_current_user
  allAudio
  modemobile: boolean = true
  shownotes: boolean = false
  showlist: boolean = false
  minimizewindow: boolean = true
  maximizewindow: boolean = true
  fileToUploadChat: File = null;
  @ViewChild('labelImportchat')
  labelImportchat: ElementRef;
  defaultLogo = "/assets/images/faces/2.jpg";
  @ViewChild('videocontainer') videocontainer: ElementRef;
  intitule_doc: any;
  clickshow: boolean = false;
  btnmsg: boolean = false;
  state: string = 'default';
  isdarkMode: boolean = true;
  details: any;
  societe: any;
  societeObj: any
  datachat: any;
  role: any;
  formvote: FormGroup
  isVoting: boolean = false;
  today: any;
  muteownaudio: boolean = false;
  muteownvideo: boolean = false;
  constructor(private modalService: NgbModal, private fb: FormBuilder,
    private actRoute: ActivatedRoute, private reunionService: ReunionService,
    private router: Router, private bottomSheet: MatBottomSheet,
    private datepipe: DatePipe, private spinner: NgxSpinnerService, private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService, private paramSociete: ParametreService, private userService: UtilisateurService,
  ) {

  }

  ngOnInit(): void {
    this.today = this.datepipe.transform(new Date(), 'dd/MM/yyyy')
    this.modemobile = true
    this.id_current_user = localStorage.getItem('id');
    this.name_current_user = localStorage.getItem('username');
    this.role = localStorage.getItem('role')
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
    });
    this.getDetailsUser(this.id_current_user)
    if (this.id) {
      this.getReunion();
      // this.OpenRoomMaster()
      this.reunionService.getchatReunion(this.id).pipe(first()).subscribe((res: any) => {
        if (res.statut === true) {
          this.datachat = res.data
        }
      })
      var video: any = document.getElementById("main-video");
      // document.getElementById("video").controls=false;

    }
    this.formvote = this.fb.group({
      oui: new FormControl(0, Validators.required),
      non: new FormControl(0, Validators.required),
      neutre: new FormControl(0, Validators.required),
    });
    this.formchat = this.fb.group({
      message: new FormControl(''),
      attachement: new FormControl(''),
    })
  }
  getDetailsUser(id) {
    this.spinner.show();
    this.userService.getuserbyId(id).subscribe((admin: any) => {
      this.details = admin.data

    })
  }
  showdetail() {
    this.clickshow = !this.clickshow
    // this.state === 'default' ? this.state = 'open' : this.state = 'default';
  }
  getReunion() {
    this.spinner.show();
    this.reunionService.getReunion(this.id).subscribe((x: any) => {
      if (x.data) {

        console.log('reunion', x.data)
        this.timerduration = x.data.DureeVote
        this.participantspointes(this.id)
        this.reunion = x.data;
        if (x?.data?.enligne) {
          this.videoconference = true
          this.OpenRoomMaster();
        }
        else {
          this.videoconference = false
        }
        this.allparticipants = x?.data?.proprietaire?.map((el) => { el.photo = this.defaultLogo; return el })
        if (this.reunion?.type?.code == "E0025")
          this.typereunion = 'Extraordinaire'
        if (this.reunion?.type?.code == "E0024")
          this.typereunion = 'Ordinaire'
        if (this.reunion?.type?.code == "E0091")
          this.typereunion = 'Délégué'
        Object(x?.data?.biens).forEach(name => {
          if (name?.appartement !== null && name.bloc !== null && name.residence !== null) {
            this.listParticipants.push(name.residence?.intitule + '|' + name.bloc?.intitule + '|' + name.appartement?.intitule)
          }
          if (name.bloc !== null && name.appartement === null && name.residence !== null)
            this.listParticipants.push(name.residence?.intitule + '|' + name.bloc?.intitule)
          if (name.appartement === null && name.residence !== null && name.bloc === null)
            this.listParticipants.push(name.residence?.intitule)
          if (name.groupement !== null && name.residence === null)
            this.listParticipants.push(name.groupement?.intitule)
        })
        this.spinner.hide();
      }
      this.spinner.hide();
    }
      , error => {
        this.spinner.hide();
      }
    );
  }
  participantspointes(cbmarq) {
    this.spinner.show();
    this.reunionService.getpresnecespointage(cbmarq).subscribe((res: any) => {
      this.participantspresents = res.data.pointage
      this.participantsnonpresents = res.data.dépointage
      this.allparticipants = res.data.pointage.concat(res.data.dépointage)
      this.spinner.hide();
      this.allparticipants = this.allparticipants.reduce((accumalator, current) => {
        if (!accumalator.some((item) => item.user?.id === current.user?.id)) {
          accumalator.push(current);
        }
        return accumalator;
      }, []);
    })

  }
  onChangeMode() {
    this.isdarkMode = !this.isdarkMode;
  }

  OpenRoomMaster() {

    var roomid = 'REU' + this.id;
    var idReunion = this.id;
    var fullName = localStorage.getItem('username')
    var roomPassword = 'REU' + this.id;
    var roleUser = localStorage.getItem('role')
    var createurId = parseInt(this.reunion?.cbcreateur?.id);
    var CoonectUserId = parseInt(this.id_current_user);
    var testonerleave = false;
    var urlfile = this.urlimg;
    var notedetails = this.notedetails
    var conteent: any[] = this.contentModal
    var timeduree = this.timerduration * 1000
    var params = {
      'userFullName': fullName,
      'userlogo': this.defaultLogo,
      'publicRoomIdentifier': roomid,
      'open': true,
      'password': roomPassword,
      'sessionid': roomid,
    }
    const connection = new RTCMultiConnection();
    // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
    connection.socketURL = 'https://muazkhan.com:9001/';
    connection.socketMessageEvent = 'AXIA-message';
    this.connection = connection;
    console.log('connn========', connection)
  
    if (parseInt(this.id_current_user) === parseInt(this.reunion?.cbcreateur?.id)) {
      this.connection.session = {
        data: true, // at least data-connection must open if he do not have camera+mic
        video: true,
        audio: true
      };
    } else {
      this.connection.session = {
        audio: true,
        data: true  // at least data-connection must open if he do not have camera+mic
      };
    }

    if (!!params.password) {
      this.connection.password = params.password;
    }

    this.connection.extra.userFullName = params.userFullName;
    this.connection.publicRoomIdentifier = params.publicRoomIdentifier;
    this.connection.socketMessageEvent = 'canvas-dashboard-demo';
    // keep room opened even if owner leaves
    this.connection.autoCloseEntireSession = true;
    this.connection.maxParticipantsAllowed = 1000;
    this.connection.chunkSize = 15 * 1000; // using 15k
    this.connection.enableFileSharing = true;

    connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };

    this.connection.DetectRTC.load(function () {
      if (connection.DetectRTC.hasMicrophone === true) {
        // enable microphone
        connection.mediaConstraints = {
          audio: {
            echoCancellation: { exact: true },
            googEchoCancellation: { exact: true },
            googAutoGainControl: { exact: true },
            googNoiseSuppression: { exact: true },
          }
        };
        // connection.mediaConstraints.audio = true;
        // connection.session.audio = true;
      } else {
        connection.mediaConstraints.audio = false;
        connection.session.audio = false;
      }
      if (connection.DetectRTC.hasWebcam === true) {
        if (createurId === CoonectUserId) {
          // enable camera
          connection.mediaConstraints.video = true;
          connection.session.video = true;
        } else {
          connection.mediaConstraints.video = false;
          connection.session.video = false;
        }
      } else {
        connection.mediaConstraints.video = false;
        connection.session.video = false;
      }
    });
    //panel discussion
    this.conversationPanel = document.getElementById('idscrl');
    var panel = this.conversationPanel
    this.myScrollContainer.nativeElement.scroll({
      top: this.myScrollContainer.nativeElement.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
    // panel.scrollTop = panel.clientHeight;
    // panel.scrollTop = panel.scrollHeight - panel.scrollTop;


    this.connection.onclose = this.connection.onerror = this.connection.onleave = function (event) {
      // this.connection.onUserStatusChanged(event);
      connection.onUserStatusChanged(event);
      if (event.extra.roomOwner === true) {
        window.location.href = '/communite/detail-reunion/' + idReunion;
      }
    };
    var modalservice = this.modalService

    this.connection.onmessage = function (event) {

      var conversationPanel = document.getElementById('globalcontainer');
      const urlattachement = urlfile

      if (event.data.chatMessage.includes("modalvote")) {
        var div = document.createElement('div');
        var titre = document.getElementById('exampleModalLabel');
        var divcontent = document.getElementById('modalmodalvote');
        divcontent.style.visibility = 'visible';
        var btnoui = document.getElementById('oui')
        var btnnon = document.getElementById('non')
        var btnneutre = document.getElementById('neutre')
        var cbmarqnote = event.data.chatMessage.slice(event.data.chatMessage.indexOf('modalvote') + 'modalvote'.length);
        titre.innerHTML = 'Vote sur la note :<br><span style="font-size: 12px; font-weight: normal;">' + event.data.chatdata + ' </span>'

        btnoui.onclick = function (e) {
          voteauto(idReunion, cbmarqnote, 1, event)
        }
        btnnon.onclick = function (b) {
          voteauto(idReunion, cbmarqnote, 2, event)
        }
        btnneutre.onclick = function (c) {
          voteauto(idReunion, cbmarqnote, 0, event)
        }

        divcontent.appendChild(div)

        setTimeout(() => {
          if (createurId != CoonectUserId) {
            window.location.reload();
          }
        }, timeduree);

      }


      else {
        if (event.data.chatMessage) {
          if ($('.chatbox-open').hasClass('chat_open') == false) {
            $('.chatbox-open').addClass('bg-danger');
          } else {
            $('.chatbox-open').removeClass('bg-danger');
            $('.chatbox-popup__header').addClass('bg-danger');
          }

          var div = document.createElement('div');
          div.className = 'chat-msg';
          div.id = 'cm-msg-1';
          var divchild = document.createElement('div');
          divchild.className = 'cm-msg-text';
          var span = document.createElement('span');
          span.className = 'cm-msg-text';
          if (event.data) {
            var Z = event.data.chatMessage.slice(event.data.chatMessage.indexOf('<br>') + '<br>'.length);
            // (click)="' + this.openAttachement(urlattachement) + '"
            div.innerHTML = '<div style="clear: both;position: relative;float: left;text-align:justify;background: #c2c2c254!important; color: #212020;padding: 10px 15px 10px 15px;max-width: 75%;text-align:left; margin-left: -11px;margin-bottom: 20px;border-radius: 30px;word-break: break-all;"><b><span style="color: #055aa5;font-weight: bold;float: left;">' + event.extra.userFullName + '</span>:<span style="font-size: x-small;float: right;color: #9c9a9a;margin-left: 5px;">' + new Date().toLocaleTimeString([], { hour12: false, hour: "numeric", minute: "numeric" }) + '</span> </br><a style="cursor:pointer" href="' + urlfile.concat(Z) + '" target="_blank" class="font-weight-normal text-dark">' + event.data.chatMessage + '</a></div>';
            if (event.data.checkmark_id) {
              connection.send({
                checkmark: 'received',
                checkmark_id: event.data.checkmark_id
              });
            }
          } else {
            divchild.innerHTML = '<b>You:</b>-------- <img class="checkmark" id="' + event.data.checkmark_id + '" title="Received" src="https://www.webrtc-experiment.com/images/checkmark.png"><br>' + event;
            divchild.style.background = '#cbffcb';
          }
          setTimeout(() => {
            var conversationPanelx2 = document.getElementById('idscrl');
            // div.appendChild(divchild)
            conversationPanelx2.appendChild(div);
            conversationPanelx2.scrollTop = conversationPanelx2.clientHeight;
            conversationPanelx2.scrollTop = conversationPanelx2.scrollHeight - conversationPanelx2.scrollTop;
          }, 500);
        }
      }

      if (event.data.checkmark === 'received') {
        var checkmarkElement = document.getElementById(event.data.checkmark_id);
        if (checkmarkElement) {
          checkmarkElement.style.display = 'inline';
        }
        return;
      }

    }

    connection.onUserStatusChanged = function (event) {
      var names = [];
      $(".fa-user-circle-o").removeClass('text-success').addClass('text-danger');
      $(".participt").removeClass('text-success').addClass('text-danger');

      connection.getAllParticipants().forEach(function (pid) {
        var user = connection.peers[pid];
        names.push([user?.extra?.usercbmarq, getFullName(pid), user?.extra?.logo]);

        $(".part" + user[0]).removeClass('text-danger').addClass('text-success');
        $(".particip" + user[0]).removeClass('text-danger').addClass('text-success');
      });

      names.forEach(function (user) {
        $(".part" + user[0]).removeClass('text-danger').addClass('text-success');
        // $(".parti" + user[0]).removeClass('bg-danger').addClass('bg-success');
        $(".particip" + user[0]).removeClass('text-danger').addClass('text-success');
      });
      if (createurId !== CoonectUserId) {
        $(".part" + connection.extra.usercbmarq).removeClass('text-danger').addClass('text-success');
        $(".particip" + connection.extra.usercbmarq).removeClass('text-danger').addClass('text-success');
      }
    }
    connection.onopen = function (event) {
      connection.onUserStatusChanged(event);
    };

    this.connection.onstream = function (event) {
      event.mediaElement.controls = true;
      if (event.stream.isScreen && !event.stream.canvasStream) {

        var div = document.getElementById('video-container-remote') as HTMLVideoElement;
        div.srcObject = event.stream;
        // $('#video-container-remote').get(0).srcObject = event.stream;
        $('#video-container-remote').hide();
      } else if (event.extra.roomOwner === true) {
        var video = document.getElementById('main-video') as HTMLVideoElement;
        video.setAttribute('data-streamid', event.streamid);
        if (event.type === 'local') {
          video.muted = false;
          video.volume = 0.5;
        }
        video.srcObject = event.stream;
        $('#main-video').show();

      } else {
      }
      connection.onUserStatusChanged(event);

    };


    window.onkeyup = function (e) {
      var filechatname = this.fileToUploadChat;
      var filechat = this.labelImportchat;
      var code = e.keyCode || e.which;
      if (code == 13) {
        filechatname = null
        filechat = null
        $('#btn-chat-message').click();
      }
    };
    console.log('params========>', params)
    if (params.open === true) {
      this.connection.extra.usercbmarq = localStorage.getItem('id');
      this.connection.extra.userOwner = false;
      this.connection.extra.logo = this.defaultLogo;
      this.connection.extra.nom = params.userFullName;

      if (parseInt(this.id_current_user) === parseInt(this.reunion?.cbcreateur?.id)) {
        this.connection.extra.roomOwner = true;
        this.connection.extra.userOwner = true;
      }
      this.connection.openOrJoin(params.sessionid, function (isRoomOpened, roomid, error) {
        if (error) {
          if (error === connection.errors.ROOM_NOT_AVAILABLE) {
            return;
          }
          console.log(error);
        }
      });
    } else {
      this.connection.join(params.sessionid, function (isRoomJoined, roomid, error) {
        if (error) {
          if (error === this.connection.errors.ROOM_NOT_AVAILABLE) {
            alert('This room does not exist. Please either create it or wait for moderator to enter in the room.');
            return;
          }
          if (error === this.connection.errors.ROOM_FULL) {
            alert('Room is full.');
            return;
          }
          if (error === this.connection.errors.INVALID_PASSWORD) {
            this.connection.password = prompt('Please enter room password.') || '';
            if (!this.connection.password.length) {
              alert('Invalid password.');
              return;
            }
            this.connection.join(params.sessionid, function (isRoomJoined, roomid, error) {
              if (error) {

              }
            });
            return;
          }
        }
      });
    }
    var detailreunion = this.reunion
    var servicereunion = this.reunionService


    var toastr = this.toastr
    function voteauto(id, note, vote, event) {
      if (vote == 0) {
        $('.non').removeClass('bgnonactive').addClass('nonactive');
        $('.oui').removeClass('bgouiactive').addClass('ouiactive');
        $('.neutre').removeClass('neutreactive').addClass('bgneutreactive');
      }
      if (vote == 1) {
        $('.non').removeClass('bgnonactive').addClass('nonactive');
        $('.oui').addClass('bgouiactive').removeClass('ouiactive');
        $('.neutre').removeClass('bgneutreactive').addClass('neutreactive');
      }
      if (vote == 2) {

        $('.non').addClass('bgnonactive').removeClass('nonactive');
        $('.oui').removeClass('bgouiactive').addClass('ouiactive');
        $('.neutre').removeClass('bgneutreactive').addClass('neutreactive');
      }
      //action vote
      if (detailreunion.mode == 2) {
        servicereunion.voterautoReunion(id, note, { vote: vote }).subscribe((res: any) => {
          if (res.statut == false) {
            toastr.error(res.message)
          }
          else {
            toastr.success(res.message)
          }
        })
      }
    }
    function getFullName(userid) {
      var _userFullName = userid;
      if (connection.peers[userid] && connection.peers[userid].extra.userFullName) {
        _userFullName = connection.peers[userid].extra.userFullName;
      }
      return _userFullName;
    }
    this.connection.socketCustomEvent = params.sessionid;
    connection.connectSocket(function (socket) {
      // listen for custom messaging event
      socket.on(connection.socketCustomEvent, function (event) {
        // check if someone sent you a custom message
        if (event.message === 'remotemuteev') {
          connection.socket.on('remotemute', function () {
            connection.attachStreams[0].mute('audio');
            // if (data.video) connection.attachStreams[0].mute('video');
          });
        }
        if (event.message === 'remoteunmuteev') {
          connection.socket.on('remoteunmute', function () {
            connection.attachStreams[0].unmute('audio');
            // if (data.video) connection.attachStreams[0].mute('video');
          });
        }
        if (event.message === 'remoteunmutepart') {
          connection.socket.on('remoteunmute', function () {
            var streamByUserId = connection.streamEvents.selectFirst({ userid: event.userid }).stream;
            streamByUserId.unmute();
            // connection.attachStreams[0].unmute('audio');

          });
        }
        if (event.message === 'remotemutepart') {
          connection.socket.on('remotemute', function () {
            var streamByUserId = connection.streamEvents.selectFirst({ userid: event.userid }).stream;
            streamByUserId.mute();
            // connection.attachStreams[0].unmute('audio');

          });
        }
        if (event.message === 'voteremote') {
          connection.socket.on('voteremote', function () {
            // var streamByUserId = connection.streamEvents.selectFirst({ userid: event.userid }).stream;

            // console.log('streamByUserId', streamByUserId)
          });
        }

      });
    });
  }

  openpanel() {
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.clientHeight;
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.myScrollContainer.nativeElement.scrollTop;

    this.btnmsg = !this.btnmsg;
    if (this.btnmsg) {
      document.getElementById('mytextarea').focus()
      $('.chatbox-open').removeClass('bg-danger');
    }
    this.myScrollContainer.nativeElement.click()
  }
  closeVote() {
    document.getElementById('modalmodalvote').style.visibility = 'hidden'
  }
  getnotes() {
    this.reunionService.getReunion(this.id).subscribe((res: any) => {
      if (res.statut)
        console.log(this.reunion)
      this.reunion = res.data
    })
  }


  onpanelopened() {
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.clientHeight;
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.myScrollContainer.nativeElement.scrollTop;
  }
  textfocus() {
    if ($("#mytextarea").is(":focus")) {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.clientHeight;
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.myScrollContainer.nativeElement.scrollTop;
      $('.chatbox-popup__header').removeClass('bg-danger');
    }
  }
  ecriremessage() {
    if (this.msgtext == '') { return; }
    this.conversationPanel = document.getElementById('idscrl');
    var filenamechat = '';
    var urlfile = '';
    var checkmark_id = this.connection.userid + this.connection.token();
    this.appendChatMessage(this.msgtext, checkmark_id);
    if (this.fileToUploadChat == null) { filenamechat = '' }
    else { filenamechat = this.fileToUploadChat.name; urlfile = this.urlimg.concat(filenamechat); }

    this.connection.send({
      chatMessage: this.msgtext + '<br>' + filenamechat,
      checkmark_id: checkmark_id
    });
    $('.textchat').val('');

  }

  appendChatMessage(event, checkmark_id) {
    var conversationPanel = document.getElementById('idscrl');

    var filenamechat = ''; var urlfile = '';
    if (this.fileToUploadChat == null) { filenamechat = '' }
    else {
      filenamechat = this.fileToUploadChat.name
      urlfile = this.urlimg.concat(filenamechat)
    }
    this.formchat.patchValue({ message: this.msgtext })
    const myFormValue = this.formchat.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    this.reunionService.postchatReunion(this.id, myFormData).pipe(first()).subscribe((res: any) => {
      if (res.statut === true) {
        this.labelImportchat.nativeElement.innerText = "";
        this.intitule_doc = null;
        this.fileToUploadChat = null;
        // this.datachat = res.data

      } else {
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    })


    if (!$('.chatbox-open').hasClass('chat_open')) {
      $('.chatbox-open').addClass('btn-danger');
    }

    var div = document.createElement('div');
    div.className = 'chat-msg';
    div.id = 'cm-msg-1';
    var divchild = document.createElement('div');
    divchild.className = 'cm-msg-text';

    divchild.innerHTML = '<div style="clear: both;background:#055aa5!important; color: white;padding: 10px 15px 10px 15px;max-width: 75%;float: right;text-align:left; margin-left: -11px;position: relative;margin-bottom: 20px;border-radius: 30px;word-break: break-all;"><div><span style="color: white;font-weight: bold;float: left;">' + this.name_current_user + '</span>:<span style="font-size: x-small;float: right;color: #9c9a9a;margin-left: 5px;">' + new Date().toLocaleTimeString([], {
      hour12: false,
      hour: 'numeric',
      minute: 'numeric'
    }) + '</span><br><span class="font-weight-normal text-white">' + event + '<br></span>' +
      '<a style="color: #9c9a9a;font-weight:normal;cursor:pointer" href="' + urlfile + '" target="_blank">' + filenamechat
      + '</a></div>';

    div.appendChild(divchild)
    conversationPanel.appendChild(div);
    conversationPanel.scrollTop = conversationPanel.clientHeight;
    conversationPanel.scrollTop = conversationPanel.scrollHeight - conversationPanel.scrollTop;

  }
  onFileChangeChat(files: FileList) {
    this.labelImportchat.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUploadChat = files.item(0);
    if (files.length > 0) {
      const file = files[0];
      this.formchat.patchValue({
        attachement: file
      });
    }
    // this.msgtext = this.fileToUploadChat
  }
  muteparticip(ppid) {
    var socket = this.connection.socket;
    socket.emit(this.connection.socketCustomEvent, {
      messageFor: this.reunion?.cbcreateur?.id,
      message: 'remotemutepart',
      fullName: '',
      userid: ''
    });
    const connection = this.connection;

    this.connection.getAllParticipants().forEach(function (pid) {
      // if(ppid==pid)
      var streamByUserId = connection.streamEvents.selectFirst({ userid: parseFloat(pid) }).stream;
      streamByUserId.mute();
    })
  }
  unmuteparticip(pid) {
    var socket = this.connection.socket;
    socket.emit(this.connection.socketCustomEvent, {
      messageFor: this.reunion?.cbcreateur?.id,
      message: 'remoteunmutepart',
      fullName: '',
      userid: ''
    });
    const connection = this.connection;
    this.connection.getAllParticipants().forEach(function (pid) {
      var streamByUserId = connection.streamEvents.selectFirst({ userid: pid }).stream;
      streamByUserId.unmute();
    })
  }
  muteall() {
    this.allAudio = true;
    /*********************REMOTE MUTEALL***********************/
    var socket = this.connection.socket;
    socket.emit(this.connection.socketCustomEvent, {
      messageFor: this.reunion?.cbcreateur?.id,
      message: 'remotemuteev',
      fullName: '',
      userid: ''
    });
    /*********************PROF MUTEALL***********************/
    const connection = this.connection;
    this.connection.getAllParticipants().forEach(function (pid) {
      var streamByUserId = connection.streamEvents.selectFirst({ userid: pid }).stream;
      streamByUserId.mute();
    });
  }
  unmuteall() {
    this.allAudio = false;
    // $("#unmutebtn").addClass('disabled');
    // $("#mutebtn").removeClass('disabled');
    /*********************REMOTE MUTEALL***********************/
    var socket = this.connection.socket;
    socket.emit(this.connection.socketCustomEvent, {
      messageFor: this.reunion?.cbcreateur?.id,
      message: 'remoteunmuteev',
      fullName: '',
      userid: ''
    });
    /*********************PROF MUTEALL***********************/
    const connection = this.connection;
    this.connection.getAllParticipants().forEach(function (pid) {
      var streamByUserId = connection.streamEvents.selectFirst({ userid: pid }).stream;
      streamByUserId.unmute();
    });

  }

  quitter() {
    var testonerleave = true;

    // disconnect with all users
    this.connection.getAllParticipants().forEach(function (pid) {
      this.connection.disconnectWith(pid);
    });

    // stop all local cameras
    this.connection.attachStreams.forEach(function (localStream) {
      localStream.stop();
    });

    // close socket.io connection
    this.connection.closeSocket();

    // window.location.replace('{{ path('cour_onligne;') }}';
  }

  closewindow() {
    // this.btnmsg = !this.btnmsg
    $('header').on('click', function () {
      // $('.chatbox-popup').toggleClass('visuallyhidden');
      $('section').toggleClass('visuallyhidden');
    });

  }
  closevideo() {
    // this.muteownvideo = !this.muteownvideo
    // var connection = this.connection

    // if (this.muteownaudio == true) {
    //   connection.attachStreams[0].mute('video');
    // }
    // if (this.muteownaudio == false) {
    //   connection.attachStreams[0].unmute('video');
    // }

  }
  closeallroom() {
    this.connection.closeSocket()
    window.location.assign('/dashboard')
  }
  mutemyaudio() {
    var connection = this.connection
    this.muteownaudio = !this.muteownaudio

    if (this.muteownaudio == true) {
      connection.attachStreams[0].mute('audio');
    }
    if (this.muteownaudio == false) {
      connection.attachStreams[0].unmute('audio');
    }

  }
}


