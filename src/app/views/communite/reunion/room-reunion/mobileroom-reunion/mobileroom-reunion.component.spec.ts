import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileroomReunionComponent } from './mobileroom-reunion.component';

describe('MobileroomReunionComponent', () => {
  let component: MobileroomReunionComponent;
  let fixture: ComponentFixture<MobileroomReunionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileroomReunionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileroomReunionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
