import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { TreeNode } from 'primeng/api';
import { AddDocument } from 'src/app/shared/models/add.document.model';
import { CategorieGetDoc } from 'src/app/shared/models/categorieGetDoc';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';

@Component({
  selector: 'app-info-document',
  templateUrl: './info-document.component.html',
  styleUrls: ['./info-document.component.scss']
})
export class InfoDocumentComponent implements OnInit {


  data: any;
  id: any = 0;
  id_current_user: any;
  residences: any[];
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['Intitule', 'Date création', 'Date modification', 'action'];

  files1: TreeNode[];
  selectedNode: any;
  isNodeSelected: boolean = false;;
  isViewChanged: boolean = false;
  submitted: boolean;
  formBasic: FormGroup;
  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File = null;
  intitule_doc: any;
  addDocumentModel: AddDocument = new AddDocument();
  fileName: string;
  nodeKey: string;
  categorieGetDocModel: CategorieGetDoc = new CategorieGetDoc();
  routeState: any;

  have_access: boolean = false;
  searchProduit: any;


  constructor(private actualiteService: ActualiteService, private toastr: ToastrService,
    private spinnerService: NgxSpinnerService, private fb: FormBuilder, private actRoute: ActivatedRoute,
    private permissionservice: DroitAccesService, private router: Router, private cdRef: ChangeDetectorRef) {


    this.id_current_user = localStorage.getItem('id');
    if (this.router.getCurrentNavigation().extras.state) {
      this.routeState = this.router.getCurrentNavigation().extras.state;
      if (this.routeState) {
        this.categorieGetDocModel = this.routeState.categorie ? JSON.parse(this.routeState.categorie) : '';
        this.addDocumentModel.produit = this.categorieGetDocModel.produit;
        this.addDocumentModel.bloc = this.categorieGetDocModel.bloc;
        this.addDocumentModel.bien = this.categorieGetDocModel.bien;
        this.addDocumentModel.destinationSelected = this.categorieGetDocModel.label;

        if (this.categorieGetDocModel.label != null && this.categorieGetDocModel.label != undefined && this.categorieGetDocModel.label != "") {
          this.isNodeSelected = true;
        }

           ////console.log("addDocumentModel: ",this.addDocumentModel)

        }
      }


      ////console.log("this.categorieGetDocModel : ",this.categorieGetDocModel)


    }

  ngOnInit(): void {
    this.spinnerService.show()
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
      if (this.id != 0) {
        this.checkAccessUpdate();
      } else {
        this.checkAccessAdd();
      }

    });
    this.listeresidence();
    this.buildFormBasic();


  }

  ngAfterViewChecked()
{
  this.cdRef.detectChanges();
}

  checkAccessAdd(){

     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000113').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }
    });

  }
  checkAccessUpdate(){

     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000111').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getDocumentById(this.id);
      }
    });

  }

  /************************************-------Bloc Formulaire-------***************************************/
  buildFormBasic() {
    this.formBasic = this.fb.group({     
      intitule: new FormControl(null, Validators.required),    
      attachement: new FormControl(null, Validators.required),  
    });
  }

onFileChange(files: FileList) {
  this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
  this.fileToUpload = files.item(0);

}

cancelInformation(){
  this.fileToUpload=null;
  this.labelImport.nativeElement.innerText = Array.from([])
  .map(f => f.name)
  .join(', ');

  this.formBasic.reset();

}

submit(){
  this.submitted=true;
 // //console.log("submit doc: ",this.formBasic);

  

 this.addDocumentModel.intitule=this.formBasic.value.intitule;
 
 this.addDocumentModel.publier=1;
 


 
/*---------- Ajout -------------*/
 if(this.id==0){

  this.addDocumentModel.attachement=this.fileToUpload;

  if(this.formBasic.invalid){
    return;
  }

  //file on add

  const myFormValue = this.addDocumentModel;
 const myFormData = new FormData();
 Object.keys(myFormValue).forEach(name => {
   myFormData.append(name, myFormValue[name]);
 });

      ////console.log("DocumentModel on ajout: ",myFormData)

      this.actualiteService.addDocument(myFormData).subscribe((res: any) => {
        if (res.statut === true) {
          this.toastr.success('Document a été ajouté avec succès !');
          this.backToList();
        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
        error => {
          this.toastr.error('Veuillez réessayer plus tard!');
        })

 }
 /*---------- Modification -------------*/
 else{

  if(this.fileToUpload!=null){
    this.addDocumentModel.attachement=this.fileToUpload; //file on update

  }

  ////console.log("DocumentModel on update: ",this.addDocumentModel)
  if(this.addDocumentModel.intitule=="" && this.addDocumentModel.attachement==null){
    return;
  }


  ////console.log("fileToUpload doc..",this.addDocumentModel.attachement);
  ////console.log("ajout doc..",this.addDocumentModel);


  const myFormValue = this.addDocumentModel;
 const myFormData = new FormData();
 Object.keys(myFormValue).forEach(name => {
   myFormData.append(name, myFormValue[name]);
 });

  ////console.log("submit model on update: ",myFormData)


  
  this.actualiteService.editDocument( this.addDocumentModel.cbmarq,myFormData).subscribe((res:any)=>{
    if (res.statut === true) {
      this.toastr.success('Document modifié avec succès !');
      this.backToList();
    } else {
      this.toastr.error(res.message, 'Erreur!', {progressBar: true});
    }
  },
  error => {
    this.toastr.error('Veuillez réessayer plus tard!');
  })
 }
   


 

}


/****************************************/

  
nodeSelect(event) {

  this.selectedNode=event.node;
  this.isNodeSelected=true;
 // //console.log("node selected: ",event.node)
 this.addDocumentModel.destinationSelected=this.selectedNode.label;
 /*** set categorieGetDocModel  ***/
 this.categorieGetDocModel.label=event?.node?.label;

 if(event.node.key=="1"){
   this.categorieGetDocModel.produit=+event.node.data;
   this.categorieGetDocModel.bloc=0;
   this.categorieGetDocModel.bien=0;
 }
 if(event.node.key=="2"){
   this.categorieGetDocModel.produit=0;
   this.categorieGetDocModel.bloc=+event.node.data;
   this.categorieGetDocModel.bien=0;
 }
 if(event.node.key=="3"){
   this.categorieGetDocModel.produit=0;
   this.categorieGetDocModel.bloc=0;
   this.categorieGetDocModel.bien=+event.node.data;
 }


 /********************** */


 if(this.id==0 && this.selectedNode!=null && this.selectedNode.key=="1"){
  this.addDocumentModel.produit=this.selectedNode.data;
  this.addDocumentModel.bloc=0;
  this.addDocumentModel.bien=0;

}
 if(this.id==0 && this.selectedNode!=null && this.selectedNode.key=="2"){
  this.addDocumentModel.bloc=this.selectedNode.data;
  this.addDocumentModel.produit=0;
  this.addDocumentModel.bien=0;

}
 if(this.id==0 && this.selectedNode!=null && this.selectedNode.key=="3"){
  this.addDocumentModel.bloc=0;
  this.addDocumentModel.bien=this.selectedNode.data;
  this.addDocumentModel.produit=0;

}


}


  listeresidence() {
    this.spinnerService.show()
    this.actualiteService.getResidenceNode().subscribe((res: any) => {
      // //console.log("listeresidence: ",res.data)
      this.files1 = res.data;
      this.spinnerService.hide()
    },
      error => {
        //console.log( 'erreur: ', error);
      }, () => {

      }
    );



  }

getDocumentById(idDoc){

  this.actualiteService.getDocumentById(idDoc).subscribe((res:any)=>{

   // //console.log("documentmodel: ",res.data);
    this.addDocumentModel=res.data;
  },error=>{},()=>{
    // extract filename of link //
    let file:string="'"+this.addDocumentModel.attachement;
    this.fileName = file.substring(file.lastIndexOf('/')+1);
    ////console.log("get fileName: ", this.fileName)

    
  this.labelImport.nativeElement.innerText= this.fileName;  
  if(this.fileName!=""){
    this.formBasic.controls['attachement'].setErrors(null);

  }


  });
 

}

backToList(){
  let navigationExtras: NavigationExtras = {
    state: {
      categorie: JSON.stringify (this.categorieGetDocModel )
    }
  }

  ////console.log("Info Doc backToList nav extra : ",navigationExtras)


  this.router.navigate(['/communite/documents'],navigationExtras);


}

filterProduit(){

  let auxList=this.files1;

  if(this.searchProduit!=""){

    this.files1 = auxList.filter(word=>(
     ( word?.label.trim().toLowerCase().includes(this.searchProduit.trim().toLowerCase())

     )
    ))

   
  }


// this prints the array of filtered

  if(this.searchProduit==""){
    this.listeresidence();
  }
}

}
