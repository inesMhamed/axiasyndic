import { Component, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NavigationExtras, Router } from '@angular/router';
import { number } from 'ngx-custom-validators/src/app/number/validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { TreeNode } from 'primeng/api';
import { CategorieGetDoc } from 'src/app/shared/models/categorieGetDoc';
import { Permission } from 'src/app/shared/models/permission.model';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-list-document',
  templateUrl: './list-document.component.html',
  styleUrls: ['./list-document.component.scss']
})
export class ListDocumentComponent implements OnInit {

  data: any;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;
  residences: any[];
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['Intitule', 'Date création', 'Date modification', 'action'];
  displayedColumnsCard: string[] = ['Intitule', 'action'];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;

  files1: TreeNode[];
  files2: TreeNode[];
  selectedNode: TreeNode[];
  isViewChanged:boolean=false;

  pagedList:any[]=[];
  breakpoint: number = 4;  //to adjust to screen
  pageEvent:any;
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];
  categorieGetDocModel:CategorieGetDoc=new CategorieGetDoc();
  searchProduit:any;
  nodeKey:string;

  have_access:boolean =false;
  routeState: any;

  constructor(private actualiteService: ActualiteService, private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService, private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService,
    private permissionservice: DroitAccesService, private router: Router) {

      ////console.log("extras on constr list doc : ",this.router.getCurrentNavigation().extras)

      if (this.router.getCurrentNavigation()?.extras?.state) {
        this.routeState = this.router.getCurrentNavigation().extras.state;
        if (this.routeState) {
          this.categorieGetDocModel = this.routeState.categorie ? JSON.parse(this.routeState.categorie) : ''; 
          
          ////console.log("inn this.categorieGetDocModel: ",this.categorieGetDocModel)
  
          if(this.categorieGetDocModel.label!=null && this.categorieGetDocModel.label!=undefined && this.categorieGetDocModel.label!=""){
            this.getDocumentByCategorie(this.categorieGetDocModel);
  
          }
  
  
        }
      }else{
          this.getDocumentByUser();
      }

     }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000113');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000111');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000112');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000110');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000098');


     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000098').subscribe((res:any)=>{
      this.have_access =res.data;
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.listeresidence();
      }
    });
  }

  addDocument() {
    this.spinnerservice.show()
    let navigationExtras: NavigationExtras = {
      state: {
        categorie: JSON.stringify (this.categorieGetDocModel )
      }
    }


    this.router.navigate(['/communite/info-document/', 0],navigationExtras);


  }

  editDocument(cbmarq) {
    console.log('cbmarq', cbmarq);
    let navigationExtras: NavigationExtras = {
      state: {
        categorie: JSON.stringify (this.categorieGetDocModel )
      }
    }
    this.router.navigate(['/communite/info-document/', cbmarq], navigationExtras);

  }

  /*** tree node table */
expandAll(){
    this.files1.forEach( node => {
        this.expandRecursive(node, true);
    } );
}

collapseAll(){
    this.files1.forEach( node => {
        this.expandRecursive(node, false);
    } );
}

private expandRecursive(node:TreeNode, isExpand:boolean){
    node.expanded = isExpand;
    if (node.children){
        node.children.forEach( childNode => {
            this.expandRecursive(childNode, isExpand);
        } );
    }
}

nodeSelect(event) {
  //event.node = selected node
////console.log("node selected: ",event.node)

  this.categorieGetDocModel.label=event?.node?.label;
  this.nodeKey = event.node.key;
  if (event.node.key === '1') {
    this.categorieGetDocModel.produit = +event.node.data;
    this.categorieGetDocModel.bloc = 0;
    this.categorieGetDocModel.bien = 0;
  }
  if (event.node.key === '2') {
    this.categorieGetDocModel.produit = 0;
    this.categorieGetDocModel.bloc = +event.node.data;
    this.categorieGetDocModel.bien = 0;
  }
  if (event.node.key === '3') {
    this.categorieGetDocModel.produit = 0;
    this.categorieGetDocModel.bloc = 0;
    this.categorieGetDocModel.bien = +event.node.data;
  }

  //getList Doc by residence/bloc/appartement
  this.getDocumentByCategorie(this.categorieGetDocModel);
}

changeView(){
  this.isViewChanged=!this.isViewChanged;
}

/*---------------------------*/
  filterRecursive(filterText: string, array: any[], property: string) {
    let filteredData;

    //make a copy of the data so we don't mutate the original
    function copy(o: any) {
      return Object.assign({}, o);
    }

    // has string
    if (filterText) {
      // need the string to match the property value
      filterText = filterText.toLowerCase();
      // copy obj so we don't mutate it and filter
      filteredData = array.map(copy).filter(function x(y) {
        if (y[property].toLowerCase().includes(filterText)) {
          return true;
        }
        // if children match
        if (y.children) {
          return (y.children = y.children.map(copy).filter(x)).length;
        }
      });
      // no string, return whole array
    } else {
      filteredData = array;
    }

    return filteredData;
  }
filterProduit(){

    let auxList=this.files1;

    console.log(this.searchProduit,this.files1);
    if (this.searchProduit!== "") {
      this.files1  = this.filterRecursive(this.searchProduit, this.files1, 'label');
      this.expandAll();
     // this.files1 = auxList.filter(word=>(
     //      ( word?.label.trim().toLowerCase().includes(this.searchProduit.trim().toLowerCase()))
     //  ));
     //

    }

  // this prints the array of filtered children

  if (this.searchProduit === '') {
    this.listeresidence();
    this.collapseAll();
  }
}

  listeresidence() {
    this.actualiteService.getResidenceNode().subscribe((res: any) => {
      this.files1 = res.data;
      this.spinnerservice.hide()
    },
      error => {
      //console.log( 'erreur: ', error);
      },()=>{

      }
    );
  }
  getDocumentByCategorie(categorieGetDocModel) {
    this.dataSource = new MatTableDataSource<any>();
    this.dataSource.data = [];
    this.pagedList = [];
    ////console.log("categorieGetDocModel: ",categorieGetDocModel)

    this.actualiteService.getDocumentByCategorie(categorieGetDocModel).subscribe((res: any) => {

      this.dataSource.data = res.listDocument;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      if (this.dataSource.data != undefined) {


        this.breakpoint = (window.innerWidth <= 800) ? 1 : 4;
        this.pagedList = this.dataSource.data.slice(0, 5);
        this.length = this.dataSource.data.length;

        ////console.log("pagedList: ",this.pagedList)
      }



    },
    error => {
      //console.log( 'erreur:' , error);
    }
    );
  }

  getDocumentByUser(){

    this.dataSource= new MatTableDataSource<any>();
    this.dataSource.data=[];
    this.pagedList=[];

    this.actualiteService.getDocumentByUser().subscribe((res: any) => {

      this.dataSource.data = res.listDocument;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;


      if( this.dataSource.data !=undefined){


        this.breakpoint = (window.innerWidth <= 800) ? 1 : 4;
        this.pagedList = this.dataSource.data.slice(0, 5);
        this.length = this.dataSource.data.length;

        ////console.log("pagedList: ",this.pagedList)
      }



    },
    error => {
      //console.log( 'erreur:' , error);
    }
    );

  }

  cancelCmp(cbmarq: any) {

    this.actualiteService.updateStatutDocument(cbmarq).subscribe(
        (res: any) => {
          if (res.statut === true) {
            this.toastr.success('Document supprimer avec succès !');
            this.getDocumentByUser();
            this.nodeSelect({node: {data: this.categorieGetDocModel.produit, key: this.nodeKey, label:  this.categorieGetDocModel.label}})

          } else {
            this.toastr.error(res.message, 'Erreur!', {progressBar: true});
          }
        },
        error => {
          this.toastr.error('Veuillez réessayer plus tard!');
        }
    );
  }
  confirmBox(cbmarq: any) {

    //console.log("cbmarq: ",cbmarq)
    Swal.fire({
      title: 'Supprimer un document',
      text: 'Voulez-vous vraiment supprimer ce document ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.cancelCmp(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    ////console.log(" this.dataSource: ", this.dataSource)
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }

    ////console.log("filterValue: ",filterValue);

    if(filterValue!=""){
      this.pagedList=this.dataSource.filteredData;
      this.length=this.pagedList.length;
    }else{
      this.pagedList = this.dataSource.data.slice(0, 5);
      this.length = this.dataSource.data.length;
    }




  }

  /*** card fct  */
  OnPageChange(event: PageEvent){
    let startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if(endIndex > this.length){
      endIndex = this.length;
    }
    let dataSourceAux=this.dataSource.data;
    this.pagedList = dataSourceAux.slice(startIndex, endIndex);
  }

  onResize(event) { //to adjust to screen size
    this.breakpoint = (event.target.innerWidth <= 800) ? 1 : 4;
  }

}
