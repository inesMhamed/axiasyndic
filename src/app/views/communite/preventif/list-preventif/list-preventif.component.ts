import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { InterventionService } from 'src/app/shared/services/intervention.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-preventif',
  templateUrl: './list-preventif.component.html',
  styleUrls: ['./list-preventif.component.scss'],
  providers: [DatePipe]
})
export class ListPreventifComponent implements OnInit {


  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File[] = [];
  displayedColumns: string[] = ['intitule','statut' ,'equipement','qte' ,'produit','emplacement','dateexecution', 'etat', 'action'];
  dataSource = new MatTableDataSource<any>();
  pageEvent: PageEvent;
  reunions;
  loading: boolean;
  submitted: boolean;
  blocs: any[];
  types: any[];
  appartements: any[] = [];
  appartementsProp: any[] = [];
  imgURL: any[] = [];
  formBasic: FormGroup;
  transforme: any = false;
  rec: any = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  data: any;
  role: any;
  isInterne: any = false;

  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: any=5;

  residences:any[]=[];

  rechAvance:boolean=false;
  formGroupSearch:FormGroup;
  statutList:any[]=[{label:"Statuts (En retard, En cours, Bientôt)",value: 0},{label:"Planifié",value:'E0079'},{label:"Bientôt",value:'E0080'},{label:"En cours",value:'E0081'},{label:"En retard",value:'E0082'},{label:"Fermé",value:'E0083'}];
  addViewList:boolean=false;
  range: FormGroup;
  currentIdTache:any;
  currentRow:any;
  have_access:boolean =false;

  constructor(
    //private reclamationService: ReclamationService,
    private residenceService: ResidenceService,
    private toastr: ToastrService,
    private preferenceService:PreferencesService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    //private appartementService: AppartementService,
    private blocService: BlocService,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    private permissionservice: DroitAccesService,
    private interventionService:InterventionService,
    private configMaintenanceService:ConfigMaintenanceService,
    private datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.spinnerService.show()
    /*this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })*/
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000086');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000088');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000087');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000089');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000090');

    this.submitted = false;
    this.role = localStorage.getItem('role');

     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000090').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.listeresidence();
        this.listePreventif();
      }
    });


   

    this.formGroupSearch = this.fb.group({

      produit:new FormControl(null),
      statut:new FormControl(null),

    });

    this.formGroupSearch.patchValue({
      produit:0,
      statut:0
    })

    this.range = new FormGroup({
      datedebut: new FormControl(null, Validators.required),
      datefin: new FormControl(null, Validators.required)
    })

    

    this.formBasic=this.fb.group({
      operation:new FormControl(null),
      responsable: new FormControl(null, Validators.required),
      telephone: new FormControl(null),
      description: new FormControl(null, Validators.required),
      datedebut: new FormControl(new Date(), Validators.required),
      datefin: new FormControl(new Date(), Validators.required),
      
    })



  }

  listeresidence() {
    this.spinnerService.show()
    this.residences = [];
    let tous = { value: 0, label: "Tous les produits" };


    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.statut) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        ////console.log("this.residence: ",this.residences)
        this.residences.splice(0, 0, tous);
        this.spinnerService.hide()
      }else{
        this.spinnerService.hide()
      }

        },
        error => {
            //console.log( 'erreur: ', error);
        },()=>{
        }
    );
  }

 

  listePreventif() {
    this.spinnerService.show()
    this.dataSource = new MatTableDataSource<any>();

    this.configMaintenanceService.operationsEncours(0,null).subscribe((res: any) => {

      if(res.statut){
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide()
      }else{
        this.spinnerService.hide()
      }
         

          ////console.log("let liste preventif planifié : ",this.dataSource.data);
        },
        error => {
          //console.log( 'erreur:' , error);
        },()=>{

        }
    );
  }

  onchangeProduit(){
    this.formGroupSearch.patchValue({
      statut: 0
    })
  }

  onSubmitSearch(){
    ////console.log("formsearch: ",this.formGroupSearch.value)
    this.spinnerService.show()
    this.dataSource = new MatTableDataSource<any>();

    let statut= this.formGroupSearch.value.statut==0?null:this.formGroupSearch.value.statut;
  
    if(this.formGroupSearch.value.produit!=0){
      this.configMaintenanceService.operationsEncours(this.formGroupSearch.value.produit,statut).subscribe((res:any)=>{

        if(res.statut){

          ////console.log("res search: ",res.data)
          this.dataSource.data=res.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.spinnerService.hide()
          ////console.log("res filtre: ",this.dataSource.data)

        }else{
          this.spinnerService.hide()
        }

      })
    } else {
      this.listePreventif();
    }

  }






  confirmBoxVerififer(row: any,etat){

    //valider/invalider que opération en instance ou invalide
    if (row.etat != 1) {
      let title = "";
      let text = "";

      if (etat == "1") {
        title = "Confirmation d\'une opération";
        text = "Voulez-vous vraiment valider la confimration de cette opération ?";
      }
      if (etat == "2") {
        title = "Refus d\'une opération";
        text = "Voulez-vous vraiment valider le refus de cette opération ?";
      }
      Swal.fire({
        title: title,
        text: text,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Annuler',
        confirmButtonText: 'Confirmer',
      }).then((result) => {
        if (result.value) {
          this.verifierOperation(row,etat);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });
    }

  
  }

  verifierOperation(row: any, etat) {
    this.spinnerService.show()
    if (etat == "1") {
      this.configMaintenanceService.postOperationEtat(row?.cbmarq, "1").subscribe((res: any) => {

        if (res.statut) {
          this.toastr.success("Opération a été validée avec succès !", 'Success!', { progressBar: true })

          this.listePreventif();

        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          this.spinnerService.hide()


        }

      }, error => {
        this.toastr.error('Erreur lors de la validation d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })

      })
    }

    if (etat == "2") {
      this.configMaintenanceService.postOperationEtat(row?.cbmarq, "2").subscribe((res: any) => {

        if (res.statut) {
          this.toastr.success("Opération a été échouée !", 'Success!', { progressBar: true })
          this.listePreventif();

        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          this.spinnerService.hide()
        }

      }, error => {
        this.toastr.error('Erreur lors de refus d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })

      })
    }
       

  }


  confirmBoxCorriger(row: any){

    //echouer operation que si l'operation est en instance
    if(row?.etat==0){
      Swal.fire({
        title: 'Correction d\'une tâche',
        text: 'Voulez-vous vraiment corriger cette tâche ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmer',
        cancelButtonText: 'Annuler'
      }).then((result) => {
        if (result.value) {
  
  
          ////console.log("confirmBoxCorriger: ",row);
          this.checkListAction(row);
          
         
  
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });
    }
   
  }


 
  
  checkListAction(row){

    let listAction=new MatTableDataSource<any>();
    this.configMaintenanceService.getListActions(row.cbmarq).subscribe((res:any)=>{

      if(res.statut){
        listAction.data=res.data;      

        ////console.log("checkListAction list action: ",this.dataSource.data)

         /* OPERATION : 0:En Instance ; 1:Valide ; 2 : Echouer */
        if(row.etat!=1){
          ////console.log("enter operation en instance .. ")

          //verifier dernier action: il faut qu'elle ne soit pas en cours
          if(listAction.data.length>0){
            ////console.log("dataSource: ",this.dataSource.data[this.dataSource.data.length-1])
            let action=listAction.data[listAction.data.length-1];
            ////console.log("derniere action : ",action)
    
            // dernier action est en cours
            if(action.etat=="0"){
              this.toastr.info('Vous ne pouvez pas ajouter une autre action avant de clôturer la dernière !', 'Info!', { progressBar: true })
            }
            //si dernier action est echouer
            if (action.etat == "1") {
              this.addViewList = true;
              this.currentIdTache = row?.cbmarq;
              this.currentRow = row;
            }
            if (action.etat == "2") {
              this.toastr.info('Vous ne pouvez pas ajouter une d\'autre action après la validation!', 'Info!', { progressBar: true })
            }
    
          
      
          
          }else{
            ////console.log("change view to add action")
            this.addViewList=true;
            this.currentIdTache=row?.cbmarq;
            this.currentRow=row;
          }
        }

      }else{
        this.addViewList=true;
        this.currentIdTache=row?.cbmarq;
        this.currentRow=row;
      }

    },error=>{},()=>{
     

    })

  }




  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  gotoListPreventif(){
    this.addViewList=false;
  }


  /** submit tache in operation **/
  onSubmit(){
    this.submitted=true;
    if(this.formBasic.invalid && this.currentRow!=null){
      return;
    }

    


    this.formBasic.patchValue({
      operation:this.currentRow.cbmarq//this.currentIdTache,   
    })

    ////console.log("onSubmit tache corrective: ",this.formBasic.value,"\n this.currentRow: ",this.currentRow);

    this.configMaintenanceService.addAction(this.formBasic.value).subscribe((res:any)=>{

      if(res.statut){
        //this.toastr.success("Action ajouté avec succès !", 'Success!', { progressBar: true })

        //set etat non valide pour l'operation
        this.verifierOperation(this.currentRow,"2");
        //
        this.gotoListPreventif();
        this.currentRow=null;
        this.formBasic.reset();
        this.submitted=false;

      }else{
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });

      }
    },error=>{
      this.toastr.error('Erreur lors de l\'ajout  d\'une action . Veuillez réessayer !', 'Erreur!', { progressBar: true })
    })


  }

 

  startChange(event)
  {
      this.formBasic.patchValue({
        datedebut: this.datepipe.transform(new Date(event.value), 'dd-MM-yyyy'),

      })


  }
  endChange(event)
  {
    this.formBasic.patchValue({
      datefin: this.datepipe.transform(new Date(event.value), 'dd-MM-yyyy'),

    })

  }


}
