import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPreventifComponent } from './list-preventif.component';

describe('ListPreventifComponent', () => {
  let component: ListPreventifComponent;
  let fixture: ComponentFixture<ListPreventifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListPreventifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPreventifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
