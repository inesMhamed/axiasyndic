import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPreventifComponent } from './info-preventif.component';

describe('InfoPreventifComponent', () => {
  let component: InfoPreventifComponent;
  let fixture: ComponentFixture<InfoPreventifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoPreventifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPreventifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
