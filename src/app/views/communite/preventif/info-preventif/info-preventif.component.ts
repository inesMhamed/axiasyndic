import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Preventif } from 'src/app/shared/models/configMaintenanceModel/preventif.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ContactsRoutingModule } from 'src/app/views/contacts/contacts-routing.module';
import Swal from 'sweetalert2';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { EmplacementPreventif } from 'src/app/shared/models/configMaintenanceModel/emplacementPreventif.model';
import { NgxSpinnerService } from 'ngx-spinner';




@Component({
  selector: 'app-info-preventif',
  templateUrl: './info-preventif.component.html',
  styleUrls: ['./info-preventif.component.scss'],
  providers: [DatePipe]

})
export class InfoPreventifComponent implements OnInit {

  id: any = 0;
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['responsable','mobile' ,'start','end','conclusion','commentaire','statut','action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_editOp: Permission;
  can_detailOp: Permission;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_cloturer: Permission;
  can_annuler: Permission;
  id_current_user: any;

  preventifModel:Preventif=new Preventif();
  viewaddListTache:boolean=false;
  formBasic: FormGroup;
  range: FormGroup;
  residences:any[]=[];
  pipe = new DatePipe('en-Fr'); // Use your own locale
  submitted:boolean=false;
  currentIdTache:any;
  idTache:any=0;
  intitulePreventif:string="";
  formOpImages:FormGroup;

  /****/
  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File = null;
  imgSrcAvant:any;

  @ViewChild('labelImport2')
  labelImport2: ElementRef;
  fileToUpload2: File = null;
  imgSrcApres:any;

  submitCommentaire:boolean=false;
  have_access:boolean =false;


  constructor(private router: Router, private permissionservice: DroitAccesService, private fb: FormBuilder, private fbImg: FormBuilder,
    private configMaintenanceService: ConfigMaintenanceService, private actRoute: ActivatedRoute, private datepipe: DatePipe,
    private spinnerService: NgxSpinnerService, private toastr: ToastrService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.spinnerService.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_editOp = this.permissionservice.search( this.id_current_user, 'FN21000087');
    this.can_detailOp = this.permissionservice.search( this.id_current_user, 'FN21000089');

    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000128');
    this.can_cloturer = this.permissionservice.search( this.id_current_user, 'FN21000129');
    this.can_annuler = this.permissionservice.search( this.id_current_user, 'FN21000130');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000131');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000132');

         

    this.range = new FormGroup({
      start: new FormControl(null, Validators.required),
      end: new FormControl(null, Validators.required)
    })

    

    this.formBasic=this.fb.group({
      operation:new FormControl(null),
      responsable: new FormControl(null, Validators.required),
      telephone: new FormControl(null),
      description: new FormControl("", Validators.required),
      datedebut: new FormControl(new Date(), Validators.required),
      datefin: new FormControl(new Date(), Validators.required),
      
    })

    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
      if(this.id!=0){
        this.checkAccessInUpdate();
       
      }else{
        this.goOut();
      }

    });  

    
    this.formOpImages=this.fbImg.group({

      commentaire:new FormControl(""),
      imgavant:new FormControl(null),
      imgapres:new FormControl(null),
      
    })


  }

 
  checkAccessInUpdate(){

     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000089').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getOperationById();
        this.getListAction();
      }
    });

  }

  goOut(){
    this.router.navigate(['403']);
  }

  getOperationById(){
    this.configMaintenanceService.getOperationById(this.id).subscribe((res:any)=>{
      if(res.statut){
        this.preventifModel=res.data;

        const now = new Date(res.data.dateExecution.date);
        const myFormattedDate = this.pipe.transform(now, 'yyyy-MM-dd');
        this.preventifModel.dateExecution=myFormattedDate;
        this.intitulePreventif=this.preventifModel.gamme.intitule+", "+this.preventifModel.gamme.equipement.intitule;

        ////console.log("res detail operation: ",res.data);
        if(this.preventifModel.gamme.equipement.emplacement==null){
          this.preventifModel.gamme.equipement.emplacement=new  EmplacementPreventif()
          this.preventifModel.gamme.equipement.emplacement.intitule="";
        }


        this.formOpImages.patchValue({
          commentaire:this.preventifModel.commentaire=="null"?"":this.preventifModel.commentaire,       
        })

        this.imgSrcAvant=this.preventifModel.imgavant;
        this.imgSrcApres=this.preventifModel.imgapres;

        /*** 1 **/
        let file1:string="'"+this.preventifModel.imgavant;
        let fileName1 = this.preventifModel.imgavant!=null?file1.substring(file1.lastIndexOf('/')+1):"";
        this.labelImport.nativeElement.innerText= fileName1; 
        /***2 */
        let file2:string="'"+this.preventifModel.imgapres;
        let fileName2 = this.preventifModel.imgapres!=null?file2.substring(file2.lastIndexOf('/')+1): "" ;
        this.labelImport2.nativeElement.innerText= fileName2;
       
    
      }
     
    })
  }

    
  

  
  gotoListPreventif(){
    this.router.navigate(['/communite/preventif'])

  }

  ajouterAction(){

    this.formBasic.reset();
    this.range.reset();
    this.submitted=false;
    this.idTache=0;
    //etat opération !=valide 
    ////console.log("ajouterAction ... this.preventifModel.etat: ",this.preventifModel.etat);

    if(this.preventifModel.etat!=1){

      /** verifier dernier action: il faut qu'elle ne soit pas en cours ***/
      if(this.dataSource.data.length>0){
        ////console.log("dataSource: ",this.dataSource.data[this.dataSource.data.length-1])
        let action=this.dataSource.data[this.dataSource.data.length-1];

        // dernier action est en cours
        if(action.etat=="0"){
          this.toastr.info('Vous ne pouvez pas ajouter une autre action avant de clôturer la dernière !', 'Info!', { progressBar: true })
        }
        //dernier action est echouer
        if(action.etat=="1"){
          this.viewaddListTache=true;
        }
        if(action.etat=="2"){
          this.toastr.info('Vous ne pouvez pas ajouter une d\'autre actions après la validation!', 'Info!', { progressBar: true })
        }

       
  
       
      }else{
        this.viewaddListTache=true;
      }
     
    }
    
  }

  /***wz */


  confirmBoxVerififer(row: any,etat){

    ////console.log("row: ",row, "etat: ",etat)

    //par defaut etat=1  ==> à valider
    //valider que opération en instance ou non valide
    if (row.etat != 1) {
      let title = "";
      let text = "";

      if (etat == "1") {
        title = "Conformité  d\'une opération";
        text = "Voulez-vous vraiment valider la conformité cette opération ?";
      }
      if (etat == "2") {
        title = "Incoformité d\'une opération";
        text = "Voulez-vous vraiment valider l'inconformité de cette opération ?";
      }
      Swal.fire({
        title: title,
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmer',
        cancelButtonText: 'Annuler'
      }).then((result) => {
        if (result.value) {
          this.verifierOperation(row,etat);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });
    }


  }

  verifierOperation(row: any, etat) {
    this.spinnerService.show()

    if(etat=="1"){
      this.configMaintenanceService.postOperationEtat(row?.cbmarq,"1").subscribe((res:any)=>{

        if (res.statut) {
          this.toastr.success("Opération a été validée avec succès !", 'Success!', { progressBar: true })
          this.router.navigate(['/communite/preventif']);
          this.spinnerService.hide()

        }else{
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });

        }

      }, error => {
        this.toastr.error('Erreur lors de la confirmation d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })

      })
    }

    if(etat=="2"){
      this.configMaintenanceService.postOperationEtat(row?.cbmarq,"2").subscribe((res:any)=>{

        if (res.statut) {
          this.toastr.success("Opération a été échouée !", 'Success!', { progressBar: true })
          this.router.navigate(['/communite/preventif']);


        }else{
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });

        }

      }, error => {
        this.toastr.error('Erreur lors de refus d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })

      })
    }


  }






  /*****/

  gotoListTache(){
    this.viewaddListTache=false;

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getListAction() {
    this.spinnerService.show()
    this.dataSource = new MatTableDataSource<any>();
    this.configMaintenanceService.getListActions(this.id).subscribe((res: any) => {

      if(res.statut){
        this.dataSource.data=res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide()
        // //console.log("list action préventif: ",this.dataSource.data)
      }

    },error=>{},()=>{
     
    })

  }

  getListActionAfterSave() {
    this.spinnerService.show()
    this.dataSource = new MatTableDataSource<any>();
    this.configMaintenanceService.getListActions(this.id).subscribe((res: any) => {

      if(res.statut){
        this.dataSource.data=res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide()
        ////console.log("getListActionAfterSave: ",this.dataSource.data)
      }

    },error=>{},()=>{
      //if data source  vide update statut non valide
      if(this.dataSource.data.length==1){

        this.verifierOperation(this.preventifModel,"2");
      }
    })

  }

  
  startChange(event)
  {
      this.formBasic.patchValue({
        datedebut: this.datepipe.transform(new Date(event.value), 'dd-MM-yyyy'),

      })


  }
  endChange(event)
  {
    this.formBasic.patchValue({
      datefin: this.datepipe.transform(new Date(event.value), 'dd-MM-yyyy'),

    })

  }

  

  /** submit tache in operation **/
  onSubmit(){
    this.submitted=true;
    if(this.formBasic.invalid){
      return;
    }

    


    this.formBasic.patchValue({
      operation:this.id,   
    })

    ////console.log("onSubmit tache corrective: ",this.formBasic.value,"idTache: ",this.idTache);



    if(this.idTache==0)
    {
      this.configMaintenanceService.addAction(this.formBasic.value).subscribe((res:any)=>{

        if(res.statut){
          this.toastr.success("Action ajouté avec succès !", 'Success!', { progressBar: true })
          this.gotoListTache();
          this.getListActionAfterSave();

        }else{
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });

        }
      },error=>{
        this.toastr.error('Erreur lors de l\'ajout  d\'une action . Veuillez réessayer !', 'Erreur!', { progressBar: true })
      },()=>{

       

      })
    }else{

      this.configMaintenanceService.putAction(this.formBasic.value,this.idTache).subscribe((res:any)=>{

        if(res.statut){
          this.toastr.success("Action modifié avec succès !", 'Success!', { progressBar: true })
          this.gotoListTache();
          this.getListAction();

        }else{
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });

        }
      },error=>{
        this.toastr.error('Erreur lors de modification  d\'une action . Veuillez réessayer !', 'Erreur!', { progressBar: true })
      })
    }
            


  }

  editLineAction(row){
    /*** OPERATION : 0: En Instance ; 1 : Valide ; 2 : Echouer ||||| TÂCHE : 0 : En Instance ; 1 : Non Valide ; 2 : Valide  **/

    // action == en instance && operation != valide
    if(row?.etat==0 &&  this.preventifModel.etat!=1){
      this.viewaddListTache=true;
      this.idTache=row.cbmarq;
  
      this.range.patchValue({
        start: new Date(row.datedebut.date),
        end: new Date(row.datefin.date)
      })
     
      this.formBasic.patchValue({
        operation: row.operation,
        responsable: row.responsable,
        telephone: row.telephone,
        description:row.description, 
        datedebut: this.datepipe.transform(new Date(row.datedebut.date), 'dd-MM-yyyy'),
        datefin: this.datepipe.transform(new Date(row.datefin.date), 'dd-MM-yyyy'),
  
       
      })
    }

   

  }

  confirmEtatAction(row,etatAction){

    // action == en instance && operation != valide
    if(row?.etat==0 &&  this.preventifModel.etat!=1){
      this.submitCommentaire=true;
      let title="";
      if(etatAction=="2"){
        title= "Valider l\'action";
      }
      if(etatAction=="1"){
        title= "Echouer l\'action";
      }

      Swal.fire({
        title: title,
        text: "Ecriver votre commentaire !",
        input: 'text',
        icon: 'warning',

      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
        customClass: {
          validationMessage: 'my-validation-message'
        },
        preConfirm: (value) => {

          ////console.log("value: ",value)
          if (!value) {
            Swal.showValidationMessage(
              '<i class="fa fa-info-circle"></i> Votre commentaire est requis'
            )
          }else{
            ////console.log("submit comment: " + value);
            let etatModel={
              etat: etatAction,
              commentaire: value
            }

            this.setEtatAction(row,etatModel)
          }
        }
      })
  
    
    }

   
    

  }

  setEtatAction(row,etatModel){

   
    this.configMaintenanceService.setEtatAction(row.cbmarq,etatModel).subscribe((res:any)=>{
      if(res.statut){
        if(etatModel.etat=="2"){
          this.toastr.success("Action cloturé avec succès !", 'Success!', { progressBar: true })
        }
        if(etatModel.etat=="1"){
          this.toastr.success("Action annuler avec succès !", 'Success!', { progressBar: true })

        }
        this.getListAction();

      }else{
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });

      }
    },error=>{
      this.toastr.error('Erreur lors de modification d\'état  d\'une action . Veuillez réessayer !', 'Erreur!', { progressBar: true })

    })

  }

  confirmDeleteAction(row){

    // action == en instance && operation != valide
    if(row?.etat==0 &&  this.preventifModel.etat!=1){

      Swal.fire({
        title: 'Supression d\'une action',
        text: 'Voulez-vous vraiment supprimer cette action ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmer',
        cancelButtonText: 'Annuler'
      }).then((result) => {
        if (result.value) {
          this.deleteAction(row);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });

    }
  
  }

  deleteAction(row: any){


    this.configMaintenanceService.deleteAction(row.cbmarq).subscribe((res:any)=>{
      
      if(res.statut){
        this.toastr.success("Action supprimer avec succès !", 'Success!', { progressBar: true })
        this.getListAction();

      }else{
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });

      }

    },error=>{
      this.toastr.error('Erreur lors de supression d\'une action . Veuillez réessayer !', 'Erreur!', { progressBar: true })

    })

  }


  /*** img & commentaire  */
  onFileChange(files: FileList) {

    //if != valider
    if(this.preventifModel.etat!=1){

      this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
        this.fileToUpload = files.item(0);

        /***** */
        //Show image preview
        let reader = new FileReader();
        reader.onload = (event: any) => {
          this.imgSrcAvant = event.target.result;
        }
        reader.readAsDataURL(this.fileToUpload);    
    }
   
   

  
  }

  onFileChange2(files: FileList) {

    ////console.log("this.onFileChange2.etat: ",this.preventifModel.etat)
    if(this.preventifModel.etat!=1){
        this.labelImport2.nativeElement.innerText = Array.from(files)
            .map(f => f.name)
            .join(', ');
        this.fileToUpload2 = files.item(0);

        //Show image preview
        let reader = new FileReader();
        reader.onload = (event: any) => {
          this.imgSrcApres = event.target.result;
        }
        reader.readAsDataURL(this.fileToUpload2); 
      }

  }

  onSubmitOpImages(){

    if(this.preventifModel.etat!=1){
      let formImage={
        commentaire:this.formOpImages.value.commentaire,
        imgavant:this.fileToUpload,
        imgapres:this.fileToUpload2,
      }
  
      ////console.log("formOpImage: ",formImage)
  
  
      const myFormValue = formImage;
      const myFormData = new FormData();
      Object.keys(myFormValue).forEach(name => {
        myFormData.append(name, myFormValue[name]);
      });
  
  
      this.configMaintenanceService.postOperationImage(this.id,myFormData).subscribe((res:any)=>{
        if(res.statut){
          this.toastr.success("Opération modifié avec succès !", 'Success!', { progressBar: true })
          this.gotoListPreventif();
  
        }else{
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
  
        }
      },error=>{
        this.toastr.error('Erreur lors de modification  d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })
      });
  
    }

  
  }
}
