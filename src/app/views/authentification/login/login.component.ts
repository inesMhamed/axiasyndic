import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/shared/services/authentification.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { first } from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthenticationService]

})
export class LoginComponent implements OnInit {
  submitted: boolean;
  loginForm: FormGroup;
  loading: boolean;
  loadingText: string;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private preferenceService: PreferencesService,
    public authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required,]
    });
  }
  get f() {
    return this.loginForm.controls;
  }


  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    //console.log('loginForm: ', this.loginForm.value)
    this.authenticationService
      .login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (data: any) => {
          if (data.status) {
            localStorage.setItem('username', this.f.username.value)
            //console.log('data login: ', data)
            //console.log('role ', data.user.role)
            this.router.navigate(["/dashboard"]);
          }
          else {
            this.toastr.warning(data.message);
            //console.log('bonjour');
            //this.router.navigate(["/"]);
          }
        },
        error => {
          this.toastr.warning(error.message);
          // this.toastr.warning("Nom utilisateur ou mot de passe incorrect");
        })
  }

}
