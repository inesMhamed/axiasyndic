import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { AuthentificationRoutingModule } from './authentification-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, AuthentificationRoutingModule,
    ReactiveFormsModule,
    NgSelectModule,
  ],
  declarations: [
    LoginComponent, 
    ]
})
export class AuthentificationModule { }
