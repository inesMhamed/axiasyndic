import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { InboxRoutingModule } from './inbox-routing.module';
import { MessagesComponent } from './messages/messages.component';
import { ComposeDialogComponent } from './compose-dialog/compose-dialog.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill';
import { DataLayerService } from 'src/app/shared/services/data-layer.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    PerfectScrollbarModule,
    QuillModule.forRoot(),
    InboxRoutingModule,
    TagInputModule,
    NgSelectModule,
    NgxSpinnerModule



  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [MessagesComponent, ComposeDialogComponent],
  providers:[DataLayerService,NgbActiveModal]

})
export class InboxModule { }
