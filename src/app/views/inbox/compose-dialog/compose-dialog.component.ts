import { Component, EventEmitter, HostListener, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Inbox } from 'src/app/shared/models/inbox.model';

/*** quill editor import */
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
Quill.register('modules/imageResize', ImageResize);
import Quill from 'quill'
  import QuillEmoji from 'quill-emoji'
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { el } from 'date-fns/locale';
import { DataLayerService } from 'src/app/shared/services/data-layer.service';
import { Observable } from 'rxjs';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
  Quill.register('modules/emoji-shortname', QuillEmoji.ShortNameEmoji)
/******/

class LabelValue
  {
    label: string;
    value: string ;
  }

@Component({
  selector: 'app-compose-dialog',
  templateUrl: './compose-dialog.component.html',
  styleUrls: ['./compose-dialog.component.scss']
})
export class ComposeDialogComponent implements OnInit {


  //pills: LabelValue[] = [];
  value: string;
  isFocussed: boolean;
  listUser: LabelValue[] = [];
  selectedUser:any[]=[];
  filteredList: LabelValue[];

  /*-------------*/
  inboxModel:Inbox=new Inbox;
  @ViewChild('editor') editor;
  emails:string="";
  subject:string="";
	htmlStringModel2: any ;
	htmlStringFormatted: any ;
  attachements:File[]=[];

	quillEditorRef: any;
  fileName = '';
	currentAttachmentPost:any;
  submitted:boolean=false;




  modules2 = {
		'emoji-shortname': true,
		'emoji-textarea': true,
		'emoji-toolbar': true,
		toolbar: [
		  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
		  ['blockquote', 'code-block'],

		  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
		  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
		  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
		  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
		  [{ 'direction': 'rtl' }],                         // text direction

		  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
		  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

		  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
		  [{ 'font': [] }],
		  [{ 'align': [] }],

		  ['clean'],                                         // remove formatting button

		  ['link', 'image', 'video']   ,
		  ['emoji']                      // link and image, video
		],
		imageResize: true // for image resize,

	  };

    selectedUsers: any;
    filteredUsers: any[];
    @Output() onBackToList = new EventEmitter<any>();



  constructor(public activeModal: NgbActiveModal,private fb: FormBuilder,private utilisateurService: UtilisateurService, private actualiteservice:ActualiteService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getListContact();
  }



  getListContact(){
    this.actualiteservice.getListContact().subscribe((res:any) => {
      ////console.log("list contact: ",res.data)
      this.listUser = res.data.map(clt => ({ value: clt.id, label: clt.nomComplet }));


  },error=>{},()=>{

  });



  }

  submit(){
    this.submitted=true;

    ////console.log("list pills :",this.pills)
    if(this.selectedUser.length==0){
      return;
    }

    for(let labelValue of this.selectedUser){
      this.inboxModel.destinateur.push(labelValue.value);
    }


    this.inboxModel.objet=this.subject;
    this.inboxModel.message=this.htmlStringFormatted;
    this.inboxModel.attachements=this.attachements;

    ////console.log("submit Inbox:: ",this.inboxModel)

    const myFormValue =this.inboxModel;
		const myFormData = new FormData();
		Object.keys(myFormValue).forEach(name => {
		  myFormData.append(name, myFormValue[name]);
		});

    this.attachements.forEach((file) => { myFormData.append('attachements[]', file); });


    this.actualiteservice.addMessagerie(myFormData).pipe(first()).subscribe((res: any) => {
      if (res.statut === true) {

        this.toastr.success('Email envoyé avec succès .', 'Success!', {progressBar: true});

        this.htmlStringFormatted="";
        this.htmlStringModel2="";
        this.inboxModel=new Inbox();
        this.backToInbox();
        this.submitted=false;

          } else {
            this.toastr.error(res.message, 'Erreur!', {progressBar: true});
          }
        },
        (error) => {

          this.toastr.error('Erreur lors d\'envoye d\'email . Veuillez réessayer !', 'Erreur!', {progressBar: true});
        });

  }


  onSelect(item) {
    //console.log('tag selected: value is ' + item);
    this.emails=this.emails+";"+item;

    //console.log("emails: ",this.emails)
  }

  onContentChanged = (event) => {
    ////console.log("content changed: ",event.html)
    this.htmlStringFormatted = event.html;
  }

  onFileSelected(event) {

    const file:File = event.target.files[0];

    if (file) {

      this.fileName = file.name;

      const formData = new FormData();

      ////console.log("form data: ",file);
      this.currentAttachmentPost=file;

      formData.append("thumbnail", file);

     //this.fileName.split('.').pop();
      this.attachements.push(file);

      ////console.log("attachement: ",this.attachements)



    }



  }

  getExtension(fileName:string):string{

    return fileName.split('.').pop()
  }

  deleteFileUpload(attachement){
    this.fileName="";
    this.currentAttachmentPost=null;

    this.attachements.forEach( (item, index) => {
      if(item === attachement) this.attachements.splice(index,1);
    });

  }

  backToInbox(){
    this.onBackToList.emit(true);
  }

  /*** ng multiselect fct */




}
