import { Component, OnInit } from '@angular/core';
import { DataLayerService } from 'src/app/shared/services/data-layer.service';
import { Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ComposeDialogComponent } from '../compose-dialog/compose-dialog.component';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { Inbox } from 'src/app/shared/models/inbox.model';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Permission } from '../../../shared/models/permission.model';
import { DroitAccesService } from '../../../shared/services/droit-acces.service';
import { Proprietaire } from 'src/app/shared/models/proprietaire.model';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

class ResInboxObject {
  envoi: Inbox[];
  nonlu: number;
  reception: Inbox[];
  trash: Inbox[];
  constructor() {
    this.envoi = [];
    this.reception = [];
    this.trash = [];
  }
}

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  animations: [SharedAnimations]
})
export class MessagesComponent implements OnInit {
  mails$: Observable<any>;
  selected: boolean = false;
  isInbox: boolean = false;
  isSended: boolean = false;
  isTrash: boolean = false;
  isnewEmail: boolean = false;
  currentEmail: Inbox = new Inbox();
  inboxModel: ResInboxObject = new ResInboxObject();
  listMessagerie: Inbox[] = [];
  moreDetails: boolean = true;
  searchEmail: string = "";

  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;
  proprietaire: Proprietaire = new Proprietaire();
  role: any;
  Admin: any;
  imgSrc: string = "../../../assets/images/faces/defaultAvatar.png";

  have_access: boolean = false;


  constructor(
    private dl: DataLayerService,
    private modalService: NgbModal,
    private actualiteservice: ActualiteService,
    private toastr: ToastrService,
    private permissionservice: DroitAccesService,
    private proprietaireService: ProprietaireService,
    private spinnerservice: NgxSpinnerService,
    private router: Router
  ) {

    this.id_current_user = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.Admin = this.role.includes('ROLE_ADMIN');

  }

  ngOnInit() {
    this.spinnerservice.show()
    this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000109');
    this.can_delete = this.permissionservice.search(this.id_current_user, 'FN21000108');
    this.can_detail = this.permissionservice.search(this.id_current_user, 'FN21000096');
    this.can_liste = this.permissionservice.search(this.id_current_user, 'FN21000097');

    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000097').subscribe((res: any) => {

      this.have_access = res.data;

    }, error => { }, () => {

      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {





        this.getDetailUtilisateur(this.id_current_user);

        this.refreshInbox();
        this.getTrashEmail()
        this.getSendedEmail();

      }

    });



    ////console.log("this.emails: ",this.mails$)

  }

  getInbox() {
    this.actualiteservice.getAllMessagerie().subscribe((res: any) => {
      this.inboxModel.reception = res.data.reception;
      this.inboxModel.nonlu = res.data.nonlu;
      ////console.log("res: ",res.data)
      ////console.log(" this.inboxModel: ", this.inboxModel)

    }, error => { }, () => {
      //this.selectReception();
    })
  }

  getDetailUtilisateur(idUtilisateur: string) {
    this.spinnerservice.show()
    if (idUtilisateur != null) {

      this.proprietaireService.getProprietairebyId(idUtilisateur).subscribe((pro: any) => {
        if (pro.statut === true) {
          this.spinnerservice.hide()
          this.proprietaire = pro.data
          this.imgSrc = pro.data.photo
          this.proprietaire.logoText = this.proprietaire.nomComplet.charAt(0);


        }
      })
    }
  }

  refreshInbox() {
    this.spinnerservice.show()
    this.actualiteservice.getAllMessagerie().subscribe((res: any) => {
      this.inboxModel.reception = res.data.reception;
      this.inboxModel.nonlu = res.data.nonlu;
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.selectReception();
    })
  }

  getTrashEmail() {
    this.actualiteservice.getMessagerieSupprime().subscribe((res: any) => {
      ////console.log("res trash: ",res)

      this.inboxModel.trash = res.data;
      ////console.log("Email supp: ",this.inboxModel.trash)

    }, error => { }, () => {
      //this.selectTrash();
    })
  }

  getSendedEmail() {
    this.spinnerservice.show()
    this.actualiteservice.getMessagerieEnvoyee().subscribe((res: any) => {
      this.inboxModel.envoi = res.data;
      this.spinnerservice.hide()
    })
  }
  selectEmail(mail: Inbox) {
    this.selected = true;
    this.isnewEmail = false;
    this.moreDetails = true;
    this.currentEmail = mail;
    this.currentEmail.logoText = this.currentEmail.cbcreateur.nomComplet.charAt(0);
    console.log("currentEmail: ", this.currentEmail)
    if (mail.statut == 0) {

      this.actualiteservice.setMesagerieVu(this.currentEmail.cbmarq).subscribe(res => {

      }, eror => { }, () => {
        mail.statut = 1;
        // this.inboxModel.nonlu = this.inboxModel.nonlu - 1;
       

        this.getInbox();
        this.getSendedEmail();
        this.getTrashEmail();
      })
    }

  }

  getExtension(fileName: string): string {

    //return fileName.split('.').pop()
    //console.log("filename: ",fileName)
    //console.log("returned extension: ",fileName.substring(fileName.lastIndexOf(".")))
    return fileName.substring(fileName.lastIndexOf("."))
  }

  getNameofFile(fileName: string): string {
    if (fileName) {
      var m = fileName.toString().match(/.*\/(.+?)\./);
      if (m && m.length > 1) {
        return m[1];
      }
    }
    return "";
  }



  selectReception() {
    this.listMessagerie = this.inboxModel.reception;
    //console.log("messagerie: ",this.listMessagerie);

    this.listMessagerie.forEach((messagerie: any) => {
      messagerie.logoText = messagerie.cbcreateur.nomComplet.charAt(0);
      /*if( messagerie.attachement){
        messagerie.attachement.forEach(attach => {
          attach.attachementExt=attach.substring(attach.lastIndexOf(".")+1)
  
        });
      }*/


    })
    this.isInbox = true;
    this.isSended = false;
    this.isTrash = false;
    this.viewListInbox();
    /** select First Msg of list */
    if (this.listMessagerie.length > 0) {
      this.selectEmail(this.listMessagerie[0]);
    }

  }

  selectSended() {
    this.listMessagerie = this.inboxModel.envoi;
    this.listMessagerie.forEach((messagerie: any) => {
      messagerie.logoText = messagerie.cbcreateur.nomComplet.charAt(0);

    })

    this.isInbox = false;
    this.isSended = true;
    this.isTrash = false;
    this.viewListInbox();

  }

  selectTrash() {
    this.listMessagerie = this.inboxModel.trash;
    this.listMessagerie.forEach((messagerie: any) => {
      messagerie.logoText = messagerie.cbcreateur.nomComplet.charAt(0);

    })
    this.isInbox = false;
    this.isSended = false;
    this.isTrash = true;
    this.viewListInbox();
  }

  openComposeModal() {
    this.isnewEmail = true;
    this.selected = null;
  }

  viewListInbox() {
    this.isnewEmail = false;
    this.selected = null;

  }

  refreshViewListInbox() {

    this.viewListInbox();
    this.refreshInbox();
    this.getSendedEmail();
    this.getTrashEmail();
  }
  cancelCmp() {

    ////console.log("currentEmail: ",this.currentEmail.cbmarq)
    this.actualiteservice.deleteMessagerie(this.currentEmail.cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Email supprimer avec succès !');
          this.getInbox();
          this.getSendedEmail();
          this.getTrashEmail();
          this.viewListInbox();

        } else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox() {

    Swal.fire({
      title: 'Supprimer un Email',
      text: 'Voulez-vous vraiment supprimer cet email ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.cancelCmp();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  viewMoreDetail() {
    this.moreDetails = !this.moreDetails;
  }


  filterEmail() {

    ////console.log("value: ",""+this.searchEmail);
    let auxList = this.listMessagerie;

    this.listMessagerie = auxList.filter(word => (

      word?.objet.trim().toLowerCase().includes(this.searchEmail.trim().toLowerCase())

    ))

    // this prints the array of filtered

    if (this.searchEmail == "") {
      ////console.log("inn get inbox")

      if (this.isInbox) {
        this.getInbox();
        this.selectReception();
      }
      if (this.isSended) {
        this.getSendedEmail();
        this.selectSended();
      }
      if (this.isTrash) {
        this.getTrashEmail();
        this.selectTrash();
      }


      this.getTrashEmail();
    }
  }



}
