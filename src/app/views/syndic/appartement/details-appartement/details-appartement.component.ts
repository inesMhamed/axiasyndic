import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Reglement } from 'src/app/shared/models/reglement.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { ReglementService } from 'src/app/shared/services/reglement.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {Permission} from '../../../../shared/models/permission.model';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-details-appartement',
  templateUrl: './details-appartement.component.html',
  styleUrls: ['./details-appartement.component.scss'],
  providers:[DatePipe]
})
export class DetailsAppartementComponent implements OnInit {
  propAppartementForm: FormGroup
  listesProprietaires: any;
  proprietaires: any;
  colistProp: any;
  displayedColumns: string[] = ['reference', 'proprietaire', 'montant', 'statut', 'action'];//, 'montantPaye'
  dataSource = new MatTableDataSource<Reglement>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  id: string;
  appartement: any = [];
  appar: any = [];
  listreglement: any = [];
  listProp: any;
  listCooProp: any;
  propSelected: any[] = [];
  coProp: boolean = false;
  proprietairesAppart: any;
  propAppartment: any;
  copropAppartment: any=[];
  coproprietaires: any;
  can_addReg: Permission;
  can_editReg: Permission;
  can_deleteReg: Permission;
  can_detailReg: Permission;
  can_addP: Permission;
  can_retireP: Permission;
  can_addC: Permission;
  can_retireC: Permission;
  id_current_user: any;
  solvabilite: any;

  displayedColumnsSolv: string[] = ['date','type' , 'montantAPayer' , 'montantPaye' , 'statut'];
  dataSourceSolv = new MatTableDataSource<any>();
  @ViewChild('MatPaginatorSolv') MatPaginatorSolv: MatPaginator;
  @ViewChild('sortSolv') sortSolv: MatSort;

  displayedColumnsHist: string[] = ['dateSortie','resident' , 'raison' , 'type' , 'telephone'];
  dataSourceHist = new MatTableDataSource<any>();
  @ViewChild('MatPaginatorHist') MatPaginatorHist: MatPaginator;
  @ViewChild('sortHist') sortHist: MatSort;


  pageSize: any;
  infoPref: any;
  listFraisAppart: any;
  steps: any = [];
  datasteps: any;
  dateRemiseCle: any;
  datepipe: any;
  suppPropForm: FormGroup;
  submitted: boolean=false;
  propSupprimer: any;
  historique: any;
  exerciceReglement: any;
  dataExercice: any = [];

  listPeriodicite:any[]=[];
  periodiciteForm:FormGroup;
  submittedPeriodicite:boolean=false;

  have_access:boolean =false;
  dateremiseCleEmitted:string="";

  eventsSubject: Subject<void> = new Subject<void>();


  constructor(private route: ActivatedRoute, private appartementService: AppartementService,
    private spinnerservice: NgxSpinnerService,
    private reglementService: ReglementService, public router: Router, private proprietaireService: ProprietaireService,
    private formBuilder: FormBuilder, private modalService: NgbModal, private preferenceService: PreferencesService,
    private toastr: ToastrService, private permissionservice: DroitAccesService,private fbPeriodicite:FormBuilder
  ) {
    this.route.paramMap.subscribe(params => {
      if (params.get('cbmarq')) { this.id = params.get('cbmarq'); }
    });
    // if (this.id) {
    //   this.appartementService.getFraisAppartement(this.id).subscribe((res: any) => {
    //     if (res.statut == true) {
    //       if (res?.data?.alert != "") { this.toastr.warning(res?.data?.alert, 'Alert!', { progressBar: true }) }
    //       this.listFraisAppart = res?.data
    //       this.steps = res?.data?.annees
    //       this.datasteps = res?.data?.annees
    //       if (this.listFraisAppart?.dateRemiseCle) {
    //         this.dateRemiseCle = this.datepipe.transform(new Date(this.listFraisAppart?.dateRemiseCle?.date), 'dd-MM-yyyy');
    //       }
    //     } else {
    //       this.toastr.error(res?.message, 'Erreur!', { progressBar: true })
    //     }
    //   })
    // }
  }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux;
      this.infoPref = pre.data;
    });
    this.listeProprietaies();

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.id_current_user = localStorage.getItem('id');
    this.can_addC = this.permissionservice.search(this.id_current_user, 'FN21000104');
    this.can_retireC = this.permissionservice.search(this.id_current_user, 'FN21000105');
    this.can_addP = this.permissionservice.search(this.id_current_user, 'FN21000027');
    this.can_retireP = this.permissionservice.search(this.id_current_user, 'FN21000028');
    this.can_addReg = this.permissionservice.search(this.id_current_user, 'FN21000062');
    this.can_editReg = this.permissionservice.search(this.id_current_user, 'FN21000063');
    this.can_deleteReg = this.permissionservice.search(this.id_current_user, 'FN21000064');
    this.can_detailReg = this.permissionservice.search(this.id_current_user, 'FN21000065');
    if (this.id) {
      //FN21000025: can_detail
      //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000025').subscribe((res:any)=>{

      this.have_access =res.data;

    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{

        this.getAppartement();
        //this.getLogProp(this.id)
        //this.getSolvabilite(this.id);
        this.getlistReglement(this.id);
        this.getPropCoprop();
        this.getExercicesRegl(this.id)
      }

    });

    
    }
    this.propAppartementForm = this.formBuilder.group({
      proprietaire: new FormControl(null, Validators.required),
      dateremisebien:new FormControl(null)
    });
    //formGroup supp prop
    this.suppPropForm = this.formBuilder.group({
      raison: new FormControl("", Validators.required),
    });


    this.periodiciteForm = this.fbPeriodicite.group({
      periodicite: new FormControl(null, Validators.required),
    });

  }

  emitEventToChild() {
    this.eventsSubject.next();
  }

  getAppartement(){
    this.spinnerservice.show()
    this.appartement=[];
    this.appartementService.getAppartement(this.id).subscribe((res: any) => {

      this.appartement = res.data;

      //console.log("appartement: ",this.appartement)

      this.spinnerservice.hide()
    })
  }
  updatetableC() {
    this.getlistReglement(this.id);
  }
  getPropCoprop() {
 
    this.copropAppartment=[];
    this.appartementService.getpropOFappartment(this.id, 2).subscribe((res: any) => {
      this.copropAppartment = res.data;

    });
  }
  listePropAppartement(cbmarq, type) {
    this.proprietairesAppart=[];
    this.proprietaireService.getPropsAppartement(cbmarq, type).subscribe((res: any) => {
      this.proprietairesAppart = res.data
    })
  }
  tabChangeEvent(event) {
    if (event.nexId = "ngb-tab-2") {
      this.appartementService.getFraisAppartement(this.id).subscribe((res: any) => {
        if (res.statut == true) {
          if (res?.data?.alert != "") { this.toastr.warning(res?.data?.alert, 'Alert!', { progressBar: true }) }
          this.listFraisAppart = res?.data
          this.steps = res?.data?.annees
          this.datasteps = res?.data?.annees
          if (this.listFraisAppart?.dateRemiseCle) {
            this.dateRemiseCle = this.datepipe?.transform(new Date(this.listFraisAppart?.dateRemiseCle?.date), 'dd-MM-yyyy');
          }
        } else {
          this.toastr.error(res?.message, 'Erreur!', { progressBar: true })
        }
      })
    }
  }
  listeProprietaies() {
    this.spinnerservice.show()
    this.proprietaires=[];
    this.proprietaireService.getAllProprietaires().subscribe((res: any) => {
      this.proprietaires = res.data;
      this.spinnerservice.hide()
      
    },error=>{},()=>{

      this.spinnerservice.hide()
      if( this.appartement?.proprietaire?.length==0){

        this.propAppartementForm.controls["dateremisebien"].setValidators(Validators.required);
        this.propAppartementForm.controls['dateremisebien'].updateValueAndValidity();

      }else{

        this.propAppartementForm.controls['dateremisebien'].clearValidators;
        this.propAppartementForm.controls['dateremisebien'].updateValueAndValidity();

      }


    })
  }
  listeCoProprietaies() {
    this.spinnerservice.show()
    this.proprietaires=[];
    this.proprietaireService.getAllCoProprietaires().subscribe((res: any) => {
      this.proprietaires = res.data

      this.spinnerservice.hide()
    })
  }
  openPro(content) {
    this.listeProprietaies()
    this.coProp = false
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then((result) => {

      }, (reason) => {

      });
  }
  openCoprop(content) {
    this.listeCoProprietaies()
    this.coProp = true
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then((result) => {

      }, (reason) => {

      });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  getExercicesRegl(cbmarq) {
    this.spinnerservice.show()
    this.reglementService.geExerciceReglement(cbmarq).subscribe((reglement: any) => {

      if(reglement.statut){
        this.exerciceReglement = reglement.data

      }
      this.spinnerservice.hide()
    })
  }
  getDataExercice(event) {

    this.dataSource.data = event.value?.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, proprietaire: el.prop.nomComplet, montant: el.montant, rest: el.rest, statut: el.statut }))
  }
  getlistReglement(id) {
    this.spinnerservice.show()
    this.listreglement=[];
    this.reglementService.getReglementbyApaprtement(id).subscribe((res: any) => {

      if(res.statut){
        this.listreglement = res.data

        this.dataSource.data = res?.data?.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, proprietaire: el.prop.nomComplet, montant: el.montant, rest: el.rest, statut: el.statut }))
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.spinnerservice.hide()
    })
  }

  getSolvabilite() {
    this.spinnerservice.show()
    this.solvabilite=[];
    this.dataSourceSolv = new MatTableDataSource<any>();
    this.appartementService.getSolvabiliteAppartement(this.id).subscribe((res: any) => {
      if(res.statut){

        this.solvabilite = res.data;
        this.dataSourceSolv.data = res.data?.DetailsAutreFrais;
        this.dataSourceSolv.paginator = this.MatPaginatorSolv;
        this.dataSourceSolv.sort = this.sortSolv;
      }
      this.spinnerservice.hide()
    });
  }
  onSubmit() {


    this.spinnerservice.show()
    if(this.propAppartementForm.invalid){
      this.spinnerservice.hide()
      this.submitted=true;
      return;
    }


    
    this.propAppartementForm.value.proprietaire.map(element =>
      this.propSelected.push({ proprietaire: element })
    );
    if (!this.coProp) {
      this.proprietaireService.addPropAppartement({ proprietaires: this.propSelected, dateremisebien: this.propAppartementForm?.value?.dateremisebien }, this.id, 1).subscribe((app: any) => {
        if (app.statut) {
          this.toastr.success("Propriétaire est ajouté au bien avec succès !", 'Success!', { progressBar: true })
          this.modalService.dismissAll();
          this.propAppartementForm.reset()
          this.ngOnInit()
        }

        else {
          this.toastr.error(app.message, 'Erreur!', { progressBar: true })
        }
        this.spinnerservice.hide()
      })
    } else {
      this.proprietaireService.addPropAppartement({ proprietaires: this.propSelected , dateremisebien: this.propAppartementForm?.value?.dateremisebien}, this.id, 2).subscribe((app: any) => {
        if (app.statut) {
          this.toastr.success("Co-Propriétaire est ajouté au bien avec succès !", 'Success!', { progressBar: true })
          this.modalService.dismissAll();
          this.propAppartementForm.reset()
          this.ngOnInit()

        }
        else {
          this.toastr.error(app.message, 'Erreur!', { progressBar: true })
        }
        this.spinnerservice.hide()
      })
    }
  }
  editerReglement(id) {
    this.spinnerservice.show()
    this.reglementService.getReglementbyCbmarq(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0036") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier règlement confirmé ! ',
        })
      }
      else if (data.statut.code == "E0037") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier règlement annulé ! ',
        })
      } else {
        this.router.navigate(['/syndic/modifier-reglement/', id])
      }
      this.spinnerservice.hide()
    })

  }
  getDetails(id) {
    this.router.navigateByUrl('/syndic/reglements', { skipLocationChange: true }).then(() => {
      this.router.navigate([`/syndic/details-reglement/${id}`]);
    });

  }
  retirerProp(appart, prop) {

    let obj:any= { raison: "" };
    Swal.fire({
      title: 'Veuillez indiquer la raison de retirer ce proprietaire !',
      input: 'text',
      icon: 'error',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      showLoaderOnConfirm: true,
      preConfirm: (value) => {
        if (!value)
          Swal.showValidationMessage(
            '<i class="fa fa-info-circle"></i> Veuillez remplir ce champs !'
          )
        else {
          obj = { raison: value };
        }

      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.isConfirmed) {
        /*Swal.fire({
          title: `Retirer propriétaire`,
          text: ' Propriétaire a été retiré avec succès',
          icon: 'success'
        });*/
        this.spinnerservice.show()
        this.appartementService.deletePropAppartement(obj, appart, prop).subscribe((sup: any) => {

          if(sup.statut){
            this.toastr.success("Proprietaire supprimé avec succès !", 'Success!', { progressBar: true })
            this.getAppartement();

          }else{
            this.toastr.error(sup.message, 'Erreur!', { progressBar: true })
          }
          this.spinnerservice.hide()
        },
          (error: any) => {
            this.spinnerservice.hide()
            this.toastr.error("Veuillez réusser plus tard !", 'Erreur!', { progressBar: true })
          })

      }
    })
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une règlement',
      text: 'Voulez-vous vraiment supprimer ce règlement ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinnerservice.show()
        this.reglementService.deleteReglement(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistReglement(this.id)
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.spinnerservice.hide()
      }
    })
  }
  getLogProp() {
    this.spinnerservice.show()
    this.dataSourceHist = new MatTableDataSource<any>();

    this.appartementService.getHistoriqueProp(this.id).subscribe((histo: any) => {
      if(histo.statut){

        this.historique = histo.data
        this.dataSourceHist.data=histo.data;
        this.dataSourceHist.paginator=this.MatPaginatorHist;
        this.dataSourceHist.sort=this.sortHist;
      }
      this.spinnerservice.hide()
    })
  }
  removeCoprop(idappart, idprop) {
    Swal.fire({
      title: 'Retirer un copropriétaire',
      text: 'Voulez-vous vraiment retirer ce copriétaire ?',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinnerservice.show()
        let obj = { raison: null };
        this.appartementService.deletePropAppartement(obj, idappart, idprop).subscribe((ress: any) => {
          if (ress.statut == true) {
            this.toastr.success("Co-Propriétaire a été retiré avec succès !", 'Succès!', { progressBar: true });
            this.getPropCoprop();
          }
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            this.toastr.error("Veuillez réessayer plus tard !", 'Erreur!', { progressBar: true });
          }
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })

  }

  addReglement(){


    this.router.navigate(['/syndic/modifier-reglement/'+0+'/'+this.id])
  }

   moreOptionPeriodicite(content){
    this.listPeriodicite=[];

    this.appartementService.gettypesAppartement('PRD').subscribe((res:any)=>{

      if(res.statut){

        this.listPeriodicite=res.data.map(el => ({ value: el.cbmarq, label: el.label }));

        
      }
    },error=>{

    },()=>{
      this.dialogMoreOptions(content);
    })



  }

  async dialogMoreOptions(content){


      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then((result) => {

      }, (reason) => {

      });

  }

  onSubmitPeriodicite(){
    this.spinnerservice.show()
    if(this.periodiciteForm.invalid){
      this.spinnerservice.hide()
      this.submittedPeriodicite=true;
      return;
    }
      

    this.appartementService.postPeriodicite(this.id,this.periodiciteForm?.value?.periodicite).subscribe((res:any)=>{

      if (res.statut) {
        this.toastr.success("Périodicité modifié avec succès !", 'Success!', { progressBar: true })
        this.modalService.dismissAll();
        this.periodiciteForm.reset()
       this.getAppartement();
      }
      else {
        this.toastr.error(res.message, 'Erreur!', { progressBar: true })
      }
      this.spinnerservice.hide()
    })
  }

  changeDateRemiseCle(dateRemiseCle){
    
    this.getAppartement();

  }

  ConvertToFloat(val) {
    return val ? parseFloat(val).toFixed(this.infoPref?.round) : 0.000;
  }
}
