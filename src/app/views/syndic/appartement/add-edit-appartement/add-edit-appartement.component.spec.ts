import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditAppartementComponent } from './add-edit-appartement.component';

describe('AddEditAppartementComponent', () => {
  let component: AddEditAppartementComponent;
  let fixture: ComponentFixture<AddEditAppartementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditAppartementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditAppartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
