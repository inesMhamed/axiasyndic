import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import {Router} from '@angular/router';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-edit-appartement',
  templateUrl: './add-edit-appartement.component.html',
  styleUrls: ['./add-edit-appartement.component.scss']
})
export class AddEditAppartementComponent implements OnInit {
  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  appartementForm: FormGroup;
  residences: any = [];
  cbmarq: any;
  selectedCat: any;
  codeCat: any;
  blocs: any = [];
  types: any = [];
  categorie: any = [];
  natures: any = [];
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  constructor(private fb: FormBuilder,private cdRef:ChangeDetectorRef,private modalService: NgbModal,
    private appartementService: AppartementService,
    private blocService: BlocService,
    private spinnerservice: NgxSpinnerService,
    private residenceService: ResidenceService,
    private toastr: ToastrService, private  router: Router, private permissionservice: DroitAccesService,
    private dialogRef: MatDialogRef<AddEditAppartementComponent>, @Inject(MAT_DIALOG_DATA) public defaults: any
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    ////console.log("ngOnInit AddEdit APP")
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.getCategorie();
    this.getType();
    this.getNature();
    this.listeresidence();

    this.appartementForm = this.fb.group({
      categorie: new FormControl("", Validators.required),
      intitule: new FormControl("", Validators.required),
      residence: new FormControl(null, Validators.required),
      bloc: new FormControl(null,),
      type: new FormControl(null, Validators.required),
      nature: new FormControl(null, Validators.required),
      description: new FormControl("",),
      surface: new FormControl(null, Validators.min(1) ),
      surfaceCouverte: new FormControl(null, Validators.min(1) ),
    });
    if (this.router.url.includes("/syndic/modifier-residence/")) {
      this.spinnerservice.show()
      this.onchangeResidence( {value: parseInt(this.defaults.idres)});
      this.residenceService.getResidence(parseInt(this.defaults.idres)).subscribe((res: any) => {
        if (res.statut === true) {
          if ( res?.data?.type?.code === 'E0062') {
            let element =  this.categorie.find(itm => itm.code === 'E0064');
            if (element) {
              this.onChangeCat(element);
              this.selectedCat = element?.value;
              this.codeCat = element?.code;
            }
          } else {
            let element =  this.categorie.find(itm => itm.code === 'E0065');
            if (element) {
              this.onChangeCat(element);
              this.selectedCat = element?.value;
              this.codeCat = element?.code;
            }
          }
          this.appartementForm.patchValue({
            residence:  parseInt(this.defaults.idres)
          });
          this.spinnerservice.hide()
        }else{
          this.spinnerservice.hide()
        }
      },
          error => {
            this.spinnerservice.hide()
            //console.log('erreur: ', error);
          }
      );

    }
    if (this.router.url.includes("/syndic/details-residence/")) {
      this.spinnerservice.show()
      this.onchangeResidence( {value: parseInt(this.defaults.idres)});
      this.residenceService.getResidence(parseInt(this.defaults.idres)).subscribe((res: any) => {
            if (res.statut === true) {

              if ( res?.data?.type?.code === 'E0062') {
                let element =  this.categorie.find(itm => itm.code === 'E0064');
                if (element) {
                  this.onChangeCat(element);
                  this.selectedCat = element?.value;
                  this.codeCat = element?.code;
                }
              } else {
                let element =  this.categorie.find(itm => itm.code === 'E0065');
                if (element) {
                  this.onChangeCat(element);
                  this.selectedCat = element?.value;
                  this.codeCat = element?.code;
                }
              }
              this.appartementForm.patchValue({
                residence:  parseInt(this.defaults.idres)
              });
              this.spinnerservice.hide()
            }},
          error => {
            this.spinnerservice.hide()
            //console.log('erreur: ', error);
          }
      );


    }

    if (this.defaults.default) {
      ////console.log('janneeeeet', this.defaults.default)
      this.spinnerservice.show()
      this.onchangeResidence( {value: this.defaults.default?.residence?.cbmarq});
      this.selectedCat = this.defaults?.default?.categorie?.cbmarq;
      this.onChangeCat( this.defaults?.default?.categorie);
      this.appartementForm = this.fb.group({
        categorie: new FormControl(null || this.defaults?.default?.categorie?.cbmarq, Validators.required),
        intitule: new FormControl("" || this.defaults?.default?.intitule, Validators.required),
        residence: new FormControl(null || this.defaults?.default?.residence?.cbmarq, Validators.required),
        bloc: new FormControl(null || this.defaults?.default?.bloc?.cbmarq, Validators.required),
        type: new FormControl(null || this.defaults?.default?.type?.cbmarq, Validators.required),
        nature: new FormControl(null || this.defaults?.default?.nature?.cbmarq, Validators.required),
        description: new FormControl("" || this.defaults?.default?.description,),
        surface: new FormControl("" || this.defaults?.default?.surface , Validators.min(0)),
        surfaceCouverte: new FormControl(""|| this.defaults?.default?.surfaceCouverte , Validators.min(0)),
      });
      this.spinnerservice.hide()
    }

    // this.appartementForm.value modifier-residence
  }

  ngAfterViewChecked():void {
    this.cdRef.detectChanges();
}


  onSubmit() {
    this.spinnerservice.show()
    this.loading = true;
    //console.log(this.appartementForm.invalid, this.appartementForm);
    if (this.appartementForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    if (!this.defaults.default) {
      if (this.can_add?.permission) {
        this.appartementService.postAppartement(this.appartementForm.value).subscribe((res: any) => {
              if (res.statut === true) {
                this.submitted = false;
                this.toastr.success("Biens ajouté avec succès !", 'Success!', { progressBar: true })
                this.appartementForm.reset();
                this.onClose("submit");
                this.spinnerservice.hide()
              }
              else {
                this.spinnerservice.hide()
                this.submitted = true;
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
              }
              this.spinnerservice.hide()
            },
            error => {
              this.spinnerservice.hide()
              this.toastr.error('Erreur lors de l\'ajout  d\'un appartement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
              //console.log('erreur: ', error);
            }
        );
      } else {
        this.spinnerservice.hide()
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }
    } else {
      this.spinnerservice.show()
      this.appartementForm.value.cbmarq = this.defaults.default.cbmarq
      if (this.can_edit?.permission) {
        ////console.log("this.appartementForm", this.appartementForm.value.cbmarq, this.appartementForm.value);
        this.appartementForm.value
        this.appartementService.putAppartement(this.appartementForm.value, this.appartementForm.value.cbmarq).subscribe((res: any) => {
          if (res.statut == true) {
            this.toastr.success("Bien modifié avec succès !", 'Success!', { progressBar: true })
            this.submitted = false;
            this.onClose("submit")
          }
          else {
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            this.toastr.error('Erreur lors de la modification d\'un appartement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
          }
        );
      } else {
        this.spinnerservice.hide()
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }
    }
  }
  getType() {
    this.appartementService.gettypesAppartement('TAP').subscribe((res: any) => {
      this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
    },
      error => {
        //console.log('erreur: ', error);

      })
  }
  getCategorie() {
    this.appartementService.gettypesAppartement('TYB').subscribe((res: any) => {
      this.categorie = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label, code: clt.code }));
      if (!this.defaults.default){
        let element =  this.categorie.find(itm => itm.code === 'E0064');
        if (element) {
          this.selectedCat = element?.value;
          this.onChangeCat(element);
        }
      }
       ////console.log('this.categorie: ', this.categorie);
    },
      error => {
        //console.log('erreur: ', error);

      })
  }
  onChangeCat(event) {
    this.spinnerservice.show()
    const bloc = this.appartementForm.controls['bloc'];
    if (event && event.code) {
        this.codeCat = event?.code;
        if (event?.code === 'E0064') {
          bloc.setValidators([Validators.required]);
        } else {
          bloc.clearValidators();
        }
      bloc.updateValueAndValidity();
      this.appartementForm.patchValue({'residence': null});
      this.residenceService.getResidences().subscribe((res: any) => {
            if ( res.statut === true) {
              this.residences = [];
              res?.data.forEach(r => {
                if (this.codeCat === 'E0064' && r?.type?.code === 'E0062') {
                  this.residences.push({ value: r.cbmarq, label: r.intitule });
                }
                if (this.codeCat === 'E0065' && r?.type?.code === 'E0063') {
                  this.residences.push({ value: r.cbmarq, label: r.intitule });
                }
              });
              // this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
            }
            this.residences = this.residences.map(clt => ({ value: clt.value, label: clt.label }));
            this.spinnerservice.hide()
          },
          error => {
            this.spinnerservice.hide()
            //console.log('erreur: ', error);
          }
      );
    }
  }
  getNature() {
    this.appartementService.gettypesAppartement('NAP').subscribe((res: any) => {
      this.natures = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
    },
      error => {
        //console.log('erreur: ', error);

      })
  }
  onchangeResidence(event:any) {
    this.spinnerservice.show()
    if (event && event.value) {
      this.blocs = [];
      this.appartementForm.patchValue({'bloc': null});
      this.blocService.getBlocs(event.value).subscribe((res: any) => {
        if (res.data) {
          this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }))
        }
        this.spinnerservice.hide()
      },
        error => {
          this.spinnerservice.hide()
          //console.log('erreur: ', error);
        }
      );
    }
  }
  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
          if ( res.statut === true) {
            // res?.data.forEach(r => {
            //   if ( r?.type?.code === this.codeCat) {
            //     this.residences.push({ value: r.cbmarq, label: r.intitule });
            //   }
            // });
            this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
          }
          this.spinnerservice.hide()
            },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  onClose(target) {
    this.dialogRef.close(target);
    //this.modalService.dismissAll();
  }
}
