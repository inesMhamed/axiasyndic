import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, HostListener, Input, OnInit, Output, Pipe, ViewChild ,EventEmitter  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {MatHorizontalStepper, MatStepper} from '@angular/material/stepper';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { th } from 'date-fns/locale';
import { date } from 'ngx-custom-validators/src/app/date/validator';
import { ToastrService } from 'ngx-toastr';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Observable, Subscription } from 'rxjs';
import {  MomentDateAdapter,  MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {  DateAdapter,  MAT_DATE_FORMATS,  MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment, Moment } from 'moment';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from './dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY'
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};
@Component({
  selector: 'app-echeance-appartement',
  templateUrl: './echeance-appartement.component.html',
  styleUrls: ['./echeance-appartement.component.scss'],
  providers: [DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }, ]

})
export class EcheanceAppartementComponent implements OnInit {

  @ViewChild('dp') dp: MatDatepicker<Moment>;
  date = new FormControl(moment());
  _max: Moment | undefined;
  @Input()
  get max(): Moment | undefined {
    return this._max ? this._max : undefined;
  }
  set max(max: Moment | undefined) {
    if (max) {
      const momentDate: Moment =
          typeof max === 'number' ? moment([max, 0, 1]) : moment(max);
      this._max = momentDate.isValid() ? momentDate : undefined;
    }
  }
  _min: Moment | undefined;
  @Input()
  get min(): Moment | undefined {
    return this._min ? this._min : undefined;
  }
  set min(min: Moment | undefined) {
    if (min) {
      const momentDate =
          typeof min === 'number' ? moment([min, 0, 1]) : moment(min);
      this._min = momentDate.isValid() ? momentDate : undefined;
    }
  }

  // Function to call when the date changes.
  onChange = (year: Date) => {};

  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {}

  writeValue(date: Date): void {
    console.log('writeValue', date)
    if (date && this._isYearEnabled(date.getFullYear())) {
      const momentDate = moment(date);
      if (momentDate.isValid()) {
        momentDate.set({ date: 1 });
        this.date.setValue(moment(date), { emitEvent: false });
      }
    }
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean): void {
    isDisabled ? (this.dp.disabled = true) : (this.dp.disabled = false);

    isDisabled ? this.date.disable() : this.date.enable();
  }

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {

    datepicker.close();

    if (!this._isYearEnabled(normalizedYear.year())) {
      return;
    } else {
      this.echeanceForm.patchValue({annee: normalizedYear.year().toString()});
    }

    console.log('chosenYearHandler',  datepicker.close());
    normalizedYear.set({ date: 1 });

    this.date.setValue(normalizedYear, { emitEvent: false });
    // this.onTouched();
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    datepicker.close();
    this.date.setValue(normalizedMonth.month());
    this.onTouched();
  }

  _openDatepickerOnClick(datepicker: MatDatepicker<Moment>) {
    console.log('datepicker',  datepicker.opened );
    if (!datepicker.opened) {
      datepicker.open();
    }
  }


  // disable if the year is greater than maxDate lower than minDate
  private _isYearEnabled(year: number) {
    if (
        year === undefined ||
        year === null ||
        (this._max && year > this._max.year()) ||
        (this._min && year < this._min.year())
    ) {
      return false;
    }

    return true;
  }

  @Input() id: number;
  smallScreen: boolean;
  idc: any;
  echeanceForm: FormGroup
  appartement: any = [];
  months = [{ value: 1, label: '1 Mois' }, { value: 2, label: '2 Mois' }, { value: 3, label: '3 Mois' }, { value: 4, label: '4 Mois' }, { value: 5, label: '5 Mois' }, { value: 6, label: '6 Mois' }, { value: 7, label: '7 Mois' }, { value: 8, label: '8 Mois' }, { value: 9, label: '9 Mois' }, { value: 10, label: '10 Mois' }, { value: 11, label: '11 Mois' }, { value: 12, label: '12 Mois' }
  ];
  submitted: boolean=false;
  tr: any
  show: boolean = false
  tranche: any;
  arrayTranche: any = []
  annee: number;
  mois: number;
  fraisannuelle: any;
  jour: number;
  frais: number;
  somme: any;
  zero: number
  display: string;
  mode: boolean;
  listeFrais: any = [];
  dateremisecle: any;
  dataFrais: any = [];
  checked: boolean = false
  steps: any = [];
  selected: any;
  sommeNonpaye: any = 0;
  test: any[] = [];
  annee_courante: any;
  checkRemise: any
  // @ViewChild('stepper', { static: false })
  // stepper: MatHorizontalStepper;
  @ViewChild('stepper') private stepper: MatStepper;
  datatimeline: any;
  datasteps: any = [];
  dateRemiseCle: any = [];
  fraisannelleSelected: any;
  fraiscetteAnnee: any;
  selectedIndex: number = 0;
  smallwidth: any = 1001;
  can_addFrais: Permission;
  can_editFrais: Permission;
  id_current_user: any;
  listFraisAppart: any;
  check: boolean = false;
  pageSize: any;
  infoPref: any;
  currentDate: Date;
  checkYear: boolean;
  dataYear: any;

  selectedYearStepper: number = 0;
  selectedFraisAnnuel: number = 0;
  anneeRemiseCle: number = 0;
  @Output() dateRemiseCleChanged: EventEmitter<any> =   new EventEmitter<any>();
  private eventsSubscription: Subscription;
  @Input() events: Observable<void>;



  constructor(private route: ActivatedRoute, private appartementService: AppartementService,private dialog: MatDialog,
    private preferenceService: PreferencesService,
    private spinnerservice: NgxSpinnerService,
    private datepipe: DatePipe, private breakpointObserver: BreakpointObserver, private elementRef: ElementRef,
    private fb: FormBuilder, private toastr: ToastrService, private modalService: NgbModal, private permissionservice: DroitAccesService) {
    this.route.paramMap.subscribe(params => {
      if (params.get('cbmarq')) {
        this.idc = params.get('cbmarq')
      }
    });

    if (this.idc) {
      this.getFrais(this.idc)
      breakpointObserver.observe([
        Breakpoints.XSmall,
        Breakpoints.Small
      ]).subscribe(result => {
        this.smallScreen = result.matches;

      });
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    event.target.innerWidth;
    this.smallwidth = event.target.innerWidth

  }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
      this.infoPref = pre.data
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_addFrais = this.permissionservice.search(this.id_current_user, 'FN21000106');
    this.can_editFrais = this.permissionservice.search(this.id_current_user, 'FN21000107');
    this.submitted = false;
    this.currentDate = new Date();
    this.annee_courante = new Date().getFullYear();
    this.echeanceForm = this.fb.group({
      dateRemiseCle: new FormControl(new Date(), Validators.required),
      fraisAnnuelle: new FormControl(0, Validators.required),
      remise: new FormControl(null),
      annee: new FormControl(null, Validators.required),
      fraisCetteAnnee: new FormControl(null, Validators.required),
      M01: new FormControl(null, Validators.required),
      M02: new FormControl(null, Validators.required),
      M03: new FormControl(null, Validators.required),
      M04: new FormControl(null, Validators.required),
      M05: new FormControl(null, Validators.required),
      M06: new FormControl(null, Validators.required),
      M07: new FormControl(null, Validators.required),
      M08: new FormControl(null, Validators.required),
      M09: new FormControl(null, Validators.required),
      M10: new FormControl(null, Validators.required),
      M11: new FormControl(null, Validators.required),
      M12: new FormControl(null, Validators.required),
    });

    this.eventsSubscription = this.events.subscribe(() => {
      this.getAppartement(this.idc);
    });

    if (this.idc) {
      this.getAppartement(this.idc);
      // this.echeanceForm.patchValue({
      //   dateRemiseCle:this.datepipe.transform(this.currentDate,'yyyy-MM-dd'),
      // })
    }
    this.onchangeDate(this.datepipe.transform(this.currentDate, 'yyyy-MM-dd'));
    this.mode = false;
  }

  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }

  openModal(content) {
    this.mode = false;
    this.echeanceForm.reset();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'})
        .result.then((result) => {

    }, (reason) => {

      if (reason == undefined || reason == 0) {
        this.echeanceForm.reset();
        this.dataYear = null;
        this.submitted = false;
      }
    });
  }

  openEdit(content) {
    this.spinnerservice.show()
    this.mode = true;
    this.appartementService.getyearFrais(this.idc, this.selectedYearStepper).subscribe((res: any) => {
      this.dataYear = res.data;
      if (res.data.totalPourPayee === 0) {
        this.confirmBox();
      } else {
        this.echeanceForm.patchValue({
          remise: res.data.remise,
          fraisAnnuelle: res.data.fraisAnnuelle,
          annee: parseInt(res.data.annee),
          fraisCetteAnnee: parseFloat(res.data.fraisCetteAnnee),
          M01: parseFloat(res.data.mois.M01.montant),
          M02: parseFloat(res.data.mois.M02.montant),
          M03: parseFloat(res.data.mois.M03.montant),
          M04: parseFloat(res.data.mois.M04.montant),
          M05: parseFloat(res.data.mois.M05.montant),
          M06: parseFloat(res.data.mois.M06.montant),
          M07: parseFloat(res.data.mois.M07.montant),
          M08: parseFloat(res.data.mois.M08.montant),
          M09: parseFloat(res.data.mois.M09.montant),
          M10: parseFloat(res.data.mois.M10.montant),
          M11: parseFloat(res.data.mois.M11.montant),
          M12: parseFloat(res.data.mois.M12.montant),
        });
        if (this.echeanceForm.value.remise > 0) {
          this.checkChecked(true);
        } else {
          this.checkChecked(false);
        }
        // if (this.echeanceForm.value.annee === this.annee_courante) {
        if (this.echeanceForm.value.annee === this.anneeRemiseCle) {
          this.checkYear = true;
          this.echeanceForm.patchValue({
            dateRemiseCle: this.datepipe.transform(this.appartement?.dateremisebien?.date, 'yyyy-MM-dd')
          });
        } else {
          this.checkYear = false;
          this.echeanceForm.value.dateRemiseCle = null;
          this.echeanceForm.patchValue({
            dateRemiseCle: null
          });
        }


        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'})
            .result.then((result) => {

        }, (reason) => {

          if (reason == undefined || reason == 0) {
            this.echeanceForm.reset();
            this.dataYear = null;
            this.submitted = false;
          }

        });
      }
      this.spinnerservice.hide()
    });
  }
  onCloseHandled() {
    this.echeanceForm.reset()
    this.display = 'none';

  }

  onSubmit() {
    this.spinnerservice.show()
    this.submitted = true;
    if (this.echeanceForm.invalid) {
      this.spinnerservice.hide()
      return;
    }
    this.somme = this.echeanceForm.value.M01 + this.echeanceForm.value.M02 + this.echeanceForm.value.M03 + this.echeanceForm.value.M04 +
      this.echeanceForm.value.M05 + this.echeanceForm.value.M06 + this.echeanceForm.value.M07 + this.echeanceForm.value.M08 +
      this.echeanceForm.value.M09 + this.echeanceForm.value.M10 + this.echeanceForm.value.M11 + this.echeanceForm.value.M12
    if (this.somme > this.fraisannuelle) {
      this.toastr.error("La somme des frais est supérieure au frais de cette année !", 'Erreur!', { progressBar: true })
    }
    else {
     

      this.dateRemiseCleChanged.emit(this.echeanceForm.value.dateRemiseCle);

      
      this.appartementService.postFrais(this.idc, this.echeanceForm.value).subscribe((res: any) => {
        if (res.statut == false) {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true })
          this.echeanceForm.reset()
          this.getFrais(this.idc)
          this.ngOnInit();
        }
        else {
          this.toastr.success(res.message, 'Success!', { progressBar: true });
          this.dateRemiseCleChanged.emit(this.echeanceForm.value.dateRemiseCle);

          this.echeanceForm.reset()
          this.getFrais(this.idc)
          this.ngOnInit();
        }
        this.spinnerservice.hide()
      },
        error => {
          this.spinnerservice.hide()
          this.toastr.error("Erreur lors de l'ajouté frais syndic !", 'Erreur!', { progressBar: true })
        })
    }

  }
  onSubmit2() {
    this.spinnerservice.show()
    this.submitted = true;
    console.log('ccffjj', this.echeanceForm);
    if (this.echeanceForm.invalid) {
      this.spinnerservice.hide()
      return;
    }
    this.somme = this.echeanceForm.value.M01 + this.echeanceForm.value.M02 + this.echeanceForm.value.M03 + this.echeanceForm.value.M04 +
      this.echeanceForm.value.M05 + this.echeanceForm.value.M06 + this.echeanceForm.value.M07 + this.echeanceForm.value.M08 +
      this.echeanceForm.value.M09 + this.echeanceForm.value.M10 + this.echeanceForm.value.M11 + this.echeanceForm.value.M12;

      if (this.somme > this.echeanceForm.value.fraisCetteAnnee) {
        this.toastr.error("La somme des frais est supérieure au frais de cette année !", 'Erreur!', { progressBar: true });
    } else {
      if (this.mode) {
        console.log('this.echeanceForm.value',this.echeanceForm.value);
        this.appartementService.putFrais(this.idc, this.echeanceForm.value).subscribe((res: any) => {
          if (res.statut === true) {
            this.toastr.success("les informations syndic sont enregistrées avec succès !", 'Success!', { progressBar: true })
            this.dateRemiseCleChanged.emit(this.echeanceForm.value.dateRemiseCle);

            this.modalService.dismissAll()
            this.submitted = false;
            this.getFrais(this.idc);
            this.ngOnInit();
            this.stepper.selectedIndex = 0;
          }
          else
           { this.toastr.error(res.message, 'Erreur!', { progressBar: true })}
            this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            this.toastr.error("Erreur lors de l'ajout du frais syndic !", 'Erreur!', { progressBar: true })
          })
      }
      else {

        this.appartementService.postFraisAgain(this.idc, this.echeanceForm.value).subscribe((res: any) => {

          if (res.statut == true) {
            this.toastr.success("les informations syndic sont enregistrées avec succès !", 'Success!', { progressBar: true })
            this.submitted = false
            this.echeanceForm.reset()
            this.modalService.dismissAll()
            this.getFrais(this.idc)
            this.ngOnInit();
          }
          else
            {this.toastr.error(res.message, 'Erreur!', { progressBar: true })}
            this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            this.toastr.error("Erreur lors de l'ajouté frais syndic !", 'Erreur!', { progressBar: true })
          })
      }
    }

  }

  onchange(event) {
    if (!this.mode) {
      this.echeanceForm.removeControl('dateRemiseCle');
    } else {
      this.echeanceForm.controls['dateRemiseCle'].clearValidators;
      this.echeanceForm.controls['dateRemiseCle'].setValidators(null);
      this.echeanceForm.controls['dateRemiseCle'].updateValueAndValidity();
    }
    this.frais = event / 12;
    if (event && !this.mode) {
      this.echeanceForm.patchValue({
        fraisCetteAnnee: parseFloat(event),
        fraisAnnuelle: parseFloat(event),
        M01: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M02: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M03: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M04: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M05: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M06: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M07: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M08: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M09: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M10: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M11: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M12: parseFloat(this.frais.toFixed(this.infoPref?.round)),

      });
    } else if (event && this.mode) {
      this.echeanceForm.patchValue({
        fraisCetteAnnee: parseFloat(event),
        fraisAnnuelle: parseFloat(event),
      })
      var nbmois = 12;
      for (let i = 0; i < nbmois; i++) {

        if (this.dataYear.mois.M01.statut.code == 'E0028') { this.echeanceForm.patchValue({ M01: this.dataYear.mois.M01.montant }) } else { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M02.statut.code == 'E0028') { this.echeanceForm.patchValue({ M02: this.dataYear.mois.M02.montant }) } else { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M03.statut.code == 'E0028') { this.echeanceForm.patchValue({ M03: this.dataYear.mois.M03.montant }) } else { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };

        if (this.dataYear.mois.M04.statut.code == 'E0028') { this.echeanceForm.patchValue({ M04: this.dataYear.mois.M04.montant }) } else { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };

        if (this.dataYear.mois.M05.statut.code == 'E0028') { this.echeanceForm.patchValue({ M05: this.dataYear.mois.M05.montant }) } else { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M06.statut.code == 'E0028') { this.echeanceForm.patchValue({ M06: this.dataYear.mois.M06.montant }) } else { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M07.statut.code == 'E0028') { this.echeanceForm.patchValue({ M07: this.dataYear.mois.M07.montant }) } else { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M08.statut.code == 'E0028') { this.echeanceForm.patchValue({ M08: this.dataYear.mois.M08.montant }) } else { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M09.statut.code == 'E0028') { this.echeanceForm.patchValue({ M09: this.dataYear.mois.M09.montant }) } else { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M10.statut.code == 'E0028') { this.echeanceForm.patchValue({ M10: this.dataYear.mois.M10.montant }) } else { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M11.statut.code == 'E0028') { this.echeanceForm.patchValue({ M11: this.dataYear.mois.M11.montant }) } else { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
        if (this.dataYear.mois.M12.statut.code == 'E0028') { this.echeanceForm.patchValue({ M12: this.dataYear.mois.M12.montant }) } else { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
      }
    }

  }
  onChangeCetteAnnee(event) {
    if (event) {
      if (event > this.echeanceForm.value.fraisAnnuelle) {
        this.onChangeValue(this.echeanceForm.value.fraisAnnuelle);
      } else {
        this.frais = event / 12
        if (this.jour > 15)
          this.echeanceForm.value.fraisCetteAnnee = (event / (12) * (12 - this.mois))
        else this.echeanceForm.value.fraisCetteAnnee = (event / (12) * (12 - this.mois + 1))
        this.zero = 0
        var nbmois = 12;
        while (nbmois > 0) {
          switch (nbmois) {
            case 1: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M01: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M01: this.zero }) } } else { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 2: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M02: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M02: this.zero }) } } else { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 3: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M03: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M03: this.zero }) } } else { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 4: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M04: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M04: this.zero }) } } else { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 5: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M05: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M05: this.zero }) } } else { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 6: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M06: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M06: this.zero }) } } else { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 7: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M07: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M07: this.zero }) } } else { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 8: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M08: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M08: this.zero }) } } else { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 9: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M09: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M09: this.zero }) } } else { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 10: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M10: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M10: this.zero }) } } else { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 11: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M11: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M11: this.zero }) } } else { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 12: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M12: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M12: this.zero }) } } else { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
          }nbmois--;

        }
      }
    }
  }
  onChangeValue(event) {
    if (event) {
      this.frais = event / 12
      if (this.jour > 15)
        this.fraisannuelle = (event / (12) * (12 - this.mois))
      else this.fraisannuelle = (event / (12) * (12 - this.mois + 1));

    }

    this.zero = 0
    var nbmois = 12;
    while (nbmois > 0) {
      switch (nbmois) {
        case 1: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M01: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M01: this.zero }) } } else { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 2: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M02: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M02: this.zero }) } } else { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 3: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M03: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M03: this.zero }) } } else { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 4: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M04: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M04: this.zero }) } } else { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 5: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M05: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M05: this.zero }) } } else { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 6: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M06: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M06: this.zero }) } } else { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 7: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M07: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M07: this.zero }) } } else { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 8: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M08: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M08: this.zero }) } } else { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 9: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M09: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M09: this.zero }) } } else { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 10: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M10: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M10: this.zero }) } } else { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 11: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M11: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M11: this.zero }) } } else { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
        case 12: {
          if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M12: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M12: this.zero }) } } else { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
        }
      }nbmois--;
      this.echeanceForm.patchValue({
        annee: this.annee,
        fraisAnnuelle: parseFloat(event),
        fraisCetteAnnee: parseFloat(this.fraisannuelle.toFixed(this.infoPref?.round))
      });
    }
  }

  onchangeDate(event) {
    if (event) {
      this.annee = (new Date(event)).getFullYear()
      this.mois = (new Date(event)).getMonth() + 1
      this.jour = (new Date(event)).getDate()

      if (this.echeanceForm.value.fraisAnnuelle != '') {
        this.onChangeValue(this.echeanceForm.value.fraisAnnuelle);
      }
      this.echeanceForm.patchValue({
        annee: this.annee
      });
    }
  }

  getAppartement(id) {
    this.spinnerservice.show()
    this.appartementService.getAppartement(id).subscribe((res: any) => {
      if (res.statut) {
        this.appartement = res.data;
        if (this.appartement?.dateremisebien) {
          this.anneeRemiseCle = new Date(this.appartement?.dateremisebien?.date).getFullYear();
          this.echeanceForm.patchValue({
            dateRemiseCle: this.datepipe.transform(new Date(this.appartement?.dateremisebien?.date), 'yyyy-MM-dd'),
            fraisAnnuelle: this.appartement?.residence?.fraisSyndic,

          });
        } else {
          this.echeanceForm.patchValue({
            dateRemiseCle: this.datepipe.transform(this.currentDate, 'yyyy-MM-dd'),
            fraisAnnuelle: this.appartement?.residence?.fraisSyndic,

          });
        }
      }
      this.spinnerservice.hide()
    }, error => {
    }, () => {
      this.spinnerservice.hide()

      //this.onChangeValue(this.appartement?.residence?.fraisSyndic);
      this.onchangeDate(this.echeanceForm.value.dateRemiseCle);
    });
  }
  cancel() {
    this.echeanceForm.reset();
    this.modalService.dismissAll();
  }
  shown() {
    this.show = true
    this.checked = true
  }
  getFrais(id) {
    this.spinnerservice.show()
    this.appartementService.getFraisAppartement(id).subscribe((res: any) => {

      if (res.statut === true) {
        if (res?.data?.alert !== '') { this.toastr.warning(res?.data?.alert, 'Alert!', { progressBar: true }); }
        this.listFraisAppart = res?.data
        this.steps = res?.data?.annees
        this.datasteps = res?.data?.annees;

        if (this.datasteps.length > 0) {
          this.selectedYearStepper = this.datasteps[0].annee;
          this.selectedFraisAnnuel = this.datasteps[0].fraisAnnuelle;
        }

        if (this.listFraisAppart.dateRemiseCle) {
          this.dateRemiseCle = this.datepipe.transform(new Date(this.listFraisAppart?.dateRemiseCle?.date), 'dd-MM-yyyy');
          this.currentDate = this.listFraisAppart?.dateRemiseCle?.date;
        }
      } else {
        this.toastr.error(res?.message, 'Erreur!', { progressBar: true });
      }
      this.spinnerservice.hide()
    });
  }

  checkChecked(event) {
    this.check = event;
    if (this.check === true) {
      this.echeanceForm.controls['remise'].clearValidators;
      this.echeanceForm.controls['remise'].setValidators([Validators.required]);
      this.echeanceForm.controls['remise'].updateValueAndValidity();
    } else {
      this.echeanceForm.controls['remise'].clearValidators;
      this.echeanceForm.controls['remise'].setValidators([]);
      this.echeanceForm.controls['remise'].updateValueAndValidity();
      this.echeanceForm.patchValue({
        remise: null
      });
    }
  }

  confirmBox() {
    Swal.fire({
      icon: 'info',
      title: 'Les frais syndic de cette année sont totalement payées, vous ne pouvez pas les modifier ! ',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    });
  }

  /***wz */
  selectionChange(event: StepperSelectionEvent) {

    let stepLabel :any = event.selectedStep.label

    this.selectedYearStepper =+ stepLabel.annee;
    this.selectedFraisAnnuel =+ stepLabel.fraisAnnuelle;

  }

_openDialog() {
    this.dialog.open(DialogComponent, { data: this.idc }).afterClosed().subscribe(() => {
      // window.location.reload();
      this.getFrais(this.idc)
      this.submitted = false
      this.ngOnInit();
    });
}

}
