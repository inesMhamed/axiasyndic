import {Component, ElementRef, EventEmitter, HostListener, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {  MomentDateAdapter,  MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {  DateAdapter,  MAT_DATE_FORMATS,  MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { AppartementService } from '../../../../shared/services/appartement.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PreferencesService } from '../../../../shared/services/preferences.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { Observable, Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
    parse: {
        dateInput: 'YYYY'
    },
    display: {
        dateInput: 'YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};
@Component(
    {
        providers: [DatePipe,
            {
                provide: DateAdapter,
                useClass: MomentDateAdapter,
                deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
            },
            { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }, ],
    template: `        
  <div class="modal-content" matDialogTitle>
      <div class="modal-header">
          <h3 class="modal-title" id="transformModalLabel">Frais syndic pour une nouvelle année</h3>
      </div>
      <form   [formGroup]="echeanceForm" (ngSubmit)="onSubmit2()" class="px-0 col-md-12">
          <div class="modal-body" matDialogContent>
                  <div *ngIf="mode && checkYear" class="col-md-12 form-group mb-3 flex-line">
                      <div class="col-sm-6 pl-0">
                          <label for="picker1">Date Remise Clé </label>
                          <div class="input-group">
                              <input id="picker11" class="form-control" placeholder="yyyy-mm-dd"  [readonly]="dataYear?.statut?.code=='E0029'"
                                     formControlName="dateRemiseCle" type="date" (blur)='onchangeDate($event.target.value)'
                                     [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.dateRemiseCle.errors || (echeanceForm.controls.dateRemiseCle.errors  && echeanceForm.get('dateRemiseCle').touched ) }">

                              <div class="invalid-feedback " *ngIf="submitted &&  echeanceForm.controls.dateRemiseCle.errors || (echeanceForm.controls.dateRemiseCle.errors  && echeanceForm.get('dateRemiseCle').touched )">
                                  Veuillez remplir ce champs !
                              </div>
                          </div>
                      </div>
                      <div class="col-sm-6 pr-0">
                          <label> Frais Syndic Annuel </label>
                          <input type="number" class="form-control form-control-specialiser" step="0.01"
                                 formControlName="fraisAnnuelle" (blur)='onChangeValue($event.target.value)'
                                 [readonly]="!jour || !mois || !annee"
                                 [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.fraisAnnuelle.errors || (echeanceForm.controls.fraisAnnuelle.errors  && echeanceForm.get('fraisAnnuelle').touched )}" />
                          <div class="invalid-feedback " *ngIf="submitted &&  echeanceForm.controls.fraisAnnuelle.errors ">
                              Veuillez remplir ce champs !
                          </div>
                          <div class="invalid-feedback "
                               *ngIf="submitted &&  echeanceForm.controls.dateRemiseCle.errors || (echeanceForm.controls.fraisAnnuelle.errors  && echeanceForm.get('fraisAnnuelle').touched ) ">
                              Veuillez remplir date remise de clé au premier !
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12 form-group mb-2  flex-line">
                      <div *ngIf="mode && !checkYear" class="col-sm px-0">
                          <label> Frais Syndic de cette année </label>
                          <div class="input-group mb-3">
                              <input type="number" step="0.01" class="form-control " formControlName="fraisCetteAnnee"
                                     (blur)='onchange($event.target.value)'
                                     [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.fraisCetteAnnee.errors || (echeanceForm.controls.fraisCetteAnnee.errors  && echeanceForm.get('fraisCetteAnnee').touched )}" />

                              <div class="input-group-append">
                                  <span class="input-group-text" id="basic-addon2">{{infoPref?.devise}} </span>
                              </div>
                              <div class="invalid-feedback "
                                   *ngIf="submitted &&  echeanceForm.controls.fraisCetteAnnee.errors || (echeanceForm.controls.fraisCetteAnnee.errors  && echeanceForm.get('fraisCetteAnnee').touched )">
                                  Veuillez remplir ce champs !
                              </div>
                          </div>
                      </div>
                      <div *ngIf="!mode" class="col-sm-6 pl-0">
                          <!--                    <label for="picker1"> Année Frais </label>-->
                          <!--                    <input type="number" class="form-control " formControlName="annee"-->
                          <!--                        [ngClass]="{ 'is-invalid': submitted &&  (echeanceForm.controls.annee.errors || (echeanceForm.controls.annee.errors  && echeanceForm.get('annee').touched ))}" />-->
                          <div class="col-xl col-lg col-md col-sm pr-md-0" style="margin-top: auto;">
                              <label>Année Frais</label>
<!--                                                      <mat-form-field appearance="fill" class="col px-0">    formControlName="annee"  [formControl]="_inputCtrl" -->
                              
                              <input class="form-control"
                                      [matDatepicker]="dp" matInput value="{{valueannee}}"
                                      (click)="_openDatepickerOnClick(dp)"
                                      (keydown.arrowdown)="_openDatepickerOnClick(dp)"
                                      (keydown.enter)="_openDatepickerOnClick(dp)"
                                     [max]="_max"
                                     [min]="_min"
                                      readonly style="background: transparent;border: 1px solid #ced4da;font-size: small;font-weight: normal;padding-left: 3%;" >
<!--                              <mat-datepicker-toggle matSuffix [for]="dp"></mat-datepicker-toggle>-->
                              <mat-datepicker #dp
                                              startView="multi-year"
                                              (yearSelected)="chosenYearHandler($event, dp)"
                                              (monthSelected)="chosenMonthHandler($event, dp)"
                                              panelClass="jp-year-picker">
                              </mat-datepicker>
<!--                                                      </mat-form-field>-->
                              <div class="invalid-feedback "
                                   *ngIf="submitted &&  (echeanceForm.controls.annee.errors || (echeanceForm.controls.annee.errors  && echeanceForm.get('annee').touched ))">
                                  Veuillez remplir ce champs !
                              </div>
                          </div>
                      </div>
                      <div *ngIf="!mode" class="col-sm-6 pr-0">
                          <label> Frais Syndic </label>
                          <div class="input-group mb-3">
                              <input type="number" step="0.01" class="form-control " formControlName="fraisCetteAnnee"
                                     (blur)='onchange($event.target.value)'
                                     [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.fraisCetteAnnee.errors || (echeanceForm.controls.fraisCetteAnnee.errors  && echeanceForm.get('fraisCetteAnnee').touched )}" />

                              <div class="input-group-append">
                                  <span class="input-group-text" id="basic-addon2">{{infoPref?.devise}} </span>
                              </div>
                              <div class="invalid-feedback "
                                   *ngIf="submitted &&  echeanceForm.controls.fraisCetteAnnee.errors || (echeanceForm.controls.fraisCetteAnnee.errors  && echeanceForm.get('fraisCetteAnnee').touched )">
                                  Veuillez remplir ce champs !
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12 form-group mb-3">
                      <label>Remise mensuel </label>
                      <div class="flex-line pl-0">
                          <div class="col-md-9 pl-0">
                              <ng-select [items]="months" formControlName="remise" bindLabel="label" bindValue="value"
                                         [readonly]="!check" placeholder="Choisir nombre de mois"
                                         [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.remise.errors || (echeanceForm.controls.remise.errors  && echeanceForm.get('remise').touched )}">
                              </ng-select>
                              <div class="invalid-feedback "
                                   *ngIf="submitted &&  echeanceForm.controls.remise.errors || (echeanceForm.controls.remise.errors  && echeanceForm.get('remise').touched )">
                                  Veuillez remplir ce champs !
                              </div>
                          </div>
                          <div class="col-md-3 pr-0">
                              <label class="mt-2 switch switch-success mr-3">
                                  <span>Activer Remise</span>
                                  <input type="checkbox" [checked]="check" (change)="checkChecked($event.target.checked)">
                                  <span class="slider"></span>
                              </label>
                          </div>

                      </div>
                  </div>
                  <div class="col-md-12 form-group mb-3  ">
                      <label> Frais par mois </label>
                      <div class="col-sm">
                          <div [ngClass]="{'flex-line': smallwidth > 1000}">
                              <div>
                                  <div class="colonne p-3">Janvier</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M01?.statut?.style}}" formControlName="M01"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M01.errors || (echeanceForm.controls.M01.errors  && echeanceForm.get('M01').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Février</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M02?.statut?.style}}" formControlName="M02"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M02.errors || (echeanceForm.controls.M02.errors  && echeanceForm.get('M02').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Mars</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M03?.statut?.style}}" formControlName="M03"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M03.errors || (echeanceForm.controls.M03.errors  && echeanceForm.get('M03').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Avril</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M04?.statut?.style}}" formControlName="M04"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M04.errors || (echeanceForm.controls.M04.errors  && echeanceForm.get('M04').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Mai</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M05?.statut?.style}}" formControlName="M05"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M05.errors || (echeanceForm.controls.M05.errors  && echeanceForm.get('M05').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Juin</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M06?.statut?.style}}" formControlName="M06"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M06.errors || (echeanceForm.controls.M06.errors  && echeanceForm.get('M06').touched ) }" />
                                  </div>
                              </div>
                          </div>
                          <div [ngClass]="{'flex-line': smallwidth > 1000}">
                              <div>
                                  <div class="colonne p-3">Juillet</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M07?.statut?.style}}" formControlName="M07"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M07.errors || (echeanceForm.controls.M07.errors  && echeanceForm.get('M07').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Août</div>
                                  <div class="ligne p-3">
                                      <input type="number" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M08?.statut?.style}}" formControlName="M08"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M08.errors || (echeanceForm.controls.M08.errors  && echeanceForm.get('M08').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Septembre</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M09?.statut?.style}}" formControlName="M09"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M09.errors || (echeanceForm.controls.M09.errors  && echeanceForm.get('M09').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Octobre</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M10?.statut?.style}}" formControlName="M10"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M10.errors || (echeanceForm.controls.M10.errors  && echeanceForm.get('M10').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Novembre</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M11?.statut?.style}}" formControlName="M11"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M11.errors || (echeanceForm.controls.M11.errors  && echeanceForm.get('M11').touched ) }" />
                                  </div>
                              </div>
                              <div>
                                  <div class="colonne p-3">Décembre</div>
                                  <div class="ligne p-3">
                                      <input type="number" step="0.01" class="col-sm frais form-control bg-month-{{dataYear?.mois?.M12?.statut?.style}}" formControlName="M12"
                                             [ngClass]="{ 'is-invalid': submitted &&  echeanceForm.controls.M12.errors || (echeanceForm.controls.M12.errors  && echeanceForm.get('M12').touched ) }" />
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
          </div>
          <div class="modal-footer" matDialogActions>
              <button class="btn btn-primary" type="submit">Enregistrer</button>
              <button class="btn btn-secondary" type="button" matDialogClose  data-dismiss="modal">Annuler</button>
          </div>
      </form>
  </div>

    `,
})
export class DialogComponent implements OnInit {
    @ViewChild('dp') dp: MatDatepicker<Moment>;
    _inputCtrl: FormControl = new FormControl( moment([2020, 0, 1]));

    valueannee:string="";
    _max: Moment | undefined;
     @Input()
    get max(): number | undefined {
        return this._max.year() ? this._max.year() : undefined;
    }
    set max(max: number | undefined) {
        if (max) {
            const momentDate: Moment =
                typeof max === 'number' ? moment([max, 0, 1]) : moment(max);
            this._max = momentDate.isValid() ? momentDate : undefined;
        }
    }
    _min: Moment | undefined;
     @Input()
    get min(): number | undefined {
        return this._min.year() ? this._min.year() : undefined;
    }
    set min(min: number | undefined) {
        if (min) {
            const momentDate =
                typeof min === 'number' ? moment([min, 0, 1]) : moment(min);
            this._min = momentDate.isValid() ? momentDate : undefined;
        }
    }

    // Function to call when the date changes.
    onChange = (year: Date) => {};

    // Function to call when the input is touched (when a star is clicked).
    onTouched = () => {}



    // Allows Angular to disable the input.
    setDisabledState(isDisabled: boolean): void {
        isDisabled ? (this.dp.disabled = true) : (this.dp.disabled = false);

        isDisabled ? this._inputCtrl.disable() : this._inputCtrl.enable();
    }

    chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {

        datepicker.close();

        if (!this._isYearEnabled(normalizedYear.year())) {
            return;
        } else {
            this.echeanceForm.patchValue({annee: normalizedYear.year().toString()});
        }

        normalizedYear.set({ date: 1 });

        this.valueannee=normalizedYear.year().toString();
        this._inputCtrl.setValue(normalizedYear, { emitEvent: false });
        this.onTouched();
    }

    chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
        datepicker.close();
        this._inputCtrl.setValue(normalizedMonth.month());
        this.onTouched();
    }

    writeValue(date: Date): void {
        if (date && this._isYearEnabled(date.getFullYear())) {
            const momentDate = moment(date);
            if (momentDate.isValid()) {
                momentDate.set({ date: 1 });
                this._inputCtrl.setValue(moment(date), { emitEvent: false });
            }
        }
    }
    _openDatepickerOnClick(datepicker: MatDatepicker<Moment>) {
        console.log(this._inputCtrl)
        if (!datepicker.opened) {
            datepicker.open();
        }
    }

    // disable if the year is greater than maxDate lower than minDate
    private _isYearEnabled(year: number) {
        // console.log('this._max', this._max, this._min);
        if (
            year === undefined ||
            year === null ||
            (this._max && year > this._max.year()) ||
            (this._min && year < this._min.year())
        ) {
            return false;
        }

        return true;
    }
    currentDate: Date;
    submitted: boolean;
    mode: boolean;
    dataYear: any;
    annee_courante: any;
    somme: any;
    echeanceForm: FormGroup;
    annee: number;
    mois: number;
    jour: number;
    fraisannuelle: any;
    frais: number;
    zero: number;
    infoPref: any;
    pageSize: any;
    listFraisAppart: any;
    checkYear: boolean;
    dateRemiseCle: any = [];
    months = [{ value: 1, label: '1 Mois' }, { value: 2, label: '2 Mois' }, { value: 3, label: '3 Mois' }, { value: 4, label: '4 Mois' }, { value: 5, label: '5 Mois' }, { value: 6, label: '6 Mois' }, { value: 7, label: '7 Mois' }, { value: 8, label: '8 Mois' }, { value: 9, label: '9 Mois' }, { value: 10, label: '10 Mois' }, { value: 11, label: '11 Mois' }, { value: 12, label: '12 Mois' }
    ];
    check: boolean = false;
    smallwidth: any = 1001;
    appartement: any = [];
    steps: any = [];
    anneeRemiseCle: number = 0;
    momentDatemin: Moment | undefined;
    momentDatemax: Moment | undefined;
    numbreDatemax: number | undefined;
    @Output() dateRemiseCleChanged: EventEmitter<any> =   new EventEmitter<any>();
    private eventsSubscription: Subscription;
    @Input() events: Observable<void>;


    constructor(private route: Router, private appartementService: AppartementService,
        private spinnerservice: NgxSpinnerService,
        private preferenceService: PreferencesService, private dialogRef: MatDialogRef<DialogComponent>,
        private datepipe: DatePipe, private breakpointObserver: BreakpointObserver, private elementRef: ElementRef,
        private fb: FormBuilder, private toastr: ToastrService, @Inject(MAT_DIALOG_DATA) public idc: any) {

    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        event.target.innerWidth;
        this.smallwidth = event.target.innerWidth;
    }
    ngOnInit(): void {
        this.spinnerservice.show()
        this.preferenceService.getPreferences(1).subscribe((pre: any) => {
            this.pageSize = pre.data.affichageTableaux;
            this.infoPref = pre.data;
        });
        this.submitted = false;
        this.currentDate = new Date();
        this.annee_courante = new Date().getFullYear();
        this.echeanceForm = this.fb.group({
            dateRemiseCle: new FormControl(new Date(), Validators.required),
            fraisAnnuelle: new FormControl(0, Validators.required),
            remise: new FormControl(null),
            annee: new FormControl(null, Validators.required),
            fraisCetteAnnee: new FormControl(null, Validators.required),
            M01: new FormControl(null, Validators.required),
            M02: new FormControl(null, Validators.required),
            M03: new FormControl(null, Validators.required),
            M04: new FormControl(null, Validators.required),
            M05: new FormControl(null, Validators.required),
            M06: new FormControl(null, Validators.required),
            M07: new FormControl(null, Validators.required),
            M08: new FormControl(null, Validators.required),
            M09: new FormControl(null, Validators.required),
            M10: new FormControl(null, Validators.required),
            M11: new FormControl(null, Validators.required),
            M12: new FormControl(null, Validators.required),
        });
        // this.eventsSubscription = this.events.subscribe(() => {
        //     this.getAppartement(this.idc);
        // });
        this.getFrais(this.idc);
        this.onchangeDate(this.datepipe.transform(this.currentDate, 'yyyy-MM-dd'));
        this.mode = false;
    }
    onSubmit2() {
        this.submitted = true;
        console.log('ccffjj', this.echeanceForm);
        if (this.echeanceForm.invalid) {
            return;
        }
        this.somme = this.echeanceForm.value.M01 + this.echeanceForm.value.M02 + this.echeanceForm.value.M03 + this.echeanceForm.value.M04 +
            this.echeanceForm.value.M05 + this.echeanceForm.value.M06 + this.echeanceForm.value.M07 + this.echeanceForm.value.M08 +
            this.echeanceForm.value.M09 + this.echeanceForm.value.M10 + this.echeanceForm.value.M11 + this.echeanceForm.value.M12;

        if (this.somme > this.echeanceForm.value.fraisCetteAnnee) {
            this.toastr.error("La somme des frais est supérieure au frais de cette année !", 'Erreur!', { progressBar: true });
        } else {
            if (this.mode) {
                this.appartementService.putFrais(this.idc, this.echeanceForm.value).subscribe((res: any) => {
                        if (res.statut === true) {
                            this.toastr.success("les informations syndic sont enregistrées avec succès !", 'Success!', { progressBar: true })
                            this.dateRemiseCleChanged.emit(this.echeanceForm.value.dateRemiseCle);

                            // this.modalService.dismissAll()
                            this.echeanceForm.reset()
                            this.submitted = false;
                            this.getFrais(this.idc);
                            this.ngOnInit();
                            this.dialogRef.close();
                            // this.route.navigate(['/syndic/details-appartement/', this.idc]);
                            // this.stepper.selectedIndex = 0;
                        }
                        else
                            this.toastr.error(res.message, 'Erreur!', { progressBar: true });

                    },
                    error => {
                        this.toastr.error("Erreur lors de l'ajout du frais syndic !", 'Erreur!', { progressBar: true })
                    });
            } else {

                this.appartementService.postFraisAgain(this.idc, this.echeanceForm.value).subscribe((res: any) => {

                        if (res.statut == true) {
                            this.toastr.success("les informations syndic sont enregistrées avec succès !", 'Success!', { progressBar: true });
                            this.submitted = false
                            this.echeanceForm.reset()
                            this.getFrais(this.idc)
                            this.ngOnInit();
                            this.dialogRef.close();
                            // this.route.navigate(['/syndic/details-appartement/', this.idc]);
                        }
                        else
                            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                    },
                    error => {
                        this.toastr.error(error, 'Erreur!', { progressBar: true });
                    })
            }
        }

    }
    onchange(event) {
        if (!this.mode) {
            this.echeanceForm.removeControl('dateRemiseCle');
        } else {
            this.echeanceForm.controls['dateRemiseCle'].clearValidators;
            this.echeanceForm.controls['dateRemiseCle'].setValidators(null);
            this.echeanceForm.controls['dateRemiseCle'].updateValueAndValidity();
        }
        this.frais = event / 12;
        if (event && !this.mode) {
            this.echeanceForm.patchValue({
                fraisCetteAnnee: parseFloat(event),
                fraisAnnuelle: parseFloat(event),
                M01: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M02: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M03: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M04: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M05: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M06: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M07: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M08: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M09: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M10: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M11: parseFloat(this.frais.toFixed(this.infoPref?.round)),
                M12: parseFloat(this.frais.toFixed(this.infoPref?.round)),

            });
        } else if (event && this.mode) {
            this.echeanceForm.patchValue({
                fraisCetteAnnee: parseFloat(event),
                fraisAnnuelle: parseFloat(event),
            })
            var nbmois = 12;
            for (let i = 0; i < nbmois; i++) {

                if (this.dataYear.mois.M01.statut.code == 'E0028') { this.echeanceForm.patchValue({ M01: this.dataYear.mois.M01.montant }) } else { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M02.statut.code == 'E0028') { this.echeanceForm.patchValue({ M02: this.dataYear.mois.M02.montant }) } else { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M03.statut.code == 'E0028') { this.echeanceForm.patchValue({ M03: this.dataYear.mois.M03.montant }) } else { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };

                if (this.dataYear.mois.M04.statut.code == 'E0028') { this.echeanceForm.patchValue({ M04: this.dataYear.mois.M04.montant }) } else { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };

                if (this.dataYear.mois.M05.statut.code == 'E0028') { this.echeanceForm.patchValue({ M05: this.dataYear.mois.M05.montant }) } else { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M06.statut.code == 'E0028') { this.echeanceForm.patchValue({ M06: this.dataYear.mois.M06.montant }) } else { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M07.statut.code == 'E0028') { this.echeanceForm.patchValue({ M07: this.dataYear.mois.M07.montant }) } else { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M08.statut.code == 'E0028') { this.echeanceForm.patchValue({ M08: this.dataYear.mois.M08.montant }) } else { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M09.statut.code == 'E0028') { this.echeanceForm.patchValue({ M09: this.dataYear.mois.M09.montant }) } else { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M10.statut.code == 'E0028') { this.echeanceForm.patchValue({ M10: this.dataYear.mois.M10.montant }) } else { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M11.statut.code == 'E0028') { this.echeanceForm.patchValue({ M11: this.dataYear.mois.M11.montant }) } else { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
                if (this.dataYear.mois.M12.statut.code == 'E0028') { this.echeanceForm.patchValue({ M12: this.dataYear.mois.M12.montant }) } else { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) };
            }
        }

    }
    onchangeDate(event) {
        if (event) {
            this.annee = (new Date(event)).getFullYear()
            this.mois = (new Date(event)).getMonth() + 1
            this.jour = (new Date(event)).getDate()

            if (this.echeanceForm.value.fraisAnnuelle != '') {
                this.onChangeValue(this.echeanceForm.value.fraisAnnuelle);
            }
            this.echeanceForm.patchValue({
                annee: this.annee
            });
        }
    }
    onChangeValue(event) {
        if (event) {
            this.frais = event / 12
            if (this.jour > 15)
                this.fraisannuelle = (event / (12) * (12 - this.mois))
            else this.fraisannuelle = (event / (12) * (12 - this.mois + 1));

        }

        this.zero = 0
        var nbmois = 12;
        while (nbmois > 0) {
            switch (nbmois) {
                case 1: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M01: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M01: this.zero }) } } else { this.echeanceForm.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 2: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M02: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M02: this.zero }) } } else { this.echeanceForm.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 3: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M03: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M03: this.zero }) } } else { this.echeanceForm.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 4: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M04: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M04: this.zero }) } } else { this.echeanceForm.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 5: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M05: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M05: this.zero }) } } else { this.echeanceForm.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 6: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M06: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M06: this.zero }) } } else { this.echeanceForm.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 7: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M07: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M07: this.zero }) } } else { this.echeanceForm.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 8: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M08: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M08: this.zero }) } } else { this.echeanceForm.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 9: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M09: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M09: this.zero }) } } else { this.echeanceForm.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 10: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M10: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M10: this.zero }) } } else { this.echeanceForm.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 11: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M11: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M11: this.zero }) } } else { this.echeanceForm.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
                case 12: {
                    if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.echeanceForm.patchValue({ M12: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.echeanceForm.patchValue({ M12: this.zero }) } } else { this.echeanceForm.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
                }
            }nbmois--;
            this.echeanceForm.patchValue({
                annee: this.annee,
                fraisAnnuelle: parseFloat(event),
                fraisCetteAnnee: parseFloat(this.fraisannuelle.toFixed(this.infoPref?.round))
            });
        }
    }
    getFrais(id) {
        this.spinnerservice.show()
        this.appartementService.getFraisAppartement(id).subscribe((res: any) => {

            if (res.statut === true) {
                if (res?.data?.alert !== '') { this.toastr.warning(res?.data?.alert, 'Alert!', { progressBar: true }); }
                this.listFraisAppart = res?.data;
                // this.steps = res?.data?.annees;
                res?.data?.annees.forEach(r => {
                    this.steps.push(r.annee);
                });
                var maxyer = parseInt(this.steps[this.steps.length - 1]) + 1;
                this.steps.push(maxyer.toString());
                if (this.steps.length > 0) {
                    this.momentDatemin = moment([(this.steps[this.steps.length - 2]), 0, 1]);
                    this.numbreDatemax = this.steps[this.steps.length - 1];
                    this.momentDatemax = moment([(this.steps[this.steps.length - 1]), 0, 1]);
                    this._min = (this.momentDatemin);
                    this._max = (this.momentDatemax);
                }

                console.log('this.steps',  this.steps);
                // this.datasteps = res?.data?.annees;
                //
                // if (this.datasteps.length > 0) {
                //     this.selectedYearStepper = this.datasteps[0].annee;
                //     this.selectedFraisAnnuel = this.datasteps[0].fraisAnnuelle;
                // }
                console.log('this.listFraisAppart.dateRemiseCle', this.listFraisAppart.dateRemiseCle);
                if (this.listFraisAppart.dateRemiseCle) {
                    this.dateRemiseCle = this.datepipe.transform(new Date(this.listFraisAppart?.dateRemiseCle?.date), 'dd-MM-yyyy');
                    this.currentDate = this.listFraisAppart?.dateRemiseCle?.date;

                    this.momentDatemin = moment([new Date(this.currentDate).getFullYear() , 0, 1]);
                    this._min = (this.momentDatemin);
                }
                this.spinnerservice.hide()
            } else {
                this.spinnerservice.hide()
                this.toastr.error(res?.message, 'Erreur!', { progressBar: true });
            }
        });
    }
    checkChecked(event) {
        this.check = event;
        if (this.check === true) {
            this.echeanceForm.controls['remise'].clearValidators;
            this.echeanceForm.controls['remise'].setValidators([Validators.required]);
            this.echeanceForm.controls['remise'].updateValueAndValidity();
        } else {
            this.echeanceForm.controls['remise'].clearValidators;
            this.echeanceForm.controls['remise'].setValidators([]);
            this.echeanceForm.controls['remise'].updateValueAndValidity();
            this.echeanceForm.patchValue({
                remise: null
            });
        }
    }
    getAppartement(id) {
        this.spinnerservice.show()
        this.appartementService.getAppartement(id).subscribe((res: any) => {
            if (res.statut) {
                this.appartement = res.data;
                if (this.appartement?.dateremisebien) {
                    this.anneeRemiseCle = new Date(this.appartement?.dateremisebien?.date).getFullYear();
                    this.echeanceForm.patchValue({
                        dateRemiseCle: this.datepipe.transform(new Date(this.appartement?.dateremisebien?.date), 'yyyy-MM-dd'),
                        fraisAnnuelle: this.appartement?.residence?.fraisSyndic,

                    });
                } else {
                    this.echeanceForm.patchValue({
                        dateRemiseCle: this.datepipe.transform(this.currentDate, 'yyyy-MM-dd'),
                        fraisAnnuelle: this.appartement?.residence?.fraisSyndic,

                    });
                }
                this.spinnerservice.hide()
            }
            this.spinnerservice.hide()
        }, error => {
        }, () => {

            this.spinnerservice.hide()
            //this.onChangeValue(this.appartement?.residence?.fraisSyndic);
            this.onchangeDate(this.echeanceForm.value.dateRemiseCle);
        });
    }

}
