import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcheanceAppartementComponent } from './echeance-appartement.component';

describe('EcheanceAppartementComponent', () => {
  let component: EcheanceAppartementComponent;
  let fixture: ComponentFixture<EcheanceAppartementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcheanceAppartementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EcheanceAppartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
