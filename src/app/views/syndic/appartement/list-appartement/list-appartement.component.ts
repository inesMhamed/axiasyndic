import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ifError } from 'assert';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { elementAt } from 'rxjs/operators';
import { Appartement } from 'src/app/shared/models/appartement.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { AddEditAppartementComponent } from '../add-edit-appartement/add-edit-appartement.component';

@Component({
  selector: 'app-list-appartement',
  templateUrl: './list-appartement.component.html',
  styleUrls: ['./list-appartement.component.scss']
})
export class ListAppartementComponent implements OnInit {
  @Input() id: number;
  displayedColumns: string[] = ['type', 'intitule', 'prop', 'residence', 'bloc',  'statut', 'action']; // 'nature','proprietaire',
  dataSource = new  MatTableDataSource<Appartement>();
  pageEvent: PageEvent;
  compteurs;
  appartForm: FormGroup;
  loading: boolean;
  submitted: boolean=false;
  residences: any[] = [];
  types: any = [];
  appartements: any = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  data: any;
  listAppartement: any = [];
  dataApp: Appartement[] = [];
  appartementForm: FormGroup;
  natures: any = [];
  blocs: any = [];
  cbmarq: string;
  typeterrain: boolean = false;
  modeResidence: boolean;
  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;
  infoPref: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(
    private appartementService: AppartementService,
    private residenceService: ResidenceService, private fb: FormBuilder,
    private toastr: ToastrService, public router: Router,
    private preferenceService: PreferencesService,
    private spinnerservice: NgxSpinnerService,
    private blocService: BlocService, private modalService: NgbModal,
    private dialog: MatDialog, private route: ActivatedRoute, private permissionservice: DroitAccesService
  ) {
    //this.dataSource.data.length = 0;
    // this.dataSource.data = [];
  }

  ngOnInit(): void {
    ////console.log("ngOnInit list APP")
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000023').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        
        this.getType();
        this.getNature();
        this.listeresidence();
      }
    });

    this.appartementForm = this.fb.group({
      residence: new FormControl(null, Validators.required),
      bloc: new FormControl(null, Validators.required),
      nbrAppartement: new FormControl(0, [Validators.required, Validators.min(1)]),
      prefix: new FormControl("", Validators.required),
      suffix: new FormControl("", Validators.required),
      type: new FormControl(null, Validators.required),
      nature: new FormControl(null, Validators.required),
      description: new FormControl("",),
      surface: new FormControl(null, Validators.min(1) ),

    });
    this.route.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
      if (this.cbmarq) {
        this.modeResidence = true;
      }
    });
    if (this.cbmarq && this.modeResidence) {
       this.dataSource.data = [];
      this.appartementService.getAppartementResidence(this.cbmarq).subscribe((res: any) => {
        if(res.statut){
          // this.dataSource = [];
          this.dataSource.data = res?.data?.map(el => ({ cbmarq: el.cbmarq, type1: el?.type?.label, intitule: el?.intitule, residence1: el?.residence?.intitule,residence: el?.residence,bloc: el?.bloc, bloc1: el?.bloc?.intitule, statut1: el?.statut, statut: el?.statut.label,surface:el?.surface,surfaceCouverte:el?.surfaceCouverte,description:el?.description,type:el?.type,nature:el?.nature,prop: el?.proprietaire?.shift() }));
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

        }
      });
      this.onchangeResidence({value: this.cbmarq});
      this.residenceService.getResidence(this.cbmarq).subscribe((res: any) => {
            if (res.statut === true) {
              if (res.data?.type?.code === 'E0063') {
                this.typeterrain = true;
              }
            }
          },
          error => {
            //console.log('erreur: ', error);
          }
      );
    } else {
      this.getAppartements();
    }
  }
  open(content) {
    if (this.cbmarq && this.modeResidence) {
      this.appartementForm.patchValue({ residence: parseInt(this.cbmarq) });
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title1' })
      .result.then((result) => {
        ////console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });


     

  }
  getAppartements() {
    this.spinnerservice.show()
    this.appartementService.getAppartements().subscribe((res: any) => {
      if(res.statut){
        this.dataApp = res.data;

        this.dataSource.data = res.data.map(el => ({ cbmarq: el.cbmarq, type1: el?.type?.label, intitule: el?.intitule, residence1: el?.residence?.intitule,residence: el?.residence,bloc: el?.bloc, bloc1: el?.bloc?.intitule, statut1: el?.statut, statut: el?.statut.label,surface:el?.surface,surfaceCouverte:el?.surfaceCouverte,description:el?.description,type:el?.type,nature:el?.nature,categorie:el?.categorie,prop: el?.proprietaire?.shift() }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

      this.spinnerservice.hide()
    });
  }
  getAppartementsbyProduit(id) {
    ////console.log('cc2');
    this.spinnerservice.show()
    this.appartementService.getAppartementResidence(id).subscribe((res: any) => {

      if(res.statut){
        this.dataSource.data = res.data.map(el => ({ cbmarq: el.cbmarq, type1: el?.type?.label, intitule: el?.intitule, residence1: el?.residence?.intitule,residence: el?.residence,bloc: el?.bloc, bloc1: el?.bloc?.intitule, statut1: el?.statut, statut: el?.statut.label,surface:el?.surface,surfaceCouverte:el?.surfaceCouverte,description:el?.description,type:el?.type,nature:el?.nature,categorie:el?.categorie,prop: el?.proprietaire?.shift() }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.spinnerservice.hide()
    });
  }
  onSubmit() {
    this.spinnerservice.show()
    this.loading = true;
    this.submitted = true;
    if (this.appartementForm.invalid) {
      this.spinnerservice.hide()
      return;
    }
    this.appartementService.genererAppartement(this.appartementForm.value).subscribe((res: any) => {
      if (res.statut === true) {
        this.submitted = false;
        this.appartementForm.reset();
        this.modalService.dismissAll()
        if (this.cbmarq && this.modeResidence) {
          if (this.router.url.includes("/syndic/modifier-residence/")) {
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/syndic/modifier-residence/', parseInt(this.cbmarq)]);
            });
          }
          this.getAppartementsbyProduit(this.cbmarq);
          this.ngOnInit()
        } else {
          this.getAppartements();
          this.ngOnInit()
        }
        this.toastr.success("Appartement généré avec succès !", 'Success!', { progressBar: true })
      }
      else {
        this.submitted = true;
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de la génération  d\'un appartement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      }
    )

  }
  onchangeResidence(event: any) {
    this.spinnerservice.show()
    this.blocs = [];
    this.appartementForm.patchValue({'bloc': null});
    if (event && event.value) {
      this.blocService.getBlocs(event.value).subscribe((res: any) => {
        if (res.data) {
          this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }))
        }
        this.spinnerservice.hide()
      },
        error => {
          this.spinnerservice.hide()
          //console.log('erreur: ', error);
        }
      );
    }
  }
  getAppartementsResidence(id) {
    this.spinnerservice.show()
    this.appartementService.getAppartementResidence(id).subscribe((res: any) => {
      if (res.data) {
        // this.dataSource1.data = res.data
      }
      this.spinnerservice.hide()
      // this.dataSource.data = res.data.map(el => ({ intitule: el.intitule, residence: el.bloc.residence.intitule, bloc: el.bloc.intitule, proprietaire: el.proprietaire, type: el.type.label, nature: el.nature.label, statut: el.statut.label }))
    })
  }
  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      if (res.statut) {
        res?.data.forEach(r => {
          if ( r?.type?.code === 'E0062') {
            this.residences.push({ value: r.cbmarq, label: r.intitule });
          }
        });
        // this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  addAppartement() {
    this.spinnerservice.show();
    this.dialog.open(AddEditAppartementComponent, { data: { default: '', idres: this.cbmarq } }).afterClosed().subscribe((res) => {

      //console.log('this.cbmarq && this.modeResidence', this.cbmarq && this.modeResidence, "res: ",res)
      if(res!="close"){
        if (this.cbmarq && this.modeResidence) {
          if (this.router.url.includes("/syndic/modifier-residence/")) {
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate(['/syndic/modifier-residence/', parseInt(this.cbmarq)]);
            });
          }
          this.getAppartementsbyProduit(this.cbmarq);
          this.ngOnInit();
        } else {
          this.getAppartements();
          this.ngOnInit();
        }
      }
      
    });
  }
  editAppartement(residence) {
    ////console.log('resid:',residence)
    this.spinnerservice.show()
    this.dialog.open(AddEditAppartementComponent, { data: { default: residence, idres: this.cbmarq } }).afterClosed().subscribe((res) => {

      if(res!="close"){
        if (this.cbmarq && this.modeResidence) {
          this.appartementService.getAppartementResidence(this.cbmarq).subscribe((res: any) => {
            if(res.statut){
              this.dataSource.data = [];
              this.dataSource.data = res.data?.map(el => ({ cbmarq: el.cbmarq, type1: el?.type?.label, intitule: el?.intitule, residence1: el?.residence?.intitule,residence: el?.residence.cbmarq,bloc: el?.bloc?.cbmarq, bloc1: el?.bloc?.intitule, statut1: el?.statut, statut: el?.statut.label, surface: el?.surface, surfaceCouverte: el?.surfaceCouverte, description: el?.description, type: el?.type, nature: el?.nature, categorie: el?.categorie }));
            
            }
            this.spinnerservice.hide()
          });
        } else {
          this.spinnerservice.show()
          this.getAppartements();
          this.ngOnInit()
        }
      }

    });

  }
  deleteAppartement(cbmarq: any) {
    this.spinnerservice.show()
    this.appartementService.deleteAppartement(cbmarq).subscribe(
      (res: any) => {
        if ( res.statut === true) {
          this.toastr.success('Bien supprimer avec succès !');
        } else {
          this.toastr.error(res.message);
        }
        if (this.cbmarq && this.modeResidence) {
          this.getAppartementsbyProduit(this.cbmarq);
        } else {
          this.getAppartements();
        }
        this.spinnerservice.hide()
      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un appartement',
      text: 'Voulez-vous vraiment supprimer cet appartement ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.spinnerservice.show()
        this.deleteAppartement(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.spinnerservice.hide()
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  getType() {
    this.appartementService.gettypesAppartement('TAP').subscribe((res: any) => {
      if(res.statut){
        this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));

      }
    },
      error => {
        //console.log('erreur: ', error);

      })
  }
  getNature() {
    this.appartementService.gettypesAppartement('NAP').subscribe((res: any) => {
      if(res.statut){
        this.natures = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));

      }
    },
      error => {
        //console.log('erreur: ', error);

      })
  }
}
