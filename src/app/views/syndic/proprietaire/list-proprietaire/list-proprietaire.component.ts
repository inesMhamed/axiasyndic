import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { Proprietaire } from 'src/app/shared/models/proprietaire.model';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { Depense } from 'src/app/shared/models/depense.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatInput } from '@angular/material/input';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-list-proprietaire',
  templateUrl: './list-proprietaire.component.html',
  styleUrls: ['./list-proprietaire.component.scss']
})
export class ListProprietaireComponent implements OnInit {
  displayedColumns: string[] = ['nomcomplet', 'username', 'email', 'telephone', 'statut', 'action'];
  displayedColumns1: string[] = ['nomcomplet', 'username', 'email', 'telephone', 'statut', 'action'];
  dataSource = new MatTableDataSource<Proprietaire>([]);
  dataSource1 = new MatTableDataSource<Proprietaire>([]);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator1: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  can_addP: Permission;
  can_addC: Permission;
  can_edit: Permission;
  can_active: Permission;
  can_listeP: Permission;
  can_listeC: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(public router: Router, private route: ActivatedRoute, private preferenceService: PreferencesService,
    private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService, private proprietaireService: ProprietaireService, private permissionservice: DroitAccesService
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_addP = this.permissionservice.search( this.id_current_user, 'FN21000039');
    this.can_addC = this.permissionservice.search( this.id_current_user, 'FN21000040');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000101');
    this.can_active = this.permissionservice.search( this.id_current_user, 'FN21000102');
    this.can_listeP = this.permissionservice.search( this.id_current_user, 'FN21000041');
    this.can_listeC = this.permissionservice.search( this.id_current_user, 'FN21000042');

    this.checkAccessListProp();

    this.getlistProp();
    this.getlistCoProp();
    
  }

  
  checkAccessListProp(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, "FN21000041").subscribe((res:any)=>{

     this.have_access =res.data;
    
   },error=>{},()=>{

    this.checkListAccessCoProp();
    
   });
  }

  checkListAccessCoProp(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, "FN21000042").subscribe((res:any)=>{

      if(!this.have_access && !res.data){
        this.router.navigate(['403']);
      }
      
    
    },error=>{},()=>{
     
    
    });
  }


  updatetableP() {
    this.getlistProp();
  }
  updatetableC() {
    this.getlistCoProp();
  }
  // ngAfterViewInit() {
  // }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  applyFilter1(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource1.filter = filterValue.trim().toLowerCase();
    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }
  }
  getlistProp() {
    this.spinnerservice.show()
    this.proprietaireService.getAllProprietaires().subscribe((res: any) => {
      if(res.statut){
        console.log(res)
        let ids = res.data.map(o => o.id)
        let filtered = res.data.filter(({id}, index) => !ids.includes(id, index + 1))
  
        this.dataSource.data =filtered;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.spinnerservice.hide()

   
    });
  }



  getlistCoProp() {
    this.spinnerservice.show()
    this.proprietaireService.getAllCoProprietaires().subscribe((res: any) => {

      if(res.statut){
        let ids = res.data.map(o => o.id)
        let filtered = res.data.filter(({id}, index) => !ids.includes(id, index + 1))
  
        this.dataSource1.data = filtered;
        this.dataSource1.paginator = this.paginator1;
        this.dataSource1.sort = this.sort1;
      }
      this.spinnerservice.hide()
    });
  }
  cancelProp(cbmarq: any) {
    Swal.fire({
      title: 'Désactiver un propriétaire',
      text: 'Voulez-vous vraiment désactiver ce propriétaire ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinnerservice.show()
        this.proprietaireService.cancelProprietaire(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistProp()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  cancelCoProp(cbmarq: any) {
    Swal.fire({
      title: 'Désactiver un Co-propriétaire',
      text: 'Voulez-vous vraiment désactiver ce co-propriétaire ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proprietaireService.cancelProprietaire(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistCoProp()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  activeProp(cbmarq: any) {
    Swal.fire({
      title: 'Activer un propriétaire',
      text: 'Voulez-vous vraiment activer ce propriétaire ?',
      icon: 'success',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proprietaireService.confirmProprietaire(cbmarq).subscribe((ress: any) => {

          if (ress.statut === true)
            this.getlistProp()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  activeCoProp(cbmarq: any) {
    Swal.fire({
      title: 'Activer un co-propriétaire',
      text: 'Voulez-vous vraiment activer ce co-propriétaire ?',
      icon: 'success',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proprietaireService.confirmProprietaire(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistCoProp()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
}
