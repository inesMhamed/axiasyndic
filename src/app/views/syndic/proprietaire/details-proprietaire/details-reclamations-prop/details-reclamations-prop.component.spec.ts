import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsReclamationsPropComponent } from './details-reclamations-prop.component';

describe('DetailsReclamationsPropComponent', () => {
  let component: DetailsReclamationsPropComponent;
  let fixture: ComponentFixture<DetailsReclamationsPropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsReclamationsPropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsReclamationsPropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
