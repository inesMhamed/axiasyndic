import { Component, OnChanges, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FicheClient } from 'src/app/shared/models/fiche-client.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';

class Proprietaire{


  active: boolean;
  adresse: string;
  avance: any;
  email: string;
  id: number;
  identifiant: any;
  nomComplet: string;
  objects: any;
  photo: any;
  profession: any;
  proprietaire: boolean;
  telephone1: string;
  telephone2: string;
  type: any;
  username: string;
  roles:any;
  constructor(){

  }
}



@Component({
  selector: 'app-details-proprietaire',
  templateUrl: './details-proprietaire.component.html',
  styleUrls: ['./details-proprietaire.component.scss']
})
export class DetailsProprietaireComponent implements OnInit, OnChanges {
  fileToUpload: File = null;
  showcardpawwsord: boolean = false
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;

  id: string;
  imgSrc: any
  proprietaire: Proprietaire = new Proprietaire();
  prop: boolean;
  url: any;
  proprietaireForm: FormGroup;
  infoPref: any;
  currentDate:Date=new Date();
  ficheClient:FicheClient=new FicheClient();

  pagedListDocument:any[]=[];
  breakpoint: number = 4;  //to adjust to screen
  pageEvent:any;
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  isproprietaire:boolean=false;
  isUserInterne:boolean=false;

  constructor(private proprietaireService: ProprietaireService,
    private actRoute: ActivatedRoute, private router: Router,
    private toastr: ToastrService, private formBuilder: FormBuilder,
    private spinnerservice: NgxSpinnerService,
    private preferenceService: PreferencesService,private permissionservice: DroitAccesService
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000113');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000111');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000112');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000110');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000098');
    this.currentDate=new Date();
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
      this.infoPref = pre.data
    })

    this.proprietaireForm = this.formBuilder.group({
      photo: null

    })
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('id');

    });
    if (this.router.url.includes("/syndic/details-proprietaire/")) {
      this.prop = true
    }
    if (this.id != null) {
     this.getProprietaire();
     this.getFicheClient();
    }

  }

  getProprietaire(){
    this.spinnerservice.show()
    this.proprietaireService.getProprietairebyId(this.id).subscribe((pro: any) => {
      if (pro.statut) {
        this.proprietaire = pro.data;

        this.isUserInterne=this.proprietaire.roles.includes("ROLE_INTERN");
        if(!this.isUserInterne){
            this.isproprietaire=this.proprietaire.proprietaire;
        }

        this.imgSrc = pro.data.photo
        this.proprietaireForm.patchValue({
          photo: this.proprietaire.photo
        })
      }
      this.spinnerservice.hide()
    })


  }

  getFicheClient(){
    this.spinnerservice.show()
    this.proprietaireService.getFicheClient(this.id).subscribe((res: any) => {
      if (res.statut === true) {
        this.ficheClient=res;
      }
      this.spinnerservice.hide()
    })
  }
  goto(id) {
    if (this.router.url.includes("/syndic/details-utilisateur/")) {
      this.router.navigate(['/syndic/modifier-utilisateur/', id])
    } else if (this.router.url.includes("/syndic/details-proprietaire/")) {
      this.router.navigate(['/syndic/modifier-proprietaire/', id])
    } else {
      this.router.navigate(['/syndic/modifier-coproprietaire/', id])
    }
  }
  onsubmit() {
    this.spinnerservice.show()
    const myFormValue = this.proprietaireForm.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    //console.log('proprietaireForm', this.proprietaireForm.value)
    this.proprietaireService.editphoto(myFormData, this.id).subscribe((pro: any) => {
      if (pro.statut === true) {
        this.toastr.success("Photo de profil est modifiée avec succès !", 'Succès!', { progressBar: true });
        this.ngOnInit()
      } else {
        this.toastr.error("Veuillez réessayer plus tard !", 'Erreur!', { progressBar: true });
      }
      this.spinnerservice.hide()
    })
  }
  ngOnChanges() {
    this.proprietaireForm.value
  }
  onSelectFile(file: FileList) {
    this.url = file.item(0);

    //Show image preview
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.imgSrc = event.target.result;
    }
    //console.log('url', this.imgSrc, this.url)
    reader.readAsDataURL(this.url);
    this.proprietaireForm.patchValue({
      photo: this.url
    })

    this.onsubmit()
  }
  onFileChange(files: FileList) {
    this.fileToUpload = files.item(0);

    if (files.length > 0) {
      const file = files[0];
      this.proprietaireForm.patchValue({
        photo: file
      });
      this.onsubmit()
    }
  }
  onSelectImage(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.imgSrc = event.target.result;
      };
      this.proprietaireForm.patchValue({
        photo: this.imgSrc
      })
      this.onsubmit()

    }
  }
}
