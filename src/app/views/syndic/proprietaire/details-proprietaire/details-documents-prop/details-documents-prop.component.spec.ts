import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsDocumentsPropComponent } from './details-documents-prop.component';

describe('DetailsDocumentsPropComponent', () => {
  let component: DetailsDocumentsPropComponent;
  let fixture: ComponentFixture<DetailsDocumentsPropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsDocumentsPropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsDocumentsPropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
