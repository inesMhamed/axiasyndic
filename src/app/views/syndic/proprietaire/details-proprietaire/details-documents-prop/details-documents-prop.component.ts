import { Component, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeNode } from 'primeng/api';
import { Appartement } from 'src/app/shared/models/appartement.model';
import { CategorieGetDoc } from 'src/app/shared/models/categorieGetDoc';
import { FicheClient } from 'src/app/shared/models/fiche-client.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ActualiteService } from 'src/app/shared/services/actualite.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';

@Component({
  selector: 'app-details-documents-prop',
  templateUrl: './details-documents-prop.component.html',
  styleUrls: ['./details-documents-prop.component.scss']
})
export class DetailsDocumentsPropComponent implements OnInit {

  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;

  id: string;
  ficheClient:FicheClient=new FicheClient();
  pagedListDocument:any[]=[];
  breakpoint: number = 4;  //to adjust to screen
  pageEvent:any;
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 10;  //displaying three cards each row 
  pageSizeOptions: number[] = [5, 10, 25, 100];

  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['Intitule'  ,'Date création', 'Date modification', 'action'];

  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;

  //files1: TreeNode[];
  listAppartements:any[];
  searchProduit:any;
  currentbien:Appartement=new Appartement();

  categorieGetDocModel:CategorieGetDoc=new CategorieGetDoc();
  nodeKey:string;

  constructor( private actRoute: ActivatedRoute, private router: Router,private proprietaireService: ProprietaireService,private permissionservice: DroitAccesService,
    private actualiteService:ActualiteService) { }

  ngOnInit(): void {

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000113');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000111');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000112');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000110');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000098');

   


    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('id');

    });

    if (this.id != null) {
      this.getFicheClient();      
     }

  }

  
getRandomColor() {
  if(this.listAppartements!=null && this.listAppartements.length>0){
    for(let produit of this.listAppartements){
      var color = Math.floor(0x1000000 * Math.random()).toString(16);
      produit.color='#' + ('000000' + color).slice(-6);
    }
  }
  

}

detailbien(bien){

  this.currentbien=bien;

 // //console.log("detail residence: ",this.currentbien);

  this.categorieGetDocModel.label=bien.intitule;
  this.categorieGetDocModel.produit=0;
  this.categorieGetDocModel.bloc=0;
  this.categorieGetDocModel.bien=bien.cbmarq;
  

  ////console.log("categorieGetDocModel: ",this.categorieGetDocModel)

  //TODO getList Doc by residence/bloc/appartement
  this.getDocumentByCategorie(this.categorieGetDocModel);

}



  
  getFicheClient(){
    this.proprietaireService.getFicheClient(this.id).subscribe((res: any) => {


      
      if (res.statut === true) {
        this.ficheClient=res;
        ////console.log('fiche : ', res) 
        
        this.listAppartements=this.ficheClient.appartements;

        //console.log("liste app: ",this.listAppartements)
        
      }
    },error=>{},()=>{
      this.getRandomColor();
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    ////console.log(" this.dataSource: ", this.dataSource)
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  
  }

  filterBien(){

    let auxList=this.listAppartements;

    if(this.searchProduit!=""){

      this.listAppartements = auxList.filter(word=>(
       ( word?.label.trim().toLowerCase().includes(this.searchProduit.trim().toLowerCase())

       )
      ))
   
    }


  // this prints the array of filtered

    if(this.searchProduit==""){
      this.getFicheClient();
    }
}




getDocumentByCategorie(categorieGetDocModel){

  ////console.log("categorieGetDocModel: ",categorieGetDocModel)

  this.actualiteService.getDocumentByCategorie(categorieGetDocModel).subscribe((res: any) => {

    this.dataSource.data = res.listDocument;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.length = this.dataSource.data.length;


    ////console.log("getDocumentByCategorie: ",this.dataSource.data)
    

  },
  error => {
    //console.log( 'erreur:' , error);
  }
  );
}


  /*** card fct  */
  OnPageChange(event: PageEvent){
    let startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if(endIndex > this.length){
      endIndex = this.length;
    }
    //let dataSourceAux=this.dataSource.data;
    //this.pagedListDocument = dataSourceAux.slice(startIndex, endIndex);
  }

  onResize(event) { //to adjust to screen size
    this.breakpoint = (event.target.innerWidth <= 800) ? 1 : 4;
  }

}
