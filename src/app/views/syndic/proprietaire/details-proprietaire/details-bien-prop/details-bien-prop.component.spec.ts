import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsBienPropComponent } from './details-bien-prop.component';

describe('DetailsBienPropComponent', () => {
  let component: DetailsBienPropComponent;
  let fixture: ComponentFixture<DetailsBienPropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsBienPropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsBienPropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
