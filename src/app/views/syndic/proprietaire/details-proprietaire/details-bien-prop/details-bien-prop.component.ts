import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FicheClient } from 'src/app/shared/models/fiche-client.model';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';




@Component({
  selector: 'app-details-bien-prop',
  templateUrl: './details-bien-prop.component.html',
  styleUrls: ['./details-bien-prop.component.scss']
})
export class DetailsBienPropComponent implements OnInit {

  id: string;
  ficheClient:FicheClient=new FicheClient();

  constructor( private actRoute: ActivatedRoute, private router: Router,private proprietaireService: ProprietaireService) { }

  ngOnInit(): void {

    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('id');

    });

    if (this.id != null) {
      this.getFicheClient();
     }
  }

  getFicheClient(){
    this.proprietaireService.getFicheClient(this.id).subscribe((res: any) => {


      
      if (res.statut === true) {
        this.ficheClient=res;
        //console.log('fiche : ', res)       

        
      }
    })
  }

}
