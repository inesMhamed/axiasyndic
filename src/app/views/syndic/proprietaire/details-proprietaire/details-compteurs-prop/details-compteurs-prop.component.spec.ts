import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCompteursPropComponent } from './details-compteurs-prop.component';

describe('DetailsCompteursPropComponent', () => {
  let component: DetailsCompteursPropComponent;
  let fixture: ComponentFixture<DetailsCompteursPropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsCompteursPropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCompteursPropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
