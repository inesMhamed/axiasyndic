import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsContratsPropComponent } from './details-contrats-prop.component';

describe('DetailsContratsPropComponent', () => {
  let component: DetailsContratsPropComponent;
  let fixture: ComponentFixture<DetailsContratsPropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsContratsPropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsContratsPropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
