import { Component, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FicheClient } from 'src/app/shared/models/fiche-client.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';


@Component({
  selector: 'app-details-contrats-prop',
  templateUrl: './details-contrats-prop.component.html',
  styleUrls: ['./details-contrats-prop.component.scss']
})
export class DetailsContratsPropComponent implements OnInit {

  
  
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  id_current_user: any;

  id: string;
  pageEvent:any;
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['type'  ,'numero', 'repartition','pourcentage','periodicite','montant'];

  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;

  ficheClient:FicheClient=new FicheClient();

  constructor(private actRoute: ActivatedRoute, private router: Router,private proprietaireService: ProprietaireService,private permissionservice: DroitAccesService) { }

  ngOnInit(): void {

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000113');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000111');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000112');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000110');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000098');

   


    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('id');

    });

    if (this.id != null) {
      this.getFicheClient();
     }
  }


  
  getFicheClient(){
    this.proprietaireService.getFicheClient(this.id).subscribe((res: any) => {


      
      if (res.statut === true) {
        this.ficheClient=res;
        //console.log('fiche : ', res)  
        
        this.dataSource.data = this.ficheClient.contrats;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    
        this.length = this.dataSource.data.length;

        
      }
    })
  }

}
