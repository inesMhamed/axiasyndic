import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditProprietaireComponent } from './add-edit-proprietaire.component';

describe('AddEditProprietaireComponent', () => {
  let component: AddEditProprietaireComponent;
  let fixture: ComponentFixture<AddEditProprietaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditProprietaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditProprietaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
