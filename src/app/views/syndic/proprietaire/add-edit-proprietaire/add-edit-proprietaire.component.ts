import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import libphonenumber from 'google-libphonenumber';
import { NgxSpinnerService } from 'ngx-spinner';
declare var require: any
// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
@Component({
  selector: 'app-add-edit-proprietaire',
  templateUrl: './add-edit-proprietaire.component.html',
  styleUrls: ['./add-edit-proprietaire.component.scss']
})
export class AddEditProprietaireComponent implements OnInit {
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.Tunisia, CountryISO.Tunisia];
  typeUser: any = [{ value: 1, label: 'Moral' }, { value: 2, label: 'Physique' }]
  proprietaireForm: FormGroup;
  submitted: boolean;
  list: any[] = [];
  fileToUpload: File = null;
  modeEdit: boolean = false;
  listAppartement: any[];
  @ViewChild('labelImport')
  labelImport: ElementRef;
  id: string;
  proprietaire: any;
  utilisateur: boolean = false;
  cbmarq: any;
  modeAppartement: boolean;
  listproduits: any;
  url: string | ArrayBuffer;
  haveNoProprietaire: boolean = false; //si ya pas de prop => true

  have_access: boolean = false;
  id_current_user: any;
  haveNoRemiseCle: boolean = false;
  role: any;
  phone1: any;
  phone2: any;
  Resident: any;

  constructor(private formBuilder: FormBuilder, private appartementService: AppartementService, private actRoute: ActivatedRoute,
    private router: Router, private toastr: ToastrService, private proprietaireService: ProprietaireService,
    private spinnerservice: NgxSpinnerService,
    private userService: UtilisateurService, private produitService: ResidenceService,private cdref: ChangeDetectorRef,
    private permissionservice: DroitAccesService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.Resident = this.role.includes('ROLE_RESIDENT');
    this.listProduit();

    this.actRoute.paramMap.subscribe(params => {
      if (params.get('cbmarq')) {
        this.cbmarq = parseInt(params.get('cbmarq'));
      } else {
        this.id = params.get('id');
      }
    });

    this.getBienById();

    //console.log('this.id', this.cbmarq, this.id)
    this.proprietaireForm = this.formBuilder.group({
      nomComplet: new FormControl("", Validators.required),
      login: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email]),
      adresse: new FormControl("",),
      profession: new FormControl("",),
      identifiant: new FormControl("",),
      type: new FormControl(2,),
      internOrProp: new FormControl(0,),
      pwd: new FormControl("", [Validators.required, Validators.minLength(8)]),
      confirmpassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
      telephone1: new FormControl("", Validators.required),
      telephone2: new FormControl("",),
      objects: new FormControl([]),
      dateremisebien: new FormControl("")
    }, { validator: MustMatch('pwd', 'confirmpassword') });
    if (this.router.url === '/syndic/ajouter-utilisateur' || this.router.url.includes('/syndic/modifier-utilisateur/')) {
      this.utilisateur = true;
      this.checkAccessUtilisateur();

    } else {
      this.utilisateur = false;
    }
    if (this.id != null || this.cbmarq != null) {
      this.modeEdit = true;
      ////console.log(this.cbmarq)
      this.listAppartement = []

      if (this.router.url.includes("/syndic/ajouter-proprietaire-appartement/") || this.router.url.includes("/syndic/ajouter-coproprietaire-appartement/")) {
        this.modeAppartement = true;
        this.modeEdit = false;
        this.proprietaireForm.patchValue({
          objects: [this.cbmarq]
        });
        this.checkAccessProprietaire();
      } else {

        this.proprietaireService.getProprietairebyId(this.id).subscribe((pro: any) => {
          this.proprietaire = pro.data
          this.convertPhoneNumber(this.proprietaire.telephone1, this.proprietaire.telephone2)
          console.log('pro.data', this.proprietaire)
          this.proprietaireForm.patchValue({
            nomComplet: this.proprietaire.nomComplet,
            login: this.proprietaire.username,
            email: this.proprietaire.email,
            internOrProp: this.proprietaire.internOrProp,
            telephone1: this.phone1,
            telephone2: this.phone2,
            adresse: this.proprietaire.adresse,
            profession: this.proprietaire.profession,
            identifiant: this.proprietaire.identifiant,
            type: this.proprietaire.type,
          })
          this.proprietaireForm.patchValue({
            objects: this.proprietaire?.objects?.map(elem => (elem.cbmarq)),
          });
        });
      }
    }
    this.getlistAppartement();

  }

  convertPhoneNumber(telephone1, telephone2) {
    const instance = libphonenumber.PhoneNumberUtil.getInstance();
    const PNF = require('google-libphonenumber').PhoneNumberFormat;
    if (telephone1) {
      let PhoneNumberphone = instance.parse(telephone1);
      const internationalNumber = instance.format(PhoneNumberphone, PNF.INTERNATIONAL);
      let region = instance.getRegionCodeForNumber(PhoneNumberphone);
      let country = PhoneNumberphone.getCountryCode();
      let phone = PhoneNumberphone.getNationalNumber();
      this.phone1 = {
        countryCode: region,
        dialCode: "+" + country.toString(),
        e164Number: telephone1,
        internationalNumber: internationalNumber.toString(),// "+216 21 000 000",
        nationalNumber: "",
        number: phone.toString(),
      }
    } else {
      this.phone1 = {
        countryCode: "TN",
        dialCode: "+216",
        e164Number: "",
        internationalNumber: "",
        nationalNumber: "",
        number: "",
      }
    }

    if (telephone2) {
      let PhoneNumberphone = instance.parse(telephone2);
      const internationalNumber = instance.format(PhoneNumberphone, PNF.INTERNATIONAL);
      let region = instance.getRegionCodeForNumber(PhoneNumberphone);
      let country = PhoneNumberphone.getCountryCode();
      let phone = PhoneNumberphone.getNationalNumber();
      this.phone2 = {
        countryCode: region,
        dialCode: "+" + country.toString(),
        e164Number: telephone2,
        internationalNumber: internationalNumber.toString(),// "+216 21 000 000",
        nationalNumber: "",
        number: phone.toString(),
      }
    } else {
      this.phone2 = {
        countryCode: "TN",
        dialCode: "+216",
        e164Number: "",
        internationalNumber: "",
        nationalNumber: "",
        number: "",
      }
    }
  }
  checkAccessUtilisateur() {

    if(this.router.url == '/syndic/ajouter-utilisateur'){
      this.checkAccess("FN21000043");
    }
    if(this.router.url.includes('/syndic/modifier-utilisateur/')){
      this.checkAccess("FN21000044");
    }

  }

  checkAccessProprietaire(){

    if(this.router.url === '/syndic/ajouter-proprietaire-appartement/'){
      this.checkAccess("FN21000039");
    }
    if(this.router.url.includes('/syndic/ajouter-coproprietaire-appartement/')){
      this.checkAccess("FN21000104");
    }
    // if(this.router.url.includes('/syndic/ajouter-coproprietaire-appartement/')){
    //   this.checkAccess("FN21000040");
    // }

  }

  checkAccess(code){
      //if access false ==> go out to 403
      this.permissionservice.getAccessUser( this.id_current_user, code).subscribe((res:any)=>{

        this.have_access =res.data;
        
      },error=>{},()=>{
    
        if(!this.have_access){
          this.router.navigate(['403']);
        }
        
      });
  }


  ngAfterContentChecked() {

    this.cdref.detectChanges();

  }

  myFunction() {
    let x = <HTMLInputElement>document.getElementById("pwd");
    let y = <HTMLInputElement>document.getElementById("confirmpassword");
    if (x.type === "password") {
      x.type = "text";
    }
    else {
      x.type = "password";
    }
    if (y.type === "password") {
      y.type = "text";
    } else {
      y.type = "password";
    }

  }
  listProduit() {
    this.spinnerservice.show()
    this.produitService.getResidences().subscribe((ress: any) => {
      this.listproduits = ress.data
      this.spinnerservice.hide()
    })
  }

  //wz
  getBienById(){
    ////console.log("innn  getBienById: ",this.cbmarq);

    this.spinnerservice.show()
    this.appartementService.getAppartement(this.cbmarq).subscribe((res: any) => {

      if(res.statut){
        if(res.data.proprietaire.length==0){
          this.haveNoProprietaire=true;
          this.proprietaireForm.get('dateremisebien').setValidators(Validators.required)

        }else{
          this.haveNoProprietaire=false;
          this.proprietaireForm.get('dateremisebien').clearValidators();

        }

        this.proprietaireForm.updateValueAndValidity();      

      }
      this.spinnerservice.hide()
    })
  }

 
  //cas proprietaire 
  onAddproduit(event){
    this.spinnerservice.show()
    this.haveNoProprietaire=false;


    for(let appart of this.proprietaireForm.value.objects){
     

        this.appartementService.getAppartement(appart).subscribe((res: any) => {
  
    
          if(res.statut){

            if(res.data.proprietaire.length==0){
              this.haveNoProprietaire= ( this.haveNoProprietaire || true);
              this.proprietaireForm.get('dateremisebien').setValidators(Validators.required)
              this.haveNoRemiseCle=true;
            }
            if(res.data.proprietaire.length>0){
              this.haveNoProprietaire= ( this.haveNoProprietaire || false);
              this.proprietaireForm.get('dateremisebien').clearValidators();
              this.haveNoRemiseCle=false;              
              
            }
    
            this.proprietaireForm.updateValueAndValidity();    
                
          }
          this.spinnerservice.hide()
        })
      
    }
 

  }

  onRemoveProduit(event){
    //console.log("listAppartement: ",this.proprietaireForm.value.objects)
    this.spinnerservice.show()
    this.haveNoProprietaire=false;

    for(let appart of this.proprietaireForm.value.objects){

        this.appartementService.getAppartement(appart).subscribe((res: any) => {
  
    
          if(res.statut){

            if(res.data.proprietaire.length==0){
              this.haveNoProprietaire= ( this.haveNoProprietaire || true);
              this.proprietaireForm.get('dateremisebien').setValidators(Validators.required)
              this.haveNoRemiseCle=true;

              
            }
            if(res.data.proprietaire.length>0){
              this.haveNoProprietaire= ( this.haveNoProprietaire || false);
              this.proprietaireForm.get('dateremisebien').clearValidators();
              this.haveNoRemiseCle=false;

            }
    
            this.proprietaireForm.updateValueAndValidity();    
                
          }
          this.spinnerservice.show()
        })
      
    }

  }

  onSubmit() {
    this.spinnerservice.show()
    if (this.modeEdit) {
      this.proprietaireForm.get('confirmpassword').disable();
      this.proprietaireForm.get('pwd').disable();
    }
    if (this.proprietaireForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true
      return;
    }

    this.proprietaireForm.value.telephone1 = this.proprietaireForm.value.telephone1.e164Number;
    if (this.proprietaireForm.value.telephone2) {
      this.proprietaireForm.value.telephone2 = this.proprietaireForm.value.telephone2.e164Number;
    }
    delete this.proprietaireForm.value.confirmpassword;
    //mode add
    this.list = [];
    this.proprietaireForm?.value?.objects?.forEach(element => {
      this.list.push({ object: element });
    });
    // console.log(this.proprietaireForm.value , this.list);
    this.proprietaireForm.value.objects = this.list;
    if (!this.modeEdit) {
      if (this.router.url === '/syndic/ajouter-Coproprietaire' || this.router.url.includes("/syndic/ajouter-coproprietaire-appartement/")) {
        //console.log('coproprietaire ajouter ', this.proprietaireForm.value)
        this.proprietaireService.postCoProprietaire(this.proprietaireForm.value).subscribe((ress: any) => {
          if (ress.statut === true) {
            this.toastr.success("Co-proprietaire est ajouté avec succès !", 'Succès!', { progressBar: true });
            this.router.navigate(['/syndic/details-coproprietaire/', ress.data.id]);
            this.proprietaireForm.reset()
          }
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        })
      }
      else if (this.router.url === '/syndic/ajouter-utilisateur') {
        //console.log('utilisateur ajouter ', this.proprietaireForm.value)
        this.userService.postUtilisateur(this.proprietaireForm.value).subscribe((ress: any) => {
          if (ress.statut === true) {
            this.toastr.success("Utilisateur est ajouté avec succès ", 'Succès!', { progressBar: true });
            //this.router.navigate(['/syndic/details-utilisateur/', ress.data.id]);
            this.router.navigate(['/syndic/utilisateurs']);
            this.proprietaireForm.reset()
          }
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        })
      }
      else if (this.router.url === '/syndic/ajouter-proprietaire' || this.router.url.includes("/syndic/ajouter-proprietaire-appartement/")) {
        //console.log('proprietaire ajouter ', this.proprietaireForm.value)
        this.proprietaireService.postProprietaire(this.proprietaireForm.value).subscribe((ress: any) => {
          if (ress.statut === true) {
            this.toastr.success("Proprietaire est ajouté avec succès ", 'Succès!', { progressBar: true });
            this.router.navigate(['/syndic/details-proprietaire/', ress.data.id])
            this.proprietaireForm.reset()
          }
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        })
      }

    }
    else {
      //mode edit
      if (this.router.url.includes("/syndic/modifier-coproprietaire/")) {
        this.proprietaireForm.value.internOrProp = 2
        //console.log('coproprietaire edit', this.proprietaireForm.value)
        this.proprietaireService.putCoProprietaire(this.proprietaireForm.value, this.id).subscribe((ress: any) => {
          if (ress.statut === true) {
            this.toastr.success("Co-Proprietaire est modifié avec succès ", 'Succès!', { progressBar: true });
            this.router.navigate(['/syndic/details-coproprietaire/', this.id])
            this.proprietaireForm.reset()
          }
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            //console.log('erre', error)
          })

      } else if (this.router.url.includes('/syndic/modifier-utilisateur/')) {
        this.proprietaireForm.value.internOrProp = 1
        //console.log('user edit', this.proprietaireForm.value)
        this.userService.putUtilisateur(this.proprietaireForm.value, this.id).subscribe((ress: any) => {

          if (ress.statut === true) {
            this.toastr.success("Utilisateur est modifié avec succès ", 'Succès!', { progressBar: true });
            //this.router.navigate(['/syndic/details-utilisateur/', this.id])
            this.router.navigate(['/syndic/utilisateurs']);
            this.proprietaireForm.reset()

          }
          else {

            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            //console.log('erre', error)
          })

      } else {
        this.proprietaireForm.value.internOrProp = 2
        //console.log('prop edit', this.proprietaireForm.value)
        this.proprietaireService.putProprietaire(this.proprietaireForm.value, this.id).subscribe((ress: any) => {
          if (ress.statut === true) {
            //console.log('ress', ress)
            this.toastr.success("Proprietaire est modifié avec succès ", 'Succès!', { progressBar: true });
            this.router.navigate(['/syndic/details-proprietaire/', this.id])
            this.proprietaireForm.reset()
          }
          else {

            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        },
          error => {
            this.spinnerservice.hide()
            //console.log('erre', error)
          })
      }

    }

  }
  getlistAppartement() {
    this.spinnerservice.show()
    this.appartementService.getAppartements().subscribe((app: any) => {
      this.listAppartement = app?.data?.map(el => ({ intitule: el?.residence?.intitule + ' | ' + el?.bloc?.intitule + ' | ' + el?.intitule, cbmarq: el?.cbmarq }))
      this.spinnerservice.hide()
    })
  }
}
