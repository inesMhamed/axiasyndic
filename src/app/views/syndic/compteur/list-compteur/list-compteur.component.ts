import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSort} from '@angular/material/sort';
import { CompteurService } from '../../../../shared/services/compteur.service';
import {MatTableDataSource} from '@angular/material/table';
import {Compteur} from '../../../../shared/models/compteur.model';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatInput} from '@angular/material/input';
import {first} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {MatDialog} from '@angular/material/dialog';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditCompteurComponent} from '../edit-compteur/edit-compteur.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import {CommuneService} from '../../../../shared/services/commune.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-compteur',
  templateUrl: './list-compteur.component.html',
  styleUrls: ['./list-compteur.component.scss'],
})
export class ListCompteurComponent implements OnInit {
  displayedColumns: string[] = ['numero', 'type' , 'cbcreation' , 'residence', 'bloc' , 'action'];
  dataSource = new MatTableDataSource<Compteur>();
  pageEvent: PageEvent;
  compteurs;
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  residences: any[];
  types: any[];
  blocs: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  @ViewChild('closeModal') private closeModal: ElementRef;
  @ViewChild('closedModal') private closedModal: ElementRef;
  data: any;
  checked: any;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(
    private preferenceService: PreferencesService,
    private compteurService: CompteurService,
    private blocService: BlocService,
    private communeService: CommuneService,
    private dl: ResidenceService,
    private fb: FormBuilder,
    private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private permissionservice: DroitAccesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.submitted = false;
    this.checked = 1;

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000029');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000030');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000031');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000033');
    this.buildFormBasic();
    this.checkAccess();

   

    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;

  }


  checkAccess() {
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000033').subscribe((res: any) => {

      this.have_access = res.data;
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.spinnerservice.hide()
      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {
        this.listeresidence();
        this.listetypes();
        this.listeCompteurs();
      }

    });
  }

  buildFormBasic() {
    this.formBasic = this.fb.group({
      numero: new FormControl(null, Validators.required),
      residence: new FormControl( null, Validators.required),
      bloc: new FormControl( null, ),
      type: new FormControl(null, Validators.required ),
      typePayement: new FormControl(1, Validators.required ),
    });
  }
  listeCompteurs() {
    this.spinnerservice.show()
    this.compteurService.getCompteurs().subscribe((res: any) => {
      if(res.statut){
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
      // .map(clt => ({ cbmarq: clt.cbmarq, numero: clt.numero, type: clt.type.label , cbcreation: clt.cbcreation , residence: clt.residence?.intitule , appartement: clt.appartement?.intitule }));
         
        },
        error => {
          this.spinnerservice.hide()
          //console.log( 'erreur:' , error);
        }
    );
  }
  listeresidence() {
    this.spinnerservice.show()
    this.dl.getResidences().subscribe((res: any) => {
      if(res.statut){
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
        },
        error => {
          this.spinnerservice.hide()
          //console.log( 'erreur: ', error);
        }
    );
  }
  listetypes() {
    this.communeService.gettype('CMT').subscribe((res: any) => {
          // this.types = res.data;
          if(res.statut){
            this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));

          }
        },
        error => {
          //console.log( 'erreur: ', error);
        }
    );
  }
  OnchangeResidence(event) {
    if (event && event.value) {
      this.blocService.getBlocs(event.value).subscribe((res: any) => {
            if (res.data) {
              this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
            }
          },
          error => {
            //console.log( 'erreur: ', error);
          }
      );
    }
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  onclose() {
    this.modalService.dismissAll();
  }
  submit() {
    this.loading = true;
    if (this.formBasic.invalid) {
      this.submitted = true;
      return;
    }

    //console.log('end', this.formBasic.value);
    this.compteurService.postCompteur(this.formBasic.value).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.toastr.success('Compteur ajouté avec succès !', 'Success!', {progressBar: true});
            this.submitted = false;
            this.listeCompteurs();
            this.modalService.dismissAll();
            this.formBasic.reset();
          } else {
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', {progressBar: true});
          }
        },
        (error) => {
          this.toastr.error('Erreur lors d\'ajout d\'un Compteur . Veuillez réessayer !', 'Erreur!', {progressBar: true});
        });

  }
  cancelCmp(cbmarq: any) {

    this.compteurService.deleteCompteur(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Compteur supprimer avec succès !');
          this.listeCompteurs();
        }
        else {
          this.toastr.error(res.message);
        }
      },
      error => {
        console.log(error)
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un compteur',
      text: 'Voulez-vous vraiment supprimer ce compteur ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.cancelCmp(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  editCompteur(doc) {
    this.compteurService.getCompteur(doc).subscribe((res: any) => {
          this.dialog.open(EditCompteurComponent, { data: res.data }).afterClosed().subscribe(() => {
            this.listeCompteurs();
          });
        },
        error => {
          //console.log( 'erreur:' , error);
        }
    );

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    //console.log(this.dataSource, this.dataSource.filteredData);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}

