import { Component, Inject, OnInit } from '@angular/core';
import { CompteurService } from '../../../../shared/services/compteur.service';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CommuneService } from '../../../../shared/services/commune.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-compteur',
  templateUrl: './edit-compteur.component.html',
  styleUrls: ['./edit-compteur.component.scss']
})
export class EditCompteurComponent implements OnInit {
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  residences: any[];
  types: any[];
  blocs: any[];

  data: any;
  checked: number;
  constructor(
    private compteurService: CompteurService,
    private communeService: CommuneService,
    private dl: ResidenceService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinnerservice: NgxSpinnerService,
    private blocService: BlocService,
    private actRoute: ActivatedRoute,
    // private modalService: NgbModal,
    private dialogRef: MatDialogRef<EditCompteurComponent>, @Inject(MAT_DIALOG_DATA) public cmp: any
  ) { }

  ngOnInit() {
    this.spinnerservice.show()
    this.submitted = false;
    this.buildFormBasic();
    this.listeresidence();
    this.listetypes();
    // this.listeCompteurs();
    this.blocService.getBlocs(this.cmp.residence.cbmarq).subscribe((re: any) => {
      this.blocs = re.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
    })

  }
  onClose() {
    this.dialogRef.close();
  }
  buildFormBasic() {
    this.formBasic = this.fb.group({
      numero: new FormControl(this.cmp.numero, Validators.required),
      residence: new FormControl(this.cmp.residence.cbmarq, Validators.required),
      bloc: new FormControl(null || this.cmp.bloc.cbmarq,),
      type: new FormControl(this.cmp.type.cbmarq, Validators.required),
      typePayement: new FormControl(this.cmp.typePayement, Validators.required),
    });
  }

  listeresidence() {
    this.spinnerservice.show()
    this.dl.getResidences().subscribe((res: any) => {
      this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  listetypes() {
    this.communeService.gettype('CMT').subscribe((res: any) => {
      this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
    },
      error => {
        //console.log('erreur: ', error);
      }
    );
  }

  OnchangeResidence(event) {
    this.spinnerservice.show()
    if (event && event.value) {
      this.blocService.getBlocs(event.value).subscribe((res: any) => {
        //console.log('res: ', res);
        if (res.data) {
          this.spinnerservice.hide()
          this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        }
        this.spinnerservice.hide()
      },
        error => {
          this.spinnerservice.hide()
          //console.log('erreur: ', error);
        }
      );
    }
  }
  // open(content) {
  //   this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' , size: 'lg'})
  //       .result.then((result) => {
  //     //console.log(result);
  //   }, (reason) => {
  //     //console.log('Err!', reason);
  //   });
  // }
  submit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }

    this.compteurService.putCompteur(this.formBasic.value, this.cmp.cbmarq).pipe(first()).subscribe((res: any) => {
      if (res.statut === true) {
        this.spinnerservice.hide()
        this.toastr.success('Compteur a été modifié avec succès !', 'Success!', { progressBar: true });
        this.submitted = false;
        this.onClose();
        // this.formBasic.reset();
      } else {
        this.spinnerservice.hide()
        this.submitted = true;
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    },
      (error) => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de la modification d\'un Compteur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
      });

  }


}
