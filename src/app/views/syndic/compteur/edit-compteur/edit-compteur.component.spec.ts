import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCompteurComponent } from './edit-compteur.component';

describe('EditCompteurComponent', () => {
  let component: EditCompteurComponent;
  let fixture: ComponentFixture<EditCompteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCompteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCompteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
