import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { CommuneService } from 'src/app/shared/services/commune.service';
import { CompteurService } from 'src/app/shared/services/compteur.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { first } from 'rxjs/operators';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-compteur',
  templateUrl: './add-compteur.component.html',
  styleUrls: ['./add-compteur.component.scss']
})
export class AddCompteurComponent implements OnInit {
  formBasic: FormGroup;
  submitted: boolean;
  loading: boolean;
  listAppartements: any = [];
  modeEdit: boolean;

  checked: any
  affecters: any = [{ value: 1, label: 'Répartition par bien' }, { value: 0, label: 'Charger syndic' }]
  residences: any[];
  types: any[];
  blocs: any[];
  cbmarq: any;
  somme: number;
  test: any;
  testsomme: boolean;
  pourcentage: string;
  hideThisDiv: boolean = false;
  typeProduit: any;
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  repartitionOnEdit:number;
  usedCompteur:boolean=false;

  have_access:boolean =false;


  constructor(private compteurService: CompteurService,
    private blocService: BlocService,
    private communeService: CommuneService,
    private residenceService: ResidenceService,
    private fb: FormBuilder,
    private spinnerservice: NgxSpinnerService,
    private router: Router,
    private actRouter: ActivatedRoute,
    private appartementService: AppartementService,
    private toastr: ToastrService,
    private permissionservice: DroitAccesService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    // document.getElementById('details').style.display='none'
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000029');
    this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000030');

    this.actRouter.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });
    this.formBasic = this.fb.group({
      numero: new FormControl(null, Validators.required),
      residence: new FormControl(null, Validators.required),
      repartition: new FormControl(null, Validators.required),
      bloc: new FormControl(null,),
      type: new FormControl(null, Validators.required),
      lignes: this.fb.array([]),
    });

    this.checkADDEDIT();
    
    this.listeresidence();
    this.listetypes()
    if (this.cbmarq) {

      this.modeEdit = true;

      this.compteurService.getCompteur(this.cbmarq).subscribe((compteur: any) => {
        this.repartitionOnEdit=compteur.data.repartition;
        this.usedCompteur=compteur.data.used;

        this.formBasic.patchValue({
          numero: compteur.data.numero,
          residence: compteur?.data?.residence?.cbmarq,
          repartition: compteur?.data?.repartition,
          bloc: compteur?.data?.bloc == null ? 0 : compteur?.data?.bloc?.cbmarq,
          type: compteur?.data?.type?.cbmarq,
          lignes: compteur?.data?.lignes,
        });

        this.typeProduit=compteur?.data?.residence.type?.label;

        if (compteur?.data?.residence?.type?.code === 'E0063') {
          this.formBasic.patchValue({
            bloc: null
          });
        }
        this.blocService.getBlocs(this.formBasic.value.residence).subscribe((bloc: any) => {
          if (bloc.statut) {
            this.spinnerservice.hide()
            this.blocs = bloc?.data?.map(el => ({value: el.cbmarq, label: el.intitule}));
          }
          this.spinnerservice.hide()
          this.blocs?.unshift({ value: 0, label: 'Tous' });
        })
        Object.keys(compteur.data).forEach(name => {
          if (name === 'lignes') {
            this.test = compteur.data[name];
            Object.keys(this.test).forEach(n => {
              this.addLigne();
              var lig = this.test[n];
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.appartement?.cbmarq);
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.appartement?.intitule);
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue(lig?.pourcentage);
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.appartement?.nature);
            });
          }
        });

        /*--Update lignes ---------------------------------------------*/
        let length = this.formBasic.value.lignes.length ;
        if (compteur?.data?.bloc === null) {
          this.appartementService.getAppartementResidenceprop(this.formBasic.value.residence).subscribe((type: any) => {
            this.spinnerservice.hide()
            if (!type.data) {
              this.toastr.error('Ce produit ne contient pas des biens occupés !', 'Erreur!', { progressBar: true });
            } else {
              this.listAppartements = type.data
              Object.keys(this.listAppartements).forEach(n => {
                var lig: any = this.listAppartements[n];
                let result = this.formBasic.value.lignes.findIndex(itm => itm.appartement === lig?.cbmarq);
                if (result === -1) {
                  this.addLigne();
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('selected').patchValue(false);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('intitule').patchValue(lig?.intitule);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('pourcentage').patchValue(0);
                  length++ ;
                }
              });
            }
          });
        } else {
          this.spinnerservice.hide()
          this.listAppartements = []
          this.blocService.getAppartementByBlocHaveProp(compteur?.data?.bloc?.cbmarq).subscribe((appart: any) => {
            if (!appart.data) {
              this.toastr.error('Ce bloc ne contient pas des biens !', 'Erreur!', { progressBar: true });
            } else {
              this.listAppartements = appart.data
              Object.keys(this.listAppartements).forEach(n => {
                var lig: any = this.listAppartements[n];
                let result = this.formBasic.value.lignes.findIndex(itm => itm.appartement === lig?.cbmarq);
                console.log('result', compteur?.data)
                if (result === -1) {
                  this.addLigne();
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('selected').patchValue(false);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('intitule').patchValue(lig?.intitule);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('pourcentage').patchValue(0);
                  length++ ;
                }
              });
            }

          });
        }
      });

    }
  }

  checkADDEDIT(){
    if(!this.cbmarq){
      this. checkAccess("FN21000029");
    }else{
      this.checkAccess("FN21000030");
    }

  }
  checkAccess(code){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, code).subscribe((res:any)=>{

     this.have_access =res.data;
    
   },error=>{},()=>{

     if(!this.have_access){
       this.router.navigate(['403']);
     }
    
   });
 }

  testSomme(): boolean {
    let testsomme = false
    if (this.somme > 100) {
      this.toastr.error('la somme de pourcentage de payement est supérieure à 100% !', 'Erreur!', { progressBar: true });
      testsomme = false
    } else if (this.somme < 100) {
      this.toastr.error('la somme de pourcentage de payement est inférieure à 100% !', 'Erreur!', { progressBar: true });
      testsomme = false
    } else {
      testsomme = true
    }
    return testsomme
  }
  onSubmit() {
    this.spinnerservice.show()
    this.somme = 0
    this.loading = true;
    // this.formBasic.patchValue({
    //   repartition: this.repartitionOnEdit,
    //
    // });

    //console.log('on submit: ', this.formBasic.value);

    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    if (this.formBasic.value.lignes != null) {
      this.formBasic.getRawValue().lignes.forEach(i => {
        if (i.selected === true) {
          this.somme = this.somme + parseFloat(i.pourcentage);
        }
        let removeIndex = this.formBasic.value.lignes.findIndex(itm => itm.selected === false);

        if (removeIndex !== -1) {
          this.formBasic.value.lignes.splice(removeIndex, 1);
        }
      });

      this.formBasic.value.lignes.forEach(i => {
        delete i.selected
        delete i.intitule
        delete i.nature
      });
      this.spinnerservice.hide()
    }
    if (!this.modeEdit) {
      this.spinnerservice.hide()
      ////console.log('validForm', this.formBasic.value);
      if (this.can_add?.permission) {
        if (this.formBasic.value.repartition !=0) {
          if (this.somme > 100) {
            this.toastr.error('la somme de pourcentage de payement est supérieure à 100% !', 'Erreur!', { progressBar: true });
          } else if (this.somme < 100) {
            this.toastr.error('la somme de pourcentage de payement est inférieure à 100% !', 'Erreur!', { progressBar: true });
          } else {
            this.compteurService.postCompteur(this.formBasic.value).pipe(first()).subscribe((res: any) => {
              if (res.statut === true) {
                this.toastr.success('Compteur a été ajouté avec succès !', 'Success!', { progressBar: true });
                this.submitted = false;
                this.formBasic.reset();
                this.hideThisDiv = false;
                this.router.navigate(['/syndic/compteurs/']);
              } else {
                this.submitted = true;
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
              }
            },
              (error) => {
                this.toastr.error('Erreur lors de l\'ajout d\'un compteur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
              });
          }
        }
        else {
          this.spinnerservice.hide()
          //console.log('validForm', this.formBasic.value);
          this.compteurService.postCompteur(this.formBasic.value).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
              this.toastr.success('Compteur a été ajouté avec succès !', 'Success!', { progressBar: true });
              this.submitted = false;
              this.router.navigate(['/syndic/compteurs/']);
              this.formBasic.reset();
              this.hideThisDiv = false;
            } else {
              this.submitted = true;
              this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
          },
            (error) => {
              this.toastr.error('Erreur lors de l\'ajout d\'un compteur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
            });
        }
      } else {
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }
    }
    else {
      if (this.can_edit?.permission) {
        //console.log('editForm', this.formBasic.value);
        if (this.formBasic.value.repartition !=0) {
       // if (this.formBasic.value.lignes != null) {
          if (this.somme > 100) {
            this.toastr.error('la somme de pourcentage de payement est supérieure à 100% !', 'Erreur!', { progressBar: true });
          } else if (this.somme < 100) {
            this.toastr.error('la somme de pourcentage de payement est inférieure à 100% !', 'Erreur!', { progressBar: true });
          } else {
            this.compteurService.putCompteur(this.formBasic.value, this.cbmarq).pipe(first()).subscribe((res: any) => {
              if (res.statut === true) {
                this.toastr.success('Compteur est modifié avec succès !', 'Success!', { progressBar: true });
                this.submitted = false;
                this.router.navigate(['/syndic/compteurs'])
              } else {
                this.submitted = true;
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
              }
            },
              (error) => {
                this.toastr.error('Erreur lors de la modifiation d\'un compteur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
              });

          }
        }
        else {
          this.spinnerservice.show()
          this.compteurService.putCompteur(this.formBasic.value, this.cbmarq).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
              this.spinnerservice.hide()
              this.toastr.success('Compteur a été modifié avec succès !', 'Success!', { progressBar: true });
              this.submitted = false;
              this.router.navigate(['/syndic/compteurs'])
            } else {
              this.spinnerservice.hide()
              this.submitted = true;
              this.toastr.error(res.message, 'Erreur!', { progressBar: true });
            }
          },
            (error) => {
              this.spinnerservice.hide()
              this.toastr.error('Erreur lors de la modifiation d\'un compteur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
            });
        }
      } else {
        this.spinnerservice.hide()
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }
    }



  }
  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      //console.log(res.data)
      if(res.statut){
        this.residences = res?.data.map(clt => ({ value: clt?.cbmarq, label: clt?.intitule, type: clt?.type?.label }));
        this.spinnerservice.hide()
      }
      //   res.data=res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }))
      //   this.residences = res?.data.filter(clt => (clt?.type?.code=="E0062"));
      //   this.terrains = res?.data.filter(clt =>  (clt?.type?.code=="E0063"));
      //   this.residences.unshift({ value: 0, label: 'Résidence' });
      this.spinnerservice.hide()
    },
      error => {

        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  listetypes() {
    this.communeService.gettype('CMT').subscribe((res: any) => {
      // this.types = res.data;
      if(res.statut){
        this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));

      }
    },
      error => {
        //console.log('erreur: ', error);
      }
    );
  }
  OnchangeResidence(event) {

    ////console.log("OnchangeResidence event: ",event)
    this.typeProduit = event.type;

    //

    if (event && event.type == 'Résidence') {
      ////console.log("inn residence type: ")
      this.blocService.getBlocs(event.value).subscribe((res: any) => {
        if (res.data) {
          this.blocs = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
          this.blocs.unshift({ value: 0, label: 'Tous' });

          this.formBasic.patchValue({
            bloc: 0,
          });
        }
      },
        error => {
          //console.log('erreur: ', error);
        }
      );
    }
    else {
      ////console.log("inn not residence type")
      this.formBasic.value.bloc = null
    }

    ////console.log("OnchangeResidence  this.formBasic: :", this.formBasic.value)
  }
  onchangeAffectation(event) {
    //console.log(event)
    if (event.value == 0) {
      this.hideThisDiv = false
      this.formBasic.controls['bloc'].clearValidators;
      this.formBasic.controls['bloc'].updateValueAndValidity();
      this.formBasic.controls['lignes'].clearValidators;
      this.formBasic.controls['lignes'].setValidators([]);
      this.formBasic.controls['lignes'].updateValueAndValidity();
      this.formBasic.value.lignes = null
      this.formBasic.value.bloc = null
    } else {
      this.hideThisDiv = true
      this.onchange({ value: 0 })
      if (this.typeProduit == "Résidence") {
        this.formBasic.controls['bloc'].setValidators([Validators.required]);
        this.formBasic.controls['bloc'].updateValueAndValidity();
      }
      else {
        this.formBasic.value.bloc = null
        this.formBasic.controls['bloc'].clearValidators;
        this.formBasic.controls['bloc'].updateValueAndValidity();
      }
      this.formBasic.controls['lignes'].setValidators([Validators.required]);
      this.formBasic.controls['lignes'].updateValueAndValidity();
    }
  }
  onchange(event) {
    this.spinnerservice.show()
    this.lignes().clear()
    this.listAppartements = []
    if (event.value === 0) {
      this.appartementService.getAppartementResidenceprop(this.formBasic.value.residence).subscribe((type: any) => {
        ////console.log('appartement by resi have prop', type.data)
        if (!type.data) {
          this.spinnerservice.hide()
          this.toastr.error('Ce produit ne contient pas des biens !', 'Erreur!', { progressBar: true });
        } else {
          this.spinnerservice.hide()
          this.listAppartements = type.data
          Object.keys(this.listAppartements).forEach(n => {
            this.addLigne();
            var lig: any = this.listAppartements[n];
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.intitule);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));

          });
        }
      });
    } else {
      this.listAppartements = []
      this.lignes().clear()
      this.blocService.getAppartementByBlocHaveProp(event.value).subscribe((appart: any) => {
        if (!appart.data) {
          this.spinnerservice.hide()
          this.toastr.error('Ce bloc ne contient pas des biens !', 'Erreur!', { progressBar: true });
        } else {
          this.spinnerservice.hide()
          this.listAppartements = appart.data
          Object.keys(this.listAppartements).forEach(n => {
            this.addLigne();
            var lig: any = this.listAppartements[n];
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.intitule);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));

          });
        }

      });
      this.spinnerservice.hide()
    }
  }
  newligne(): FormGroup {
    return this.fb.group({
      selected: new FormControl(true,),
      nature: new FormControl(null, Validators.required),
      appartement: new FormControl(null, Validators.required),
      intitule: new FormControl('', Validators.required),
      pourcentage: new FormControl('', Validators.required),
    });
  }
  lignes(): FormArray {
    return this.formBasic.get('lignes') as FormArray;
  }
  addLigne() {
    this.lignes().push(this.newligne());
  }
  removeLigne(ligIndex: number) {
    this.lignes().removeAt(ligIndex);
  }
  checkCheckBoxvalue(event, index) {
    let nbSelect: any[] = this.formBasic.value.lignes.filter(el => (el.selected == true))
    this.formBasic.getRawValue().lignes.forEach((i, index2) => {
      this.pourcentage = (100 / nbSelect.length).toFixed(1);
      if (i.selected == true) {
        i.pourcentage = this.pourcentage;
        ((this.formBasic.get('lignes') as FormArray).at((index2)) as FormGroup).get('pourcentage').patchValue(this.pourcentage);
        ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').enable();
        ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('nature').enable();
        ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').enable();
        ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').enable();
        ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).enable();
      }
    });
    if (event.target.checked) {
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue(this.pourcentage);
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('nature').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).enable();
    } else {
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue('0');
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').disable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('nature').disable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').disable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').disable();
    }
  }

  gotoListe(){
    this.router.navigate(['/syndic/compteurs/']);
  }
}
