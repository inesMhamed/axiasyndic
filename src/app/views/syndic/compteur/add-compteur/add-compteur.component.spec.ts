import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCompteurComponent } from './add-compteur.component';

describe('AddCompteurComponent', () => {
  let component: AddCompteurComponent;
  let fixture: ComponentFixture<AddCompteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCompteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCompteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
