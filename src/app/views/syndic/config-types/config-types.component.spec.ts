import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTypesComponent } from './config-types.component';

describe('ConfigTypesComponent', () => {
  let component: ConfigTypesComponent;
  let fixture: ComponentFixture<ConfigTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
