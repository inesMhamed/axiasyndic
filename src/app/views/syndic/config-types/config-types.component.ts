import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';

@Component({
  selector: 'app-config-types',
  templateUrl: './config-types.component.html',
  styleUrls: ['./config-types.component.scss']
})
export class ConfigTypesComponent implements OnInit {

 genericTypes:any[]=[
   {name:"Type Compteur",code:"CMT"},
   {name:"Type Bien",code:"TAP"},
   {name:"Nature Bien",code:"NAP"},
   {name:"Type Service",code:"CNT"},
   {name:"Type Réunion",code:"REU"},
   {name:"Type Depense",code:"TDP"},
   {name:"Catégorie Reclamation",code:"CRC"},

  ];
 

  
  id_current_user:any;
  have_access:boolean =false;

   constructor(private router: Router, private spinnerservice: NgxSpinnerService,private permissionservice: DroitAccesService) {

    this.id_current_user = localStorage.getItem('id');

   }

  ngOnInit(): void {
    this.spinnerservice.show()
     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000127').subscribe((res:any)=>{

      this.have_access =res.data;
      this.spinnerservice.hide()
    },error=>{},()=>{
      this.spinnerservice.hide()
      if(!this.have_access){
        this.router.navigate(['403']);
      }
     
    });

  
  }

  

}
