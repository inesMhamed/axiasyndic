import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenTypesComponent } from './gen-types.component';

describe('GenTypesComponent', () => {
  let component: GenTypesComponent;
  let fixture: ComponentFixture<GenTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
