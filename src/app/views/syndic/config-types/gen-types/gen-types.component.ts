import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ParametreService } from 'src/app/shared/services/parametre.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-gen-types',
  templateUrl: './gen-types.component.html',
  styleUrls: ['./gen-types.component.scss']
})
export class GenTypesComponent implements OnInit {

  @Input() name;
  @Input() code;


 
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['intitule' ,'style','description','action'];  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5; 
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;

  loading: boolean;
  submitted: boolean;
  paramForm: FormGroup;
  cbmarq: number=0;
  styles:any[]=[{label:'primary',value:'primary'},{label:'secondary',value:'secondary'},{label:'success',value:'success'},
  {label:'danger',value:'danger'},{label:'warning',value:'warning'},{label:'info',value:'info'},{label:'light',value:'light'},{label:'dark',value:'dark'}]
 
  codeCategorie="";
  nameComponent="";

  constructor(private parametreService: ParametreService, private spinnerservice: NgxSpinnerService, private preferenceService: PreferencesService, private proprietaireService: ProprietaireService,
    private permissionservice: DroitAccesService, private toastr: ToastrService, private cdRef: ChangeDetectorRef, private fb: FormBuilder) {

     
     }

  ngOnInit(): void {

    ////console.log("name component: ",this.name);
    ////console.log("code component: ",this.code);
    this.spinnerservice.show()
    this.codeCategorie = this.code;
    this.nameComponent = this.name;

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.getList();

  


    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');

    this.paramForm = this.fb.group({
      label: new FormControl("", Validators.required),
      description:new FormControl(""),
      codeCategorie:new FormControl(this.codeCategorie),
      style: new FormControl(""),

    });

   
  }

    
  ngAfterViewChecked()
  {
    this.cdRef.detectChanges();
  }




  getList() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<any>();

    this.parametreService.getListParametre(this.codeCategorie).subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
        ////console.log("datasource: ",this.dataSource.data)
      }
    })
  }

  delete(cbmarq: any) {
    this.spinnerservice.show()
    this.parametreService.deleteParametre(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.spinnerservice.hide()
          this.toastr.success('Paramètre a été supprimé avec succès !');
          this.getList();
        } else {
          this.spinnerservice.hide()
          this.toastr.error(res.message);
        }

      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un paramètre',
      text: 'Voulez-vous vraiment supprimer ce paramètre ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {

        this.delete(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  edit(row){

    this.cbmarq=row.cbmarq;
    this.paramForm.patchValue({
      cbmarq:this.cbmarq,
      label:row?.label,
      description:row?.description,
      codeCategorie:this.codeCategorie,
      style:row?.style,
    })
    ////console.log("row on edit: ",this.paramForm.value);   

  } 


  resetForm(){
    this.paramForm.reset();
    this.getList();
    this.cbmarq=0;
    this.submitted = false;
    this.paramForm.patchValue({
      codeCategorie:this.codeCategorie
    })
  }

  onSubmit() {
    this.spinnerservice.show()
    this.submitted = true;

    if (this.paramForm.invalid) {
      this.spinnerservice.hide()
      return;
    }

   // //console.log("submit: ",this.paramForm.value)

    //ajout
    if (this.cbmarq == 0) {
      this.spinnerservice.show()


      this.parametreService.postParametre(this.paramForm.value).subscribe((res: any) => {
        if (res.statut) {
          this.spinnerservice.hide()
          this.toastr.success("Paramètre a été ajouté avec succès !", 'Success!', { progressBar: true })
          this.resetForm();

        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },error => {
        this.toastr.error('Erreur lors de l\'ajout  . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
      

    } else { //modification
      this.spinnerservice.show()
      ////console.log("update ....")

      let formUpdate={
        cbmarq:this.cbmarq,
        description: this.paramForm.value.description,
        label: this.paramForm.value.label,
        style: this.paramForm.value.style
      }

      this.parametreService.putParametre(formUpdate).subscribe((res: any) => {
        if (res.statut) {
          this.spinnerservice.hide()
          this.toastr.success("Paramètre a été modifié avec succès !", 'Success!', { progressBar: true })
          this.resetForm();

        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de modification . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
    }

  }

}
