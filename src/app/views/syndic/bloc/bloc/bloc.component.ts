import { Component, Input, NgModule, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import Swal from 'sweetalert2';
import { AddBlocComponent } from '../add-bloc/add-bloc.component';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-bloc',
  templateUrl: './bloc.component.html',
  styleUrls: ['./bloc.component.scss'],
  animations: [SharedAnimations]

})
export class BlocComponent implements OnInit {
  @Input() id: number;
  viewMode: 'list' | 'grid' = 'list';
  allSelected: boolean;
  page = 1;
  pageSize = 8;
  blocs: any[] = [];
  primary = 'primary';
  residences: any = [];
  message: any;
  selectedValue: any;
  idd: any;
  blocForm:FormGroup
  submitted: boolean;
  residenceObj: any;
  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  id_current_user: any;
  infoPref: any;
  constructor(private blocService: BlocService, private fb: FormBuilder, private modalService: NgbModal,
    private spinnerservice: NgxSpinnerService,
    private dialog: MatDialog, private toastr: ToastrService, private route: ActivatedRoute, public router: Router,
    private residenceService: ResidenceService, private permissionservice: DroitAccesService,
    private preferenceService: PreferencesService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.infoPref = pre.data
      this.pageSize = this.infoPref.affichageTableaux
    })
    const id = this.route.snapshot.params['cbmarq'];
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000016');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000007');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000017');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000018');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000019');

    this.getBlocs(id)
    this.residenceService.getResidence(id).subscribe((res: any) => {
      if(res.statut){
        this.residenceObj = res.data;
        this.spinnerservice.hide()
      }else{
        this.spinnerservice.hide()
      }
    })

    this.idd = id;
    this.listeresidence()
    this.blocForm = this.fb.group({
      residence: new FormControl(null, Validators.required),
      nbrBloc: new FormControl(0, [Validators.required, Validators.min(1)]),
      nbrAppartementParBloc: new FormControl(0, [Validators.required, Validators.min(0)]),
      prefix: new FormControl("", Validators.required),
      suffix: new FormControl("", Validators.required),
    });

  }
  onSubmit() {
    this.spinnerservice.show()
    if (this.blocForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    //console.log(this.blocForm.value)
    this.blocService.genererBloc(this.blocForm.value).subscribe((res: any) => {
      if (res.statut == true) {
        this.toastr.success("Bloc généré avec succès !", 'Success!', { progressBar: true })
        this.submitted = false;
        this.getBlocs(this.idd)
        this.modalService.dismissAll()
        this.ngOnInit();
        if (this.router.url.includes('/syndic/modifier-residence/')) {
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/syndic/modifier-residence/', parseInt(this.idd)]);
          });
        }
        this.blocForm.reset();
        this.spinnerservice.hide()
      }
      else {
        this.spinnerservice.hide()
        this.submitted = true;
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de la génération  d\'un bloc . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      }
    )

  }
  open(content) {
    this.residenceService.getResidence(this.idd).subscribe((res: any) => {
      //console.log('datarrrrrr',res.data)
      this.blocForm.patchValue({
        residence: res?.data?.cbmarq,
      })
    })
    // this.blocForm.patchValue({
    //   residence:this.idd,
    // })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title1' },)
    .result.then((result) => {
      //console.log(result);
    }, (reason) => {
      //console.log('Err!', reason);
    });
  }
  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      if(res.statut){
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  getBlocs(id) {
    this.spinnerservice.show()
    this.blocService.getBlocs(id).subscribe((res: any) => {
      if (res.statut) {
        //console.log('bloc', res.data)
        this.blocs = res.data;
        this.spinnerservice.hide()
      }
      else {
        this.spinnerservice.hide()
        this.blocs.length = 0
      }

    },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  addBloc1() {
    this.dialog.open(AddBlocComponent, { data: { default: '', idres: this.idd} }).afterClosed().subscribe(() => {
      this.getBlocs(this.idd);
      this.ngOnInit();

    });
  }
  addBloc() {
    this.dialog.open(AddBlocComponent).afterClosed().subscribe(() => {
      this.getBlocs(this.idd);
      // this.router.navigate(['/syndic/modifier-residence/', res.data?.cbmarq]);
      this.ngOnInit();

    });
  }
  editBloc(residence) {
    //console.log(residence)
    this.dialog.open(AddBlocComponent, { data: { default: residence, idres: this.idd} }).afterClosed().subscribe(() => {
      this.getBlocs(this.idd)
      this.ngOnInit()

    });

  }

  deleteBloc(cbmarq: any) {
    this.spinnerservice.show()
    this.blocService.deleteBloc(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Bloc supprimer avec succès !', 'Success!', { progressBar: true });
          this.getBlocs(this.idd);
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!', 'Erreur!', { progressBar: true });
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un bloc',
      text: 'Voulez-vous vraiment supprimer ce bloc ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.deleteBloc(cbmarq);
        this.ngOnInit()
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
}
