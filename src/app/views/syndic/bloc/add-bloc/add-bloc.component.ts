import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Bloc } from 'src/app/shared/models/bloc.model';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { BlocComponent } from '../bloc/bloc.component';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-bloc',
  templateUrl: './add-bloc.component.html',
  styleUrls: ['./add-bloc.component.scss']
})
export class AddBlocComponent implements OnInit {
  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  blocForm: FormGroup;
  residences: any = [];
  cbmarq: any;
  idd: any;
  readonly: boolean = true;
  modeEdit: boolean;
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  constructor(private fb: FormBuilder,
    private blocService: BlocService,
    private residenceService: ResidenceService,
    private router: Router,
    private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService,
    private route: ActivatedRoute, private actRoute: ActivatedRoute , private permissionservice: DroitAccesService ,
    private dialogRef: MatDialogRef<AddBlocComponent>, @Inject(MAT_DIALOG_DATA) public defaults: any
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    if (this.router.url.includes('/syndic/modifier-residence/')) {
      this.modeEdit = false;
    } else if (this.defaults.default && this.router.url.includes('/syndic/modifier-residence/')) {
      this.modeEdit = true;
    }

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000016');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000017');

    this.listeresidence();
    this.blocForm = this.fb.group({
      intitule: new FormControl("", Validators.required),
      residence: new FormControl(null || parseInt(this.defaults.idres), Validators.required),
      nbrAppartement: new FormControl(0, [Validators.required, Validators.min(0)]),
    });

    if (this.defaults.default && this.router.url.includes("/syndic/modifier-residence/")) {
      this.blocForm = this.fb.group({
        intitule: new FormControl("" || this.defaults.default.intitule, Validators.required),
        residence: new FormControl(null || this.defaults.default.residence.cbmarq, Validators.required),
        nbrAppartement: new FormControl(0 || this.defaults.default.nbrAppartement, [Validators.required, Validators.min(0)]),

      });
      this.spinnerservice.hide()
    }
    this.spinnerservice.hide()
    //console.log('dddd:',this.defaults,this.router.url);
    if (this.defaults.default && this.router.url.includes("syndic/details-residence/")) {
      this.blocForm = this.fb.group({
        intitule: new FormControl("" || this.defaults.default.intitule, Validators.required),
        residence: new FormControl(null || this.defaults.default.residence.cbmarq, Validators.required),
        nbrAppartement: new FormControl(0 || this.defaults.default.nbrAppartement,[Validators.required, Validators.min(0)]),

      });
      this.spinnerservice.hide()
    }
  }

  onSubmit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.blocForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    if (!this.defaults.default) {

      if (this.can_add?.permission) {
        this.blocService.postBloc(this.blocForm.value).subscribe((res: any) => {
          if (res.statut == true) {
            this.toastr.success("Bloc ajouté avec succès !", 'Success!', { progressBar: true })
            this.submitted = false;
            this.onClose();
            this.blocForm.reset();
            this.ngOnInit();
            if (this.router.url.includes('/syndic/modifier-residence/')) {
              this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
                this.router.navigate(['/syndic/modifier-residence/', parseInt(this.defaults.idres)]);
              });
            }
            this.spinnerservice.hide()
          }
          else {
            this.spinnerservice.hide()
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }

        },
          error => {
            this.spinnerservice.hide()
            this.toastr.error('Erreur lors de l\'ajout  d\'un bloc . Veuillez réessayer !', 'Erreur!', { progressBar: true })
            //console.log('erreur: ', error);
          }
        );
      } else {
        this.spinnerservice.hide()
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }
    } else {
      this.blocForm.value.cbmarq = this.defaults.default.cbmarq;
      if (this.can_edit?.permission) {
        this.blocService.putBloc(this.blocForm.value).subscribe((res: any) => {
          if (res.statut == true) {
            this.toastr.success("Bloc modifié avec succès !", 'Success!', { progressBar: true })
            this.submitted = false;
            this.onClose()
            this.ngOnInit()
            this.spinnerservice.hide()
          }
          else {
            this.spinnerservice.hide()
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }

        },
          error => {
            this.spinnerservice.hide()
            this.toastr.error('Erreur lors de la modification d\'un bloc . Veuillez réessayer !', 'Erreur!', { progressBar: true });
          }
        );
      } else {
        this.spinnerservice.hide()
        this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
      }
    }

  }
  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  onClose() {
    this.dialogRef.close();
  }
}
