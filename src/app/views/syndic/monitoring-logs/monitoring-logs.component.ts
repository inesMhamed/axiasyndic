import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatInput} from '@angular/material/input';
import {first} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material/dialog';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Router } from '@angular/router';
import { Permission } from 'src/app/shared/models/permission.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';

@Component({
  selector: 'app-monitoring-logs',
  templateUrl: './monitoring-logs.component.html',
  styleUrls: ['./monitoring-logs.component.scss']
})
export class MonitoringLogsComponent implements OnInit {

  displayedColumns: string[] = ['numero', 'type' , 'cbcreation' , 'residence', 'bloc' , 'action'];
  dataSource = new MatTableDataSource<any>();
  pageEvent: PageEvent;
  compteurs;
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  residences: any[];
  types: any[];
  blocs: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  @ViewChild('closeModal') private closeModal: ElementRef;
  @ViewChild('closedModal') private closedModal: ElementRef;
  data: any;
  checked: any;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor( private preferenceService: PreferencesService,
    private blocService: BlocService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private permissionservice: DroitAccesService,
    private router: Router) { }

  ngOnInit(): void {

    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.submitted = false;
    this.checked = 1;

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000029');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000030');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000031');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000033');
    this.checkAccess();

  }

   
checkAccess(){
  //if access false ==> go out to 403
  this.permissionservice.getAccessUser( this.id_current_user, 'FN21000033').subscribe((res:any)=>{

   this.have_access =res.data;
  
 },error=>{},()=>{

   if(!this.have_access){
     this.router.navigate(['403']);
   }else{
    this.listeLogs();
   }
  
 });
}

listeLogs(){

}

applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();
  //console.log(this.dataSource, this.dataSource.filteredData);
  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}


}
