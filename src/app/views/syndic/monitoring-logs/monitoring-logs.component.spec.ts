import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringLogsComponent } from './monitoring-logs.component';

describe('MonitoringLogsComponent', () => {
  let component: MonitoringLogsComponent;
  let fixture: ComponentFixture<MonitoringLogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonitoringLogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
