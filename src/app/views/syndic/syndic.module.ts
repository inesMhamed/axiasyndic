import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListResidenceComponent } from './residences/list-residence/list-residence.component';
import { AddResidenceComponent } from './residences/add-residence/add-residence.component';
import { SyndicRoutingModule } from './syndic-routing.module';

import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ngx-custom-validators';
import { TagInputModule } from 'ngx-chips';
import { ImageCropperModule } from 'ngx-img-cropper';
import { TextMaskModule } from 'angular2-text-mask';
import { FormWizardModule } from '../../shared/components/form-wizard/form-wizard.module';
import { FormsRoutingModule } from '../forms/forms-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { EditResidenceComponent } from './residences/edit-residence/edit-residence.component';
import { ListGroupementComponent } from './groupement/list-groupement/list-groupement.component';
import { AddGroupementComponent } from './groupement/add-groupement/add-groupement.component';
import { EditGroupementComponent } from './groupement/edit-groupement/edit-groupement.component';
import { ListCompteurComponent } from './compteur/list-compteur/list-compteur.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
// import { NgSelectModule } from '@ng-select/ng-select';
import { CustomPaginator } from '../customPaginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';


import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioButton, MatRadioModule } from '@angular/material/radio';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { CdkTableModule } from '@angular/cdk/table';
import { EditCompteurComponent } from './compteur/edit-compteur/edit-compteur.component';
import { ContratsComponent } from './contrat/contrats/contrats.component';
import { EditContratComponent } from './contrat/edit-contrat/edit-contrat.component';
import { DetailsAppartementComponent } from './appartement/details-appartement/details-appartement.component';
import { EcheanceAppartementComponent } from './appartement/echeance-appartement/echeance-appartement.component';
import { AddEditAppartementComponent } from './appartement/add-edit-appartement/add-edit-appartement.component';
import { ListAppartementComponent } from './appartement/list-appartement/list-appartement.component';
import { BlocComponent } from './bloc/bloc/bloc.component';
import { AddBlocComponent } from './bloc/add-bloc/add-bloc.component';
import { DetailsResidenceComponent } from './residences/details-residence/details-residence.component';
import { CaisseComponent } from './caisse/caisse.component';
import { AddEditReglementComponent } from './reglement/add-edit-reglement/add-edit-reglement.component';
import { ListReglementComponent } from './reglement/list-reglement/list-reglement.component';
import { DetailsReglementComponent } from './reglement/details-reglement/details-reglement.component';
import { ListDepenseComponent } from './depense/list-depense/list-depense.component';
import { AddEditDepenseComponent } from './depense/add-edit-depense/add-edit-depense.component';
import { DetailsDepenseComponent } from './depense/details-depense/details-depense.component';
import { AddEditProprietaireComponent } from './proprietaire/add-edit-proprietaire/add-edit-proprietaire.component';
import { DetailsProprietaireComponent } from './proprietaire/details-proprietaire/details-proprietaire.component';
import { ListProprietaireComponent } from './proprietaire/list-proprietaire/list-proprietaire.component';
import { AddEditTransfertComponent } from './transfert/add-edit-transfert/add-edit-transfert.component';
import { ListTransfertComponent } from './transfert/list-transfert/list-transfert.component';
import { DetailsTransfertComponent } from './transfert/details-transfert/details-transfert.component';
import { ListUtilisateursComponent } from './utilisateurs/list-utilisateurs/list-utilisateurs.component';
import { DroitAccesComponent } from './droit-acces/droit-acces.component';
import {OrderByPipe} from '../../orderby.pipe';
import { BarRatingModule } from 'ngx-bar-rating';
import { AddContratComponent } from './contrat/add-contrat/add-contrat.component';
import { AlertComponent } from './contrat/alert/alert.component';
import { AddCompteurComponent } from './compteur/add-compteur/add-compteur.component';
import { DetailsCaisseComponent } from './caisse/details-caisse/details-caisse.component';
import { TracabiliteTransfertComponent } from './transfert/tracabilite-transfert/tracabilite-transfert.component';
import { NodeTransfertComponent } from './transfert/node-transfert/node-transfert.component';
import { InputSocketComponent } from './transfert/socket-transfert/input-socket.component';
import { OutputSocketComponent } from './transfert/socket-transfert/output-socket.component';
import { DragDropModule} from '@angular/cdk/drag-drop';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import { InfoEquipementComponent } from './config-maintenance/equipement/info-equipement/info-equipement.component';
import { ListEquipementComponent } from './config-maintenance/equipement/list-equipement/list-equipement.component';
import { ListFamilleEquipementComponent } from './config-maintenance/famille-equipement/list-famille-equipement/list-famille-equipement.component';
import { InfoFamilleEquipementComponent } from './config-maintenance/famille-equipement/info-famille-equipement/info-famille-equipement.component';
import { InfoGammeOperatoireComponent } from './config-maintenance/gamme-operatoire/info-gamme-operatoire/info-gamme-operatoire.component';
import { ListGammeOperatoireComponent } from './config-maintenance/gamme-operatoire/list-gamme-operatoire/list-gamme-operatoire.component';
import { ListOperationComponent } from './config-maintenance/operation/list-operation/list-operation.component';
import { InfoOperationComponent } from './config-maintenance/operation/info-operation/info-operation.component';
import { ConfigMaintenanceComponent } from './config-maintenance/config-maintenance.component';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import {A11yModule} from '@angular/cdk/a11y';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatDividerModule} from '@angular/material/divider';
import {OverlayModule} from '@angular/cdk/overlay';
import { InfoEmplacementComponent } from './config-maintenance/emplacement/info-emplacement/info-emplacement.component';
import { ListEmplacementComponent } from './config-maintenance/emplacement/list-emplacement/list-emplacement.component';
import { DetailsBienPropComponent } from './proprietaire/details-proprietaire/details-bien-prop/details-bien-prop.component';
import { DetailsDocumentsPropComponent } from './proprietaire/details-proprietaire/details-documents-prop/details-documents-prop.component';
import { DetailsCompteursPropComponent } from './proprietaire/details-proprietaire/details-compteurs-prop/details-compteurs-prop.component';
import { DetailsContratsPropComponent } from './proprietaire/details-proprietaire/details-contrats-prop/details-contrats-prop.component';
import { DetailsReclamationsPropComponent } from './proprietaire/details-proprietaire/details-reclamations-prop/details-reclamations-prop.component';
import { DetailsUserComponent } from './utilisateurs/details-user/details-user.component';
import { DetailsProduitsUserComponent } from './utilisateurs/details-user/details-produits-user/details-produits-user.component';
import { DetailsCaissesUserComponent } from './utilisateurs/details-user/details-caisses-user/details-caisses-user.component';
import {DetailsInterventionUserComponent} from './utilisateurs/details-user/details-intervention-user/details-intervention-user.component';
import { DetailsTransfertsUserComponent } from './utilisateurs/details-user/details-transferts-user/details-transferts-user.component';
import { DetailsLogsUserComponent } from './utilisateurs/details-user/details-logs-user/details-logs-user.component';
import { ListTransfertRecusComponent } from './transfert/list-transfert/list-transfert-recus/list-transfert-recus.component';
import { ListTransfertEnvoyeComponent } from './transfert/list-transfert/list-transfert-envoye/list-transfert-envoye.component';
import { ConfigNotificationComponent } from './config-notification/config-notification.component';
import { GenFraisSyndicComponent } from './residences/gen-frais-syndic/gen-frais-syndic.component';
import { GenOperationComponent } from './config-maintenance/operation/gen-operation/gen-operation.component';
import { ConfigTypesComponent } from './config-types/config-types.component';
import { ConfigSocieteComponent } from './config-societe/config-societe.component';
import { GenTypesComponent } from './config-types/gen-types/gen-types.component';
import { ListGenOperationComponent } from './config-maintenance/operation/list-gen-operation/list-gen-operation.component';
import { MonitoringLogsComponent } from './monitoring-logs/monitoring-logs.component';
import { UserPhoneComponent } from './user-phone/user-phone.component';
import { LogAdministrationComponent } from './log-administration/log-administration.component';
import { LogSystemComponent } from './log-system/log-system.component';
import { ChangepwdComponent } from './utilisateurs/changepwd/changepwd.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {DialogComponent} from './appartement/echeance-appartement/dialog.component';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  declarations: [ListResidenceComponent, AddResidenceComponent, EditResidenceComponent, ListGroupementComponent, AddGroupementComponent,
    EditGroupementComponent, ListCompteurComponent, EditCompteurComponent, ContratsComponent, EditContratComponent,
    AddBlocComponent, BlocComponent, DetailsResidenceComponent, ListAppartementComponent, AddEditAppartementComponent,
    EcheanceAppartementComponent, DetailsAppartementComponent, CaisseComponent, AddEditReglementComponent, ListReglementComponent,
    DetailsReglementComponent, ListDepenseComponent, AddEditDepenseComponent, DetailsDepenseComponent, AddEditProprietaireComponent,
    DetailsProprietaireComponent, ListProprietaireComponent, AddEditTransfertComponent, ListTransfertComponent, DetailsTransfertComponent,
    ListUtilisateursComponent, DroitAccesComponent, OrderByPipe, AddContratComponent, AlertComponent, AddCompteurComponent, DetailsCaisseComponent, TracabiliteTransfertComponent, NodeTransfertComponent,
    InputSocketComponent, OutputSocketComponent,
    InfoEquipementComponent, ListEquipementComponent, ListFamilleEquipementComponent, InfoFamilleEquipementComponent, InfoGammeOperatoireComponent, ListGammeOperatoireComponent, ListOperationComponent,
    InfoOperationComponent, ConfigMaintenanceComponent, InfoEmplacementComponent, ListEmplacementComponent, DetailsBienPropComponent, DetailsDocumentsPropComponent, DetailsCompteursPropComponent,
    DetailsContratsPropComponent, DetailsReclamationsPropComponent, DetailsUserComponent, DetailsProduitsUserComponent, DetailsCaissesUserComponent, DetailsInterventionUserComponent,
    DetailsTransfertsUserComponent, DetailsLogsUserComponent, ListTransfertRecusComponent, ListTransfertEnvoyeComponent, ConfigNotificationComponent, GenFraisSyndicComponent, GenOperationComponent,
    ConfigTypesComponent, ConfigSocieteComponent,
    GenTypesComponent,
    ListGenOperationComponent,
    MonitoringLogsComponent,
    UserPhoneComponent,
    LogAdministrationComponent,
    LogSystemComponent,
    ChangepwdComponent, DialogComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    NgxIntlTelInputModule,

    CommonModule,
    SharedComponentsModule,
    NgbModule,
    SyndicRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDaterangepickerMd.forRoot({
      format: 'DD/MM/YYYY', // could be 'YYYY-MM-DDTHH:mm:ss.SSSSZ'
      displayFormat: 'DD/MM/YYYY', // default is format value
      direction: 'ltr', // could be rtl
      weekLabel: 'S',
      separator: ' à ', // default is ' - '
      cancelLabel: 'Annuler', // detault is 'Cancel'
      applyLabel: 'OK', // detault is 'Apply'
      clearLabel: 'VIDER', // detault is 'Clear'
      customRangeLabel: 'Personnalisé',
      daysOfWeek: ['dim', 'lu', 'ma', 'me', 'je', 've', 'sa'],
      monthNames: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juil', 'Juin', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
      firstDay: 1 // first day is monday
    }),
    CustomFormsModule,
    TagInputModule,
    ImageCropperModule,
    TextMaskModule,
    FormWizardModule,
    FormsRoutingModule,
    NgxPaginationModule,
    NgxDatatableModule,
    NgSelectModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatSortModule
    CdkTableModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatIconModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    NgSelectModule,
    MatFormFieldModule,
    BarRatingModule,
    DragDropModule,
    NgxSpinnerModule

  ],
  exports: [
    PortalModule,
    ScrollingModule,
    CdkStepperModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDividerModule,
    OverlayModule,
    A11yModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: MatPaginatorIntl, useValue: CustomPaginator() },  // Here
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    SharedDataService
  ],
})
export class SyndicModule { }
