import {AfterViewInit, Component, OnInit, QueryList, ViewChild} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Caisse } from 'src/app/shared/models/caisse.model';
import { CaisseService } from 'src/app/shared/services/caisse.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {Permission} from '../../../shared/models/permission.model';
import {DroitAccesService} from '../../../shared/services/droit-acces.service';

@Component({
  selector: 'app-caisse',
  templateUrl: './caisse.component.html',
  styleUrls: ['./caisse.component.scss']
})
export class CaisseComponent implements OnInit {
  displayedColumns: string[] = ['intitule', 'solde', 'transfert', 'caisseSiege', 'action'];
  displayedColumnsSiege: string[] = ['intitule', 'solde', 'transfert', 'action'];
  dataSource = new MatTableDataSource<Caisse>();
  dataSource1 = new MatTableDataSource<Caisse>([]);
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator ;
  @ViewChild(MatPaginator, {static: false}) paginator1: MatPaginator ;
  @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatSort) sort1: MatSort;
  listCaisse: any = [];
  listecaissesiege: any = [];
  formCaisse: FormGroup
  formResponsable: FormGroup
  submitted: boolean;
  modeEdit: boolean = false;
  idCaisse: any;
  typeSiege: boolean;
  caisseSiegeDefault: any;
  listResponsables: any
  list: any[] = []
  typeCaisse: any;
  responsables: any=[];
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  can_addS: Permission;
  can_editS: Permission;
  can_deleteS: Permission;
  can_listeS: Permission;
  can_detailS: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(
    private preferenceService: PreferencesService, private spinnerservice: NgxSpinnerService,
    private caisseService: CaisseService, private toastr: ToastrService, private fb: FormBuilder, private utilisateurService: UtilisateurService,
    private modalService: NgbModal, private proprietaireService: ProprietaireService, private permissionservice: DroitAccesService, private router: Router
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000052');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000053');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000054');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000055');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000056');

    this.can_addS = this.permissionservice.search( this.id_current_user, 'FN21000057');
    this.can_editS = this.permissionservice.search( this.id_current_user, 'FN21000058');
    this.can_deleteS = this.permissionservice.search( this.id_current_user, 'FN21000059');
    this.can_detailS = this.permissionservice.search( this.id_current_user, 'FN21000060');
    this.can_listeS = this.permissionservice.search( this.id_current_user, 'FN21000061');

      
    this.getAccessListCaisse();
    

    this.caisseList();
    this.caisseSiege();
    this.formCaisse = this.fb.group({
      intitule: new FormControl(null, Validators.required),
      caisseSiege: new FormControl(null, Validators.required),
    });
    this.formResponsable = this.fb.group({
      responsabels: new FormControl([], Validators.required),
    });

  }

  getAccessListCaisse(){

    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000056').subscribe((res:any)=>{

      this.have_access =res.data;
    },error=>{},()=>{
      this.getAccessListCaisseSource();
    }
    );

  }
  getAccessListCaisseSource(){
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000061').subscribe((res:any)=>{
      if(!this.have_access && !res.data){
        this.router.navigate(['403']);

      }
    },error=>{},()=>{
    }
    );
  }

  

  updatetableS() {
    this.caisseSiege();
  }
  updatetableC() {
    this.caisseList();
  }
  onSubmitSiege() {
    this.formCaisse.removeControl('caisseSiege')
    this.submitted = true;
    //console.log('formCaisse :', this.formCaisse.value)
    if (this.formCaisse.invalid) {
      return;
    }
    if (this.modeEdit) {
      // //console.log('editerrr :', this.idCaisse,this.formCaisse.value)
      this.formCaisse.value.cbmarq = this.idCaisse
      this.caisseService.putCaisseSiege(this.formCaisse.value).subscribe((res: any) => {
        //console.log('editerrr :', res)
        if (res.statut) {
          this.submitted = false;
          this.formCaisse.reset();
          this.modalService.dismissAll()
          this.caisseSiege()
          this.toastr.success('La caisse siège est modifiée avec succès ', 'Success!', { progressBar: true })

        }
        else {
          this.toastr.error(res.message, 'Error!', { progressBar: true })
        }
      })
    }
    else {
      this.caisseService.postCaisseSiege(this.formCaisse.value).subscribe((res: any) => {
        //console.log('ressss :', res)
        if (res.statut) {
          this.submitted = false;
          this.formCaisse.reset();
          this.caisseSiege()
          this.modalService.dismissAll()

          this.toastr.success('La caisse siège est enregistrée avec succès ', 'Success!', { progressBar: true })

        }
        else {
          this.toastr.error(res.message, 'Error!', { progressBar: true })
        }
      })
    }
  }

  onSubmit() {
    this.submitted = true;
    //console.log('formCaisse :', this.formCaisse.value)
    if (this.formCaisse.invalid) {
      return;
    }
    if (this.modeEdit) {
      this.formCaisse.value.cbmarq = this.idCaisse
      this.caisseService.putCaisse(this.formCaisse.value).subscribe((res: any) => {
        //console.log('editerrr :', res)
        if (res.statut) {
          this.submitted = false;
          this.formCaisse.reset();
          this.modalService.dismissAll()
          this.caisseList()
          this.toastr.success('La caisse est modifiée avec succès ', 'Success!', { progressBar: true })

        }
        else {
          this.toastr.error(res.message, 'Error!', { progressBar: true })
        }
      })
    }
    else {
      this.caisseService.postCaisse(this.formCaisse.value).subscribe((res: any) => {
        //console.log('ressss :', res)
        if (res.statut) {
          this.submitted = false;
          this.formCaisse.reset();
          this.modalService.dismissAll()
          this.caisseList()

          this.toastr.success('La caisse est enregistrée avec succès ', 'Success!', { progressBar: true })

        }
        else {
          this.toastr.error(res.message, 'Error!', { progressBar: true })
        }
      })
    }

  }
  addResponsables(id) {
    //   this.addResponsable(this.formResponsable.value,)
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }

  }
  applyFilterSiege(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource1.filter = filterValue.trim().toLowerCase();
    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }

  }
  //liste caisse
  caisseList() {
    this.caisseService.getCaisses().subscribe((res: any) => {
      if(res.statut){
        //console.log("caisseList: ",res.data)
        this.dataSource.data = res.data;
        this.dataSource.paginator =  this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.spinnerservice.hide()
    });
  }
  //list caisse siège
  caisseSiege() {
    this.spinnerservice.show()
    this.caisseService.getCaisseSiege().subscribe((res: any) => {
      if(res.statut){
        //console.log("caisseSiege : ",res.data)
        this.listecaissesiege = res.data;
        this.dataSource1.data = res.data;
        this.dataSource1.paginator =  this.paginator1;
        this.dataSource1.sort = this.sort;
      }
      this.spinnerservice.hide()
    });
  }
  //editer caisse
  editCaisse(content, row) {
    this.spinnerservice.show()
    //this.caisseSiege()
    //console.log("editrow: ",row)
    this.idCaisse = row.cbmarq
    this.modeEdit = true
    this.caisseService.getCaissebyId(row.cbmarq).subscribe((res: any) => {
      if(res.statut){
        //console.log("caisse id :",res.data)
        this.caisseSiegeDefault = res.data.caisseSiege.cbmarq
        this.formCaisse.patchValue({
          intitule: res.data.intitule,
          caisseSiege: res.data.caisseSiege.cbmarq,
        })
      }
      this.spinnerservice.hide()
    })
    /***** check if have caisse siege *****/
    if(this.listecaissesiege.some(item=> item.cbmarq===row.caisseSiege.cbmarq)){

      this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title1' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });

    }else{
      this.toastr.error('Vous n\'avez pas le droit de modifier la caisse siège "'+row?.caisseSiege?.intitule+'"', 'Erreur!', { progressBar: true })

    }
   

    

  }
  submitResponsable() {
    this.spinnerservice.show()
    this.submitted = true;
    if (this.formResponsable.invalid) {
      this.spinnerservice.hide()
      return;
    }
    this.formResponsable.value.responsabels.forEach(element => {
      this.list.push({ user: element })
    });
    this.formResponsable.value.responsabels = this.list
    //console.log('formResponsable111 :', this.formResponsable.value,this.typeCaisse)
    this.caisseService.AddResponsable(this.formResponsable.value,this.idCaisse, this.typeCaisse).subscribe((result: any) => {
      if (result.statut == true) {
        this.submitted = false;
          this.formResponsable.reset();
          this.modalService.dismissAll()
          this.caisseList()
        this.toastr.success('Responsable est ajouté à la caisse avec succès ', 'Success!', { progressBar: true })
      }
      else{
        this.toastr.error(result.message, 'Erreur!', { progressBar: true })
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard !', 'Success!', { progressBar: true })
      })
  }
  //editer caisse siège
  editCaisseSiege(content, id) {
    this.spinnerservice.show()
    this.idCaisse = id

    this.modeEdit = true
    this.caisseService.getCaisseSiegebyId(id).subscribe((data: any) => {
      this.formCaisse.patchValue({
        intitule: data.data.intitule,
      })
      this.spinnerservice.hide()
    })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title2' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });

  }
  //add responsabels
  modalResponsables(content, cbmarq,type) {
    this.idCaisse = cbmarq
    this.typeCaisse=type
    this.modeEdit = false
    this.getListResponsables(cbmarq,type);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title3' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  //add caisse
  addCaisse(content) {
    this.formCaisse.addControl('caisseSiege', this.fb.control('')); //wz

    this.caisseSiege()
    this.formCaisse.reset()
    this.modeEdit = false
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title1' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  //add caisse siège
  addCaisseSiege(content) {
    this.modeEdit = false
    this.formCaisse.reset()
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title2' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }

  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une caisse',
      text: 'Voulez-vous vraiment supprimer cett caisse ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.spinnerservice.show()
        this.caisseService.deleteCaisse(cbmarq).subscribe((res: any) => {
          //console.log('supprimer  :', res)
          if (res.statut) {
            this.submitted = false;
            this.formCaisse.reset();
            this.modalService.dismissAll()
            this.spinnerservice.hide()
            this.caisseList()
            this.toastr.success('La caisse est supprimée avec succès ', 'Success!', { progressBar: true })

          }
          else {
            this.toastr.error(res.message, 'Error!', { progressBar: true })
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  confirmBoxSiege(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une caisse',
      text: 'Voulez-vous vraiment supprimer cett caisse siège?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.caisseService.deleteCaisseSiege(cbmarq).subscribe((res: any) => {
          //console.log('supprimer siège :', res)
          if (res.statut) {
            this.submitted = false;
            this.formCaisse.reset();
            this.modalService.dismissAll()
            this.caisseSiege()
            this.toastr.success('La caisse siège est supprimée avec succès ', 'Success!', { progressBar: true })

          }
          else {
            this.toastr.error(res.message, 'Error!', { progressBar: true })
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  onchangeCaisse(event) {
    if (this.modeEdit) {
      //console.log(event.cbmarq)
      Swal.fire({
        title: 'Modifier caisse siège',
        text: "Voulez-vous vraiment changer la caisse siège de cette caisse!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui',
        cancelButtonText: 'Annuler'
      }).then((result) => {
        if (result.isConfirmed) {
          this.formCaisse.value.caisseSiege = event.cbmarq
        }
        else {
          this.caisseService.getCaissebyId(this.idCaisse).subscribe((data: any) => {
            this.formCaisse.patchValue({
              intitule: data.data.intitule,
              caisseSiege: data.data.caisseSiege.cbmarq,
            })
          })
        }
      })
    }
  }

  getListResponsables(cbmarq: any, type: any) {
    // this.utilisateurService.getAllUtilisateurs().subscribe((data: any) => {
    //   this.listResponsables = data.data;
    // });
    this.caisseService.getResponsableNoExiste(cbmarq , type).subscribe((data: any) => {
      this.listResponsables = data.data;
    });
  }
}
