import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCaisseComponent } from './details-caisse.component';

describe('DetailsCaisseComponent', () => {
  let component: DetailsCaisseComponent;
  let fixture: ComponentFixture<DetailsCaisseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsCaisseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCaisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
