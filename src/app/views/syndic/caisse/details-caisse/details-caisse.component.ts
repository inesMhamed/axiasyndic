import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { CaisseService } from 'src/app/shared/services/caisse.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Moment } from 'moment';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-details-caisse',
  templateUrl: './details-caisse.component.html',
  styleUrls: ['./details-caisse.component.scss'],
  providers: [DatePipe]
})
export class DetailsCaisseComponent implements OnInit {
  selected: { startDate: Moment, endDate: Moment };
  public singlePicker = {

    opens: "left"
  }
  ranges: any = {
    Aujourdhui: [moment(), moment()],
    Hier: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    '7 derniers jours': [moment().subtract(6, 'days'), moment()],
    '30 derniers jours': [moment().subtract(29, 'days'), moment()],
    'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
    'Mois dernier': [
      moment()
        .subtract(1, 'month')
        .startOf('month'),
      moment()
        .subtract(1, 'month')
        .endOf('month')
    ],
    '3 derniers mois': [
      moment()
        .subtract(3, 'month')
        .startOf('month'),
      moment()
        .subtract(1, 'month')
        .endOf('month')
    ]
  };
  cbmarq: any;
  detailsCaisse: any;
  formResponsable: FormGroup
  submitted: boolean;
  list: any[] = []
  listResponsables: any;
  typeCaisse: any;
  idCaisse: any;
  listeResponsables: any;
  resposablesCaisse: any;
  mouvement: any;
  pageSize: any;
  infoPref: any;

  have_access:boolean =false;
  id_current_user: any;

  displayedColumns: string[] = ['Nature','Type' ,'Montant','Date','Mode de payement' ,'Action'];
  dataSource = new MatTableDataSource<any>();  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(public router: Router, private proprietaireService: ProprietaireService,
    private formBuilder: FormBuilder, private modalService: NgbModal,
    private preferenceService: PreferencesService, private spinnerservice: NgxSpinnerService,
    private datepipe: DatePipe, private permissionservice: DroitAccesService,
    private route: ActivatedRoute, private utilisateurService: UtilisateurService,
    private toastr: ToastrService, private caisseService: CaisseService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');

    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
      this.infoPref = pre.data
    })
    this.route.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq')
    });


    //this.getMouvement();
    if (this.cbmarq && this.router.url.includes("/syndic/details-caisse/")) {
      this.checkAccessCaisse();
      this.getDetailsCaisse(this.cbmarq)
      this.typeCaisse = 1

    } else if (this.cbmarq && this.router.url.includes("/syndic/details-caisseSiege/")) {
      this.checkAccessCaisseSiege();
      this.getDetailsSiege(this.cbmarq)
      this.typeCaisse = 2
    }
    this.formResponsable = this.formBuilder.group({
      responsabels: new FormControl([], Validators.required),
    })

  }

  checkAccessCaisse() {
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000055').subscribe((res: any) => {

      this.have_access = res.data;

    }, error => { }, () => {

      if (!this.have_access) {
        this.router.navigate(['403']);
      }

    });
  }

  checkAccessCaisseSiege() {
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000060').subscribe((res: any) => {

      this.have_access = res.data;

    }, error => { }, () => {

      if (!this.have_access) {
        this.router.navigate(['403']);
      }

    });
  }

  getDetailsCaisse(id) {
    this.caisseService.getCaissebyId(id).subscribe((caisse: any) => {
      this.detailsCaisse = caisse?.data;
      this.spinnerservice.hide()
    });

  }
  getDetailsSiege(id) {
    this.caisseService.getCaisseSiegebyId(id).subscribe((caisse: any) => {
      this.detailsCaisse = caisse?.data;
      this.spinnerservice.hide()
    });

  }
  getMouvement() {
    this.spinnerservice.show()
    this.caisseService.getMouvementCaisse(this.cbmarq).subscribe((mvt: any) => {
      if (mvt.statut) {
        this.mouvement = mvt.data;
        this.dataSource.data = mvt.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.spinnerservice.hide()
    });

  }
  onChange(event) {
    //console.log("on change: ",event)
    if(event?.startDate?._d!=null && event?.endDate?._d){
     
      this.dataSource.data=this.dataSource.data.filter(mvt=>  ( (event?.startDate?._d <= new Date(mvt.dateMvt.date) )  &&  (new Date(mvt.dateMvt.date) <=event?.endDate?._d  ))  )

     
    }else{
      this.getMouvement();
    }
   
  }
  openResponsable(content, cbmarq) {
    this.idCaisse = cbmarq;
    this.getListResponsables(cbmarq, this.typeCaisse );
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title3' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  submitResponsable() {
    this.submitted = true;
    if (this.formResponsable.invalid) {
      return;
    }
    this.formResponsable.value.responsabels.forEach(element => {
      this.list.push({ user: element });
    });
    this.formResponsable.value.responsabels = this.list
    this.caisseService.AddResponsable(this.formResponsable.value, this.idCaisse, this.typeCaisse).subscribe((result: any) => {
      if (result.statut === true) {
        this.submitted = false;
        this.formResponsable.reset();
        this.modalService.dismissAll()
        this.getResponsablesCaisse(this.cbmarq, this.typeCaisse)
        this.toastr.success('Responsable est ajouté à la caisse avec succès ', 'Success!', { progressBar: true });
      } else {
        this.toastr.error(result.message, 'Error!', { progressBar: true });
      }
    },
      error => {
        this.toastr.error('Veuillez réessayer plus tard !', 'Success!', { progressBar: true });
      });
  }
  getResponsablesCaisse(cbmarq, type) {
    this.caisseService.getResponsableCaisse(cbmarq, type).subscribe((result: any) => {
      if (result.statut == true) {
        this.resposablesCaisse = result.data;

        //console.log("this.resposablesCaisse: ",this.resposablesCaisse)
        this.detailsCaisse.responsables=this.resposablesCaisse;

        //console.log("add resp: ",this.detailsCaisse)

      }
    })
  }
  confirmBox(type, idcaisse, idresponsable) {
    Swal.fire({
      title: 'Retirer responsable ',
      text: 'Voulez-vous vraiment retirer ce responsable de la caisse ?',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.removResponsble(type, idcaisse, idresponsable);

      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  removResponsble(type, idcaisse, idresponsable) {
    this.caisseService.deleteResponsable(type, idcaisse, idresponsable).subscribe((result: any) => {
      if (result.statut == true) {
        this.toastr.success('Responsable est retiré de la caisse avec succès ', 'Success!', { progressBar: true })
        // this.getResponsablesCaisse(this.cbmarq, this.typeCaisse)
        this.detailsCaisse = result.data
      }
      else {
        this.toastr.error(result.message, 'Error!', { progressBar: true })

      }
    })
  }
  getListResponsables(cbmarq: any , type: any) {
    // this.utilisateurService.getAllUtilisateurs().subscribe((data: any) => {
    //   this.listResponsables = data.data
    // })
    this.caisseService.getResponsableNoExiste(cbmarq , type).subscribe((data: any) => {
      this.listResponsables = data.data;
    });
  }
}
