import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogAdministrationComponent } from './log-administration.component';

describe('LogAdministrationComponent', () => {
  let component: LogAdministrationComponent;
  let fixture: ComponentFixture<LogAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
