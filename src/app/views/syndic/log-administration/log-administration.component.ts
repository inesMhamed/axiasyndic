import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LogAdmin } from 'src/app/shared/models/logadmin';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { LogadminService } from 'src/app/shared/services/logadmin.service';
import { DatePipe } from '@angular/common';
import { Toast, ToastrService } from 'ngx-toastr';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import * as XLSX from 'xlsx';
import { NgxSpinnerService } from 'ngx-spinner';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Component({
  selector: 'app-log-administration',
  templateUrl: './log-administration.component.html',
  styleUrls: ['./log-administration.component.scss'],
  providers: [DatePipe]
})
export class LogAdministrationComponent implements OnInit {
  displayedColumns: string[] = ['Action', 'Actionneur', 'Date', 'Description'];
  public searchInput = new FormControl();
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  pageSize = 10;
  logList: any;
  @ViewChild('TABLE') table: ElementRef;
  ws: any;
  pageEvent: PageEvent;
  pageIndex: number = 0;
  length: number;
  searchTerm: any;

  constructor(private logadminService: LogadminService, private datePipe: DatePipe, private toastr: ToastrService, private spinnerservice: NgxSpinnerService,) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.getArticles('');
    this.searchInput.valueChanges
      .pipe(debounceTime(1_000), distinctUntilChanged())
      .subscribe((searchTerm) => {
        this.pageIndex = 0;
        this.searchTerm = searchTerm;
        this.getArticles(searchTerm);
      });
  }

  public getServerData(event?: PageEvent) {
    this.logadminService.getLogAdmin(event.pageIndex, event.pageSize, '').subscribe((res: any) => {
      if (res.statut === false) {
        this.dataSource.data = [];
      } else {
        this.logList = res.data;
        this.dataSource.data = res.data;
        this.length = res.count;

      }
    });
  }

  getloglist(page, perpage, keyword) {
    this.spinnerservice.show()
    this.logadminService.getLogAdmin(page, perpage, '').subscribe((res: any) => {
      if (res.statut === false) {
        this.dataSource.data = [];
      } else {
        console.log(res);
        this.logList = res.data;
        this.dataSource.data = res.data;
        this.length = res.count;
        this.getServerData({ previousPageIndex: 0, pageIndex: 1, pageSize: 10, length: this.length });
      }
      this.spinnerservice.hide()
    });
  }

  exportdata() {
    this.logadminService.getLogAdmin(-1, this.pageSize, '')
      .subscribe((resp: any) => {
        if (resp.data.length > 0) {
          let dataSource = resp.data.map(el => ({ id: el.cbmarq, date: this.datePipe.transform(el.date.date, 'short'), action: el.action, actionneur: el.actionneur, description: el.description }))
          this.logadminService.exportAsExcelFile(dataSource, 'Log Administration');
          // const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(dataSource);
          // const wb: XLSX.WorkBook = XLSX.utils.book_new();
          // XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
          //
          // /* save to file */
          // XLSX.writeFile(wb, 'SheetJS.xlsx');
        } else {
          this.toastr.warning('Aucune donnée à exporter !');
        }
      });

  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  //
  // }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



  public getArticles(searchTerm = '') {
    this.spinnerservice.show()
    this.logadminService.getLogAdmin(this.pageIndex, this.pageSize, searchTerm)
      .subscribe((resp: any) => {
        this.length = resp.count;
        // this.dataSource.data = resp.data;
        this.logList = resp.data;
        this.spinnerservice.hide()
      });
  }

  public changePage(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize
    this.getArticles('');
  }

}
