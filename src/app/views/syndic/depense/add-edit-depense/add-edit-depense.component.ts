import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinner, NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { CompteurService } from 'src/app/shared/services/compteur.service';
import { ContratService } from 'src/app/shared/services/contrat.service';
import { DepenseService } from 'src/app/shared/services/depense.service';
import { GroupementService } from 'src/app/shared/services/groupement.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';

@Component({
    selector: 'app-add-edit-depense',
    templateUrl: './add-edit-depense.component.html',
    styleUrls: ['./add-edit-depense.component.scss'],
    providers: [DatePipe]
})
export class AddEditDepenseComponent implements OnInit {
    @ViewChild('modalBasic') opnModal: TemplateRef<any>;
    depenseForm: FormGroup;
    submitted: boolean;
    prelevementCaisse: boolean = false;
    repartitionAppartement: boolean = false;
    somme: any;
    modeEdit: any;
    logement = 1;
    id: string;
    depense = 43;
    modedepense: any = [{ value: 1, label: 'Résidence' }, { value: 2, label: 'Groupement' }, { value: 3, label: 'Terrain' }];
    listgroupes: any;
    listResidence: any;
    listTerrains: any;
    typeDepList: any;
    listes: any = null;
    compteur: boolean = false;
    contrat: boolean = false;
    tranche: number;
    listAppartements: any = [];
    idResidence: any;
    selected: any = '';
    labelSelect: string;
    test: any;
    showButton: boolean;
    hideThisDiv: boolean = false;
    montantTranche = 0;
    @ViewChild('labelImport')
    labelImport: ElementRef;
    fileToUpload: File = null;
    labelCaisse: any;
    typedep: string;
    pourcentage: string;
    repartiSelected: any;
    can_add: Permission;
    can_edit: Permission;
    id_current_user: any;
    pageSize: any;
    infoPref: any;
    currentDate: any;
    @ViewChild('repartitionAppartementElm')
    repartitionAppartementElm: ElementRef;

    have_access:boolean =false;


    constructor(private formBuilder: FormBuilder, private depenseService: DepenseService,
        private datepipe: DatePipe,
        private spinnerservice:NgxSpinnerService,
        private preferenceService: PreferencesService,
        private residenceService: ResidenceService, private groupeService: GroupementService,
        private route: ActivatedRoute, private router: Router, private toastr: ToastrService,
        private compteurService: CompteurService, private contratService: ContratService,
        private appartementService: AppartementService, private modalService: NgbModal, private permissionservice: DroitAccesService,
        private cdRef:ChangeDetectorRef
    ) {
    }

    ngOnInit(): void {
        this.spinnerservice.show()
        this.preferenceService.getPreferences(1).subscribe((pre: any) => {
            this.pageSize = pre.data.affichageTableaux
            this.infoPref = pre.data
        })
        this.modeEdit = false;
        this.compteur = false;
        this.contrat = false;
        this.id_current_user = localStorage.getItem('id');
        this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000068');
        this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000069');
        this.listgroupement();
        this.currentDate = new Date();

        this.listresidence();
        this.typeDepense('TDP');
        this.depenseForm = this.formBuilder.group({
            select: new FormControl(null, Validators.required),
            date: new FormControl(new Date(), Validators.required),
            residence: new FormControl(null),
            groupement: new FormControl(null),
            type: new FormControl(null, Validators.required),
            intitule: new FormControl('', Validators.required),
            description: new FormControl('',),
            montant: new FormControl('', Validators.required),
            compteur: new FormControl(null),
            reste: new FormControl(0),
            choix: new FormControl(0),
            contrat: new FormControl(null),
            repartitionAppartement: new FormControl(false),//
            prelevementCaisse: new FormControl(false),
            lignes: this.formBuilder.array([]),
        });

        this.route.paramMap.subscribe(params => {
            this.id = params.get('cbmarq');
        });
        if (this.id != null) {
            this.modeEdit = true;
            // this.getDepense(this.id);
            this.depenseService.getDepensebyId(this.id).subscribe((res: any) => {
                this.depenseForm.value.lignes = [];
                this.listAppartements=[];
                this.listAppartements = res.data.lignes;
                ////console.log('onInit editet', res.data);
                if (this.modeEdit) {
                    this.depenseForm.patchValue({
                        date: res?.data?.date,
                        intitule: res?.data?.intitule,
                        description: res?.data?.description,
                        montant: res?.data?.montant,
                        type: res?.data?.type?.cbmarq,
                        residence: res?.data?.residence?.cbmarq,
                        groupement: res?.data?.groupement?.cbmarq,
                        compteur: res?.data?.compteur?.cbmarq,
                        contrat: res?.data?.contrat?.cbmarq,
                        repartitionAppartement: res?.data?.repartitionAppartement,
                        prelevementCaisse: res?.data?.prelevementCaisse,

                    });
                    this.tranche = this.depenseForm.value.montant;
                    ////console.log('depenseForm: ', this.depenseForm.value);
                    this.repartitionAppartement=res?.data?.repartitionAppartement;

                    Object.keys(res.data).forEach(name => {
                        if (name === 'lignes') {
                            this.test = res.data[name];
                            Object.keys(this.test).forEach(n => {
                                 this.addLigne();
                                var lig = this.test[n];

                                ////console.log(" model edit ligne: ",lig);

                                ( (this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('appartement').patchValue(<number>lig?.appartement?.cbmarq);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('intitule').patchValue(lig?.appartement?.intitule);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('montant').patchValue(lig?.montant.toFixed(3));
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('pourcentage').patchValue(lig?.pourcentage);


                                ////console.log("appartement array: ",( (this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('appartement').value);

                            });
                        }
                    });
                    /***** get residence/groupement/terrain *****/
                    //    [{ value: 1, label: 'Résidence' }, { value: 2, label: 'Groupement' }, { value: 3, label: 'Terrain' }];

                    //terrain
                    if (res.data.residence!=null && res.data.residence.code=="E0063") {

                        this.selected = 'Terrain';
                        this.onchangeStyle({ cbmarq: this.depenseForm.value.residence });
                        this.depenseForm.patchValue({
                            select: 3
                        })

                        

                    } 
                    //produit
                    if (res.data.residence!=null && res.data.residence.code=="E0062") {

                        this.selected = 'Résidence';
                        this.onchangeStyle({ cbmarq: this.depenseForm.value.residence });
                        this.depenseForm.patchValue({
                            select: 1
                        })

                    } 
                    if (res.data.groupement!=null && res.data.residence==null) {
                    
                        this.selected = 'Groupement';
                        this.onchangeStyle({ cbmarq: this.depenseForm.value.groupement });
                        this.depenseForm.patchValue({
                            select: 2
                        })

                    }

                    ////console.log("after getDep: ",this.depenseForm.value)
                  
                    
                    if (this.depenseForm.value.compteur != null) {
                        this.compteur = true;
                        this.contrat = false;
                        if (this.depenseForm.value.residence != null && this.depenseForm.value.groupement == null) {
                            this.compteurService.getCompteursResidence(this.depenseForm.value.residence).subscribe((compteur: any) => {
                                ////console.log('compteur residence', compteur.data);
                                this.listes = compteur.data;
                            });
                        } else {
                            this.compteurService.getCompteursGroupement(this.depenseForm.value.groupement).subscribe((compteur: any) => {
                                this.listes = compteur.data;
                            });
                        }

                    } else if (this.depenseForm.value.contrat != null) {
                        this.compteur = false;
                        this.contrat = true;
                        if (this.depenseForm.value.residence != null && this.depenseForm.value.groupement == null) {
                            this.contratService.getContratsResidence(this.depenseForm.value.residence).subscribe((contrat: any) => {
                                this.listes = contrat.data;
                            });
                        } else {
                            this.contratService.getContratsGroupement(this.depenseForm.value.groupement).subscribe((contrat: any) => {
                                this.listes = contrat.data;
                            });
                        }
                    }
                }
            });
            this.spinnerservice.hide()
        }else{
            this.spinnerservice.hide()
        }

        this.checkAccessAddEdit();

    }

    checkAccessAddEdit(){

        if (this.id && this.modeEdit) {
            this.checkAccess('FN21000069');
        } else {
            this.checkAccess('FN21000068');
        }

    }

    checkAccess(code){
        //if access false ==> go out to 403
        this.permissionservice.getAccessUser( this.id_current_user, code).subscribe((res:any)=>{
    
        this.have_access =res.data;
        
      },error=>{},()=>{
    
        if(!this.have_access){
          this.router.navigate(['403']);
        }
        
      });
      }

    ngAfterViewChecked()
    {
        this.cdRef.detectChanges();
    }

    onSubmit() {
        this.spinnerservice.show()
        // console.log('1 submit form add', this.depenseForm.value);

        this.depenseForm.patchValue({
            repartitionAppartement: this.repartiSelected
        });

        if (this.depenseForm.invalid) {
            this.spinnerservice.hide()
            ////console.log('invalid form,', this.depenseForm.controls);
            this.submitted = true;
            return;
        }
        if (this.depenseForm.value.repartitionAppartement == false) {
            this.depenseForm.controls['lignes'].clearValidators;
            this.depenseForm.controls['lignes'].updateValueAndValidity();
        } else {
            this.depenseForm.controls['lignes'].setValidators([Validators.required]);
            this.depenseForm.controls['lignes'].updateValueAndValidity();

        }

        this.somme = 0;

        if (this.depenseForm.value.lignes.length != 0 && this.depenseForm.value.repartitionAppartement) {
            this.depenseForm.getRawValue().lignes.forEach(i => {
                if (i.selected === true) {
                    ////console.log("this.somme: ",(+this.somme.toFixed(3)),"%: ",i.pourcentage ," parseFloat%: ",parseFloat(i.pourcentage))
                    this.somme = (+this.somme.toFixed(3)) + parseFloat(i.pourcentage);
                }
                let removeIndex = this.depenseForm.value.lignes.findIndex(itm => itm.selected === false);
                if (removeIndex !== -1) {
                    this.depenseForm.value.lignes.splice(removeIndex, 1);
                }
            });
           // //console.log("somme = ",this.somme)

            if (this.somme > 100) {
                this.toastr.error('La somme des pourcentages  est supérieure à 100 !', 'Erreur!', { progressBar: true });
                return;
            } else if (this.somme < 100) {
                this.toastr.error('La somme des pourcentages  est inférieure à 100 !', 'Erreur!', { progressBar: true });
                return;
            } else {
                /*this.depenseForm.value.lignes.forEach(i => {
                    delete i.selected;
                    delete i.pourcentage;
                    delete i.intitule;
                });wzz to delete*/
            }
            this.spinnerservice.hide()
        }

        if (!this.modeEdit) {
             if (this.can_add?.permission) {
                 this.depenseForm.value.date = this.datepipe.transform(new Date(this.depenseForm.value.date), 'dd-MM-yyyy');
                 delete this.depenseForm.value.select;
                 this.depenseForm.value.montant = this.depenseForm.value.montant.toString();
                 //post deponse
                 //console.log('3 submit form add', this.depenseForm.value);
                 this.depenseService.postDepense(this.depenseForm.value).subscribe((submit: any) => {
                     ////console.log('statut', submit)
                     if (submit.statut === true) {
                         this.toastr.success('Dépense est enregistrée avec succès !', 'Succès!', { progressBar: true });
                         this.depenseForm.reset();
                        this.router.navigate(['/syndic/depenses']);
                     } else {
                         this.toastr.error(submit.message, 'Erreur!', { progressBar: true });
                     }
                 });
                 this.spinnerservice.hide()
             } else {
                this.spinnerservice.hide()
                this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
             }
             this.spinnerservice.hide()
        } else {
            if (this.can_edit?.permission) {
                //put deponse

                this.depenseForm.patchValue({
                    residence:this.depenseForm.value.residence!=undefined?this.depenseForm.value.residence:null,
                    groupement:this.depenseForm.value.groupement!=undefined?this.depenseForm.value.groupement:null,
                    compteur: this.depenseForm.value.compteur!=undefined?this.depenseForm.value.compteur:null,
                    contrat: this.depenseForm.value.contrat!=undefined?this.depenseForm.value.contrat:null,
                });

                this.depenseForm.value.cbmarq = parseInt(this.id);
                this.depenseForm.value.montant = this.depenseForm.value.montant.toString();
                //console.log('form edit', this.depenseForm.value);

                this.depenseService.putDepense(this.depenseForm.value).subscribe((submit: any) => {
                   // //console.log('statut edit', submit);
                    if (submit.statut == true) {
                        this.toastr.success("Dépense modifié avec succès !", 'Success!', { progressBar: true })

                        //this.router.navigate([`/syndic/details-depense/`]);
                        this.router.navigate(['/syndic/depenses'])
                    } else {
                        this.toastr.error(submit.message, 'Erreur!', { progressBar: true });
                    }
                },
                    error => {
                        //console.log(error);
                    });
                    this.spinnerservice.hide()
            } else {
                this.spinnerservice.hide()
                this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
            }
            this.spinnerservice.hide()
        }

    }

    // getDepense(id) {
    //     this.depenseService.getDepensebyId(id).subscribe((res: any) => {
    //         this.listAppartements = res.data.lignes;
    //         if (this.modeEdit) {
    //             this.depenseForm.patchValue({
    //                 date: res.data.date,
    //                 intitule: res.data.intitule,
    //                 description: res.data.description,
    //                 montant: res.data.montant,
    //                 type: res.data.type.cbmarq,
    //                 residence: res?.data?.residence?.cbmarq,
    //                 groupement: res?.data?.groupement?.cbmarq,
    //                 compteur: res?.data?.compteur?.cbmarq,
    //                 contrat: res?.data?.contrat?.cbmarq,
    //                 repartitionAppartement: res.data.repartitionAppartement,
    //                 prelevementCaisse: res.data.prelevementCaisse,

    //             });

    //             if (this.depenseForm.value.residence) {
    //                 this.selected = 'Résidence';
    //                 this.depenseForm.value.groupement = null;
    //                 this.onchangeStyle({ cbmarq: this.depenseForm.value.residence });
    //             } else {
    //                 this.selected = 'Groupement';
    //                 this.depenseForm.value.residence = null;
    //                 this.onchangeStyle({ cbmarq: this.depenseForm.value.groupement });
    //             }

    //             Object.keys(this.listAppartements).forEach(n => {
    //                 var lig: any = this.listAppartements[n];
    //                 this.addLigne();
    //                 ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.appartement?.cbmarq);
    //                 ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.appartement?.intitule);
    //                 ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(lig?.montant);
    //             });
    //             //console.log(this.modedepense);
    //             if (this.depenseForm.value.groupement != null) {
    //                 this.depenseForm.patchValue({ select: this.modedepense[1].value });
    //             } else if ((this.depenseForm.value.residence != null)) {
    //                 this.depenseForm.patchValue({ select: this.modedepense[0].value });
    //             }

    //             if (this.depenseForm.value.compteur != null) {
    //                 this.compteur = true;
    //                 this.contrat = false;
    //                 this.compteurService.getCompteurs().subscribe((compteur: any) => {
    //                     this.listes = compteur.data;
    //                 });
    //             } else if (this.depenseForm.value.contrat != null) {
    //                 this.compteur = false;
    //                 this.contrat = true;
    //                 this.contratService.getContrats().subscribe((contrat: any) => {
    //                     this.listes = contrat.data;
    //                 });
    //             }
    //         }
    //     });
    // }

    typeDepense(code) {
        this.depenseService.getTypeDepense(code).subscribe((type: any) => {
            //console.log(type.data);
            this.typeDepList = type.data;

        },
            error => {
                //console.log(error);
            });
    }

    listresidence() {
        this.residenceService.getResidences().subscribe((residence: any) => {
            this.listResidence = residence?.data.filter(el => (el?.type?.code == 'E0062'));
            this.listTerrains = residence?.data.filter(el => (el?.type?.code == 'E0063'));
        });
    }

    listgroupement() {
        this.groupeService.getGroupements().subscribe((groupe: any) => {
            this.listgroupes = groupe.data;
        });

    }

    open(content) {

        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
            .result.then((result) => {
                //console.log(result);
            }, (reason) => {
                //console.log('Err!', reason);
            });
    }

    changeProduit(event) {
        this.listes = [];

    }

    //1. Type Produit
    onchangecategorie(event) {

        this.depenseForm.patchValue({
            residence:null,
            groupement:null,
            type: null,
            compteur:null,
            contrat:null,
            montant:0,
            repartitionAppartement:false
         });

        this.selected = event.label;
        ////console.log("selected: ",event)
        const residence = this.depenseForm.controls['residence'];
        const groupement = this.depenseForm.controls['groupement'];

        if (this.selected === 'Résidence' || this.selected === 'Terrain') {
            this.depenseForm.value.groupement = 0;
            residence.setValidators([Validators.required]);
            groupement.clearValidators;
            groupement.setValidators([]);
            // this.depenseForm.patchValue({ groupement: 0 });
        }
        if (this.selected === 'Groupement') {
            groupement.setValidators([Validators.required]);
            residence.clearValidators;
            residence.setValidators([]);
            this.depenseForm.value.residence = 0;
            // this.depenseForm.patchValue({ residence: 0 });
        }
        groupement.updateValueAndValidity();
        residence.updateValueAndValidity();
    }

    //2.
    changeListeProduit(event) {

        this.depenseForm.patchValue({
            type: null,
            compteur: null,
            contrat: null,
            montant: 0,
            repartitionAppartement: false
         });
        this.residenceService.getResidence(this.depenseForm?.value?.residence).subscribe((res: any) => {
            if(res.statut){
                this.labelCaisse = res.data?.caisse?.intitule;

            }
        });

        if (event === 'Terrain') {
            this.onchangeStyle('Terrain');
        } else {
            for (let i = 0; i < this.depenseForm.value.lignes.length; i++) {
                this.removeLigne(i);
            }
        }

    }

    //3.
     //on change type depense
     onchangeType(event) {
        this.listAppartements=[];
        this.depenseForm.patchValue({
            compteur:null,
            contrat:null,
            // montant:0,
            repartitionAppartement:false
         });
        ////console.log("event changetype: ",event)

        /**** select compteur ****/
        if (event?.code === 'E0043') {
            this.typedep = event?.code;
            this.listes = [];
            this.compteur = true;
            this.contrat = false;
            this.labelSelect = 'Compteur';
            this.depenseForm.get('compteur').setValidators([Validators.required]);
            this.depenseForm.get('contrat').clearValidators();
            // this.depenseForm.get('repartitionAppartement').disable();          

            this.depenseForm.controls['compteur'].updateValueAndValidity();
            this.depenseForm.controls['contrat'].updateValueAndValidity();

            this.depenseForm.value.contrat = 0;
            if (this.depenseForm.value.residence != null && this.depenseForm.value.groupement == null) {
                this.compteurService.getCompteursResidence(this.depenseForm.value.residence).subscribe((compteur: any) => {
                    //console.log('compteur residence', compteur.data);
                    this.listes = compteur.data;
                });
            } else {
                this.compteurService.getCompteursGroupement(this.depenseForm.value.groupement).subscribe((compteur: any) => {
                    this.listes = compteur.data;
                });
            }
        }
          /**** select contrat ****/
        else if (event?.code === 'E0044') {
            this.typedep = event?.code;
            this.listes = [];
            this.contrat = true;
            this.compteur = false;
            this.labelSelect = 'Contrat';
            this.depenseForm.get('contrat').setValidators([Validators.required]);
            this.depenseForm.get('compteur').clearValidators();
            //this.depenseForm.controls['compteur'].setValidators([]);
            // this.depenseForm.get('repartitionAppartement').disable();

            this.depenseForm.controls['compteur'].updateValueAndValidity();
            this.depenseForm.controls['contrat'].updateValueAndValidity();

            this.depenseForm.value.compteur = null;

            if (this.depenseForm.value.residence != null && this.depenseForm.value.groupement == null) {
                this.contratService.getContratsResidence(this.depenseForm.value.residence).subscribe((contrat: any) => {
                    this.listes = contrat.data;
                });
            } else {
                this.contratService.getContratsGroupement(this.depenseForm.value.groupement).subscribe((contrat: any) => {
                    this.listes = contrat.data;
                });
            }
        }
        /***** travaux ***/
        else if (event?.code !== 'E0044' || event.code !== 'E00443') {

            const arr = this.depenseForm.controls.lignes as FormArray; this.listAppartements = [];
            while (0 !== arr.length) {
                arr.removeAt(0);
            }

            this.labelSelect = 'Travaux';
            this.typedep = event?.code;
            this.contrat = false;
            this.compteur = false;
            this.listes = null
            // this.depenseForm.get('repartitionAppartement').enable();
            this.depenseForm.get('compteur').clearValidators();
            this.depenseForm.controls['compteur'].updateValueAndValidity();
            this.depenseForm.get('contrat').clearValidators();
            this.depenseForm.controls['contrat'].updateValueAndValidity();

            ////console.log("change Travaux this.depenseForm: ",this.depenseForm)

            if (this.depenseForm.value.residence != null && this.depenseForm.value.groupement == null) {
                this.appartementService.getAppartementResidenceprop(this.depenseForm.value.residence).subscribe((app: any) => {
                    this.listAppartements = app.data;
                    ////console.log("Travaux: listAppartements: ",this.listAppartements)
                    if (!this.modeEdit) {
                        if (this.listAppartements != null) {
                            //console.log("Size listAppartements: ",this.listAppartements.length)

                            //console.log("pourcentage: ",(100 / this.listAppartements.length).toFixed(1))
                            Object.keys(this.listAppartements).forEach(n => {
                                this.addLigne();
                                var lig: any = this.listAppartements[n];
                                ////console.log("pourcentage: ",lig.intitule);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig.cbmarq);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(<number>lig.intitule);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue((this.tranche * parseFloat(lig?.pourcentage) / 100).toFixed(3));
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));

                            });

                        }
                    }

                });
            }else{
                this.appartementService.getAppartementGroupementprop(this.depenseForm.value.groupement).subscribe((app: any) => {
                    this.listAppartements = app.data;

                    if (!this.modeEdit) {
                        if (this.listAppartements != null) {
                            Object.keys(this.listAppartements).forEach(n => {
                                this.addLigne();
                                var lig: any = this.listAppartements[n];

                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig.cbmarq);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue((this.tranche * parseFloat(lig?.pourcentage) / 100).toFixed(3));
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(<number>lig.intitule + ' | ' + lig.residence.intitule);
                                ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));
                            });

                        }
                    }
                });
            }

        }
        /***** Autres */
        else {

        }
        this.depenseForm.get('contrat').updateValueAndValidity();
        this.depenseForm.get('compteur').updateValueAndValidity();

        this.repartiSelected=false;
        this.repartitionAppartement=false;

    }

    //4.list contrat
    changeContrat(event) {

        // console.log("change contrat: ",event , this.tranche);
        const arr = this.depenseForm.controls.lignes as FormArray; this.listAppartements = [];
        while (0 !== arr.length) {
            arr.removeAt(0);
        }

        this.contratService.getContrat(event.cbmarq).subscribe((app: any) => {
            this.repartiSelected = app?.data?.repartition;

            this.listAppartements = app.data.lignes;
            if (this.listAppartements != null) {

                console.log("changeContrat listApp: ",this.listAppartements)
                Object.keys(this.listAppartements).forEach(n => {
                    this.addLigne();
                    var lig: any = this.listAppartements[n];

                    ////console.log("change contrat: ",lig);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.appartement?.cbmarq);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.appartement?.intitule);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue(<number>lig?.pourcentage);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue((this.tranche * parseFloat(lig?.pourcentage) / 100).toFixed(3));
                    // ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                    if (this.depenseForm.value.residence === null && this.depenseForm.value.groupement != null) {
                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.appartement.intitule + ' | ' + lig.appartement.produit);
                    }
                });
            }

            ////console.log('rrr', this.repartiSelected)
            if (app?.data?.repartition == 1) {
                this.repartitionAppartement = true;
                this.depenseForm.patchValue({ repartitionAppartement: true });

            } else {
                ////console.log('rrr', this.repartiSelected)
                this.repartitionAppartement = false;
                this.depenseForm.patchValue({ repartitionAppartement: false });
            }

        });

    }
    //4.Compteur
    changeCompteur(event) {
        const arr = this.depenseForm.controls.lignes as FormArray; this.listAppartements = [];
        ////console.log(event);
        while (0 !== arr.length) {
            arr.removeAt(0);
        }
        this.compteurService.getCompteur(event.cbmarq).subscribe((app: any) => {
            this.repartiSelected = app?.data?.repartition;
            ////console.log('rrr', this.repartiSelected)

            this.listAppartements = app.data.lignes;
            //console.log("changeCompteur listApp: ",this.listAppartements)

            if (this.listAppartements != null) {
                Object.keys(this.listAppartements).forEach(n => {
                    this.addLigne();
                    var lig: any = this.listAppartements[n];

                    ////console.log("change compteur: ",lig);

                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.appartement?.cbmarq);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.appartement?.intitule);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue(<number>lig?.pourcentage);
                    // ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                    ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue((this.tranche * parseFloat(lig?.pourcentage) / 100).toFixed(3));
                    if (this.depenseForm.value.residence === null && this.depenseForm.value.groupement != null) {
                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.appartement.intitule + ' | ' + lig.appartement.produit);
                    }
                });
            }


            if (app.data.repartition === 1) {
                this.repartitionAppartement = true;
                this.depenseForm.patchValue({ repartitionAppartement: true });

            } else {
                this.repartitionAppartement = false;
                this.depenseForm.patchValue({ repartitionAppartement: false });

                while (0 !== arr.length) {
                    arr.removeAt(0);
                }
            }

        });
    }


    //5. montant
    changeMontant(event) {

        this.tranche = event;
        ////console.log("tranche: ",this.tranche," repartitionAppartement: ",this.depenseForm.value.repartitionAppartement)
        if (this.listAppartements && this.depenseForm.value.repartitionAppartement) {
            Object.keys(this.listAppartements).forEach(n => {
                var lig: any = this.listAppartements[n];
                let montantLigne: any;
                montantLigne = ((((this.depenseForm?.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('pourcentage')?.value * this.tranche) / 100).toFixed(3);

                if (true) {//!this.modeEdit

                    if (((this.depenseForm?.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('selected').value) {
                        ((this.depenseForm?.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('montant').patchValue(montantLigne);

                    } else {
                        ((this.depenseForm?.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('appartement').patchValue(lig.cbmarq);
                    }

                }

            });
            ////console.log("on change montant: ",this.depenseForm.value)
        }

    }


    //Répartition par appartement change
    changeLignes(event) {

        if (event.checked === false && !this.repartitionAppartementElm.nativeElement.readOnly) {
            const arr = this.depenseForm.controls.lignes as FormArray;
            ////console.log("changeLignes",arr);
            while (0 !== arr.length) {
                arr.removeAt(0);
                ////console.log("remove at 0: ",arr)
            }
        } else {
            let typeDep = this.typeDepList.find(itm => itm.cbmarq === this.depenseForm.value.type);
            if (typeDep !== -1) {
                if (typeDep?.code !== 'E0044' || typeDep.code !== 'E00443') {

                    const arr = this.depenseForm.controls.lignes as FormArray; this.listAppartements = [];
                    while (0 !== arr.length) {
                        arr.removeAt(0);
                    }

                    this.listes = null
                    // this.depenseForm.get('repartitionAppartement').enable();
                    this.depenseForm.get('compteur').clearValidators();
                    this.depenseForm.controls['compteur'].updateValueAndValidity();
                    this.depenseForm.get('contrat').clearValidators();
                    this.depenseForm.controls['contrat'].updateValueAndValidity();

                    if (this.depenseForm.value.residence != null && this.depenseForm.value.groupement == null) {
                        this.appartementService.getAppartementResidenceprop(this.depenseForm.value.residence).subscribe((app: any) => {
                            this.listAppartements = app.data;
                            if (!this.modeEdit) {
                                if (this.listAppartements != null) {
                                    var purcent = (100 / (this.listAppartements.length));
                                    Object.keys(this.listAppartements).forEach(n => {
                                        this.addLigne();
                                        var lig: any = this.listAppartements[n];

                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig.cbmarq);
                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(<number>lig.intitule);
                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue((this.tranche * (purcent) / 100).toFixed(3));
                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));

                                    });
                                }
                            }

                        });
                    }else {
                        this.appartementService.getAppartementGroupementprop(this.depenseForm.value.groupement).subscribe((app: any) => {
                            this.listAppartements = app.data;
                            console.log('app.data', app ,  app.data);
                            var purcent = (100 / (this.listAppartements.length));
                            if (!this.modeEdit) {
                                if (this.listAppartements != null) {
                                    Object.keys(this.listAppartements).forEach(n => {
                                        this.addLigne();
                                        var lig: any = this.listAppartements[n];

                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig.cbmarq);
                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue((this.tranche * purcent / 100).toFixed(3));
                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.intitule + ' | ' + lig.residence.intitule);
                                        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));
                                    });

                                }
                            }
                        });
                    }

                }
            }
        }
    }


    onchangeStyle(event) {

        this.listAppartements = [];
        ////console.log("WZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ on change style innn...")
        if (this.selected === 'Résidence' || this.selected === 'Terrain') {
            this.appartementService.getAppartementResidenceprop(event.cbmarq).subscribe((app: any) => {
                this.listAppartements = app.data;
                if (!this.modeEdit) {
                    if (this.listAppartements != null) {
                        Object.keys(this.listAppartements).forEach(n => {
                            this.addLigne();
                            var lig: any = this.listAppartements[n];
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig.cbmarq);
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(<number>lig.intitule);
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue(<number>lig.pourcentage);
                        });
                    }
                }

            });
        } else {
            this.appartementService.getAppartementGroupementprop(event.cbmarq).subscribe((app: any) => {
                this.listAppartements = app.data;
                if (!this.modeEdit) {
                    if (this.listAppartements != null) {
                        Object.keys(this.listAppartements).forEach(n => {
                            this.addLigne();
                            var lig: any = this.listAppartements[n];

                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig.cbmarq);
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(<number>lig.intitule);
                            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue(<number>lig.pourcentage);
                        });

                    }
                }
            });
        }
    }

    newligne(): FormGroup {
        return this.formBuilder.group({
            selected: new FormControl(true,),
            appartement: new FormControl(null, Validators.required),
            intitule: new FormControl('', Validators.required),
            pourcentage: new FormControl('', Validators.required),
            montant: new FormControl('', [Validators.required, , Validators.min(0)]),
        });
    }

    lignes(): FormArray {
        return this.depenseForm.get('lignes') as FormArray;
    }

    addLigne() {
        this.lignes().push(this.newligne());
    }

    removeLigne(ligIndex: number) {
        this.lignes().removeAt(ligIndex);
    }

    //Selectionner /déselectionner une ligne
    checkCheckBoxvalue(event, index) {

        let nbSelect: any[] = this.depenseForm.value.lignes.filter(el => (el.selected === true));
        let montantligne: any = 0;
        this.tranche = this.depenseForm.value.montant;
        // console.log("this.depenseForm.value: ",this.depenseForm.value)
        this.depenseForm.getRawValue().lignes.forEach((i, index2) => {
            this.pourcentage = (100 / nbSelect.length).toFixed(1);
            ////console.log("this.tranche : ",this.tranche )
            montantligne = (this.tranche * parseFloat(this.pourcentage) / 100).toFixed(3);

            if (i.selected === true) {
                i.pourcentage = this.pourcentage;
                i.montant = (this.tranche * parseFloat(this.pourcentage) / 100).toFixed(3);
                ((this.depenseForm.get('lignes') as FormArray).at((index2)) as FormGroup).get('pourcentage').patchValue(this.pourcentage);
                ((this.depenseForm.get('lignes') as FormArray).at((index2)) as FormGroup).get('montant').patchValue(i.montant);
                // console.log("montant     : ", ((this.depenseForm.get('lignes') as FormArray).at((index2)) as FormGroup).get('montant'));
            }
        });
        if (event.target.checked) {
            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue(this.pourcentage);
            // ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').enable();//.enable();
            // this.depenseForm['controls'].lignes['controls'][index].controls['appartement'].disable();//.enable();
            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').enable();
            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').disable();//.enable();
            // ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montant').disable();//.enable();
        } else {
            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montant').patchValue(0);
            // ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').enable();
            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue(0);
            ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').disable();
        }
        if (this.depenseForm.value === 0) {
            this.showButton = true;
        }

    }

    changePourcentage(event, index) {

        this.tranche=this.depenseForm.value?.montant;
        ////console.log("change pourcentage: ",event.target.value,";  index: ",index);
        ////console.log("montant Total tranche: ",this.tranche);

        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue(event.target.value);
        ((this.depenseForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montant').patchValue(((event.target.value * this.tranche) / 100).toFixed(3));

        ////console.log("this.depenseForm: ",this.depenseForm)
    }

    onFileChange(files: FileList) {
        this.labelImport.nativeElement.innerText = Array.from(files)
            .map(f => f.name)
            .join(', ');
        this.fileToUpload = files.item(0);

        if (files.length > 0) {
            const file = files[0];
            this.depenseForm.patchValue({
                document: file
            });
        }
    }

    gotoDepenseList(){
        this.router.navigate(['/syndic/depenses'])
    }
}

