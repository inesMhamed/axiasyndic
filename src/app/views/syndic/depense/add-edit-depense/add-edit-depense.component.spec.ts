import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddEditDepenseComponent } from './add-edit-depense.component';


describe('AddEditDepenseComponent', () => {
  let component: AddEditDepenseComponent;
  let fixture: ComponentFixture<AddEditDepenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditDepenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDepenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
