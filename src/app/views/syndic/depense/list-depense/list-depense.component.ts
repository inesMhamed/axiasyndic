import { Component, Input, OnInit, ViewChild } from '@angular/core';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { Depense } from 'src/app/shared/models/depense.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatInput } from '@angular/material/input';
import { DepenseService } from 'src/app/shared/services/depense.service';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-depense',
  templateUrl: './list-depense.component.html',
  styleUrls: ['./list-depense.component.scss']
})
export class ListDepenseComponent implements OnInit {
  displayedColumns: string[] = ['date', 'intitule', 'type', 'repartitionAppartement' , 'montant', 'statut', 'action'];
  dataSource = new MatTableDataSource<Depense>();
  @Input() id: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  listdepenses: any = [];
  cbmarq: string;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  can_confirm: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(public router: Router, private route: ActivatedRoute,
    private preferenceService: PreferencesService, private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService, private depenseService: DepenseService, private permissionservice: DroitAccesService) { }

  ngOnInit(): void {
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000068');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000069');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000070');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000071');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000072');
    this.can_confirm = this.permissionservice.search( this.id_current_user, 'FN21000073');

    this.checkAccess();

    this.getlistDepense();
  }

  checkAccess(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000072').subscribe((res:any)=>{

    this.have_access =res.data;
    
  },error=>{},()=>{

    if(!this.have_access){
      this.router.navigate(['403']);
    }
    
  });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  getlistDepense() {
    this.spinnerservice.show()
    this.depenseService.getAllDepenses().subscribe((res: any) => {
      if(res.statut){
        //console.log(res.data)
        this.listdepenses = res.data;
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
      } else {
        this.spinnerservice.hide()
      }
    });
  }
  editerDepense(id) {
    this.spinnerservice.show()
    this.depenseService.getDepensebyId(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0036") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier dépense confirmée ! ',
        })
      }
      else if (data.statut.code == "E0037") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier dépense annulée ! ',
        })
      } else {
        this.router.navigate(['/syndic/modifier-depense/', id])
      }
      this.spinnerservice.hide()
    })

  }

  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une dépense',
      text: 'Voulez-vous vraiment supprimer cette dépense ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.depenseService.deleteDepense(cbmarq).subscribe((ress: any) => {
          this.spinnerservice.show()
          if (ress.statut == true) {
            this.getlistDepense();
            this.toastr.success('Dépense a été supprimée avec succès !');
            this.spinnerservice.hide()
          }
          else {
            this.spinnerservice.hide()
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
}
