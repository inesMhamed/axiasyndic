import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DepenseService } from 'src/app/shared/services/depense.service';
import Swal from 'sweetalert2'
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-details-depense',
  templateUrl: './details-depense.component.html',
  styleUrls: ['./details-depense.component.scss']
})
export class DetailsDepenseComponent implements OnInit {
  @ViewChild('modalLong') modalLong: TemplateRef<any>;
  @ViewChild('labelImport', { static: true }) labelImport: ElementRef;

  fileToUpload: File = null;
  cbmarq: string;
  depense: any;
  lignes: any;
  imgURL: any;
  depenseForm: FormGroup;
  loading: boolean;
  submitted: boolean;
  typeDepense: string;
  can_confirm: Permission;
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  pageSize: any;
  infoPref: any;
  filenom: any = '';
  have_access:boolean =false;

  constructor(private actRoute: ActivatedRoute, private depenseService: DepenseService,
    private formBuilder: FormBuilder, private modalService: NgbModal, private toastr: ToastrService,
    private permissionservice: DroitAccesService,
    private spinnerservice: NgxSpinnerService,
    private preferenceService: PreferencesService, private router: Router
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
      this.infoPref = pre.data
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000068');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000069');
    this.can_confirm = this.permissionservice.search( this.id_current_user, 'FN21000073');
    this.actRoute.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');

    });
    this.filenom = '';
    this.checkAccess();
    if (this.cbmarq != null) {
      this.depenseService.getDepensebyId(this.cbmarq).subscribe((dep: any) => {
        if (dep.statut === true) {
          console.log(dep.data)
          this.depense = dep.data;
          this.typeDepense = this.depense?.type?.label
          //console.log('fff',this.depense?.document.slice(50))
          this.lignes = dep.data.lignes;
          // this.sourceFrais = reg.data.lignes.sourceFrais;
          // this.prop = reg.data.prop
        }
        this.spinnerservice.hide()
      });
    }
    this.depenseForm = this.formBuilder.group({
      document: new FormControl(null, Validators.required),
    });
  }

  checkAccess() {
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000071').subscribe((res: any) => {
      this.have_access = res.data;
    }, error => {
    }, () => {
      if (!this.have_access) {
        this.router.navigate(['403']);
      }
    });
  }


  confirmerdepense(id) {
    this.spinnerservice.show()
    const swalWithBootstrapButtons = Swal.mixin({})

    swalWithBootstrapButtons.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous voulez vraiment confirmer cette dépense !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.depenseService.confirmDepense(id).subscribe((ress: any) => {
          this.spinnerservice.hide()
          window.location.reload()

        })
        swalWithBootstrapButtons.fire(

          'Confirmer!',
          'Votre dépense est confirmée',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          'La confirmation est annuler ',
          'error'
        )
      }
    })
  }
  Annulerdepense(id) {
    const swalWithBootstrapButtons = Swal.mixin({})

    swalWithBootstrapButtons.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous voulez vraiment annuler cette dépense!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.depenseService.cancelDepense(id).subscribe((ress: any) => {

          window.location.reload()
        })
        swalWithBootstrapButtons.fire(

          'Confirmer!',
          'Votre dépense est annulée',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          'La confirmation est annulée ',
          'error'
        )
      }
    })
  }
  openAttachement(url: any) {
    window.open(url, '_blank');
}

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  // onFileChange(files: FileList) {
  //   let reader = new FileReader();
  //   this.fileToUpload = files.item(0);
  //   reader.readAsDataURL(files.item(0));
  //   reader.onload = (_event) => {
  //     this.imgURL = (reader.result);
  //   };
  // }
  onFileChange(files: FileList) {

    // if (this.labelImport) {
    //   this.labelImport.nativeElement.innerText = Array.from(files)
    this.filenom = Array.from(files)
          .map(f => f.name)
          .join(', ');
      this.fileToUpload = files.item(0);

      if (files.length > 0) {
        const file = files[0];
        this.depenseForm.patchValue({
          document: file
        });
      }
    // }
  }

  onSubmit() {
    this.spinnerservice.show()
    this.loading = true;
    console.log(this.labelImport, this.depenseForm.invalid);
    if (this.depenseForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    const myFormValue = this.depenseForm.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    this.depenseService.addDocument(myFormData, this.cbmarq).pipe(first()).subscribe((res: any) => {
      if (res.statut === true) {
        this.toastr.success('Document est ajouté à la dépense avec succès !');
        this.ngOnInit();
      } else {
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
      this.spinnerservice.hide()

    },
      (error) => {
        this.toastr.error('Erreur . Veuillez réessayer !', 'Erreur!', { progressBar: true });
      });
  }
}
