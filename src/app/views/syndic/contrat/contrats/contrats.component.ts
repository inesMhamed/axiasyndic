import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Compteur} from '../../../../shared/models/compteur.model';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSort} from '@angular/material/sort';
import {MatInput} from '@angular/material/input';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material/dialog';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {first} from 'rxjs/operators';
import {EditContratComponent} from '../../contrat/edit-contrat/edit-contrat.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { ContratService } from '../../../../shared/services/contrat.service';
import { DatePipe } from '@angular/common';
import { CommuneService } from '../../../../shared/services/commune.service';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-contrats',
  templateUrl: './contrats.component.html',
  styleUrls: ['./contrats.component.scss'],
  providers: [DatePipe]
})

export class ContratsComponent implements OnInit {

  displayedColumns: string[] = ['type', 'intitule', 'fournisseur', 'montant', 'dateDebut', 'dateFin', 'action'];
  dataSource = new MatTableDataSource<Compteur>();
  pageEvent: PageEvent;
  compteurs;
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  residences: any[];
  types: any[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  @ViewChild('closeModal') private closeModal: ElementRef;
  @ViewChild('closedModal') private closedModal: ElementRef;
  data: any;
  range: any;
  periodes: any[];
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: any;

  have_access: boolean = false;

  constructor(
    private preferenceService: PreferencesService,
    private contratService: ContratService,
    private communeService: CommuneService,
    private dl: ResidenceService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private spinnerservice: NgxSpinnerService,
    private modalService: NgbModal,
    private datepipe: DatePipe,
    private permissionservice: DroitAccesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.submitted = false;
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000034');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000035');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000036');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000038');

    this.buildFormBasic();

    this.checkAccess();

    
    this.range = new FormGroup({
      start: new FormControl(null, Validators.required ),
      end: new FormControl(null, Validators.required )
    })


  }

  checkAccess(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000038').subscribe((res:any)=>{

      this.have_access = res.data;
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.spinnerservice.hide()
      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {
        this.listeresidence();
        this.listetypes();
        this.listeContrats();
        this.listePeriodes('PRD');
      }

    });
  }


  buildFormBasic() {
    this.formBasic = this.fb.group({
      intitule: new FormControl(null, Validators.required),
      residence: new FormControl( null, Validators.required),
      fournisseur: new FormControl( null, ),
      montant: new FormControl( null,  Validators.min(0)),
      type: new FormControl(null, Validators.required ),
      periode:new FormControl(null, Validators.required ),
      dateDebut: new FormControl(null, ),
      dateFin: new FormControl(null,  ),
      dateAlert: new FormControl(0,  Validators.min(0)),
    });
  }
  listeContrats() {
    this.spinnerservice.show()
    this.contratService.getContrats().subscribe((res: any) => {
      if (res.statut) {
        ////console.log("data source contrat: ",res.data)
        this.dataSource.data = res.data.map(clt => ({ cbmarq: clt.cbmarq, intitule: clt.intitule, type: clt.type.label, fournisseur: clt.fournisseur, montant: clt.montant, dateDebut: clt.dateDebut, dateFin: clt.dateFin, dateDebutList: clt.dateDebutList, dateFinList: clt.dateFinList }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()

    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur:' , error);
      }
    );
  }
  listePeriodes(type){
    this.contratService.gettypesContrat(type).subscribe((type:any)=>{
      if(type.statut){
        this.periodes=type.data
      }
     
    })
  }
  listeresidence() {
    this.spinnerservice.show()
    this.dl.getResidences().subscribe((res: any) => {

      if (res.statut) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
    },
      error => {this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }
  listetypes() {
    this.communeService.gettype('CNT').subscribe((res: any) => {
          // this.types = res.data;
          if(res.statut){
            this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
          }
         
        },
        error => {
          //console.log( 'erreur: ', error);
        }
    );
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' ,windowClass: 'custom-class-modal', size: 'lg'})
        .result.then((result) => {
      //console.log(result);
    }, (reason) => {
      //console.log('Err!', reason);
    });
  }
  onclose() {
    this.modalService.dismissAll();
  }

  /**
   * Compares two Date objects and returns e number value that represents
   * the result:
   * 0 if the two dates are equal.
   * 1 if the first date is greater than second.
   * -1 if the first date is less than second.
   * @param date1 First date object to compare.
   * @param date2 Second date object to compare.
   */
  compareDate(date1: Date, date2: Date): number
  {
    // With Date object we can compare dates them using the >, <, <= or >=.
    // The ==, !=, ===, and !== operators require to use date.getTime(),
    // so we need to create a new instance of Date with 'new Date()'
    let d1 = new Date(date1); let d2 = new Date(date2);

    // Check if the dates are equal
    let same = d1.getTime() === d2.getTime();
    if (same) return 0;

    // Check if the first is greater than second
    if (d1 > d2) return 1;

    // Check if the first is less than second
    if (d1 < d2) return -1;
  }

  onSubmit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }


    this.formBasic.value.dateFin = new Date(this.range.value.end);
    this.formBasic.value.dateDebut = new Date(this.range.value.start);
    var now = new Date(this.formBasic.value.dateFin);
    now.setDate(now.getDate() - this.formBasic.value.dateAlert);
    var result = this.compareDate(now, this.formBasic.value.dateDebut);
    //console.log('now',now,'\n this.formBasic.value.dateDebut',this.formBasic.value.dateDebut)
    if (result < 1) {
      this.spinnerservice.hide()
      this.toastr.error('Erreur !!! Veuillez vérifier fréquence d\'alert et réessayer !', 'Erreur!', { progressBar: true });
    } else {
      this.formBasic.value.dateAlert = this.datepipe.transform(new Date(now), 'dd-MM-yyyy');
      this.formBasic.value.dateDebut = this.datepipe.transform(new Date(this.formBasic.value.dateDebut), 'dd-MM-yyyy');
      this.formBasic.value.dateFin = this.datepipe.transform(new Date(this.formBasic.value.dateFin), 'dd-MM-yyyy');

      this.contratService.postContrat(this.formBasic.value).pipe(first()).subscribe((res: any) => {
        if (res.statut === true) {
          this.toastr.success('Service ajouté avec succès !', 'Success!', { progressBar: true });
          this.submitted = false;
          this.listeContrats();
          this.modalService.dismissAll();
          this.formBasic.reset();
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.submitted = true;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
        (error) => {
          this.spinnerservice.hide()
          this.toastr.error('Erreur lors d\'ajout d\'un Service . Veuillez réessayer !', 'Erreur!', { progressBar: true });
        });
    }
  }
  cancelCmp(cbmarq: any) {
    this.spinnerservice.show()
    this.contratService.deleteContrat(cbmarq).subscribe(
      (res: any) => {
        if (res.statut == true) {
          this.spinnerservice.hide()
          this.listeContrats();
          this.toastr.success('Service a été supprimé avec succès !');

        }
        else {
          this.spinnerservice.hide()
          this.toastr.error(res.message)
        }
      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un service',
      text: 'Voulez-vous vraiment supprimer ce service ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.cancelCmp(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  editContrat(doc) {
    this.contratService.getContrat(doc).subscribe((res: any) => {
      this.dialog.open(EditContratComponent, { data: res.data }).afterClosed().subscribe(() => {
        this.listeContrats();
      });
    },
      error => {
        //console.log( 'erreur:' , error);
      }
    );

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    //console.log(this.dataSource, this.dataSource.filteredData);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}

