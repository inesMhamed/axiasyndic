import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import {count, first} from 'rxjs/operators';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { CommuneService } from 'src/app/shared/services/commune.service';
import { ContratService } from 'src/app/shared/services/contrat.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';

@Component({
  selector: 'app-add-contrat',
  templateUrl: './add-contrat.component.html',
  styleUrls: ['./add-contrat.component.scss'],
  providers: [DatePipe, ResidenceService, AppartementService, BlocService]
})
export class AddContratComponent implements OnInit {
  hideThisDiv: boolean = false
  formBasic: FormGroup
  submitted: boolean;
  range: FormGroup;
  residences: any[];
  types: any[];
  loading: boolean;
  periodes: any[];
  cbmarq: any;
  modeEdit: boolean;
  affecters: any = [{ value: 1, label: 'Répartition par bien' }, { value: 0, label: 'Charger syndic' }]
  listAppartements: any = [];
  somme: any = 0;
  listBlocs: any[] = [];
  test: any;
  blocID: any;
  errorAlertContrat: number=0;
  nbrDay: number;
  errorAlertPeriode: number=0;
  pourcentage: string;
  typeProduit: any;
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  usedContrat:boolean=false;

  have_access:boolean =false;


  constructor(private fb: FormBuilder, private toastr: ToastrService, private route: Router,
    private spinnerservice: NgxSpinnerService,
    private actRouter: ActivatedRoute, private router: Router,
    private contratService: ContratService, private residenceService: ResidenceService,
    private communeService: CommuneService, private datepipe: DatePipe,
    private blocService: BlocService, private appartementService: AppartementService, private permissionservice: DroitAccesService
  ) {
  }

  ngOnInit() {
    this.spinnerservice.show()
    this.actRouter.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000034');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000035');

    this.submitted = false;
    this.buildFormBasic();

    this.checkAccessAddEdit();


    this.range = new FormGroup({
      start: new FormControl(null, Validators.required),
      end: new FormControl(null, Validators.required)
    })
    if (this.cbmarq) {
      this.spinnerservice.show()
      this.errorAlertPeriode = 0;
      this.errorAlertContrat = 0;
      this.modeEdit = true;
      this.contratService.getContrat(this.cbmarq).subscribe((contrat: any) => {
        console.log("contrat : ",contrat.data)

        this.usedContrat=contrat.data.used;


        this.range.patchValue({
          start: new Date(contrat.data.dateDebut),
          end: new Date(contrat.data.dateFin)
        })
        this.formBasic.patchValue({
          intitule: contrat.data.intitule,
          residence: contrat.data.residence.cbmarq,
          fournisseur: contrat.data.fournisseur,
          montant: contrat.data.montant,
          type: contrat.data.type.cbmarq,
          periode: contrat.data.priodicite.cbmarq,
          dateDebut: this.datepipe.transform(new Date(contrat.data.dateDebut), 'dd-MM-yyyy'),
          repartition: contrat.data.repartition,
          dateFin: this.datepipe.transform(new Date(contrat.data.dateFin), 'dd-MM-yyyy'),
          dateAlert: contrat.data.alertContrat,
          dateAlertPeriode: contrat.data.alertPeriode,
          bloc: contrat?.data?.bloc?.cbmarq
        });


        this.onchangeResidence({type: contrat?.data?.residence?.type?.label },false);
        this.blocService.getBlocs(this.formBasic.value.residence).subscribe((bloc: any) => {
          this.listBlocs = bloc?.data?.map(el => ({ value: el.cbmarq, label: el.intitule }))
          this.listBlocs?.unshift({ value: 0, label: 'Tous' });
          this.spinnerservice.hide()
        });
        Object.keys(contrat.data).forEach(name => {
          if (name === 'lignes') {
            this.test = contrat.data[name];

            Object.keys(this.test).forEach(n => {
              this.addLigne();
              var lig = this.test[n];
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.appartement?.cbmarq);
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.appartement?.intitule);
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue(lig?.pourcentage);
              ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.appartement?.nature);

              // this.appartementService.getAppartement(lig?.appartement?.cbmarq).subscribe(( eny: any) => {
              //   // this.formBasic.patchValue({ bloc: eny?.data?.bloc?.cbmarq });
              //   ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(eny?.data?.nature?.label);
              // });
            });
          }
        });


        /*--Add Lignes non selected------------------------------------------------------*/
        let length = this.formBasic.value.lignes.length ;
        if ( contrat?.data?.bloc === null) {
          this.formBasic.patchValue({ bloc: 0 });
          this.appartementService.getAppartementResidenceprop(this.formBasic.value.residence).subscribe((type: any) => {
            if (!type.data) {
              this.toastr.error('Ce produit ne contient pas des biens occupés !', 'Erreur!', { progressBar: true });
            } else {
              this.listAppartements = type.data;
              Object.keys(this.listAppartements).forEach(n => {
                var lig : any = this.listAppartements[n];
                let result = this.formBasic.value.lignes.findIndex(itm => itm.appartement === lig?.cbmarq);
                if (result === -1) {
                  this.addLigne();
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('selected').patchValue(false);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('intitule').patchValue(lig?.intitule);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('pourcentage').patchValue(0);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
                  length++;
                }

              });
            }
          });
        } else {
          this.listAppartements = []
          this.appartementService.getAppartementByBlocHaveProp(contrat?.data?.bloc?.cbmarq).subscribe((appart: any) => {
            if (!appart.data) {
              this.toastr.error('Ce bloc ne contient pas des appartements !', 'Erreur!', { progressBar: true });
            } else {
              this.listAppartements = appart.data
              Object.keys(this.listAppartements).forEach(n => {
                var lig: any = this.listAppartements[n];
                 console.log('result', lig , this.formBasic.value.lignes );
                let result = this.formBasic.value.lignes.findIndex(itm => itm.appartement === lig?.cbmarq);
                if ( result === -1 ) {
                  this.addLigne();
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('selected').patchValue(false);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('intitule').patchValue(lig?.intitule);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('pourcentage').patchValue(0);
                  ((this.formBasic.get('lignes') as FormArray).at(parseInt(length)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
                  length++;
                }

                // ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
                // ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.intitule);
                // ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
                // ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));
              });
            }

          });
        }
        this.spinnerservice.hide()
      });
    } else {
      this.spinnerservice.hide()
      this.modeEdit = false;
    }
    // document.getElementById('details').style.display='none'

  }

  checkAccessAddEdit(){
    if(!this.cbmarq){
      this.checkAccess("FN21000034");
    }else{
      this.checkAccess("FN21000035");
    }
  }
  
checkAccess(code){
  //if access false ==> go out to 403
  this.permissionservice.getAccessUser( this.id_current_user, code).subscribe((res:any)=>{

   this.have_access =res.data;
  
 },error=>{},()=>{

   if(!this.have_access){
     this.router.navigate(['403']);
   }else{
     
    this.listeresidence();
    this.listetypes();
    this.listePeriodes('PRD');
   }
  
 });
}

  buildFormBasic() {
    this.formBasic = this.fb.group({
      intitule: new FormControl(null, Validators.required),
      residence: new FormControl(null, Validators.required),
      bloc: new FormControl(null,),
      fournisseur: new FormControl(null,Validators.required),
      montant:  [null, Validators.compose([ Validators.required, Validators.min(0)])],
      type: new FormControl(null, Validators.required),
      periode: new FormControl(null, Validators.required),
      dateDebut: new FormControl(null,),
      repartition: new FormControl(null, Validators.required),
      dateFin: new FormControl(null,),
      dateAlert: [0, Validators.compose([ Validators.required, Validators.min(0)])], //new FormControl(0, Validators.min(0)),
      dateAlertPeriode: [0, Validators.compose([ Validators.required, Validators.min(0)])], //new FormControl(0, Validators.required),
      lignes: this.fb.array([]),
    });
  }
  testDateContrat(event) {
    //console.log('event', event)
    this.formBasic.value.dateFin = new Date(this.range.value.end);
    this.formBasic.value.dateDebut = new Date(this.range.value.start);
    //console.log("dte fin", this.formBasic.value.dateFin, this.formBasic.value.dateDebut)
    //console.log("dte  debut", this.formBasic.value.dateFin, this.formBasic.value.dateDebut)
    var now = new Date(this.formBasic.value.dateFin);
    //console.log("now", now)
    now.setDate(now.getDate() - event);
    //console.log("now", now)
    var result = this.compareDate(now, this.formBasic.value.dateDebut);
    //console.log("result", result)
    if (result < 1) {
      this.errorAlertContrat = -1
      this.toastr.error('Veuillez vérifier la fréquence d\'alert et réessayer !', 'Erreur!', { progressBar: true });
    } else {
      this.errorAlertContrat = 0
      this.formBasic.patchValue({
        dateDebut: this.datepipe.transform(new Date(this.formBasic.value.dateDebut), 'dd-MM-yyyy'),
        dateFin: this.datepipe.transform(new Date(this.formBasic.value.dateFin), 'dd-MM-yyyy')
      });
      //this.formBasic.value.dateFin = this.datepipe.transform(new Date(this.formBasic.value.dateFin), 'dd-MM-yyyy');
      //console.log(this.formBasic.value.dateDebut, this.formBasic.value.dateFin)
    }
  }
  periodParjour(event) {
    this.nbrDay = parseInt(event.description)
  }
  testDate(event) {
    if (event > this.nbrDay) {
      this.errorAlertPeriode = -1
      this.toastr.error('Date d\'alerte est supérieure à la périodicité du payement, veuillez réessayer !', 'Erreur!', { progressBar: true });

    }
    else {
      this.errorAlertPeriode = 0
    }
  }



    startChange(event)
    {
    ////console.log("startChange",event.value);
    this.formBasic.patchValue({
      dateDebut: this.datepipe.transform(new Date(event.value), 'dd-MM-yyyy'),
      //dateFin: this.datepipe.transform(new Date(contrat.data.dateFin), 'dd-MM-yyyy'),

    })

    ////console.log("this.formBasic debut: ",this.formBasic.value)

    }
    endChange(event)
    {
    ////console.log("endChange",event.value)

    this.formBasic.patchValue({
      dateFin: this.datepipe.transform(new Date(event.value), 'dd-MM-yyyy'),

    })

    ////console.log("this.formBasic: fin ",this.formBasic.value)

    }



  onSubmit() {
    this.spinnerservice.show()
    ////console.log('1 onSubmit', this.formBasic.value)
    this.somme = 0
    this.loading = true;

    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      this.toastr.error('Veuillez vérifier votre saisie !', 'Erreur!', { progressBar: true });
      this.submitted = true;
      return;
    }

    if (this.formBasic.value.repartition == 1) {
      this.formBasic.getRawValue().lignes.forEach(i => {
        if (i.selected === true) {
          this.somme = this.somme + parseFloat(i.pourcentage);
        }
        let removeIndex = this.formBasic.value.lignes.findIndex(itm => itm.selected === false);
        if (removeIndex !== -1)
          this.formBasic.value.lignes.splice(removeIndex, 1);
      });
      this.spinnerservice.hide()
    }

    ////console.log('3 onSubmit', this.formBasic.value)

    ////console.log('3   errorAlertPeriode', this.errorAlertPeriode, '\n errorAlertContrat,', this.errorAlertContrat)

    if (this.errorAlertPeriode == 0 && this.errorAlertContrat == 0) {

      // delete this.formBasic.value.bloc
      this.formBasic.value.lignes.forEach(i => {
        delete i.selected
        delete i.intitule
        delete i.nature
      });
      //**** AJOUT  ****/
      if (!this.modeEdit) {
        if ( this.can_add?.permission ) {
          if (this.formBasic.value.lignes !== 0 && this.formBasic.value.repartition == 1) {
            if (this.somme > 100) {
              this.spinnerservice.hide()
              this.toastr.error('la somme de pourcentage de payement est supérieure à 100% !', 'Erreur!', { progressBar: true });
            } else if (this.somme < 100) {
              this.spinnerservice.hide()
              this.toastr.error('la somme de pourcentage de payement est inférieure à 100% !', 'Erreur!', { progressBar: true });
            } else {

              //console.log("Ajouuuuutttt  post contrat: ",this.formBasic.value)
              this.contratService.postContrat(this.formBasic.value).pipe(first()).subscribe((res: any) => {
                if (res.statut === true) {
                  this.toastr.success('Contrat est ajouté avec succès !', 'Success!', { progressBar: true });
                  this.submitted = false;
                  this.formBasic.reset();
                  this.hideThisDiv = false
                  this.range.reset();
                  //console.log("inn sucess port copntrat redirect == vers ==> contrat  && repartition=1")
                  this.router.navigate(['/syndic/contrats'])
                  this.spinnerservice.hide()
                } else {
                  this.spinnerservice.hide()
                  this.submitted = true;
                  this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                }
              },
                (error) => {
                  this.spinnerservice.hide()
                  this.toastr.error('Erreur lors de l\'ajout d\'un contrat . Veuillez réessayer !', 'Erreur!', { progressBar: true });
                });
            }
          }

          else if(this.formBasic.value.repartition == 0) {

            this.contratService.postContrat(this.formBasic.value).pipe(first()).subscribe((res: any) => {
              if (res.statut === true) {
                this.toastr.success('Contrat est ajouté avec succès !', 'Success!', { progressBar: true });
                this.submitted = false;
                this.formBasic.reset();
                this.hideThisDiv = false
                this.range.reset();
                //console.log("inn sucess port copntrat redirect == vers ==> contrat  && repartition=0")
                this.spinnerservice.hide()
                this.router.navigate(['/syndic/contrats'])

              } else {
                this.spinnerservice.hide()
                this.submitted = true;
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
              }
            },
              (error) => {
                this.spinnerservice.hide()
                this.toastr.error('Erreur lors de l\'ajout d\'un contrat . Veuillez réessayer !', 'Erreur!', { progressBar: true });
              });
          }
          else {
            this.spinnerservice.hide()
            this.toastr.error('Veuillez vérifier votre saisie !', 'Erreur!', { progressBar: true });
          }
        } else {
          this.spinnerservice.hide()
          this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
        }

      }
      /**** UPDATE ****/
      else {
        if (this.can_edit?.permission) {
          if (this.formBasic.value.lignes.length !== 0 && this.formBasic.value.repartition == 1) {
            this.spinnerservice.hide()
            if (this.somme > 100) {
              this.toastr.error('la somme de pourcentage de payement est supérieure à 100% !', 'Erreur!', { progressBar: true });
            } else if (this.somme < 100) {
              this.toastr.error('la somme de pourcentage de payement est inférieure à 100% !', 'Erreur!', { progressBar: true });
            } else {
              this.contratService.putContrat(this.formBasic.value, this.cbmarq).pipe(first()).subscribe((res: any) => {
                if (res.statut === true) {
                  this.toastr.success('Service a été modifié avec succès !', 'Success!', { progressBar: true });
                  //window.location.reload()
                  this.submitted = false;
                  this.formBasic.reset();
                  this.hideThisDiv = false
                  this.range.reset();
                  this.router.navigate(['/syndic/contrats'])
                } else {
                  this.submitted = true;
                  this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                }
              },
                (error) => {
                  this.toastr.error('Erreur lors de la modification d\'un service . Veuillez réessayer !', 'Erreur!', { progressBar: true });
                });

            }
          }
          else if (this.formBasic.value.repartition == 0) {
            this.spinnerservice.hide()
            ////console.log('cbmarq', this.cbmarq, this.modeEdit, '\n Form edit :', this.formBasic.value)
            this.contratService.putContrat(this.formBasic.value, this.cbmarq).pipe(first()).subscribe((res: any) => {
              if (res.statut === true) {
                this.toastr.success('Service a été modifié avec succès !', 'Success!', { progressBar: true });
                //window.location.reload()
                this.submitted = false;
                this.router.navigate(['/syndic/contrats'])
              } else {
                this.submitted = true;
                this.toastr.error(res.message, 'Erreur!', { progressBar: true });
              }
            },
              (error) => {
                this.spinnerservice.hide()
                this.toastr.error('Erreur lors de la modification d\'un service . Veuillez réessayer !', 'Erreur!', { progressBar: true });
              });
          }
          else {
            this.spinnerservice.hide()
            this.toastr.error('Veuillez vérifier votre saisie !', 'Erreur!', { progressBar: true });
          }
        } else {
          this.spinnerservice.hide()
          this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
        }

      }
    }

  }
  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      this.residences = res?.data?.map(clt => ({ value: clt?.cbmarq, label: clt?.intitule, type: clt?.type?.label }));
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log('erreur: ', error);
      }
    );
  }
  listetypes() {
    this.communeService.gettype('CNT').subscribe((res: any) => {
          // this.types = res.data;
          this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
        },
        error => {
          //console.log('erreur: ', error);
        }
    );
  }

  /**
   * Compares two Date objects and returns e number value that represents
   * the result:
   * 0 if the two dates are equal.
   * 1 if the first date is greater than second.
   * -1 if the first date is less than second.
   * @param date1 First date object to compare.
   * @param date2 Second date object to compare.
   */
  compareDate(date1: Date, date2: Date): number {
    // With Date object we can compare dates them using the >, <, <= or >=.
    // The ==, !=, ===, and !== operators require to use date.getTime(),
    // so we need to create a new instance of Date with 'new Date()'
    let d1 = new Date(date1); let d2 = new Date(date2);

    // Check if the dates are equal
    let same = d1.getTime() === d2.getTime();
    if (same) return 0;

    // Check if the first is greater than second
    if (d1 > d2) return 1;

    // Check if the first is less than second
    if (d1 < d2) return -1;
  }
  listePeriodes(type) {
    this.contratService.gettypesContrat(type).subscribe((type: any) => {
      //console.log('periode', type.data)
      this.periodes = type.data
    })
  }
  onchangeResidence(event, fromFront) {
    this.typeProduit = event.type
    if (this.typeProduit === 'Terrain') {
      this.onchange({ value: 0, label: 'Tous' });
    } else {
       //this.lignes().clear()
       /** vider la liste affecté à **/
       if (fromFront) {
        this.formBasic.patchValue({
          repartition: null
        });
       }

      this.blocService.getBlocs(event.value).subscribe((bloc: any) => {
        this.listBlocs = bloc?.data?.map(el => ({ value: el.cbmarq, label: el.intitule }))
        this.listBlocs?.unshift({ value: 0, label: 'Tous' });
      });
    }

  }
  onchangeAffectation(event) {
    if (event.value === 0) {
      this.hideThisDiv = false
      // this.formBasic.controls['lignes'].clearValidators;
      // this.formBasic.controls['lignes'].setValidators([]);
      // this.formBasic.controls['lignes'].updateValueAndValidity();

      this.formBasic.value.bloc = null;
      this.formBasic.value.lignes = [];

    } else {
      this.hideThisDiv = true
      this.onchange({ value: 0, label: 'Tous' });
      this.formBasic.patchValue({ bloc: 0 });
      // this.formBasic.controls['lignes'].setValidators([Validators.required]);
      // this.formBasic.controls['lignes'].updateValueAndValidity();
    }
  }
  onchange(event) {
    this.spinnerservice.show()
    this.lignes().clear()
    console.log('tous', event , this.formBasic.value );
    this.listAppartements = [];
    if (event.value === 0) {
      this.appartementService.getAppartementResidenceprop(this.formBasic.value.residence).subscribe((type: any) => {
        if (!type.data) {
          this.spinnerservice.hide()
          this.toastr.error('Ce produit ne contient pas des biens !', 'Erreur!', { progressBar: true });
        } else {
          this.spinnerservice.hide()
          this.listAppartements = type.data;
          Object.keys(this.listAppartements).forEach(n => {
            this.addLigne();
            var lig: any = this.listAppartements[n];
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.intitule);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
          });
        }
      });
    } else {
      this.listAppartements = []
      this.lignes().clear()
      this.appartementService.getAppartementByBlocHaveProp(event.value).subscribe((appart: any) => {
        if (!appart.data) {
          this.spinnerservice.hide()
          this.toastr.error('Ce bloc ne contient pas des appartements !', 'Erreur!', { progressBar: true });
        } else {
          this.spinnerservice.hide()
          this.listAppartements = appart.data
          Object.keys(this.listAppartements).forEach(n => {
            this.addLigne();
            var lig: any = this.listAppartements[n];
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('appartement').patchValue(<number>lig?.cbmarq);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig?.intitule);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('nature').patchValue(lig?.nature?.label);
            ((this.formBasic.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('pourcentage').patchValue((100 / this.listAppartements.length).toFixed(1));
          });
        }

      });
    }
  }
  newligne(): FormGroup {
    return this.fb.group({
      selected: new FormControl(true, ),
      nature: new FormControl(null, Validators.required),
      appartement: new FormControl(null, Validators.required),
      intitule: new FormControl('', Validators.required),
      pourcentage: new FormControl('', Validators.required),
    });
  }
  lignes(): FormArray {
    return this.formBasic.get('lignes') as FormArray;
  }
  addLigne() {
    this.lignes().push(this.newligne());
  }
  removeLigne(ligIndex: number) {
    this.lignes().removeAt(ligIndex);
  }
  nobre() {

  }
  checkCheckBoxvalue(event, index) {
    let nbSelect: any[] = this.formBasic.value.lignes.filter(el => (el.selected == true))
    this.formBasic.getRawValue().lignes.forEach((i, index2) => {
      this.pourcentage = (100 / nbSelect.length).toFixed(1);
      if (i.selected == true) {
        i.pourcentage = this.pourcentage;
        ((this.formBasic.get('lignes') as FormArray).at((index2)) as FormGroup).get('pourcentage').patchValue(this.pourcentage);
      }
    });
    if (event.target.checked) {
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue(this.pourcentage);
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('nature').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').enable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).enable();
    } else {
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').patchValue('0');
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('appartement').disable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('nature').disable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('pourcentage').disable();
      ((this.formBasic.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('intitule').disable();
    }

  }
  gotoListe() {
    this.router.navigate(['/syndic/contrats/']);
  }
}
