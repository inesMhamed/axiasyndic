import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { Compteur } from 'src/app/shared/models/compteur.model';
import { ContratService } from 'src/app/shared/services/contrat.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  providers: [DatePipe]
})
export class AlertComponent implements OnInit {
  public listAlert: any=[];
  displayedColumns: string[] = ['date' , 'intitule',  'statut' ];
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  constructor(private contratService:ContratService, private datepipe:DatePipe,private spinnerservice: NgxSpinnerService,) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.contratService.getAlertContrats().subscribe((alert:any)=>{

      this.dataSource.data=alert.data
      //console.log('list',alert.data)
      this.listAlert=alert.data.map(el=>({cbmarq:el.cbmarq,intitule:el.intitule,statut:el.statut, date: this.datepipe.transform(el?.dateAlert?.date, 'dd-MM-yyyy')}))
      this.spinnerservice.hide()
    })
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    //console.log(this.dataSource, this.dataSource.filteredData);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
