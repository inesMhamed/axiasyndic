import { Component, Inject, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ContratService } from '../../../../shared/services/contrat.service';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { ToastrService } from 'ngx-toastr';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import { CommuneService } from '../../../../shared/services/commune.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-contrat',
  templateUrl: './edit-contrat.component.html',
  styleUrls: ['./edit-contrat.component.scss'],
  providers: [DatePipe]
})
export class EditContratComponent implements OnInit {
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  residences: any[];
  types: any[];
  data: any;
  range: any;
  periodes: any[];

  constructor(
    private contratService: ContratService,
    private communeService: CommuneService,
    private dl: ResidenceService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spinnerservice: NgxSpinnerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private datepipe: DatePipe,
    private dialogRef: MatDialogRef<EditContratComponent>, @Inject(MAT_DIALOG_DATA) public contrat: any
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.submitted = false;
    this.buildFormBasic();
    this.listeresidence();
    this.listetypes();
    this.listePeriodes('PRD')

    this.range = new FormGroup({
      start: new FormControl(null || new Date(this.contrat.dateDebut), Validators.required ),
      end: new FormControl(null || new Date(this.contrat.dateFin), Validators.required )
    });
  }
  buildFormBasic() {
    this.formBasic = this.fb.group({
      intitule: new FormControl(null ||this.contrat.intitule, Validators.required),
      residence: new FormControl( null ||this.contrat.residence.cbmarq, Validators.required),
      fournisseur: new FormControl( null ||this.contrat.fournisseur, ),
      montant: new FormControl( null ||this.contrat.montant,  Validators.min(0)),
      type: new FormControl(null ||this.contrat.type.cbmarq, Validators.required ),
      dateDebut: new FormControl(null, ),
      periode:new FormControl(null, Validators.required ),

      dateFin: new FormControl(null,  ),
      dateAlert: new FormControl(0 || this.datediff(this.contrat.dateAlert, this.contrat.dateFin),  Validators.min(0)),
    });
  }
  listePeriodes(type){
    this.contratService.gettypesContrat(type).subscribe((type:any)=>{
      this.periodes=type.data
    })
  }
  listeresidence() {
    this.spinnerservice.show()
    this.dl.getResidences().subscribe((res: any) => {
      this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }
  listetypes() {
    this.communeService.gettype('CNT').subscribe((res: any) => {
          this.types = res.data.map(clt => ({ value: clt.cbmarq, label: clt.label }));
        },
        error => {
          //console.log( 'erreur: ', error);
        }
    );
  }
  onclose() {
    this.dialogRef.close();
  }
  datediff( first , second) {
    const date1 = new Date(first);
    const date2 = new Date(second);
    const Time = date2.getTime() - date1.getTime();
    const Days = Time / (1000 * 3600 * 24);
    return Days;
  }

  /**
   * Compares two Date objects and returns e number value that represents
   * the result:
   * 0 if the two dates are equal.
   * 1 if the first date is greater than second.
   * -1 if the first date is less than second.
   * @param date1 First date object to compare.
   * @param date2 Second date object to compare.
   */
  compareDate(date1: Date, date2: Date): number
  {
    // With Date object we can compare dates them using the >, <, <= or >=.
    // The ==, !=, ===, and !== operators require to use date.getTime(),
    // so we need to create a new instance of Date with 'new Date()'
    let d1 = new Date(date1); let d2 = new Date(date2);

    // Check if the dates are equal
    let same = d1.getTime() === d2.getTime();
    if (same) return 0;

    // Check if the first is greater than second
    if (d1 > d2) return 1;

    // Check if the first is less than second
    if (d1 < d2) return -1;
  }
  onClose() {
    this.dialogRef.close();
  }
  onSubmit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    this.formBasic.value.dateFin = new Date(this.range.value.end);
    this.formBasic.value.dateDebut = new Date(this.range.value.start);
    var now = new Date(this.formBasic.value.dateFin);
    now.setDate(now.getDate() - this.formBasic.value.dateAlert);
    var result = this.compareDate(now , this.formBasic.value.dateDebut);

    if (result < 1) {
      this.spinnerservice.hide()
      this.toastr.error('Erreur !!! Veuillez vérifier fréquence d\'alert et réessayer !', 'Erreur!', { progressBar: true });
    } else {

      this.formBasic.value.dateAlert = this.datepipe.transform(new Date(now), 'dd-MM-yyyy');
      this.formBasic.value.dateDebut = this.datepipe.transform(new Date(this.formBasic.value.dateDebut), 'dd-MM-yyyy');
      this.formBasic.value.dateFin = this.datepipe.transform(new Date(this.formBasic.value.dateFin), 'dd-MM-yyyy');

      this.contratService.putContrat(this.formBasic.value, this.contrat.cbmarq).pipe(first()).subscribe((res: any) => {
        if (res.statut === true) {
          this.spinnerservice.hide()
          this.toastr.success('Contrat a été modifié avec succès !', 'Success!', { progressBar: true });
          this.submitted = false;
          this.onClose();
          // window.location.href = '/syndic/contrats' ;
        } else {
          this.spinnerservice.hide()
          this.submitted = true;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
        (error) => {
          this.spinnerservice.hide()
          this.toastr.error('Erreur lors de la modification un Contrat . Veuillez réessayer !', 'Erreur!', { progressBar: true });
        });
    }
  }


}
