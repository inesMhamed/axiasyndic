import { Component, OnInit } from '@angular/core';
import { ConfigNotificationService } from 'src/app/shared/services/config-notification.service';
import { ToastrService } from 'ngx-toastr';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


class NotifAddModel{
  ordre:number;
  sms:number;
  email:number;
  messagerie:number;

  constructor(){

  }
}
@Component({
  selector: 'app-config-notification',
  templateUrl: './config-notification.component.html',
  styleUrls: ['./config-notification.component.scss']
})
export class ConfigNotificationComponent implements OnInit {

  listConfigNotif:any[]=[];
  listNotifIdentifiant:any[]=[];
  listNotifReclamation:any[]=[];
  listNotifPublication:any[]=[];
  listNotifReunion:any[]=[];
  listNotifIntervention:any[]=[];
  listNotifPaiement:any[]=[];

  
  id_current_user:any;
  have_access:boolean =false;

  constructor(private configNotificationService:ConfigNotificationService,private spinnerservice: NgxSpinnerService,private toastr: ToastrService,private router: Router,private permissionservice: DroitAccesService) {

    this.id_current_user = localStorage.getItem('id');

  }

  ngOnInit() {
    this.spinnerservice.show()
     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000125').subscribe((res:any)=>{

      this.have_access =res.data;
      this.spinnerservice.hide()
    },error=>{},()=>{
      this.spinnerservice.hide()
      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getConfigNotif();
      }
     
    });

   
  }

  getConfigNotif(){
    this.spinnerservice.show()
    this.configNotificationService.getConfigNotif().subscribe((res:any)=>{


      if(res.statut){
        ////console.log("res configNotif: ",res.data);
        this.listConfigNotif=res.data;    
        this.spinnerservice.hide()
      }
    })
  }

  submitConfigNotif(){
    this.spinnerservice.show()
    
    let listConfigModel:any[]=[];

    this.listConfigNotif.forEach(item=>{
      let modelNotif:NotifAddModel=new NotifAddModel();
      modelNotif.ordre=item.ordre
      modelNotif.sms=item.sms?1:0;
      modelNotif.email=item.email?1:0;
      modelNotif.messagerie=item.messagerie?1:0;
      listConfigModel.push(modelNotif);

    })

    ////console.log("submit config notif...",listConfigModel);


    this.configNotificationService.postConfigNotif({'notification':listConfigModel}).subscribe((res:any)=>{
      if(res.statut){
        this.toastr.success("Configuration de notification a été enregistrée avec succès !", 'Success!', { progressBar: true })
        this.getConfigNotif();
        this.spinnerservice.hide()
      }else{
        this.spinnerservice.hide()
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });

      }
    })



  }

  
}
