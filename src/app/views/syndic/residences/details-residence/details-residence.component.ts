import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import Swal from 'sweetalert2';
import { AddBlocComponent } from '../../bloc/add-bloc/add-bloc.component';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DelegateService } from 'src/app/shared/services/delegate.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Base62EncoderService } from 'src/app/shared/services/base-62-encoder.service';

@Component({
  selector: 'app-details-residence',
  templateUrl: './details-residence.component.html',
  styleUrls: ['./details-residence.component.scss'],
  animations: [SharedAnimations],
  providers: [Base62EncoderService]

})
export class DetailsResidenceComponent implements OnInit {

  residence: any = [];
  viewMode: 'list' | 'grid' = 'list';
  allSelected: boolean;
  page = 1;
  pageSize = 5;
  blocs: any = [];
  primary = 'primary';
  residences: any = [];
  message: any;
  selectedValue: any;
  idd: string;
  can_delete: Permission;
  can_active: Permission;
  delegateForm: FormGroup
  id_current_user: any;
  listeDeligues: any;
  dataSourceListeDeligue= new  MatTableDataSource<any>();
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['Année','nomPrenom', 'username', 'tel', 'email']; 


  dataSourceListeLogDeligue= new  MatTableDataSource<any>();
  @ViewChild('paginatorLog') paginatorLog: MatPaginator;
  @ViewChild(MatSort) sortLog: MatSort;
  displayedColumnsLog: string[] = ['Année','nomPrenom', 'username', 'tel', 'email']; 

  submitted: boolean;
  listeprops: any = [];
  delegates: any = []
  logDeligues: boolean = false;
  listFutureDeligues: any = [];
  listeLogDeligues: any = [];
  usersInternes: any = [];

  have_access:boolean =false;

  constructor(private residenceServiice: ResidenceService, private appartementService: AppartementService,
    public spinnerservice: NgxSpinnerService, public encodeService: Base62EncoderService,
    private router: Router, private proprietaireService: ProprietaireService, private fb: FormBuilder,
    private delegateService: DelegateService, private utilisateurService: UtilisateurService,
    private actRoute: ActivatedRoute, private toastr: ToastrService, private permissionservice: DroitAccesService
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.actRoute.paramMap.subscribe(params => {
      console.log('id=======',  encodeURIComponent(params.get('cbmarq')))
      encodeURIComponent(params.get('cbmarq'))
      var idcode = this.encodeService.decode(params.get('cbmarq'))
      console.log('code=======', idcode)
      this.idd = (idcode).toString()
    });
    this.id_current_user = localStorage.getItem('id');
    this.can_delete = this.permissionservice.search(this.id_current_user, 'FN21000011');
    this.can_active = this.permissionservice.search(this.id_current_user, 'FN21000010');

    this.checkAccess();
    
    if (this.idd) {
      this.getResidence(this.idd);
      this.getListeProprietaires()
    }
    this.delegateForm = this.fb.group({
      delegate: new FormControl(null, Validators.required),
    })
  }

  checkAccess(){
     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000006').subscribe((res:any)=>{

      this.have_access = res.data;
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.spinnerservice.hide()
      if (!this.have_access) {
        this.router.navigate(['403']);
      }

    });
  }

  onTabChange(event:NgbTabChangeEvent){
    ////console.log("eventtabchange: ",event)
    if(event.nextId=="idTabDélégués"){
      this.getlisteDeligues();
    }
   
  }

  getResidence(id) {
    this.spinnerservice.show()
    this.residenceServiice.getResidence(id).subscribe((res: any) => {
      this.residence = res.data;
      this.spinnerservice.hide()
    });
  }
  getAppartementsResidence(id) {
    this.spinnerservice.show()
    this.appartementService.getAppartementResidence(id).subscribe((res: any) => {
      this.spinnerservice.hide()
    })
  }

  cancelCmp(cbmarq: any) {
    this.spinnerservice.show()
    this.residenceServiice.deleteResidence(cbmarq).subscribe(
      (res: any) => {
        this.spinnerservice.hide()
        if (res.statut === true) {
          this.toastr.success('Produit  a été supprimée avec succès !');
          this.getResidence(this.idd);
          this.router.navigate((['/syndic/residences']));
        } else {
          this.toastr.error(res.message);
        }
      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un produit',
      text: 'Voulez-vous vraiment supprimer ce produit ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.cancelCmp(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  getListeProprietaires() {
    this.spinnerservice.show()
    this.proprietaireService.getProprietairesResidence(this.idd).subscribe((prop: any) => {
      this.spinnerservice.hide()
      if (prop.statut) {
        this.listeprops = prop?.data?.map(el => ({ id: el.id, nomComplet: el.nomComplet + ' | Proprietaire', email: el.email, username: el.username, identifiant: el.identifiant, telephone: el.telephone1 }))
      }
    },error => {}, () => {
      this.utilisateurService.getAllUtilisateurs().subscribe((prop: any) => {
        if(prop.statut){
          this.usersInternes = prop?.data?.map(el => ({ id: el.id, nomComplet: el.nomComplet + ' | Utilisateur Interne', email: el.email, username: el.username, identifiant: el.identifiant, telephone: el.telephone1 }))
          ////console.log('usersInternes', this.usersInternes)
          this.listFutureDeligues = this.listeprops?.concat(this.usersInternes)
          const test = [...this.listeprops, ...this.usersInternes]
          ////console.log('test', test)
        }

      })
    } )
  }
  getlisteDeligues() {
    this.spinnerservice.show()
    this.logDeligues = false;
    this.dataSourceListeDeligue=new  MatTableDataSource<any>(); 
    this.listeDeligues=[];
    this.delegateService.getDelegates(this.idd).subscribe((res: any) => {

      if (res.statut) {
        this.listeDeligues = res.data;
        this.dataSourceListeDeligue.data=res.data;
        this.dataSourceListeDeligue.paginator = this.paginator;
        this.dataSourceListeDeligue.sort = this.sort;
        this.spinnerservice.hide()
        // //console.log('listeDeligues',  this.dataSourceListeDeligue.data)
        ////console.log('listeDeligues size',  this.dataSourceListeDeligue.data.length)
      }else {
        this.spinnerservice.hide()
      }
    },
        error => {
          this.spinnerservice.hide()
        });
  }
  historiqueDeligue() {
    this.spinnerservice.hide()
    this.logDeligues = true;
    this.listeLogDeligues = [];
    this.dataSourceListeDeligue = new MatTableDataSource<any>();
    this.delegateService.getLogDelegates(this.idd).subscribe((res: any) => {
      if (res.statut) {
        this.listeLogDeligues = res.data
        ////console.log('listeLogDeligues', this.listeLogDeligues)

        this.dataSourceListeLogDeligue.data = res.data;
        this.dataSourceListeLogDeligue.paginator = this.paginator;
        this.dataSourceListeLogDeligue.sort = this.sort;
        this.spinnerservice.hide()
      }

    })
  }
  synchroneDeligue(produit) {
    Swal.fire({
      title: 'Synchroniser ancien délégué',
      text: 'Voulez-vous vraiment Synchroniser des anciens délégués ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinnerservice.show()
        this.delegateService.synchronazeDelegate(produit).subscribe((delegates: any) => {
          this.listeDeligues = delegates.data
          if (delegates.statut == true) {
              this.spinnerservice.hide();
            this.toastr.success("Délégués synchronisés avec succès !", 'Succès!', { progressBar: true });
            this.getlisteDeligues()
            this.spinnerservice.hide();
          }
          else {
            this.spinnerservice.hide();
            this.toastr.error(delegates.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.spinnerservice.hide();
      }
    })
  }
  submitdelegate() {
    this.spinnerservice.show()
    if (this.delegateForm.invalid) {
      this.spinnerservice.hide()
      ////console.log('delegateForm', this.delegateForm.controls)
      this.submitted = true;
      return;
    }
    ////console.log('submit', this.delegateForm.value)
    this.delegateService.addDelegate(this.delegateForm.value.delegate, this.idd).subscribe((delegates: any) => {
      if (delegates.statut == true) {
        this.spinnerservice.hide()
        this.toastr.success("Délégué a été ajouté au produit avec succès !", 'Succès!', { progressBar: true });
        //console.log(this.delegates.data)
        this.getlisteDeligues();
        this.ngOnInit()
      }
      else {
        this.spinnerservice.hide()
        this.toastr.error(delegates.message, 'Erreur!', { progressBar: true });
      }
    })
  }
  change(event) {
    this.delegateForm.value.delegate = event.id
  }
  retirerDelegate(user, produit) {
    Swal.fire({
      title: 'Retirer un délégué',
      text: 'Voulez-vous vraiment retirer ce délégué ?',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinnerservice.show()
        this.delegateService.removeDelegate(user, produit).subscribe((delegates: any) => {
          this.listeDeligues = delegates.data
          if (delegates.statut == true) {
            this.spinnerservice.hide()
            this.toastr.success("Délégué a été retiré avec succès !", 'Succès!', { progressBar: true });
            this.getlisteDeligues()
          }
          else {
            this.spinnerservice.hide()
            this.toastr.error(delegates.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  cancelCmpAct(cbmarq: any, active: any, title: any) {

    this.residenceServiice.desactiverResidences(cbmarq, active).subscribe(
      res => {
        this.toastr.success('Produit  ' + title + ' avec succès !');
        this.getResidence(this.idd);
        this.spinnerservice.hide()
      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBoxAct(cbmarq: any, active: any) {
    var title;
    if (active === false) {
      title = 'Désactiver';
    } else {
      title = 'Activer';
    }
    Swal.fire({
      title: title + ' un produit',
      text: 'Voulez-vous vraiment ' + title + ' ce produit ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.spinnerservice.show()
        ////console.log('active', active);
        this.cancelCmpAct(cbmarq, active, title);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
}
