import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenFraisSyndicComponent } from './gen-frais-syndic.component';

describe('GenFraisSyndicComponent', () => {
  let component: GenFraisSyndicComponent;
  let fixture: ComponentFixture<GenFraisSyndicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenFraisSyndicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenFraisSyndicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
