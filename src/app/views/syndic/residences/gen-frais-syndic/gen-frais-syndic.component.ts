import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';

@Component({
  selector: 'app-gen-frais-syndic',
  templateUrl: './gen-frais-syndic.component.html',
  styleUrls: ['./gen-frais-syndic.component.scss']
})
export class GenFraisSyndicComponent implements OnInit {

  formBasic:FormGroup;
  submitted:boolean=false;
  residences:any[]=[];
  annees:any[]=[];
  infoPref: any;
  smallwidth: any = 1001;
  check: boolean = false;
  months = [{ value: 0, label: '0' }, { value: 1, label: '1' }, { value: 2, label: '2' }, { value: 3, label: '3' }, { value: 4, label: '4' }, { value: 5, label: '5' }, { value: 6, label: '6' }, { value: 7, label: '7' }, { value: 8, label: '8' }, { value: 9, label: '9' }, { value: 10, label: '10' }, { value: 11, label: '11' }, { value: 12, label: '12' }
];

//
tranche: any;
arrayTranche: any = []
annee: number;
mois: number;
fraisannuelle: any;
jour: number;
frais: number;
somme: any;
zero: number

have_access:boolean =false;
id_current_user: any;



  constructor(private fb:FormBuilder, private residenceService: ResidenceService,private preferenceService: PreferencesService,
    private appartementService:AppartementService,private toastr: ToastrService,private router: Router, private permissionservice: DroitAccesService
    ) {

      this.id_current_user = localStorage.getItem('id');


    this.formBasic=this.fb.group({
      produit:new FormControl(null,Validators.required),
      annee:new FormControl(null,[Validators.required]),
      fraisAnnuelle:new FormControl(null,[Validators.required,Validators.min(0)]),
      remise: new FormControl(null),
      fraisCetteAnnee: new FormControl(null,[Validators.required,Validators.min(0)]),
      M01: new FormControl(null, Validators.required),
      M02: new FormControl(null, Validators.required),
      M03: new FormControl(null, Validators.required),
      M04: new FormControl(null, Validators.required),
      M05: new FormControl(null, Validators.required),
      M06: new FormControl(null, Validators.required),
      M07: new FormControl(null, Validators.required),
      M08: new FormControl(null, Validators.required),
      M09: new FormControl(null, Validators.required),
      M10: new FormControl(null, Validators.required),
      M11: new FormControl(null, Validators.required),
      M12: new FormControl(null, Validators.required),
    })
   }

  ngOnInit(): void {

    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.infoPref = pre.data
    })

    this.checkAccess();


    let currentDate=new Date();
    this.annees=[{'label':currentDate.getFullYear(),'value':currentDate.getFullYear()},{'label':currentDate.getFullYear()+1,'value':currentDate.getFullYear()+1}]
    this.getListResidences();
  }

  
checkAccess(){
  //if access false ==> go out to 403
  this.permissionservice.getAccessUser( this.id_current_user, 'FN21000133').subscribe((res:any)=>{

   this.have_access =res.data;
  
 },error=>{},()=>{

   if(!this.have_access){
     this.router.navigate(['403']);
   }
  
 });
}

  getListResidences(){

    this.residenceService.getResidences().subscribe((res: any) => {
      if(res.statut){
        this.residences = res.data.map(item => ({ label: item.intitule, value: item.cbmarq }))

      }

      ////console.log('res', this.residences);
    },
      error => {
        //console.log('erreur: ', error);
      }
    );

  }

  checkChecked(event) {
    this.check = event
    if (this.check == true) {
    
      this.formBasic.controls["remise"].setValidators(Validators.required);
      this.formBasic.controls["remise"].updateValueAndValidity()


    } else {
      this.formBasic.get('remise').clearValidators();
      this.formBasic.controls["remise"].updateValueAndValidity()
      
      this.formBasic.patchValue({
        remise:null
      })
      
    }

  }

  

  //Frais Syndic de cette année
  onChangeCetteAnnee(event) {

    /*
    if (event) {
      if (event > this.formBasic.value.fraisAnnuelle) {
        this.onChangeValue(this.formBasic.value.fraisAnnuelle)
      } else {
        this.frais = event / 12
        if (this.jour > 15)
          this.formBasic.value.fraisCetteAnnee = (event / (12) * (12 - this.mois))
        else this.formBasic.value.fraisCetteAnnee = (event / (12) * (12 - this.mois + 1))
        this.zero = 0
        var nbmois = 12;
        while (nbmois > 0) {
          switch (nbmois) {
            case 1: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M01: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M01: this.zero }) } } else { this.formBasic.patchValue({ M01: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 2: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M02: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M02: this.zero }) } } else { this.formBasic.patchValue({ M02: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 3: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M03: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M03: this.zero }) } } else { this.formBasic.patchValue({ M03: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 4: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M04: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M04: this.zero }) } } else { this.formBasic.patchValue({ M04: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 5: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M05: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M05: this.zero }) } } else { this.formBasic.patchValue({ M05: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 6: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M06: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M06: this.zero }) } } else { this.formBasic.patchValue({ M06: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 7: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M07: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M07: this.zero }) } } else { this.formBasic.patchValue({ M07: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 8: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M08: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M08: this.zero }) } } else { this.formBasic.patchValue({ M08: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 9: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M09: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M09: this.zero }) } } else { this.formBasic.patchValue({ M09: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 10: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M10: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M10: this.zero }) } } else { this.formBasic.patchValue({ M10: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 11: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M11: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M11: this.zero }) } } else { this.formBasic.patchValue({ M11: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
            case 12: {
              if (this.mois >= nbmois) { if (nbmois == this.mois && this.jour > 15) { this.formBasic.patchValue({ M12: this.zero }) } else if (nbmois == this.mois && this.jour <= 15) { this.formBasic.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } else { this.formBasic.patchValue({ M12: this.zero }) } } else { this.formBasic.patchValue({ M12: parseFloat(this.frais.toFixed(this.infoPref?.round)) }) } break;
            }
          }nbmois--;

        }
      }
    }*/
  }

  //Frais Syndic Annuel
  onChangeValue(event) {
    if (event) {
     //console.log("event: ",event);

      this.frais = event / 12;     
      this.fraisannuelle=event;
      //console.log("event: ",event,"fraisannuelle: ",+(this.fraisannuelle));

      this.formBasic.patchValue({   
       
        fraisCetteAnnee:  +(this.fraisannuelle),  
        M01: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M02: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M03: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M04: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M05: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M06: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M07: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M08: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M09: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M10: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M11: parseFloat(this.frais.toFixed(this.infoPref?.round)),
        M12: parseFloat(this.frais.toFixed(this.infoPref?.round)),
  
      })

      //console.log("this.formBasic: ",this.formBasic.value)


    }
      
   
  }
  //


  onSubmit(){

    this.submitted=true;


    if(this.formBasic.invalid){     
      return;
    }



    
    let formSubmit=({
      fraisCetteAnnee:  parseFloat(this.formBasic.value.fraisCetteAnnee),
      fraisAnnuelle:  parseFloat(this.formBasic.value.fraisAnnuelle),
      remise: 0,
      annee:this.formBasic.value.annee,
      M01:  parseFloat(this.formBasic.value.M01),
      M02:  parseFloat(this.formBasic.value.M02),
      M03:  parseFloat(this.formBasic.value.M03),
      M04:  parseFloat(this.formBasic.value.M04),
      M05:  parseFloat(this.formBasic.value.M05),
      M06:  parseFloat(this.formBasic.value.M06),
      M07:  parseFloat(this.formBasic.value.M07),
      M08:  parseFloat(this.formBasic.value.M08),
      M09:  parseFloat(this.formBasic.value.M09),
      M10:  parseFloat(this.formBasic.value.M10),
      M11:  parseFloat(this.formBasic.value.M11),
      M12:  parseFloat(this.formBasic.value.M12),

    })

    ////console.log("2 formSubmit: ",formSubmit)

    this.somme = this.formBasic.value.M01 + this.formBasic.value.M02 + this.formBasic.value.M03 + this.formBasic.value.M04 +
      this.formBasic.value.M05 + this.formBasic.value.M06 + this.formBasic.value.M07 + this.formBasic.value.M08 +
      this.formBasic.value.M09 + this.formBasic.value.M10 + this.formBasic.value.M11 + this.formBasic.value.M12
    if (this.somme > this.fraisannuelle) {
      this.toastr.error("La somme des frais est supérieure au frais de cette année !", 'Erreur!', { progressBar: true })
    }
    else {

      ////console.log("submit ....... ok");
      this.appartementService.genererFraisAnnuel(this.formBasic.value.produit,formSubmit).subscribe((res:any)=>{

            ////console.log("res: ",res)
            if(res.statut){
              this.toastr.success(res.message, 'Success!', { progressBar: true })
              this.formBasic.reset();
              this.router.navigate(['/syndic/residences']);
            }
            else{
              this.toastr.error(res.message, 'Erreur!', { progressBar: true })

            }
          },error=>{},()=>{

          })

        }


  }

}
