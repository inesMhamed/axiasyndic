import { Component, OnInit } from '@angular/core';
import { DataLayerService } from 'src/app/shared/services/data-layer.service';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { SearchService } from '../../../../shared/services/search.service';
import { FormControl } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith, debounceTime, switchMap, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import 'sweetalert2/src/sweetalert2.scss';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { Permission } from '../../../../shared/models/permission.model';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Base62EncoderService } from 'src/app/shared/services/base-62-encoder.service';

@Component({
  selector: 'app-list-residence',
  templateUrl: './list-residence.component.html',
  styleUrls: ['./list-residence.component.scss'],
  animations: [SharedAnimations],
  providers: [Base62EncoderService]
})
export class ListResidenceComponent implements OnInit {

  viewMode: 'list' | 'grid' = 'list';
  allSelected: boolean;
  page = 1;
  pageSize = 8;
  residences: any = [];
  primary = 'primary';
  results$: Observable<any[]>;
  moduleLoading: boolean;
  can_add: Permission;
  can_edit: Permission;
  can_active: Permission;
  can_detail: Permission;
  can_Gen: Permission;
  id_current_user: any;
  searchCtrl: FormControl = new FormControl('');
  infoPref: any;
  residencesDesactives: any;
  desactive: boolean = false;

  have_access: boolean = false;

  constructor(
    private dl: ResidenceService,
    public preferenceService: PreferencesService,
    public spinnerservice: NgxSpinnerService,
    public searchService: SearchService,
    private toastr: ToastrService,
    private permissionservice: DroitAccesService,
    private router: Router,
    public encodeService: Base62EncoderService
  ) { }

  ngOnInit() {

    this.spinnerservice.show()

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000001');
    this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000002');
    this.can_active = this.permissionservice.search(this.id_current_user, 'FN21000010');
    this.can_detail = this.permissionservice.search(this.id_current_user, 'FN21000006');
    this.can_Gen = this.permissionservice.search(this.id_current_user, 'FN21000133');

    //

    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      console.log('eeee======>', pre)
      this.infoPref = pre.data
      this.pageSize = this.infoPref.affichageTableaux

      this.spinnerservice.hide()
    })
    this.spinnerservice.hide()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000003').subscribe((res: any) => {
      this.have_access = res.data;

    }, error => { }, () => {
      this.spinnerservice.hide()
      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {

        this.listeresidence();


      }

    });
  }

  listeresidence() {
    this.spinnerservice.show();
    //this.moduleLoading = true;
    this.desactive = false
    this.dl.getResidences().subscribe((res: any) => {
      this.residences = res.data;
      this.spinnerservice.hide();
    },
      error => {
        this.spinnerservice.hide();
        //console.log('erreur: ', error);
      }
    );
  }
  listeresidenceDesactiver() {
    this.spinnerservice.show()
    this.desactive = true
    this.dl.getResidencesDesactives().subscribe((res: any) => {
      this.residencesDesactives = res.data;
      this.spinnerservice.hide();
      ////console.log('res desact', this.residencesDesactives);
    },
      error => {
        this.spinnerservice.hide();
        //console.log('erreur: ', error);
      }
    );
  }
  selectAll(e) {
    this.residences = this.residences.map(p => {
      p.isSelected = this.allSelected;
      return p;
    });

    if (this.allSelected) {

    }
    //console.log(this.allSelected);
  }
  searchres() {
    this.searchService.searchOpen = true;
    this.results$ = combineLatest(
      this.residences,
      this.searchCtrl.valueChanges
        .pipe(startWith(''), debounceTime(200))
    )
      .pipe(map(([data, searchTerm]) => {
        return this.residences.filter(p => {
          ////console.log('searchhhhhhhhh jannette', p, searchTerm)
          return p.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
      }));
  }
  cancelCmp(cbmarq: any, active: any, title: any) {

    this.dl.desactiverResidences(cbmarq, active).subscribe(
      (res: any) => {
        this.spinnerservice.hide()
        if (res.statut === true) {
          this.toastr.success('Produit  ' + title + ' avec succès !');
          this.listeresidence();
          this.ngOnInit();
        } else {
          this.toastr.error(res.message);
        }
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any, active: any) {
    var title;
    if (this.desactive === false) {
      title = 'Désactiver';
    } else {
      title = 'Activer';
    }
    Swal.fire({
      title: title + ' un produit',
      text: 'Voulez-vous vraiment ' + title + ' ce produit ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        //console.log('active', this.desactive);
        this.cancelCmp(cbmarq, this.desactive, title);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  modifierResidence(cbmarq) {

    if (!this.desactive) {
      this.router.navigate(['/syndic/modifier-residence/', cbmarq]);

    }
  }


}

