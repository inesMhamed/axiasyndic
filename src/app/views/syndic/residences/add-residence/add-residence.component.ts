import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { CaisseService } from '../../../../shared/services/caisse.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { GroupementService } from 'src/app/shared/services/groupement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { MatStepper } from '@angular/material/stepper';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { ifError } from 'assert';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-add-residence',
  templateUrl: './add-residence.component.html',
  styleUrls: ['./add-residence.component.scss'],
  providers: [ResidenceService, CaisseService]
})
export class AddResidenceComponent implements OnInit, AfterViewInit {
  @ViewChild('labelImport')
  labelImport: ElementRef;
  @ViewChild('stepper') private stepper: MatStepper;
  formBasic: FormGroup;
  formstep: FormGroup;
  loading: boolean;
  radioGroup: FormGroup;
  submitted: boolean;
  isLinear = false;
  moduleLoading: boolean;
  fileToUpload: File = null;
  listecaisse: any[] = []; //= [{ value: 0, label: 'Générer une nouvelle caisse'}];
  listetypeBiens: any[]=[];
  step: any;
  dataStep1: any;
  isResidence: any;
  listProps: any;
  listecaisseSiege: any[] = [];
  id: string;
  modeEdit: boolean;
  groupementList: any[]=[];
  public idResidencce: any
  editable: boolean = false;
  etape: any = 1;
  selectedIndex: any;
  completed0: any;
  completed1: any;
  completed2: any;
  completed3: any;
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;

  have_access:boolean =false;

  constructor(
    private fb: FormBuilder,
    private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService,
    private proprietaireService: ProprietaireService,
    private residenceService: ResidenceService,
    private groupementService: GroupementService,
    private blocService: BlocService,
    private router: Router, private actRoute: ActivatedRoute,
    private caisseService: CaisseService,
    private permissionservice: DroitAccesService
  ) {

  }

  ngOnInit() {
    this.spinnerservice.show()
    this.moduleLoading = true;
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000001');
    this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000002');

    this.submitted = false;
    this.formstep = this.fb.group({
      type: new FormControl('E0062', Validators.required),
      intitule: new FormControl(null, Validators.required),
      adresse: new FormControl("", Validators.required),
      description: new FormControl("",),
      nbrBloc: new FormControl(0, [Validators.required, Validators.min(1)]),
      nbrAppartement: new FormControl("", [Validators.required, Validators.min(1)]),
      photo: new FormControl("",),
    })
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
      if (this.id) {
        this.modeEdit = true;
      }
    });



    this.checkAccessADDEDIT();
    this.buildFormBasic();

    this.getlisttypebiens();
    this.getlistCaisses();
    this.getGroupement()
    this.getlistCaissesSiege();




    if (this.id && this.modeEdit) {
      this.spinnerservice.show()
      this.residenceService.getResidence(this.id).subscribe((data: any) => {

        if(data.statut){
            this.etape = data.data?.etape;
           
              this.formstep.patchValue({
                type: data.data?.type?.code,
                intitule: data.data?.intitule,
                cbmarq: data.data?.cbmarq,
                photo: data.data?.photo,
                adresse: data.data?.adresse,
                description: data.data?.description,
                nbrBloc: data.data?.nbrBloc,
                nbrAppartement: data.data?.nbrAppartement,
              })
           
          
            if (data.data?.etape >= 3) {

              this.formBasic.patchValue({
                type: data.data?.type?.code,
                intitule: data.data?.intitule,
                cbmarq: data.data?.cbmarq,
                photo: data.data?.photo,
                adresse: data.data?.adresse,
                description: data.data?.description,
                nbrBloc: data.data?.nbrBloc,
                nbrAppartement: data.data?.nbrAppartement,
                caisse: data?.data?.caisse?.cbmarq,
                caisseSiege: data?.data?.caisse?.caisseSiege?.cbmarq,
                fraisSyndic: data?.data?.fraisSyndic,
                etape: data?.data?.etape
              });

            }
            this.completed0 = this.etape > 1;
            this.completed1 = this.etape > 2;
            this.completed2 = this.etape > 3;
            this.completed3 = this.etape > 4;
            setTimeout(() => {
              if (this.etape === 4 || this.etape === 5) {
                this.stepper.selectedIndex = 0;

              } else {
                this.stepper.selectedIndex = (this.etape - 1);
              }

          }, 0);
          this.moduleLoading = false;
          this.spinnerservice.hide()
        }


        this.spinnerservice.hide()

      });

    } else {
      this.spinnerservice.hide()
      this.completed0 = false;
      this.completed1 = false;
      this.completed2 = false;
      this.completed3 = false;
      this.moduleLoading = false;
    }

    

  }

  checkAccessADDEDIT(){

    //mode Edit
    if (this.id && this.modeEdit){
        this.checkAccess('FN21000002')
    }else{
      this.checkAccess('FN21000001')
    }
    
 }

 checkAccess(code){

      //if access false ==> go out to 403
      this.permissionservice.getAccessUser( this.id_current_user,code ).subscribe((res:any)=>{

        this.have_access =res.data;
      
      },error=>{},()=>{

        if(!this.have_access){
          this.router.navigate(['403']);
        }
      
      });

 }


  ngAfterViewInit() {
    this.selectedIndex = (this.etape - 1);
  }
  move(index: number) {
    this.stepper.selectedIndex = index;
  }
  getGroupement() {
    this.spinnerservice.show()
    this.groupementService.getGroupements().subscribe((res: any) => {

      if (res.statut) {
        this.spinnerservice.hide()
        this.groupementList = res.data.map(c => ({ value: c.cbmarq, label: c.intitule }));
      }
      this.spinnerservice.hide()

    }
    );
  }
  onchangeCaisse(event) {
    //console.log('', event)
    this.formBasic.patchValue({
      caisseSiege: event.siege
    })
  }
  getlistCaisses() {
    this.spinnerservice.show()
    this.listecaisse.unshift({ value: 0, label: 'Générer caisse produit', siege: 0 });
    this.caisseService.getCaisses().subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()

        this.listecaisse = res.data.map(c => ({ value: c.cbmarq, label: c.intitule, siege: c?.caisseSiege?.cbmarq }));
          this.listecaisse.unshift({ value: 0, label: 'Générer caisse produit', siege: 0 });
          this.listecaisse=this.listecaisse.filter(item=>item.value!=undefined);


      }
      this.spinnerservice.hide()
    }
    );
  }
  getlistCaissesSiege() {
    this.spinnerservice.show()
    this.listecaisseSiege.unshift({ value: 0, label: 'Générer caisse siége produit' });
    this.caisseService.getCaisseSiege().subscribe((res: any) => {

      if (res.statut) {
        this.spinnerservice.hide()
        this.listecaisseSiege = res.data.map(c => ({ value: c.cbmarq, label: c.intitule }));
        this.listecaisseSiege.unshift({ value: 0, label: 'Générer caisse siége produit' });
      }

      this.spinnerservice.hide()
    }
    );
  }
  getlisttypebiens() {
    this.spinnerservice.show()
    this.residenceService.gettypebien('TYB').subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()
        this.listetypeBiens = res.data;
      }
      this.spinnerservice.hide()
    }
    );
  }
  buildFormBasic() {
    this.formBasic = this.fb.group({
      type: new FormControl(null, Validators.required),
      intitule: new FormControl(null, Validators.required),
      adresse: new FormControl("", Validators.required),
      description: new FormControl("",),
      nbrBloc: new FormControl("",),
      nbrAppartement: new FormControl("", [Validators.required, Validators.min(0)]),
      etape: new FormControl(null,),
      caisse: new FormControl(null, Validators.required),
      caisseSiege: new FormControl(null,),
      fraisSyndic: new FormControl(null, [Validators.required, Validators.min(1)]),
      photo: new FormControl("",),
    });

  }
  changerType(event) {
    const nbrBloc = this.formstep.controls['nbrBloc'];
    if (event && event.value === 'E0062') {
      nbrBloc.setValidators([Validators.required, Validators.min(1)]);
    } else {
      nbrBloc.clearValidators();
    }
    nbrBloc.updateValueAndValidity();
  }
  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUpload = files.item(0);

    if (files.length > 0) {
      const file = files[0];
      this.formstep.patchValue({
        photo: file
      });
    }
  }

  submit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.formstep.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    const myFormValue = this.formstep.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    if (this.can_add?.permission) {
      this.residenceService.postResidences(myFormData).pipe(first()).subscribe((res: any) => {
        this.spinnerservice.hide()
        if (res.statut === true) {
          this.toastr.success('Produit a été ajoutée avec succès !', 'Success!', { progressBar: true });
          //console.log('after submit', res)
          this.dataStep1 = res.data
          this.id = res.data.cbmarq
          this.etape = res.data.etape
          this.formBasic.patchValue({
            type: res.data.type?.code,
            intitule: res.data.intitule,
            cbmarq: res.data.cbmarq,
            photo: res.data.photo,
            adresse: res.data.adresse,
            description: res.data.description,
            nbrBloc: res.data.nbrBloc,
            nbrAppartement: res.data.nbrAppartement,
          })
          this.submitted = false;
          this.router.navigate(['/syndic/modifier-residence/', res.data?.cbmarq]);
          setTimeout(() => {
            this.stepper.selectedIndex = (this.etape - 1);
          }, 0);
          // this.formstep.reset();
        } else {
          this.submitted = true;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
        (error) => {
          this.toastr.error('Erreur lors de l\'ajout d\'une résidence . Veuillez réessayer !', 'Erreur!', { progressBar: true });
        });
    } else {
      this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
    }
  }

  submit1() {
    this.spinnerservice.show()
    this.formBasic.patchValue({
      type: this.formstep.value.type,
      intitule: this.formstep.value.intitule,
      cbmarq: this.formstep.value.cbmarq,
      adresse: this.formstep.value.adresse,
      photo: this.formstep.value.photo,
      description: this.formstep.value.description,
      nbrBloc: this.formstep.value.nbrBloc,
      // etape: this.etape,
      nbrAppartement: this.formstep.value.nbrAppartement,
    });
    if (this.etape === 2) {
      this.formBasic.patchValue({ etape: 3 });
    } else {
      this.formBasic.patchValue({ etape: this.etape });
    }
    this.loading = true;
    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      //console.log('formBasic, ', this.formBasic.invalid)
      this.toastr.error('Veuillez remplir tous les champs obligatoires dans les étapes 1 ou 2 !!', 'Erreur!', { progressBar: false });
      this.submitted = true;
      return;
    }
    const myFormValue = this.formBasic.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    if (this.can_edit?.permission) {
      //console.log(this.formBasic.value,this.id)
      this.residenceService.putResidences(myFormData, this.id).pipe(first()).subscribe((res: any) => {
        this.spinnerservice.hide()
        if (res.statut) {
          this.etape = res.data.etape;
          this.completed1 = this.etape > 2;
          setTimeout(() => {

            if (this.etape === 4 || this.etape === 5) {
              // this.stepper.selectedIndex = (this.etape - 2);
              // this.stepper.selectedIndex = 0;
            } else {
              this.stepper.selectedIndex = (this.etape - 1);
            }


          }, 0);
          //console.log(res.data, this.completed1, this.etape)
          this.toastr.success('Produit a été modifié avec succès !', 'Success!', { progressBar: true });
        } else {
          this.spinnerservice.hide()
          this.submitted = true;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },
        (error) => {
          this.spinnerservice.hide()
          this.toastr.error('Erreur lors de la modification d\'une résidence . Veuillez réessayer !', 'Erreur!', { progressBar: true });
        });
    } else {
      this.spinnerservice.hide()
      this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
    }
  }



}
