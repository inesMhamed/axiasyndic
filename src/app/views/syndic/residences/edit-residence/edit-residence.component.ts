import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {Residence} from '../../../../shared/models/residence.model';
import {CaisseService} from '../../../../shared/services/caisse.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-residence',
  templateUrl: './edit-residence.component.html',
  styleUrls: ['./edit-residence.component.scss']
})
export class EditResidenceComponent implements OnInit {

  @ViewChild('labelImport')
  labelImport: ElementRef;
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  fileToUpload: File = null;
  id: any;
  listecaisse: any[];

  constructor(
      private fb: FormBuilder,
      private toastr: ToastrService,
      private residenceService: ResidenceService,
      private actRoute: ActivatedRoute ,
      private router: Router,
      public spinnerservice: NgxSpinnerService,
      private caisseService: CaisseService
  ) {

  }

  ngOnInit() {
    this.spinnerservice.show()
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
    });
    this.getlistCaisses();
    this.submitted = false;
    this.buildFormBasic();
    if (this.id) {
      this.residenceService.getResidence(this.id).pipe(first())
          .subscribe((x: any) => {
            var ent: Residence = x.data ;
            console.log("edit residence : ",x)
            this.formBasic.patchValue(ent);
            this.formBasic.patchValue({caisse: ent['caisse'].cbmarq});
          });
          this.spinnerservice.hide()
    }
  }

  getlistCaisses() {
    this.spinnerservice.show()
    this.caisseService.getCaisses().subscribe(
        (res: any) => {
          this.listecaisse = res.data.map(c => ({ value: c.cbmarq, label: c.intitule }));
          this.spinnerservice.hide()
        }
    );
  }

  buildFormBasic() {
    this.formBasic = this.fb.group({
      intitule: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
      description: new FormControl('', ),
      nbrBloc: new FormControl(null, [Validators.required, Validators.min(1)]),
      nbrAppartement: new FormControl('', [Validators.required, Validators.min(1)]),
      president: new FormControl(null, ),
      vice: new FormControl(null, ),
      tresorerie: new FormControl(null, ),
      caisse: new FormControl(null, ),
      caissesiege: new FormControl(null, ),
      groupement: new FormControl(null, ),
      photo: new FormControl('', ),
    });
  }

  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
        .map(f => f.name)
        .join(', ');
    this.fileToUpload = files.item(0);

    if ( files.length > 0) {
      const file =  files[0];
      this.formBasic.patchValue({
        photo: file
      });
    }
  }

  submit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.formBasic.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }

    // this.formBasic.value.photo = this.fileToUpload;
    const myFormValue = this.formBasic.value;

    const myFormData = new FormData();

    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });

    this.residenceService.putResidences(myFormData, this.id).pipe(first()).subscribe((res: any) => {
          if (res.statut === true) {
            this.spinnerservice.hide()
            this.toastr.success('Résidence a été modifiée avec succès !', 'Success!', {progressBar: true});
            this.router.navigate(['/syndic/residences']);
            this.submitted = false;
            this.formBasic.reset();
          } else {
            this.spinnerservice.hide()
            this.submitted = true;
            this.toastr.error(res.message, 'Erreur!', {progressBar: true});
          }
        },
        (error) => {
          this.spinnerservice.hide()
          this.toastr.error('Erreur lors de la modification d\'une résidence . Veuillez réessayer !', 'Erreur!', {progressBar: true});
        });

  }

}
