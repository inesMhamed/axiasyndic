import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';

@Component({
  selector: 'app-info-operation',
  templateUrl: './info-operation.component.html',
  styleUrls: ['./info-operation.component.scss']
})
export class InfoOperationComponent implements OnInit {



  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  formGroup: FormGroup;
  residences: any = [];
  cbmarq: any;
  selectedCat: any;
  codeCat: any;
  blocs: any = [];
  types: any = [];
  categorie: any = [];
  natures: any = [];
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  currentModel:any;
  typeOperation:string="date";
  negatifValueDate:boolean=false;
  negatifValueHeure:boolean=false;
  negatifValueKm:boolean=false;
  negatifValueNotifAvant:boolean=false;

  radioItems: Array<string>;
  model   = {option: 'date'};

  gammeOperatoireList: any[] = [];

  currentDate:any;
  pipe = new DatePipe('en-Fr'); // Use your own locale


  constructor(private sharedData: SharedDataService, private fb: FormBuilder, private permissionservice: DroitAccesService, private configMaintenanceService: ConfigMaintenanceService
    , private toastr: ToastrService, private cdRef: ChangeDetectorRef, private spinnerservice: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.spinnerservice.hide()
    this.currentDate = new Date();
    this.radioItems = ['date', 'heure'];//, 'km'
    this.sharedData.sharedidOperation.subscribe(message => this.cbmarq = message);
    this.getListGammeOperatoire();
    ////console.log("on init .. cbmarq info operation: ",this.cbmarq)
    if(this.cbmarq!=0){
      this.getDetail();
    }


    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');

    this.formGroup = this.fb.group({
      intitule:new FormControl(''),
      date: new FormControl(null, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      km: new FormControl(null),
      heure: new FormControl(null),
      //options: new FormControl(""),
      datedeb: new FormControl(new Date(), [Validators.required]),
      notifavant:new FormControl(null, [Validators.required]),
      gamme: new FormControl(null, [Validators.required]),
     
    });

  }
  ngAfterViewChecked()
{
  this.cdRef.detectChanges();
}


  getListGammeOperatoire(){

    this.spinnerservice.show()
    this.configMaintenanceService.getListGammeOperatoire().subscribe((res: any) => {
      if (res.statut) {

          ////console.log("resGamme: ",res.data)
          this.gammeOperatoireList = res.data.map(clt => ( {

            value: clt.cbmarq, label: (clt.intitule + " - Equipement : "+ clt.equipement?.intitule
           +" - Emplacement : "+   clt.equipement?.produit?.intitule
           + (clt.equipement?.emplacement?.parent?.intitule!=undefined? ', '+clt.equipement?.emplacement?.parent?.intitule:'')
           +(clt.equipement?.emplacement?.intitule!=undefined?', '+clt.equipement?.emplacement?.intitule:'')
           )


        } ));

        this.spinnerservice.hide()
      }
    })
  }

  


  getDetail() {
    this.spinnerservice.show()
    this.configMaintenanceService.getOperationById(this.cbmarq).subscribe((res: any) => {

      if(res.statut){
        this.currentModel=res.data;

        //console.log("current operation : ",this.currentModel)

        const now = new Date(this.currentModel.dateExecution?.date);
        const myFormattedDate = this.pipe.transform(now, 'yyyy-MM-dd');
        this.currentDate=myFormattedDate;

       

        this.formGroup.patchValue({
          date: this.currentModel.date!=undefined?this.currentModel.date:null,
          km: this.currentModel.km!=undefined?this.currentModel.km:null,
          heure: this.currentModel.heuure!=undefined? this.currentModel.heuure:null,
          datedeb: myFormattedDate,
          notifavant:this.currentModel.notifavant,
          gamme: this.currentModel.gamme?.cbmarq,

        })

        this.spinnerservice.hide()


      }
    }, error => { }, () => {
      this.spinnerservice.hide()
      if (this.formGroup.value.date) {
        this.model = { option: 'date' };
        this.typeOperation = 'date';
      }
      if (this.formGroup.value.heure) {
        this.model = { option: 'heure' };
        this.typeOperation = 'heure';
      }
      if (this.formGroup.value.km) {
        this.model = { option: 'km' };
        this.typeOperation = 'km'
      }


    })
  }

  onSubmit(){

    this.spinnerservice.show()

    this.submitted = true;
    if (this.formGroup.invalid || this.negatifValueDate || this.negatifValueHeure || this.negatifValueNotifAvant) {
      this.spinnerservice.hide()
      return;
    }

    //console.log("submit: ",this.formGroup.value)

    //return;
    //ajout
    if (this.cbmarq == 0) {
      this.spinnerservice.show()

      this.configMaintenanceService.addOperation(this.formGroup.value).subscribe((res: any) => {
        if (res.statut) {
          this.toastr.success("Opération a été ajoutée avec succès !", 'Success!', { progressBar: true })
          this.formGroup.reset();
          this.sharedData.changeViewOperation(false);
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de l\'ajout  d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })


    } else { //modification
      this.configMaintenanceService.putOperation(this.formGroup.value, this.cbmarq).subscribe((res: any) => {
        if (res.statut) {

          this.toastr.success("Opération a été modifiée avec succès !", 'Success!', { progressBar: true })
          this.formGroup.reset();
          this.sharedData.changeViewOperation(false);
          this.sharedData.changeViewGenOperation(false);
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      }, error => {
        this.toastr.error('Erreur lors de modification  d\'une opération . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
    }

  }

  goListeOperation(){
    this.sharedData.changeViewOperation(false);
    this.sharedData.changeViewGenOperation(false);
  }

  changeRadio(type) {
    this.spinnerservice.show()
    // //console.log("on change radio: ",type)
    this.typeOperation = type;

    if (this.typeOperation == "date") {
      const now = Date.now();
      const myFormattedDate = this.pipe.transform(now, 'yyyy-MM-dd');
      this.currentDate = myFormattedDate;

      this.formGroup = this.fb.group({
        intitule: new FormControl(null),
        date: new FormControl(null, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
        km: new FormControl(null),
        heure: new FormControl(null),
        datedeb: new FormControl(myFormattedDate, [Validators.required]),
        notifavant:new FormControl(null, [Validators.required]),
        gamme: new FormControl(null, [Validators.required]),
       
      });
      this.spinnerservice.hide()
    }
    if (this.typeOperation == 'heure') {

      const now = Date.now();
      const myFormattedDate = this.pipe.transform(now, 'yyyy-MM-dd');
      this.currentDate=myFormattedDate;


      this.formGroup = this.fb.group({
        intitule:new FormControl(null),
        date: new FormControl(null),
        km: new FormControl(null),
        heure: new FormControl(null,[Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
        datedeb: new FormControl(myFormattedDate, [Validators.required]),
        notifavant:new FormControl(null, [Validators.required]),
        gamme: new FormControl(null, [Validators.required]),
       
      });


      this.spinnerservice.hide()

    }
    if(this.typeOperation=="km"){

      const now = Date.now();
      const myFormattedDate = this.pipe.transform(now, 'yyyy-MM-dd');
      this.currentDate=myFormattedDate;

      this.formGroup = this.fb.group({
        intitule:new FormControl(null),
        date: new FormControl(null),
        km: new FormControl(null, [Validators.required]),
        heure: new FormControl(null),
        datedeb: new FormControl(myFormattedDate, [Validators.required]),
        notifavant:new FormControl('', [Validators.required]),
        gamme: new FormControl('', [Validators.required]),
       
      });
      this.spinnerservice.hide()

     
    }

   // //console.log("this.formGroup: ",this.formGroup)


  }

  onchangeValueDate(event){

    ////console.log("onChangeValueDate: ",this.formGroup.value.date)
    if(this.formGroup.value.date>0 || this.formGroup.value.date==null){
      this.negatifValueDate=false;

    }else if(this.formGroup.value.date<0 && this.formGroup.value.date!=null){
      this.negatifValueDate=true;
    }

  }

  onchangeValueHeure(event){

    ////console.log("onChangeValueDate: ",this.formGroup.value.date)
    if(this.formGroup.value.heure>0 || this.formGroup.value.heure==null){
      this.negatifValueHeure=false;

    }else if(this.formGroup.value.heure<0 && this.formGroup.value.heure!=null){
      this.negatifValueHeure=true;
    }

  }

  onchangeValueKm(event){

    ////console.log("onChangeValueDate: ",this.formGroup.value.date)
    if(this.formGroup.value.km>0 || this.formGroup.value.km==null){
      this.negatifValueKm=false;

    }else if(this.formGroup.value.km<0 && this.formGroup.value.km!=null){
      this.negatifValueKm=true;
    }

  }


  onchangeValueNotifAvant(event){

    if(this.formGroup.value.notifavant>0 || this.formGroup.value.notifavant==null){
      this.negatifValueNotifAvant=false;

    }else if(this.formGroup.value.notifavant<0 && this.formGroup.value.notifavant!=null){
      this.negatifValueNotifAvant=true;
    }

  }
}
