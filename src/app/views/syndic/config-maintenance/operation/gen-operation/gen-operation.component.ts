import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DaterangepickerComponent } from 'ngx-daterangepicker-material';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-gen-operation',
  templateUrl: './gen-operation.component.html',
  styleUrls: ['./gen-operation.component.scss'],
  providers: [DatePipe,SharedDataService],  
 
})
export class GenOperationComponent implements OnInit {

 
 id_current_user: any;
 have_access:boolean =false;
 viewListGenOperation:boolean=false; 
 idOperation:number=0;


  constructor(private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService,private toastr: ToastrService,private fb:FormBuilder,private fbGen:FormBuilder,
    private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService,
    private datepipe: DatePipe,private router: Router,private sharedData: SharedDataService) { 
     
      this.id_current_user = localStorage.getItem('id'); 
     
     
    }

      
  ngOnInit(): void {

    this.sharedData.sharedViewGenOperation.subscribe(message => this.viewListGenOperation = message)
    this.sharedData.sharedidOperation.subscribe(message => this.idOperation = message)

    ////console.log("Init Parent List viewListOperation: ",this.viewListGenOperation);


    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000124').subscribe((res:any)=>{

     this.have_access =res.data;
    
   },error=>{},()=>{

     if(!this.have_access){
       this.router.navigate(['403']);
     }else{     
     
     
      
      
     

     }
    
   });

   


 }


    


}
