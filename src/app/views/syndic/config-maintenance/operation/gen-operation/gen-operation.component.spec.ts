import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenOperationComponent } from './gen-operation.component';

describe('GenOperationComponent', () => {
  let component: GenOperationComponent;
  let fixture: ComponentFixture<GenOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenOperationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
