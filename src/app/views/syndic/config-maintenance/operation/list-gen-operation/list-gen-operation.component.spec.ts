import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGenOperationComponent } from './list-gen-operation.component';

describe('ListGenOperationComponent', () => {
  let component: ListGenOperationComponent;
  let fixture: ComponentFixture<ListGenOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListGenOperationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGenOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
