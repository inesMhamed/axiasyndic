import { DatePipe } from '@angular/common';
import { Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DaterangepickerComponent } from 'ngx-daterangepicker-material';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';
import { ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {  MomentDateAdapter,  MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {  DateAdapter,  MAT_DATE_FORMATS,  MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment, Moment } from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY'
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};
/** @title Datepicker emulating a Year and month picker */
@Component({
  selector: 'app-list-gen-operation',
  templateUrl: './list-gen-operation.component.html',
  styleUrls: ['./list-gen-operation.component.scss'],
  providers: [DatePipe, SharedDataService,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ListGenOperationComponent),
      multi: true
    }

  ],

})

export class ListGenOperationComponent implements OnInit, ControlValueAccessor {

  @ViewChild('dp') dp: MatDatepicker<Moment>;

  date = new FormControl(moment());

  _max: Moment | undefined;
  @Input()
  get max(): Moment | undefined {
    return this._max ? this._max : undefined;
  }
  set max(max: Moment | undefined) {
    if (max) {
      const momentDate: Moment =
        typeof max === 'number' ? moment([max, 0, 1]) : moment(max);
      this._max = momentDate.isValid() ? momentDate : undefined;
    }
  }

  _min: Moment | undefined;
  @Input()
  get min(): Moment | undefined {
    return this._min ? this._min : undefined;
  }
  set min(min: Moment | undefined) {
    if (min) {
      const momentDate =
        typeof min === 'number' ? moment([min, 0, 1]) : moment(min);
      this._min = momentDate.isValid() ? momentDate : undefined;
    }
  }

  // Function to call when the date changes.
  onChange = (year: Date) => {};

  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {};

  writeValue(date: Date): void {
    if (date && this._isYearEnabled(date.getFullYear())) {
      const momentDate = moment(date);
      if (momentDate.isValid()) {
        momentDate.set({ date: 1 });
        this.date.setValue(moment(date), { emitEvent: false });
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean): void {
    isDisabled ? (this.dp.disabled = true) : (this.dp.disabled = false);

    isDisabled ? this.date.disable() : this.date.enable();
  }

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
   
    datepicker.close();

    if (!this._isYearEnabled(normalizedYear.year())) {
      return;
    }else{
      this.formGroupSearch.patchValue({exercice:normalizedYear.year().toString()})
      this.formGroupGenerer.patchValue({exercice:normalizedYear.year().toString()})
    }
   

    normalizedYear.set({ date: 1 });

    this.date.setValue(normalizedYear, { emitEvent: false });
    this.onTouched();
   console.log(this.formGroupSearch.controls)
   console.log(this.formGroupGenerer.controls)
  }

  chosenMonthHandler(
    normalizedMonth: Moment,
    datepicker: MatDatepicker<Moment>
  ) {
    datepicker.close();

    this.date.setValue(normalizedMonth.month());
    this.onTouched();
  }

  _openDatepickerOnClick(datepicker: MatDatepicker<Moment>) {
    if (!datepicker.opened) {
      datepicker.open();
    }
  }

  // disable if the year is greater than maxDate lower than minDate
  private _isYearEnabled(year: number) {
    if (
      year === undefined ||
      year === null ||
      (this._max && year > this._max.year()) ||
      (this._min && year < this._min.year())
    ) {
      return false;
    }

    return true;
  }

  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['produit','emplacement','gamme','equipement' ,'notifieravant','dateexecution','action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;

  formGroupSearch:FormGroup;
  formGroupGenerer:FormGroup;
  blocs: any[]=[];
  residences: any[]=[];
  appartements: any[]=[];
  rechAvance:boolean=false;
  genererBtn:boolean=false;
  gammeOperatoireList:any[]=[];
  selectedEmplacementId :any;
  emplacements:any[]=[];
  currentYear={label: new Date().getFullYear(),value:new Date().getFullYear()};
  currentExercice:Date=new Date();
  exerciceDatePicker:any;
  listExercice:any[]=[];
  datepickerOptions = {
    datepickerMode:"'year'",
    minMode:"'year'",
    minDate:"minDate",
    showWeeks:"false",
 };

 have_access:boolean =false;
 viewListGenOperation:boolean=false; 
 idOperation:number=0;
 myVar: any;

  constructor(private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService,private toastr: ToastrService,private fb:FormBuilder,private fbGen:FormBuilder,
    private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService,
    private spinnerservice: NgxSpinnerService,
    private datepipe: DatePipe, private router: Router, private sharedData: SharedDataService) {

      this.id_current_user = localStorage.getItem('id');

     }

    
     
  ngOnInit(): void {
    this.spinnerservice.show()
    this.sharedData.sharedViewGenOperation.subscribe((message: any) => {
      this.viewListGenOperation = message;
      if(this.viewListGenOperation !== this.myVar){
        ////console.log("do something... on listGenOperation ",this.formGroupSearch?.value);

        if(this.formGroupSearch?.value?.produit!=null && this.formGroupSearch?.value?.exercice!=null ){
          this.onSubmitSearch();
        }
      }
    } );
  

    this.sharedData.sharedidOperation.subscribe(message => this.idOperation = message)


    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000124').subscribe((res:any)=>{

     this.have_access =res.data;
    
   },error=>{},()=>{

     if(!this.have_access){
       this.router.navigate(['403']);
     }else{
       
       this.listeresidence();
       this.getYears();
     
      

       


     }
    
   });

   

  

   //document.getElementById('inputExerciceId')

   this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
   this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
   this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
   this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
   this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
   this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

   this.formGroupSearch = this.fb.group({      
    
     produit:new FormControl(null,Validators.required),
     exercice:new FormControl(this.currentYear),
    

   });

   this.formGroupGenerer = this.fbGen.group({    
    
     exercice:new FormControl(this.currentYear,Validators.required),     

   });

  

 }

 getYears(){

   for(let annee=2000 ;annee<=2050;annee++){
     this.listExercice.push({label:annee,value:annee})
   }
 }

  submitGenererOperation() {
    this.spinnerservice.show()
    if (this.formGroupGenerer.invalid) {
      this.spinnerservice.hide()
      return;
    }
    console.log('form eeee', this.formGroupGenerer.value)
    let exercice = "" + this.formGroupGenerer.value.exercice;
    ////console.log("submit generer:exercice  ",exercice)
    console.log('exercice', exercice)

    this.configMaintenanceService.genererOperation(exercice).subscribe((res: any) => {
      if (res.statut) {
        this.toastr.success('Opération générer avec succès !');
        this.genererBtn = false;
        this.spinnerservice.hide()
      } else {
        this.spinnerservice.hide()
        this.toastr.error("Erreur lors du génération des opérations de maintenance");
      }
    })

  }

  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
      }
      else{
        this.spinnerservice.hide()
      }
    },
      error => {
        //console.log( 'erreur: ', error);
      }
    );
  }

  OnChangeResidence(event: any) {
    this.spinnerservice.show()
    this.emplacements = [];
    let tous = { value: null, label: "Tous" };
    this.configMaintenanceService.getEmplacementByFiltreTrier(event.value).subscribe((res: any) => {
      if (res.statut) {
        this.emplacements = res.data.map(clt =>
          ({ value: clt.cbmarq, label: clt.intitule, parent: clt.parent?.intitule, idParent: clt.parent?.cbmarq })
        );
        this.spinnerservice.hide()
      } else{
        this.spinnerservice.hide()
      }
    }, error => { }, () => {

      this.formGroupSearch.patchValue({ emplacement: null });
    })
  }

  onSubmitSearch() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<any>();
    if (this.formGroupSearch.invalid) {
      this.spinnerservice.hide()
      return;
    }

    this.configMaintenanceService.filterOperationByExercice(this.formGroupSearch.value).subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
      } else {
        this.spinnerservice.hide()
        this.toastr.error("Aucune résultat trouvée, veuillez réessayer.");

      }
    })


  }

 deleteRow(cbmarq: any) {
   this.configMaintenanceService.deleteOperation(cbmarq).subscribe(
     (res: any) => {
       if ( res.statut === true) {
         this.toastr.success('Opération supprimer avec succès !');
         //this.getListOperation();
       } else {
         this.toastr.error(res.message);
       }
      
     },
     error => {
       this.toastr.error('Veuillez réessayer plus tard!');
     }
   );
 }
 confirmBox(cbmarq: any) {
   Swal.fire({
     title: 'Supprimer une opération',
     text: 'Voulez-vous vraiment supprimer cette opération ?',
     icon: 'warning',
     showCancelButton: true,
     confirmButtonText: 'Confirmer',
     cancelButtonText: 'Annuler'
   }).then((result) => {
     if (result.value) {
       this.deleteRow(cbmarq);
     } else if (result.dismiss === Swal.DismissReason.cancel) {
     }
   })
 }

 applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}

 editLine(cbmarq:any){

   //console.log("editLine: ",cbmarq)
   this.sharedData.changeViewGenOperation(true);
   this.sharedData.sendIdOperation(cbmarq);
  

 }

}
