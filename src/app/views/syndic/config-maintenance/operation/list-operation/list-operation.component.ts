import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-operation',
  templateUrl: './list-operation.component.html',
  styleUrls: ['./list-operation.component.scss']
})
export class ListOperationComponent implements OnInit {

  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['produit','emplacement','gamme','equipement' ,'frequence','datedebut','notifieravant','action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;

  formGroupSearch:FormGroup;
  blocs: any[]=[];
  residences: any[]=[];
  appartements: any[]=[];
  rechAvance:boolean=false;
  gammeOperatoireList:any[]=[{ value: 0, label: "Tous" }];
  selectedEmplacementId :any;
  emplacements:any[]=[{ value: 0, label: "Tous" }];


  constructor(private sharedData: SharedDataService, private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService, private toastr: ToastrService, private fb: FormBuilder, private spinnerservice: NgxSpinnerService,
    private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.formGroupSearch = this.fb.group({

      produit:new FormControl(0,Validators.required),
      emplacement:new FormControl(0),
      gamme: new FormControl(0),

    });

    this.getListOperation();
    this.listeresidence();

  }

  ajouterOperation(){
    this.sharedData.changeViewOperation(true);
    this.sharedData.sendIdOperation(0);
  }



  getListGammeOperatoire() {
    this.spinnerservice.show()
    this.gammeOperatoireList = [];

    let formSearchGamme={
      produit: this.formGroupSearch.value.produit,
      emplacement: this.selectedEmplacementId,
      equipement: null
    }

    let tous={ value: 0, label: "Tous" };


    ////console.log("formSearchGamme: ",formSearchGamme)


    this.configMaintenanceService.getGammeByFiltre(formSearchGamme).subscribe((res: any) => {
      if (res.statut) {
        this.gammeOperatoireList = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));

        this.gammeOperatoireList.splice(0, 0, tous);
        this.spinnerservice.hide()
        ////console.log("inn get list gamme",this.gammeOperatoireList)

        }
    })
  }


  getListOperation() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<any>();

    this.configMaintenanceService.getListOperation().subscribe((res:any)=>{
      ////console.log("inn getListOperation: ", res)

      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.spinnerservice.hide()
      }
    })
  }

  
  listeresidence() {
    this.spinnerservice.show()
    let tous = { value: 0, label: "Tous" };

    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        ////console.log("this.residence: ",this.residences)
        this.residences.splice(0, 0, tous);
        this.spinnerservice.hide()
      }

    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }


  OnChangeResidence(event: any) {
    this.spinnerservice.show()
    this.emplacements = [{ value: 0, label: "Tous" }];
    this.gammeOperatoireList = [{ value: 0, label: "Tous" }];
    this.selectedEmplacementId = null;
    this.formGroupSearch.patchValue({
      emplacement: 0,
      gamme: 0
    });

    let tous = { value: 0, label: "Tous" };



    this.configMaintenanceService.getEmplacementByFiltreTrier(event.value).subscribe((res: any) => {
      if (res.statut) {

        ////console.log("OnChangeResidence: ",res.data)
        this.emplacements = res.data.map(clt =>
          ( { value: clt.cbmarq, label: clt.intitule ,parent: clt.parent?.intitule ,idParent:clt.parent?.cbmarq })

        );
        this.spinnerservice.hide()

        ////console.log("list emplacement: ",this.emplacements)

        this.emplacements.splice(0, 0, tous);



      }
    }, error => { }, () => {

      this.spinnerservice.hide()

    })

  }

  onRemoveEmplacement(event){
    this.selectedEmplacementId=null;
  }

  OnChangeEmplacement(event) {
    this.spinnerservice.show()
    this.gammeOperatoireList = [{ value: 0, label: "Tous" }];
    this.selectedEmplacementId = null;
    this.formGroupSearch.patchValue({
      gamme: 0
    });

    let label: string = "";
    if (event?.label) {
      label = event?.label;
    } else {
      label = event?.parent;
    }



    ////console.log("selected Label: ",label)



    let modelEmpl={
      produit: this.formGroupSearch.value.produit,
      intitule: label
    }

    ////console.log("modelEmpl: ",modelEmpl)
    if(event!=undefined && event!=null && event?.value!=0){

      this.configMaintenanceService.getEmplacementByLabelAndProduit(modelEmpl).subscribe((res: any) => {
        if (res.statut) {
          this.spinnerservice.hide()
          ////console.log("res data : ",res.data)

          this.selectedEmplacementId=res?.data?.cbmarq;

          ////console.log("selectedEmplacementId : ",res.data)
        }
      }, error => { }, () => {
        this.getListGammeOperatoire();
      })
    }






  }


  onSubmitSearch(){

    this.dataSource=new MatTableDataSource<any>();



    if(this.formGroupSearch.invalid){
      return;
    }

    let formSearch={
      produit: this.formGroupSearch.value.produit?this.formGroupSearch.value.produit:null,
      emplacement: this.selectedEmplacementId? this.selectedEmplacementId:null,
      gamme: this.formGroupSearch.value.gamme==0?null:this.formGroupSearch.value.gamme,
    }

    ////console.log("formSearch : ",formSearch)


    if(formSearch.produit!=null){
      this.configMaintenanceService.filterOperation(formSearch).subscribe((res:any)=>{
        if(res.statut){

          ////console.log("res search: ",res.data)
          this.dataSource.data=res.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

          ////console.log("res filtre: ",this.dataSource.data)

        }else{
          //console.log(res)
        }
      })
    }else{
      this.getListOperation();
    }




  }

  deleteRow(cbmarq: any) {
    this.configMaintenanceService.deleteOperation(cbmarq).subscribe(
      (res: any) => {
        if ( res.statut === true) {
          this.toastr.success('Opération supprimer avec succès !');
          this.getListOperation();
        } else {
          this.toastr.error(res.message);
        }
       
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une opération',
      text: 'Voulez-vous vraiment supprimer cette opération ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.deleteRow(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editLine(cbmarq:any){

    //console.log("editLine: ",cbmarq)
    this.sharedData.changeViewOperation(true);
    this.sharedData.sendIdOperation(cbmarq);

  }

}
