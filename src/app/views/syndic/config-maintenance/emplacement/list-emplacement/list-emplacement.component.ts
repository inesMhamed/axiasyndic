import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Emplacement } from 'src/app/shared/models/emplacement.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-emplacement',
  templateUrl: './list-emplacement.component.html',
  styleUrls: ['./list-emplacement.component.scss']
})
export class ListEmplacementComponent implements OnInit {


  
  dataSource = new MatTableDataSource<Emplacement>();
  displayedColumns: string[] = ['intitule','produit','parentemplacement' ,'action'];  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5; 
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;
  rechAvance:boolean=false;
  formGroupSearch:FormGroup;
  residences:any[]=[];
  emplacements:any[]=[{ value: 0, label: "Tous" }];



  constructor(private sharedData: SharedDataService, private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService, private toastr: ToastrService, private fb: FormBuilder, private spinnerservice: NgxSpinnerService,
    private residenceService: ResidenceService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.getList();
    this.getListProduit();

    this.formGroupSearch = this.fb.group({      
      produit: new FormControl(0, Validators.required),
      parent: new FormControl(0)
    });

    this.formGroupSearch.patchValue({
      produit:0,
      parent: 0
    });
  }

  
  ajouter() {
    this.spinnerservice.show()
    this.sharedData.changeViewEmplacement(true);
    this.sharedData.sendIdEmplacement(0);
    this.spinnerservice.hide()
  }

  getListProduit() {
    this.spinnerservice.show()
    let tous = { value: 0, label: "Tous" };

    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        ////console.log("this.residence: ",this.residences)
        this.residences.splice(0, 0, tous);
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }

  //list all emplacement (datasource)
  getList() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<Emplacement>();
    ////console.log("inn get list famille")

    this.configMaintenanceService.getListEmplacement().subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
        //this.emplacements= res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule +", "+clt.produit?.intitule }));

        ////console.log("datasource: ",this.dataSource.data)
      }
      this.spinnerservice.hide()
    })
  }

  OnChangeResidence(event) {
    this.spinnerservice.show()
    this.formGroupSearch.patchValue({ parent: 0 });
    this.emplacements = [{ value: 0, label: "Tous" }];
    let tous = { value: null, label: "Tous" };

    this.configMaintenanceService.getListEmplacementByProduit(event.value).subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()
        ////console.log("empl: ",res.data)
        this.emplacements = (res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule })));
        this.emplacements.splice(0, 0, tous);



      }
    }, error => { }, () => {
      this.spinnerservice.hide()


      ////console.log("emplacements: ",this.emplacements)
      ////console.log("formGroupSearch: ",this.formGroupSearch.value)

    })
  }

  delete(cbmarq: any) {
    this.spinnerservice.show()
    this.configMaintenanceService.deleteEmplacement(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.spinnerservice.hide()
          this.toastr.success('Emplacement a été supprimée avec succès !');
          this.getList();
        } else {
          this.spinnerservice.hide()
          this.toastr.error(res.message);
        }

      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un emplacement',
      text: 'Voulez-vous vraiment supprimer cet emplacement ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.delete(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  edit(cbmarq:any){

    ////console.log("editFamille: ",cbmarq)
    this.sharedData.changeViewEmplacement(true);
    this.sharedData.sendIdEmplacement(cbmarq);

  }

  onSubmitSearch() {
    this.spinnerservice.show()
    ////console.log("submit search: ",this.formGroupSearch.value)


    this.dataSource = new MatTableDataSource<any>();
    // //console.log("submit search: ",this.formGroupSearch.value)

    if (this.formGroupSearch.value.produit != 0) {
      this.configMaintenanceService.getEmplacementByFiltre(this.formGroupSearch.value).subscribe((res: any) => {

        if (res.statut) {
          this.spinnerservice.hide()
          ////console.log("res search: ",res.data)
          this.dataSource.data = res.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

          ////console.log("res filtre: ",this.dataSource.data)

          //this.formGroupSearch.reset();
        }
        this.spinnerservice.hide()
      })
    } else {
      this.spinnerservice.hide()
      this.getList();
    }



  }

}
