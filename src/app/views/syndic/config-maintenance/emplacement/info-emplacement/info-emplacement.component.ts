import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Emplacement } from 'src/app/shared/models/emplacement.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';

@Component({
  selector: 'app-info-emplacement',
  templateUrl: './info-emplacement.component.html',
  styleUrls: ['./info-emplacement.component.scss']
})
export class InfoEmplacementComponent implements OnInit {


  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  addeditForm: FormGroup;
  residences: any = [];
  cbmarq: any;
  selectedCat: any;
  codeCat: any;
  blocs: any = [];
  types: any = [];
  categorie: any = [];
  natures: any = [];
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  emplacementModel:any;
  emplacements:any[]=[];
  //existFamille:boolean=false;
  //residence:any[]=[];

  constructor(private sharedData: SharedDataService, private fb: FormBuilder, private permissionservice: DroitAccesService, private configMaintenanceService: ConfigMaintenanceService
    , private toastr: ToastrService, private cdRef: ChangeDetectorRef, private residenceService: ResidenceService,
    private spinnerservice: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.sharedData.sharedidEmplacement.subscribe(message => this.cbmarq = message);
    this.listeresidence();
    

    ////console.log("on init .. cbmarq info Famille: ",this.cbmarq)
    if(this.cbmarq!=0){
      this.getDetail();

    }


    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');

    this.addeditForm = this.fb.group({
      intitule: new FormControl("", Validators.required),
      produit: new FormControl(null, Validators.required),
      parent: new FormControl(null),

    });

  }


  
ngAfterViewChecked()
{
  ////console.log( "! changement de la familleEquipementModel du composant !" );
  this.cdRef.detectChanges();
}


  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }



  getEmplacementByProduit(event) {
    this.spinnerservice.show()
    this.emplacements = [];
    this.addeditForm.patchValue({ parent: null });

    this.configMaintenanceService.getListEmplacementByProduit(event.value).subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()
        this.emplacements = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule + ", " + clt.produit?.intitule }));

      }
    })

  }



  gotoList(){
    this.sharedData.changeViewEmplacement(false);

  }

  getDetail() {
    this.spinnerservice.show()
    this.configMaintenanceService.getEmplacementById(this.cbmarq).subscribe((res: any) => {

      if (res.statut) {
        this.emplacementModel = res.data;
        this.spinnerservice.hide()
        ////console.log("getDetails: ",this.addeditForm.value);
        this.getEmplacementByProduitInDetails(res.data.produit?.cbmarq);

      }
    })
  }



  getEmplacementByProduitInDetails(idProduit) {
    this.spinnerservice.show()
    this.emplacements = [];
    //this.addeditForm.patchValue({parent: null});
    ////console.log("idProduit: ",idProduit)

    this.configMaintenanceService.getListEmplacementByProduit(idProduit).subscribe((res: any) => {
      if (res.statut) {

        this.emplacements = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
        ////console.log("listEmpl: ",this.emplacements)

      }
    }, error => { }, () => {
      this.spinnerservice.hide()
      ////console.log("this.emplacementModel: ",this.emplacementModel)

      this.addeditForm.patchValue({
        intitule: this.emplacementModel.intitule,
        produit: this.emplacementModel.produit?.cbmarq,
        parent: this.emplacementModel.parent?.cbmarq


      })

  ////console.log("this.emplacementModel: ", this.addeditForm.value)


})

}


  onSubmit() {
    this.spinnerservice.show()
    this.submitted = true;



    if (this.addeditForm.value.parent == undefined) {
      this.spinnerservice.show()
      this.addeditForm.patchValue({
        parent: null
      })
    }


    //ajout
    if (this.cbmarq == 0) {
      this.spinnerservice.show()
      if (this.addeditForm.invalid) {
        this.spinnerservice.hide()
        return;
      }

      this.configMaintenanceService.addEmplacement(this.addeditForm.value).subscribe((res: any) => {
        if (res.statut) {
          this.toastr.success("Emplacement a été ajouté avec succès !", 'Success!', { progressBar: true })
          this.addeditForm.reset();
          this.sharedData.changeViewEmplacement(false);
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de l\'ajout  d\'un emplacement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
      

    } else { //modification

      if (this.addeditForm.invalid) {
        this.spinnerservice.hide()
        return;
      }

      ////console.log("update: ",this.addeditForm.value)

      this.configMaintenanceService.putEmplacement(this.addeditForm.value, this.cbmarq).subscribe((res: any) => {
        if (res.statut) {
          this.spinnerservice.hide()
          this.toastr.success("Emplacement a été modifié avec succès !", 'Success!', { progressBar: true })
          this.addeditForm.reset();
          this.sharedData.changeViewEmplacement(false);

        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de modification  d\'un emplacement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
    }

  }





}
