import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoEmplacementComponent } from './info-emplacement.component';

describe('InfoEmplacementComponent', () => {
  let component: InfoEmplacementComponent;
  let fixture: ComponentFixture<InfoEmplacementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoEmplacementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoEmplacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
