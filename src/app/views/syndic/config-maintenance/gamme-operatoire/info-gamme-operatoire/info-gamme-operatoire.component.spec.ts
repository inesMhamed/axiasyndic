import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoGammeOperatoireComponent } from './info-gamme-operatoire.component';

describe('InfoGammeOperatoireComponent', () => {
  let component: InfoGammeOperatoireComponent;
  let fixture: ComponentFixture<InfoGammeOperatoireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoGammeOperatoireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoGammeOperatoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
