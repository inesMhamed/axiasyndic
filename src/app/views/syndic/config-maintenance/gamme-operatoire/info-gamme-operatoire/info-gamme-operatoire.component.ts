import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';

@Component({
  selector: 'app-info-gamme-operatoire',
  templateUrl: './info-gamme-operatoire.component.html',
  styleUrls: ['./info-gamme-operatoire.component.scss']
})
export class InfoGammeOperatoireComponent implements OnInit {



  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  formGroup: FormGroup;
  residences: any = [];
  cbmarq: any;
  selectedCat: any;
  codeCat: any;
  blocs: any = [];
  types: any = [];
  categorie: any = [];
  natures: any = [];
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  currentModel:any;

  
  equipementList: any = [];

  constructor(private sharedData: SharedDataService, private fb: FormBuilder, private permissionservice: DroitAccesService, private configMaintenanceService: ConfigMaintenanceService
    , private toastr: ToastrService, private spinnerservice: NgxSpinnerService,) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.sharedData.sharedidGOperatoire.subscribe(message => this.cbmarq = message);
    this.getListEquipement();
    ////console.log("on init .. cbmarq info gamme operatoire: ",this.cbmarq)
    if (this.cbmarq != 0) {
      this.getDetail();
    }


    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');

    this.formGroup = this.fb.group({
      intitule: new FormControl('', [Validators.required]),
      //plusdetails: new FormControl(""),
      equipement: new FormControl('', [Validators.required]),

    });

  }

  getListEquipement() {
    this.spinnerservice.show()
    this.configMaintenanceService.getListEquipement().subscribe((res: any) => {
      if (res.statut) {

          ////console.log("getListEquipement res: ",res.data)

        this.equipementList = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule + ' - ' + (clt.produit?.intitule != undefined ? clt.produit?.intitule : '') + ', ' + (clt.emplacement?.parent?.intitule != undefined ? clt.emplacement?.parent?.intitule : '') + ', ' + (clt.emplacement?.intitule != undefined ? clt.emplacement?.intitule : '') }));
        this.spinnerservice.hide()
        ////console.log("getlist equipement: ", this.equipementList)
      }
    })
  }

  gotoListGammeOperatoire(){
    this.sharedData.changeViewGammeOperatoire(false);
  }


  getDetail() {
    this.spinnerservice.show()
    this.configMaintenanceService.getGOperatoireById(this.cbmarq).subscribe((res: any) => {

      if(res.statut){
        this.currentModel=res.data;

        this.formGroup.patchValue({
          intitule: this.currentModel.intitule,        
          equipement: this.currentModel.equipement?.cbmarq,
          //plusdetails:this.currentModel.plusdetails

        })
        this.spinnerservice.hide()
        ////console.log("currentModel: ",this.currentModel)
      }
    })
  }

  onSubmit() {
    this.spinnerservice.show()
    this.submitted = true;
    if (this.formGroup.invalid) {
      this.spinnerservice.hide()
      return;
    }

    ////console.log("submit: ",this.formGroup.value)
    //ajout
    if (this.cbmarq == 0) {
      this.spinnerservice.show()

      this.configMaintenanceService.addGammeOperatoire(this.formGroup.value).subscribe((res: any) => {
        if (res.statut) {
          this.toastr.success("Gamme Opératoire a été ajoutée avec succès !", 'Success!', { progressBar: true })
          this.formGroup.reset();
          this.sharedData.changeViewGammeOperatoire(false);
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de l\'ajout  d\'une gamme opératoire . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
      

    }else{ //modification
      this.configMaintenanceService.putGammeOperatoire(this.formGroup.value,this.cbmarq).subscribe((res:any)=>{
        if(res.statut){

          this.toastr.success("Gamme Opératoire modifié avec succès !", 'Success!', { progressBar: true })
          this.formGroup.reset();
          this.sharedData.changeViewGammeOperatoire(false);
          this.spinnerservice.hide()
        } else {
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          this.spinnerservice.hide()
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de modification  d\'une gamme opératoire . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
    }

  }

}
