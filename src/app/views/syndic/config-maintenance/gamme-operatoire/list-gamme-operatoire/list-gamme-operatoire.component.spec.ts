import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGammeOperatoireComponent } from './list-gamme-operatoire.component';

describe('ListGammeOperatoireComponent', () => {
  let component: ListGammeOperatoireComponent;
  let fixture: ComponentFixture<ListGammeOperatoireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListGammeOperatoireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGammeOperatoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
