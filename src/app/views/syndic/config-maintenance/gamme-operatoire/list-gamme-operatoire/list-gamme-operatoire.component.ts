import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-gamme-operatoire',
  templateUrl: './list-gamme-operatoire.component.html',
  styleUrls: ['./list-gamme-operatoire.component.scss']
})
export class ListGammeOperatoireComponent implements OnInit {

  
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['intitule' ,'equipement','produit','parent','emplacement','action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;


  formGroupSearch:FormGroup;
  blocs: any[]=[];
  residences: any[]=[];
  appartements: any[]=[];
  rechAvance:boolean=false;

  emplacements:any[]=[{ value: 0, label: "Tous" }];
  equipementList:any[]=[{ value: 0, label: "Tous" }];
  selectedEmplacementId :any;

  constructor(private sharedData: SharedDataService, private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService, private toastr: ToastrService, private fb: FormBuilder, private spinnerservice: NgxSpinnerService,
    private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.formGroupSearch = this.fb.group({
      
      produit:new FormControl(0, Validators.required),
      emplacement:new FormControl(0),
      equipement: new FormControl(0),


    });

    this.getList();
    this.listeresidence();

  }

  ajouterGammeOperatoire(){
    this.sharedData.changeViewGammeOperatoire(true)
    this.sharedData.sendIdGOperatoire(0);


  }

  
  listeresidence() {
    this.spinnerservice.show()
    this.residences = [];
    let tous = { value: 0, label: "Tous" };


    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        ////console.log("this.residence: ",this.residences)
        this.residences.splice(0, 0, tous);
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }

  
  OnChangeResidence(event: any) {
    this.spinnerservice.show()
    this.emplacements = [{ value: 0, label: "Tous" }];
    this.equipementList = [{ value: 0, label: "Tous" }];

    let tous={ value: 0, label: "Tous" };
    this.selectedEmplacementId=null;
    this.formGroupSearch.patchValue({emplacement: 0});
    this.formGroupSearch.patchValue({equipement: 0});


    this.configMaintenanceService.getEmplacementByFiltreTrier(event.value).subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()
        ////console.log("OnChangeResidence: ",res.data)
        this.emplacements = res.data.map(clt =>
          ( { value: clt.cbmarq, label: clt.intitule ,parent: clt.parent?.intitule ,idParent:clt.parent?.cbmarq })

        );
        this.spinnerservice.hide()
        this.emplacements.splice(0, 0, tous);


      }
    }, error => { }, () => {
      this.spinnerservice.hide()
      ////console.log("list emplacement: ",this.emplacements)



    })

  }

  onRemoveEmplacement(event){
    this.selectedEmplacementId=null;
  }

  OnChangeEmplacement(event){
    ////console.log("event: ",event);
    this.formGroupSearch.patchValue({equipement: 0});
    this.equipementList=[{ value: 0, label: "Tous" }];
    this.selectedEmplacementId=null;

    let label:string="";
    if(event?.label){
      label=event?.label;
    }else{
      label=event?.parent;
    }



    ////console.log("selected Label: ",label)



    let modelEmpl={
      produit: this.formGroupSearch.value.produit,
      intitule: label
    }

    ////console.log("modelEmpl: ",modelEmpl)
    if(event!=undefined && event!=null && event?.value!=0){

        this.configMaintenanceService.getEmplacementByLabelAndProduit(modelEmpl).subscribe((res:any)=>{
          if(res.statut){

            ////console.log("res data : ",res.data)

            this.selectedEmplacementId=res.data.cbmarq

            ////console.log("id empl : ",this.selectedEmplacementId)
          }
        },error=>{},()=>{
          this.getListEquipement();
        })

      }



  }

  getListEquipement() {
    this.spinnerservice.show()
    let formEqByEmpl = {
      produit: this.formGroupSearch.value.produit,
      emplacement:this.selectedEmplacementId,
    }

    let tous={ value: 0, label: "Tous" };

    this.configMaintenanceService.getEquipementsByEmplacement(formEqByEmpl).subscribe((res:any)=>{
        if(res.statut){

          ////console.log("getListEquipement res: ",res.data)

        this.equipementList = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.equipementList.splice(0, 0, tous);
        this.spinnerservice.hide()
        ////console.log("getlist equipement: ", this.equipementList)
      }
    })
  }



  getList() {
    this.spinnerservice.show()
    this.configMaintenanceService.getListGammeOperatoire().subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
        ////console.log("inn get list gamme",this.dataSource.data)

      }
      this.spinnerservice.hide()
    })
  }

  onSubmitSearch() {
    this.spinnerservice.show()
    ////console.log("submit search: ",this.formGroupSearch)

    this.dataSource=new MatTableDataSource<any>();

    let formSearch={
      produit: this.formGroupSearch.value.produit?this.formGroupSearch.value.produit:null,
      emplacement: this.selectedEmplacementId? this.selectedEmplacementId:null,
      equipement: this.formGroupSearch.value.equipement!=0?this.formGroupSearch.value.equipement:null

    }

    ////console.log("formSearch: ",formSearch)


    if(formSearch.produit!=null){
      this.configMaintenanceService.getGammeByFiltre(formSearch).subscribe((res:any)=>{

        if(res.statut){

          ////console.log("res search: ",res.data)
          this.dataSource.data=res.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

          ////console.log("res filtre: ",this.dataSource.data)
          this.spinnerservice.hide()
        }

      })
    } else {
      this.getList();
    }
  }

  deleteRow(cbmarq: any) {
    this.spinnerservice.show()
    this.configMaintenanceService.deleteGammeOperatoire(cbmarq).subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()
        this.toastr.success('Gamme Opératoire a été supprimée avec succès !');
        this.getList();
      } else {
        this.spinnerservice.hide()
        this.toastr.error(res.message);
      }
    },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une gamme opératoire',
      text: 'Voulez-vous vraiment supprimer cette gamme opératoire ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.spinnerservice.hide()
        this.deleteRow(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editLine(cbmarq: any) {

    ////console.log("editLine: ",cbmarq)
    this.sharedData.changeViewGammeOperatoire(true);
    this.sharedData.sendIdGOperatoire(cbmarq);

  }

}
