import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Equipement } from 'src/app/shared/models/configMaintenanceModel/equipement.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-equipement',
  templateUrl: './list-equipement.component.html',
  styleUrls: ['./list-equipement.component.scss']
})
export class ListEquipementComponent implements OnInit {

  dataSource = new MatTableDataSource<Equipement>();
  displayedColumns: string[] = ['intitule' ,'famille', 'quantite','produit','parent', 'emplacement','action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;
  familleList:any[]=[{ value: 0, label: "Tous" }];
  emplacements:any[]=[{ value: 0, label: "Tous" }];
  residences: any[]=[{ value: 0, label: "Tous" }];

  searchModel:any;
  formGroupSearch:FormGroup;
  blocs: any[]=[];
  appartements: any[]=[];
  rechAvance:boolean=false;
  emplacementsparent:any[]=[];
  selectedEmplacementId :any;


  constructor(private sharedData: SharedDataService, private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService, private toastr: ToastrService, private fb: FormBuilder, private spinnerservice: NgxSpinnerService,
    private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.formGroupSearch = this.fb.group({
      
      famille: new FormControl(0, Validators.required),
      produit:new FormControl(0),
      emplacement:new FormControl(0),

    });

    this.getListEquipement();
    this.getListFamilleEquipement();
    this.listeresidence();

  }

  ajouterEquipement() {
    this.sharedData.changeViewEquipement(true)
    this.sharedData.sendIdEquipement(0);
  
  }

  listeresidence() {
    this.spinnerservice.show()
    this.residences = [];
    let tous = { value: 0, label: "Tous" };


    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        ////console.log("this.residence: ",this.residences)
        this.residences.splice(0, 0, tous);
        this.spinnerservice.hide()
      }

    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }

  OnChangeFamille(event){

    this.formGroupSearch.patchValue({
      produit: 0,
      emplacement:0
    });
    this.emplacements=[{ value: 0, label: "Tous" }];

    ////console.log("OnChangeFamille: ",this.formGroupSearch.value)


  }

  OnChangeResidence(event: any) {
    this.spinnerservice.show()
    this.formGroupSearch.patchValue({ emplacement: 0 });
    this.emplacements = [{ value: 0, label: "Tous" }];
    let tous = { value: 0, label: "Tous" };
    this.selectedEmplacementId = null;


    this.configMaintenanceService.getEmplacementByFiltreTrier(event?.value).subscribe((res: any) => {
      if (res.statut) {
        this.spinnerservice.hide()
        ////console.log("OnChangeResidence: ",res.data)
        this.emplacements = res.data.map(clt =>
          ( { value: clt.cbmarq, label: clt.intitule ,parent: clt.parent?.intitule ,idParent:clt.parent?.cbmarq })

        );
        this.emplacements.splice(0, 0, tous);
        this.spinnerservice.hide()


      }
    }, error => { }, () => {
      this.spinnerservice.hide()

    })

  }

  onRemoveEmplacement(event){
    this.selectedEmplacementId=null;
  }

  OnChangeEmplacement(event){
    ////console.log("event: ",event);

    let label:string="";
    if(event?.label){
      label=event?.label;
    }else{
      label=event?.parent;
    }



    ////console.log("selected Label: ",label)



    let modelEmpl={
      produit: this.formGroupSearch.value.produit,
      intitule: label
    }

    ////console.log("modelEmpl: ",modelEmpl)
    if (event != undefined && event != null && event?.value != 0) {
      this.spinnerservice.hide()

      this.configMaintenanceService.getEmplacementByLabelAndProduit(modelEmpl).subscribe((res: any) => {
        if (res.statut) {
          this.spinnerservice.hide()
          ////console.log("res data : ",res.data)

          this.selectedEmplacementId = res?.data?.cbmarq

          //console.log("id empl : ",res.data)
        }
      })
    }

  }
  compareParent = (item, selected) => {

    ////console.log("item: ",item, "   Vs  ",selected)
    if (selected.parent && item.parent) {
        return item.parent === selected.parent;
    }
    if (item.label && selected.label) {
        return item.label === selected.label;
    }
    return false;
};



  onSubmitSearch() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<Equipement>();
    //console.log("submit search: ",this.formGroupSearch.value)

    let formSearch={
      famille: this.formGroupSearch.value.famille!=0?this.formGroupSearch.value.famille:0,
      produit: this.formGroupSearch.value.produit?this.formGroupSearch.value.produit:0,
      emplacement: this.selectedEmplacementId? this.selectedEmplacementId:0
    }
    

    if(formSearch.famille!=0){
      this.configMaintenanceService.getEquipementsByFiltre(formSearch).subscribe((res:any)=>{

        if(res.statut){

          ////console.log("res search: ",res.data)
          this.dataSource.data = res.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.spinnerservice.hide()
          ////console.log("res filtre: ",this.dataSource.data)

        }

      })
    } else {
      this.getListEquipement();
    }


  }

  getListEquipement() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<Equipement>();

    this.configMaintenanceService.getListEquipement().subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
        //console.log("getlist equipement: ", this.dataSource.data)
      }
    })
  }

  getListFamilleEquipement() {
    this.spinnerservice.show()
    this.familleList = [];
    let tous = { value: 0, label: "Tous" };

    this.configMaintenanceService.getListFamilleEquipement().subscribe((res: any) => {
      if (res.statut) {

        if (res.data != null) {
          this.familleList = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
          ////console.log("this.residence: ",this.residences)
          this.familleList.splice(0, 0, tous);

        } this.spinnerservice.hide()

      }


    })
  }


  deleteRow(cbmarq: any) {this.spinnerservice.hide()
    this.configMaintenanceService.deleteEquipement(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Equipement a été  supprimé avec succès !');
          this.getListEquipement();
        } else {
          this.spinnerservice.hide()
          this.toastr.error(res.message);
        }

      },
      error => {
        this.spinnerservice.hide()
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un équipement',
      text: 'Voulez-vous vraiment supprimer cet équipement ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.deleteRow(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editLine(cbmarq:any){

    ////console.log("editLine: ",cbmarq)
    this.sharedData.changeViewEquipement(true);
    this.sharedData.sendIdEquipement(cbmarq);

  }

  downloadAttachement(attach){
    if(attach){
      window.open(attach, '_blank');
    }


  }

}
