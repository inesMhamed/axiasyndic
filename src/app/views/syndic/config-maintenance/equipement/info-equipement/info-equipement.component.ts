import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Equipement } from 'src/app/shared/models/configMaintenanceModel/equipement.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { BlocService } from 'src/app/shared/services/bloc.service';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';

@Component({
  selector: 'app-info-equipement',
  templateUrl: './info-equipement.component.html',
  styleUrls: ['./info-equipement.component.scss']
})
export class InfoEquipementComponent implements OnInit {


  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  formGroup: FormGroup;
  cbmarq: any;
  types: any = []; 
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  familleList:any[]=[];

  emplacements: any[]=[];
  residences: any[]=[];
  sousemplacements: any[]=[];

  
  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File = null;
  fileName:string;

  equipementModel:any;//Equipement=new Equipement();
  negatifValueQte:boolean=false;
  checkedGenEquipement:boolean=false;
  viewModal=false;
  selectedEmplacementId :any=null;


  constructor(private sharedData: SharedDataService, private fb: FormBuilder, private permissionservice: DroitAccesService, private configMaintenanceService: ConfigMaintenanceService
    , private toastr: ToastrService, private residenceService: ResidenceService, private blocService: BlocService, private appartementService: AppartementService, private cdRef: ChangeDetectorRef,
    private modalService: NgbModal,
    private spinnerservice: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.sharedData.sharedidEquipement.subscribe(message => this.cbmarq = message);

    this.listeresidence();
    this.getListFamilleEquipement();

    if (this.cbmarq != 0) {
      this.getDetail();
    }


    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');

    this.formGroup = this.fb.group({
      intitule: new FormControl("", Validators.required),
      //description: new FormControl(""),
      famille: new FormControl(null, Validators.required),
      produit:new FormControl(null, Validators.required),
      emplacement:new FormControl(null),    
      attachement: new FormControl(null),
      quantite:new FormControl(null, Validators.required),
      generer:new FormControl(2),

    });

  }

  ngAfterViewChecked()
{

  this.cdRef.detectChanges();
  }

  infoQte(typeModal){

    let ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      keyboard : false
};

  // { ariaLabelledBy: 'modal-basic-title' },
      this.modalService.open(typeModal,ngbModalOptions)
      .result.then((result) => {
        //console.log("result",result);
      }, (reason) => {
        //console.log('Err!', reason);
      });

    

  }

  onChangeSwitch(){
    this.checkedGenEquipement=!this.checkedGenEquipement;
  }

  getListFamilleEquipement() {
    this.spinnerservice.show()
    this.configMaintenanceService.getListFamilleEquipement().subscribe((res: any) => {
      if (res.statut) {

        if (res.data != null) {
          this.spinnerservice.hide()
          this.familleList = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
          ////console.log("this.residence: ",this.residences)
        }

      }


    })
  }

  /*** bloc /residence/ appartement fonctionnalité */

  listeresidence() {
    this.spinnerservice.show()
    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        this.spinnerservice.hide()
      }

    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }
    );
  }


  OnChangeEmplacement(event){
    ////console.log("event: ",event);

    let label:string="";
    if(event.label){ 
      label=event?.label; 
    }else{
      label=event.parent;
    }
    let modelEmpl = {
      produit: this.formGroup.value.produit,
      intitule: label
    }

    ////console.log("modelEmpl: ",modelEmpl)

    this.configMaintenanceService.getEmplacementByLabelAndProduit(modelEmpl).subscribe((res: any) => {
      if (res.statut) {
        this.selectedEmplacementId = res.data.cbmarq
        this.spinnerservice.hide()
        ////console.log("id empl : ",res.data)
      }
    })


   
  }


  OnChangeResidence(event: any) {
    this.spinnerservice.show()
    this.emplacements = [];

    if (event!=null && event.value) {
          

            let formSearch={
              produit: event.value,
              parent: null
            }

            this.configMaintenanceService.getEmplacementByFiltreTrier(event.value).subscribe((res: any) => {

              if (res.data) {

                ////console.log("on change residence: ",res.data)
                        this.emplacements = this.emplacements.concat(res.data.map(clt =>
                           ({ value: clt.cbmarq, label: clt.intitule ,parent: clt.parent?.intitule })));

          this.spinnerservice.hide()

        }
      },
        error => {
          this.spinnerservice.hide()
        }, () => {
          this.spinnerservice.hide()
          this.formGroup.patchValue({ emplacement: null });

        }
      );

    }

  }

  onChangeResidenceInDetail(){
    //console.log("OnChangeResidence in details: ",this.equipementModel)
    this.spinnerservice.show()
    let formSearch = {
      produit: this.equipementModel.produit?.cbmarq,
      parent: null
    }

    this.configMaintenanceService.getEmplacementByFiltre(formSearch).subscribe((res: any) => {

      if (res.data) {

        ////console.log("getEmplacementByFiltre : ",res.data )

           this.emplacements = this.emplacements.concat(res.data.map(clt =>
          ({ value: clt.cbmarq, label: clt.intitule ,parent: clt.parent?.intitule })));

        this.spinnerservice.hide()

      }
    },
      error => {
        this.spinnerservice.hide()
        //console.log( 'erreur: ', error);
      }, () => {
        this.spinnerservice.hide()
        this.formGroup.patchValue({
          intitule: this.equipementModel.intitule,
          quantite: this.equipementModel.quantite,
          produit: this.equipementModel.produit?.cbmarq,
          emplacement: this.equipementModel.emplacement != null ? this.equipementModel.emplacement?.cbmarq : null,
          famille: this.equipementModel.famille?.cbmarq,
          attachement: this.equipementModel.attachement
        })


        this.selectedEmplacementId = this.equipementModel.emplacement != null ? this.equipementModel.emplacement?.cbmarq : null


        ////console.log("formGrp: ",  this.formGroup.value)

      }
    );

  }


/*** end fct residence/bloc/appartement  **/

  gotoListEquipement(){
    this.sharedData.changeViewEquipement(false)

  }


  getDetail() {
    this.spinnerservice.show()
    this.configMaintenanceService.getEquipementById(this.cbmarq).subscribe((res: any) => {

      if (res.statut) {
        this.equipementModel = res.data;
        //console.log("getDetail: ",res.data)
        this.onChangeResidenceInDetail();
        this.spinnerservice.hide()

      }
    }, error => { }, () => {
      this.spinnerservice.hide()
      // extract filename of link //
      let file: string = "" + (this.equipementModel.attachement != null ? this.equipementModel.attachement : "");
      this.fileName = file ? file.substring(file.lastIndexOf('/') + 1) : "";
      ////console.log("get fileName: ", this.fileName)


      this.labelImport.nativeElement.innerText = this.fileName;

      ////console.log("labelImport: ",this.labelImport)

      this.spinnerservice.hide()

    })
  }

  onSubmit(){

    this.submitted=true;
    if(this.formGroup.invalid || this.negatifValueQte){
      return;
    }



    this.formGroup.patchValue({
      emplacement:  this.selectedEmplacementId,
    })
    
    //console.log("submit: ",this.formGroup.value);
   

    //ajout
    if (this.cbmarq == 0) {
      this.spinnerservice.show()
      const myFormValue = this.formGroup.value;
      const myFormData = new FormData();
      Object.keys(myFormValue).forEach(name => {
        myFormData.append(name, myFormValue[name]);
      });


      this.configMaintenanceService.addEquipement(myFormData).subscribe((res: any) => {
        if (res.statut) {
          this.toastr.success("Equipement a été ajouté avec succès !", 'Success!', { progressBar: true })
          this.formGroup.reset();
          this.sharedData.changeViewEquipement(false);
          this.spinnerservice.hide()
        } else {
          this.submitted = false;
          this.spinnerservice.hide()
          this.toastr.error(res.message, 'Erreur d\'ajout!', { progressBar: true });
        }
      }, error => {
        this.toastr.error('Erreur lors de l\'ajout  d\'un équipement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })


    } else { //modification

      if (this.fileToUpload != null) {
        this.formGroup.value.attachement = this.fileToUpload; //file on update

      }

      const myFormValue = this.formGroup.value;//this.equipementModel;
      const myFormData = new FormData();
      Object.keys(myFormValue).forEach(name => {
        myFormData.append(name, myFormValue[name]);
      });

      ////console.log("cbmarq: ",this.cbmarq," -- update: ",this.formGroup.value)

      this.configMaintenanceService.putEquipement(myFormData, this.cbmarq).subscribe((res: any) => {
        if (res.statut) {

          this.toastr.success("Equipement a été modifié avec succès !", 'Success!', { progressBar: true })
          this.formGroup.reset();
          this.sharedData.changeViewEquipement(false);
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur de modification!', { progressBar: true });
        }
      }, error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de modification  d\'un équipement . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
    }

  }

  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUpload = files.item(0);

    ////console.log("fileUp: ",this.fileToUpload)
    ////console.log("formGroup: ",this.formGroup.value)

    if (this.fileToUpload == null) {
      this.fileName = this.formGroup.value.attachement ? this.formGroup.value.attachement?.name : "";
      this.labelImport.nativeElement.innerText = this.fileName;
    }
    if (files.length > 0) {
      const file = files[0];
      this.formGroup.patchValue({
        attachement: file
      });
    }




  }

  onchangeValueQte(event){

     ////console.log("onChangeValueDate: ",this.formGroup.value.date)
     if(this.formGroup.value.quantite>1 || this.formGroup.value.quantite==null){
      this.negatifValueQte=false;

    }else if(this.formGroup.value.quantite<1 && this.formGroup.value.quantite!=null){
      this.negatifValueQte=true;
    }

  }

  confirmerModal(){
    //console.log("confirmer modal: notice génération auto",this.checkedGenEquipement)


    if(this.checkedGenEquipement){
      this.formGroup.patchValue({
        generer: 1
      })
    }else{
      this.formGroup.patchValue({
        generer: 2
      })
    }


    //console.log("formGroup: ", this.formGroup)


    this.modalService.dismissAll();
  }

}
