import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoEquipementComponent } from './info-equipement.component';

describe('InfoEquipementComponent', () => {
  let component: InfoEquipementComponent;
  let fixture: ComponentFixture<InfoEquipementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoEquipementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoEquipementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
