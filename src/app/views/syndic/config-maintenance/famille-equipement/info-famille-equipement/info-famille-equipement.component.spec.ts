import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoFamilleEquipementComponent } from './info-famille-equipement.component';

describe('InfoFamilleEquipementComponent', () => {
  let component: InfoFamilleEquipementComponent;
  let fixture: ComponentFixture<InfoFamilleEquipementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoFamilleEquipementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoFamilleEquipementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
