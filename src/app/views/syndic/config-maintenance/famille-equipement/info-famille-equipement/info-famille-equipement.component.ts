import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FamilleEquipement } from 'src/app/shared/models/configMaintenanceModel/familleEquipement.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';

@Component({
  selector: 'app-info-famille-equipement',
  templateUrl: './info-famille-equipement.component.html',
  styleUrls: ['./info-famille-equipement.component.scss']
})
export class InfoFamilleEquipementComponent implements OnInit {

  loading: boolean;
  editForm: boolean = false
  submitted: boolean;
  familleForm: FormGroup;
  residences: any = [];
  cbmarq: any;
  selectedCat: any;
  codeCat: any;
  blocs: any = [];
  types: any = [];
  categorie: any = [];
  natures: any = [];
  can_add: Permission;
  can_edit: Permission;
  id_current_user: any;
  familleEquipementModel:FamilleEquipement=new FamilleEquipement();
  existFamille:boolean=false;
  familleList:any[]=[];

  constructor(private sharedData: SharedDataService,private fb: FormBuilder,private permissionservice: DroitAccesService,private configMaintenanceService: ConfigMaintenanceService
    ,private toastr: ToastrService,private cdRef:ChangeDetectorRef, private spinnerservice:NgxSpinnerService) {
   
  }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.sharedData.sharedidFamilleEquipement.subscribe(message => this.cbmarq = message);
    this.getListFamilleEquipement();

    ////console.log("on init .. cbmarq info Famille: ",this.cbmarq)
    if(this.cbmarq!=0){
      this.getDetailFamille();

    }


    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');

    this.familleForm = this.fb.group({
      intitule: new FormControl("", Validators.required),
      //description: new FormControl(null, Validators.required),
      //emplacement: new FormControl("",),

    });

  }

ngAfterViewChecked()
{
  ////console.log( "! changement de la familleEquipementModel du composant !" );
  this.cdRef.detectChanges();
}

onchangeValueFamille(event){
  let familles:any[]=[];

  familles=this.familleList.filter(item=>item.intitule.toLowerCase()===event.toLowerCase());
  ////console.log("res familleList: ",familles)

  if(familles.length>0){
    this.existFamille=true;
  }else{
    this.existFamille=false;
  }
 

  ////console.log("final exist famille: ",this.existFamille)
}


getListFamilleEquipement(){
  this.spinnerservice.show()
  ////console.log("inn get list famille")

  this.configMaintenanceService.getListFamilleEquipement().subscribe((res:any)=>{
      if(res.statut){
        this.familleList=res.data;      
        this.spinnerservice.hide()
        //console.log("datasource: ",this.familleList)
      }
  })
}

  gotoListFamille(){
    this.sharedData.changeViewFamille(false);

  }

  getDetailFamille(){
    this.spinnerservice.show()
    this.configMaintenanceService.getFamilleById(this.cbmarq).subscribe((res:any)=>{

      if(res.statut){
        this.familleEquipementModel=res.data;
        this.spinnerservice.hide()
      }
    })
  }

  onSubmit(){
    this.spinnerservice.show()
    this.submitted=true;
   

    ////console.log("submit: ",this.familleForm.value)
    //ajout
    if(this.cbmarq==0){

      if(this.familleForm.invalid || this.existFamille){
        this.spinnerservice.hide()
        return;
      }

     
      this.configMaintenanceService.addFamilleEquipement(this.familleForm.value).subscribe((res:any)=>{
        if(res.statut){
          this.spinnerservice.hide()
          this.toastr.success("Famille a été ajoutée avec succès !", 'Success!', { progressBar: true })
          this.familleForm.reset();
          this.sharedData.changeViewFamille(false);
        }else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de l\'ajout  d\'une famille . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
      

    }else{ //modification

      if(this.familleForm.invalid){
        this.spinnerservice.hide()
        return;
      }
      this.configMaintenanceService.putFamilleEquipement(this.familleForm.value,this.cbmarq).subscribe((res:any)=>{
        if(res.statut){
          this.spinnerservice.hide()
          this.toastr.success("Famille a été modifiée avec succès !", 'Success!', { progressBar: true })
          this.familleForm.reset();
          this.sharedData.changeViewFamille(false);
          
        }else {
          this.spinnerservice.hide()
          this.submitted = false;
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
      },error => {
        this.spinnerservice.hide()
        this.toastr.error('Erreur lors de modification  d\'une famille . Veuillez réessayer !', 'Erreur!', { progressBar: true })
        //console.log('erreur: ', error);
      })
    }

  }

}
