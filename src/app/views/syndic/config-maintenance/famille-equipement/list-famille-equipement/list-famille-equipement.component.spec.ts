import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFamilleEquipementComponent } from './list-famille-equipement.component';

describe('ListFamilleEquipementComponent', () => {
  let component: ListFamilleEquipementComponent;
  let fixture: ComponentFixture<ListFamilleEquipementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListFamilleEquipementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFamilleEquipementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
