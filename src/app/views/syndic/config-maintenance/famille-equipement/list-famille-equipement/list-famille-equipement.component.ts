import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FamilleEquipement } from 'src/app/shared/models/configMaintenanceModel/familleEquipement.model';
import { Permission } from 'src/app/shared/models/permission.model';
import { ConfigMaintenanceService } from 'src/app/shared/services/config-maintenance.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-famille-equipement',
  templateUrl: './list-famille-equipement.component.html',
  styleUrls: ['./list-famille-equipement.component.scss']
})
export class ListFamilleEquipementComponent implements OnInit {

  dataSource = new MatTableDataSource<FamilleEquipement>();
  //displayedColumns: string[] = ['intitule'  ,'description','action'];  
  displayedColumns: string[] = ['intitule' ,'action'];  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5; 
  pageSizeOptions: number[] = [5, 10, 25, 100];

  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;


  constructor(private sharedData: SharedDataService, private configMaintenanceService: ConfigMaintenanceService, private preferenceService: PreferencesService,
    private permissionservice: DroitAccesService, private toastr: ToastrService, private spinnerservice: NgxSpinnerService,) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.getListFamilleEquipement();
  }

  
  ajouterFamille() {

    this.sharedData.changeViewFamille(true);
    this.sharedData.sendIdFamille(0);
  
  }

  getListFamilleEquipement() {
    this.spinnerservice.show()
    this.dataSource = new MatTableDataSource<FamilleEquipement>();
    ////console.log("inn get list famille")

    this.configMaintenanceService.getListFamilleEquipement().subscribe((res: any) => {
      if (res.statut) {
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerservice.hide()
        ////console.log("datasource: ",this.dataSource)
      }else{
        this.spinnerservice.hide()
      }
    })
  }

  deleteFamilleEquipement(cbmarq: any) {
    this.spinnerservice.show()
    this.configMaintenanceService.deleteFamilleEquipement(cbmarq).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Famille a été supprimée avec succès !');
          this.getListFamilleEquipement();
        } else {
          this.spinnerservice.hide()
          this.toastr.error(res.message);
        }

      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une famille',
      text: 'Voulez-vous vraiment supprimer cette famille ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.deleteFamilleEquipement(cbmarq);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editFamille(cbmarq:any){

    ////console.log("editFamille: ",cbmarq)
    this.sharedData.changeViewFamille(true);
    this.sharedData.sendIdFamille(cbmarq);

  }


}
