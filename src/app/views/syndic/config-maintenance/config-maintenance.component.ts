import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Permission } from 'src/app/shared/models/permission.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.services';

@Component({
  selector: 'app-config-maintenance',
  templateUrl: './config-maintenance.component.html',
  styleUrls: ['./config-maintenance.component.scss']
})
export class ConfigMaintenanceComponent implements OnInit {


  viewListFamilleEquipement:boolean=false; //view=false ;add=false
  viewListEquipement:boolean=false;
  viewListGammeOperatoire:boolean=false;
  viewListOperation:boolean=false; 
  viewListEmplacement:boolean=false;
  viewListSousEmplacement:boolean=false;

  idFamilleEquipement:number=0;
  idEquipement:number=0;
  idGOperatoire:number=0;
  idOperation:number=0;
  idEmplacement:number=0;
  idSousEmplacement:number=0;

  can_viewFamille: Permission=new Permission();
  can_ViewEmplacement: Permission=new Permission();
  can_ViewEquipement: Permission=new Permission();
  can_ViewGamme: Permission=new Permission();
  can_ViewOperation: Permission=new Permission();
  id_current_user: any;

  
  have_access:boolean =false;

  constructor(private sharedData: SharedDataService, private permissionservice: DroitAccesService,
    private spinnerservice: NgxSpinnerService, private router: Router) {

    this.id_current_user = localStorage.getItem('id');
    this.can_viewFamille = this.permissionservice.search( this.id_current_user, "FN21000115");
    this.can_ViewEmplacement = this.permissionservice.search( this.id_current_user, 'FN21000116');
    this.can_ViewEquipement = this.permissionservice.search( this.id_current_user, 'FN21000117');
    this.can_ViewGamme = this.permissionservice.search( this.id_current_user, 'FN21000118');
    this.can_ViewOperation = this.permissionservice.search( this.id_current_user, 'FN21000119');

  }

  ngOnInit(): void {

    this.spinnerservice.show()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000114').subscribe((res: any) => {

      this.have_access = res.data;
      // this.spinnerservice.hide()
    }, error => { }, () => {
      // this.spinnerservice.hide()
      if (!this.have_access) {
        this.router.navigate(['403']);
      }else{
        
        
        this.sharedData.sharedViewFamille.subscribe(message => this.viewListFamilleEquipement = message)
        this.sharedData.sharedViewEquipement.subscribe(message => this.viewListEquipement = message)
        this.sharedData.sharedViewGammeOperatoire.subscribe(message => this.viewListGammeOperatoire = message)
        this.sharedData.sharedViewOperation.subscribe(message => this.viewListOperation = message)
        this.sharedData.sharedViewEmplacement.subscribe(message => this.viewListEmplacement = message)
        this.sharedData.sharedViewSousEmplacement.subscribe(message => this.viewListSousEmplacement = message)
    
    
        this.sharedData.sharedidFamilleEquipement.subscribe(message => this.idFamilleEquipement = message)
        this.sharedData.sharedidEquipement.subscribe(message => this.idEquipement = message)
        this.sharedData.sharedidGOperatoire.subscribe(message => this.idGOperatoire = message)
        this.sharedData.sharedidOperation.subscribe(message => this.idOperation = message)
        this.sharedData.sharedidEmplacement.subscribe(message => this.idEmplacement = message)
        this.sharedData.sharedidSousEmplacement.subscribe(message => this.idSousEmplacement = message)

      }
     
    });


  




  }

  
  
}
