import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigMaintenanceComponent } from './config-maintenance.component';

describe('ConfigMaintenanceComponent', () => {
  let component: ConfigMaintenanceComponent;
  let fixture: ComponentFixture<ConfigMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigMaintenanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
