import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupementComponent } from './edit-groupement.component';

describe('EditGroupementComponent', () => {
  let component: EditGroupementComponent;
  let fixture: ComponentFixture<EditGroupementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditGroupementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
