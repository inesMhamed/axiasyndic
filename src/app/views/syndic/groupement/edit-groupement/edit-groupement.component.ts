import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CaisseService} from '../../../../shared/services/caisse.service';
import {GroupementService} from '../../../../shared/services/groupement.service';
import {first} from 'rxjs/operators';
import {Groupement} from '../../../../shared/models/groupement.model';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-groupement',
  templateUrl: './edit-groupement.component.html',
  styleUrls: ['./edit-groupement.component.scss']
})
export class EditGroupementComponent implements OnInit {

  @ViewChild('labelImport')
  labelImport: ElementRef;
  formBasic: FormGroup;
  loading: boolean;
  submitted: boolean;
  listecaisse: any[] = []; //= [{ value: -1, label: 'Générer une nouvelle caisse'}];
  listecaisseSiege: any[] = [];
  listeresidence: any[];
  selected: any[];
  id: string;
  can_edit: Permission;
  id_current_user: any;

  constructor(
      private fb: FormBuilder,
      private toastr: ToastrService,
      private residenceService: ResidenceService,
      private router: Router,
      private caisseService: CaisseService,
      private groupementService: GroupementService,
      private actRoute: ActivatedRoute ,
      private spinnerservice: NgxSpinnerService,
      private permissionservice: DroitAccesService
  ) {

  }

  ngOnInit() {
    this.spinnerservice.show()
    this.actRoute.paramMap.subscribe(params => {
      this.id = params.get('cbmarq');
    });
    this.submitted = false;
    this.id_current_user = localStorage.getItem('id');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000013');
    this.getlistCaisses();
    this.getlistCaissesSiege();
    this.getlistResidences();
    this.buildFormBasic();
    if (this.id) {
      this.groupementService.getGroupement(this.id).pipe(first())
          .subscribe((x: any) => {
            this.spinnerservice.hide()
            var ent: Groupement = x.data ;
            this.formBasic.patchValue(ent);
            //console.log('x.data', x.data);
            if (ent['caisse']) {
              this.formBasic.patchValue({caisse: ent['caisse'].cbmarq});
              this.formBasic.patchValue({caissesiege: ent['caisse'].caisseSiege.cbmarq});
            }
            this.formBasic.patchValue({residences: ent['residences']});
          });
    }
  }

  getlistCaisses() {
    this.spinnerservice.hide()
    // this.listecaisse.unshift({value: 0, label: 'Générer caisse groupement'});
    this.caisseService.getCaisses().subscribe(
        (res: any) => {
          this.listecaisse = res.data.map(c => ({ value: c.cbmarq, label: c.intitule  , siege: c.caisseSiege?.cbmarq }));
          this.spinnerservice.hide()
        }
    );
  }
  getlistCaissesSiege() {
    this.spinnerservice.show()
    this.caisseService.getCaisseSiege().subscribe(
        (res: any) => {

          this.listecaisseSiege = res.data.map(c => ({value: c.cbmarq, label: c.intitule}));
          this.spinnerservice.hide()
        }
    );
  }

  onchangeCaisse(event) {
    //console.log('', event);
    this.formBasic.patchValue({
      caissesiege: event.siege
    });
  }
  getlistResidences() {
    this.spinnerservice.show()
    this.residenceService.getResidencesNoCroupe().subscribe(
        (res: any) => {
          this.listeresidence = res.data?.map(c => ({ cbmarq: c.cbmarq, label: c.intitule }));
          this.spinnerservice.hide()
        }
    );
  }
  buildFormBasic() {
    this.formBasic = this.fb.group({
      intitule: new FormControl(null, Validators.required),
      president: new FormControl( null, ),
      vice: new FormControl( null, ),
      tresorerie: new FormControl(null, ),
      residences: new FormControl(null, [Validators.required]),
      caisse: new FormControl(null, Validators.required),
      caissesiege: new FormControl(null, Validators.required ),
    });
  }


  submit() {
    this.spinnerservice.show()
    this.loading = true;
    if (this.formBasic.invalid) {
      this.submitted = true;
      return;
    }
    this.formBasic.value.residences.forEach(i => {
      delete i.label;
    });
    if (this.can_edit?.permission) {
      this.groupementService.putGroupement(this.formBasic.value, this.id).pipe(first()).subscribe((res: any) => {
            if (res.statut === true) {
              this.toastr.success('Groupement a été modifié avec succès !', 'Success!', {progressBar: true});
              this.router.navigate(['/syndic/groupements']);
              this.submitted = false;
              this.formBasic.reset();
              this.spinnerservice.hide()
            } else {
              this.spinnerservice.hide()
              this.submitted = true;
              this.toastr.error(res.message, 'Erreur!', {progressBar: true});
            }
          },
          (error) => {
            this.spinnerservice.hide()
            this.toastr.error('Erreur lors de l\'ajout d\'un groupement . Veuillez réessayer !', 'Erreur!', {progressBar: true});
          });
    } else {
      this.spinnerservice.hide()
      this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
    }
  }
}
