import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ResidenceService } from '../../../../shared/services/residence.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CaisseService } from '../../../../shared/services/caisse.service';
import { GroupementService } from '../../../../shared/services/groupement.service';
import { first } from 'rxjs/operators';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-add-groupement',
    templateUrl: './add-groupement.component.html',
    styleUrls: ['./add-groupement.component.scss']
})
export class AddGroupementComponent implements OnInit {

    @ViewChild('labelImport')
    labelImport: ElementRef;
    formBasic: FormGroup;
    loading: boolean;
    submitted: boolean;
    listecaisseSiege: any[] = [];
    listecaisse: any[] = [];  //= [{ value: -1, label: 'Générer une nouvelle caisse'}];
    listeresidence: any[] = [];
    selected: any[]=[];
    selected1: any[]=[];
    can_add: Permission;
    can_edit: Permission;
    id_current_user: any;
    listeprops: any[];
    cbmarq: string;
    modeEdit: any;
    listSelectedDelegue:any[]=[];

    have_access:boolean =false;

    constructor(
        private fb: FormBuilder,
        private toastr: ToastrService,
        private residenceService: ResidenceService,
        private proprietaireService: ProprietaireService,
        private router: Router,
        private route: ActivatedRoute, private spinnerservice: NgxSpinnerService,
        private caisseService: CaisseService,
        private groupementService: GroupementService,
        private permissionservice: DroitAccesService,
    ) {

    }

    ngOnInit() {
        this.spinnerservice.show()
        this.route.paramMap.subscribe(params => {
            this.cbmarq = params.get('cbmarq');
        });

        this.submitted = false;
        this.id_current_user = localStorage.getItem('id');
        this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000012');
        this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000013');

        this.buildFormBasic();
        this.checkAccessADDEDIT();

        this.getlistCaisses();
        this.getlistCaissesSiege();

        if (this.cbmarq) {
            this.spinnerservice.show()
            this.getlistResidencesNoGroupe();
            this.getlistResidencesByGroupe();

            this.modeEdit = true;
            this.groupementService.getGroupement(this.cbmarq).subscribe((res: any) => {

                if (res.statut) {

                    this.formBasic.patchValue({

                        intitule: res.data.intitule,
                        delegues: res.data.delegues,
                        residences: res.data.residences,
                        caisse: res?.data?.caisse?.cbmarq,
                        caisseSiege: res?.data?.caisse?.caisseSiege?.cbmarq,
                    })
                    this.spinnerservice.hide()
                }

            }, error => { }, () => {
                this.spinnerservice.hide()
                this.getListPropOnEdit(this.formBasic.value.residences);
            })
        } else {
            this.spinnerservice.hide()
            this.getlistResidencesNoGroupe();
        }
    }

    checkAccessADDEDIT(){

        if (this.cbmarq &&  this.modeEdit) {
            this.checkAccess("FN21000013");
        }else{
            this.checkAccess("FN21000012");

        }

    }

    checkAccess(code){
        //if access false ==> go out to 403
        this.permissionservice.getAccessUser( this.id_current_user, code).subscribe((res:any)=>{
            //console.log('res access: ',res.data)
            this.have_access = res.data;
            this.spinnerservice.hide()
        }, error => { }, () => {
            this.spinnerservice.hide()
            if (!this.have_access) {
                this.router.navigate(['403']);
            } else {


            }

        });
    }

    getListDelegueWithProduit(produits) {
        this.spinnerservice.show()
        this.selected1.forEach(delegue => {
            produits.forEach(prop => {
                if (delegue.cbmarq == prop.cbmarq) {
                    delegue.idproduit = prop.idproduit;
                    this.spinnerservice.hide()
                }
            })
        })
    }



    getListPropOnEdit(produits) {
        this.spinnerservice.show()
        this.listeprops = [];
        let listDelegue: any[] = [];
        for (let produit of produits) {
            this.proprietaireService.getProprietairesResidence(produit.cbmarq).subscribe((prop: any) => {
                if (prop.statut) {
                    listDelegue = prop.data?.map(el => ({ cbmarq: el.id, nomComplet: el.nomComplet + ' | ' + produit.label, idproduit: produit.cbmarq }));
                    this.spinnerservice.hide();
                }
            }, error => { }, () => {
                this.spinnerservice.hide();
                this.listeprops = this.listeprops.concat(listDelegue);
                this.formBasic.patchValue({
                    delegues: this.listeprops,
                });
            });
        }
    }

    onAdd(event) {
        this.spinnerservice.show()
        this.listeprops = [];
        let listDelegue: any[] = [];
        for (let produit of this.selected) {
            this.proprietaireService.getProprietairesResidence(produit.cbmarq).subscribe((prop: any) => {
                if (prop.statut) {
                    listDelegue = prop.data?.map(el => ({ cbmarq: el.id, nomComplet: el.nomComplet + ' | ' + produit.label, idproduit: produit.cbmarq }))
                    this.listeprops = this.listeprops.concat(listDelegue);
                    this.spinnerservice.hide()
                }

            }, error => { }, () => {
                this.spinnerservice.hide()
            })
        }
    }

    onRemove(event){   
            
       this.selected1=this.selected1?.filter(item=> item.idproduit!=event.value.cbmarq)   
       if( this.selected.length==0){
           this.selected1=[];
       }
    }

  
   

    getlistCaisses() {
        this.spinnerservice.show()
        this.listecaisse.unshift({ value: 0, label: 'Générer caisse groupement' });
        this.caisseService.getCaisses().subscribe(
            (res: any) => {
                this.listecaisse = res.data.map(c => ({ value: c.cbmarq, label: c.intitule, siege: c.caisseSiege?.cbmarq }));
                this.listecaisse.unshift({ value: 0, label: 'Générer caisse groupement' });
                this.spinnerservice.hide()
            }
        );
    }

    getlistResidencesNoGroupe() {
        this.spinnerservice.show()
        this.residenceService.getResidencesNoCroupe().subscribe((res: any) => {

            if (res.statut) {
                this.spinnerservice.hide()
                this.listeresidence = this.listeresidence?.concat(res.data?.map(c => ({ cbmarq: c.cbmarq, label: c.intitule })));
            }
        });
    }


    getlistResidencesByGroupe() {
        this.spinnerservice.show()
        this.residenceService.getResidencesByCroupe(this.cbmarq).subscribe((res: any) => {
            console.log('resres', res);
            if (res.statut) {
                this.spinnerservice.hide()
                // this.listeresidence = res.data?.map(c => ({ cbmarq: c.cbmarq, label: c.intitule }));
                this.listeresidence = this.listeresidence?.concat(res.data?.map(c => ({ cbmarq: c.cbmarq, label: c.intitule })));
            }
        });

    }

    buildFormBasic() {
        this.formBasic = this.fb.group({
            intitule: new FormControl(null, Validators.required),
            delegues: new FormControl(null),
            residences: new FormControl(null, [Validators.required]),
            caisse: new FormControl(null, Validators.required),
            caisseSiege: new FormControl(null, Validators.required),
        });
    }

    getlistCaissesSiege() {
        this.spinnerservice.show()
        this.listecaisseSiege.unshift({ value: 0, label: 'Générer caisse siége groupement' });
        this.caisseService.getCaisseSiege().subscribe((res: any) => {
            if (res.statut) {
                this.listecaisseSiege = res.data.map(c => ({ value: c.cbmarq, label: c.intitule }));
                this.listecaisseSiege.unshift({ value: 0, label: 'Générer caisse siége groupement' });
                this.spinnerservice.hide()
            }
        }

        );
    }

    onchangeCaisse(event) {
        //console.log('', event);
        this.formBasic.patchValue({
            caisseSiege: event.siege
        });
    }

    submit() {
        this.spinnerservice.show()
        this.loading = true;
        if (this.formBasic.invalid) {
            this.spinnerservice.hide()
            this.submitted = true;
            return;
        }
        this.formBasic.value.residences.forEach(i => {
            delete i.label;
        });
        this.formBasic.value.delegues = this.selected1.map(item => ({ cbmarq: item.cbmarq, idproduit: item.idproduit }));

        if (this.can_add?.permission || this.can_edit?.permission) {
            if (this.modeEdit) {
                this.groupementService.putGroupement(this.formBasic.value, this.cbmarq).pipe(first()).subscribe((res: any) => {
                    if (res.statut === true) {
                        this.toastr.success('Groupement a été modifié avec succès !', 'Success!', { progressBar: true });
                        this.router.navigate(['/syndic/groupements']);
                        this.submitted = false;
                        this.formBasic.reset();
                        this.spinnerservice.hide()
                    } else {
                        this.spinnerservice.hide()
                        this.submitted = true;
                        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                    }
                },
                    (error) => {
                        this.spinnerservice.hide()
                        this.toastr.error('Erreur lors d\'ajout d\'un groupement . Veuillez réessayer !', 'Erreur!', { progressBar: true });
                    });
            }
            else {
                this.groupementService.postGroupement(this.formBasic.value).pipe(first()).subscribe((res: any) => {
                    if (res.statut === true) {
                        this.toastr.success('Groupement a été ajouté avec succès !', 'Success!', { progressBar: true });
                        this.router.navigate(['/syndic/groupements']);
                        this.submitted = false;
                        this.formBasic.reset();
                        this.spinnerservice.hide()
                    } else {
                        this.spinnerservice.hide()
                        this.submitted = true;
                        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
                    }
                },
                    (error) => {
                        this.spinnerservice.hide()
                        this.toastr.error('Erreur lors d\'ajout d\'un groupement . Veuillez réessayer !', 'Erreur!', { progressBar: true });
                    });
            }

        } else {
            this.spinnerservice.hide()
            this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
        }


    }


    getListeProprietaires() {
        this.spinnerservice.show()
        this.proprietaireService.getAllProprietaires().subscribe((prop: any) => {
            this.listeprops = prop.data?.map(el => ({ cbmarq: el.id, nomComplet: el.nomComplet + ' | Proprietaire' }))
            this.spinnerservice.hide()
        })
        this.proprietaireService.getAllCoProprietaires().subscribe((prop: any) => {
            this.listeprops = this.listeprops?.concat(prop.data?.map(el => ({ cbmarq: el.id, nomComplet: el.nomComplet + ' | Co-Proprietaire' })))
            this.spinnerservice.hide()
        })
        this.spinnerservice.hide()
    }

    gotoGroupement() {

        this.router.navigate(['/syndic/groupements']);

    }



}

