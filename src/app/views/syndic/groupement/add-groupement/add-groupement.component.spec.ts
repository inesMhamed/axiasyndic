import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGroupementComponent } from './add-groupement.component';

describe('AddGroupementComponent', () => {
  let component: AddGroupementComponent;
  let fixture: ComponentFixture<AddGroupementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddGroupementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGroupementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
