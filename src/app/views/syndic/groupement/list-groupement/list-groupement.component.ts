import { Component, OnInit } from '@angular/core';
import {ResidenceService} from '../../../../shared/services/residence.service';
import {GroupementService} from '../../../../shared/services/groupement.service';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ToastrService } from 'ngx-toastr';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-groupement',
  templateUrl: './list-groupement.component.html',
  styleUrls: ['./list-groupement.component.scss'],
  animations: [SharedAnimations]
})
export class ListGroupementComponent implements OnInit {

  viewMode: 'list' | 'grid' = 'list';
  allSelected: boolean;
  page = 1;
  pageSize = 8;
  groupements: any = [];
  primary = 'primary';
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  id_current_user: any;

  have_access:boolean =false;

  constructor(
    private preferenceService: PreferencesService,
      private dl: ResidenceService,
      private spinnerservice: NgxSpinnerService,
      private toastr: ToastrService,
      private groupementService: GroupementService,
      private permissionservice: DroitAccesService,
      private router: Router
  ) { }

  ngOnInit() {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000012');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000013');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000100');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000014');

    this.checkAccess();
  }

  checkAccess(){
  //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000014').subscribe((res: any) => {

      this.have_access = res.data;
      this.spinnerservice.hide()
    }, error => {
    }, () => {
      this.spinnerservice.hide()
      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {
        this.getListGroupement();
      }

    });
}

 getListGroupement() {
  this.spinnerservice.show()
        this.groupementService.getGroupements().subscribe((res: any) => {
                if (res.statut) {
                    this.groupements = res.data;
                    this.spinnerservice.hide()
                }else{
                  this.spinnerservice.hide()
                  this.groupements = [];
                }

            },
            error => {
              this.spinnerservice.hide()
            }
        );

    }

  selectAll(e) {
    this.groupements = this.groupements.map(p => {
      p.isSelected = this.allSelected;
      return p;
    });

    if (this.allSelected) {

    }
    ////console.log(this.allSelected);
  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un groupement',
      text: 'Voulez-vous vraiment supprimer ce groupement ?',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.spinnerservice.show()
        this.groupementService.deleteGroupement(cbmarq).subscribe((res: any) => {
          if (res.statut === true) {
            this.spinnerservice.hide()
            this.toastr.success('Groupement a été supprimé avec succès !', 'Success!', { progressBar: true });
            // this.groupements = res.data;
            // if (res.data == null) {
            //   this.groupements = [];
            // }
            this.getListGroupement();
            // this.ngOnInit()
          } else {
            this.spinnerservice.hide()
            this.toastr.error(res.message, 'Erreur!', { progressBar: true });
          }
        }
          ,
          (error) => {
            this.spinnerservice.hide()
            this.toastr.error(error.message, 'Erreur!', { progressBar: true });
          });

      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
}


