import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListResidenceComponent } from './residences/list-residence/list-residence.component';
import {AddResidenceComponent} from './residences/add-residence/add-residence.component';
import { EditResidenceComponent } from './residences/edit-residence/edit-residence.component';
import {ListGroupementComponent} from './groupement/list-groupement/list-groupement.component';
import {AddGroupementComponent} from './groupement/add-groupement/add-groupement.component';
import {ListCompteurComponent} from './compteur/list-compteur/list-compteur.component';
import { ContratsComponent } from './contrat/contrats/contrats.component';
import {BlocComponent} from './bloc/bloc/bloc.component';
import {AddBlocComponent} from './bloc/add-bloc/add-bloc.component';
import {DetailsAppartementComponent} from './appartement/details-appartement/details-appartement.component';
import {ListAppartementComponent} from './appartement/list-appartement/list-appartement.component';
import {EcheanceAppartementComponent} from './appartement/echeance-appartement/echeance-appartement.component';
import {DetailsResidenceComponent} from './residences/details-residence/details-residence.component';
import { CaisseComponent } from './caisse/caisse.component';
import {AddEditReglementComponent} from './reglement/add-edit-reglement/add-edit-reglement.component';
import {AddReunionComponent} from '../communite/reunion/add-reunion/add-reunion.component';
import {ListReglementComponent} from './reglement/list-reglement/list-reglement.component';
import {DetailsReglementComponent} from './reglement/details-reglement/details-reglement.component';
import {AddEditDepenseComponent} from './depense/add-edit-depense/add-edit-depense.component';
import {ListDepenseComponent} from './depense/list-depense/list-depense.component';
import {DetailsDepenseComponent} from './depense/details-depense/details-depense.component';
import {ListProprietaireComponent} from './proprietaire/list-proprietaire/list-proprietaire.component';
import {AddEditProprietaireComponent} from './proprietaire/add-edit-proprietaire/add-edit-proprietaire.component';
import {DetailsProprietaireComponent} from './proprietaire/details-proprietaire/details-proprietaire.component';
import {AddEditTransfertComponent} from './transfert/add-edit-transfert/add-edit-transfert.component';
import {ListTransfertComponent} from './transfert/list-transfert/list-transfert.component';
import {DetailsTransfertComponent} from './transfert/details-transfert/details-transfert.component';
import {ListUtilisateursComponent} from './utilisateurs/list-utilisateurs/list-utilisateurs.component';
import { AuthGuard } from 'src/app/_directives/_helpers/auth.guard';
import { DroitAccesComponent } from './droit-acces/droit-acces.component';
import { AddContratComponent } from './contrat/add-contrat/add-contrat.component';
import { AlertComponent } from './contrat/alert/alert.component';
import { AddCompteurComponent } from './compteur/add-compteur/add-compteur.component';
import { DetailsCaisseComponent } from './caisse/details-caisse/details-caisse.component';
import { TracabiliteTransfertComponent } from './transfert/tracabilite-transfert/tracabilite-transfert.component';
import { ConfigMaintenanceComponent } from './config-maintenance/config-maintenance.component';
import { DetailsUserComponent } from './utilisateurs/details-user/details-user.component';
import { ConfigNotificationComponent } from './config-notification/config-notification.component';
import { GenFraisSyndicComponent } from './residences/gen-frais-syndic/gen-frais-syndic.component';
import { ConfigTypesComponent } from './config-types/config-types.component';
import { ConfigSocieteComponent } from './config-societe/config-societe.component';
import { MonitoringLogsComponent } from './monitoring-logs/monitoring-logs.component';
import { UserPhoneComponent } from './user-phone/user-phone.component';
import { LogAdministrationComponent } from './log-administration/log-administration.component';
import { LogSystemComponent } from './log-system/log-system.component';
import { ChangepwdComponent } from './utilisateurs/changepwd/changepwd.component';

const routes: Routes = [
  {
    path: 'residences',
    component: ListResidenceComponent, canActivate: [AuthGuard]
  },
  {
    path: 'addresidence',
    component: AddResidenceComponent, canActivate: [AuthGuard]
  }
  , {
    path: 'modifier-residence/:cbmarq',
    component: AddResidenceComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-residence/:cbmarq',
    component: DetailsResidenceComponent, canActivate: [AuthGuard]
  },
  {
    path: 'gen-frais-syndic',
    component: GenFraisSyndicComponent, canActivate: [AuthGuard]
  },
  {
    path: 'groupements',
    component: ListGroupementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-groupement',
    component: AddGroupementComponent, canActivate: [AuthGuard]
  }, {
    path: 'modifier-groupement/:cbmarq',
    component: AddGroupementComponent, canActivate: [AuthGuard]
  }, {
    path: 'compteurs',
    component: ListCompteurComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-compteur',
    component: AddCompteurComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-compteur/:cbmarq',
    component: AddCompteurComponent, canActivate: [AuthGuard]
  },
  {
    path: 'contrats/alertes',
    component: AlertComponent, canActivate: [AuthGuard]
  }, {
    path: 'contrats',
    component: ContratsComponent, canActivate: [AuthGuard]
  }, {
    path: 'ajouter-contrat',
    component: AddContratComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-contrat/:cbmarq',
    component: AddContratComponent, canActivate: [AuthGuard]
  },
  {
    path: 'bloc',
    component: BlocComponent, canActivate: [AuthGuard]
  },
  {
    path: 'bloc/:id',
    component: BlocComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add-bloc',
    component: AddBlocComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add-edit-appartement',
    component: AddBlocComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-appartement/:cbmarq',
    component: DetailsAppartementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add-bloc/:id',
    component: AddBlocComponent
  },
  {
    path: 'utilisateur-mobile',
    component: UserPhoneComponent, canActivate: [AuthGuard]
  },
  {
    path: 'log-admin',
    component: LogAdministrationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'log-system',
    component: LogSystemComponent, canActivate: [AuthGuard]
  },
  {
    path: 'appartements',
    component: ListAppartementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'appartements/:cbmarq',
    component: ListAppartementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'echeance-syndic/:cbmarq',
    component: EcheanceAppartementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'reglements',
    component: ListReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'reglements/proprietaire/:cbmarq',
    component: ListReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'reglements-appartement/:cbmarq',
    component: ListReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-reglement/:cbmarq',
    component: DetailsReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-reglement',
    component: AddEditReglementComponent
  },
  {
    path: 'ajouter-reglement-appartement/:cbmarq',
    component: AddEditReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-reglement/:cbmarq',
    component: AddEditReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-reglement/:cbmarq/:idAppart',
    component: AddEditReglementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'caisses',
    component: CaisseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-caisse/:cbmarq',
    component: DetailsCaisseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-caisseSiege/:cbmarq',
    component: DetailsCaisseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'reunion',
    component: AddReunionComponent, canActivate: [AuthGuard]
  },
  {
    path: 'depenses',
    component: ListDepenseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-depense/:cbmarq',
    component: AddEditDepenseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-depense',
    component: AddEditDepenseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-depense/:cbmarq',
    component: DetailsDepenseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-proprietaire/:id',
    component: DetailsProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-proprietaire',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-proprietaire-appartement/:cbmarq',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-Coproprietaire',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-coproprietaire-appartement/:cbmarq',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-proprietaire/:id',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-coproprietaire/:id',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-coproprietaire/:id',
    component: DetailsProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'proprietaires',
    component: ListProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-transfert',
    component: AddEditTransfertComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-transfert/:cbmarq',
    component: AddEditTransfertComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-transfert/:cbmarq',
    component: DetailsTransfertComponent, canActivate: [AuthGuard]
  },
  {
    path: 'tracabilite-transfert/:cbmarq',
    component: TracabiliteTransfertComponent, canActivate: [AuthGuard]
  },
  {
    path: 'transferts',
    component: ListTransfertComponent, canActivate: [AuthGuard]
  },
  {
    path: 'utilisateurs',
    component: ListUtilisateursComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ajouter-utilisateur',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-utilisateur/:id',
    component: AddEditProprietaireComponent, canActivate: [AuthGuard]
  },

  {
    path: 'details-utilisateur/:id',
    component: DetailsProprietaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'details-utilisateur-interne/:id',
    component: DetailsUserComponent, canActivate: [AuthGuard]
  },
  {
    path: 'droit-acces',
    component: DroitAccesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'config-maintenance',
    component: ConfigMaintenanceComponent, canActivate: [AuthGuard]
  },
  {
    path: 'config-notification',
    component: ConfigNotificationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'config-types',
    component: ConfigTypesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'config-societe',
    component: ConfigSocieteComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monitoring-logs',
    component: MonitoringLogsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'modifier-motdepasse',
    component: ChangepwdComponent, canActivate: [AuthGuard]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SyndicRoutingModule { }
