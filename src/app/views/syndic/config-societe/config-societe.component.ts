import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { ParametreService } from 'src/app/shared/services/parametre.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import libphonenumber from 'google-libphonenumber';
import { NgxSpinnerService } from 'ngx-spinner';
declare var require: any
class Societe{
    cbmarq: number;
    cbcreation: string;
    intitule: string;
    description: string;
    matriculeFiscale: string;
    adresse: string;
    telephone: string;
    email: string;
    banque: string;
    compteBanque: string;
    devise: string;
    round: number;
    affichageTableaux: number;
    demo: boolean;
    duplicate: number;
    constructor(){}
}

@Component({
  selector: 'app-config-societe',
  templateUrl: './config-societe.component.html',
  styleUrls: ['./config-societe.component.scss']
})
export class ConfigSocieteComponent implements OnInit {
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.Tunisia, CountryISO.Tunisia];
  submitted: boolean;
  societeModel:Societe=new Societe();
  paramForm: FormGroup;
  
  can_add: Permission;
  can_generate: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_liste: Permission;
  can_detail: Permission;
  id_current_user: any;

  have_access:boolean =false;
  phone1: any;
  phone2: any;
  role: any;
  Admin: any;
  IdSoc: any;
  code: any;
  url: any;
  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File = null;
  constructor(private paramtreService: ParametreService, private toastr: ToastrService, private fb: FormBuilder, private permissionservice: DroitAccesService,
    private proprietaireService: ProprietaireService, private router: Router, private spinnerservice: NgxSpinnerService,) {

      this.id_current_user = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.Admin = this.role.includes('ROLE_ADMIN');
     }

  ngOnInit(): void {
    this.spinnerservice.show()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000126').subscribe((res:any)=>{
      this.have_access =res.data;
    },error=>{},()=>{
      if (!this.have_access) {
        this.router.navigate(['403']);
      } else {
        this.getFicheClient();
      }
    });

    if (!this.Admin) {
      this.router.navigate(['403']);
    }

    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000021');
    this.can_generate = this.permissionservice.search( this.id_current_user, 'FN21000008');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000022');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000026');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000023');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000025');

    this.paramForm = this.fb.group({
      intitule:  new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      matriculeFiscale: new FormControl(null, Validators.required),
      adresse: new FormControl(null, Validators.required),
      telephone: new FormControl("", Validators.required),
      email: new FormControl(null, Validators.required),
      banque: new FormControl(null, Validators.required),
      compteBanque: new FormControl(null, Validators.required),
      devise: new FormControl(null, Validators.required),
      round: new FormControl(null, Validators.required),
      affichageTableaux: new FormControl(null, Validators.required),
      logo: new FormControl(null),
    });


  }

  getFicheClient() {
    this.spinnerservice.show()
    this.proprietaireService.getProprietairebyId(this.id_current_user).subscribe((res: any) => {
      if (res.statut === true) {
        this.IdSoc = res?.data?.societe;
        this.spinnerservice.hide()
      }
    }, error => {
      this.spinnerservice.hide()
    }, () => {
      this.spinnerservice.hide()
      this.getInfoSociete(this.IdSoc);
    });
  }

  convertPhoneNumber(telephone1) {
    const instance = libphonenumber.PhoneNumberUtil.getInstance();
    const PNF = require('google-libphonenumber').PhoneNumberFormat;
    if (telephone1) {
      let PhoneNumberphone = instance.parse(telephone1);
      const internationalNumber = instance.format(PhoneNumberphone, PNF.INTERNATIONAL);
      let region = instance.getRegionCodeForNumber(PhoneNumberphone);
      let country = PhoneNumberphone.getCountryCode();
      let phone = PhoneNumberphone.getNationalNumber();
      this.phone1 = {
        countryCode: region,
        dialCode: '+' + country.toString(),
        e164Number: telephone1,
        internationalNumber: internationalNumber.toString(),// "+216 21 000 000",
        nationalNumber: '',
        number: phone.toString(),
      };
    }
  }

  getInfoSociete(IdSoc) {
    this.spinnerservice.show()
    this.paramtreService.getInfoSociete(IdSoc).subscribe((res: any) => {

      if (res.statut) {
        this.convertPhoneNumber(res.data.telephone);
        this.paramForm.patchValue({
          intitule: res.data.intitule,
          description: res.data.description,
          matriculeFiscale: res.data.matriculeFiscale,
          adresse: res.data.adresse,
          telephone: this.phone1,
          email: res.data.email,
          banque: res.data.banque,
          compteBanque: res.data.compteBanque,
          devise: res.data.devise,
          round: res.data.round,
          affichageTableaux: res.data.affichageTableaux,
        });
        this.code = res.data.code ;
        this.url = res.data.logo;
        this.spinnerservice.hide()
      }
      this.spinnerservice.hide()
    });
  }

  onFileChange(files: FileList) {
    this.labelImport.nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
    this.fileToUpload = files.item(0);

    if (files.length > 0) {
      const file = files[0];
      this.paramForm.patchValue({
        logo: file
      });
    }
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      };
      this.paramForm.patchValue({
        logo: event.target.files[0]
      });
    }
  }
  public delete() {
    this.url = null;
  }

  onSubmit() {
    this.spinnerservice.show()
    console.log(this.paramForm);
    if (this.paramForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true;
      return;
    }
    this.paramForm.value.telephone = this.paramForm.value.telephone.e164Number;
    const myFormValue = this.paramForm.value;
    const myFormData = new FormData();
    Object.keys(myFormValue).forEach(name => {
      myFormData.append(name, myFormValue[name]);
    });
    console.log(this.paramForm.value, myFormData);
    this.paramtreService.putInfoSociete(this.IdSoc, myFormData).subscribe((res: any) => {
      if (res.statut === true) {
        this.toastr.success('Societé a été modifiée avec succès !', 'Success!', { progressBar: true });
        window.location.reload();
        // this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        //   this.router.navigate(['/syndic/config-societe']);
        // });
        this.spinnerservice.hide()
      } else {
        this.spinnerservice.hide()
        this.toastr.error(res.message, 'Erreur!', { progressBar: true });
      }
    }, (error: any) => {
      this.spinnerservice.hide()
      this.toastr.error('Erreur lors de la modification de la societé . Veuillez réessayer !', 'Erreur!', { progressBar: true });
    }, () => {
      this.getInfoSociete(this.IdSoc);
    });
  }
}
