import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigSocieteComponent } from './config-societe.component';

describe('ConfigSocieteComponent', () => {
  let component: ConfigSocieteComponent;
  let fixture: ComponentFixture<ConfigSocieteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigSocieteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigSocieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
