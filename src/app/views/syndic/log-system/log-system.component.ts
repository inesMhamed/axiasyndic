import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LogadminService } from 'src/app/shared/services/logadmin.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-log-system',
  templateUrl: './log-system.component.html',
  styleUrls: ['./log-system.component.scss'],
  providers: [DatePipe]
})
export class LogSystemComponent implements OnInit {
  displayedColumns: string[] = ['Action', 'Actionneur', 'Date', 'Description'];
  public searchInput = new FormControl();
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  pageSize = 10;
  logList: any;
  @ViewChild('TABLE') table: ElementRef;
  ws: any;
  pageEvent: PageEvent;
  pageIndex: number = 0;
  length: number;
  searchTerm: any;
  constructor(private logadminService: LogadminService, private datePipe: DatePipe, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getArticles('');
    this.searchInput.valueChanges
      .pipe(debounceTime(1_000), distinctUntilChanged())
      .subscribe((searchTerm) => {
        this.pageIndex = 0;
        this.searchTerm = searchTerm;
        this.getArticles(searchTerm);
      });
  }
  getloglist(page, perpage, keyword) {
    this.logadminService.getLogSystem(page, perpage, keyword).subscribe((res: any) => {
      if (res.statut == false) {
        this.dataSource.data = []
      } else {
        this.logList = res.data
        this.dataSource.data = res.data;
      }
    })
  }
  exportdata() {

    this.logadminService.getLogSystem(0, this.length, '').subscribe((res: any) => {
      if (res.statut === false) {
        this.toastr.warning('Aucune donnée à exporter !');
      } else {
        let dataSource = res.data.map(el => ({ id: el.cbmarq, date: this.datePipe.transform(el.date.date, 'short'), action: el.action, actionneur: el.actionneur, description: el.description }));
        this.logadminService.exportAsExcelFile(dataSource, 'Log Système');
      }
    });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public getArticles(searchTerm = '') {
    this.logadminService.getLogSystem(this.pageIndex, this.pageSize, searchTerm)
      .subscribe((resp: any) => {
        this.length = resp.count;
        // this.dataSource.data = resp.data;
        this.logList = resp.data;
      });
  }

  public changePage(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize
    this.getArticles('');
  }
}
