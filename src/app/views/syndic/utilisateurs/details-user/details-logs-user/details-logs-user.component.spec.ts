import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsLogsUserComponent } from './details-logs-user.component';

describe('DetailsLogsUserComponent', () => {
  let component: DetailsLogsUserComponent;
  let fixture: ComponentFixture<DetailsLogsUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsLogsUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsLogsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
