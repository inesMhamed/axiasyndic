import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsInterventionUserComponent } from './details-intervention-user.component';

describe('DetailsInterventionUserComponent', () => {
  let component: DetailsInterventionUserComponent;
  let fixture: ComponentFixture<DetailsInterventionUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsInterventionUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsInterventionUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
