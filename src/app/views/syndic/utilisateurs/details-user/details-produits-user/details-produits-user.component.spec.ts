import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsProduitsUserComponent } from './details-produits-user.component';

describe('DetailsProduitsUserComponent', () => {
  let component: DetailsProduitsUserComponent;
  let fixture: ComponentFixture<DetailsProduitsUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsProduitsUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsProduitsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
