import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCaissesUserComponent } from './details-caisses-user.component';

describe('DetailsCaissesUserComponent', () => {
  let component: DetailsCaissesUserComponent;
  let fixture: ComponentFixture<DetailsCaissesUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsCaissesUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCaissesUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
