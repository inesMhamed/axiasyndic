import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsTransfertsUserComponent } from './details-transferts-user.component';

describe('DetailsTransfertsUserComponent', () => {
  let component: DetailsTransfertsUserComponent;
  let fixture: ComponentFixture<DetailsTransfertsUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsTransfertsUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsTransfertsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
