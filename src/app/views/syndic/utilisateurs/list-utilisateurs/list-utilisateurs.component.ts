import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Proprietaire } from 'src/app/shared/models/proprietaire.model';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatInput } from '@angular/material/input';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { ifStmt } from '@angular/compiler/src/output/output_ast';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import libphonenumber from 'google-libphonenumber';
import { NgxGalleryService } from '@kolkov/ngx-gallery';
import { NgxSpinnerService } from 'ngx-spinner';
declare var require: any
// custom validator to check that two fields match
// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
@Component({
  selector: 'app-list-utilisateurs',
  templateUrl: './list-utilisateurs.component.html',
  styleUrls: ['./list-utilisateurs.component.scss']
})

export class ListUtilisateursComponent implements OnInit {
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.Tunisia, CountryISO.Tunisia];
  @ViewChild('labelImport')
  labelImport: ElementRef;
  fileToUpload: File = null;

  displayedColumns: string[] = ['nomcomplet', 'username', 'email', 'telephone', 'statut', 'action'];
  dataSource = new MatTableDataSource<Proprietaire>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  userForm: FormGroup
  submitted: boolean
  modeEdit: boolean;
  idUser: any;
  isFileChosen: boolean = false;
  fileName: string = '';
  can_add: Permission;
  can_edit: Permission;
  can_active: Permission;
  can_liste: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(public router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder,
    private toastr: ToastrService, private proprietaireService: ProprietaireService,
    private preferenceService: PreferencesService, private spinnerservice: NgxSpinnerService,
    private modalService: NgbModal, private utilisateurService: UtilisateurService, private permissionservice: DroitAccesService
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000043');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000044');
    this.can_active = this.permissionservice.search( this.id_current_user, 'FN21000046');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000045');

    this.checkAccess();


    this.getlistUser();
    this.userForm = this.formBuilder.group({
      nomComplet: new FormControl("", Validators.required),
      login: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email]),
      photo: new FormControl("",),
      adresse: new FormControl("",),
      profession: new FormControl("",),
      identifiant: new FormControl("",),
      type: new FormControl(2,),
      pwd: new FormControl("", [Validators.required, Validators.minLength(8)]),
      confirmpassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
      telephone1: new FormControl("", [Validators.required, Validators.minLength(8)]),
      telephone2: new FormControl("", Validators.minLength(8)),

    }, { validator: MustMatch('pwd', 'confirmpassword') });
  }

    
  checkAccess(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000045').subscribe((res:any)=>{

    this.have_access =res.data;
    
  },error=>{},()=>{

    if(!this.have_access){
      this.router.navigate(['403']);
    }
    
  });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  edit(content, id) {

    this.idUser = id
    if (this.idUser) {
      this.modeEdit = true
    }
     var myphone = null;
    const instance = libphonenumber.PhoneNumberUtil.getInstance();
    const PNF = require('google-libphonenumber').PhoneNumberFormat;
    this.utilisateurService.getUtilisateurbyId(id).subscribe((ress: any) => {

      let PhoneNumberphone:any
      try {
         PhoneNumberphone = instance.parse(ress.telephone1)
         const internationalNumber = instance.format(PhoneNumberphone, PNF.INTERNATIONAL);
         let region = instance.getRegionCodeForNumber(PhoneNumberphone);
         let country = PhoneNumberphone.getCountryCode();
         let phone = PhoneNumberphone.getNationalNumber();
         myphone = {
          countryCode: region,
          dialCode: "+" + country.toString(),
          e164Number: ress.telephone1,
          internationalNumber: internationalNumber.toString(),// "+216 21 000 000",
          nationalNumber: "",
          number: phone.toString(),
        }
      } catch (error) {
        myphone = {
          countryCode: 'TN',
          dialCode: "+216",
          e164Number: '',
          internationalNumber: '',// "+216 21 000 000",
          nationalNumber: "",
          number: '',
        }
      }
      this.userForm.patchValue({
        id: ress.id,
        nomComplet: ress.data.nomComplet,
        login: ress.data.username,
        adresse:  ress.data.adresse,
        profession: ress.data.profession,
        identifiant:  ress.data.identifiant,
        type:  ress.data.type,
        email: ress.data.email,
        telephone1: myphone,
        telephone2: ress.data.telephone2,
      })
    })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
  }
  // onFileChange(files: FileList) {
  //   this.labelImport.nativeElement.innerText = Array.from(files)
  //     .map(f => f.name)
  //     .join(', ');
  //   this.fileToUpload = files.item(0);

  //   if (files.length > 0) {
  //     const file = files[0];
  //     this.userForm.patchValue({
  //       photo: file
  //     });
  //   }
  // }
  onFileChange(event){
    let file = event.target.files[0];
    if (event.target.files.length > 0){
    this.isFileChosen = true;
    }        
    this.userForm.patchValue({
      photo: file
    });
    this.fileName = file.name;
  }

  getlistUser() {
    this.spinnerservice.show()
    this.utilisateurService.getAllUtilisateurs().subscribe((res: any) => {
      if(res.statut){
        this.dataSource.data = res.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      this.spinnerservice.hide()
    });
  }

  onSubmit() {
    this.spinnerservice.show()
    if (this.modeEdit) {
      this.userForm.get('confirmpassword').disable();
      this.userForm.get('pwd').disable();
    }
    if (this.userForm.invalid) {
      this.spinnerservice.hide()
      //console.log("userForm erreur", this.userForm.controls,)
      this.submitted = true
      return;
    }
    //console.log("userForm after control", this.userForm.value)
    this.userForm.patchValue({ telephone1: this.userForm.value.telephone1.e164Number, });
    if (this.userForm.value.telephone2) {
      this.userForm.patchValue({ telephone2: this.userForm.value.telephone2.e164Number, });

    }
    if (this.modeEdit) {

      const myFormValue = this.userForm.value;

      const myFormData = new FormData();

      Object.keys(myFormValue).forEach(name => {
        myFormData.append(name, myFormValue[name]);
      });
      this.utilisateurService.putUtilisateur(myFormData, this.idUser).subscribe((res: any) => {
        if (res.statut) {
          this.toastr.success('Utilisateur est modifié avec succès ', 'Succès!', { progressBar: true });

          this.getlistUser()
          this.modalService.dismissAll();
        }
        else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
        this.spinnerservice.hide()
      })
    } else {
      this.utilisateurService.postUtilisateur(this.userForm.value).subscribe((res: any) => {
        if (res.statut) {
          //console.log(res.data)
          this.userForm.reset()
          this.getlistUser()
          this.toastr.success('Utilisateur est enregistré avec succès !', 'Succès!', { progressBar: true });
          this.modalService.dismissAll();
        }
        else {
          this.toastr.error(res.message, 'Erreur!', { progressBar: true });
        }
        this.spinnerservice.hide()
      })
    }

  }
  myFunction() {
    let x = <HTMLInputElement>document.getElementById("pwd");
    let y = <HTMLInputElement>document.getElementById("confirmpassword");
    if (x.type === "password") {
      x.type = "text";
    }
    else {
      x.type = "password";
    }
    if (y.type === "password") {
      y.type = "text";
    } else {
      y.type = "password";
    }

  }
  cancelUtilisateur(cbmarq: any) {
    Swal.fire({
      title: 'Désactiver un utilisateur',
      text: 'Voulez-vous vraiment désactiver cet utilisateur ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinnerservice.show()
        this.proprietaireService.cancelProprietaire(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistUser()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
          this.spinnerservice.hide()
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  activeUtilisateur(cbmarq: any) {
    Swal.fire({
      title: 'Activer un utilisateur',
      text: 'Voulez-vous vraiment activer cet utilisateur ?',
      icon: 'success',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proprietaireService.confirmProprietaire(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistUser()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

}
