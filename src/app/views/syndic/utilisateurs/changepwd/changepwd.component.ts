import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.scss']
})
export class ChangepwdComponent implements OnInit {
  change: boolean = false
  @Input() PData: number;
  passwordForm: FormGroup;
  submitted: boolean = false;
  roleConnect: string;
  current_user: string;
  old: any;
  constructor(private formBuilder: FormBuilder, private toastrService: ToastrService, private utlisateurService: UtilisateurService) { }

  ngOnInit(): void {
    this.roleConnect = localStorage.getItem('role')
    this.current_user = localStorage.getItem('id')

    this.passwordForm = this.formBuilder.group({
      userID: ["", Validators.required],
      password: ["", Validators.required],
      newPassword: ["", [Validators.required, Validators.minLength(8)]],
      confirmpassword: ["", [Validators.required, Validators.minLength(8)]],
    }, { validator: MustMatch('newPassword', 'confirmpassword') }
    );
  }
  get f() {
    return this.passwordForm.controls;
  }
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const password = this.old
    const oldpassword = group.controls.oldpassword.value;

    return password === oldpassword ? null : { notSame: true }
  }
  myFunction() {
    this.change = !this.change
    let x = <HTMLInputElement>document.getElementById("newPassword");
    let y = <HTMLInputElement>document.getElementById("password");
    let z = <HTMLInputElement>document.getElementById("confirmpassword");
    if (x.type === "password") {
      x.type = "text";
    }
    else {
      x.type = "password";
    }
    if (y.type === "password") {
      y.type = "text";
    } else {
      y.type = "password";
    }
    if (z.type === "password") {
      z.type = "text";
    } else {
      z.type = "password";
    }
  }
  onSubmit() {
    this.passwordForm.patchValue({
      userID: this.current_user
    })
    //this.user.id = this.id
    this.submitted = true;
    // stop here if form is invalid
    if (this.passwordForm.invalid) {
      console.log(this.passwordForm.controls)
      console.log(this.passwordForm.value)
      return;
    }
    console.log(this.passwordForm.value)
    this.utlisateurService.changePassword(this.passwordForm.value).subscribe(
      (res:any) => {
        console.log('res:', res)
        if (res.message === "Mot de passe incorrecte.") {
          this.toastrService.error("Ancien mot de passe incorrecte ! ")
        } else {
          this.toastrService.success("Mot de passe modifié avec success ")
        }
        this.submitted = false;
        this.passwordForm.reset()
      },
      error => {
        this.toastrService.error("Veuillez essayer plus tard ! ")
      }
    )



  }
}
