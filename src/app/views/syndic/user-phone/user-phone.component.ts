import { Component, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { Utilisateur } from 'src/app/shared/models/utilisateur.model';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';

@Component({
  selector: 'app-user-phone',
  templateUrl: './user-phone.component.html',
  styleUrls: ['./user-phone.component.scss']
})
export class UserPhoneComponent implements OnInit {
  userPhoneList: any = [];
  displayedColumns: string[] = ['nomcomplet', 'username', 'email', 'telephone', 'statut'];
  dataSource = new MatTableDataSource<Utilisateur>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  pageSize = 10;
  constructor(private userphoneService: UtilisateurService,
    private spinnerservice: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.userphoneService.getUtilisateurPhone().subscribe((res: any) => {
      console.log('rere', res)
      this.userPhoneList = res.data
      this.dataSource.data = res.data;
      this.spinnerservice.hide()
    })

  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
