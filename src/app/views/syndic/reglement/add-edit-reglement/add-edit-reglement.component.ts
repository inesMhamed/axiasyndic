import {DatePipe} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AppartementService} from 'src/app/shared/services/appartement.service';
import {ReglementService} from 'src/app/shared/services/reglement.service';
import {first} from 'rxjs/operators';
//import Swal from 'sweetalert2/dist/sweetalert2.js';
import Swal from 'sweetalert2';
import { Reglement } from 'src/app/shared/models/reglement.model';
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { NgxSpinnerService } from 'ngx-spinner';

class Alert {
    type: string;
    message: string;
}

@Component({
    selector: 'app-add-edit-reglement',
    templateUrl: './add-edit-reglement.component.html',
    styleUrls: ['./add-edit-reglement.component.scss'],
    providers: [DatePipe]
})
export class AddEditReglementComponent implements OnInit {
    formReglement: FormGroup;
    typesBien: any = [{value: 1, label: 'Appartement'}, {value: 2, label: 'Lot'}];
    selection: any = 'E0032';
    submitted: boolean;
    listProp: any[];
    listCooProp: any[];
    typesPayment: any;
    listAppartements: any;
    propSelected: any;
    listtypeFrais: any[];
    somme: number;
    tranche: any;
    id: any = 0;
    idAppartement: any = 0;
    modeEdit: boolean = false;
    test: any;
    currentDate: any;
    modeAppartement: boolean;
    can_add: Permission;
    can_edit: Permission;
    id_current_user: any;
    isCooproprietaire: boolean = false;
    routeState: any;
    avanceClient: any;
    useAdvance: boolean = false;
    alertModel: Alert = new Alert();
    isAlertVisible: boolean = false;
    sommeAPayer: number = 0;
    contrainteMinAvance: boolean = false;

    have_access:boolean =false;


    constructor(private formBuilder: FormBuilder, private reglementService: ReglementService,
        private appartementService: AppartementService, private toastr: ToastrService,
        private route: ActivatedRoute, private router: Router, private datepipe: DatePipe, private permissionservice: DroitAccesService,
        private proprietaireService: ProprietaireService, private spinnerservice: NgxSpinnerService,
    ) {


    }

    ngOnInit(): void {
        this.spinnerservice.show()
        this.id_current_user = localStorage.getItem('id');
        this.can_add = this.permissionservice.search(this.id_current_user, 'FN21000062');
        this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000063')
        this.modeEdit = false;
        //this.modeAppartement = false;
        this.route.paramMap.subscribe(params => {
            this.id = params.get('cbmarq');
            this.idAppartement = params.get('idAppart');
        });
        this.currentDate = new Date();
        if (this.id != null && this.id != 0) {
            ////console.log("innn update reglement...")
            this.modeEdit = true;
            if (this.router.url.includes('/syndic/modifier-reglement/')) {
                this.modeEdit = true;
            } else {
                this.modeEdit = false;
            }
            this.getReglement(this.id);
        } else {
            ////console.log("innn add reglement...")

            this.getlistPropiretaire();
        }

        this.checkAccessAddEdit();

        this.tranche = 0;
        this.submitted = false;
        //this.getlistPropiretaire();
        //this.getListAppartement()
        this.getPayementType();
        this.getTypeFrais();
        // this.getReglement(this.id)
        this.formReglement = this.formBuilder.group({
            autresPersonnes: new FormControl(null,),
            date: new FormControl(new Date(), Validators.required),
            proprietaire: new FormControl(null, Validators.required),
            montant: new FormControl('', [Validators.required, Validators.min(0)]),
            dateCheque: new FormControl('',),
            referenceCheque: new FormControl(null,),
            typePayement: new FormControl(null, Validators.required),
            lignes: this.formBuilder.array([]),
        });

        this.getTypeFrais();
    }

    checkAccessAddEdit(){

        if(this.id && this.modeEdit){
            this.checkAccess("FN21000063");
        }else{
            this.checkAccess("FN21000062");
        }

    }

    checkAccess(code){
        //if access false ==> go out to 403
        this.permissionservice.getAccessUser( this.id_current_user, code).subscribe((res:any)=>{
   
         this.have_access =res.data;
        
       },error=>{},()=>{
   
         if(!this.have_access){
           this.router.navigate(['403']);
         }
        
       });
   }

    onSubmit() {
        this.spinnerservice.show()
        this.submitted = true;
        if (this.formReglement.invalid) {
            this.spinnerservice.hide()
            this.submitted = true;
            return;
        }
        //************* Somme des montant à payé et adapter form
        this.somme = 0;
        var ecart: number = 0;
        this.formReglement.getRawValue().lignes.forEach(i => {
            if (i.selected === true) {
                this.somme = this.somme + parseFloat(i.montant);
            }
            let removeIndex = this.formReglement.value.lignes.findIndex(itm => itm.selected === false);
            if (removeIndex !== -1) {
                this.formReglement.value.lignes.splice(removeIndex, 1);
            }
        });
        this.formReglement.value.lignes.forEach(i => {
            delete i.selected;
            delete i.intitule;
            delete i.solde;
        });
        if (this.somme > parseFloat(this.formReglement.value.montant)) {
            this.toastr.error('La somme des frais syndic est supérieure au montant de règlement !', 'Erreur!', { progressBar: true });
        } else {
            if (!this.modeEdit) {
                if (this.can_add?.permission) {
                    this.formReglement.value.montant = this.formReglement.value.montant.toString();
                    if (this.useAdvance) {
                        this.formReglement.value.avance = this.avanceClient;
                        ////console.log("submit ajout -- avance client: ", this.formReglement.value);
                    } else {
                        this.formReglement.value.avance = 0;
                        ////console.log("submit ajout -- no avance client: ", this.formReglement.value);

                    }


                    this.reglementService.postReglements(this.formReglement.value).subscribe((ress: any) => {
                        if (ress.statut == true) {
                            //this.toastr.success('Règlement est enregistée avec succès !', 'Succès!', { progressBar: true });
                            Swal.fire({
                                title: 'Règlement est enregisté avec succès!',
                                text: 'Merci de suivre l\'étape suivante pour confirmer votre demande de règlement !',
                                icon: 'info',
                                width: '650px',

                                iconColor: '#213c7f',
                                confirmButtonColor: '#213c7f',
                                confirmButtonText: 'Continuer'
                            }).then((result) => {
                                if (result.value) {
                                    ////console.log(result.value)
                                    this.router.navigate([`/syndic/details-reglement/`, ress.data.cbmarq]);
                                }
                            });
                            this.formReglement.reset();
                        } else {
                            this.toastr.warning(ress.message, 'Erreur!', { progressBar: true });
                        }
                        this.spinnerservice.hide()
                    },
                        error => {
                            this.spinnerservice.hide()
                            //console.log('error:', error);
                        });
                } else {
                    this.spinnerservice.hide()
                    this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
                }
            } else {
                this.spinnerservice.show()
                if (this.can_edit?.permission) {
                    this.formReglement.value.montant = this.formReglement.value.montant.toString();

                    if (this.useAdvance) {
                        this.formReglement.value.avance = this.avanceClient;
                        ////console.log("submit update -- avance client: ", this.formReglement.value);
                    } else {
                        this.formReglement.value.avance = 0;
                        ////console.log("submit update -- no avance client: ", this.formReglement.value);

                    }

                    // this.formReglement.value.date = this.datepipe.transform(this.formReglement.value.date , 'shortDate')
                    // this.formReglement.value.dateCheque = this.datepipe.transform(this.formReglement.value.dateCheque , 'shortDate')
                    //this.formReglement.cbmarq=this.id
                    this.id = parseInt(this.id);
                    ////console.log("putReglements",this.formReglement.value, this.id)


                    this.reglementService.putReglements(this.formReglement.value, this.id).subscribe((ress: any) => {
                        if (ress.statut == true) {
                            //this.toastr.success('Règlement est enregistée avec succès !', 'Succès!', { progressBar: true });
                            this.formReglement.reset();
                            Swal.fire({
                                title: 'Règlement est modifié avec succès!',
                                text: 'Merci de suivre l\'étape suivante pour confirmer votre demande de règlement !',
                                icon: 'info',
                                width: '650px',

                                iconColor: '#213c7f',
                                confirmButtonColor: '#213c7f',
                                confirmButtonText: 'Continuer'
                            }).then((result) => {
                                if (result.value) {
                                    ////console.log(result.value)
                                    //this.router.navigate([`/syndic/details-reglement/`, ress.data.cbmarq]);
                                    this.gotoListReglement();
                                }
                            });

                        } else {
                            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
                        }
                        this.spinnerservice.hide()
                    },
                        error => {
                            this.spinnerservice.hide()
                            //console.log('error:', error);
                        });
                } else {

                    this.toastr.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
                }
                this.spinnerservice.hide()
            }
        }
    }

    gotoListReglement(){
        this.router.navigate([`/syndic/reglements`]);

    }

    getlistPropiretaire() {
        this.spinnerservice.show()
        this.listProp = [];
        if (this.idAppartement == undefined || this.idAppartement == null || this.idAppartement == 0) {
            this.proprietaireService.getAllProprietaires().subscribe((prop: any) => {
                //this.reglementService.getPorpietaire().subscribe((prop: any) => {
                this.listProp = prop.data;
                console.log("getPorpietaire: ", this.idAppartement, prop)
                this.spinnerservice.hide()
            });

        } else {
            this.appartementService.getPropAppartements(this.idAppartement, 1).subscribe((prop: any) => {
                this.listProp = prop.data;
                ////console.log("getPorpietaire APPARTEMENT: ",this.listProp[0] )

                if (this.id == 0 && this.idAppartement != 0) {
                    this.formReglement.patchValue({
                        proprietaire: this.listProp[0].id,
                    });
                    //set default avance
                    this.avanceClient = +this.listProp[0]?.avance?.toFixed(3);
                    ////console.log("set default user in new Reglm: ",this.formReglement?.value?.proprietaire)
                    //check msg information avance && échéance
                    this.onchangeProprietaire(this.listProp[0]);

                }
                this.spinnerservice.hide()
            });
            this.spinnerservice.hide()
        }
    }

    getlistCooproprietaire() {
        this.spinnerservice.show()
        this.listProp = [];
        if (this.idAppartement == undefined || this.idAppartement == null || this.idAppartement == 0) {
            this.proprietaireService.getAllCoProprietaires().subscribe((prop: any) => {

                this.listProp = prop.data;
                ////console.log("getCooproprietaire: ",prop)

                this.spinnerservice.hide()
            });
            this.spinnerservice.hide()
        } else {
            ////console.log("this.idApp: ",this.idAppartement)

            this.appartementService.getCoPropAppartements(this.idAppartement, 2).subscribe((prop: any) => {
                this.listProp = prop.data;
                ////console.log("getCoPropAppartements APP: ",prop)
                if (this.id == 0 && this.idAppartement != 0) {
                    ////console.log("set default user in new Reglm: ",this.formReglement)
                    this.formReglement.patchValue({
                        proprietaire: this.listProp[0].id,
                    });

                    //set default avance
                    this.avanceClient = +this.listProp[0].avance.toFixed(3);
                    ////console.log("set default user in new Reglm: ",this.formReglement?.value?.proprietaire)

                    //check msg information avance && échéance
                    this.onchangeProprietaire(this.listProp[0]);
                    this.spinnerservice.hide()
                }
                this.spinnerservice.hide()
            });
            this.spinnerservice.hide()
        }

    }


    getReglement(cbmarq) {
        this.spinnerservice.show()
        //this.sommeAPayer=0;

        this.reglementService.getReglementbyCbmarq(cbmarq).pipe(first())
            .subscribe((x: any) => {
                ////console.log("detttttt",x)
                //checkbox prop/cooprop
                if (x?.data?.prop?.proprietaire || x?.data == null) {
                    this.isCooproprietaire = false;
                    this.getlistPropiretaire();
                } else if (x?.data?.prop?.proprietaire == false) {
                    this.isCooproprietaire = true;
                    this.getlistCooproprietaire();
                }
                this.selection = x?.data?.typePayement?.code;

                if (this.modeEdit) {
                    /*** get avance information from reglement */
                    this.avanceClient = x?.data?.avance;
                    if (this.avanceClient != 0) {
                        this.useAdvance = true;
                    } else {
                        this.useAdvance = false;
                    }
                    /******/
                    this.formReglement.patchValue({
                        date: x?.data?.date,
                        proprietaire: x?.data?.prop?.id,
                        montant: x?.data?.montant,
                        dateCheque: x?.data?.dateCheque,
                        referenceCheque: x?.data?.referenceCheque,
                        typePayement: x?.data?.typePayement?.cbmarq,
                    });
                    this.spinnerservice.hide()
                }

                if (!this.modeEdit) {

                    this.reglementService.getlistFrais(this.formReglement.value.proprietaire).subscribe((frais: any) => {
                        this.listAppartements = frais.data;

                        this.tranche = (this.formReglement.value.montant / this.listAppartements?.length).toFixed(3);
                        if (this.listAppartements != null) {

                            Object?.keys(this.listAppartements).forEach(n => {
                                this.addLigne();
                                var lig: any = this.listAppartements[n];

                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idBien').patchValue(<number>lig.bien.cbmarq);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeReglement').patchValue(lig.typePayement.cbmarq);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeBien').patchValue(lig.typeBien);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.bien.Intitule);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('solde').patchValue(lig.montant.toFixed(3));
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);

                            });
                        }

                        this.spinnerservice.hide()
                    });

                } else {
                    ////console.log('prp', this.formReglement.value)

                    Object?.keys(x.data).forEach(name => {
                        if (name == 'lignes') {
                            this.test = x.data[name];
                            Object?.keys(this.test).forEach(n => {
                                this.addLigne();
                                var lig = this.test[n];
                                ////console.log('test', this.test);

                                ////console.log("this.test.appartement.cbmarq: ",this.test[0]?.appartement?.cbmarq);

                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idBien').patchValue(<number>lig?.appartement?.cbmarq);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeReglement').patchValue(lig?.type?.cbmarq);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeBien').patchValue(lig?.typeBien);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.appartement.intitule);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('solde').patchValue(lig?.montant);
                                ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(lig?.montant);
                            });
                        }
                    });
                }
                this.spinnerservice.hide()
            });
    }

    getPayementType() {
        this.reglementService.getTypePayement('TPY').subscribe((prop: any) => {
            this.typesPayment = prop.data;
            this.typesPayment.reverse();;
        });
    }

    datereglement(event) {
        //console.log(event.target.value);
        this.formReglement.value.date = this.datepipe.transform(event.target.value, 'shortDate');
    }

    getTypeFrais() {
        this.reglementService.getTypePayement('TRG').subscribe((prop: any) => {
            this.listtypeFrais = prop.data;
        });
    }

    changeMontant(event) {
        if (this.modeEdit) {
            ////console.log('change montant on edit',event);

            if (this.useAdvance && event < this.avanceClient) {

                this.formReglement.controls['montant'].setErrors({'incorrect': true});
                this.contrainteMinAvance = true;

            } else {

                this.contrainteMinAvance = false;
            }


            this.tranche = (event / this.formReglement.get('lignes')?.value?.length).toFixed(3);

            Object?.keys(this.formReglement.get('lignes')?.value?.length).forEach(n => {
                var lig: any = this.formReglement.get('lignes')?.value?.length[n];

                if (((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('selected').value) {
                    ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                } else {
                    ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(0);
                }
            });
        } else {
            ////console.log('change montant on add ',event);
            ////console.log("convert this.listAppartements: ",this.listAppartements)
            if (this.useAdvance && event < this.avanceClient) {

                this.formReglement.controls['montant'].setErrors({'incorrect': true});
                this.contrainteMinAvance = true;

            } else {
                //this.formReglement.controls['montant'].setErrors(null);
                this.contrainteMinAvance = false;
            }


            if (this.listAppartements != undefined) {

                this.tranche = (event / this.listAppartements?.length).toFixed(3);
                Object?.keys(this.listAppartements).forEach(n => {
                    var lig: any = this.listAppartements[n];

                    if (((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('selected').value) {
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                    } else {
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(0);
                    }
                });

            }
        }
    }

    changemodeRegl(event) {
        // this.selection = event.label;
        this.selection = event.code;
        console.log('event', event , this.selection === 'E0034')
        const echeance_cheque = this.formReglement.controls['dateCheque'];
        const reference_cheque = this.formReglement.controls['referenceCheque'];

        if (this.selection === 'E0034') {
            reference_cheque.setValidators([Validators.required]);
            // echeance_cheque.setValidators([Validators.required]);
            echeance_cheque.clearValidators();
        }
        if (this.selection === 'E0033' || this.selection === 'E0071') {
            echeance_cheque.setValidators([Validators.required]);
            reference_cheque.setValidators([Validators.required]);
        }
        if (this.selection === 'E0032') {
            echeance_cheque.clearValidators();
            reference_cheque.clearValidators();
        }
        echeance_cheque.updateValueAndValidity();
        reference_cheque.updateValueAndValidity();
    }

    onchangeProprietaire(event) {

        // init var
        this.sommeAPayer = 0;
        this.useAdvance = false;
        this.initVarAvance();
        //finish init var
        ////console.log("onchangeProprietaire: ",event)
        this.propSelected = event?.id;

        ////console.log("propSelected: ",event)
        /*==  select Avance ==*/
        if (event == null || event?.avance == null) {
            this.avanceClient = 0;

        } else {
            this.avanceClient = +event.avance.toFixed(3);

        }
        /*=======*/
        const arr = this.formReglement.controls.lignes as FormArray;
        while (0 !== arr.length) {
            arr.removeAt(0);
        }
        this.reglementService.getlistFrais(this.propSelected).subscribe((frais: any) => {

                ////console.log("onchangeProprietaire getfrais: ",frais)
                if (this.idAppartement != null && this.idAppartement != 0) {
                    this.listAppartements = frais.data?.filter((filterApp: any) => filterApp?.bien?.cbmarq == this.idAppartement);
                    if (this.listAppartements != null) {
                        Object?.keys(this.listAppartements).forEach(n => {
                            this.addLigne();
                            var lig: any = this.listAppartements[n];

                            //add somme a payer
                            this.sommeAPayer = this.sommeAPayer + (+lig.montant.toFixed(3));
                            this.isAlertVisible = true;
                            ////console.log("sommeAPayer: ",this.sommeAPayer)
                            this.alertModel.message = 'Montant à payer ' + this.sommeAPayer.toFixed(3);
                            this.alertModel.type = 'info';

                            //

                            ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idBien').patchValue(<number>lig.bien.cbmarq);
                            ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeReglement').patchValue(lig.typePayement.cbmarq);
                            ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeBien').patchValue(lig.typeBien);
                            ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.bien.Intitule);
                            ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('solde').patchValue(lig.montant.toFixed(3));
                            ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                        });
                    }
                } else {
                    this.listAppartements = frais.data;
                    ////console.log('not mode appartement',frais)
                    if (this.listAppartements != null) {


                        Object?.keys(this.listAppartements).forEach(n => {
                            this.addLigne();
                            var lig: any = this.listAppartements[n];

                            //add somme a payer
                            this.sommeAPayer = this.sommeAPayer + (+lig.montant.toFixed(3));
                            this.isAlertVisible = true;
                            this.alertModel.message = 'Montant à payer ' + this.sommeAPayer.toFixed(3);
                            this.alertModel.type = 'info';
                            //

                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idBien').patchValue(<number>lig.bien.cbmarq);
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeReglement').patchValue(lig.typePayement.cbmarq);
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('typeBien').patchValue(lig.typeBien);
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('intitule').patchValue(lig.bien.Intitule);
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('solde').patchValue(lig.montant.toFixed(3));
                        ((this.formReglement.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.tranche);
                    });
                } else {
                    this.isAlertVisible = true;
                    this.alertModel.type = 'info';
                    this.alertModel.message = frais.message;
                }
            }
            this.spinnerservice.hide()
        },
            err => {
            }
        );
    }

    newligne(): FormGroup {
        return this.formBuilder.group({
            selected: new FormControl(true,),
            typeBien: new FormControl(null, Validators.required),
            idBien: new FormControl(null, Validators.required),
            intitule: new FormControl('', Validators.required),
            solde: new FormControl('', Validators.required),
            montant: new FormControl('', [Validators.required, , Validators.min(0)]),
            typeReglement: new FormControl(null, Validators.required),
        });
    }

    lignes(): FormArray {
        return this.formReglement.get('lignes') as FormArray;
    }

    addLigne() {
        this.lignes().push(this.newligne());
    }

    removeLigne(ligIndex: number) {
        this.lignes().removeAt(ligIndex);
    }

    checkCheckBoxvalue(event, index) {
        if (event.target.checked) {
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).enable();
        } else {
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montant').patchValue(0);
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('typeBien').disable();
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('idBien').disable();
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montant').disable();
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('solde').disable();
            ((this.formReglement.get('lignes') as FormArray).at(parseInt(index)) as FormGroup)?.get('typePayement')?.disable();
        }
    }

    onSelectAutrePerson() {
        // //console.log("isCooproprietaire: ",this.isCooproprietaire)

        if (!this.isCooproprietaire) {
            this.getlistPropiretaire();
        } else {
            this.getlistCooproprietaire();
        }

    }


    closeAlert() {

        this.alertModel = new Alert();
        this.isAlertVisible = false;
    }

    onUseAdvance() {

        if (this.useAdvance) {
            this.formReglement.patchValue({
                montant: this.avanceClient,
            });

            this.changeMontant(this.avanceClient);

        } else {
            this.initVarAvance();
        }


    }

    initVarAvance() {
        this.formReglement.patchValue({
            montant: 0,
        });

        this.changeMontant(0);
    }


}
