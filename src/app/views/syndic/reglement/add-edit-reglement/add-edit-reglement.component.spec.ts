import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditReglementComponent } from './add-edit-reglement.component';

describe('AddEditReglementComponent', () => {
  let component: AddEditReglementComponent;
  let fixture: ComponentFixture<AddEditReglementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditReglementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditReglementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
