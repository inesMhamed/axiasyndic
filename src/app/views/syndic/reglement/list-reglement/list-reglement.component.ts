import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Reglement } from 'src/app/shared/models/reglement.model';
import { ReglementService } from 'src/app/shared/services/reglement.service';
import { ElementFlags } from 'typescript';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {Permission} from '../../../../shared/models/permission.model';
import {DroitAccesService} from '../../../../shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-reglement',
  templateUrl: './list-reglement.component.html',
  styleUrls: ['./list-reglement.component.scss']
})
export class ListReglementComponent implements OnInit {
  displayedColumns: string[] = ['reference', 'proprietaire', 'montant', 'montantPaye', 'statut', 'action'];
  dataSource = new MatTableDataSource<Reglement>([]);
  @Input() id: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  listreglement: any = [];
  cbmarq: string;
  modeAppartement: boolean;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  can_confirm: Permission;
  id_current_user: any;
  pageSize: any;

  have_access:boolean =false;

  constructor(private preferenceService: PreferencesService, private reglementService: ReglementService,
    public router: Router, private route: ActivatedRoute,
    private spinnerservice: NgxSpinnerService,
    private toastr: ToastrService, private permissionservice: DroitAccesService
  ) { }

  ngOnInit(): void {
    // this.spinnerservice.show()
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000062');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000063');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000064');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000065');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000066');
    this.can_confirm = this.permissionservice.search( this.id_current_user, 'FN21000067');

    this.checkAccess();

    // this.getlistReglement()
    if (this.router.url.includes('/syndic/details-reglement/')) {
      this.spinnerservice.show()
      this.reglementService.getReglementbyProp(this.id).subscribe((reg: any) => {
        if (reg.statut) {
          this.dataSource.data = reg.data.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, proprietaire: el.prop.nomComplet, montant: el.montant, mode: el?.typePayement?.label, statut: el.statut }))
          this.spinnerservice.hide()
        }
      });
    } else {
      this.spinnerservice.show()
      this.reglementService.getReglements().subscribe((res: any) => {
        console.log(res)

        if (res.statut == true) {
          this.listreglement = res.data
          this.dataSource.data = res.data.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, proprietaire: el.prop.nomComplet, montant: el.montant, mode: el?.typePayement?.label, statut: el.statut }))
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.spinnerservice.hide()
        } else {
          this.spinnerservice.hide()
        }
      });
    }
    this.route.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
      this.modeAppartement = true;
    });

  }

  checkAccess(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000066').subscribe((res:any)=>{

     this.have_access =res.data;
    
   },error=>{},()=>{

     if(!this.have_access){
       this.router.navigate(['403']);
     }
    
   });
 }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getlistReglement() {
    // this.spinnerservice.show()
    this.reglementService.getReglements().subscribe((res: any) => {
      this.listreglement = res.data;
      this.dataSource.data = res.data.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, proprietaire: el.prop.nomComplet, montant: el.montant, mode: el?.typePayement?.label, statut: el.statut }))
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinnerservice.hide()
    })
  }
  editerReglement(id) {
    this.spinnerservice.show()
    this.reglementService.getReglementbyCbmarq(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0036") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier règlement confirmé ! ',
        })
        this.spinnerservice.hide()
      }
      else if (data.statut.code == "E0037") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier règlement annulé ! ',
        })
      } else {
        this.router.navigate(['/syndic/modifier-reglement/' + id])
        this.spinnerservice.hide()
      }

    })

  }
  AjouterReglement(){
    this.router.navigate(['/syndic/ajouter-reglement'])

  }
  getDetails(id) {
    this.router.navigateByUrl('/syndic/reglements', { skipLocationChange: true }).then(() => {
      this.router.navigate([`/syndic/details-reglement/${id}`]);
    });

  }

  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer une règlement',
      text: 'Voulez-vous vraiment supprimer ce règlement ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.reglementService.deleteReglement(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistReglement()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
}
