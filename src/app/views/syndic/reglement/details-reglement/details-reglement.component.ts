import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReglementService } from 'src/app/shared/services/reglement.service';
import { first } from 'rxjs/operators';
import { Reglement } from 'src/app/shared/models/reglement.model';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as pdfmake from 'pdfmake/build/pdfmake';
import { Preferences } from 'src/app/shared/models/preferences.model';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { Permission } from '../../../../shared/models/permission.model';
import { DroitAccesService } from '../../../../shared/services/droit-acces.service';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

class Alert {
  type: string;
  message: string;
}


@Component({
  selector: 'app-details-reglement',
  templateUrl: './details-reglement.component.html',
  styleUrls: ['./details-reglement.component.scss'],
  providers: [DatePipe]
})
export class DetailsReglementComponent implements OnInit {
  cbmarq: any
  refreObj: any
  clientObj: any

  reglement: any = [];
  lignes: any[] = [];
  depenses:any[] = [];
  aTens = ["vingt", "trente", "quarante", "cinquante", "soixante", "soixante-dix", "quatre-vingt", "quatre-vingt-dix"];
  aOnes = ["zéro", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix-sept", "dix-huit", "dix-neuf"];
  prop: any = [];
  sourceFrais: any[] = [];
  can_confirm: Permission;
  id_current_user: any;
  residence: any;
  role: any;
  check_role: boolean = false;

  have_access:boolean =false;

  hasDuplicateCopiePrint:boolean=false;
  noCopieToPrint:boolean=false;
  nameDuplicatedCopie:string="";
  nameDuplicatedCopiePdf:string="";
  nbrDuplicatedCopiePdf:number=0;
  alertModel:Alert=new Alert();

  constructor(private actRoute: ActivatedRoute, private reglementService: ReglementService,
    private spinnerservice: NgxSpinnerService,
    private preferenceService: PreferencesService, private toastr: ToastrService, private datePipe: DatePipe,
    private router: Router, private permissionservice: DroitAccesService,
  ) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.role = localStorage.getItem('role')
    this.can_confirm = this.permissionservice.search(this.id_current_user, 'FN21000067');
    if (this.role.includes('ROLE_RESIDENT')) {
      this.check_role = false;
    } else {
      this.check_role = true;
    }
    this.actRoute.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });

    this.checkAccess();


    if (this.cbmarq) {
      this.getReglement();

    }
    this.clientObj = {
      prop: this.prop,
      reglement: this.reglement,
      datedoc: this.reglement.date,
      lignes: this.lignes,
    }
    this.preferenceService.getPreferences(1)
      .pipe(first())
      .subscribe(
        (ref: any) => {
          this.refreObj = {

            cbmarq: ref.data.cbmarq = 1,
            intitule: ref.data.intitule,
            matriculeFiscale: ref.data.matriculeFiscale,
            adresse: ref.data.adresse,
            telephone: ref.data.telephone,
            round: ref.data.round,
            telephone1: ref.data.telephone1,
            compteBanque: ref.data.compteBanque,
            devise: ref.data.devise,
            affichageTableaux: ref.data.affichageTableaux
          }

        }
      )
  }

  getReglement(){

    this.lignes=[];
    this.depenses=[];
    this.reglement=[];

    this.reglementService.getReglementbyCbmarq(this.cbmarq).subscribe((reg: any) => {
      if (reg.statut === true) {
        this.reglement = reg?.data;
       

        /*** diffirencier échéance / depense */
        reg?.data?.lignes.forEach(lig => {
          if(lig?.type?.code!="E0061"){
            this.lignes.push(lig);
          }else{
            this.depenses.push(lig);
          }
        });

        //console.log("inn get reglement: ", this.reglement)
        //console.log("get reglm: ligne: ",this.lignes);
        //console.log("depenses: ",this.depenses)




        /****/
       
        this.sourceFrais = reg.data.lignes.sourceFrais;
        this.prop = reg.data.prop
      }
      this.spinnerservice.hide()
    }, error => { }, () => {

      this.reglementService.checkDuplicate(this.cbmarq).subscribe((res:any)=>{
        if(res.statut ){
          this.hasDuplicateCopiePrint=res.data;
          if(res.data && this.reglement.duplicate>=0){           
            this.nameDuplicatedCopie="- Duplicata "+(this.reglement.duplicate+1);
          }else if(!(res.data) && this.reglement.duplicate>=0){
            this.nameDuplicatedCopie="- Duplicata "+(this.reglement.duplicate);
          }

          //console.log("hasDuplicateCopiePrint: ",this.hasDuplicateCopiePrint)

        }
        this.spinnerservice.hide()
      })

    })

  }
    
  checkAccess(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000065').subscribe((res:any)=>{

    this.have_access =res.data;
    
  },error=>{},()=>{

    if(!this.have_access){
      this.router.navigate(['403']);
    }
    
  });
  }

  // convert nombres décimaux en lettres //////////////////////////
  ConvertToHundreds(num) {
    var cNum, nNum;
    var cWords = "";

    num %= 1000;
    if (num > 99) {
      /* Hundreds. */
      cNum = String(num);
      nNum = Number(cNum.charAt(0));
      cWords += this.aOnes[nNum] + " cent(s)";
      num %= 100;
      if (num > 0)
        cWords += " "
    }

    if (num > 19) {
      /* Tens. */
      cNum = String(num);
      nNum = Number(cNum.charAt(0));
      cWords += this.aTens[nNum - 2];
      num %= 10;
      if (num > 0)
        cWords += "-";
    }

    if (num > 0) {
      /* Ones and teens. */
      nNum = Math.floor(num);
      cWords += this.aOnes[nNum];
    }

    return cWords;
  }
  /* function convert chiffre en lettre*/
  ConvertToWords(num) {
    var aUnits = ["mille(s)", "million(s)", "milliard(s)", "trilliard(s)", "quadrillion(s)"];
    var cWords = (num >= 1 && num < 2) ? "dinar et " : "dinars et ";
    var nLeft = Math.floor(num);
    for (var i = 0; nLeft > 0; i++) {
      if (nLeft % 1000 > 0) {
        if (i != 0)
          cWords = this.ConvertToHundreds(nLeft) + " " + aUnits[i - 1] + " " + cWords;
        else
          cWords = this.ConvertToHundreds(nLeft) + " " + cWords;
      }
      nLeft = Math.floor(nLeft / 1000);
    }
    num = Math.round(num * 100) % 100;
    if (num > 1)
      cWords += this.ConvertToHundreds(num) + " millimes";
    else if (num == 1)
      cWords += this.ConvertToHundreds(num) + " millime";
    else
      cWords += "zéro millime";

    return cWords;
  }
  //convert to pdf
  downloadAsPDF() {

   // this.reglementService.getReglementbyCbmarq(this.cbmarq).pipe(first()).subscribe((x: any) => {

         /** nomination duplicated copy */
        if(this.reglement.duplicate>=0 && this.hasDuplicateCopiePrint){
          this.nameDuplicatedCopiePdf="- Duplicata "+(this.reglement.duplicate+1);
        }
        else if(this.reglement.duplicate>=0 && !this.hasDuplicateCopiePrint){
          this.nameDuplicatedCopiePdf="- Duplicata "+(this.reglement.duplicate);
        }

            
        let reglm=this.reglement;

       
        this.clientObj = {
          reference: reglm.reference ,
          reglement: reglm,
          prop: reglm.prop?.nomComplet,
          lignes: reglm.lignes
        }
        let body = reglm.lignes.map(ligne => {

          return [
            {
              text: ligne?.appartement?.type?.label + ' ' + ligne?.appartement?.intitule + ':  ' + ligne?.sourceFrais?.intitule, alignment: 'left'
            },
            { text: ligne?.montant, alignment: 'center', },
          ]

        })
        var docDefinition = {
          footer: function (page, pages) {
            return {
              margin: [35, 0, 35, 0],
              height: 20,
              table: {
                widths: ['98%'],
                heights: [10.25],
                body: [[{ text: '', style: 'filledHeader' }]]
              }
            }
          },
          info: {
            title: 'Règlement N°' + this.clientObj?.reference ,
            author: 'axiasolution',
            subject: 'axiasolution',
            keywords: 'axiasolution',
          },

          content: [
            {
              table: {
                widths: ['*'],
                heights: [10.25],
                body: [[{ text: '', style: 'filledHeader' }]]
              }
            },
            {
              table: {
                widths: ['70%', '30%'],
                body: [
                  [{
                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH0AAAA9CAYAAACA2GZJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABFGSURBVHhe7ZwLmBXFlceregaY4aEXGI0agYtIcEmMotmFYcYo6CYxMasE42fig8QIH7irRPNtNiY6c2eIWRVDFAWVUR5fiIkaYE1IIm5Q1HmgxAcxiC+WC+ii4ZFBgQFmbtf+Tt9ynPF23zeLhv5D3a6qrq7HOadOnVNdPSpEiBAhQoQIESLExxPaXkOkQbS6pozLHVrpKcmc7jBKbYaU58UbYy/ZrI80HHsNcRghZPphiJDphyFCph+GCJmeDYz3L8FPu19QxnR4JT4mKLHXEGkQGTROvJye+DpvcX3GJ7Qoo5tat6zaTTxEiBAhQoQ4FChoRy46JoYp6JZgDpZSVQ/lqqGkj1Ou7k/NfZU2m5XrvMn9TRg7GDzqQLy5Pvns2Bppu5dydJAxmYg31u2XSLQyRhtG1tT0/e3QbfHVsYwGVfRU+t3Hq49+p4GrOhgXfY7R39qeyXH6QIw4o/dRLmtjLjo25kAfoVkZFQxSJQSljie0EnarhLOZ9uLU3A4927uOK1pd29tGU+Gq9nhzXbtN+SJvpkdH1ZaqcvVpOj6WWj5PVZ+nsuPs7W6gt3TeLCf6awawJt5StxemD+G5WUrrE5KlUvA0TL9GIjB9JMIUo/yJtBPcZ1d9lwE/aVOBiI6pORoT9vtUdbbNSoUx+2Dk/YRFjFHEjfJqor3bHUa9TbmrYfobNicQ0eoYDDNDeGY0yfMJ1YyowrvZBSbpDWwk/J6whD48H2+sf1fuwfQ1XPwF0FULoMFsm/JFXkyPVtecxiCFYJfR4ZOTuZnBKN7kZyHP/gImumQtg5ojk3dT8LuNjXXn2biKVtVM5nK71jpQyql/NUM6P94Y+6vNSgFEL1PGnUS0jro+kcxNhTHmEZVQ31P7nA2qr1vw3jvaxUG7nAzFLyD5L4RTIH5W3hP8R1OqxUyY+1Sb3qT7IpDiTfiAsrcgHD+wSV/k5KfT8Z6oufOp+U46/+NcGC5gkMfzw4wxcxnAaTY7OyScxbR7h035wyiE0Uy3KX8YM5yeTErPcPUiZWYrx4nHX2QpKBCo8l4wXBg9l/AD6HBatgwXIGxD+L0Wbt2seplB9C+vyfo+sma6MFz1Nv9KcxBej6FVX0nLBPvcmURmcfVdDvzAmtamjPMT45pf2yw/9IBlF6OJvmjT3RCtih3B5XLC57wMHzDD/5ffu+hpI+o6YbPzBgwvp75pRO9gzEI30Ro5gwkmGm4CSv1+rultkQzIiunRqlqNpH6LTt9A40MIBe3kyfOEY5HgiM3KCvGW2G5Mq+thzCs2qxuok396MMvHd6JjagfabA+e8eaakygxjXIIRyqo9wDa4hG1x1kcb4odsNl5w2tTmfFQawZtDobhhdKtJ+Ecb5wFIGMnPMvZqC8RvY7GBiRzgwHh9spsYW3ZbMPbBM8KLwpKnTjD/xEqbpvN6QaoUcrPWfx+g/X7A+b2Mf3xAGZyv4/N6Qb6mOC5laj0m1HpxelvuRkNhR+kzb42JxDQrK0b3UTjmCLSrQsyS542x0KMq2D4CJvjCzraSkfFQr+ZxNVcp9ownfRM7q0k7CVdEFC54katJDqb+v6WzO0OiHwUbV4M0braHNewjuNlBMCoDfzWYYxhlBUOBO4TrNoLgoTMAwOQMRBWkLqFIN5Kkm7QEANxFnR9XGhLXtGQ1phgPZL7E6AiBAteR5h167jMpKOz8MuX45Ktb9385Bs2rIsMHPeUKjXN6KdNlB4N8VnnMuJ1nn3Axruhdcuq/ZHjz/wfokMg6kjq9RPeYwjvRQaPey4y+Kx/Iv4zxuDfrgijwd1rqhfipyAy+EwZ+7lo1dOTOSnYhag90Lp5VafXEBl05ve5TKDNwIkFU9/g50boO4cl5bfxNXXrOum25cn1kSHjVlGqmaJS7zDaz6hpQRPP/9HGfZFhppsyOjSVjgcaHx7DjboOI6sh3ly/xW9zBHWZiLfUv85aew9+5EQeKvzFRJvzFsxmJug/2ZxuoM9iME6irUp6KZ5GIMGMq3+IsC61yYLBZPkklwsJwQw35nVcwq/T7s8Rto1CI3urE2idBO7Xa0TnQDtZ0kQbFYz0TDfqRIglmwi+oOPbUf83xZvrHkPtZlx/KHMA5q8yWl8ses1m5wWIZOJNdc/Tviwdb9rsbqDvx6ADfsf1DJvVHdIJY5aw1t+FsKbdxcoN7leo/BjaRRGlgjbfVQfUOfHV9Wtpl+UqPWB8G2N9kAfn8+x7NjtvpGe6o75pYykQanF5nA79MpmTA4x5nN+izCzaX4pwLqY7bTYrazCAF5ltN6BaC3bN3gcWu3gQ1YhcP5vlh7nooXdsPHu4zt10+jlL+7yRnumyxRoI7aLS59lEbtAOWkEvhvnFmV2u858QQ4zIrAHZsAl0Ldb6G8XYgOlEmRJX8Xhmub9bqDA+jVqGoOVsmaMVxHB9hCC7mXkjw5quZG0KgqHpFhvPCaxVdNq8hrhutVkFAWK8y1DEf3/WZqWFR3hl7oXpTyS9gSKiVEVQ6sEbMEa9xv0deQuadmS38CDOdKUH20gKkGQDsfN3wbTqIBTtpAnM24Cqxj3EzsgEox5FYBfGm2LFP+li1NGEYDctKeiyd54fDnie0sFjOlbIx+s4leOswZr33sylhVYnMLKhNlVcaNwrrfbYVCqM6kXIpGEDEV8T2yYTzibzQtrGUYONNpoC1kQdrYydZJMfCTDbMcj0Cvr203SGHZYWHomeHq2qHZzcKi0iDBpMFr4gOGoEHTiy6O3mgEwSl87CdHB1LrHxjwxQ2TuRSDEwV2QwFOXd+GTV26SzsnNHQraHWWKM8WU8AifnB0ar3m5BL00KQXqmi5UZDPFBJ0arYp9NJg8tFjzb5wPm7XVkp2s2OvAvNicFdF5OzkxDcC+KVnuneIoC7BxR7S/RdrALqdVVKqH729T/OzIxfWWQYWQ3Hk6kUG20OibHfA4ZGloqRnS0ly2b1zTwroamAaM2voB34DhN9HAuS1Sgh8AAxL36sXJ1cZcpVz9J7btsygf6FGyKOaj4dAbfQUN6pjuqFYn9L5tKgeeLanUegjE7OvbQrO8w+gTjmkVEx9GXaZgaTzU0D1x6/a23n/qlycsWkv8wjA+0lpHdY/j5LQwooprXK7G1XoEuvgYXwlbCz4UsLctpN+MbuGIjrVrzjI0y93S6+CutNbM6GAzvJTTDnUQbcSN3cBX/V854lCnjvWDZFm+q69xCRKV+ikvWx6U+jPsbKzSL5nDa+hltfIGhdF8jjWqD2Q/HX/3kTQ/cd8Ec7ehz7B1f0P+HqeNKbALvHFpX5POpMpNgvNLuUuh2pM1KAc/Jf3lxNBMa/TdxcYFl00ZeD4uNfgS/2+KN9d20xtDqWilzcI5LeRsIjvMqA7oXoU37eo9Zf7J21Dw6KW947iXI0SY5HSOz7TG6cwXEy+btWkYseqafdrU7wmhDB/V4QopRxL0D9GXT0JPe3MYYpkKMtfZWEL7MU9fBrOCTpjkAT0K2mu9BmgKNSWacrJLDuNxFEhqp+QShmWy3Cg3/yPUqJl9RXedM1rt0frdyza+IPgTjg/1PC0ZxHNJ9AeFSwrcY1BfIG8YIp+DIDPe0RwFY8ARToaMkigqpIXkB9absfsFgmQnLic2fPHbn35iB8nYK1W82eQV8QB9lfb2SZy6kj8WxrI1zGxok47sJmF8qNIJe53o003oigbFpORY9EUs/8HhXPsjIdA/yGtNVtxH7PZKb5zEiLYcwJqlyt1cynSd6RAawJM6AUhMgjI/m8NbR59CNs3qUux8w2VWrCTFup3sxIwdGpqs+xv+tXI5gwuC66Rp69HOblRMQBuHPZ4hchgYKXCZyRVZMl9eYSO3rar+ezAAeZMbnzHhrvFxDTNbyvNGhS+5mBlyMBgna334Lht/C/Re+PUrs0CTizfWMQS81RgeeqGW2OfTxVKLTcUWHJ3MLhBwccdW/Y2zORuByfsmCYDNJ9FexXcbZrIKR3UwH+J8q/qe6XRDvcvTndTB/Qzbq/kPAlVIzbDwnYJEfiaUuDL/IEyBfiCFkFqDSf3Pl2B2dDH8f8ZY6jDRzH/1+hOD7osWbXVp9kXKXRStrI4UuR8x2afcdqpUZ/x+0+xdCGwKQ0r9gmKP5GY0g5nSQNAhZM70rYPwclVDy2vVm+v4EYSMh0C3i3i6GKIZUA6HWy8wBDc0VMuh6ZqKv9SxgGgsRl5aW7r81meMP+r6BwmIsreWRgF0zz06YSuRrqtwUZX2H+TJh7kBjfp32b8UmWQVd5Msff0OPm0AOmQrd5qqE+aXaowo+QCFgfPmDdQaV7Yqx8Q+ET1PdIGqUs+cnMDAIqreSli3J9ajW9aT/zOC9NRVLXnakzkcNdzuq3AUbcdm8gxYwXTZ/vkII9GmF6UjwQ8xw31M0XREdE+upHPcMhOizaI5AwUeMxJ1ajmoV4RhPX313H5E2NIhehsGY+Q2fRbQyJt/9Jemm9VAqQbjMUdQjWkw2lGQ7dzP5a5XryAmbzkmFy3YtF99+Q4dncdmetklfFMT0roCQpSjdcmqk025EWqezu0m1QYyCT8H+vSJa7blj/byFT7u4i4ii0rvRpPIxZv6vYEOE6IqizfTDFfc1D+httBqJVyBWvyw/Lcq4L6No35tSlf9x9XlN/c9Gg/c3KvEKyrNC/HZU50rqfox6c/YCuiIt06NjayOq5NC9AvxIIqH2q70le3BjPSOwoWlgHZfvEdowzvBmDIw3PyU9e0rVzrTezbzmgRXKJPbBRO8Ez4IXIiXtbSX9YIt4Gc+ThS1jfki6HXvicuINcGxJaVnCbW8rFb+9fcrY7Wle7PgjPdOramooUZBf/XcFph3W3XKYvqST6c0DW8mX3bxnMOjk48qeMGg9cfnDDCO0Mc9xfYr8ia4x74jhyqwdhpH4DgbiP/LsVvIWGUfeweMtyMeVRq/j3k0wZweNyt7GYOo7hbw7HaPEE5pktDOUewiLWd6j3H3026Nasz7rl57p1TVP06Fqmzzsgfsk38/9KN5UL66nB2a6fIEyhrALNS9/lEBodjuMuZR4PUbtKkyzOhh5O4xbQt7ZMH08dT1IPlddCjNvQTh2oinko4zNPPsqZS6hxQ2EqTz7E+qWt4FXaNdcAdsmkpavcU5CULbAxGsnV+34M+mskJeffvhCb4IBL9qEBxh5LgyogWG7YPbnCP+G4zINRj4PA8nWp/MzhXJxir/G/QqYKu8CvkueCMk+SrGE6i/DvKNgrHzTtlZcUO63IhT7uMonWuLTj6LcWdTZk/uXEi6hfH1Cu/KnzrJGeqYbtYeKd4WBkHzL+LLaqztnFKpdPhu+URuYp80EyslM7gHd5ETsszCrDSZHuP/PMPQPMEgbbY5AFjZC3L0kK8nZRz5qXPdDOLYaV4uvL/660F/aGsCsH0TqbfkrKKAUaXiZOsvRDv1c5b5FnTvlRrbwagkChtzZypENgxAwoF0Z55V4c0yOIHtoaBow3Wh9NfeOJbkdavaDoLL/Oz2hEyuwgX9BnnzTJip+EsySNXsGzJrvarPQUY7stlGfqHCFdlAXwfg/MKNHEh/K9UZYtB+m30aZmZR9mLrFSKymvBxaHU65hbQ1Y1pla9Z7IWmZHiI9sLZ7Ym3LixD5UyoDIKZoA/ngYgMq+Fjlqu8wOeUA5jVY2YsamiukLDaSkTMHWyH/ZXItLe+Y39HmjEaVf5U0M13v1lr2282jxPEG9BnEV5aWJ1bTXiXtnAvzZTKugYMrplTu2EI8a4RMLwLubokwGVWvXr3UAaxot6Gp4jwoewO3hjEjH2IRvX5y5faUEzkfxrymiOz5t+PCpf22DmEr7dhX2pc689oICJl+EHBPc/9BqO5TWG//WmJK1k2u2p7r28gQIUKECBEiRIgQIT4Mpf4PR3jLW/AqCkMAAAAASUVORK5CYII=', style: 'filll',
                    fit: [120, 60]
                  },
                  // this.refreObj.gouvernorat
                  { text: 'Sousse , le ' + this.datePipe.transform(new Date(), 'mediumDate'), style: 'date' },
                  ]
                ]
              },
              layout: 'noBorders'
            },
            '\n',
            {

              table: {
                widths: ['50%', '*', '40%'],
                body: [
                  [
                    { text: ' \n', border: [false, false, false, false], color: '#102758', margin: [5, 0, 0, 0], style: 'header' },
                    { text: '', border: [false, false, false, false], },
                    { text: 'Destinataire \n', fillColor: '#f5f6fa', border: [false, false, false, true], margin: [0, 0, 0, 0], bold: true, alignment: 'left', style: 'header' },
                  ],
                  [
                    { text: 'Syndic ' + this.clientObj?.lignes[0].appartement?.produit + ' \n', border: [false, false, false, false], color: '#102758', margin: [5, 0, 0, 0], style: 'header' },
                    { text: '', border: [false, false, false, false], },
                    { text: 'Résidence: ' + this.clientObj?.lignes[0].appartement?.produit + ' \n', fillColor: '#f5f6fa', border: [false, false, false, false], margin: [0, 0, 0, 0], alignment: 'left', style: 'header' },
                  ],
                  [
                    { text: 'Adresse: ' + (this.clientObj?.lignes[0].appartement?.adresse_produit!=undefined?this.clientObj?.lignes[0].appartement?.adresse_produit:'') + '\n', border: [false, false, false, false], color: '#102758', margin: [5, -5, 0, 0], style: 'header' },
                    { text: '', border: [false, false, false, false], },
                    { text: 'Client: ' + this.clientObj.prop + ' \n', fillColor: '#f5f6fa', border: [false, false, false, false], margin: [0, 0, 5, 0], alignment: 'left', style: 'header' },
                  ],
                  [
                    { text: '' , border: [false, false, false, false], color: '#102758', margin: [5, 0, 0, 0], style: 'header' },
                    { text: '', border: [false, false, false, false], },
                    { text: 'Ident/MF: ' + (this.clientObj?.prop?.identifiant!=undefined? this.clientObj?.prop?.identifiant: '') + '\n', fillColor: '#f5f6fa', border: [false, false, false, false], margin: [0, -3, 5, 0], alignment: 'left', style: 'header' },
                  ],
                  [
                    { text: '' ,border: [false, false, false, false], color: '#102758', margin: [5, 0, 0, 0], style: 'header'  },
                    { text: '', border: [false, false, false, false], },
                    { text: 'Téléphone:  ' + (this.clientObj?.prop?.telephone1!=undefined?this.clientObj?.prop?.telephone1:'') + '\n', fillColor: '#f5f6fa', border: [false, false, false, false], margin: [0, -3, 5, 0], alignment: 'left', style: 'header' },
                  ],
                  [
                    { text: '', border: [false, false, false, false], color: '#102758', margin: [5, 0, 0, 0], style: 'header' },
                    { text: '', border: [false, false, false, false], },
                    { text: 'Adresse: '+(this.clientObj?.lignes[0].appartement?.adresse_produit!=undefined?this.clientObj?.lignes[0].appartement?.adresse_produit:'')+'\n', fillColor: '#f5f6fa', border: [false, false, false, false], margin: [0, -3, 5, 0], alignment: 'left', style: 'header' },
                  ],

                ],
              },
              layout: {
                hLineColor: 'white',
                vLineColor: 'white'
              }
            },
            '\n\n',
            {
              text: [
                { text: 'RÈGLEMENT ', fontSize: 18, bold: true, color: '#102758', alignment: 'left' },
                { text: ' N° ' + this.clientObj?.reference, fontSize: 18, color: '#102758', alignment: 'left' },
                { text: this.nameDuplicatedCopiePdf ,fontSize: 18, bold: true, color: '#102758', alignment: 'left'}
              ]
            },

            '\n',

            {
              table: {
                fontSize: '9',
                alignments: ['left', 'center',],
                widths: ['80%', '20%'],
                body: [
                  [
                    { text: 'Désignation ', alignment: 'left', style: 'filledHeader' },
                    { text: 'Montant ', alignment: 'center', style: 'filledHeader' },

                  ]
                ].concat(body)

              },
              layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? '#dee2e6' : '#dee2e6';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? '#dee2e6' : '#dee2e6';
                },
                fillColor: function (rowIndex, node, columnIndex) {

                  return (rowIndex % 2 === 0) ? '#f5f6fa' : null;
                },
                paddingLeft: function (i, node) { return 2; },
                paddingRight: function (i, node) { return 2; },
                paddingTop: function (i, node) { return 4; },
                paddingBottom: function (i, node) { return 4; },
              }
            },
            {
              table: {
                fontSize: '9',
                alignments: ['left', 'center'],
                widths: ['80%', '20%'],
                body: [
                  [
                    //{ text: 'Reste ', alignment: 'left', color: '#102758', style: 'header', border: [true, false, false, true] },
                    //{ text: this.clientObj?.reglement?.rest + ' ' + this.refreObj.devise, alignment: 'center', border: [false, false, true, true], color: '#102758', style: 'header' },

                  ]
                ]

              },
              layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? '#dee2e6' : '#dee2e6';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? '#dee2e6' : '#dee2e6';
                },
              }
            }, '\n', '\n',
            {
              table: {
                widths: ['65%', 'auto'],
                body: [
                  [
                    { text: '' },
                    {

                      table: {
                        body: [
                          [
                            { text: 'Total', style: 'totals' },
                            { text: ' ' },
                          ],
                          [
                            { text: 'Montant Règlement : ', style: 'totals' },
                            { text: this.clientObj?.reglement?.montant.toFixed(this.refreObj.round) + ' ' + this.refreObj.devise, style: 'totals' },
                          ],
                        ]
                      },
                      layout: 'noBorders'
                    }
                  ],

                ],

              },
              layout: 'noBorders'
            },

            {
              margin: [0, -35, 0, 8],
              fontSize: 9,
              text: [
                'Mode de paiement: ',
                { text: this.clientObj?.reglement?.typePayement?.label + '\n', fontSize: 10, bold: true, lignment: 'left' },
                { text: 'Arrête le présent document à la somme de:\n\t\t', fontSize: 9, alignment: 'left' },
                { text: '\t\t' + this.ConvertToWords(this.clientObj?.reglement?.montant.toFixed(this.refreObj.round)), bold: true, margin: [0, 0, 0, 0], fontSize: 10 },
              ]
            },

            {
              table: {
                alignments: ['left', 'center', 'right'],
                widths: ['20%', '60%', '20%'],
                body: [
                  [
                    { text: 'Signature Société', bold: true, italics: true, alignment: 'left', fontSize: 10, border: [false, false, false, false], margin: [10, 45, 0, 0] },
                    { text: '', bold: true, italics: true, alignment: 'center', fontSize: 10, border: [false, false, false, false], margin: [0, 45, 0, 0] },
                    { text: 'Signature Client', bold: true, italics: true, alignment: 'left', fontSize: 10, border: [false, false, false, false], margin: [0, 45, 0, 0] },

                  ]
                ]

              },

            },
          ],
          styles: {
            date: {
              alignment: 'right',
              fontSize: '10',
              color: '#102758',
              fillColor: '#f5f6fa',
              margin: [0, 20, 5, 0]
            },
            table: {
              fontSize: '10',
              color: 'black',
              alignment: 'left',
            },
            filll: {
              fontSize: '10',
              color: '#102758',
              fillColor: '#f5f6fa',
              margin: [0, 0, 5, 0]
            },
            filledHeader: {
              borderColor: '#f5f6fa',
              fontSize: '10',
              color: 'white',
              fillColor: '#102758',
            },

            header: {
              color: '#102758',
              fontSize: '10',

            },
            total: {
              color: 'black',
              fontSize: '10',
              alignment: 'right',
              italics: true,
              margin: [5, 0, 5, 0],

            },
            totals: {
              color: '#102758',
              bold: true,
              fontSize: '10',
              margin: [0, 0, 5, 0],
              alignment: 'right',
              italics: true,

            },

            pageSize: 'A4',
            pageOrientation: 'portrait'

          },
        };

        pdfmake.createPdf(docDefinition).open();
        pdfmake.createPdf(docDefinition).download("Devis Réf: " + this.clientObj?.reference + ".pdf");
     

      this.getReglement();
       
     

  }
  confirmerreglement(id) {
    const swalWithBootstrapButtons = Swal.mixin({})

    swalWithBootstrapButtons.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous voulez vraiment confirmer ce règelement!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.reglementService.confirmReglement(id).subscribe((ress: any) => {
          //this.ngOnInit();
          this.getReglement();
        })
        swalWithBootstrapButtons.fire(

          'Confirmer!',
          'Votre règlement est confirmé',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          'La confirmation est annuler ',
          'error'
        )
      }
    })
  }
  Annulerreglement(id) {
    const swalWithBootstrapButtons = Swal.mixin({})

    swalWithBootstrapButtons.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous voulez vraiment annuler ce règlement!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.reglementService.cancelReglement(id).subscribe((ress: any) => {
          this.ngOnInit()
        })
        swalWithBootstrapButtons.fire(

          'Confrimer!',
          'Votre règlment est annulé',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          'La confirmation est annulé ',
          'error'
        )
      }
    })
  }


  checkDuplicate(){

    this.reglementService.checkDuplicate(this.cbmarq).subscribe((res:any)=>{
      //console.log("res checkduplicate: ",res)
      this.hasDuplicateCopiePrint=res.data;
      if(res.data ){ 
        
        this.downloadAsPDF();
        this.updateDuplicate();
       
      }else{
        this.noCopieToPrint=true;
        //console.log("Vous n'avez pas le droit d'imprimer d'autre copie")

        this.alertModel.message="Vous n'avez pas le droit d'imprimer d'autre copie";
        this.alertModel.type="info";
        

      }
    },error=>{},()=>{

    })

  }

  updateDuplicate(){
    this.reglementService.updateDuplicate(this.cbmarq).subscribe((res:any)=>{
    
    },error=>{},()=>{
      this.getReglement();
    })
  }

  closeAlert() {

    this.alertModel=new Alert();
    this.noCopieToPrint=false;
  }

}
