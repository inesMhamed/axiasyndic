import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { Node, Path, Position, Size } from 'src/app/shared/models/tracabilite.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { TransfertService } from 'src/app/shared/services/transfert.service';
import { IOUtils } from 'src/app/shared/utils/io.utils';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-transfert',
  templateUrl: './details-transfert.component.html',
  styleUrls: ['./details-transfert.component.scss']
})
export class DetailsTransfertComponent implements OnInit {
  cbmarq: any;
  transfert: any;
  lignes: any[] = [];
  admin: boolean;
  username: string;
  pageSize: any;
  infoPref: any;
  accepterTransfert: boolean = false;
  id_current_user: any;
  have_access: boolean = false;
  can_edit: Permission;

  /*** tracabilite */

  nodes: Node[] = [];
  nodesTab: Node[] = [];
  paths: Path[] = [];
  popOverVisible:boolean=false;

  private nodeSize: Size = { width: 100, height: 100 };
  private nodePositions: { [nodeId: number]: Position } = {};
  /**** */
  screenHeight: number;
  screenWidth: number;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
   this.screenHeight = window.innerHeight;
   this.screenWidth = window.innerWidth;
}

  constructor(private actRoute: ActivatedRoute, private toastr:ToastrService,
    private transfertService: TransfertService,public router: Router,
    private preferenceService: PreferencesService,private permissionservice: DroitAccesService) { }

  ngOnInit(): void {

    this.id_current_user = localStorage.getItem('id');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000075');


    if (localStorage.getItem('role').indexOf('ROLE_ADMIN') == -1) {
      this.admin = false;
      this.username = localStorage.getItem('username');
    } else {
      this.admin = true;
    }

    
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    this.infoPref=pre.data
    })
    this.actRoute.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });

    this.checkAccessDetail();

    if (this.cbmarq) {
      this.getSuivi();
      this.getTransfert();
 
    }
   
  }

  checkAccessDetail(){
     //if access false ==> go out to 403
     this.permissionservice.getAccessUser( this.id_current_user, 'FN21000077').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }
     
    });
  }

  getTransfert(){
    this.transfertService.getTransfert(this.cbmarq).subscribe((reg: any) => {
      if (reg.statut === true) {
        this.transfert = reg.data;
        this.lignes = reg.data.lignes;
        //console.log('transfert', this.transfert);

      }
      //console.log("username: ",this.username)
    });
  }

  getSuivi(){

    this.nodes=[];
    this.paths=[];
    this.nodePositions=[];

    this.transfertService.getSuivi(this.cbmarq).subscribe((res: any) => {
      if (res.statut === true) {
        //console.log('res.datasuivi: ', res.data);

        res.data.liste.forEach(val => this.nodes.push(Object.assign({}, val)));
        res.data.paths.forEach(val => this.paths.push(Object.assign({}, val)));
        // this.nodePositions=Object.assign({}, res.data.nodes);
        let i = 0;
        let mapped = res.data.nodes.reduce(function (map, obj) {

          map[i + 1] = obj;
          i++;
          return map;
        }, {});

        this.nodePositions = mapped;
        ////console.log("convertArrayToObject: ",mapped)


        ////console.log("res nodePositions: ",Object.assign({}, res.data.nodes))

      }
    }, error => {
    }, () => {

      /*** supp duplication */
      const ids = this.nodes.map(o => o.detail.caisseSource.cbmarq);
      ////console.log("source ids: ",ids)
      this.nodesTab = [];
      this.nodesTab = this.nodes.filter(({id}, index) => !ids.includes(id, index + 1));
      //console.log("nodes tabs: ",this.nodesTab)


    });

  }


  confirmerTransfert(id) {
    const swalWithBootstrapButtons = Swal.mixin({})
    
    swalWithBootstrapButtons.fire({
      title: 'Confirmer un transfert',
      text: "Vous voulez vraiment confirmer ce transfert?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.confirmTransfert(id).subscribe((ress:any)=>{
          //window.location.reload()
          this.getTransfert();
        })
        swalWithBootstrapButtons.fire(
         
          'Confirmer!',
          'Votre transfert est confirmé',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          'La confirmation est annuler ',
          'error'
        )
      }
    })
  }
  AnnulerTransfert(id) {
    const swalWithBootstrapButtons = Swal.mixin({})
    
    swalWithBootstrapButtons.fire({
      title: 'Êtes-vous sûr ?',
      text: "Vous voulez vraiment annuler ce transfert!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.cancelTransfert(id).subscribe((ress:any)=>{
          //window.location.reload()    
          this.getTransfert(); 
        
        })
        swalWithBootstrapButtons.fire(
         
          'Confirmer!',
          'Votre transfert est annulé',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          'La confirmation est annulé ',
          'error'
        )
      }
    })
  }

  acceptTransfert(cbmarq: any) {
    Swal.fire({
      title: 'Accepter un transfert',
      text: 'Voulez-vous vraiment accepter ce transfert ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.acceptTransfert(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true){
            this.getTransfert();
            this.toastr.success("Transfert accepté avec succès", 'Succès!', { progressBar: true });
          }          
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  
  //Intermédiare : Accepter ,Envoyer
  acceptSendTransfertIntermidiare(id,action:string) {

    let actionUpperCase=action[0].toUpperCase() + action.substring(1);
    let actionLowerCase=action.toLocaleLowerCase();
    let actionFaite=action.replace("er","é");

   
    const swalWithBootstrapButtons = Swal.mixin({})
    
    swalWithBootstrapButtons.fire({
      title: actionUpperCase+' un transfert',
      text: 'Vous voulez vraiment '+actionLowerCase+' ce transfert?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.acceptSendTransfertIntermidiare(id).subscribe((ress:any)=>{
          //window.location.reload();
          this.getTransfert();
        })
        swalWithBootstrapButtons.fire(
         
          actionUpperCase,
          'Votre transfert est '+actionFaite,
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          actionUpperCase+' est annuler ',
          'error'
        )
      }
    })
  }


  /**** fct suivi */


  // handleChangeNodePosition(event: { id: number; position: Position }) {
  //
  //   ////console.log("this.screenHeight: ",this.screenHeight," ---  this.screenWidth: ", this.screenWidth)
  //   if(this.screenWidth<411 && this.screenHeight<823){
  //     //console.log("small screen")
  //     event.position.x= event.position.x-20;//1:-20 wz
  //     event.position.y= event.position.y-300;//1:-300 wz
  //   }else{
  //     //console.log("large screen")
  //     event.position.x= event.position.x-170;//1:-170 wz
  //     event.position.y= event.position.y-300;//1:-260 wz
  //   }
  //
  //
  //   /*if(this.screenWidth>411 || this.screenHeight>823){
  //
  //   }else{
  //
  //   }*/
  //
  //
  //   this.nodePositions[event.id] = event.position;
  // }

  handleChangeNodePosition(event: { id: number; position: Position }) {
    this.nodePositions[event.id] = event.position;
  }
  getD(path: Path) {
    const startPosition = this.getNodePosition(path.from, "OUTPUT");
    const endPosition = this.getNodePosition(path.to, "INPUT");
    return (
      startPosition && endPosition && IOUtils.getD(startPosition, endPosition)
    );
  }

  private getNodePosition(
    nodeId: number,
    socketType: "INPUT" | "OUTPUT"
  ): Position | undefined {
    const position = this.nodePositions[nodeId];
    if (!position) {
      return undefined;
    }
    if (socketType === "INPUT") {
      //console.log('position',position);
      return IOUtils.setOffset(position, { x: 0, y: (this.nodeSize.height / 2) });//wz +13
    }
    return IOUtils.setOffset(position, {
      x: this.nodeSize.width,
      y: (this.nodeSize.height / 2)//wz +13
    });
  }

}
