import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditTransfertComponent } from './add-edit-transfert.component';

describe('AddEditTransfertComponent', () => {
  let component: AddEditTransfertComponent;
  let fixture: ComponentFixture<AddEditTransfertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditTransfertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
