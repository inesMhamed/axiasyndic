import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { date } from 'ngx-custom-validators/src/app/date/validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CaisseService } from 'src/app/shared/services/caisse.service';
import { ProprietaireService } from 'src/app/shared/services/proprietaire.service';
import { TransfertService } from 'src/app/shared/services/transfert.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-edit-transfert',
  templateUrl: './add-edit-transfert.component.html',
  styleUrls: ['./add-edit-transfert.component.scss'],
  providers: [DatePipe]
})
export class AddEditTransfertComponent implements OnInit {
  transfertForm: FormGroup
  submitted: boolean
  typeCaisseSource: any
  typeCaisseDestinataire: any
  listCaisseSource: any
  responsableSource: any
  responsableDestinataire: any
  listCaisseDestinataire: any
  modeEdit: boolean = false
  listResponsableSource: any;
  listResponsableDestinataire: any;
  listReglement: any;
  montant = 0;
  changeCaisse: any;
  changeCaisseDest: any;
  disable: boolean;
  type: number;
  listtypetransfert: any;
  cbmarq: any;
  test: any;
  role: any;
  admin: boolean;
  listTransporteur: any[] = [];
  destinationIsRequired: boolean = true;//false
  transfertModel: any;

  constructor(private formBuilder: FormBuilder, private caisseService: CaisseService,
    private datePipe: DatePipe,private spinnerservice: NgxSpinnerService,
    private route: ActivatedRoute, private router: Router, private toastr: ToastrService,
    private transfertService: TransfertService, private proprietaireService: ProprietaireService, private utilisateurservice: UtilisateurService
  ) { }

  ngOnInit(): void {
    //this.getTypeCaisseSource('TCS')
    //this.getTypeCaisseDestinataire('TCD')
    this.spinnerservice.show()
    this.getCaisseSource();
    this.getCaisseDestination();
    this.getTransporteur();
    this.getTypeTransfert()
    this.role = localStorage.getItem('role')
    this.transfertForm = this.formBuilder.group({
      date: new FormControl(new Date(), Validators.required),
      typeCaisseSource: new FormControl(null, Validators.required),
      idCaisseSource: new FormControl(null, Validators.required),
      userSource: new FormControl(null, Validators.required),
      typeCaisseDestination: new FormControl(null),//, Validators.required
      idCaisseDestination: new FormControl(null, Validators.required),
      userDestination: new FormControl(null, Validators.required),
      montant: new FormControl('', [Validators.required, Validators.min(0)]),
      lignes: this.formBuilder.array([]),
      userIntermediere : new FormControl(null),
    })
    this.route.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });
    if (localStorage.getItem('role').indexOf('ROLE_ADMIN') == -1) {
      this.admin = false;
      ////console.log('bonjour')
      this.transfertForm.patchValue({
        userSource: localStorage.getItem('id')
      });
    }
    else {
      this.admin = true;
    }
    if (this.cbmarq != null) {
      this.modeEdit = true;
      this.transfertService.getTransfert(this.cbmarq).subscribe((trns: any) => {

        this.transfertModel=trns.data;

        this.transfertForm.patchValue({
          date: trns.data.date,
          typeCaisseSource: trns?.data?.typeCaisseSource?.cbmarq,
          typeCaisseDestination: trns?.data?.typeCaisseDestination?.cbmarq,
          idCaisseSource: trns?.data?.caisseSource?.cbmarq,
          idCaisseDestination: trns?.data?.caisseDestination?.cbmarq,
          userSource: trns?.data?.userSource?.id,
          userDestination: trns?.data?.userDestination?.id,
          montant: trns?.data?.montant,
          userIntermediere:trns?.data?.userIntermediere?.id!=undefined? trns?.data?.userIntermediere?.id : null,

        });

        //on change mode edit
        /*Object.keys(trns.data).forEach(name => {
          if (name == 'lignes') {
            this.test = trns.data[name];
            Object.keys(this.test).forEach(n => {
              var lig = this.test[n];
              //console.log("lig: ",lig);
              ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('idSource')?.patchValue(<number>lig?.source);
              ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('type')?.patchValue(lig?.type?.cbmarq);
              ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup)?.get('montant')?.patchValue(lig.montant);
            })
          }
        });*/
      },error=>{},()=>{
        this.onchangeCaisseSourceInDetail(this.transfertModel?.caisseSource?.cbmarq,this.transfertModel?.typeCaisseSource?.code);
        this.onchangeCaisseDestinationInDetail(this.transfertModel?.caisseDestination?.cbmarq,this.transfertModel?.typeCaisseDestination?.code);
        this.montant = this.transfertForm?.value?.montant;
        //this.onchangeTransporteurInDetail(this.transfertModel?.userIntermediere?.id)
      })
    }
  }

  getCaisseSource(){
    this.spinnerservice.show()
    this.caisseService.getCaisseSource().subscribe((res:any)=>{
      if(res.statut){
        ////console.log("caisse source: ",res)
        this.listCaisseSource=res.data;
      }
      this.spinnerservice.hide()
    })

  }

  getCaisseDestination(){
    this.spinnerservice.show()
    this.caisseService.getCaisseDestination().subscribe((res:any)=>{
      if(res.statut){
        ////console.log("caisse dest: ",res)
        this.listCaisseDestinataire=res.data;
      }
      this.spinnerservice.hide()
    })
  }

  onSubmit() {
    this.spinnerservice.show()
    if (this.transfertForm.invalid) {
      this.spinnerservice.hide()
      this.submitted = true
      return;
    }
    this.transfertForm.value.date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

    this.transfertForm.getRawValue().lignes.forEach(i => {
      let removeIndex = this.transfertForm.value.lignes.findIndex(itm => itm.selected === false);
      if (removeIndex !== -1)
        this.transfertForm.value.lignes.splice(removeIndex, 1);
    });
    this.transfertForm.value.lignes.forEach(i => {
      delete i.selected;
       i.montant = i.montantTransfert;
    });

    this.transfertForm.value.montant.toString();

    //creation
    if (!this.modeEdit) {
      //console.log('Ajoutt this.transfertForm.value.', this.transfertForm.value)
      this.transfertService.postTransfert(this.transfertForm.value).subscribe((transfert: any) => {
        if (transfert.statut) {
          this.toastr.success("Transfert enregistré avec succès", 'Succès!', { progressBar: true });
          Swal.fire({
            title: 'Transfert est enregisté avec succès!',
            text: "Merci de suivre l\'étape suivante pour confirmer votre demande de transfert !",
            icon: 'info',
            width: '650px',

            iconColor: '#213c7f',
            confirmButtonColor: '#213c7f',
            confirmButtonText: 'Continuer'
          }).then((result) => {
            if (result.value) {
              //console.log(result.value)
              this.router.navigate([`/syndic/details-transfert/`, transfert.data.cbmarq])
            }
          })
          this.transfertForm.reset();
        }
        else {
          //console.log("error", transfert)
          this.toastr.error(transfert.message, 'Erreur!', { progressBar: true });
        }
        this.spinnerservice.hide()
      },
        error => {
          //console.log(error)
        })
    }
    // edition
    else {
      this.transfertService.putTransfert(this.transfertForm.value, this.cbmarq).subscribe((transfert: any) => {
        if (transfert.statut) {
          this.toastr.success("Transfert modifié avec succès", 'Succès!', { progressBar: true });
          Swal.fire({
            title: 'Transfert est modifié avec succès!',
            text: "Merci de suivre l\'étape suivante pour confirmer votre demande de transfert !",
            icon: 'info',
            width: '650px',

            iconColor: '#213c7f',
            confirmButtonColor: '#213c7f',
            confirmButtonText: 'Continuer'
          }).then((result) => {
            if (result.value) {
              ////console.log(result.value)
              this.router.navigate([`/syndic/details-transfert/`, transfert.data.cbmarq])
            }
          })
          this.transfertForm.reset()
        }
        else {
          this.toastr.error(transfert.message, 'Erreur!', { progressBar: true });
        }
        this.spinnerservice.hide()
      },
        error => {
          this.spinnerservice.hide()
          //console.log(error)
        })
    }

  }

  getTypeTransfert() {
    this.transfertService.gettypet('TET').subscribe((type: any) => {
      this.listtypetransfert = type.data?.map(el => ({ value: el.cbmarq, label: el.label }))
    })
  }
  getTypeCaisseSource(code) {
    this.caisseService.gettypeCaisse(code).subscribe((type: any) => {
      this.typeCaisseSource = type.data
    })
  }
  getTypeCaisseDestinataire(code) {
    this.caisseService.gettypeCaisse(code).subscribe((type: any) => {
      this.typeCaisseDestinataire = type.data
    })
  }

  /** to delete **/
  onchangeTypeCaisseSource(event) {
    this.changeCaisse = event
    //console.log('fff', this.changeCaisse)
    this.listCaisseSource = [];
    this.listResponsableSource = []
    this.transfertForm.patchValue({ idCaisseSource: null, userSource: null })
    if (this.changeCaisse.code == 'E0050') {
      if (this.transfertForm.value.lignes != null) {
        this.transfertForm.value.lignes.forEach(i => {
          this.removeLigne(i)
        })
      }
      this.type = 1
      this.disable = false
      if (!this.admin) {
        this.caisseService.getCaisseByResponsable().subscribe((caisse: any) => {
          //console.log('1:', caisse.data)
          this.listCaisseSource = caisse.data
        })
      } else {
        this.caisseService.getCaisses().subscribe((caisse: any) => {
          //console.log('2:', caisse.data)
          this.listCaisseSource = caisse.data
        })
      }

    }
    else if (this.changeCaisse.code == 'E0051') {
      this.transfertForm.value.lignes.forEach(i => {
        this.removeLigne(i)
      })
      this.type = 2
      this.disable = true
      // this.transfertForm.patchValue({ montant: 0, lignes: [] })
      if (!this.admin) {
        this.caisseService.getCaisseSiegeByResponsable().subscribe((caisse: any) => {
          this.listCaisseSource = caisse.data
        });
      } else {
        this.caisseService.getCaisseSiege().subscribe((caisse: any) => {
          this.listCaisseSource = caisse.data
        });
      }
    } else {
      if (!this.admin) {
        this.caisseService.getCaisseInstanceByResponsable().subscribe((caisse: any) => {
          this.listCaisseSource = caisse.data
        });
      } else {
        this.caisseService.getCaisseInstance().subscribe((caisse: any) => {
          this.listCaisseSource = caisse.data
        });
      }
      this.transfertForm.value.lignes.forEach(i => {
        this.removeLigne(i)

      })
    }
  }

  onchangeTypeCaisseDestinataire(event) {
    this.spinnerservice.show()
    this.changeCaisseDest = event.code
    this.listCaisseDestinataire = [];
    this.listResponsableDestinataire = []
    this.transfertForm.patchValue({ idCaisseDestination: null, userDestination: null })
    if (event.code == 'E0053') {
      this.transfertForm.patchValue({ idCaisseDestination: null })
      this.type = 1;

      this.caisseService.getAllCaisses().subscribe((caisse: any) => {
        this.listCaisseDestinataire = caisse.data
        this.spinnerservice.hide()
      });
      this.transfertForm.controls['idCaisseDestination'].setValidators([Validators.required]);
      this.transfertForm.controls['idCaisseDestination'].updateValueAndValidity();
    }
    else if (event.code == 'E0054') {
      this.type = 2
      this.caisseService.getAllCaisseSiege().subscribe((caisse: any) => {
        this.listCaisseDestinataire = caisse.data
        this.spinnerservice.hide()
      });

      this.transfertForm.controls['idCaisseDestination'].setValidators([Validators.required]);
      this.transfertForm.controls['idCaisseDestination'].updateValueAndValidity();
    }
    else {
      this.transfertForm.patchValue({ idCaisseDestination: null })
      this.caisseService.getCaisseInstance().subscribe((caisse: any) => {
        this.listCaisseDestinataire = caisse.data
        this.spinnerservice.hide()
      });
      this.transfertForm.controls['idCaisseDestination'].setValidators([]);
      this.transfertForm.controls['idCaisseDestination'].clearValidators;
      this.transfertForm.controls['idCaisseDestination'].updateValueAndValidity();
      this.utilisateurservice.getAllUtilisateurs().subscribe((responsable: any) => {
        this.spinnerservice.hide()
        this.listResponsableDestinataire = responsable.data.filter(resp => resp.username != localStorage.getItem('username'))
      })
    }
  }

  /*** old fct **/

  onchangeCaisseSource1(event) {
    this.transfertForm.controls['lignes'].setValidators([Validators.required]);
    this.transfertForm.controls['lignes'].updateValueAndValidity();
    if (!this.admin) {
      this.listResponsableSource = [{ id: localStorage.getItem('id'), nomComplet: localStorage.getItem('username') }]
    } else {
      this.caisseService.getResponsableCaisse(event.cbmarq, this.type).subscribe((responsable: any) => {
        this.listResponsableSource = responsable.data
      })
    }
    this.transfertForm.value.lignes.forEach(i => {
      this.removeLigne(i)
    })
    this.montant = 0;
    this.transfertService.getElementToTransfert(event.cbmarq, this.changeCaisse.cbmarq).subscribe((regl: any) => {
      this.listReglement = regl.data
      //console.log(regl.data);
      Object.keys(this.listReglement).forEach(n => {
        this.montant += this.listReglement[n].montant;
        this.transfertForm.patchValue({ montant: this.montant })
        this.addLigne();
        ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('selected').patchValue(true);
        ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idSource').patchValue(<number>this.listReglement[n].idSource);
        ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('type').patchValue(this.listReglement[n]?.type?.cbmarq);
        ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.listReglement[n].montant);
        ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montantTransfert').patchValue(this.listReglement[n].montant);
      })
    })

  }

  onchangeCaisseDestinataire1(event) {
    this.listResponsableDestinataire = [];
    if (this.changeCaisseDest == 'E0055') {
     
      this.utilisateurservice.getAllUtilisateurs().subscribe((responsable: any) => {
        this.listResponsableDestinataire = responsable.data.filter(resp => resp.username != localStorage.getItem('username'))
      })
    }
    else {
      this.caisseService.getResponsableCaisse(event.cbmarq, this.type).subscribe((responsable: any) => {
        this.listResponsableDestinataire = responsable.data.filter(resp => resp.username != localStorage.getItem('username'))
      })
    }

  }

  //wz on change caisse to select responsable
  onchangeCaisseSource(event){

    ////console.log("on change caisse source: ",event)

    this.transfertForm.patchValue({
      userSource: null,
    })
    if(event.type=='E0050'){
      this.type=1;
    }
    if(event.type=='E0051'){
      this.type=2;
    }

    this.transfertForm.patchValue({
      typeCaisseSource:event.type
    })

    this.transfertForm.controls['lignes'].setValidators([Validators.required]);
    this.transfertForm.controls['lignes'].updateValueAndValidity();
    if (!this.admin) {
      this.listResponsableSource = [{ id: localStorage.getItem('id'), nomComplet: localStorage.getItem('username') }]
    } else {
      this.caisseService.getResponsableCaisse(event.cbmarq, this.type).subscribe((responsable: any) => {
        this.listResponsableSource = responsable.data
      })
    }
    this.transfertForm.value.lignes.forEach(i => {
      this.removeLigne(i)
    })
    this.montant = 0;

    this.transfertService.getElementToTransfert(event.cbmarq, event.type).subscribe((regl: any) => {
      this.listReglement = regl.data;
      //console.log("liste reglement: ",this.listReglement);
      //console.log(regl.data);
      if(regl.data){

        Object.keys(this.listReglement).forEach(n => {
          this.montant += this.listReglement[n].montant;
          this.transfertForm.patchValue({ montant: this.montant })
          this.addLigne();

          if(this.listReglement[n].montant==null){
            //console.log("this.listReglement[n].montant: --",this.listReglement[n].montant,"--");
            ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').disable();
          }else{
            ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').enable();
          }
          ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('selected').patchValue(true);
          ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idSource').patchValue(<number>this.listReglement[n].idSource);
          ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('type').patchValue(this.listReglement[n]?.type?.cbmarq);
          ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.listReglement[n].montant);
          ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montantTransfert').patchValue(this.listReglement[n].montant);
        })

    }
    },error=>{

    },()=>{
      //console.log("fin caisse source detail: ",this.transfertForm)
    })
  }

  onchangeCaisseDestinataire(event){
    //console.log("ajout caisse")
    this.transfertForm.patchValue({
      userDestination:null,
      typeCaisseDestination:event?.type
    })



    this.changeCaisseDest=event?.type;
    ////console.log("type caisse dest: ",event.type, "cbmarq: :",event)
    if(event?.type=='E0053'){
      this.type=1;
    }
    if(event?.type=='E0054'){
      this.type=2;
    }

    this.listResponsableDestinataire = [];
    if (event.type == 'E0055') {
     
      this.utilisateurservice.getAllUtilisateurs().subscribe((responsable: any) => {
        //console.log("getalluser: ",responsable)
        if(responsable?.data){
          this.listResponsableDestinataire = responsable?.data?.filter(resp => resp.username != localStorage.getItem('username'))
        }

      })
    }
    else {

      this.caisseService.getResponsableCaisse(event.cbmarq, this.type).subscribe((responsable: any) => {
        //console.log("getResponsableCaisse: ",responsable)
        if(responsable?.data){
          this.listResponsableDestinataire = responsable?.data?.filter(resp => resp.username != localStorage.getItem('username'))

        }
      })
    }

  }


  onchangeCaisseSourceInDetail(idCaisseSource,code){

    this.transfertForm.patchValue({
      typeCaisseSource: code
    })


    if(code=='E0050'){
      this.type=1;
    }
    if(code=='E0051'){
      this.type=2;
    }

    this.transfertForm.controls['lignes'].setValidators([Validators.required]);
    this.transfertForm.controls['lignes'].updateValueAndValidity();

    if (!this.admin) {
      this.listResponsableSource = [{ id: localStorage.getItem('id'), nomComplet: localStorage.getItem('username') }]
    } else {
      ////console.log("data get resp: cbmarqCaisseSource: ",idCaisseSource, " type: ",this.type)
      this.caisseService.getResponsableCaisse(idCaisseSource, this.type).subscribe((responsable: any) => {
        this.listResponsableSource = responsable.data

        ////console.log("get list res source : ",this.listResponsableSource)
      });
    }
    this.transfertForm.value.lignes.forEach(i => {
      this.removeLigne(i);
    });

    this.montant = 0;

    ////console.log("elm to transfert: ",idCaisseSource,"  code: ",code)

    this.listReglement=this.transfertModel.lignes;

    Object.keys(this.listReglement).forEach(n => {
      //this.montant += this.listReglement[n].montant;
      //this.transfertForm.patchValue({ montant: this.montant })

      //console.log("onchangeCaisseSourceInDetail : list regelement ",n," ",this.listReglement[n])
      this.addLigne();
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('selected').patchValue(true);
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('idSource').patchValue(<number>this.listReglement[n].source);
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('type').patchValue(this.listReglement[n]?.type?.cbmarq);
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montant').patchValue(this.listReglement[n].montant);
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('montantTransfert').patchValue(this.listReglement[n].montant);
    })



  }

  onchangeCaisseDestinationInDetail(caisseDestination,code){

    this.transfertForm.patchValue({
      userDestination:null,
      typeCaisseDestination:code
    })



    this.changeCaisseDest=code;
    ////console.log("caisse dest: ",caisseDestination,"     type caisse dest: ",code)
    if(code=='E0053'){
      this.type=1;
    }
    if(code=='E0054'){
      this.type=2;
    }

    if (code == 'E0055') {
      this.listResponsableDestinataire = [];
      this.utilisateurservice.getAllUtilisateurs().subscribe((responsable: any) => {
        ////console.log("getalluser: ",responsable)
        if(responsable?.data){
          this.listResponsableDestinataire = responsable?.data?.filter(resp => resp.username != localStorage.getItem('username'))
        }

      },error=>{},()=>{
        this.transfertForm.patchValue({ userDestination: this.transfertModel.userDestination.id })

      })
    }
    else {

      ////console.log("before get resp caisse dest :",caisseDestination,"   -  ", this.type)
      this.caisseService.getResponsableCaisse(caisseDestination, this.type).subscribe((responsable: any) => {
        ////console.log("getResponsableCaisse Dest: ",responsable)
        if(responsable?.data){
          this.listResponsableDestinataire = responsable?.data?.filter(resp => resp.username != localStorage.getItem('username'))

        }
      },error=>{

      },()=>{
        this.transfertForm.patchValue({ userDestination: this.transfertModel.userDestination.id })
      })
    }
  }

  onchangeTransporteur(event){
    ////console.log("on change transporteur ",event);

    if(event?.id){
      this.destinationIsRequired=true;

      this.transfertForm.controls["idCaisseDestination"].setValidators(Validators.required);
      this.transfertForm.controls["userDestination"].setValidators(Validators.required);
      this.transfertForm.controls['idCaisseDestination'].updateValueAndValidity();
      this.transfertForm.controls['userDestination'].updateValueAndValidity();

      ////console.log("onchangeTransporteur : ",this.transfertForm)

    }

  }

  onchangeTransporteurInDetail(idTransporteur){
    //console.log("on change transporteur ",idTransporteur);

    if(idTransporteur){
      this.destinationIsRequired=true;

      this.transfertForm.controls["idCaisseDestination"].setValidators(Validators.required);
      this.transfertForm.controls["userDestination"].setValidators(Validators.required);
      this.transfertForm.controls['idCaisseDestination'].updateValueAndValidity();
      this.transfertForm.controls['userDestination'].updateValueAndValidity();

      ////console.log("onchangeTransporteur : ",this.transfertForm)

    }

  }

  clearTransporteur(event){
    ////console.log("clear transporteurs ...")
    this.destinationIsRequired=false;

    this.transfertForm.controls['idCaisseDestination'].clearValidators();
    this.transfertForm.controls['userDestination'].clearValidators();
    this.transfertForm.controls['idCaisseDestination'].updateValueAndValidity();
    this.transfertForm.controls['userDestination'].updateValueAndValidity();


  }



  getTransporteur(){
    this.utilisateurservice.getAllUtilisateurs().subscribe((res: any) => {
      if(res?.statut){
        this.listTransporteur = res?.data?.filter(resp => resp.username != localStorage.getItem('username'))
      }

    })
  }

  /** end**/

  newligne(): FormGroup {
    return this.formBuilder.group({
      selected: new FormControl(true,),
      type: new FormControl(null, Validators.required),
      montant: new FormControl('', Validators.required),
      idSource: new FormControl('', Validators.required),
      montantTransfert: new FormControl('', Validators.required),
    });
  }
  lignes(): FormArray {
    return this.transfertForm.get('lignes') as FormArray;
  }
  addLigne() {
    this.lignes().push(this.newligne());
  }
  removeLigne(ligIndex: number) {
    this.lignes().removeAt(ligIndex);
  }
  checkCheckBoxvalue(event, index) {
    if (event.target.checked) {

      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).enable();
      this.montant += ((this.transfertForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montantTransfert').value;
      this.transfertForm.patchValue({ montant: this.montant });
    } else {
      console.log('this.montant' ,this.montant);
      if (this.montant > 0) {
        this.montant -= ((this.transfertForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montantTransfert').value;
        this.transfertForm.patchValue({ montant: this.montant });
      }
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('montant').disable();
      ((this.transfertForm.get('lignes') as FormArray).at(parseInt(index)) as FormGroup).get('type').disable();
    }

  }
  changeMontant(event) {
    this.montant = 0;
    // console.log( this.transfertForm['controls'].lignes['controls'][0].controls['montantTransfert']);
    Object.keys(this.transfertForm.value.lignes).forEach(n => {

      if (this.transfertForm.value.lignes[n].montantTransfert > this.transfertForm.value.lignes[n].montant) {
        this.transfertForm['controls'].lignes['controls'][n].controls['montantTransfert'].setValidators([Validators.max(this.transfertForm.value.lignes[n].montant)]);
        this.transfertForm['controls'].lignes['controls'][n].controls['montantTransfert'].updateValueAndValidity();
        ((this.transfertForm.get('lignes') as FormArray).at(parseInt(n)) as FormGroup).get('selected').patchValue(false);
        // return ;
      }
      if (this.transfertForm.value.lignes[n].selected) {
        this.montant += this.transfertForm.value.lignes[n].montantTransfert;
        // this.transfertForm.value.lignes[n].montant = this.transfertForm.value.lignes[n].montantTransfert;
      }
      this.transfertForm.patchValue({ montant: this.montant });
    });
  }
}
