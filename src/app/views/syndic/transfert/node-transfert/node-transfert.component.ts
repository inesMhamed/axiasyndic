import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Node, Position } from 'src/app/shared/models/tracabilite.model';
import { IOUtils } from 'src/app/shared/utils/io.utils';
import { CdkDragMove } from "@angular/cdk/drag-drop";
//import {CdkDragMove,CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';   
import {MatDialog} from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {TransfertService} from '../../../../shared/services/transfert.service';


@Component({
  selector: 'app-node-transfert',
  templateUrl: './node-transfert.component.html',
  styleUrls: ['./node-transfert.component.scss']
})
export class NodeTransfertComponent{

  popOverVisible:boolean=false;
  currentNode: any;
  changeText: any;
  cbmarq: any;
  transfert: any;
  @Input() node: Node;
  @Output() changePosition: EventEmitter<{
    id: number;
    position: Position;
  }> = new EventEmitter<{ id: number; position: Position }>();

  handleDragMoved(event: CdkDragMove) {
    const newPosition = IOUtils.getElementPosition(
      event.source.element.nativeElement
    );
    // //console.log('new',event.source.element.nativeElement,newPosition);
    this.changePosition.emit({ id: this.node.id, position: newPosition });
  }


  constructor(public dialog: MatDialog, private modalService: NgbModal, private actRoute: ActivatedRoute,
              private transfertService: TransfertService) {
    this.actRoute.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
      if (this.cbmarq) {
        this.transfertService.getTransfert(this.cbmarq).subscribe((reg: any) => {
          if (reg.statut === true) {
            this.transfert = reg.data;
          }
          //console.log('transfert', this.transfert);
        });
      }
    });
  }


  mouseOver(node) {
    this.changeText = true;
    this.currentNode = node;
  }

  mouseOut(node) {
    this.changeText = false;
  }

}
