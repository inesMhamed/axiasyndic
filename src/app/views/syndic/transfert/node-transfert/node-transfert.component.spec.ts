import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeTransfertComponent } from './node-transfert.component';

describe('NodeTransfertComponent', () => {
  let component: NodeTransfertComponent;
  let fixture: ComponentFixture<NodeTransfertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeTransfertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
