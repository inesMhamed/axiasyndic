import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSocketComponent } from './input-socket.component';

describe('InputSocketComponent', () => {
  let component: InputSocketComponent;
  let fixture: ComponentFixture<InputSocketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputSocketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
