import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputSocketComponent } from './output-socket.component';

describe('OutputSocketComponent', () => {
  let component: OutputSocketComponent;
  let fixture: ComponentFixture<OutputSocketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutputSocketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputSocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
