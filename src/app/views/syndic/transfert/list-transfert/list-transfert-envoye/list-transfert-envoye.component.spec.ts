import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTransfertEnvoyeComponent } from './list-transfert-envoye.component';

describe('ListTransfertEnvoyeComponent', () => {
  let component: ListTransfertEnvoyeComponent;
  let fixture: ComponentFixture<ListTransfertEnvoyeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListTransfertEnvoyeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTransfertEnvoyeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
