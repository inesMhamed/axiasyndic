import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTransfertRecusComponent } from './list-transfert-recus.component';

describe('ListTransfertRecusComponent', () => {
  let component: ListTransfertRecusComponent;
  let fixture: ComponentFixture<ListTransfertRecusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListTransfertRecusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTransfertRecusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
