import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { TransfertList } from 'src/app/shared/models/transfert-list.model';
import { Transfert } from 'src/app/shared/models/transfert.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { TransfertService } from 'src/app/shared/services/transfert.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-list-transfert-recus',
  templateUrl: './list-transfert-recus.component.html',
  styleUrls: ['./list-transfert-recus.component.scss']
})
export class ListTransfertRecusComponent implements OnInit {

  displayedColumns: string[] = ['reference', 'userSource', 'caisseSource' ,'userDestination',  'date', 'montant','statut', 'action'];
  dataSource = new MatTableDataSource<any>();
  @Input() id: number;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  listtransfert: TransfertList = new TransfertList();
  cbmarq: string;
  admin: boolean;
  username: string;
  pageSize: any;
  montantTotal:number=0;

  
  id_current_user: any;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;

  constructor(public router: Router, private route: ActivatedRoute,
    private preferenceService:PreferencesService,
    private toastr: ToastrService, private transfertService: TransfertService,private permissionservice: DroitAccesService) { }

  ngOnInit(): void {
    this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.username=localStorage.getItem('username');

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000074');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000075');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000076');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000077');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000078');

    ////console.log(this.username)
    if (this.router.url.includes('/syndic/details-transfert/')) {
      this.transfertService.getTransfert(this.id).subscribe((transf: any) => {
        //console.log('details transfert', transf.data)
        if (transf.statut === true) {
          this.dataSource.data = transf.data?.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, userSource: el.userSource.username, caisseSource: el.caisseSource.intitule, userDestination: el.userDestination.username,date: el.date, montant: el.montant, statut: el.statut }))
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      })
    }
    else {
     
      this.getlistTransfert();
    }
    this.route.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });
    if (localStorage.getItem('role').indexOf('ROLE_ADMIN') == -1) {
      this.admin = false      
    }
    else {
      this.admin = true
    }
  }
  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getlistTransfert() {
    this.dataSource = new MatTableDataSource<any>();

    this.transfertService.getAllTransfertsRecusEnvoi().subscribe((res: any) => {

      if(res.statut){
        this.listtransfert = res.data;


        
        let arrayRecu:any[]=[];
       
          if(res.data.Recu.length==undefined){
            arrayRecu.push(res.data.Recu)

          }else{
            arrayRecu=res.data.Recu
          }

          
          this.dataSource.data = arrayRecu.map(el => ({ cbmarq: el.cbmarq, reference: el.reference,
             userSource: el?.userSource?.nomComplet,
             caisseSource: el?.caisseSource?.intitule,
             userDestination: el.userDestination?.username,
             userDestinationId: el.userDestination?.id,
             userIntermediere: el.userIntermediere?.username,
             userIntermediereId:el.userIntermediere?.id,
             date: el.date, montant: el.montant,
             statut: el.statut ,
             acceptBtn: el.userIntermediere?( el.userIntermediere?.id==this.id_current_user ) : ( el.userDestination?.id==this.id_current_user),
             transfertBtn: el.userIntermediere?( el.userIntermediere?.id==this.id_current_user ) :  false
            }))

           

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;


          
        
      
      }
      
    })
  }
  editerTransfert(id) {
    this.transfertService.getTransfert(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0057" || data.statut.code == "E0059") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier un transfert est déjà confirmé ! ',
        })
      }
      else if (data.statut.code == "E0058") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier un transfert est déjà annulé ! ',
        })
      } else {
        this.router.navigate(['/syndic/modifier-transfert/', id])
      }

    })

  }
  getDetails(id) {
    this.router.navigateByUrl('/syndic/transferts', { skipLocationChange: true }).then(() => {
      this.router.navigate([`/syndic/details-transfert/${id}`]);
    });

  }

  deleteTransfert(id) {
    this.transfertService.getTransfert(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0057" || data.statut.code == "E0059") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas supprimer un transfert est déjà confirmé ! ',
        })
      }
      else if (data.statut.code == "E0058") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas supprimer un transfert est déjà annulé ! ',
        })
      } else {
        this.confirmBox(id)
      }

    })

  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un transfert',
      text: 'Voulez-vous vraiment supprimer ce transfert ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.deleteTransfert(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true){
            this.toastr.success("Transfert supprimé avec succès", 'Succès!', { progressBar: true });
            this.getlistTransfert()
          }           
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  acceptTransfert(cbmarq: any) {
    Swal.fire({
      title: 'Accepter un transfert',
      text: 'Voulez-vous vraiment accepter ce transfert ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.acceptTransfert(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true){
            this.toastr.success("Transfert accepté avec succès", 'Succès!', { progressBar: true });
            this.getlistTransfert()
          }           
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  gotoTracabilite(cbmarq){

    this.router.navigate(['/syndic/tracabilite-transfert/', cbmarq])

  }


  //Intermédiare : Accepter ,Envoyer
  acceptSendTransfertIntermidiare(id,action:string) {

    let actionUpperCase=action[0].toUpperCase() + action.substring(1);
    let actionLowerCase=action.toLocaleLowerCase();
    let actionFaite=action.replace("er","é");

   
    const swalWithBootstrapButtons = Swal.mixin({})
    
    swalWithBootstrapButtons.fire({
      title: actionUpperCase+' un transfert',
      text: 'Vous voulez vraiment '+actionLowerCase+' ce transfert?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#213c7f',
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.acceptSendTransfertIntermidiare(id).subscribe((ress:any)=>{
         // window.location.reload();
         this.getlistTransfert();
        })
        swalWithBootstrapButtons.fire(
         
          actionUpperCase,
          'Votre transfert est '+actionFaite,
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annuler',
          actionUpperCase+' est annuler ',
          'error'
        )
      }
    })
  }

}
