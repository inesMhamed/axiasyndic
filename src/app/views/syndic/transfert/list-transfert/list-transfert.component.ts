import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/shared/models/permission.model';
import { TransfertList } from 'src/app/shared/models/transfert-list.model';
import { Transfert } from 'src/app/shared/models/transfert.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { PreferencesService } from 'src/app/shared/services/preferences.service';
import { TransfertService } from 'src/app/shared/services/transfert.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-list-transfert',
  templateUrl: './list-transfert.component.html',
  styleUrls: ['./list-transfert.component.scss']
})
export class ListTransfertComponent implements OnInit {
  displayedColumns: string[] = ['reference', 'userSource', 'caisseSource' ,'userDestination',  'date', 'montant','statut', 'action'];
  dataSource = new MatTableDataSource<Transfert>();
  @Input() id: number;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;
  listtransfert: TransfertList=new TransfertList();
  cbmarq: string;
  admin: boolean;
  username: string;
  pageSize: any;
  montantTotal:number=0;

  id_current_user: any;
  can_add: Permission;
  can_edit: Permission;
  can_delete: Permission;
  can_detail: Permission;
  can_liste: Permission;
  have_access:boolean =false;



  constructor(public router: Router, private route: ActivatedRoute, private spinnerservice: NgxSpinnerService,
    private preferenceService: PreferencesService, private permissionservice: DroitAccesService,
    private toastr: ToastrService, private transfertService: TransfertService) { }

  ngOnInit(): void {
    this.spinnerservice.show()

    this.id_current_user = localStorage.getItem('id');
    this.can_add = this.permissionservice.search( this.id_current_user, 'FN21000074');
    this.can_edit = this.permissionservice.search( this.id_current_user, 'FN21000075');
    this.can_delete = this.permissionservice.search( this.id_current_user, 'FN21000076');
    this.can_detail = this.permissionservice.search( this.id_current_user, 'FN21000077');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000078');

    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000078').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        
        this.getlistTransfert();

      }
     
    });


  


    /*this.preferenceService.getPreferences(1).subscribe((pre: any) => {
      this.pageSize = pre.data.affichageTableaux
    })
    this.username=localStorage.getItem('username')
    //console.log(this.username)
    if (this.router.url.includes('/syndic/details-transfert/')) {
      this.transfertService.getTransfert(this.id).subscribe((transf: any) => {
        //console.log('details transfert', transf.data)
        if (transf.statut === true) {
          this.dataSource.data = transf.data?.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, userSource: el.userSource.username, caisseSource: el.caisseSource.intitule, userDestination: el.userDestination.username,date: el.date, montant: el.montant, statut: el.statut }))
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      })
    }
    else {

      this.getlistTransfert();


    }
    this.route.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
    });
    if (localStorage.getItem('role').indexOf('ROLE_ADMIN') == -1) {
      this.admin = false      
    }
    else {
      this.admin = true
    }*/
  }
  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getlistTransfert() {
    this.spinnerservice.show()
    this.transfertService.getAllTransfertsRecusEnvoi().subscribe((res: any) => {

      if (res.statut) {
        this.listtransfert = res.data;
      }
      this.spinnerservice.hide()
      /*this.listtransfert = res.data;
      this.dataSource.data = res.data?.map(el => ({ cbmarq: el.cbmarq, reference: el.reference, userSource: el.userSource.nomComplet, caisseSource: el.caisseSource.intitule, userDestination: el.userSource.nomComplet,date: el.date, montant: el.montant, statut: el.statut }))
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;*/
      //console.log("list transfert main: ",res.data)
    })
  }
  editerTransfert(id) {
    this.spinnerservice.show()
    this.transfertService.getTransfert(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0057" || data.statut.code == "E0059") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier un transfert est déjà confirmé ! ',
        })
      }
      else if (data.statut.code == "E0058") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas modifier un transfert est déjà annulé ! ',
        })
      } else {
        this.router.navigate(['/syndic/modifier-transfert/', id])
      }
      this.spinnerservice.show()
    })

  }
  getDetails(id) {
    this.router.navigateByUrl('/syndic/transferts', { skipLocationChange: true }).then(() => {
      this.router.navigate([`/syndic/details-transfert/${id}`]);
    });

  }

  deleteTransfert(id) {
    this.spinnerservice.show()
    this.transfertService.getTransfert(id).subscribe((regl: any) => {
      let data = regl.data
      if (data.statut.code == "E0057" || data.statut.code == "E0059") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas supprimer un transfert est déjà confirmé ! ',
        })
      }
      else if (data.statut.code == "E0058") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Vous ne pouvez pas supprimer un transfert est déjà annulé ! ',
        })
      } else {
        this.confirmBox(id)
      }
      this.spinnerservice.hide()
    })

  }
  confirmBox(cbmarq: any) {
    Swal.fire({
      title: 'Supprimer un transfert',
      text: 'Voulez-vous vraiment supprimer ce transfert ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.deleteTransfert(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistTransfert()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }
  acceptTransfert(cbmarq: any) {
    Swal.fire({
      title: 'Accepter un transfert',
      text: 'Voulez-vous vraiment accepter ce transfert ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.transfertService.acceptTransfert(cbmarq).subscribe((ress: any) => {

          if (ress.statut == true)
            this.getlistTransfert()
          else {
            this.toastr.error(ress.message, 'Erreur!', { progressBar: true });
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  gotoTracabilite(cbmarq){

    this.router.navigate(['/syndic/tracabilite-transfert/', cbmarq])

  }
}
