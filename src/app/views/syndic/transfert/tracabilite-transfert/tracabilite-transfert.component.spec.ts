import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracabiliteTransfertComponent } from './tracabilite-transfert.component';

describe('TracabiliteTransfertComponent', () => {
  let component: TracabiliteTransfertComponent;
  let fixture: ComponentFixture<TracabiliteTransfertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracabiliteTransfertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracabiliteTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
