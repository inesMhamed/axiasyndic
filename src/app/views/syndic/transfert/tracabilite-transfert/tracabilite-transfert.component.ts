import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Node, Path, Position, Size } from 'src/app/shared/models/tracabilite.model';
import { TransfertService } from 'src/app/shared/services/transfert.service';
import { IOUtils } from 'src/app/shared/utils/io.utils';
import { ContactsRoutingModule } from 'src/app/views/contacts/contacts-routing.module';

@Component({
  selector: 'app-tracabilite-transfert',
  templateUrl: './tracabilite-transfert.component.html',
  styleUrls: ['./tracabilite-transfert.component.scss']
})
export class TracabiliteTransfertComponent implements OnInit {

  nodes: Node[] = [];
  paths: Path[] = [];
  cbmarq:any;
  popOverVisible:boolean=false;
  @Output() changePosition: EventEmitter<{
    id: number;
    position: Position;
  }> = new EventEmitter<{ id: number; position: Position }>();

  private nodeSize: Size = { width: 100 , height: 100 };
  private nodePositions: { [nodeId: number]: Position } = {};

  constructor(private actRoute: ActivatedRoute,
    private transfertService: TransfertService) {

     
    this.actRoute.paramMap.subscribe(params => {
      this.cbmarq = params.get('cbmarq');
      if (this.cbmarq) {
        this.getSuivi();
      }
    });

    // this.nodePositions = {
    //   1: { x: 70, y: 60},
    //   2: { x: 280, y: 20 },
    //   3: { x: 500, y: 100 },
    //   4: { x: 800, y: 30}
    // };

   }

  ngOnInit(): void {
  }


  getSuivi(){

    this.transfertService.getSuivi(this.cbmarq).subscribe((res: any) => {
      if (res.statut === true) {
       // //console.log("res.datasuivi: ",res.data)

        res.data.liste.forEach(val => this.nodes.push(Object.assign({}, val)));
        res.data.paths.forEach(val => this.paths.push(Object.assign({}, val)));
        //this.nodePositions=Object.assign({}, res.data.nodes); 
        let i=0;
        let mapped = res.data.nodes.reduce (function (map, obj) { 
         
          map[i+1] = obj; 
          i++;
          return map;
        },{});

        this.nodePositions=mapped;

      }
    },error=>{},()=>{
    })
  }
  // handleChangeNodePosition(event: { id: number; position: Position }) {
  //
  //   event.position.x= event.position.x-150;//-150 wz
  //   event.position.y= event.position.y-110;//-110 wz
  //
  //   this.nodePositions[event.id] = event.position;
  // }
  handleChangeNodePosition(event: { id: number; position: Position }) {
    this.nodePositions[event.id] = event.position;
  }
  getD(path: Path) {

    const startPosition = this.getNodePosition(path.from, "OUTPUT");
    const endPosition = this.getNodePosition(path.to, "INPUT");
    return (
      startPosition && endPosition && IOUtils.getD(startPosition, endPosition)
    );
  }

  private getNodePosition(
    nodeId: number,
    socketType: "INPUT" | "OUTPUT"
  ): Position | undefined {
    const position = this.nodePositions[nodeId];
    if (!position) {
      return undefined;
    }
    if (socketType === "INPUT") {
      return IOUtils.setOffset(position, { x: 0, y: this.nodeSize.height/ 2  });//wz +35
    }
    return IOUtils.setOffset(position, {
      x: this.nodeSize.width,
      y: this.nodeSize.height / 2  //wz +35
    });
  }






}
