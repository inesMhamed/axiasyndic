import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
// RxJS v6+
import { from, of, zip } from 'rxjs';
import { first, groupBy, mergeMap, toArray } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Fonctionnalite } from 'src/app/shared/models/fonctionnalite.model';
import { AuthenticationService } from 'src/app/shared/services/authentification.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { Permission } from '../../../shared/models/permission.model';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-droit-acces',
  templateUrl: './droit-acces.component.html',
  styleUrls: ['./droit-acces.component.scss'],
})
export class DroitAccesComponent implements OnInit {
  fonction: Fonctionnalite = new Fonctionnalite()
  listCollaborateurs: any
  listFonctions: any[] = []
  listFonctionsCbmarq: any
  listFct: any
  listRole: any[] = []
  fonctionForm: FormGroup
  fonctionroleForm: FormGroup
  submitted: boolean;
  checkedList: any[] = [];
  show: boolean = true
  par_utilisateur: boolean
  listRoles = [{ value: 'interne', label: 'Utilisateur Interne' }, { value: 'proprietaire', label: 'Propriétaire' }, { value: 'cooproprietaire', label: 'Coo-propriétaire' }]

  isChecked: boolean;
  username: any;
  role: any;
  admin: number;
  livinterne: number;
  livexterne: number;
  checkedList1: any;
  interne: number;
  proprietaire: number;
  listGroupes: any;
  type: number;
  idUser: any;
  listUser: any;
  can_add: Permission;
  can_editR: Permission;
  can_editU: Permission;
  can_listett: Permission;
  can_liste: Permission;
  id_current_user: any;

  have_access:boolean =false;

  constructor(private userService: UtilisateurService, private toastrservice: ToastrService, private router: Router,
    private spinnerservice: NgxSpinnerService,
    private authSevice: AuthenticationService, private droitaccesService: DroitAccesService, private permissionservice: DroitAccesService) { }

  ngOnInit(): void {
    this.spinnerservice.show()
    this.id_current_user = localStorage.getItem('id');
    this.can_editR = this.permissionservice.search( this.id_current_user, 'FN21000050');
    this.can_editU = this.permissionservice.search( this.id_current_user, 'FN21000050');
    this.can_listett = this.permissionservice.search( this.id_current_user, 'FN21000049');
    this.can_liste = this.permissionservice.search( this.id_current_user, 'FN21000048');

    this.checkAccessRole();

    this.getlistCollaborateurs();

  }

  checkAccessRole(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000049').subscribe((res:any)=>{

     this.have_access =res.data;
    
   },error=>{},()=>{

    this.checkAccessUser();
    
   });
  }

  checkAccessUser(){
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000048').subscribe((res:any)=>{
     
      if(!this.have_access && !res.data){
        this.router.navigate(['403']);
      }
    
  },error=>{},()=>{
        
  });
  }

  checkBoxvalue(event, idgrp) {
    const checked = event.target.value;
    if (event.target.checked) {

      this.listUser.forEach(item => {
        if (item.cbmarq == idgrp) {
          item.checked = true
          item.fonctionnalites.forEach(it => {
            it.statut = true;

          })
        }
      });
    }
    else {
      this.listUser.forEach(item => {
        if (item.cbmarq == idgrp) {
          item.checked = false
          item.fonctionnalites.forEach(it => {
            it.statut = false;

          })
        }
      });
    }

  }
  checkBoxvalueRole(event, idgrp) {

    const checked = event.target.value;
    if (event.target.checked) {

      this.listRole.forEach(item => {
        if (item.cbmarq == idgrp) {
          item.checked = true
          item.fonctionnalites.forEach(it => {
            it.statut = true;

          })
        }
      });
    }
    else {
      this.listRole.forEach(item => {
        if (item.cbmarq == idgrp) {
          item.checked = false
          item.fonctionnalites.forEach(it => {
            it.statut = false;

          })
        }
      });
    }
  }
  checkCheckBoxvalue(event, fct, fonction) {
    const checked = event.target.value;
    let testcheck = true;
    if (checked) {
      fonction.fonctionnalites.forEach(item => {
        if (item.statut == false)
          testcheck = false;
      });
      (document.getElementById(fonction.cbmarq) as HTMLInputElement).checked = testcheck;

    }
    else {
      (document.getElementById(fonction.cbmarq) as HTMLInputElement).checked = false;
    }
  }

  checkCheckBoxRole(event, fct, fonction) {

    const checked = event.target.value;
    let testcheck = true;
    if (checked) {
      fonction.fonctionnalites.forEach(item => {
        if (item.statut == false)
          testcheck = false;
      });
      (document.getElementById(fonction.cbmarq) as HTMLInputElement).checked = testcheck;

    }
    else {
      (document.getElementById(fonction.cbmarq) as HTMLInputElement).checked = false;
    }
  }
  onChange(event: any) {
    this.listUser = []
    this.idUser = event.value

    this.droitaccesService.getFonctionalitesUser(this.idUser).subscribe(
      res => {
        res?.data?.forEach(element => {
          let testcheck = false
          element.fonctionnalites.find(item => {
            if (item.statut == false) {
              testcheck = false

            } else {
              testcheck = true
            }
          })
          this.listUser.push({ checked: testcheck,close: true, cbmarq: element.cbmarq, intitule: element.intitule, fonctionnalites: element.fonctionnalites })
        })
       }
    );
  }
  getlisteGroupe() {
    this.droitaccesService.getGroupes().subscribe(
      (res: any) => {
        this.listGroupes = res.data
      })
  }
  onChangeRole(event: any) {
    this.checkedList = []
    this.listRole = []
    this.role = event.value
    if (this.role == 'interne') {
      this.type = 1
      this.droitaccesService.getFonctionalitesRole(this.type).subscribe(
        (res: any = []) => {

          res?.data?.forEach(element => {
            let testcheck = false
            element?.fonctionnalites?.find(item => {
              if (item.statut == false) {
                testcheck = false

              } else {
                testcheck = true
              }
            })
            this.listRole.push({ checked: testcheck, close:true, cbmarq: element.cbmarq, intitule: element.intitule, fonctionnalites: element.fonctionnalites })
          })

        })
      this.checkedList = this.listFonctions.map(fct => ({ fonctionnalite: fct.cbmarqfc, statut: fct.statut }))


    }
    else if (this.role == 'proprietaire') {
      this.type = 2
      this.droitaccesService.getFonctionalitesRole(this.type).subscribe(
        (res: any = []) => {
          // this.listRole = res.data
          res.data.forEach(element => {
            let testcheck = false
            element.fonctionnalites.find(item => {
              if (item.statut == false) {
                testcheck = false

              } else {
                testcheck = true
              }
            })
            this.listRole.push({ checked: testcheck,close:true,  cbmarq: element.cbmarq, intitule: element.intitule, fonctionnalites: element.fonctionnalites })
          })

        })

    }
    else{
      this.type = 3
      this.droitaccesService.getFonctionalitesRole(this.type).subscribe(
          (res: any = []) => {
            // this.listRole = res.data
            res.data.forEach(element => {
              let testcheck = false
              element.fonctionnalites.find(item => {
                if (item.statut === false) {
                  testcheck = false;

                } else {
                  testcheck = true;
                }
              })
              this.listRole.push({ checked: testcheck,close:true,  cbmarq: element.cbmarq, intitule: element.intitule, fonctionnalites: element.fonctionnalites })
            })

          })
    }
  }


  //submit by user
  onSubmit1(): void {

    if (this.can_editU?.permission) {
      this.checkedList = [];
      this.listUser.forEach(item => {
        item.close=true;
        item.fonctionnalites.forEach(fct => {
          this.checkedList.push({ fonctionnalite: fct.cbmarq, statut: fct.statut });
        })
      });
      this.droitaccesService.editFonctionUser({ fonctionnalites: this.checkedList }, this.idUser).pipe(first()).subscribe(
          (res) => {
            this.toastrservice.success('Votre modification a été enregistrée avec succès ! ');
            // window.location.reload();
          },
          err => {
            this.toastrservice.error('Veuillez essayer plus tard ! ');
          }
      );
    } else {
      this.toastrservice.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
    }
  }
  //submit by role
  onSubmit(): void {
    this.spinnerservice.show()
    if (this.can_editR?.permission) {
      this.checkedList1 = [];
      this.listRole.forEach(item => {
        item.close = true;
        item.fonctionnalites.forEach(fct => {
          this.checkedList1.push({ fonctionnalite: fct.cbmarq, statut: fct.statut });
        })
      });
      this.droitaccesService.editFonctionRole({ fonctionnalites: this.checkedList1 }, this.type).pipe(first()).subscribe(
        (res) => {
          this.toastrservice.success('Votre modification a été enregistrée avec succès ! ');
          // window.location.reload();
          this.spinnerservice.hide()
        },
        err => {
          this.spinnerservice.hide()
          this.toastrservice.error('Veuillez essayer plus tard ! ');
        }
      );

    } else {
      this.spinnerservice.hide()
      this.toastrservice.error('Accès Interdie !!!', 'Erreur!', { progressBar: true });
    }
  }
  getlistCollaborateurs() {
    this.userService.getAllUtilisateurs().subscribe((res: any) => {
      if (res.statut) {
        this.listCollaborateurs = res.data.map(clt => ({ value: clt.id, label: clt.username }));

      }
      this.spinnerservice.hide()
    }
    );
  }



}
