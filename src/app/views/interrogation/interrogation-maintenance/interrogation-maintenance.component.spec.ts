import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogationMaintenanceComponent } from './interrogation-maintenance.component';

describe('InterrogationMaintenanceComponent', () => {
  let component: InterrogationMaintenanceComponent;
  let fixture: ComponentFixture<InterrogationMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterrogationMaintenanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogationMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
