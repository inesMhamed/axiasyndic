import { KeyValue } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { InterroagtionService } from 'src/app/shared/services/interrogation.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
//import EmployeesJson from '../../../../assets/maintenance-produit-mois.json';



@Component({
  selector: 'app-interrogation-maintenance',
  templateUrl: './interrogation-maintenance.component.html',
  styleUrls: ['./interrogation-maintenance.component.scss']
})
export class InterrogationMaintenanceComponent implements OnInit {

  rows = [];
  dataSource:any[]=[];//=EmployeesJson;
  residences:any[]=[];
  emplacements:any[]=[{ value: 0, label: "Tous" }];  
  listExercice:any[]=[];
  listMois:any[]=[{ value: 0, label: "Tous" },{'label':'Janvier','value':1},{'label':'Février','value':2},{'label':'Mars','value':3},{'label':'Avril','value':4}
  ,{'label':'Mai','value':5},{'label':'Juin','value':6},{'label':'Juillet','value':7},{'label':'Aout','value':8},
  {'label':'Septembre','value':9},{'label':'Octobre','value':10},{'label':'Novembre','value':11},{'label':'Décembre','value':12}];
  formGroupSearch:FormGroup;
  listData:any[]=[];
  daysOfMonth:number=0;
  listDaysOfMonth:number[]=[];
  currentYear:number=new Date().getFullYear();
  currentMonth:number=new Date().getMonth()+1;
  operationsListClicked:any[]=[];

  id_current_user:any;
  have_access:boolean =false;

  constructor(private interrogationService: InterroagtionService, private residenceService: ResidenceService,
    private fb: FormBuilder, private modalService: NgbModal,
    private spinnerservice: NgxSpinnerService,
    private router: Router, private permissionservice: DroitAccesService) {

      this.id_current_user = localStorage.getItem('id');

  }

  ngOnInit(): void {
    this.spinnerservice.show()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000123').subscribe((res:any)=>{

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getListProduit(); 
      }
     
    });


    

      

    this.formGroupSearch = this.fb.group({      
      produit: new FormControl(null, Validators.required),
      emplacement: new FormControl(null),
      annee: new FormControl(null, Validators.required),
      mois: new FormControl(null),
    });

    this.formGroupSearch.patchValue({
      produit:null,
      emplacement: 0,
      annee: null,
      mois: 0,
    });
  }

 

  onChangeYear(){
    this.listData=[];
    this.currentYear=this.formGroupSearch.value.annee;
  }

  onChangeMonth(){

    this.listData=[];
    this.currentMonth=this.formGroupSearch.value.mois;
    this.generateListDaysOfMonth();

  }

  generateListDaysOfMonth(){
   
    this.listDaysOfMonth=[];

    this.daysOfMonth= new Date(this.currentYear, this.currentMonth, 0).getDate();

    for(let i=1 ;i<=this.daysOfMonth;i++){
      this.listDaysOfMonth.push(i);
    }
  }

  
  //TOVERIFYWZ
  getListMaintenance(){
    let modelSearch={
      produit:14,
      mois:1
    }

    ////console.log("modelSearch M : ",modelSearch)
    this.interrogationService.getInterrogationMaintenance(modelSearch).subscribe((res:any)=>{

      if(res.statut){
        this.dataSource=res.data;
        ////console.log("this.datasource: ",this.dataSource)
      }

    },error=>{},()=>{
      if(this.dataSource.length>0){
        //this.generateTable();
      }
      
    })
  }

  getListProduit() {
    this.spinnerservice.show()
    this.residences = [];
    //let tous={ value: 0, label: "Tous" };

    this.residenceService.getResidences().subscribe((res: any) => {
      ////console.log("list residence res: ",res);
      if (res.data != null) {
        this.residences = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));
        ////console.log("this.residence: ",this.residences)
        //this.residences.splice(0, 0, tous);

      }
      this.spinnerservice.hide()
    },
      error => {
        //console.log( 'erreur: ', error);
      }, () => {
        this.spinnerservice.hide()
        this.onChangeResidence(false);
      }
    );
  }

  onChangeResidence(fromFront){

    if(fromFront){
      this.formGroupSearch.patchValue({
        emplacement:0,
        annee:null,
      })
    }else{
      this.formGroupSearch.patchValue({
        annee:null,
      })
    }



   this.getEmplacementParent();
   this.onChangeListExercice();

  }

  getEmplacementParent() {
    this.spinnerservice.show()
    this.emplacements = [];
    let tous = { value: 0, label: "Tous" };


    this.interrogationService.getEmplacementParent(this.formGroupSearch?.value?.produit).subscribe((res:any)=>{

      if(res.statut){

        this.emplacements = res.data.map(clt => ({ value: clt.cbmarq, label: clt.intitule }));       
        this.emplacements.splice(0, 0, tous);

        ////console.log("list empl: ",res.data)
      }
     

      this.spinnerservice.hide()

    }, error => { }, () => {
      this.spinnerservice.hide()
      ////console.log("list emplacement : ",this.emplacements)
     
    })

  }

  onChangeListExercice() {
    this.spinnerservice.show()
    this.listExercice = [];

    this.interrogationService.getExerciceByProduit(this.formGroupSearch?.value?.produit).subscribe((res:any)=>{

      if(res.statut){

        this.listExercice = res.data.map(clt => ({ value: clt, label: clt }));       

        ////console.log("list exercice: ",res.data)
      }
      this.spinnerservice.hide()

    })
  }

  

  


  interroger() {
    this.spinnerservice.show()
    let modelSearch = {
      produit: this.formGroupSearch.value.produit == 0 ? null : this.formGroupSearch.value.produit,
      emplacement: this.formGroupSearch.value.emplacement == 0 ? null : "" + this.formGroupSearch.value.emplacement,
      annee: this.formGroupSearch.value.annee == 0 ? null : this.formGroupSearch.value.annee,
      mois: this.formGroupSearch.value.mois == 0 ? null : this.formGroupSearch.value.mois,
    }

    console.log("interroger maintenance : ",modelSearch)

    this.interrogationService.getInterrogationMaintenance(modelSearch).subscribe((res: any) => {
      if (res.statut) {
        console.log("list maintenance: json: ", res.data)
        this.dataSource = res.data;
      }
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.spinnerservice.hide()

      //Par année
      if(this.formGroupSearch.value.mois==0){
        this.generateTableParAnne();
      }
      //par mois
      if(this.formGroupSearch.value.mois!=0){
        this.generateTableParMois();
      }
     
    })

  }

  
  generateTableParMois() {

    this.listData=[];
   
    //console.log("generateTableParMois: ....")
    if (!this.dataSource) {
      return;
    }

    

    //foreach sur datasource
    let indexData:number=0;
    for(let data of this.dataSource){
      this.rows=[];
      //push One parent intitule with rowspan
      //if(data.parent.cbmarq==1){ //7 //2 //5

        let maxRowSpan = 0;
        let nbrRowSpanFil:number=0;
        let nbrRowSpanEquipement:number=0;

        this.rows.push([
          {
            text: data?.parent?.intitule,
            rowspan: 0
          }
        ]);


        /**** FOREACH SOUS EMPLACEMENT  ***/
        data.fils.forEach((fil, i) => {

          let filAux:any=Object.values(fil)[0];
          let listEquipement:any;
          listEquipement=Object.values(fil)[1];
         
          //console.log("list equipement: ",listEquipement.length,"/// filAux?.nbrfilsgamme: ",filAux?.nbrfilsgamme)
          let filRowSpanEmpl =0; 
          //check max line of fils
          if(filAux?.nbrfilsgamme>=listEquipement.length){
            filRowSpanEmpl=filAux?.nbrfilsgamme
          }
          if(listEquipement.length>filAux?.nbrfilsgamme){
            filRowSpanEmpl=listEquipement.length;
          }          
          if(filRowSpanEmpl==0){
            filRowSpanEmpl=1;
          }
          //
          nbrRowSpanFil+=filRowSpanEmpl;
          if(filRowSpanEmpl>maxRowSpan){
            maxRowSpan=filRowSpanEmpl;
          }
         
         
          //console.log("filAux?.intitule: ",filAux?.intitule," ///filRowSpanEmpl: ",filRowSpanEmpl," /// nbrRowSpanFil: ",nbrRowSpanFil," //// maxRowSpan: ",maxRowSpan)

          //push fil intitule with rowspan
          if (i > 0) {
            this.rows.push([])
          }
          this.rows[this.rows.length - 1].push({
            text:  filAux?.intitule,
            rowspan: filRowSpanEmpl//filRowSpan
          });
    

          /****** FOREACH EQUIPEMENT *******/
          if(listEquipement.length>0){


            listEquipement.forEach((equip, j) => {
            
              let EquipAux:any=Object.values(equip)[0];
              let listGamme:any;
              listGamme=Object.values(equip)[1];
            
              ////console.log("listGamme: ",listGamme)
              const equipRowSpan = EquipAux.nbcheckliste==0?1:EquipAux.nbcheckliste; 
              nbrRowSpanEquipement+=equipRowSpan; //===nbr total checklist
              if(equipRowSpan>maxRowSpan){
                maxRowSpan=equipRowSpan;
              }
            
              ////console.log(" elemRowSpanEquip: ",equipRowSpan)
    
              //push equipement intitule with rowspan
              if (j > 0) {
                this.rows.push([])
              }
              this.rows[this.rows.length - 1].push(
                {
                  text:  EquipAux?.intitule,
                  rowspan: equipRowSpan
                },
                {
                  text:  EquipAux?.quantite,
                  rowspan: equipRowSpan
                }            
                
                );

                /******** FOREACH GAMME OPERATOIRE ******/

                ////console.log("list gamme: ",listGamme)
                if(listGamme.length>0 ){
                  listGamme.forEach((gamme, k) => {
                  
                    let GammeAux:any=Object.values(gamme)[0];
                    let listOperation:any=[];
                   

                    listOperation=Object.values(gamme)[1]; 
                    //console.log("GammeAux: ",GammeAux.intitule," cbmarq: ",GammeAux.cbmarq," listOperation:\n ",listOperation)
                   
                        
                    //push GAME operatoire intitule with rowspan
                    if (k > 0) {
                      this.rows.push([])
                    }
                
                    
                      this.rows[this.rows.length - 1].push(
                      //set Nom GammeOperatoire
                        {
                        text:  GammeAux?.intitule,
                        rowspan: 1
                        }              
                                           
                      
                      );

                    /******************* set etat operation*****************************/

                    if(listOperation.length>0){

                        //foreach all days of month
                        for(let i=1;i<=this.daysOfMonth;i++){

                          ////console.log("=========================================",i)
                          let etat:any;
                          let nbrOfOperation:number=0;
                          let etatValide:number=2;//etat non valide
                          let operations:any[]=[]; //list operation filtred

                            //foreach list operation
                            for(let operation of listOperation){  
                              
                              //find etat in list operation

                                let datestr=operation?.dateExecution?.date;
                                let dateD=new Date(datestr);
          
                                /* ===== set operation Etat ======*/
                                ////console.log("operation: ",operation.gamme.cbmarq," gamme: ",GammeAux?.cbmarq ," day: ",i)

                                if(dateD?.getDate()==i && operation?.etat==1 && operation.gamme.cbmarq==GammeAux.cbmarq){

                                  nbrOfOperation+=nbrOfOperation;
                                  etat=operation?.etat;
                                  etatValide=1;
                                  operations.push(operation);
                                  ////console.log("inn push operation: ",operation," gamme: ",GammeAux?.intitule ," day: ",i)
                                                                   
                                }

                                

                           
                            
                            }//fin foreach operations     
                            
                            //set etat after foreach
                            ////console.log("list operations per day: ",operations)
                            if(operations?.length>=2){
                              this.rows[this.rows.length - 1].push(
                                                  
                                //set etat
                                {
                                  text:  "2+",
                                  rowspan: 1,
                                  operations:operations
                                }                             
                              
                              );
                            }else{
                              this.rows[this.rows.length - 1].push(                                                  
                                //set etat
                                {
                                  text:  etatValide==1?"X":"",
                                  rowspan: 1,
                                  operations:operations
                                }                             
                              
                              );
                            }
                            /*******/
                            operations=[];
                         }//fin foreach days of month
                       

                    }else{
                       //foreach all days of month
                       //console.log("this.daysOfMonth: ",this.daysOfMonth)
                       for(let i=1;i<=this.daysOfMonth;i++){
                        //foreach list operation
                           
                        /* ===== set operation vide ======*/
                        this.rows[this.rows.length - 1].push(
                                        
                            //foreach operationList and set etat
                            {
                              text:  "",
                              rowspan: 1
                            }
                        
                          
                        );                          

                      }
                    }
                   



                  })

                }
                //list gamme vide
                else{
                                
                  this.rows[this.rows.length - 1].push(
                    //checkList
                    {
                    text:  "",
                    rowspan: 1
                    },               
                  
                    
                    );

                    for(let v of this.listDaysOfMonth){

                      this.rows[this.rows.length - 1].push(                                     
                        //operation set
                        {
                          text:  "",
                          rowspan: 1
                        }                      
                        
                        );

                    }
                }

                /******* FIN *******/            


            })


          }
          //list equipement vide
          else{

              //equipement vide              
            this.rows[this.rows.length - 1].push(
              //equipement
              {
                text:  "",
                rowspan: 1
              }, 
              //qte
              {
                text:  "",
                rowspan: 1
              }, 
              //checkList
              {
              text:  "",
              rowspan: 1
              }             
                          
              );

              for(let v of this.listDaysOfMonth){

                this.rows[this.rows.length - 1].push(                                     
                  //operation set
                  {
                    text:  "",
                    rowspan: 1
                  }                      
                  
                  );

              }
          }
           

         

          /******* fin foreach equipement *****/
        });
        /****** FIN FOREACH SOUS EMPLACEMENT */
        //check rowspan max
        if(maxRowSpan<nbrRowSpanFil){
          maxRowSpan=nbrRowSpanFil;
        }
        if(maxRowSpan<nbrRowSpanEquipement){
          maxRowSpan=nbrRowSpanEquipement;
        }

        //checkMax rowspan
        if(data?.parent?.nbrgammes>maxRowSpan){
          this.rows[0][0].rowspan=data?.parent?.nbrgammes;
        }else{
          this.rows[0][0].rowspan=maxRowSpan;
        }

        ////console.log("rows: ",this.rows)
        indexData ++;
      
        this.listData.push(this.rows);

      //}//fin choix item of data

       


    }

    ////console.log("all Data: ",this.listData)
   


    
  }

  generateTableParAnne() {

    this.listData=[];
   
    ////console.log("")
    if (!this.dataSource) {
      return;
    }

    //foreach sur datasource
    let indexData:number=0;
    for(let data of this.dataSource){
      this.rows=[];
      //push One parent intitule with rowspan
      //if(data.parent.cbmarq==7){ //7 //2 //5

        let maxRowSpan = 0;
        let nbrRowSpanFil:number=0;
        let nbrRowSpanEquipement:number=0;

        this.rows.push([
          {
            text: data?.parent?.intitule,
            rowspan: 0
          }
        ]);


        /**** FOREACH SOUS EMPLACEMENT  ***/
        data.fils.forEach((fil, i) => {

          let filAux:any=Object.values(fil)[0];
          let listEquipement:any;
          listEquipement=Object.values(fil)[1];
         
          ////console.log("list equipement: ",listEquipement)
          let filRowSpan =0; //listEquipement.length==0?1:listEquipement.length;//filAux?.nbrfilsgamme
          //check max line of fils
          if(filAux?.nbrfilsgamme>=listEquipement.length){
            filRowSpan=filAux?.nbrfilsgamme
          }
          if(listEquipement.length>filAux?.nbrfilsgamme){
            filRowSpan=listEquipement.length;
          }
          
          if(filRowSpan==0){
            filRowSpan=1;
          }
          //
          nbrRowSpanFil+=filRowSpan;
          if(filRowSpan>maxRowSpan){
            maxRowSpan=filRowSpan;
          }
         
         
          ////console.log("filRowSpan: ",filRowSpan)

          //push fil intitule with rowspan
          if (i > 0) {
            this.rows.push([])
          }
          this.rows[this.rows.length - 1].push({
            text:  filAux?.intitule,
            rowspan: filRowSpan
          });
    

          /****** FOREACH EQUIPEMENT *******/
          if(listEquipement.length>0){


            listEquipement.forEach((equip, j) => {
            
              let EquipAux:any=Object.values(equip)[0];
              let listGamme:any;
              listGamme=Object.values(equip)[1];
            
              ////console.log("listGamme: ",listGamme.length)
              const equipRowSpan = EquipAux.nbcheckliste==0?1:EquipAux.nbcheckliste; 
              nbrRowSpanEquipement+=equipRowSpan; //===nbr total checklist
              if(equipRowSpan>maxRowSpan){
                maxRowSpan=equipRowSpan;
              }
            
              ////console.log(" elemRowSpanEquip: ",equipRowSpan)
    
              //push equipement intitule with rowspan
              if (j > 0) {
                this.rows.push([])
              }
              this.rows[this.rows.length - 1].push(
                {
                  text:  EquipAux?.intitule,
                  rowspan: equipRowSpan
                },
                {
                  text:  EquipAux?.quantite,
                  rowspan: equipRowSpan
                }            
                
                );

                /******** FOREACH GAMME OPERATOIRE ******/

                ////console.log("list gamme: ",listGamme)
                if(listGamme.length>0 ){
                  listGamme.forEach((gamme, k) => {
                  
                    let GammeAux:any=Object.values(gamme)[0];
                    let listOperation:any=[];
                   

                    listOperation=Object.values(gamme)[1]; 
                    ////console.log("listOperation: ",listOperation) 
                   
                        
                    //push GAME operatoire intitule with rowspan
                    if (k > 0) {
                      this.rows.push([])
                    }
                
                    
                      this.rows[this.rows.length - 1].push(
                      //set Nom GammeOperatoire
                        {
                        text:  GammeAux?.intitule,
                        rowspan: 1
                        }              
                                           
                      
                      );

                    /******************* set etat operation*****************************/

                    if(listOperation.length>0){

                        //foreach all days of month
                        for(let i=1;i<=12;i++){
                          let operations:any[]=[];//filtred valid
                          ////console.log("========================================= Mois ",i," GammeAux: ",GammeAux?.intitule)
                          let etat:any;
                          let nbrOfOperation:number=0;
                          let etatValide:number=2;//non valide
                          
                          
                            //foreach list operation
                            for(let operation of listOperation){  
                              
                              //find etat in list operation


                                let datestr=operation?.dateExecution?.date;
                                let dateD=new Date(datestr);
          
                                /* ===== set operation Etat 1:Valide;2:non Valide;0:En Instance======*/
                               //même mois && etat valide
                                if((dateD?.getMonth()+1)==i && operation?.etat==1){
                                  ////console.log("Month EXE: ",dateD?.getMonth()+1,"Vs Day: ",i," ===> Etat: ",operation?.etat)
                                  nbrOfOperation+=nbrOfOperation;
                                  etat=operation?.etat;
                                  etatValide=1;
                                  operations.push(operation);
                                  ////console.log("push operation : ",operation)
                                 
                                }                           
                            
                            }//fin foreach operations 
                            
                           // //console.log("==> List operations filtred: ",operations)

                            
                            //set etat after foreach

                            if(operations?.length>=2){
                              this.rows[this.rows.length - 1].push(
                                                  
                                //set etat
                                {
                                  text:  "2+",
                                  rowspan: 1,
                                  operations:operations
                                }                             
                              
                              );
                            }else{
                              this.rows[this.rows.length - 1].push(                                                  
                                //set etat
                                {
                                  text:  etatValide==1?"X":"",
                                  rowspan: 1,
                                  operations:operations
                                }                             
                              
                              );
                            }
                            /*******/

                         }//fin foreach month of year
                       

                    }else{
                       //foreach all month  of year
                       for(let i=1;i<=12;i++){
                        //foreach list operation
                           
                        /* ===== set operation vide ======*/
                        this.rows[this.rows.length - 1].push(
                                        
                            //foreach operationList and set etat
                            {
                              text:  "",
                              rowspan: 1
                            }
                        
                          
                        );                          

                      }
                    }
                   



                  })

                }
                //list gamme vide
                else{
                                
                  this.rows[this.rows.length - 1].push(
                    //checkList
                    {
                    text:  "",
                    rowspan: 1
                    },               
                  
                    
                    );

                    for(let month=1;month<=12 ;month++){

                      this.rows[this.rows.length - 1].push(                                     
                        //operation set
                        {
                          text:  "",
                          rowspan: 1
                        }                      
                        
                        );

                    }
                }

                /******* FIN *******/            


            })


          }
          //list equipement vide
          else{

              //equipement vide              
            this.rows[this.rows.length - 1].push(
              //equipement
              {
                text:  "",
                rowspan: 1
              }, 
              //qte
              {
                text:  "",
                rowspan: 1
              }, 
              //checkList
              {
              text:  "",
              rowspan: 1
              }             
                          
              );

              for(let v=1;v<=12;v++){

                this.rows[this.rows.length - 1].push(                                     
                  //operation set
                  {
                    text:  "",
                    rowspan: 1
                  }                      
                  
                  );

              }
          }
           

         

          /******* fin foreach equipement *****/
        });
        /****** FIN FOREACH SOUS EMPLACEMENT */
        //check rowspan max
        if(maxRowSpan<nbrRowSpanFil){
          maxRowSpan=nbrRowSpanFil;
        }
        if(maxRowSpan<nbrRowSpanEquipement){
          maxRowSpan=nbrRowSpanEquipement;
        }

        //checkMax rowspan
        if(data?.parent?.nbrgammes>maxRowSpan){
          this.rows[0][0].rowspan=data?.parent?.nbrgammes;
        }else{
          this.rows[0][0].rowspan=maxRowSpan;
        }
        //this.rows[0][0].rowspan = data?.parent?.nbrgammes;//maxRowSpan;

       // //console.log("conclusion RowSpan: Parent: ",data?.parent?.nbrgammes);
        ////console.log("rows: ",this.rows)
        indexData ++;
      
        this.listData.push(this.rows);

      //}//fin choix item of data

       


    }

    ////console.log("all Data: ",this.listData)
   


    
  }

  infoColumn(col,typeModal){
    ////console.log("column: ",col);
    ////console.log("row: ",row);
    if(col.text === 'X' || col.text === '2+'){

      this.operationsListClicked=[];
      this.operationsListClicked=col?.operations;

      this.modalService.open(typeModal, { ariaLabelledBy: 'modal-basic-title', size: 'lg' })
      .result.then((result) => {
        ////console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
    }



  }

  gotoOperation(detailOperation){
    //this.router.navigate(['/communite/documents']);
    ////console.log("detailOperation: ",detailOperation.cbmarq);
    this.router.navigate(['/communite/info-preventif/'+detailOperation.cbmarq]);
    this.modalService.dismissAll();
  }


  originalOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return 0;
  }

  reverseKeyOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
  }

  valueOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return a.value.localeCompare(b.value);
  }

}
