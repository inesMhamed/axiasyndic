import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/_directives/_helpers/auth.guard';
import { InterrogationMaintenanceComponent } from './interrogation-maintenance/interrogation-maintenance.component';
import { InterrogationParProduitComponent } from './interrogation-par-produit/interrogation-par-produit.component';
import { InterrogationPayementComponent } from './interrogation-payement/interrogation-payement.component';
import { ListInterrogationComponent } from './list-interrogation/list-interrogation.component';


const routes: Routes = [
  {
    path: 'list-interrogation',
    component: ListInterrogationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'interrogation-par-produit',
    component: InterrogationParProduitComponent, canActivate: [AuthGuard]
  },
  {
    path: 'interrogation-payement',
    component: InterrogationPayementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'interrogation-maintenance',
    component: InterrogationMaintenanceComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterrogationRoutingModule { }
