import { DatePipe, KeyValue } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { echartStyles } from 'src/app/shared/echart-styles';
import { CompteurModel } from 'src/app/shared/models/compteurs.model';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { InterroagtionService } from 'src/app/shared/services/interrogation.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';


class InterrogationModel{
  produit:number;
  exercice:number;
  constructor(){
   this.produit=null;
   this.exercice=null;
  }
}



@Component({
  selector: 'app-interrogation-par-produit',
  templateUrl: './interrogation-par-produit.component.html',
  styleUrls: ['./interrogation-par-produit.component.scss']
})
export class InterrogationParProduitComponent implements OnInit {

  residenceSelected:any;
  excerciceSelected:any;
  listResidence:any[];
  listExercice:any[]=[];

  listSolde:any;
  soldeInitial:any[]=[];
  soldeFinal:any[]=[];
  soldeDepense:any[]=[];
  soldeRecette:any[]=[];


  interrogationModel: InterrogationModel = new InterrogationModel();
  submitSearch: boolean = false;
  chartBar: any;
  dounghoutChart: any;
  chartLine: any;
  dataSourceDetailRecette = new MatTableDataSource<any>();
  displayedColumns: string[] = ['CODE', 'JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN', 'JUIL', 'AOUT', 'SEP', 'OCT', 'NOV', 'DEC', 'TOTAL', 'RESTE'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) Input: MatInput;

  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  listDepense: any;
  listRecette: any[];
  viewDetail: boolean = false;

  /******/
  totalDepense:any[]=[];
  totalDepenseList:any[]=[];
  autresDepense:any[]=[];
  autresDepenseList:any[]=[];

  compteurDepense:any[]=[];
  compteurDepenseList:any[]=[];
  compteurDepenseSubList:any[]=[];
  compteurModelList:CompteurModel[]=[];

  compteurSteg:any[]=[];
  compteurSonede:any[]=[];
  totalCompteurSteg:any[]=[];
  totalCompteurSonede:any[]=[];
  totalCompteur:any[]=[];


  contratDepense:any[]=[];
  contratDepenseList:any[]=[];

  libelleAutresDepense:any[]=[];
  libellecompteurDepense:any[]=[];
  libelleContratDepense:any[]=[];
  sousLibelleCompteur:any[]=[];
  lengthDepenseList:number=0;

  /***  var cheque traite ***/

  listCheque:any[]=[];
  listTraite:any[]=[];
  listTotalChequetraite:any[]=[];
  dialogDetailSolde:boolean=false;
  currentSolde:any;


  id_current_user: any;
  have_access: boolean = false;

  constructor(private interrogationService: InterroagtionService, public router: Router, private datepipe: DatePipe,
    private spinnerservice: NgxSpinnerService,
    private produitService: ResidenceService, private modalService: NgbModal, private permissionservice: DroitAccesService) {

      this.id_current_user = localStorage.getItem('id');

    this.initChart();

   }


  ngOnInit(): void {
    this.spinnerservice.show()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000121').subscribe((res: any) => {

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getListResidence();
      }
     
    });


   

  }




  initChart() {
    this.initDounghoutChart();
    this.initBarChart();
    this.initChartLine();
  }


  initDounghoutChart(){
     
   this.dounghoutChart = {
     title: {
       text: 'Statistique Annuelle',
       subtext: 'Solde Final',
       left: 'center'
     },
       tooltip: {
         trigger: 'item'
       },
       legend: {
         top: '5%',
         orient: 'vertical',
         left: 'left'
       },
       series: [
         {
           name: 'Access From',
           type: 'pie',
           radius: ['40%', '70%'],
           data: [
             { value: 0, name: 'JAN' },
             { value: 0, name: 'FEV' },
             { value: 0, name: 'MARS' },
             { value: 0, name: 'AVR' },
             { value: 0, name: 'MAI' },
             { value: 0, name: 'JUIN' },
             { value: 0, name: 'JUIL' },
             { value: 0, name: 'AOUT' },
             { value: 0, name: 'SEP' },
             { value: 0, name: 'OCT' },
             { value: 0, name: 'SEP' },
             { value: 0, name: 'DEC' },
           ],
           emphasis: {
             itemStyle: {
               shadowBlur: 10,
               shadowOffsetX: 0,
               shadowColor: 'rgba(0, 0, 0, 0.5)'
             }
           }
         }
       ]
   
   };

  }

  initBarChart(){
   this.chartBar = {
     legend: {},
     tooltip: {},  
    
     dataset: {
       source: [
         ['product', 'Prévu', 'Réel'],

         ['Janvier', 0, 0],
         ['Février', 0, 0],
         ['Mars', 0, 0],
         ['Avril', 0, 0],
         ['Mai', 0, 0],
         ['Juin', 0, 0],
         ['Juillet', 0, 0],
         ['Aout', 0, 0],
         ['Septembre', 0, 0],
         ['Octobre', 0, 0],
         ['Novembre', 0, 0],
         ['Décembre', 0, 0],

       ],
       
      
     },
  
     xAxis: { type: 'category' },
     yAxis: {},
     // Declare several bar series, each will be mapped
     // to a column of dataset.source by default.
     series: [
       {
          type: 'bar',
          itemStyle: {
            color: '#0d47a1'
          }

      },
        {
           type: 'bar',
           itemStyle: {
            color: '#26a69a'
          }
          
        }
      
      
      ]
   };
  }

  initChartLine(){

    this.chartLine = {
      title: {
        text: 'Statistique annuelle chèque et traite',
        left: 'center'
      },
      xAxis: {
        type: 'category',
        data:  ['JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN', 'JUIL', 'AOUT','SEP','OCT','NOV','DEC']
      },
      yAxis: {
        type: 'value'
      },
     
      series: [
        {
          name: 'Chèque',
          data: [0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0],
          type: 'line',
          smooth: true,
         
        },
         {
          name: 'Traite',
          data: [0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0],
          type: 'line',
          smooth: true,
         
        }
      ]
    };

    /*this.chartLine = {
      title: {
        //text: 'Stacked Line'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['Chèque', 'Traite']
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN', 'JUIL', 'AOUT','SEP','OCT','NOV','DEC']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: 'Chèque',
          type: 'line',
          stack: 'Total',
          data: [0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0]
        },
        {
          name: 'Traite',
          type: 'line',
          stack: 'Total',
          data: [0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0]
        }
       
      ]
    };*/
  }


  /************ SOLDE  section ***********/
  getListResidence() {
    this.spinnerservice.show()
    this.produitService.getResidences().subscribe((res: any) => {
      if (res.statut) {
        this.listResidence = res.data;
        ////console.log("liste residence: ",this.listResidence)

      }
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.spinnerservice.hide()
      this.interrogationModel.produit = this.listResidence[0].cbmarq;
      ////console.log("get list residence: ",this.interrogationModel)
      this.onChangeProduit(false);
    })

  }

  onChangeProduit(fromFront) {
    this.spinnerservice.show()
    ////console.log("get onChangeProduit : ",this.interrogationModel)

    let residenceSelectedTab:any;
    this.excerciceSelected=null;

    residenceSelectedTab=this.listResidence.filter(elm=>elm.cbmarq==this.interrogationModel.produit);
    this.residenceSelected=residenceSelectedTab[0];
    ////console.log("residence selected: ",this.residenceSelected)

    this.interrogationService.getExerciceParProduit(this.interrogationModel.produit).subscribe((res:any)=>{
      if(res.statut){
        this.listExercice=res.data;
        this.interrogationModel.exercice=this.listExercice[0];
       // //console.log("this.interrogationModel.exercice: ",this.interrogationModel.exercice)
       // //console.log("onChangeProduit interrogation model: ",this.interrogationModel)
        if(!fromFront && this.interrogationModel.exercice){
            this.interroger();
        }

      }
      this.spinnerservice.hide()
    })
  }

  onChangeExercice(){
    ////console.log("exercice selected: ",this.interrogationModel);
    this.excerciceSelected=this.interrogationModel.exercice;

    
   
  }

  interroger(){

    this.submitSearch=true;
   
    ////console.log("interroger....",this.interrogationModel);
    this.excerciceSelected=this.interrogationModel.exercice;
   
    this.interrogerParProduit();
    this.getInterrogationRecetteParProduit();
    this.getInterroagtionDepenceParProduit();
    this.getInterrogationChequeTraite();

     ////console.log("listSolde: ",this.listSolde)
     //this.interrogationModel=new InterrogationModel();
     this.submitSearch=false;

  }

  interrogerParProduit() {
    this.spinnerservice.show()
    this.interrogationService.getInterrogationParProduit(this.interrogationModel).subscribe((res: any) => {

      //console.log("solde : " ,res.data)
      if(res.statut){
        this.listSolde=res.data;

        this.soldeInitial = Object.values(res.data.soldeI);
        this.soldeFinal = Object.values(res.data.soldeF);
        this.soldeDepense = Object.values(res.data.depense);
        this.soldeRecette = Object.values(res.data.recette);


      }
      this.spinnerservice.hide()
    }, error => { }, () => {
      // let dounghoutAux:any;
      this.spinnerservice.hide()
      this.dounghoutChart = {
          title: {
            text: 'Statistique Annuelle',
            subtext: 'Solde Final',
            left: 'center'
          },
            tooltip: {
              trigger: 'item'
            },
            legend: {
              top: '15%',
              orient: 'vertical',
              left: 'left'
            },
            series: [
              {
                name: 'Solde final',
                type: 'pie',
                radius: ['40%', '70%'],
                top: '15%',
                data: [
                  { value: this.soldeFinal[0].toFixed(3), name: 'JAN' },
                  { value: this.soldeFinal[1].toFixed(3), name: 'FEV' },
                  { value: this.soldeFinal[2].toFixed(3), name: 'MARS' },
                  { value: this.soldeFinal[3].toFixed(3), name: 'AVR' },
                  { value: this.soldeFinal[4].toFixed(3), name: 'MAI' },
                  { value: this.soldeFinal[5].toFixed(3), name: 'JUIN' },
                  { value: this.soldeFinal[6].toFixed(3), name: 'JUIL' },
                  { value: this.soldeFinal[7].toFixed(3), name: 'AOUT' },
                  { value: this.soldeFinal[8].toFixed(3), name: 'SEP' },
                  { value: this.soldeFinal[9].toFixed(3), name: 'OCT' },
                  { value: this.soldeFinal[10].toFixed(3), name: 'SEP' },
                  { value: this.soldeFinal[11].toFixed(3), name: 'DEC' },
                ],
                emphasis: {
                  itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                  }
                }
              }
            ]
        
        };

      
    })


  }



    /************ Recette  section ***********/

  getInterrogationRecetteParProduit() {
    this.spinnerservice.show()
    this.interrogationService.getInterrogationRecetteParProduit(this.interrogationModel).subscribe((res: any) => {

        if(res.statut){

          this.listRecette = Object.values(res.data.tabRecette);

          // console.log("listRecette: jjjjjjjjjjjjjjjj ",this.listRecette);
          let keys = Object.keys(res.data.Detailrecette);
          this.dataSourceDetailRecette.data=Object.values(res.data.Detailrecette);

          this.dataSourceDetailRecette.data.forEach((item,i)=>{
            item.code=keys[i];
          })
          this.dataSourceDetailRecette.paginator = this.paginator;
          this.dataSourceDetailRecette.sort = this.sort;

          //console.log("dataSourceRecette: ",this.dataSourceDetailRecette);

      }
      this.spinnerservice.hide()
    }, error => { }, () => {

      this.spinnerservice.hide()
      this.chartBar = {
        legend: {},
        tooltip: {},

        dataset: {
          source: [
            ['product', 'Prévu', 'Réel'],

            ['Janvier', this.listRecette[0].Prevu?.toFixed(3), this.listRecette[0].Reel?.toFixed(3)],
            ['Février', this.listRecette[1].Prevu?.toFixed(3), this.listRecette[1].Reel?.toFixed(3)],
            ['Mars', this.listRecette[2].Prevu?.toFixed(3), this.listRecette[2].Reel?.toFixed(3)],
            ['Avril', this.listRecette[3].Prevu?.toFixed(3), this.listRecette[3].Reel?.toFixed(3)],
            ['Mai', this.listRecette[4].Prevu?.toFixed(3), this.listRecette[4].Reel?.toFixed(3)],
            ['Juin', this.listRecette[5].Prevu?.toFixed(3), this.listRecette[5].Reel?.toFixed(3)],
            ['Juillet', this.listRecette[6].Prevu?.toFixed(3), this.listRecette[6].Reel?.toFixed(3)],
            ['Aout', this.listRecette[7].Prevu?.toFixed(3), this.listRecette[7].Reel?.toFixed(3)],
            ['Septembre', this.listRecette[8].Prevu?.toFixed(3), this.listRecette[8].Reel?.toFixed(3)],
            ['Octobre', this.listRecette[9].Prevu?.toFixed(3), this.listRecette[9].Reel?.toFixed(3)],
            ['Novembre', this.listRecette[10].Prevu?.toFixed(3), this.listRecette[10].Reel?.toFixed(3)],
            ['Décembre', this.listRecette[11].Prevu?.toFixed(3), this.listRecette[11].Reel?.toFixed(3)],

          ]
        },
        xAxis: { type: 'category' },
        yAxis: {},
        // Declare several bar series, each will be mapped
        // to a column of dataset.source by default.
        series: [
          {
            type: 'bar',
            itemStyle: {
              color: '#0d47a1'
            }
  
        },
          {
             type: 'bar',
             itemStyle: {
              color: '#26a69a'
            }
            
          }
         
         
         ]
      };



    });
  }

  afficherDetails(){
    this.viewDetail=!this.viewDetail;
  }

  /************ Depense  section ***********/
  getInterroagtionDepenceParProduit() {
    this.spinnerservice.show()
    this.lengthDepenseList = 0;
    this.compteurModelList = [];
    this.listDepense = [];
    this.totalDepenseList = [];
    this.autresDepense = [];
    this.libelleAutresDepense = [];
    this.lengthDepenseList = 0;
    this.compteurSteg = [];
    this.totalCompteurSonede = [];
    this.totalCompteur = [];
    this.contratDepense = [];
    this.libelleContratDepense = [];

      this.interrogationService.getInterrogationDepenseParProduit(this.interrogationModel).subscribe((res:any)=>{

        ////console.log(res)
        if(res.statut){

          this.listDepense=res.data;
         // //console.log("liste depense: ",this.listDepense)
        
          //total
          this.totalDepenseList=Object.values(res.data.Total);

          //autre depense
          this.autresDepense=Object.values(res.data.autres);
          this.libelleAutresDepense= Object.keys(res.data.autres);        



          //compteur
        
          let i=0;
         
          for (const key in res.data.compteur) {
            if (res.data.compteur.hasOwnProperty(key)) {
             
              // your logic here
              let value = res.data.compteur[key];
              ////console.log("key : ",key,"value: ",value);

              /*----- init rubrique object ----------*/
              let comteurModel:CompteurModel=new CompteurModel();
              comteurModel.rubriqueName=key;
              /* ----- test if key contain total ==> set objet total ----*/
              if(key.includes("Total")){
                comteurModel.total=value;
              
                           
              }else{
                comteurModel.total=null;
              }
              

              /*----- set sous rubrique object -----*/
              let j=0;
              let sousRubriqueList:CompteurModel[]=[];
              for(const key in value){

                if (value.hasOwnProperty(key)) {
                  let valueSub = value[key];
                  ////console.log("keySub : ",key,"valueSub: ",valueSub);
                  //this.sousLibelleCompteur.push(key);
                  let comteurSubModel:CompteurModel=new CompteurModel();
                  comteurSubModel.rubriqueName=key;
                  comteurSubModel.total=valueSub;
                 
                  sousRubriqueList.push(comteurSubModel);
                 
                
                  j++; 
                }
              }

              comteurModel.sousRubriqueList=sousRubriqueList;

              this.compteurModelList.push(comteurModel);             
             


              i++;
            }
      }


     // //console.log("nouvelle liste compteur: ", this.compteurModelList);

      //this.compteurSteg= this.compteurModelList[0].sousRubriqueList;

      for (let cpt of this.compteurModelList) {

        if(cpt.rubriqueName.toLocaleLowerCase().includes("steg") && !cpt.rubriqueName.includes("Total")){
          // your logic here
         this.compteurSteg= cpt.sousRubriqueList;
         this.lengthDepenseList=this.lengthDepenseList+cpt.sousRubriqueList.length;
         ////console.log("++++ LINE STEG : ",this.compteurSteg)

                    
       } 
       if(cpt.rubriqueName.toLocaleLowerCase().includes("sonede") && !cpt.rubriqueName.includes("Total")){
        // your logic here
       this.compteurSonede= cpt.sousRubriqueList;
       this.lengthDepenseList=this.lengthDepenseList+cpt.sousRubriqueList.length;
         ////console.log("++++ LINE  SONEDE : ",this.lengthDepenseList)
                  
     } 

        if(cpt.rubriqueName.toLocaleLowerCase().includes("totalsteg")){
           // your logic here
          this.totalCompteurSteg=Object.values(cpt.total);
          this.lengthDepenseList=this.lengthDepenseList+1;
          ////console.log("++++ LINE STEG TOTAL: ",this.lengthDepenseList)
                     
        }      
       if(cpt.rubriqueName.toLocaleLowerCase().includes("totalsonede")){
        // your logic here
          this.totalCompteurSonede=Object.values(cpt.total);
          this.lengthDepenseList=this.lengthDepenseList+1;
          ////console.log("++++ LINE SONEDE TOTAL: ",this.lengthDepenseList)
                  
        }
        if(!cpt.rubriqueName.includes("totalsteg") && !cpt.rubriqueName.toLocaleLowerCase().includes("totalsonede") && cpt.rubriqueName.toLocaleLowerCase().includes("total") && cpt.rubriqueName.length==5){
          // your logic here
         this.totalCompteur=Object.values(cpt.total);
         this.lengthDepenseList=this.lengthDepenseList+1;
         ////console.log("++++ LINE  TOTAL: ",this.lengthDepenseList)
                    
       }
      }

      ////console.log("lengthLine: ",this.lengthDepenseList)


      ////console.log("totalCompteurSteg: ",this.totalCompteurSteg)

     
     
     

         
        


          //contrat
          this.contratDepense=Object.values(res.data.contrat);
          this.libelleContratDepense=Object.keys(res.data.contrat)
           



      }
      this.spinnerservice.hide()
    });

    }

    originalOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
      return 0;
    }
  
    reverseKeyOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
      return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
    }
  
    valueOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
      return a.value.localeCompare(b.value);
    }

    /*** cheque /traite  */

  getInterrogationChequeTraite() {
    this.spinnerservice.show()
    this.interrogationService.getInterrogationChequeTraiteParProduit(this.interrogationModel).subscribe((res: any) => {
      if (res.statut) {
        ////console.log("res cheque traite: ",res.data)
        //
        this.listCheque = Object.values(res.data.cheque);
        this.listTraite = Object.values(res.data.traite);
        this.listTotalChequetraite = Object.values(res.data.total)

          ////console.log("Cheque Nov: ",this.listCheque[10].Prevu?.toFixed(3))
         // //console.log("Traite Nov: ",this.listTraite[11].Prevu?.toFixed(3))


      }
      this.spinnerservice.hide()
    }, error => { }, () => {
      this.spinnerservice.hide()
      this.chartLine = {
        title: {
          text: 'Statistique annuelle chèque et traite',
          left: 'center'
        },
        xAxis: {
          type: 'category',
          data: ['JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN', 'JUIL', 'AOUT', 'SEP', 'OCT', 'NOV', 'DEC']
        },
        yAxis: {
          type: 'value'
        },
        tooltip: {
          trigger: 'axis'
        },

        series: [
          {
            name: 'Chèque',
            data: [this.listCheque[0].Prevu?.toFixed(3), this.listCheque[1].Prevu?.toFixed(3), this.listCheque[2].Prevu?.toFixed(3), this.listCheque[3].Prevu?.toFixed(3),
            this.listCheque[4].Prevu?.toFixed(3), this.listCheque[5].Prevu?.toFixed(3), this.listCheque[6].Prevu?.toFixed(3), this.listCheque[7].Prevu?.toFixed(3),
            this.listCheque[8].Prevu?.toFixed(3), this.listCheque[9].Prevu?.toFixed(3), this.listCheque[10].Prevu?.toFixed(3), this.listCheque[11].Prevu?.toFixed(3)]
            ,
            type: 'line',
            smooth: true,
            lineStyle: { color: '#0d47a1' }
          },
          {
            name: 'Traite',
            data: [this.listTraite[0].Prevu?.toFixed(3), this.listTraite[1].Prevu?.toFixed(3), this.listTraite[2].Prevu?.toFixed(3), this.listTraite[3].Prevu?.toFixed(3),
            this.listTraite[4].Prevu?.toFixed(3), this.listTraite[5].Prevu?.toFixed(3), this.listTraite[6].Prevu?.toFixed(3), this.listTraite[7].Prevu?.toFixed(3),
            this.listTraite[8].Prevu?.toFixed(3), this.listTraite[9].Prevu?.toFixed(3), this.listTraite[10].Prevu?.toFixed(3), this.listTraite[11].Prevu?.toFixed(3)],
            type: 'line',
            smooth: true,
            lineStyle: { color: '#26a69a' }
          }
        ]
      };

        /*this.chartLine = {
          title: {
            //text: 'Stacked Line'
          },
          tooltip: {
            trigger: 'axis'
          },
          legend: {
            data: ['Chèque', 'Traite']
          },
          grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
          },
          toolbox: {
            feature: {
              saveAsImage: {}
            }
          },
          xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ['JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN', 'JUIL', 'AOUT','SEP','OCT','NOV','DEC']
          },
          yAxis: {
            type: 'value'
          },
          series: [
            {
              name: 'Chèque',
              type: 'line',
              stack: 'Total',
              data: [ this.listCheque[0].Prevu?.toFixed(3), this.listCheque[1].Prevu?.toFixed(3), this.listCheque[2].Prevu?.toFixed(3), this.listCheque[3].Prevu?.toFixed(3),
              this.listCheque[4].Prevu?.toFixed(3), this.listCheque[5].Prevu?.toFixed(3), this.listCheque[6].Prevu?.toFixed(3), this.listCheque[7].Prevu?.toFixed(3),
              this.listCheque[8].Prevu?.toFixed(3), this.listCheque[9].Prevu?.toFixed(3), this.listCheque[10].Prevu?.toFixed(3), this.listCheque[11].Prevu?.toFixed(3)]
            },
            {
              name: 'Traite',
              type: 'line',
              stack: 'Total',
              data: [this.listTraite[0].Prevu?.toFixed(3), this.listTraite[1].Prevu?.toFixed(3), this.listTraite[2].Prevu?.toFixed(3), this.listTraite[3].Prevu?.toFixed(3),
              this.listTraite[4].Prevu?.toFixed(3), this.listTraite[5].Prevu?.toFixed(3), this.listTraite[6].Prevu?.toFixed(3), this.listTraite[7].Prevu?.toFixed(3),
              this.listTraite[8].Prevu?.toFixed(3), this.listTraite[9].Prevu?.toFixed(3), this.listTraite[10].Prevu?.toFixed(3), this.listTraite[11].Prevu?.toFixed(3)]
            }
           
          ]
        };*/
      })

    }

    infoSolde(solde,typeModal){
      ////console.log("solde choisie: ",solde)
      this.dialogDetailSolde=true;

      if(solde.Prevu!=0){
        this.dialogDetailSolde=true;
        this.currentSolde=solde;

        ////console.log("currentSolde: ",this.currentSolde)
    
        this.modalService.open(typeModal, { ariaLabelledBy: 'modal-basic-title' , centered: true})
        .result.then((result) => {
          //console.log(result);
        }, (reason) => {
          //console.log('Err!', reason);
        });
      }
    }


}
