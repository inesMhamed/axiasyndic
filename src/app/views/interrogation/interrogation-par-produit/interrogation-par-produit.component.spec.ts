import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogationParProduitComponent } from './interrogation-par-produit.component';

describe('InterrogationParProduitComponent', () => {
  let component: InterrogationParProduitComponent;
  let fixture: ComponentFixture<InterrogationParProduitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterrogationParProduitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogationParProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
