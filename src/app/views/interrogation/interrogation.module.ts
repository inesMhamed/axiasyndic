import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { InterrogationRoutingModule } from './interrogation-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ngx-custom-validators';
import { ListInterrogationComponent } from './list-interrogation/list-interrogation.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';

import { InterrogationParProduitComponent } from './interrogation-par-produit/interrogation-par-produit.component';
import { MatTabsModule } from '@angular/material/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxEchartsModule } from 'ngx-echarts';
import { InterrogationPayementComponent } from './interrogation-payement/interrogation-payement.component';
import { InterrogationMaintenanceComponent } from './interrogation-maintenance/interrogation-maintenance.component';
import { CustomPaginator } from '../customPaginator';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [ListInterrogationComponent, InterrogationParProduitComponent, InterrogationPayementComponent, InterrogationMaintenanceComponent],
  imports: [
    CommonModule,
    InterrogationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    NgbModule,
    NgSelectModule,
    NgxEchartsModule,
    NgxSpinnerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers:[DatePipe,
    { provide: MatPaginatorIntl, useValue: CustomPaginator() },  // Here
    ,]
})
export class InterrogationModule { }
