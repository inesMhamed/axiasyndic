import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterrogationPayementComponent } from './interrogation-payement.component';

describe('InterrogationPayementComponent', () => {
  let component: InterrogationPayementComponent;
  let fixture: ComponentFixture<InterrogationPayementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterrogationPayementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterrogationPayementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
