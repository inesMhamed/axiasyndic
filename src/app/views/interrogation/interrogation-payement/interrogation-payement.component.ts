import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { InterroagtionService } from 'src/app/shared/services/interrogation.service';
import { ResidenceService } from 'src/app/shared/services/residence.service';
import { AppartementService } from 'src/app/shared/services/appartement.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';

class ObjectDataSource{
  NbrRetard:number;
  TtImpaye:number;
  Tthistorique:number;
  liste:any[];
  constructor(){
    this.liste=[];
  }
}

class InterrogationModel{
  produit:number;
  periodicite:number;
  constructor(){
   this.produit=0;
   this.periodicite=0;
  }
}

@Component({
  selector: 'app-interrogation-payement',
  templateUrl: './interrogation-payement.component.html',
  styleUrls: ['./interrogation-payement.component.scss']
})
export class InterrogationPayementComponent implements OnInit {


  interrogationModel:InterrogationModel=new InterrogationModel();
  searchProduit:any;
  listResidence:any[]=[];
  listType:any[]=[];
  listCategorie:any[]=[];

  dataSource = new MatTableDataSource<any>();
  objectDataSource:ObjectDataSource=new ObjectDataSource();
  displayedColumns: string[] = ['Intitulé'  ,'apayer', 'payer','impaye','historique'  ,'periodicite', 'Action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];
  listCbamrqBien:any[]=[];
  currentDate:Date=new Date();
  formGroupSearch:FormGroup;
  bienToNotif: any;
  impayeToNotif: any;
  id_current_user:any;
  have_access:boolean =false;

  constructor(private produitService: ResidenceService, private interrogationService: InterroagtionService,
    private appartementService: AppartementService, private fb: FormBuilder  ,private toastr: ToastrService
    , private spinnerservice: NgxSpinnerService, private permissionservice: DroitAccesService, public router: Router) {
    this.id_current_user = localStorage.getItem('id');

   }

  ngOnInit(): void {

    this.spinnerservice.show()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser(this.id_current_user, 'FN21000122').subscribe((res: any) => {

      this.have_access =res.data;
     
    },error=>{},()=>{

      if(!this.have_access){
        this.router.navigate(['403']);
      }else{
        this.getListResidence();
        this.getListPeriodicite();
      }
     
    });



    ////console.log("currentDate: ",this.currentDate)
    this.listType=[{name:'Frais Syndic',value:1},{name:'Autres',value:2}]

   
   

    this.formGroupSearch = this.fb.group({      
      produit: new FormControl(0, Validators.required),
      periodicite: new FormControl(0),
     
    }); 
    
    this.formGroupSearch.patchValue({
      produit: 0,
      periodicite: 0,
    })

    this.interroger();

  }

  getListResidence() {
    this.spinnerservice.show()
    this.listResidence = [];
    let tous = { cbmarq: 0, intitule: "Tous" };

    this.produitService.getResidences().subscribe((res:any)=>{
      if(res.statut){
        this.listResidence=res.data;
        this.listResidence.splice(0, 0, tous);
        ////console.log("liste residence: ",this.listResidence)

      }
      this.spinnerservice.hide()
    })

  }

  getListPeriodicite() {
    this.spinnerservice.show()
    let tous = { cbmarq: 0, label: "Tous" };

    this.appartementService.gettypesAppartement('PRD').subscribe((res: any) => {
      if (res.statut) {
        this.listCategorie = res.data;
        this.listCategorie.splice(0, 0, tous);

        ////console.log("listCategorie: ",this.listCategorie)

      }
      this.spinnerservice.hide()
    })

  }




  filterProduit(event){

    let auxList=this.dataSource.data;

    this.dataSource.data = auxList.filter(word=>(

      word?.bien?.intitule.trim().toLowerCase().includes(event.trim().toLowerCase())

    ))

    // this prints the array of filtered

    if(event==""){

      this.interroger();
    }

  }

  interroger(){


    let modelSearch={
      produit:this.formGroupSearch.value.produit==0?null:this.formGroupSearch.value.produit,
      periodicite:this.formGroupSearch.value.periodicite==0?null:this.formGroupSearch.value.periodicite
    }

    this.interrogationService.getInterrogationPayement(modelSearch).subscribe((res:any)=>{

      if(res.statut){
        this.objectDataSource=res.data;

        this.dataSource.data=Object.values(this.objectDataSource.liste);
        this.listCbamrqBien=Object.keys(this.objectDataSource.liste);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;      


      }
    })


  }

  alertPayement(row) {
    this.bienToNotif = row?.bien?.cbmarq;
    this.impayeToNotif = parseFloat(row?.hitorique + row?.impaye);
    this.interrogationService.postNotificationPaiement( this.bienToNotif, this.impayeToNotif).subscribe((res:any)=>{

      if (res.statut) {
        this.toastr.success(res.message, 'Success!', {progressBar: true});
      } else {
        this.toastr.error(res.message, 'Erreur!', {progressBar: true});
      }
    });
  }

getRandomColor() {
  for(let produit of this.dataSource.data){
    var color = Math.floor(0x1000000 * Math.random()).toString(16);
    //produit.color='#' + ('000000' + color).slice(-6);
  }
 
}

}
