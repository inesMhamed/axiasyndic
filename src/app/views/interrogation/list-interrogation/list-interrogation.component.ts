import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InterroagtionService } from 'src/app/shared/services/interrogation.service';
import { DatePipe } from '@angular/common'
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { Permission } from 'src/app/shared/models/permission.model';
import { NgxSpinnerService } from 'ngx-spinner';


class SearchInterroagtion{    
  datedebut: string;
  datefin: string;
  constructor(){
    this.datedebut= null;
    this.datefin= null;
  }

}

@Component({
  selector: 'app-list-interrogation',
  templateUrl: './list-interrogation.component.html',
  styleUrls: ['./list-interrogation.component.scss']
})
export class ListInterrogationComponent implements OnInit {

  searchModel:SearchInterroagtion=new SearchInterroagtion();
  chiffreAffaire:number=0;
  recette:number=0;
  depense:number=0;
  totalSolde:number=0;
  listProduit:any[]=[];
  dataSourceProduit= new MatTableDataSource<any>();
  displayedColumnsProduit: string[] = ['produit'  ,'recette','depense','solde','ca'];
  @ViewChild(MatPaginator, {static: false}) paginatorProd: MatPaginator;
  @ViewChild(MatSort) sortProd: MatSort;

  viewListeOrDetailResidence:boolean=true;

  dataSourceRecette = new MatTableDataSource<any>();
  displayedColumns: string[] = ['Appartement'  ,'Recette', 'action'];
 
  
  dataSourceDepense = new MatTableDataSource<any>();  
  displayedColumnsDepense: string[] = ['Type'  ,'Total Dépense'];
  @ViewChild(MatPaginator, {static: false}) paginatorDep: MatPaginator;
  @ViewChild(MatSort) sortDep: MatSort;

  
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  // MatPaginator Inputs
  length: number = 0;
  pageSize: number = 5;  //displaying three cards each row
  pageSizeOptions: number[] = [5, 10, 25, 100];

  //listRecette:any[]=[];
  listDepense:any[]=[];

  currentResidence:any;
  displayDialogDetailRecette:boolean=false;
  detailRecetteModel:any;
  searchProduit:any;
  dateDebut:Date;
  dateFin:Date;

  id_current_user:any;
  can_ViewIntérrogation_globale:boolean =false;

  constructor(private interrogationService: InterroagtionService, public router: Router,
    private modalService: NgbModal, private datepipe: DatePipe,
    private spinnerservice: NgxSpinnerService,
    private permissionservice: DroitAccesService) {
      this.id_current_user = localStorage.getItem('id');
     


     }

  ngOnInit(): void {
    this.spinnerservice.show()
    //if access false ==> go out to 403
    this.permissionservice.getAccessUser( this.id_current_user, 'FN21000120').subscribe((res:any)=>{

      this.can_ViewIntérrogation_globale =res.data;
      
    },error=>{},()=>{

      if(!this.can_ViewIntérrogation_globale){
        this.router.navigate(['403']);
      }else{
        this.getGlobalInterrogation();
      }
     
    });
  }

  getGlobalInterrogation() {
    this.spinnerservice.show()
    ////console.log("search model get global interroagtion :",this.searchModel)

    this.interrogationService.getGlobalInterrogation(this.searchModel).subscribe((res:any)=>{
      if(res.statut){
        //console.log("globalInterroagtion: ",res)
        this.chiffreAffaire=+res.data?.CA?.toFixed(3);
        this.depense=+res.data?.Depense?.toFixed(3);
        this.recette=+res.data?.Recette?.toFixed(3);
        this.totalSolde=+res.data?.Solde?.toFixed(3);
        this.listProduit=res.data?.Produits;
        this.dataSourceProduit.data=res.data.Produits;
        this.dataSourceProduit.paginator = this.paginatorProd;
        this.dataSourceProduit.sort = this.sort;

      }else{
        //console.log("error loading interrogation")
      }
      this.spinnerservice.hide()
    })

  }

  detailresidence(residence) {
    this.spinnerservice.show()
    this.currentResidence = residence;
    this.viewListeOrDetailResidence = false;

    ////console.log("detail residence: ",this.currentResidence);
    ////console.log("searchmodel: ",this.searchModel)
    this.interrogationService.getDetailRecette(this.searchModel, residence.cbmarq).subscribe((res: any )=>{
      if (res.statut) {
       // //console.log("res detail residence: ",res.data);
        this.dataSourceRecette.data = res.data.listeBien;
        this.dataSourceRecette.paginator = this.paginator;
        this.dataSourceRecette.sort = this.sort;


        this.dataSourceDepense.data = res.data.listeDepense;
        this.dataSourceDepense.paginator = this.paginatorDep;
        this.dataSourceDepense.sort = this.sort;


        this.listDepense = res.data.listeDepense;

      }
      this.spinnerservice.hide()
    })
  }

  Actualiser(){
    this.dateDebut=null;
    this.dateFin=null;
    this.viewListeOrDetailResidence=true;
    this.searchModel=new SearchInterroagtion();
    this.listDepense=[];
    this.dataSourceRecette=new MatTableDataSource<any>();;
    this.getGlobalInterrogation();
  }

  closeDetail(){
    this.viewListeOrDetailResidence=true;
    this.getGlobalInterrogation();
  }

  getDetailRecette(row,typeModal){

   // //console.log("detail recette: ",row)
    if(row.TotalPayed!=0){
      this.displayDialogDetailRecette=true;
      this.detailRecetteModel=row;

      this.modalService.open(typeModal, { ariaLabelledBy: 'modal-basic-title' })
      .result.then((result) => {
        //console.log(result);
      }, (reason) => {
        //console.log('Err!', reason);
      });
    }


  }

  filterProduit(){

    let auxList=this.listProduit;

    if(this.searchProduit!=""){
     
      this.listProduit = auxList.filter(word=>(
       ( word?.intitule.trim().toLowerCase().includes(this.searchProduit.trim().toLowerCase()) 

       )
      ))   
       
    }
   

  // this prints the array of filtered

    if(this.searchProduit==""){
      this.getGlobalInterrogation();
    }
}

rechercheGlobale(){

  ////console.log("dateDebut: ",this.dateDebut);
  ////console.log("Date fin: ",this.dateFin)
  this.searchModel.datedebut=this.datepipe.transform(this.dateDebut, 'yyyy-MM-dd'); 
  this.searchModel.datefin=this.datepipe.transform(this.dateFin, 'yyyy-MM-dd'); 
  this.getGlobalInterrogation();

}

getRandomColor() {
  for(let produit of this.listProduit){
    var color = Math.floor(0x1000000 * Math.random()).toString(16);
    produit.color='#' + ('000000' + color).slice(-6);
  }

}



}
