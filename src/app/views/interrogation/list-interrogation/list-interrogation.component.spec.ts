import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListInterrogationComponent } from './list-interrogation.component';

describe('ListInterrogationComponent', () => {
  let component: ListInterrogationComponent;
  let fixture: ComponentFixture<ListInterrogationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListInterrogationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListInterrogationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
