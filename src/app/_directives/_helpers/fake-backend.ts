// import { Injectable } from '@angular/core';
// import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
// import { Observable, of, throwError } from 'rxjs';
// import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

// // array in local storage for users
// const usersKey = 'angular-9-jwt-refresh-token-users';
// const users = JSON.parse(localStorage.getItem(usersKey)) || [];

// // add test user and save if users array is empty
// // if (!users.length) {
// //     users.push({ id: 1,  firstName: 'Test', lastName: 'User', username: 'test', password: 'test', refreshTokens: [] });
// //     localStorage.setItem(usersKey, JSON.stringify(users));
// // }

// @Injectable()
// export class FakeBackendInterceptor implements HttpInterceptor {
//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         const { url, method, headers, body } = request;

//         // wrap in delayed observable to simulate server api call
//         return of(null)
//             .pipe(mergeMap(handleRoute))
//             .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
//             .pipe(delay(500))
//             .pipe(dematerialize());

//         function handleRoute() {
//             switch (true) {
//                 case url.endsWith('/login') && method === 'POST':
//                     return authenticate();
              
//                 default:
//                     // pass through any requests not handled above
//                     return next.handle(request);
//             }
//         }

//         // route functions

//         function authenticate() {
//             const { username, password } = body;
//             const user = users.find(x => x.username === username && x.password === password);
//             if (!user) return error('Username or password is incorrect');
//             return ok({
//                 id: user.id,
//                 username: user.user.username,
//                 token: user.user.token
//             })
//         }

       

//         // helper functions

//         function ok(body?) {
//             return of(new HttpResponse({ status: 200, body }))
//         }

//         function error(message) {
//             return throwError({ error: { message } });
//         }

//         function unauthorized() {
//             return throwError({ status: 401, error: { message: 'Unauthorised' } });
//         }

//         function isLoggedIn() {
//             return headers.get('Authorization') === 'Bearer fake-jwt-token';
//         }
//     }
// }

// export let fakeBackendProvider = {
//     // use fake backend in place of Http service for backend-less development
//     provide: HTTP_INTERCEPTORS,
//     useClass: FakeBackendInterceptor,
//     multi: true
// };