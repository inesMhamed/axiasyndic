import { Residence } from "./residence.model";

export class Emplacement{
    cbmarq: number;
    intitule: string;
    nbrAppartement: number;
    residence: Residence;

    constructor(){
        
    }

}