class Createur{
    id: number;
    nomComplet:string;
    photo: string;
}

class DateAjtModif{
    date: Date;
    timezone_type: number;
    timezone: string;
}

export class Document{
    
        cbmarq: number;
        cbcreateur:Createur ;
        cbcreation: DateAjtModif;
           
        cbmodification: DateAjtModif;
        intitule: string;
        publier: boolean;
        attachement: string;
        attachementExt: string;
    
}