export class Reunion {
    cbmarq: number
    cbcreateur:any;
    objet: string
    description: string
    emplacement: string
    dateDebut: any
    dateDebut2: any
    dateFin: any
    dateFin2: any
    duree: any
    type: any
    enligne: any
    mode: any
    invitation: any
    nbr_invi: any
    nbr_attente: any
    nbr_accept: any
    conclusion: any
    notes: any[]
    biens: any=[]
    externe: any[]
    interne: any[]
    statut: any[]
    proprietaire: any[]
    attachements: any[]
    cbcreation: Date
    cdmodification: Date
    constructor() {
    }

}
