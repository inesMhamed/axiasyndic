class InfoUser{
    id: number;
    nomComplet: string;
    photo: string;
}

class DateAddUpdate{
    date: string;
    timezone_type: number;
    timezone: string;
}

export class Inbox{
    cbmarq:number;
    cbcreateur:InfoUser;
    destinateur:any[];
    cbcreation:DateAddUpdate;
    cbmodification:DateAddUpdate;
    objet:string;
    message: string;
    attachements:File[];
    statut : number;
    attachementExt: string;
    logoText:string;
    constructor(){
        this.destinateur=[];
        this.attachements=[];
    }

    
      
}
