export class Preferences {
    cbmarq: number
    cbcreation: Date
    code: string
    societe: string
    matricule_fiscale: string
    telephone1: string
    telephone2: string
    telephone3: string
    email1: string
    email2: string
    email3: string
    ville: string
    adresse: string
    code_postale: string
    gouvernorat: string
    payer: string
    activite: string
    devise: string
    round_prix: number
    round_qte: number
    max_ecart: string
}