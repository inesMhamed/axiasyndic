export class Transfert {
    typeCaisseSource: number
    idCaisseSource: number
    userSource: number
    typeCaisseDestination: number
    idCaisseDestination: number
    userDestination:number
    date: Date
    montant: string
    lignes:any[]=[]
    constructor() {
    }

}