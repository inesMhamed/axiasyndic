
class Createur{
    id: number;
    name:string;
    photo:string;

}

export class Commentaire{
    cbcreateur: Createur ;
    cbcreation: string;
    cbmarq: number;
    commentaire: string;
    logoText:string;
}
