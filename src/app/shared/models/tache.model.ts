export class Tache{
    cbmarq:any;
    intervenant:string;
    tel:string;
    dateDebut: Date;
    dateFin: Date;
    heureDebut:Date;
    heureFin:Date;
    description:string;
    statut:boolean;// true: corrigé ; false: non corrigé

    constructor(){
        
    }

}