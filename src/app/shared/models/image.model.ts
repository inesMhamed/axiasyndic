export class Image{
    previewImageSrc?;
    thumbnailImageSrc?;
    alt?;
    title?;
}