export class Reclamation {
    cbmarq: number
    objet: string
    description: string
    cbcreateur: any
    type: any
    priorite: any
    nbrc: any
    statut: any
    emplacement: any
    commentaire: any[]
    appartement: any[]
    attachements: any[]
    cbcreation: Date
    cbmodification: Date
    constructor() {
    }

}
