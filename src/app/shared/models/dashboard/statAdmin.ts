export class StatAdmin{

    /*** admin */
    NbrMessageNnLus:number;
    NbrOperation:number;
    NbrReclamations:number;
    NbrRetardPay:number;
    NbrTransfertEncours:number;
    listeReunion:any[];
    listetransfert:any[];
    recettes:any[];
    actualites:any[];
    allrec:number;
    firstRec:any;
    secondRec:any;
    url:any;


    /*** prop ***/
    ReactionReclamation:number;
    TotalAutreFrais:number;
    DayReunion:any;
    CoopropToActive:any[];
    PayfraisSyndic:number;
    listeOperation:any[];
    TransfertRecu:any[];
    listeDocuments:any[];

    constructor(){
        this.listeReunion=[];
        this.listetransfert=[];
        this.recettes=[];
        this.actualites=[];
        this.NbrMessageNnLus=0;
        this.NbrOperation=0;
        this.NbrReclamations=0;
        this.NbrRetardPay=0;
        this.NbrTransfertEncours=0;
        this.allrec=0;

        this.TotalAutreFrais=0;
        this.CoopropToActive=[];
        this.PayfraisSyndic=0;
        this.listeOperation=[];
        this.TransfertRecu=[];
        this.listeDocuments=[];
    }


}