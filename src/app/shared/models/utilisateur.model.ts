export class Utilisateur {
    id: number
    nomComplet: string
    username:string
    login:string
    pwd:string
    email: string
    active: boolean
    proprietaire: boolean
    appartements:any[]=[]
    token?: string;
    constructor() {
    }

    
}
