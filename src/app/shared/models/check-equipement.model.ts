import { MoisPreventif } from "./mois-preventif.model";
import { Tache } from "./tache.model";

export class CheckEquipement{
    cbmarq:any;
    intitule:any;//intitule equipement
    quantite:any;//qte equipement
    echeance:number; //echeance equipement
    annee:number;
    cbcreation:Date;
    cbModification:Date;
    statut:any; //attente //encours // corrigé // non corrigé
    
    tachesList:Tache[];
    moisPreventifList:MoisPreventif[];
    constructor(){
        this.tachesList=[];  
        this.moisPreventifList=[];
    }
}