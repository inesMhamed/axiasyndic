export class Proprietaire {
    id: number
    nomComplet: string
    username:string
    login:string
    pwd:string
    email: string
    active: boolean
    proprietaire: boolean
    lignes:any[];
    appartements:any[];
    photo:string;
    //wz
    logoText:string;
    constructor() {
        this.lignes=[];
        this.appartements=[];
    }

    
}
