export class Residence {
    cbmarq: number
    intitule: string
    adresse: string
    description: string
    nbrBloc: number
    nbrAppartement: number
    president: number
    vice: number
    tresorerie: number
    groupement: number
    cbcreation: Date
    cdmodification: Date
    constructor() {
    }

}
