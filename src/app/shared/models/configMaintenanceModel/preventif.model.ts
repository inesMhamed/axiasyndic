
import { Gamme } from "./gamme.model";
import { StatutPreventif } from "./statutPreventif.model";


export class Preventif{

    
        cbmarq: any;
        cbcreation: any;
        cbmodification:any;
        datedeb: any;
        dateExecution:any;
        dateNotif:any;
        date: number;
        notifavant: number;
        km: any;
        heuure: any;
        gamme: Gamme;
        etat: any;
        statut: StatutPreventif;

        commentaire:string;
        imgavant:File;
        imgapres:File;

        constructor(){
            this.statut=new StatutPreventif();
            this.gamme=new Gamme();
            this.commentaire="";
        }
    

}
