import { ParentEmpl } from "./parentEmpl.model";
import { ProduitGamme } from "./produitGamme.model";

export class EmplacementPreventif{

    cbmarq: any;
    cbcreation: any;       
    intitule: string;
    produit: ProduitGamme;
    parent: ParentEmpl;

    constructor(){
        this.produit=new ProduitGamme();
        this.parent=new ParentEmpl();
    }

}