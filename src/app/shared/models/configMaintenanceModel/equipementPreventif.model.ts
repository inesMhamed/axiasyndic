import { EmplacementPreventif } from "./emplacementPreventif.model";
import { FamilleEquipement } from "./familleEquipement.model";
import { ProduitGamme } from "./produitGamme.model";

export class EquipementPreventif{
    cbmarq: any;
    cbcreation: any;
    cbmodification: any;
    intitule: string ;
    quantite: number;
    attachement: any;
    produit: ProduitGamme;
    emplacement: EmplacementPreventif;
    famille:FamilleEquipement;

    constructor(){
        this.produit=new ProduitGamme();
        this.emplacement=new EmplacementPreventif();
        this.famille=new FamilleEquipement();
    }
}