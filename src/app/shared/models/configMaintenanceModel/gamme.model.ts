import { EquipementPreventif } from "./equipementPreventif.model";

export class Gamme{
    cbmarq: any;
    cbcreation: any;
    cbmodification:any;
    intitule: string;
    equipement:EquipementPreventif;
    constructor(){
        this.equipement=new EquipementPreventif();
    }
}