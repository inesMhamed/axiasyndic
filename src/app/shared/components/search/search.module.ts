import { NgModule } from '@angular/core';
import {HighlightSearch, SearchComponent} from './search.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ReactiveFormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        PerfectScrollbarModule,
        RouterModule
    ],
    declarations: [SearchComponent, HighlightSearch],
    exports: [SearchComponent]
})
export class SearchModule {

}
