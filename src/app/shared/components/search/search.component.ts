import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { DataLayerService } from '../../services/data-layer.service';
import { Observable, combineLatest } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, debounceTime, switchMap, map, distinctUntilChanged } from 'rxjs/operators';
import { SharedAnimations } from '../../animations/shared-animations';
import { SearchService } from '../../services/search.service';
import { ResidenceService } from '../../services/residence.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Permission } from '../../models/permission.model';
import { DroitAccesService } from '../../services/droit-acces.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ToastrService } from 'ngx-toastr';
@Pipe({
  name: 'highlight'
})
export class HighlightSearch implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(value: any, args: any): any {
    if (!args) {
      return value;
    }
    // Match in a case insensitive maneer
    const re = new RegExp(args, 'gi');
    const match = value.match(re);

    // If there's no match, just return the original value.
    if (!match) {
      return value;
    }

    const replacedValue = value.replace(re, '<mark>' + match[0] + '</mark>')
    return this.sanitizer.bypassSecurityTrustHtml(replacedValue);
  }
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [SharedAnimations]
})
export class SearchComponent implements OnInit {
  page = 1;
  pageSize = 6;
  desactive: boolean = false;
  residences: any = [];
  data: any = [];
  results$: Observable<any[]>;
  searchCtrl: FormControl = new FormControl('');
  searchTerm: string;
  can_edit: Permission;
  can_active: Permission;
  id_current_user: any;
  constructor(
    private permissionservice: DroitAccesService,
    private dl: DataLayerService,
    private toastr: ToastrService,
    public searchService: SearchService,
    private residenceService: ResidenceService,
  ) { }

  ngOnInit() {
    this.id_current_user = localStorage.getItem('id');
    this.can_edit = this.permissionservice.search(this.id_current_user, 'FN21000002');
    this.can_active = this.permissionservice.search(this.id_current_user, 'FN21000010');
    this.results$ = combineLatest(
      this.listeresidence(),
      this.searchCtrl.valueChanges
        .pipe(startWith(''), debounceTime(200)
        ))
      .pipe(map(([residences, searchTerm]) => {
        if (residences[0] != null) {
          return residences[0].filter(p => {
            return p.intitule.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || p.adresse.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || p.nbrAppartement.toString().indexOf(searchTerm.toLowerCase()) > -1 || p?.type?.label.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
          });
        }
      }));
  }

  updateSearch(e) {
    this.searchTerm = e.target.value;
  }
  cancelCmp(cbmarq: any, active: any, title: any) {

    this.residenceService.desactiverResidences(cbmarq, active).subscribe(
      (res: any) => {
        if (res.statut === true) {
          this.toastr.success('Produit  ' + title + ' avec succès !');
          this.listeresidence();
          this.ngOnInit();
        } else {
          this.toastr.error(res.message);
        }
      },
      error => {
        this.toastr.error('Veuillez réessayer plus tard!');
      }
    );
  }
  confirmBox(cbmarq: any, active: any) {
    var title;
    if (this.desactive === false) {
      title = 'Désactiver';
    } else {
      title = 'Activer';
    }
    Swal.fire({
      title: title + ' un produit',
      text: 'Voulez-vous vraiment ' + title + ' ce produit ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirmer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        //console.log('active', this.desactive);
        this.cancelCmp(cbmarq, this.desactive, title);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  public listeresidence() {
    return this.residences = this.residenceService.getSearchResidences();
  }

}
