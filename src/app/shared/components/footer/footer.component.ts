import { Component, OnInit } from '@angular/core';
import {ParametreService} from '../../services/parametre.service';
import {ProprietaireService} from '../../services/proprietaire.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  id_current_user: any;
  root: any;
  role: any;
  IdSoc: any;
  url: any = null;
  year: number;

  constructor(private paramtreService: ParametreService , private proprietaireService: ProprietaireService) {
    this.id_current_user = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.root = this.role.includes('ROLE_ROOT');
  }

  ngOnInit() {
    this.year=(new Date()).getFullYear()
    this.getFicheClient();
  }

  getInfoSociete(IdSoc) {
    this.paramtreService.getInfoSociete(IdSoc).subscribe((res: any) => {

      if (res.statut) {
        this.url = res.data.logo;
      }

    });
  }
  getFicheClient() {
    this.proprietaireService.getProprietairebyId(this.id_current_user).subscribe((res: any) => {
      if (res.statut === true) {
        this.IdSoc = res?.data?.societe;
      }
    }, error => {
    }, () => {
      this.getInfoSociete(this.IdSoc);
    });
  }
}
