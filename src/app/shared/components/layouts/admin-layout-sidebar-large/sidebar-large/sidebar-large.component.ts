import { Component, OnInit, HostListener, ViewChildren, QueryList } from '@angular/core';
import {
  NavigationService,
  IMenuItem,
  IChildItem
} from '../../../../services/navigation.service';
import { Router, NavigationEnd } from '@angular/router';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';

import { filter } from 'rxjs/operators';
import { Utils } from '../../../../utils';
import {DroitAccesService} from '../../../../services/droit-acces.service';

@Component({
  selector: 'app-sidebar-large',
  templateUrl: './sidebar-large.component.html',
  styleUrls: ['./sidebar-large.component.scss']
})
export class SidebarLargeComponent implements OnInit {
  selectedItem: IMenuItem;
  nav: IMenuItem[];
  test: any;
  one: any;
  id: any;
  role: any;
  Admin: any;
  root: any;
  fonct: any = [];
  pack: any = [];
  @ViewChildren(PerfectScrollbarDirective) psContainers: QueryList<PerfectScrollbarDirective>;
  psContainerSecSidebar: PerfectScrollbarDirective;

  constructor(public router: Router, public navService: NavigationService,  private droitaccesService: DroitAccesService) {

    this.id = localStorage.getItem('id');
    this.role = localStorage.getItem('role');
    this.Admin = this.role.includes('ROLE_ADMIN');
    this.root = this.role.includes('ROLE_ROOT');
    this.pack = JSON.parse(localStorage.getItem('pack'));

    setTimeout(() => {
      this.psContainerSecSidebar = this.psContainers.toArray()[1];
    });
  }

  ngOnInit() {


    // console.log("wzzzzzz localstorage: ",localStorage , this.root)

    if (this.id) {
      this.droitaccesService.getFonctionalitesUser(this.id).subscribe(
          (res: any) => {
            if (res.statut === true) {
              console.log('res==================>',res)
              this.test = res.data;
              this.test.forEach(element => {
                this.fonct[element.intitule] = element.fonctionnalites;
              });
            } else {
              this.test = [];
            }


          },
          err => {
            //console.log(err);
          },()=>{

            //debut
            /*this.test.forEach(element => {
              this.fonct[element.intitule] = element.fonctionnalites;
            });*/

            if (this.Admin || this.root) {
              this.navService.defaultMenu.forEach(reslt => {
                reslt.disabled = false;
                reslt.sub.forEach(subMenu => {
                  if (subMenu.name === 'Log Système' )
                    subMenu.disabled = !this.root ;
                  else
                  subMenu.disabled = false;
                });
              });

              if (this.Admin) {
                this.navService.defaultMenu.forEach(reslt => {
                  reslt.disabled = !this.pack[reslt?.name];
                  // console.log(this.pack[reslt?.name], reslt?.name);
                  reslt.sub.forEach(subMenu => {
                    if (subMenu.name === 'Log Système' )
                      subMenu.disabled = !this.root ;
                    else
                      subMenu.disabled = false;
                  });
                });

              }

              ////console.log("IS ADMIN: ",this.navService.defaultMenu)
            } else {

              ////console.log("NOT ADMIN this.navService.defaultMenu: ",this.navService.defaultMenu);
              console.log("this.fonct: ",this.fonct)
              this.navService.defaultMenu.forEach(reslt => {
                //reslt.disabled = !this.fonct.hasOwnProperty(reslt.name);
               

                ////console.log("Module: ",reslt)
                this.one = this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000003');
                reslt?.sub?.forEach(resx => {

                  /******* SYNDIC  OK*******/
                  if (resx.name === 'Produits')
                    resx.disabled = !this.one?.statut;
                  if (resx.name === 'Groupement Résidences')
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000014')?.statut;
                  if (resx.name === 'Biens')
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000023')?.statut ;
                  if (resx.name === 'Services')
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000038')?.statut ;
                  if (resx.name === 'Compteurs')
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000033')?.statut ;
                  
                  
                  /****** COMMUNAUTE ******/
                  if (resx.name === 'Réunions')
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000095')?.statut ;
                  if (resx.name === 'Messageries')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000097')?.statut ;
                  if (resx.name === 'Documents')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000098')?.statut ;

                  /******* MAINTENANNCE  *****/
                  if (resx.name === 'Réclamations')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000084')?.statut ;
                  if (resx.name === 'Interventions Préventives')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000090')?.statut ;

                   //Générer maintenance 
                   if (resx.name === 'Générer Maintenances')
                   resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000124')?.statut ;

                   /****** TRISORERIE  ********/
                   if (resx.name === 'Règlements')
                   resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000066')?.statut ;

                   if (resx.name === 'Dépenses')
                   resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000072')?.statut ;

                   if (resx.name === 'Caisses')
                   resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000056')?.statut ||
                    !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000061')?.statut ;

                  if (resx.name === 'Transferts')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000078')?.statut ;

                  /******* ADMINISTRATION *********/
                  if (resx.name === 'Utilisateurs')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000045')?.statut ;

                  if (resx.name === 'Utilisateurs Mobiles')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000045')?.statut ;

                  if (resx.name === 'Propriétaires & Coo') {
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000041')?.statut ||
                        !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000042')?.statut ;
                  }

                  if (resx.name === 'Droit d\'accès')
                    resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000049')?.statut ;

                  if (resx.name === 'Log Système' )
                    resx.disabled = !this.root ;
                  // else
                  // resx.disabled = true ;
                  if (resx.name === 'Log Administration')
                    resx.disabled = !( this.Admin || this.root) ;
                  // else
                  // resx.disabled = true ;
                  /************ ANALYTICS  *************/
                  // if (resx.name === 'Intérrogation Globale')
                  //   resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000099')?.statut ;
                  //consultation Intérrogation globale
                  if (resx.name === 'Intérrogation Globale')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000120')?.statut ;
                  ////console.log("resx.name: ",resx.name);
                  //consultation interrogation par produit                  
                  if (resx.name === 'Intérrogation par produit')
                  resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000121')?.statut ;

                 // Intérrogation payement                
                 if (resx.name === 'Intérrogation payement')
                 resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000122')?.statut ;

                // Intérrogation Maintenance                 
                if (resx.name === 'Intérrogation Maintenance')
                resx.disabled = !this.fonct[reslt?.name]?.find(x => x.reference === 'FN21000123')?.statut ;
                });



                /**** check module ****/
                reslt.disabled=true;                
                let findOne=reslt?.sub.find(x=> !x.disabled)
                if(findOne)
                {
                  reslt.disabled=false;
                }

              });
            }
            //fin

            this.updateSidebar();
            // CLOSE SIDENAV ON ROUTE CHANGE
            this.router.events
              .pipe(filter(event => event instanceof NavigationEnd))
              .subscribe(routeChange => {
                this.closeChildNav();
                if (Utils.isMobile()) {
                  this.navService.sidebarState.sidenavOpen = false;
                }
              });

            this.navService.menuItems$.subscribe(items => {
              this.nav = items;
              this.setActiveFlag();
            });

          });


    }
    /*this.updateSidebar();
    // CLOSE SIDENAV ON ROUTE CHANGE
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(routeChange => {
        this.closeChildNav();
        if (Utils.isMobile()) {
          this.navService.sidebarState.sidenavOpen = false;
        }
      });

    this.navService.menuItems$.subscribe(items => {
      this.nav = items;
      this.setActiveFlag();
    });*/
  }

  selectItem(item) {
    this.navService.sidebarState.childnavOpen = true;
    this.navService.selectedItem = item;
    this.setActiveMainItem(item);

    // Scroll to top secondary sidebar
    setTimeout(() => {
      this.psContainerSecSidebar.update();
      this.psContainerSecSidebar.scrollToTop(0, 400);
    });
  }
  closeChildNav() {
    this.navService.sidebarState.childnavOpen = false;
    this.setActiveFlag();
  }

  onClickChangeActiveFlag(item) {
    this.setActiveMainItem(item);
  }
  setActiveMainItem(item) {
    this.nav.forEach(i => {
      i.active = false;
    });
    item.active = true;
  }

  setActiveFlag() {
    if (window && window.location) {
      const activeRoute = window.location.hash || window.location.pathname;
      this.nav.forEach(item => {
        item.active = false;
        if (activeRoute.indexOf(item.state) !== -1) {
          this.navService.selectedItem = item;
          item.active = true;
        }
        if (item.sub) {
          item.sub.forEach(subItem => {
            subItem.active = false;
            if (activeRoute.indexOf(subItem.state) !== -1) {
              this.navService.selectedItem = item;
              item.active = true;
            }
            if (subItem.sub) {
              subItem.sub.forEach(subChildItem => {
                if (activeRoute.indexOf(subChildItem.state) !== -1) {
                  this.navService.selectedItem = item;
                  item.active = true;
                  subItem.active = true;
                }
              });
            }
          });
        }
      });
    }
  }

  updateSidebar() {
    if (Utils.isMobile()) {
      this.navService.sidebarState.sidenavOpen = false;
      this.navService.sidebarState.childnavOpen = false;
    } else {
      this.navService.sidebarState.sidenavOpen = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.updateSidebar();
  }
}
