import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../../../services/navigation.service';
import { SearchService } from '../../../../services/search.service';
import { AuthenticationService } from 'src/app/shared/services/authentification.service';
import { Router } from '@angular/router';
import { UtilisateurService } from 'src/app/shared/services/utilisateur.service';
import { DatePipe } from '@angular/common';
import { ContratService } from 'src/app/shared/services/contrat.service';
import { DroitAccesService } from 'src/app/shared/services/droit-acces.service';
import { Permission } from 'src/app/shared/models/permission.model';
import Swal from 'sweetalert2';
import { ReunionService } from 'src/app/shared/services/reunion.service';
import { InvitationReunion } from 'src/app/shared/models/invitationReunion';
import { ToastrService } from 'ngx-toastr';
import { ParametreService } from 'src/app/shared/services/parametre.service';



@Component({
  selector: 'app-header-sidebar-large',
  templateUrl: './header-sidebar-large.component.html',
  styleUrls: ['./header-sidebar-large.component.scss'],
  providers: [DatePipe]
})
export class HeaderSidebarLargeComponent implements OnInit {
  id: any
  notifications: any[];
  details: any;
  fonctionalites: any ;
  role: string;
  listAlert: any = [];
  listInvitReunion:InvitationReunion[]=[];
  lengthAlert: number=0;
  token = localStorage.getItem('token');
  decodedToken: any;
  expirationDate: Date;
  isExpired: boolean;

  can_viewMaintenance: Permission = new Permission();
  can_ViewType: Permission = new Permission();
  can_ViewNotifications: Permission = new Permission();
  can_ViewSociete: Permission = new Permission();
  isReunionExpire: boolean = false;
  datePipe: DatePipe = new DatePipe('en-FR');
  listSocietes: any;
  infosociete: any;
  itemselect: any='';


  constructor(
    private contratService: ContratService, private datepipe: DatePipe,  private reunionService: ReunionService,
    private router: Router,
    private navService: NavigationService,
    public searchService: SearchService,
    private authService: AuthenticationService,
    private userService: UtilisateurService,
    private paramtreService: ParametreService,
    private permissionservice: DroitAccesService,
    private toastr: ToastrService
  ) {
   
  }
  compareDate(date1: Date, date2: Date): number {
    // With Date object we can compare dates them using the >, <, <= or >=.
    // The ==, !=, ===, and !== operators require to use date.getTime(),
    // so we need to create a new instance of Date with 'new Date()'
    let d1 = new Date(date1); let d2 = new Date(date2);

    // Check if the dates are equal
    let same = d1.getTime() === d2.getTime();
    if (same) return 0;

    // Check if the first is greater than second
    if (d1 > d2) return 1;

    // Check if the first is less than second
    if (d1 < d2) return -1;
  }
  ngOnInit() {
    this.id = localStorage.getItem('id')
    if (this.id) {
      this.getlistSocietes()

      this.getDetailsUser(this.id)
      this.getListeAlert();
      this.getListInvitationReunion();

      this.can_viewMaintenance = this.permissionservice.search( this.id, "FN21000114");
      this.can_ViewType = this.permissionservice.search( this.id, 'FN21000127');
      this.can_ViewNotifications = this.permissionservice.search( this.id, 'FN21000125');
      this.can_ViewSociete = this.permissionservice.search( this.id, 'FN21000126');

    }

    this.role = localStorage.getItem('role')
  }
  changeStatut(event) {
    this.contratService.setStatus(event).subscribe((alert: any) => {
      if(alert.statut){
        this.getListeAlert()
        //console.log(alert)
      }
     
    })
  }
  getListeAlert(){
    var hier = new Date()
    hier = new Date(hier.setDate(hier.getDate() - 1))
    var datehier = this.datePipe.transform(new Date(hier), 'yyyy-MM-dd HH:mm')
     this.contratService.getAlertContrats().subscribe((alert: any) => {
       if(alert.statut){
        this.listAlert = alert?.data
        alert.data.forEach(element => {
          if (element.dateAlertFS)
          if ((new Date(element.dateAlertFS)).getTime() >= hier.getTime()) {
            console.log("list notif alert :======> ",element)
          }
          // console.log("list notif alert : ", alert.data)
          // this.listAlert = alert?.data.forEach(element => {
          //   if (element.dateAlert != null) {
          //     console.log("hier:",hier)
          //     console.log("dateAlert:",element.dateAlert.date.getTime())
          //     if (element.dateAlert.date.getTime() == hier.getTime()) { console.log("qqqqqqqqqq ") }
          //   }
          // });
          // // this.listAlert
          // console.log("this.listAlert : ", this.listAlert)
        this.lengthAlert = alert?.data?.filter((el: any) => { return el?.statut == false  }).length
      
        })
      }
     })
  }

  getListInvitationReunion(){
    this.lengthAlert=0;
   
    this.reunionService.getListInvitationReunionByUser().subscribe((res:any)=>{

      if(res.statut){
        //console.log("res : ",res.data)

          //this.listInvitReunion=res.data.filter(item=>item.presence==0);
          res.data.forEach(element => {

            if(element.presence==0){
              if(this.returnDifferenceTime(element.reunion.datedebut.date,element.reunion.datefin.date)<=0){
                element.expirer=true;
              }else{
              this.listInvitReunion.push(element);
              element.expirer = false;
            }

            this.listInvitReunion.filter((reu: any) => { })
            }
            
          });
         
          //console.log("this.listInvitation : ",this.listInvitReunion)
          this.lengthAlert=this.listInvitReunion.length;        

      }
  })


  }

  
  returnDifferenceTime(datedebut,datefin){

    let dDay = new Date(datedebut);
    let timeDifference = dDay.getTime() - new  Date().getTime();
   if (timeDifference > 0) {
       //this.allocateTimeUnits(timeDifference);
   } else {
       dDay = new Date(datefin);
       timeDifference = dDay.getTime() - new  Date().getTime();
       if (timeDifference > 0) {
           //this.allocateTimeUnits(timeDifference);
       }
   }

   return timeDifference;

  }

  confirmInvitationReunion(invitation){

    //console.log("invitation : ",invitation)

    /*** check reunion expiré */
    if(this.returnDifferenceTime(invitation.reunion?.datedebut.date,invitation.reunion?.datefin.date)<=0){ 
      this.toastr.error("Réunion Expiré !","Information")          
      this.isReunionExpire=true;
      

    }else{

      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success ml-2',
          cancelButton: 'btn btn-danger',
          denyButton: 'btn btn-primary ml-2'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Confirmer présence?',
  
        html:  "<div style='text-align:justify'><p>Madame/Mademoiselle/Monsieur,</p>"+ 
        "<p>Nous avons le plaisir de vous informer de la tenue d'une réunion "+(invitation.reunion?.type?.label?invitation.reunion.type.label:'')+".</p>"+
        "<p>Elle se déroulera le "+(invitation.reunion.datedebut? this.datePipe.transform(invitation.reunion.datedebut.date, 'dd/MM/yyyy HH:mm')
        : '')+" dans nos locaux de "+(invitation.reunion.emplacement?invitation.reunion.emplacement:'')+".</p></div>",   
        icon: 'warning',
        showCancelButton: true,
        showDenyButton: true,
        confirmButtonText: 'Oui, confirmer!',
        cancelButtonText: 'Non, refuser!',
        denyButtonText: `Plus d'informations`,
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          this.presenceReunion(invitation.reunion.cbmarq,1);
         
        } else if (result.dismiss === Swal.DismissReason.cancel ) {       
          this.presenceReunion(invitation.reunion.cbmarq,2);    
        }
        else if (result.isDenied) {
          this.router.navigate(['communite/detail-reunion/' + invitation.reunion.cbmarq])
        }
      })


    }


  

  }


  presenceReunion(cbmarq,action){
          
    let msg:string="";
    if(action==1){
        msg="Vous avez accepté d\'assister la réunion !";
    }else{
        msg="Vous avez refuser d\'assister la réunion !";

    }

    let modelPresence={
        presence: action,
        commentaire: ""
    }
  

    this.reunionService.presenceReunion(cbmarq,modelPresence).subscribe((res:any)=>{
        if(res.statut){
                this.toastr.success(msg, 'Success!', {progressBar: true});
                this.getListInvitationReunion();
                                                          

        }
        })
  


  }
  getlistSocietes() {
    this.paramtreService.getListSociete().subscribe((res: any) => {
      if (res.statut) {
        this.listSocietes = res.data
      }

    });
  }
  getsociete(idsoc) {
    this.paramtreService.getInfoSociete(idsoc).subscribe((res: any) => {
      if (res.statut) {
        this.infosociete = res.data
      }

    });
  }

  changeSociete(idsociete) {
    this.paramtreService.updateSociete(idsociete).subscribe((res: any) => {
      if (res.statut) {
        // this.toastr.success('Société a été changée avec succès !')
        this.getsociete(idsociete)
        window.location.reload()

      }

    });
  }


  getDetailsUser(id) {
    this.userService.getuserbyId(id).subscribe((admin: any) => {
      this.details = admin.data
      console.log("eeee", this.details)
      this.getsociete(this.details.societe)
    })
  }
  toggelSidebar() {
    const state = this.navService.sidebarState;
    if (state.childnavOpen && state.sidenavOpen) {
      return state.childnavOpen = false;
    }
    if (!state.childnavOpen && state.sidenavOpen) {
      return state.sidenavOpen = false;
    }
    // item has child items
    //console.log("this.navService.selectedItem: ",this.navService.selectedItem)
    if (!state.sidenavOpen && !state.childnavOpen
      && this.navService.selectedItem?.type === 'dropDown') {
      state.sidenavOpen = true;
      setTimeout(() => {
        state.childnavOpen = true;
      }, 50);
    }
    // item has no child items
    if (!state.sidenavOpen && !state.childnavOpen) {
      state.sidenavOpen = true;
    }
  }

logout() {
  this.authService.logout();
  localStorage.clear();


  this.router.navigate(['/authentification/login']);
}

}
