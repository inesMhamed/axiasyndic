import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'timeAgo' })
export class TimeAgoPipe implements PipeTransform {
  transform(d: any): string {

    let currentDate = new Date(new Date().toUTCString());
    let date = new Date(d );//+ "Z"

    let year = currentDate.getFullYear() - date.getFullYear();
    let month = currentDate.getMonth() - date.getMonth();
    let day = currentDate.getDate() - date.getDate();
    let hour = currentDate.getHours() - date.getHours();
    let minute = currentDate.getMinutes() - date.getMinutes();
    let second = currentDate.getSeconds() - date.getSeconds();

    let createdSecond = (year * 31556926) + (month * 2629746) + (day * 86400) + (hour * 3600) + (minute * 60) + second;


    /*=========================================*/
    /*============= English Version ===========*/
    /*=========================================*/
    /*if (createdSecond >= 31556926) {
      let yearAgo = Math.floor(createdSecond / 31556926);
      return yearAgo > 1 ? yearAgo + " years ago" : yearAgo + " year ago";
    } else if (createdSecond >= 2629746) {
      let monthAgo = Math.floor(createdSecond / 2629746);
      return monthAgo > 1 ? monthAgo + " months ago" : monthAgo + " month ago";
    } else if (createdSecond >= 86400) {
      let dayAgo = Math.floor(createdSecond / 86400);
      return dayAgo > 1 ? dayAgo + " days ago" : dayAgo + " day ago";
    } else if (createdSecond >= 3600) {
      let hourAgo = Math.floor(createdSecond / 3600);
      return hourAgo > 1 ? hourAgo + " hours ago" : hourAgo + " hour ago";
    } else if (createdSecond >= 60) {
      let minuteAgo = Math.floor(createdSecond / 60);
      return minuteAgo > 1 ? minuteAgo + " minutes ago" : minuteAgo + " minute ago";
    } else if (createdSecond < 60) {
      return createdSecond > 1 ? createdSecond + " seconds ago" : createdSecond + " second ago";
    } else if (createdSecond < 0) {
      return "0 second ago";
    }*/

    /*========================================*/
    /*============= French Version ===========*/
    /*========================================*/
    if (createdSecond >= 31556926) {
        let yearAgo = Math.floor(createdSecond / 31556926);
        return yearAgo > 1 ? ("il y a "+yearAgo +" ans" ): ("il y a "+yearAgo +" an");
      } else if (createdSecond >= 2629746) {
        let monthAgo = Math.floor(createdSecond / 2629746);
        return monthAgo > 1 ? ("il y a "+monthAgo + " mois") :("il y a "+monthAgo + " mois");
      } else if (createdSecond >= 86400) {
        let dayAgo = Math.floor(createdSecond / 86400);
        return dayAgo > 1 ? ("il y a "+dayAgo + " jours") :("il y a "+dayAgo + " jour");
      } else if (createdSecond >= 3600) {
        let hourAgo = Math.floor(createdSecond / 3600);
        return hourAgo > 1 ? ("il y a "+hourAgo + " heures") : ("il y a "+hourAgo + " heure");
      } else if (createdSecond >= 60) {
        let minuteAgo = Math.floor(createdSecond / 60);
        return minuteAgo > 1 ? ("il y a "+minuteAgo + " minutes") : ("il y a "+minuteAgo + " minute");
      } else if (createdSecond < 60) {
        return createdSecond > 1 ? ("il y a "+createdSecond + " secondes") : ("il y a "+createdSecond + " seconde");
      } else if (createdSecond < 0) {
        return "il y a 0 seconde";
      }

  }
}
