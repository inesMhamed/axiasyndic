import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable()
export class AuthenticationService {

  public token: string;
  infos: any;
  headers: any;

  constructor(private http: HttpClient) {

  }

  login(username: string, password: string) {


    return this.http.post<any>(`${environment.apiUrl}/login`, { username: username, password: password }
    )
      .pipe(
        map(
          user => {

            if (user.data?.token) {
              this.token = user.data.token;
              // set token property
              localStorage.setItem('pack',  JSON.stringify(user.data.pack));
              localStorage.setItem('token', user.data.token);
              localStorage.setItem('username', user.data.username);
              localStorage.setItem('id', user.data.id);
              localStorage.setItem('role', user.data.role);
              localStorage.setItem('refresh_token', user.refresh_token);

              return user;
            }
            else {
              // return false to indicate failed login
              return user;
            }
          },
          err => {
            return err;
            // return false;
          }

        ));
  }
  refreshtoken(id) {
    return this.http.get<any>(`${environment.apiUrl}/refresh/${(id)}`)
  }
  getdetailsUser() {
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('id');
    localStorage.removeItem('refresh_token');


  }

  getCurrentUser(): any {
    return localStorage.getItem('username');

  }

  getUserToken(): any {
    return localStorage.getItem('token');
  }
  getRoleUser() {
    return localStorage.getItem('role')

  }
  isLoggedIn(): any {
    if (localStorage.getItem('token') != null) {
      return true;
    } else {
      return false;
    }
  }
  GetUserId(): any {
    if (localStorage.getItem('username')) {
      let user = JSON.parse(localStorage.getItem('username'));
      alert('fff');
      return user.id;
    } else {
      return false;
    }
  }




}
