import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Reclamation } from '../models/reclamation.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ReclamationService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

 
    getReclamations() {
        return this.http.get(`${environment.apiUrl}/communite/reclamation`,this.httpOptions);
    }

    postReclamation(reclamation: any , rec: any) {
        return this.http.post(`${environment.apiUrl}/communite/reclamation/ajout/${(rec)}`, reclamation,this.httpOptions);
    }

    postcommentaire(reclamation: any , commentaire: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reclamation/commentaire/${(reclamation)}`, commentaire,this.httpOptions);
    }

    changeStatut(reclamation: any , corps: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reclamation/statut/${(reclamation)}`, corps,this.httpOptions);
    }

    getReclamation(cbmarq) {
        return this.http.get(`${environment.apiUrl}/communite/reclamation/${(cbmarq)}`,this.httpOptions);
    }

    putReunion(reunion: any, cbmarq: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/edit/${(cbmarq)}`, reunion,this.httpOptions);
    }

    deleteAttachement(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/communite/reclamation/delete/${(cbmarq)}`,this.httpOptions);
    }

    deleteReclamation(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/communite/reclamation/delete/${(cbmarq)}`,this.httpOptions);
    }
}
