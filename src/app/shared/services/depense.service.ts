import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Residence } from '../models/residence.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DepenseService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getAllDepenses() {
        return this.http.get(`${environment.apiUrl}/depense`,this.httpOptions);
    }

    getDepensebyId(cbmarq) {
        return this.http.get(`${environment.apiUrl}/depense/${(cbmarq)}`,this.httpOptions);
    }

    getTypeDepense(code) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`,this.httpOptions);
    }
   
    postDepense(depense: any) {
        return this.http.post(`${environment.apiUrl}/depense`, depense,this.httpOptions);
    }
    addDocument(ducument: any,cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/depense/add-document/${(cbmarq)}`, ducument,this.httpOptions);
    }
    putDepense(depense: any) {
        return this.http.put(`${environment.apiUrl}/depense/${(depense.cbmarq)}`, depense,this.httpOptions);
    }
    deleteDepense(cbmarq){
        return this.http.delete(`${environment.apiUrl}/depense/${(cbmarq)}`,this.httpOptions);
    }
    cancelDepense(cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/depense/annulation/${(cbmarq)}`,'',this.httpOptions);
    }
    confirmDepense(cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/depense/confirmation/${(cbmarq)}`,'',this.httpOptions);
    }

}
