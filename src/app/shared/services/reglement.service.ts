import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Reglement } from '../models/reglement.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ReglementService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getReglements() {
        return this.http.get(`${environment.apiUrl}/reglement`,this.httpOptions);
    }

    postReglements(reglement: any ) {
        //console.log('servicer', reglement)
        return this.http.post(`${environment.apiUrl}/reglement`, reglement,this.httpOptions);
    }

    getReglementbyCbmarq(cbmarq) {
        return this.http.get(`${environment.apiUrl}/reglement/${(cbmarq)}`,this.httpOptions);
    }
    getReglementbyApaprtement(cbmarq) {
        return this.http.get(`${environment.apiUrl}/reglement/appartement/${(cbmarq)}`,this.httpOptions);
    }
    getReglementbyProp(cbmarq) {
        return this.http.get(`${environment.apiUrl}/reglement/prop/${(cbmarq)}`,this.httpOptions);
    }
    putReglements(reglement: any, cbmarq: any ) {
        return this.http.put(`${environment.apiUrl}/reglement/${(cbmarq)}`, reglement,this.httpOptions);
    }

    getPorpietaire() {
        return this.http.get(`${environment.apiUrl}/root/users/proprietaire`,this.httpOptions);
    }
    getCooproprietaire(){
        return this.http.get(`${environment.apiUrl}/proprietaire/co-prop`,this.httpOptions);

    }
    getTypePayement(codeCategorie) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(codeCategorie)}`,this.httpOptions);
    }
    getlistFrais(proprietaire) {
        return this.http.get(`${environment.apiUrl}/reglement/echeances/${(proprietaire)}`,this.httpOptions);
    }
    getTypeFrais(codeCategorie) {
        return this.http.get(`${environment.apiUrl}/root/parametre/${(codeCategorie)}`,this.httpOptions);
    }
    deleteReglement(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/reglement/${(cbmarq)}`,this.httpOptions);
    }
    confirmReglement(cbmarq) {
        return this.http.post(`${environment.apiUrl}/reglement/confirmation/${(cbmarq)}`,'',this.httpOptions);
    }
    cancelReglement(cbmarq) {
        return this.http.post(`${environment.apiUrl}/reglement/annulation/${(cbmarq)}`,'',this.httpOptions);
    }
    geExerciceReglement(cbmarq) {
        return this.http.get(`${environment.apiUrl}/reglementExercice/appartement/${(cbmarq)}`,this.httpOptions);
    }
    updateDuplicate(cbmarq) {
        return this.http.get(`${environment.apiUrl}/reglement/duplicate/${(cbmarq)}`,this.httpOptions);

    }

    checkDuplicate(cbmarq) {
        return this.http.get(`${environment.apiUrl}/reglement/check_duplicate/${(cbmarq)}`,this.httpOptions);

    }
}
