import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Residence } from '../models/residence.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProprietaireService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getAllProprietaires() {
        return this.http.get(`${environment.apiUrl}/proprietaire`,this.httpOptions);
    }
    getAllCoProprietaires() {
        return this.http.get(`${environment.apiUrl}/proprietaire/co-prop`,this.httpOptions);
    }
    getProprietairesResidence(cbmarq) {
        return this.http.get(`${environment.apiUrl}/prop/byresidence/${(cbmarq)}`, this.httpOptions);
    }
    getProprietairebyId(cbmarq) {
        return this.http.get(`${environment.apiUrl}/utilisateur/detail/${(cbmarq)}`,this.httpOptions);
    }
    addPropAppartement(appartement: any,cbmarq:any,type:any) {
        return this.http.post(`${environment.apiUrl}/bien/add-prop-to-appart/${(cbmarq)}/${(type)}`,appartement, this.httpOptions);
    }
    getPropsAppartement(cbmarq:any,type:any) {
        return this.http.get(`${environment.apiUrl}/bien/propbyappart/${(cbmarq)}/${(type)}`, this.httpOptions);
    }

    postProprietaire(proprietaire: any) {
        return this.http.post(`${environment.apiUrl}/proprietaire`, proprietaire,this.httpOptions);
    }
    postCoProprietaire(proprietaire: any) {
        return this.http.post(`${environment.apiUrl}/proprietaire/add-coprop`, proprietaire,this.httpOptions);
    }
    putProprietaire(proprietaire: any,cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/edit/${(cbmarq)}`, proprietaire,this.httpOptions);
    }
    putCoProprietaire(proprietaire: any,cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/edit/${(cbmarq)}`, proprietaire,this.httpOptions);
    }
    cancelProprietaire(cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/desactivation/${(cbmarq)}`,'',this.httpOptions);
    }
    confirmProprietaire(cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/activation/${(cbmarq)}`,'',this.httpOptions);
    }
    removeProp(idappart,idprop){
        return this.http.delete(`${environment.apiUrl}/bien/remove-proprietaire-from-appart/${(idappart)}/${(idprop)}`,this.httpOptions);
    }
    editphoto(formPhoto:any,cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/edit-photo/${(cbmarq)}`,formPhoto,this.httpOptions);
    }

    getFicheClient(cbmarq:any){

        return this.http.get(`${environment.apiUrl}/ficheClient/${(cbmarq)}`,this.httpOptions);

    }

    getFicheClientInterne(cbmarq:any){

        return this.http.get(`${environment.apiUrl}/ficheuserinterne/${(cbmarq)}`,this.httpOptions);

    }

    activateCopropMobile(formActivate:any){
        return this.http.post(`${environment.apiUrl}/mobile/activate-coprop`,formActivate,this.httpOptions);

    }
}
