import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Residence } from '../models/residence.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UtilisateurService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getAllUtilisateurs() {
        return this.http.get(`${environment.apiUrl}/utilisateur/user-intern`,this.httpOptions);
    }

    getUtilisateurPhone() {
        return this.http.get(`${environment.apiUrl}/proprietairehaveapplication`,this.httpOptions);
    }
    getUtilisateurbyId(cbmarq) {
        return this.http.get(`${environment.apiUrl}/utilisateur/detail/${(cbmarq)}`,this.httpOptions);
    }

    postUtilisateur(user: any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/user-intern`, user,this.httpOptions);
    }
    
    putUtilisateur(user: any,cbmarq:any) {
        return this.http.post(`${environment.apiUrl}/utilisateur/edit/${(cbmarq)}`, user,this.httpOptions);
    }
    getuserbyId(cbmarq) {
        return this.http.get(`${environment.apiUrl}/utilisateur/detail/${(cbmarq)}`,this.httpOptions);
    }
    activeUser(id){
        return this.http.post(`${environment.apiUrl}/utilisateur/activation/${(id)}`,this.httpOptions);
    }
    cancelUser(id){
        return this.http.post(`${environment.apiUrl}/utilisateur/desactivation/${(id)}`,this.httpOptions);
    }
    changePassword(form) {
        return this.http.post(`${environment.apiUrl}/mobile/updatepassword`, form, this.httpOptions);
    }
}
