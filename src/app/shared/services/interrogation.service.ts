import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})


export class InterroagtionService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }

    getGlobalInterrogation(modelSearach) {
        return this.http.post(`${environment.apiUrl}/interrogation/global`,modelSearach,this.httpOptions);
    }

    getDetailRecette(modelSearach,cbmarq) {

        return this.http.post(`${environment.apiUrl}/interrogation/detailrecette/${(cbmarq)}`,modelSearach,this.httpOptions);
    }

    getExerciceParProduit(cbmarq){
        return this.http.get(`${environment.apiUrl}/exercices/par_produit/${(cbmarq)}`,this.httpOptions);

    }

    getInterrogationParProduit(interrogationModel){
        return this.http.post(`${environment.apiUrl}/interrogation/par_produit`,interrogationModel,this.httpOptions);

    }

    getInterrogationRecetteParProduit(interrogationModel){

        return this.http.post(`${environment.apiUrl}/interrogation/recette/par_produit`,interrogationModel,this.httpOptions);

    }

    getInterrogationDepenseParProduit(interrogationModel){

        return this.http.post(`${environment.apiUrl}/interrogation/depense/par_produit`,interrogationModel,this.httpOptions);

    }

    getInterrogationChequeTraiteParProduit(interrogationModel){

        return this.http.post(`${environment.apiUrl}/interrogation/cheque/par_produit`,interrogationModel,this.httpOptions);

    }

    /** interrogation payement **/
    getInterrogationPayement(interrogationModel){

        return this.http.post(`${environment.apiUrl}/interrogation/paiement`,interrogationModel,this.httpOptions);

    }
    /** interrogation maintenance */
    postNotificationPaiement(bien: any, impaye: any) {

        return this.http.get(`${environment.apiUrl}/notification_paiement/${(bien)}/${(impaye)}`, this.httpOptions);

    }
    /** interrogation maintenance */
    getInterrogationMaintenance(interrogationModel){

        return this.http.post(`${environment.apiUrl}/interrogation/maintenance`,interrogationModel,this.httpOptions);

    }


    /*********/
    getExerciceByProduit(cbmarq){

        return this.http.get(`${environment.apiUrl}/maintenance/exercice/par_produit/`+cbmarq,this.httpOptions);

    }

    getEmplacementParent(produit){

        return this.http.get(`${environment.apiUrl}/emplacement_parent/`+produit,this.httpOptions);

    }

}
