import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConfigNotificationService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }

    getConfigNotif() {

        return this.http.get(`${environment.apiUrl}/administration/notification`,this.httpOptions);
    }
    
    postConfigNotif(modeleNotif: any) {
        return this.http.post(`${environment.apiUrl}/administration/notification`, modeleNotif,this.httpOptions);
    }

}
