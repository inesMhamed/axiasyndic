

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
    };
    constructor(
        private http: HttpClient
    ) { }

    getstatisticAdmin() {
        return this.http.get(`${environment.apiUrl}/dashbord/statistic1`, this.httpOptions);
    }

    getstatisticInterne() {
        return this.http.get(`${environment.apiUrl}/dashbord/statistic2`, this.httpOptions);
    }

    getstatisticProprietaire() {
        return this.http.get(`${environment.apiUrl}/dashbord/statistic3`, this.httpOptions);
    }
   
}
