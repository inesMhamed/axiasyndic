import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppartementService {
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
    };
    constructor(
        private http: HttpClient
    ) {
    }

    getAppartementByBloc(bloc: any) {
        return this.http.get(`${environment.apiUrl}/bien/appartement/bybloc/${(bloc)}`, this.httpOptions);
    }
    getAppartementByBlocHaveProp(bloc) {
        return this.http.get(`${environment.apiUrl}/bien/appartement/haveprop/bybloc/${(bloc)}`, this.httpOptions);
    }
    getAppartementResidence(residence: any) {
        return this.http.get(`${environment.apiUrl}/bien/${(residence)}`, this.httpOptions);
    }
    getAppartementResidenceprop(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/bien/haveprop/byresidence/${(cbmarq)}`, this.httpOptions);
    }
    getAppartementGroupementprop(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/bien/appartement/haveprop/bygroupement/${(cbmarq)}`, this.httpOptions);
    }
    getpropOFappartment(id, type) {
        return this.http.get(`${environment.apiUrl}/bien/propbyapparts/${(id)}/${(type)}`, this.httpOptions);
    }
    getAppartements() {
        return this.http.get(`${environment.apiUrl}/bien`, this.httpOptions);
    }
    getAppartementsProp(cbmarq) {
        return this.http.get(`${environment.apiUrl}/bien/byprop/${(cbmarq)}`, this.httpOptions);
    }
    getPropAppartements(cbmarq, type) {
        return this.http.get(`${environment.apiUrl}/bien/propbyapparts/${(cbmarq)}/${(type)}`, this.httpOptions);
    }
    getCoPropAppartements(cbmarq, type) {
        return this.http.get(`${environment.apiUrl}/bien/propbyapparts/${(cbmarq)}/${(type)}`, this.httpOptions);
    }

    postAppartement(appartement: any) {
        return this.http.post(`${environment.apiUrl}/bien/add`, appartement, this.httpOptions);
    }

    genererAppartement(appartement) {
        return this.http.post(`${environment.apiUrl}/administration/produit/generate/appartement`, appartement, this.httpOptions);
    }
    getAppartement(cbmarq) {
        return this.http.get(`${environment.apiUrl}/bien/id/${(cbmarq)}`, this.httpOptions);
    }
    gettypesAppartement(code: any) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`, this.httpOptions);
    }
    getnaturesAppartement(code: any) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`, this.httpOptions);
    }

    putAppartement(appartement: any,cbmarq) {
        return this.http.put(`${environment.apiUrl}/bien/${(cbmarq)}`, appartement, this.httpOptions);
    }
    deleteAppartement(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/bien/${(cbmarq)}`, this.httpOptions);
    }
    deletePropAppartement(raison,idappart: any,idprop: any) {
        return this.http.post(`${environment.apiUrl}/bien/remove-proprietaire-from-appart/${(idappart)}/${(idprop)}`,raison, this.httpOptions);
    }
    postFrais(cbmarq: any, appartement: any) {
        return this.http.post(`${environment.apiUrl}/bien/frais-syndic/${(cbmarq)}`, appartement, this.httpOptions);
    }
    postFraisAgain(cbmarq: any, appartement: any) {
        return this.http.post(`${environment.apiUrl}/bien/frais-syndic/autre/${(cbmarq)}`, appartement, this.httpOptions);
    }
    putFrais(cbmarq: any, appartement: any) {
        return this.http.post(`${environment.apiUrl}/bien/frais-syndic/put/${(cbmarq)}`, appartement, this.httpOptions);
    }
    getyearFrais(cbmarq: any, annee: any) {
        return this.http.get(`${environment.apiUrl}/bien/frais-syndic/${(cbmarq)}/${(annee)}`, this.httpOptions);
    }
    getAllFrais(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/bien/frais-syndic/liste/${(cbmarq)}`, this.httpOptions);
    }
    getFraisAppartement(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/bien/frais-syndic/appartement/detail/${(cbmarq)}`, this.httpOptions);
    }
    getSolvabiliteAppartement(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/bien/solvabilite/${(cbmarq)}`, this.httpOptions);
    }
    getHistoriqueProp(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/bien/log_data/${(cbmarq)}`, this.httpOptions);
    }

    postPeriodicite(cbmarq,periodicite){

        return this.http.get(`${environment.apiUrl}/bien/bien/periodicite/`+cbmarq+'/'+periodicite, this.httpOptions);

    }

    genererFraisAnnuel(cbmarqProduit: any, formGenerer: any) {

        return this.http.post(`${environment.apiUrl}/bien/generer/frais-syndic/${(cbmarqProduit)}`, formGenerer, this.httpOptions);
    }
}
