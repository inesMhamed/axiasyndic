import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Compteur } from '../models/compteur.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CompteurService {
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
    };
    constructor(
        private http: HttpClient
    ) { }

    getCompteurs() {
        return this.http.get(`${environment.apiUrl}/administration/compteurs`, this.httpOptions);
    }
    getCompteursResidence(residence: any) {
        return this.http.get(`${environment.apiUrl}/administration/compteur/${(residence)}`, this.httpOptions);
    }

    getCompteursGroupement(groupement: any) {
        return this.http.get(`${environment.apiUrl}/administration/compteurbygroupement/${(groupement)}`, this.httpOptions);
    }
    postCompteur(compteur: any) {
        return this.http.post(`${environment.apiUrl}/administration/compteur`, compteur, this.httpOptions);
    }

    getCompteur(cbmarq) {
        return this.http.get(`${environment.apiUrl}/administration/compteur/id/${(cbmarq)}`, this.httpOptions);
    }

    putCompteur(compteur: any, cbmarq: any) {
        return this.http.post(`${environment.apiUrl}/administration/compteur/${(cbmarq)}`, compteur, this.httpOptions);
    }

    gettypesCompteurs(code: any) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`, this.httpOptions);
    }

    getappartements(residence: any) {
        return this.http.get(`${environment.apiUrl}/administration/appartement/${(residence)}`, this.httpOptions);
    }

    deleteCompteur(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/administration/compteur/${(cbmarq)}`, this.httpOptions);
    }
}
