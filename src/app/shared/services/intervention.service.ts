import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})


export class InterventionService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }

    /*** preventif */
    getListPreventif() {
        return this.http.get(`${environment.apiUrl}/preventif`,this.httpOptions);
    }

    addEditPreventif(modelPreventif){
        return this.http.post(`${environment.apiUrl}/addEditPreventif`,modelPreventif,this.httpOptions);

    }

    deletePreventif(cbmarq){
        return this.http.delete(`${environment.apiUrl}/deletePreventif/`+cbmarq,this.httpOptions);

    }

    /**** maintenance ****/
    getListMaintenance(){
        return this.http.get(`${environment.apiUrl}/maintenance`,this.httpOptions);

    }

    addEditCorrectif(modelCorrectif){
        return this.http.post(`${environment.apiUrl}/addEditCorrectif`,modelCorrectif,this.httpOptions);

    }

    deleteCorrectif(cbmarq){
        return this.http.delete(`${environment.apiUrl}/deleteCorrectif/`+cbmarq,this.httpOptions);

    }
   
    
}
