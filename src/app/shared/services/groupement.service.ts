import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Groupement } from '../models/groupement.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GroupementService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getGroupements() {
        return this.http.get(`${environment.apiUrl}/administration/groupement`,this.httpOptions);
    }

    postGroupement(groupement: any ) {
        return this.http.post(`${environment.apiUrl}/administration/groupement`, groupement,this.httpOptions);
    }

    getGroupement(cbmarq) {
        return this.http.get(`${environment.apiUrl}/administration/groupement/${(cbmarq)}`,this.httpOptions);
    }

    putGroupement(groupement: any, cbmarq: any ) {
        return this.http.post(`${environment.apiUrl}/administration/groupement/${(cbmarq)}`, groupement,this.httpOptions);
    }
    deleteGroupement(cbmarq: any ) {
        return this.http.delete(`${environment.apiUrl}/administration/groupement/${(cbmarq)}`,this.httpOptions);
    }
}
