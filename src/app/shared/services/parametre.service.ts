import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ParametreService {
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
    };
    constructor(
        private http: HttpClient
    ) {
    }

   /**** params types */
    getListParametre(code: any) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`, this.httpOptions);
    }

    postParametre(modelParams){
        return this.http.post(`${environment.apiUrl}/root/parametre/admin`,modelParams, this.httpOptions);

    }

    putParametre(modelParams){
        return this.http.post(`${environment.apiUrl}/root/parametre/put`,modelParams, this.httpOptions);

    }

    deleteParametre(cbmarq){

        return this.http.delete(`${environment.apiUrl}/root/parametre/`+cbmarq, this.httpOptions);

    }

    /**** params societe */
   getInfoSociete(cbmarq){
    return this.http.get(`${environment.apiUrl}/root/societe/`+cbmarq, this.httpOptions);
   }
   getListSociete(){
    return this.http.get(`${environment.apiUrl}/root/societes`, this.httpOptions);
   }
   updateSociete(cbmarq){
    return this.http.get(`${environment.apiUrl}/root/societe/update/`+cbmarq, this.httpOptions);
   }
    /**** params societe */
   putInfoSociete(cbmarq, form) {
    return this.http.post(`${environment.apiUrl}/root/societe/put/`+cbmarq, form, this.httpOptions);

   }

}
