import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ConfigMaintenanceService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }

    /***** Famille equipement ***/
    addFamilleEquipement(modelFamille) {
        return this.http.post(`${environment.apiUrl}/famille/ajout`,modelFamille, this.httpOptions);
    }
    putFamilleEquipement(modelFamille: any,cbmarq) {
        return this.http.put(`${environment.apiUrl}/famille/${(cbmarq)}`, modelFamille, this.httpOptions);
    }
    deleteFamilleEquipement(cbmarq: any) { 
        return this.http.delete(`${environment.apiUrl}/famille/${(cbmarq)}`, this.httpOptions);
    }  
     
    getListFamilleEquipement() {
        return this.http.get(`${environment.apiUrl}/familles`,this.httpOptions);
    }

    getFamilleById(cbmarq){
        return this.http.get(`${environment.apiUrl}/famille/`+cbmarq,this.httpOptions);

    }

    /***** Equipement ***/
    addEquipement(modelEquipement) {
        return this.http.post(`${environment.apiUrl}/equipement/ajout`,modelEquipement, this.httpOptions);
    }
    putEquipement(modelEquipement: any,cbmarq) {
        return this.http.post(`${environment.apiUrl}/equipement/${(cbmarq)}`, modelEquipement, this.httpOptions);
    }
    deleteEquipement(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/equipement/${(cbmarq)}`, this.httpOptions);
    }  
     
    getListEquipement() {
        return this.http.get(`${environment.apiUrl}/equipements`,this.httpOptions);
    }

    getEquipementById(cbmarq){
        return this.http.get(`${environment.apiUrl}/equipement/`+cbmarq,this.httpOptions);

    }

    getEquipementsByFiltre(modelSearch){        
        return this.http.post(`${environment.apiUrl}/equipementsByFiltre`,modelSearch,this.httpOptions);

    }

    getEquipementsByEmplacement(modelSearch){
        return this.http.post(`${environment.apiUrl}/equipementsByemplacement`,modelSearch,this.httpOptions);

    }

     /***** gamme operatoire ***/
     addGammeOperatoire(modelGammeOperatoire) {
        return this.http.post(`${environment.apiUrl}/gamme/ajout`,modelGammeOperatoire, this.httpOptions);
    }
    putGammeOperatoire(modelGammeOperatoire: any,cbmarq) {
        return this.http.put(`${environment.apiUrl}/gamme/${(cbmarq)}`, modelGammeOperatoire, this.httpOptions);
    }
    deleteGammeOperatoire(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/gamme/${(cbmarq)}`, this.httpOptions);
    }  
     
    getListGammeOperatoire() {
        return this.http.get(`${environment.apiUrl}/gammes`,this.httpOptions);
    }

    getGOperatoireById(cbmarq){
        return this.http.get(`${environment.apiUrl}/gamme/`+cbmarq,this.httpOptions);

    }

    getGammeByFiltre(modelSearch){

        return this.http.post(`${environment.apiUrl}/gammesbyfilter/`,modelSearch,this.httpOptions);

    }

    /***** operation ***/
    addOperation(modelOperation) {
        return this.http.post(`${environment.apiUrl}/operation/ajout`,modelOperation, this.httpOptions);
    }
    putOperation(modelOperation: any,cbmarq) {
        return this.http.put(`${environment.apiUrl}/operation/${(cbmarq)}`, modelOperation, this.httpOptions);
    }
    deleteOperation(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/operation/${(cbmarq)}`, this.httpOptions);
    }  
     
    getListOperation() {
        return this.http.get(`${environment.apiUrl}/operations`,this.httpOptions);
    }

    getOperationById(cbmarq){
        return this.http.get(`${environment.apiUrl}/operation/`+cbmarq,this.httpOptions);

    }

    operationsEncours(cbmarqProduit,statut){
        return this.http.get(`${environment.apiUrl}/operationsEncours/`+cbmarqProduit+'/'+statut,this.httpOptions);

    }

    postOperationImage(cbmarq,modelOperationImage){
        return this.http.post(`${environment.apiUrl}/operation/images/`+cbmarq,modelOperationImage, this.httpOptions);

    }

    filterOperation(modelSearch){
        return this.http.post(`${environment.apiUrl}/operationsfilter`,modelSearch,this.httpOptions);

    }

    filterOperationByExercice(modelSearch){
        return this.http.post(`${environment.apiUrl}/operationsfilter/exercice`,modelSearch,this.httpOptions);

    }


    genererOperation(exercice){
        return this.http.get(`${environment.apiUrl}/generer/operationbyexercice/`+exercice,this.httpOptions);

    }


    /**** action / tâche ***/
    addAction(modelAction) {

        return this.http.post(`${environment.apiUrl}/actions/ajout`,modelAction, this.httpOptions);
    }
    putAction(modelAction: any,cbmarq) {

        return this.http.post(`${environment.apiUrl}/actions/put/${(cbmarq)}`, modelAction, this.httpOptions);
    }
    deleteAction(cbmarq: any) {

        return this.http.delete(`${environment.apiUrl}/actions/delete/${(cbmarq)}`, this.httpOptions);
    }

    getListActions(idOperation) {
        return this.http.get(`${environment.apiUrl}/actions/liste/`+idOperation,this.httpOptions);
    }

    setEtatAction(cbmarq,modelEtat){
        return this.http.post(`${environment.apiUrl}/actions/etat/`+cbmarq,modelEtat,this.httpOptions);

    }

    postOperationEtat(idOperation,etat) {

        return this.http.get(`${environment.apiUrl}/operationsEtat/`+idOperation+'/'+etat, this.httpOptions);
    }





    /*** emplacement */
    addEmplacement(modelemplacement) {
        return this.http.post(`${environment.apiUrl}/emplacement/ajout`,modelemplacement, this.httpOptions);
    }
    putEmplacement(modelemplacement: any,cbmarq) {
        return this.http.post(`${environment.apiUrl}/emplacement/${(cbmarq)}`, modelemplacement, this.httpOptions);
    }
    deleteEmplacement(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/emplacement/${(cbmarq)}`, this.httpOptions);
    }

    getListEmplacement() {
        return this.http.get(`${environment.apiUrl}/emplacements`,this.httpOptions);
    }

    getListEmplacementByProduit(idProduit){

        return this.http.get(`${environment.apiUrl}/emplacements/parents/`+idProduit,this.httpOptions);

    }

    getEmplacementById(cbmarq){
        return this.http.get(`${environment.apiUrl}/emplacement/`+cbmarq,this.httpOptions);

    }

    getEmplacementByFiltre(modelFiltre){

        return this.http.post(`${environment.apiUrl}/emplacementsByFiltre`,modelFiltre,this.httpOptions);

    }

    getEmplacementByFiltreTrier(idProduit){

        return this.http.get(`${environment.apiUrl}/emplacements/trier/`+idProduit,this.httpOptions);

    }

    getEmplacementByLabelAndProduit(modelEmpl){

        return this.http.post(`${environment.apiUrl}/emplacementsByIntitule`,modelEmpl,this.httpOptions);

    }


}
