import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Preferences } from '../models/preferences.model';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreferencesService {
  // public preferences = new Preferences() ;
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: `Bearer ${localStorage.getItem('token')}`,

    }),
  };

  constructor(private http: HttpClient) { }



  getPreferences(cbmarq): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/root/societe/${(cbmarq)}`, this.httpOptions)
  }



  addPreferences(preference: Preferences): Observable<any> {
    return this.http.post<Preferences>(`${environment.apiUrl}/administration/references`,preference, this.httpOptions)

  }
  updatePreferences(preference: Preferences): Observable<any> {
    return this.http.put<Preferences>(`${environment.apiUrl}/administration/references/update`,preference, this.httpOptions)
  }
}
