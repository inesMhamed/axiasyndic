import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Reunion } from '../models/reunion.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ReunionService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getReunions() {
        return this.http.get(`${environment.apiUrl}/communite/reunions`,this.httpOptions);
    }

    postReunion(reunion: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/ajout`, reunion,this.httpOptions);
    }

    postReunionParticipants(reunion: any , participants: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/participant/${(reunion)}`, participants,this.httpOptions);
    }
    postchatReunion(reunion: any, msg: any) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/chat/${(reunion)}`, msg, this.httpOptions);
    }

    getchatReunion(cbmarq) {
        return this.http.get(`${environment.apiUrl}/communite/reunion/chat/${(cbmarq)}`, this.httpOptions);
    }
    getReunion(cbmarq) {
        return this.http.get(`${environment.apiUrl}/communite/reunions/${(cbmarq)}`,this.httpOptions);
    }
    cloturerReunion(cbmarq,conclusion) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/cloturer/${(cbmarq)}`,conclusion, this.httpOptions);
    }
    joindreReunion(cbmarq) {
        return this.http.post(`${environment.apiUrl}/communite/joindrereunion/${(cbmarq)}`, this.httpOptions);
    }
    reportReunion(cbmarq, report) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/report/${(cbmarq)}`, report, this.httpOptions);
    }
    presenterReunion(cbmarq, presence) {
        return this.http.post(`${environment.apiUrl}/communite/presencereunion/${(cbmarq)}`, presence, this.httpOptions);
    }
    voterautoReunion(reunionID, noteID,vote) {
        return this.http.post(`${environment.apiUrl}/communite/voteautoreunion/${(reunionID)}/${(noteID)} `,vote, this.httpOptions);
    }
    lancerVote(reunionID, noteID) {
        return this.http.post(`${environment.apiUrl}/communite/lancervoteautoreunion/${(reunionID)}/${(noteID)} `, this.httpOptions);
    }
    votermanuelReunion(reunionID, noteID,vote) {
        return this.http.post(`${environment.apiUrl}/communite/votemanuellereunion/${(reunionID)}/${(noteID)} `,vote, this.httpOptions);
    }
    pointagerReunion(idparticipantsreunion) {
        return this.http.post(`${environment.apiUrl}/communite/pointagereunion/${(idparticipantsreunion)}`, this.httpOptions);
    }

    putReunion(reunion: any, cbmarq: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/edit/${(cbmarq)}`, reunion,this.httpOptions);
    }

    putReunionParticipants(cbmarq: any , participants: any) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/edit/participant/${(cbmarq)}`, participants,this.httpOptions);
    }

    sendInvitation(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/communite/reunion/envoi_invitation/${(cbmarq)}` ,this.httpOptions);
    }

    deleteReunion(reunion: any ) {
        return this.http.delete(`${environment.apiUrl}/communite/reunion/delete/${(reunion)}`,this.httpOptions);
    }

    annulerReunion(reunion: any, commentai: any) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/annuler/${(reunion)}`, commentai,this.httpOptions);
    }

    deleteNote(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/communite/reunion/notes/${(cbmarq)}`,this.httpOptions);
    }

    deleteAttachement(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/communite/reunion/attachement/${(cbmarq)}`,this.httpOptions);
    }

    postAttachementReunion(cbmarq: any , attachement: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/attachement/${(cbmarq)}`, attachement,this.httpOptions);
    }
    startReunion(cbmarq: any) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/commencer/${(cbmarq)}`, this.httpOptions);
    }

    addAutreInvitation(reunion: any , appartement: any ) {
        return this.http.post(`${environment.apiUrl}/communite/reunion/autre_appartement/${(reunion)}`, appartement,this.httpOptions);
    }
    /*****************************Groupement Api********************************/

    getInternes() {
        return this.http.get(`${environment.apiUrl}/communite/user/interne`,this.httpOptions);
    }
    getpresnecespointage(cbmarq) {
        return this.http.get(`${environment.apiUrl}/communite/participantsReunion/${(cbmarq)}`, this.httpOptions);
    }



    /*************** new services ************** */
    presenceReunion(cbmarq,modelPresence){

        return this.http.post(`${environment.apiUrl}/communite/presencereunion/${(cbmarq)}`,modelPresence,this.httpOptions);

    }

    getListInvitationReunionByUser(){

        return this.http.get(`${environment.apiUrl}/communite/invitations/byuser`,this.httpOptions);

    }

    getInvitationByReunionAndUserId(cbmarq){

        return this.http.get(`${environment.apiUrl}/communite/invitationuser/byreunion/`+cbmarq,this.httpOptions);

    }
}
