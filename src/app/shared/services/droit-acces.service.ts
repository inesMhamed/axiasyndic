import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Fonctionnalite } from '../models/fonctionnalite.model';
import {Subject} from 'rxjs';
import {Permission} from '../models/permission.model';

@Injectable({
  providedIn: 'root'
})
export class DroitAccesService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`,

        }),
      };
    results: Permission;
  constructor(private http: HttpClient) { }


  getFonctionalitesRole(type): Observable<any> {
    return this.http.get<Fonctionnalite[]>(`${environment.apiUrl}/fonctionnalite/role/${(type)}`, this.httpOptions)
  }
  getFonctionalitesUser(cbmarq): Observable<any> {
    return this.http.get<Fonctionnalite[]>(`${environment.apiUrl}/fonctionnalite/user/${(cbmarq)}`, this.httpOptions)
  }
  getGroupes(): Observable<any> {
    return this.http.get<Fonctionnalite[]>(`${environment.apiUrl}/fonctionnalite/groupe`, this.httpOptions)
  }
   getPermissionUser(cbmarq, reference) {
    var check =  new Permission();
    this.http.get<any>(`${environment.apiUrl}/permission/user/${(cbmarq)}/${(reference)}`, this.httpOptions).subscribe(
         (res: any) => {
          check.permission =  res.data;
        });
    return check?.permission;
  }

  getAccessUser(cbmarq,reference){
    return this.http.get<Fonctionnalite[]>(`${environment.apiUrl}/permission/user/${(cbmarq)}/${(reference)}`, this.httpOptions)

  }
  // getPermissionFonctionalitesUser(cbmarq, reference): Promise<boolean> {
  //  return this.getPermissionUser(cbmarq, reference);
  // }

    search(cbmarq, reference) {
        var check =  new Permission();
        let promise = new Promise((resolve, reject) => {
            this.http.get<any>(`${environment.apiUrl}/permission/user/${(cbmarq)}/${(reference)}`, this.httpOptions)
                .toPromise()
                .then(
                    res => { // Success

                        check.permission = res.data;
                        resolve();
                    },
                    msg => { // Error
                        reject(msg);
                    }
                );
        });
        return check;
    }

  addFonction(fonction: Fonctionnalite): Observable<any> {
    return this.http.post<Fonctionnalite>(`${environment.apiUrl}/fonctionnalite`, fonction, this.httpOptions);

  }

  editFonctionRole(arrayfonctionnalite: any, type: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/fonctionnalite/edit-fonctionnalite-role/${(type)}`, arrayfonctionnalite, this.httpOptions);
  }

  editFonctionUser(fonctionnalites: any, cbmarq: number): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/fonctionnalite/edit-fonctionnalite-user/${(cbmarq)}`, fonctionnalites, this.httpOptions);
  }
}
