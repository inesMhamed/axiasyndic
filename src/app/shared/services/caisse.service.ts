import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Residence } from '../models/residence.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CaisseService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getCaisses() {
        return this.http.get(`${environment.apiUrl}/caisse`,this.httpOptions);
    }

    getAllCaisses() {
        return this.http.get(`${environment.apiUrl}/caisse/all`,this.httpOptions);
    }

    getCaissebyId(cbmarq) {
        return this.http.get(`${environment.apiUrl}/caisse/${(cbmarq)}`,this.httpOptions);
    }
    postCaisse(caisse: any) {
        return this.http.post(`${environment.apiUrl}/caisse`, caisse,this.httpOptions);
    }
    getResponsable(cbmarq: any,type:any) {
        return this.http.get(`${environment.apiUrl}/caisse/responsables/${(cbmarq)}/${(type)}`,this.httpOptions);
    }
    getClientdocs(cbmarq: any,type:number) {
        return this.http.get<any>(`${environment.apiUrl}/clients/alldocument/${(cbmarq)}/${(type)}`,this.httpOptions)
      }
      
    AddResponsable(caisse: any,cbmarq:any,type:any) {
        return this.http.post(`${environment.apiUrl}/caisse/responsables/${(cbmarq)}/${(type)}`, caisse,this.httpOptions);
    }
    putCaisse(caisse: any) {
        return this.http.put(`${environment.apiUrl}/caisse/${(caisse.cbmarq)}`, caisse,this.httpOptions);
    }
    deleteCaisse(cbmarq){
        return this.http.delete(`${environment.apiUrl}/caisse/${(cbmarq)}`,this.httpOptions);
    }
    getCaisseSiege() {
        return this.http.get(`${environment.apiUrl}/caisse-siege`,this.httpOptions);
    }
    getAllCaisseSiege() {
        return this.http.get(`${environment.apiUrl}/caisse-siege/all`,this.httpOptions);
    }
    getCaisseSiegebyId(cbmarq) {
        return this.http.get(`${environment.apiUrl}/caisse-siege/${(cbmarq)}`,this.httpOptions);
    }
    postCaisseSiege(caisse: any) {
        return this.http.post(`${environment.apiUrl}/caisse-siege`, caisse,this.httpOptions);
    }
    putCaisseSiege(caisse: any) {
        return this.http.put(`${environment.apiUrl}/caisse-siege/${(caisse.cbmarq)}`, caisse,this.httpOptions);
    }
    deleteCaisseSiege(cbmarq){
        return this.http.delete(`${environment.apiUrl}/caisse-siege/${(cbmarq)}`,this.httpOptions);
    }
    deleteResponsable(type,idcaisse,idresponsable){
        // /api/caisse/remove-responsables/{type}/{idcaisse}/{idresponsable}
        return this.http.delete(`${environment.apiUrl}/caisse/remove-responsables/${(type)}/${(idcaisse)}/${(idresponsable)}`,this.httpOptions);
    }
    gettypeCaisse(code){
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`, this.httpOptions);
    }
    getResponsableCaisse(cbmarq,type){
        return this.http.get(`${environment.apiUrl}/caisse/responsables/${(cbmarq)}/${(type)}`, this.httpOptions);
    }
    getCaisseSiegeByResponsable(){
        return this.http.get(`${environment.apiUrl}/caissesiegebyresponsable`, this.httpOptions);
    }
    getCaisseByResponsable(){
        return this.http.get(`${environment.apiUrl}/caissebyresponsable`, this.httpOptions);
    }
    getResponsableNoExiste(cbmarq , type) {
        return this.http.get(`${environment.apiUrl}/caisse/responsablesnonexist/${(cbmarq)}/${(type)}`, this.httpOptions);
    }
    getCaisseInstance(){
        return this.http.get(`${environment.apiUrl}/caisse-instance`, this.httpOptions);
    }
    getCaisseInstanceByResponsable(){
        return this.http.get(`${environment.apiUrl}/caisse-instance/byuser`, this.httpOptions);
    }
    getMouvementCaisse(cbmarq){
        return this.http.get(`${environment.apiUrl}/caisse/mouvement-caisse/info/${(cbmarq)}`, this.httpOptions);
    }

    //wz
    getCaisseSource(){
        return this.http.get(`${environment.apiUrl}/caisses/source`, this.httpOptions);

    }
    getCaisseDestination(){
        return this.http.get(`${environment.apiUrl}/caisses/destinataire`,this.httpOptions);

    }
}
