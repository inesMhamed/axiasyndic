import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TransfertService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }

    getTransfert(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/transfert/${(cbmarq)}`,this.httpOptions);
    }
    
    postTransfert(transfert: any) {
        return this.http.post(`${environment.apiUrl}/transfert`, transfert,this.httpOptions);
    }

    getAllTransferts() {
        return this.http.get(`${environment.apiUrl}/transfert`,this.httpOptions);
    }
    getReglemetsConfirmer(cbmarq) {
        return this.http.get(`${environment.apiUrl}/transfert/reglement-confirm/${(cbmarq)}`,this.httpOptions);
    }
    getElementToTransfert(cbmarq, type) {
        return this.http.get(`${environment.apiUrl}/transfert/elements-to-transfert/${(cbmarq)}/${(type)}`, this.httpOptions);
    }

    putTransfert(transfert: any,cbmarq) {
        return this.http.put(`${environment.apiUrl}/transfert/${(cbmarq)}`, transfert,this.httpOptions);
    }
    deleteTransfert(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/transfert/${(cbmarq)}`,this.httpOptions);
    }
    confirmTransfert(cbmarq){
        return this.http.post(`${environment.apiUrl}/transfert/confirmation/${(cbmarq)}`,'',this.httpOptions);
    }
    cancelTransfert(cbmarq){
        return this.http.post(`${environment.apiUrl}/transfert/annulation/${(cbmarq)}`,'',this.httpOptions);
    }
    gettypet(code){
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`,this.httpOptions);
    }
    acceptTransfert(cbmarq){
        return this.http.post(`${environment.apiUrl}/transfert/acceptation/${(cbmarq)}`,this.httpOptions);
    }

    getSuivi(cbmarq){

        return this.http.get(`${environment.apiUrl}/transfert/suivi/${(cbmarq)}`,this.httpOptions);

    }

    getAllTransfertsRecusEnvoi() {

        return this.http.get(`${environment.apiUrl}/transfert/recu/envoi`,this.httpOptions);

    }

    acceptSendTransfertIntermidiare(cbmarq){

        return this.http.post(`${environment.apiUrl}/transfert/acceptation/intermediare/${(cbmarq)}`,this.httpOptions);

    }
}
