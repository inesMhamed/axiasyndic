import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Residence } from '../models/residence.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ResidenceService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

    getResidences() {
        return this.http.get(`${environment.apiUrl}/administration/produit/all`,this.httpOptions);
    }
    getResidencesDesactives() {
        return this.http.get(`${environment.apiUrl}/administration/produit/all/desactive`,this.httpOptions);
    }
    getDeligues(cbmarq) {
        return this.http.get(`${environment.apiUrl}/administration/produit/${(cbmarq)}`,this.httpOptions);
    }

    getSearchResidences() {
        return this.http.get(`${environment.apiUrl}/administration/produits/search`, this.httpOptions);
    }

    postResidences(residence: any ) {
        //console.log('servicer', residence)
        return this.http.post(`${environment.apiUrl}/administration/produit`, residence , this.httpOptions);
    }

    getResidence(cbmarq) {
        return this.http.get(`${environment.apiUrl}/administration/produit/${(cbmarq)}` , this.httpOptions);
    }

    gettypebien(cbmarq) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(cbmarq)}`, this.httpOptions);
    }

    putResidences(residence: any, cbmarq: any ) {
        return this.http.post(`${environment.apiUrl}/administration/produit/${(cbmarq)}`, residence, this.httpOptions);
    }

    deleteResidence(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/administration/produit/${(cbmarq)}`, this.httpOptions);
    }

    desactiverResidences( cbmarq: any , active: any) {
        return this.http.post(`${environment.apiUrl}/administration/produit/activation/${(cbmarq)}/${(active)}`, this.httpOptions);
    }



    /*****************************Groupement Api TYB********************************/
    getResidencesNoCroupe() {
        return this.http.get(`${environment.apiUrl}/administration/res_groupe`);
    }
    getResidencesByCroupe(cbmarq: any) {
        return this.http.get(`${environment.apiUrl}/administration/produit/residence/groupement/${(cbmarq)}`);
    }
}
