import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BlocService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }

    getAppartementsBloc(bloc: any) {
        return this.http.get(`${environment.apiUrl}/appartement/bybloc/${(bloc)}`, this.httpOptions);
    }
    getAppartementByBlocHaveProp(bloc) {
        return this.http.get(`${environment.apiUrl}/bien/appartement/haveprop/bybloc/${(bloc)}`, this.httpOptions);
    }
    getBlocs(residence: any) {
        return this.http.get(`${environment.apiUrl}/administration/bloc/residence/${(residence)}`,this.httpOptions);
    }
    genererBloc(bloc) {
        return this.http.post(`${environment.apiUrl}/administration/produit/generate/bloc`, bloc,this.httpOptions);
    }
    postBloc(bloc: any) {
        return this.http.post(`${environment.apiUrl}/administration/bloc`, bloc,this.httpOptions);
    }

    // getBloc(cbmarq) {
    //     return this.http.get(`${environment.apiUrl}/administration/bloc/id/${(cbmarq)}`,this.httpOptions);
    // }

    getBloc(cbmarq) {
        return this.http.get(`${environment.apiUrl}/administration/bloc/${(cbmarq)}`, this.httpOptions);
    }
    putBloc(bloc: any) {
        return this.http.put(`${environment.apiUrl}/administration/bloc/${(bloc.cbmarq)}`, bloc,this.httpOptions);
    }
    deleteBloc(cbmarq: any) {
        return this.http.delete(`${environment.apiUrl}/administration/bloc/${(cbmarq)}`,this.httpOptions);
    }
}
