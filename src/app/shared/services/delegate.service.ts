import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DelegateService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) {
    }
    addDelegate(user,produit) {
        return this.http.post(`${environment.apiUrl}/administration/delegue/add-delegue/${(user)}​/${(produit)}`, this.httpOptions);
    }
    removeDelegate(user,produit) {
        return this.http.post(`${environment.apiUrl}/administration/delegue/retirer-delegue/${(user)}​/${(produit)}`, this.httpOptions);
    }
    synchronazeDelegate(produit) {
        return this.http.post(`${environment.apiUrl}/administration/delegue/synchronisation-delegue/${(produit)}`, this.httpOptions);
    }
    getDelegates(produit) {
        return this.http.get(`${environment.apiUrl}/administration/delegue/list-delegue/${(produit)}`,this.httpOptions);
    }
    getLogDelegates(produit) {
        return this.http.get(`${environment.apiUrl}/administration/delegue/historique/${(produit)}`,this.httpOptions);
    }

}
