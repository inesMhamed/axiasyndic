import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import { Actualite } from '../models/actualites.model';
import { Document } from '../models/document.model';
import { TreeNode } from 'primeng/api';
import { AddDocument } from '../models/add.document.model';

@Injectable({
    providedIn: 'root'
})
export class ActualiteService {
    httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
      };
    constructor(
        private http: HttpClient
    ) { }

 
    getActualites() {
        return this.http.get(`${environment.apiUrl}/actualite/actualite-by-user`,this.httpOptions);
    }

    getCommentsOfActualite(publicationId:string){
        return this.http.get(`${environment.apiUrl}/mobile/getreactioncomment/`+publicationId,this.httpOptions);
    }

    deleteComment(commentObject,cbmarq){
        ///api/actualite/delete/commentaire/{recation}
        return this.http.post(`${environment.apiUrl}/actualite/delete/commentaire/`+cbmarq,commentObject,this.httpOptions);

    }

    postReactionOfActualite(commentObject,publicationId){
        /*
            {
            "type": "1: commentaire , 2: j'aime, 3: je n'aime pas",
            "commentaire": "string"
            }
        */

            return this.http.post(`${environment.apiUrl}/mobile/publication/reaction/`+publicationId,commentObject,this.httpOptions);

    }

    postActualites(actualite: any) {
        return this.http.post(`${environment.apiUrl}/actualite/ajout`, actualite , this.httpOptions);
    }

    updateStatutActualite(actualite:Actualite){
        return this.http.post(`${environment.apiUrl}/actualite/updateStatut_actualite/`+actualite.cbmarq ,actualite, this.httpOptions);

    }


    deleteDefinetlyPost(actualite:Actualite){
        return this.http.post(`${environment.apiUrl}/actualite/delete/`+actualite.cbmarq,actualite,this.httpOptions);

    }

    /**** documents API *****/

    getDocumentById(cbmarq){
        return this.http.get(`${environment.apiUrl}/actualite/document/by-id/`+cbmarq,this.httpOptions);

    }

    getDocumentByCategorie(categorieGetDocModel){
        return this.http.post(`${environment.apiUrl}/actualite/documents-by-user-category`,categorieGetDocModel,this.httpOptions);

    }

    getDocumentByUser(){
        return this.http.get(`${environment.apiUrl}/actualite/documents-by-user`,this.httpOptions);

    }

    getAllDocument(){

       return this.http.get<any>('../../../assets/documents.json');


    }

    updateStatutDocument(cbmarq:any){
        let document:Document=new Document();
        return this.http.post(`${environment.apiUrl}/actualite/updateStatut_document/`+cbmarq,document,this.httpOptions);

    }

    addDocument(document:FormData){
        return this.http.post(`${environment.apiUrl}/actualite/document/ajout`,document,this.httpOptions);

    }

    editDocument(iddoc,document:FormData){
        return this.http.post(`${environment.apiUrl}/actualite/document/edit/`+iddoc,document,this.httpOptions);

    }


    getResidenceNode() {

        return this.http.get(`${environment.apiUrl}/actualite/produitByUser`,this.httpOptions);
    }

    filterProduit(){
        return this.http.get(`${environment.apiUrl}/actualite/document/filterProduit`,this.httpOptions);

    }

    /***************** MESSAGERIE API ****************** */
    //messagerie reçus
    getAllMessagerie() {

        return this.http.get(`${environment.apiUrl}/actualite/messagerie/all`,this.httpOptions);
    }
    //messagerie envoyé
    getMessagerieEnvoyee() {

        return this.http.get(`${environment.apiUrl}/actualite/messagerie/envoi`,this.httpOptions);
    }
    //corbeille
    getMessagerieSupprime() {

        return this.http.get(`${environment.apiUrl}/actualite/messagerie/supprimer`,this.httpOptions);
    }
    getMessagerieById(cbmarq) {

        return this.http.get(`${environment.apiUrl}/actualite/messagerie/`+cbmarq,this.httpOptions);
    }
    addMessagerie(messagerie) {

        return this.http.post(`${environment.apiUrl}/actualite/messagerie/ajout`,messagerie,this.httpOptions);
    }
    deleteMessagerie(cbmarq){
        return this.http.delete(`${environment.apiUrl}/actualite/messagerie/`+cbmarq,this.httpOptions);

    }
    setMesagerieVu(cbmarq){
        return this.http.get(`${environment.apiUrl}/actualite/messagerie/vue/`+cbmarq,this.httpOptions);

    }

    getListContact(){
        return this.http.get(`${environment.apiUrl}/actualite/messagerie/contacts`,this.httpOptions);

    }






}
