import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SharedDataService {

    private message = new BehaviorSubject('');
    sharedMessage = this.message.asObservable();

    /*==================== CONFIG MAINTENANCE =====================*/
    //famille
    viewListFamilleEquipement=new BehaviorSubject(false);
    sharedViewFamille = this.viewListFamilleEquipement.asObservable();

    idFamilleEquipement=new BehaviorSubject(0);
    sharedidFamilleEquipement = this.idFamilleEquipement.asObservable();

    //equipement
    viewListEquipement=new BehaviorSubject(false);
    sharedViewEquipement = this.viewListEquipement.asObservable();

    idEquipement=new BehaviorSubject(0);
    sharedidEquipement = this.idEquipement.asObservable();

    //G.Operatoire
    viewListGammeOperatoire=new BehaviorSubject(false);
    sharedViewGammeOperatoire = this.viewListGammeOperatoire.asObservable();

    idGOperatoire=new BehaviorSubject(0);
    sharedidGOperatoire = this.idGOperatoire.asObservable();

    //operation
    viewListOperation=new BehaviorSubject(false);
    sharedViewOperation = this.viewListOperation.asObservable();

    viewListGenOperation=new BehaviorSubject(false);    
    sharedViewGenOperation = this.viewListGenOperation.asObservable();

    idOperation=new BehaviorSubject(0);
    sharedidOperation = this.idOperation.asObservable();

    //emplacement
    viewListEmplacement=new BehaviorSubject(false);
    sharedViewEmplacement = this.viewListEmplacement.asObservable();

    idEmplacement=new BehaviorSubject(0);
    sharedidEmplacement = this.idEmplacement.asObservable();

    //sous emplacement
    viewListSousEmplacement=new BehaviorSubject(false);
    sharedViewSousEmplacement = this.viewListSousEmplacement.asObservable();

    idSousEmplacement=new BehaviorSubject(0);
    sharedidSousEmplacement = this.idSousEmplacement.asObservable();

    /*====================== PARAMS TYPES ================*/
    //typeproduit
    viewListTypeProduit=new BehaviorSubject(false);
    sharedviewListTypeProduit = this.viewListTypeProduit.asObservable();

    idTypeProduit=new BehaviorSubject(0);
    sharedidTypeProduit = this.idTypeProduit.asObservable();

    //CategorieBien
    viewListCategorieBien=new BehaviorSubject(false);
    sharedviewListCategorieBien = this.viewListCategorieBien.asObservable();

    idCategorieBien=new BehaviorSubject(0);
    sharedidCategorieBien = this.idCategorieBien.asObservable();

    //NatureBien
    viewListNatureBien=new BehaviorSubject(false);
    sharedviewListNatureBien = this.viewListNatureBien.asObservable();

    idNatureBien=new BehaviorSubject(0);
    sharedidNatureBien = this.idNatureBien.asObservable();

    //TypeBien
    viewListTypeBien=new BehaviorSubject(false);
    sharedviewListTypeBien = this.viewListTypeBien.asObservable();

    idTypeBien=new BehaviorSubject(0);
    sharedidTypeBien = this.idTypeBien.asObservable();

    //TypeService
    viewListTypeService=new BehaviorSubject(false);
    sharedviewListTypeService = this.viewListTypeService.asObservable();

    idTypeService=new BehaviorSubject(0);
    sharedidTypeService = this.idTypeService.asObservable();

    //TypeCompteur
    viewListTypeCompteur=new BehaviorSubject(false);
    sharedviewListTypeCompteur = this.viewListTypeCompteur.asObservable();

    idTypeCompteur=new BehaviorSubject(0);
    sharedidTypeCompteur = this.idTypeCompteur.asObservable();

    //CategorieReclamation
    viewListCategorieReclamation=new BehaviorSubject(false);
    sharedviewListCategorieReclamation = this.viewListCategorieReclamation.asObservable();

    idCategorieReclamation=new BehaviorSubject(0);
    sharedidCategorieReclamation = this.idCategorieReclamation.asObservable();

    //ModePaiement
    viewListModePaiement=new BehaviorSubject(false);
    sharedviewListModePaiement = this.viewListModePaiement.asObservable();

    idModePaiement=new BehaviorSubject(0);
    sharedidModePaiement = this.idModePaiement.asObservable();

    //TypeDepense
    viewListTypeDepense=new BehaviorSubject(false);
    sharedviewListTypeDepense = this.viewListTypeDepense.asObservable();

    idTypeDepense=new BehaviorSubject(0);
    sharedidTypeDepense = this.idTypeDepense.asObservable();



    /*=====================================================*/


    constructor() { }
  
    nextMessage(message: string) {
      this.message.next(message)
    }

    /*========== CONFIG MAINTENANCE =============*/
    //change view
    changeViewFamille(visibilité:boolean){
        this.viewListFamilleEquipement.next(visibilité);
    }
 
    changeViewEquipement(visibilité:boolean){
        this.viewListEquipement.next(visibilité);
    }

    changeViewGammeOperatoire(visibilité:boolean){
        this.viewListGammeOperatoire.next(visibilité);
    }

    changeViewOperation(visibilité:boolean){
        this.viewListOperation.next(visibilité);
    }

    changeViewGenOperation(visibilité:boolean){
        this.viewListGenOperation.next(visibilité);

        //console.log("changeViewGenOperation: ",visibilité)
    }

    changeViewEmplacement(visibilité:boolean){
        this.viewListEmplacement.next(visibilité);
    }

    changeViewSousEmplacement(visibilité:boolean){
        this.viewListSousEmplacement.next(visibilité);
    }

    //sendID
    sendIdFamille(id:number){
        this.idFamilleEquipement.next(id);
    }
    sendIdEquipement(id:number){
        this.idEquipement.next(id);
    }
    sendIdGOperatoire(id:number){
        this.idGOperatoire.next(id);
    }
    sendIdOperation(id:number){
        this.idOperation.next(id);
    }

    sendIdEmplacement(id:number){
        this.idEmplacement.next(id);
    }

    sendIdSousEmplacement(id:number){
        this.idSousEmplacement.next(id);
    }

    /*========== CONFIG TYPES =============*/

     //change view
     changeViewTypeProduit(visibilité:boolean){
        this.viewListTypeProduit.next(visibilité);
    }

    changeViewCategorieBien(visibilité:boolean){
        this.viewListCategorieBien.next(visibilité);
    }

    changeViewNatureBien(visibilité:boolean){
        this.viewListNatureBien.next(visibilité);
    }

    changeViewTypeBien(visibilité:boolean){
        this.viewListTypeBien.next(visibilité);
    }

    changeViewTypeService(visibilité:boolean){
        this.viewListTypeService.next(visibilité);
    }

    changeViewTypeCompteur(visibilité:boolean){
        this.viewListTypeCompteur.next(visibilité);
    }

    changeViewCategorieReclamation(visibilité:boolean){
        this.viewListCategorieReclamation.next(visibilité);
    }

    changeViewModePaiement(visibilité:boolean){
        this.viewListModePaiement.next(visibilité);
    }

    changeViewTypeDepense(visibilité:boolean){
        this.viewListTypeDepense.next(visibilité);
    }

    //sendID
    sendIdTypeProduit(id:number){
        this.idTypeProduit.next(id);
    }
    sendIdCategorieBien (id:number){
        this.idCategorieBien.next(id);
    }
    sendIdNatureBien(id:number){
        this.idNatureBien.next(id);
    }
    sendIdTypeBien(id:number){
        this.idTypeBien.next(id);
    }

    sendIdTypeService(id:number){
        this.idTypeService.next(id);
    }

    sendIdTypeCompteur(id:number){
        this.idTypeCompteur.next(id);
    }

    sendIdCategorieReclamation(id:number){
        this.idCategorieReclamation.next(id);
    }

    sendIdModePaiement(id:number){
        this.idModePaiement.next(id);
    }

    sendIdTypeDepense(id:number){
        this.idTypeDepense.next(id);
    }



}
