import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Compteur } from '../models/compteur.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ContratService {
    listAlert: any = []
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }),
    };
    constructor(
        private http: HttpClient
    ) { }

    getContrats() {

        return this.http.get(`${environment.apiUrl}/administration/contrat`, this.httpOptions);
    }
    getAlertContrats() {
        return this.http.get(`${environment.apiUrl}/administration/contrat/alerts/getall`, this.httpOptions);
    }
    getAppartementContrat() {
        return this.http.get(`${environment.apiUrl}/administration/contrat/alerts/getall`, this.httpOptions);
    }
    getAppartementCompteur() {
        return this.http.get(`${environment.apiUrl}/administration/contrat/alerts/getall`, this.httpOptions);
    }
    getContratsGroupement(idgroupe) {
        return this.http.get(`${environment.apiUrl}/administration/contratbygroupement/${(idgroupe)}`, this.httpOptions);
    }
    getContratsResidence(residence) {
        return this.http.get(`${environment.apiUrl}/administration/contrat/${(residence)}`, this.httpOptions);
    }
    postContrat(compteur: any) {
        return this.http.post(`${environment.apiUrl}/administration/contrat/add-contrat`, compteur, this.httpOptions);
    }

    getContrat(cbmarq) {
        return this.http.get(`${environment.apiUrl}/administration/contrat/id/${(cbmarq)}`, this.httpOptions);
    }

    putContrat(compteur: any, cbmarq: any) {
        return this.http.post(`${environment.apiUrl}/administration/contrat/${(cbmarq)}`, compteur, this.httpOptions);
    }
    setStatus(cbmarq){
        return this.http.post(`${environment.apiUrl}/administration/contrat/alert/seen/${(cbmarq)}`, this.httpOptions);
    }
    gettypesContrat(code: any) {
        return this.http.get(`${environment.apiUrl}/root/parametre/categorie?CodeCategorie=${(code)}`, this.httpOptions);
    }

    getappartements(residence: any) {
        return this.http.get(`${environment.apiUrl}/administration/appartement/${(residence)}`, this.httpOptions);
    }

    deleteContrat(cbmarq) {
        return this.http.delete(`${environment.apiUrl}/administration/contrat/${(cbmarq)}`, this.httpOptions);
    }
}
