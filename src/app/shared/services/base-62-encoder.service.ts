import { Injectable } from '@angular/core';

@Injectable()
export class Base62EncoderService {

  charset = '0123456789àçèéêëìíîïñòóôœùúûüýÿabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    .split('');


  encode(paramToBeEncoded: number | string ) {
    let integer = +paramToBeEncoded;
    if (integer === 0) {
      return 0;
    }
    let s = [];
    while (integer > 0) {
      s = [this.charset[integer % 73], ...s];
      integer = Math.floor(integer / 73);
    }
    return s.join('');
  }

  decode(chars: string | number) {
    return chars.toString().split('').reverse().reduce((prev, curr, i) =>
      prev + (this.charset.indexOf(curr) * (73 ** i)), 0)
  }

}